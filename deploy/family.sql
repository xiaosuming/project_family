-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: 120.26.103.174    Database: family
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gener_action`
--

DROP TABLE IF EXISTS `gener_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_action` (
  `action` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` int(11) NOT NULL,
  PRIMARY KEY (`action`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_gener_action_module_idx` (`module`),
  CONSTRAINT `fk_gener_action_module` FOREIGN KEY (`module`) REFERENCES `gener_module` (`module`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_activity`
--

DROP TABLE IF EXISTS `gener_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(10) unsigned NOT NULL,
  `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `deadline` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_activity_1_idx` (`familyId`),
  KEY `index5` (`start_time`),
  KEY `index6` (`deadline`),
  CONSTRAINT `fk_gener_activity_1` FOREIGN KEY (`familyId`) REFERENCES `gener_family` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_activityComment`
--

DROP TABLE IF EXISTS `gener_activityComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_activityComment` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `activityId` int(30) NOT NULL,
  `userId` int(30) NOT NULL,
  `replyto` int(11) NOT NULL,
  `replyto_userId` int(11) NOT NULL DEFAULT '-1',
  `comment` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_activity_participator`
--

DROP TABLE IF EXISTS `gener_activity_participator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_activity_participator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activityId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `cancel` int(11) NOT NULL COMMENT '是否取消，0是未取消，1是取消',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_album_person`
--

DROP TABLE IF EXISTS `gener_album_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_album_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photoId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_area`
--

DROP TABLE IF EXISTS `gener_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_area` (
  `areaid` int(11) NOT NULL,
  `areaname` varchar(50) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`areaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_auth`
--

DROP TABLE IF EXISTS `gener_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auth` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  `operateId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_branch`
--

DROP TABLE IF EXISTS `gener_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分支名',
  `branch_personId` int(11) DEFAULT NULL COMMENT '如果是主分支的话，会存在一个特殊的合并点',
  `type` int(11) NOT NULL COMMENT 'type代表分支的类型，1是主分支,2是子分支',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL COMMENT '分支的连接人物id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_collection`
--

DROP TABLE IF EXISTS `gener_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_collection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `moduleId` int(11) NOT NULL,
  `recordId` int(11) NOT NULL,
  `record_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `record_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_info1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_info2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_eventComment`
--

DROP TABLE IF EXISTS `gener_eventComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_eventComment` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `eventId` int(30) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `replyto` int(30) NOT NULL DEFAULT '-1',
  `replyto_userId` int(30) NOT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_eventEditHistory`
--

DROP TABLE IF EXISTS `gener_eventEditHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_eventEditHistory` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `eventId` int(30) NOT NULL,
  `exp_time` date NOT NULL,
  `exp_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `exp_content` text CHARACTER SET utf8mb4 NOT NULL,
  `edit_type` int(10) NOT NULL,
  `writer` int(30) NOT NULL,
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_gener_eventEditHistory_1_idx` (`eventId`),
  CONSTRAINT `fk_gener_eventEditHistory_1` FOREIGN KEY (`eventId`) REFERENCES `gener_significantEvent` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=491 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family`
--

DROP TABLE IF EXISTS `gener_family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family` (
  `id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `min_level` int(10) NOT NULL,
  `max_level` int(10) NOT NULL,
  `permission` int(10) NOT NULL,
  `originator` int(30) NOT NULL,
  `member_count` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `qrcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hx_chatroom` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_Family_originator_idx` (`originator`)
) ENGINE=InnoDB AUTO_INCREMENT=307 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_familyAdmin`
--

DROP TABLE IF EXISTS `gener_familyAdmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_familyAdmin` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `familyId` int(30) unsigned NOT NULL,
  `adminId` int(30) NOT NULL,
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_familyAdmin_1_idx` (`familyId`),
  CONSTRAINT `fk_gener_familyAdmin_family` FOREIGN KEY (`familyId`) REFERENCES `gener_family` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_familyNews`
--

DROP TABLE IF EXISTS `gener_familyNews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_familyNews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(30) unsigned NOT NULL,
  `module` int(6) NOT NULL,
  `action` int(6) NOT NULL,
  `operatorId` int(30) NOT NULL,
  `thingId` int(30) NOT NULL COMMENT '存储的是被操作的事物的id',
  `thing_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `extra_info1` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '额外信息1，例如某人为某人增加了照片，可以在这里存放照片的路径',
  `extra_info2` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '额外信息2',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_familyNews_module_idx` (`module`),
  KEY `fk_gener_familyNews_action_idx` (`action`),
  CONSTRAINT `fk_gener_familyNews_action` FOREIGN KEY (`action`) REFERENCES `gener_action` (`action`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_familyNews_module` FOREIGN KEY (`module`) REFERENCES `gener_module` (`module`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_album`
--

DROP TABLE IF EXISTS `gener_family_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相册名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相册描述',
  `familyId` int(11) NOT NULL,
  `album_size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `update_by` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_branch`
--

DROP TABLE IF EXISTS `gener_family_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `branchId` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_heirloom`
--

DROP TABLE IF EXISTS `gener_family_heirloom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_heirloom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_legend`
--

DROP TABLE IF EXISTS `gener_family_legend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_legend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_permission`
--

DROP TABLE IF EXISTS `gener_family_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `personId` int(11) NOT NULL,
  `familyId` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_photo`
--

DROP TABLE IF EXISTS `gener_family_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `albumId` int(11) NOT NULL COMMENT '照片所在家族的相册',
  `photo` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片url',
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `country` int(11) NOT NULL DEFAULT '-1',
  `province` int(11) NOT NULL DEFAULT '-1',
  `city` int(11) NOT NULL DEFAULT '-1',
  `area` int(11) NOT NULL,
  `town` int(11) NOT NULL,
  `size` int(11) NOT NULL COMMENT '图片的大小,单位为KB',
  `address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_user`
--

DROP TABLE IF EXISTS `gener_family_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_user` (
  `id` int(13) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(13) unsigned NOT NULL,
  `userId` int(13) unsigned NOT NULL,
  `is_delete` int(1) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_feedback`
--

DROP TABLE IF EXISTS `gener_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feedback_message` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `handle_status` int(11) NOT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `problem_type` int(11) NOT NULL,
  `photo` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_by` char(255) CHARACTER SET latin1 NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` char(255) CHARACTER SET latin1 NOT NULL,
  `update_time` datetime NOT NULL,
  `sender` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_state` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_file`
--

DROP TABLE IF EXISTS `gener_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) CHARACTER SET utf8 NOT NULL,
  `module` int(11) NOT NULL,
  `create_by` int(8) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file_path_UNIQUE` (`file_path`)
) ENGINE=InnoDB AUTO_INCREMENT=2687 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_clien