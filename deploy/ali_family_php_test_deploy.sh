#!/bin/bash
## 自动部署脚本,不要在本地执行!!!!!
cd homepage

# 自动将测试服务器的数据库表转移到family_test
mysqldump -d -ufamily -pFlare1111 -h120.26.103.174 family > family.sql
mysql -ufamily -pFlare1111 -h120.26.103.174 family_test < family.sql

# 修改db的数据库名
echo "\$GLOBALS['db_name'] = 'family_test';" >> php/config/db_config.php

# 写死激活邮件的auth code
sed -i 's/\$auth = Util::generateRandomCode(60);/\$auth = "123456123456123456";/g' php/logicserver/account_action/1/userRegister.php

# 写死验证码
sed -i 's/\$vcode = Util::generateVcode(6);/\$vcode = 123456;/g' php/logicserver/account_action/1/getPhoneAuth.php

scp -r php/* root@120.26.103.174:/alidata/www/family_test/php 

