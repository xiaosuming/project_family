#!/bin/bash
## 自动部署脚本
cd homepage

echo "<?php\nrequire_once('environment.prod.php');" > php/config/environment.php  

# 覆盖掉配置文件
cp /home/xinhuo/db_config.php php/config/db_config.php

# 覆盖掉配置文件
cp /home/xinhuo/process_config.php ../processor/config.php

scp -r ../processor/* root@116.62.25.128:/var/www/family/process

scp -r php/* root@116.62.25.128:/var/www/family/php
