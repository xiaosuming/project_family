#!/bin/bash
## 自动部署脚本
cd homepage/html
cnpm install
ng build -prod -aot -env=prod --no-extract-license
scp -r dist/* root@120.26.103.174:/alidata/www/family

cd src

scp  login.html root@120.26.103.174:/alidata/www/family
scp  login.js root@120.26.103.174:/alidata/www/family

scp  register-email.html root@120.26.103.174:/alidata/www/family
scp  register-phone.html root@120.26.103.174:/alidata/www/family
scp  register.css root@120.26.103.174:/alidata/www/family
scp  register.js root@120.26.103.174:/alidata/www/family

