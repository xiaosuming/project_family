#!/bin/bash
## 爱族群手机页面自动部署脚本

cd homepage/html
cnpm install
ng build -prod -aot -env=izuqun --no-extract-license
scp -r dist/* root@116.62.25.128:/var/www/family

cd src

scp  login.html root@120.26.103.174:/alidata/www/family
scp  login.js root@120.26.103.174:/alidata/www/family

scp  register-email.html root@120.26.103.174:/alidata/www/family
scp  register-phone.html root@120.26.103.174:/alidata/www/family
scp  register.css root@120.26.103.174:/alidata/www/family
scp  register.js root@120.26.103.174:/alidata/www/family

