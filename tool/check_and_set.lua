-- author: jiangpengfei
-- date: 2018-04-17
-- 令牌通算法的的lua脚本
-- 注意：修改该脚本务必跑tool文件夹中的init_xxxx_server.php
--      并更新global_struct中的sha值
-- $bucket是KEYS[1], $capacity是ARGV[1], $time是ARGV[2], $cycle是ARGV[3]

local function mysplit(inputstr, sep)
    if sep == nil then
            sep = "%s"
    end
    local t={} ; 
    local i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
            t[i] = str
            i = i + 1
    end
    return t
end

local bucket = KEYS[1]
local cap = tonumber(ARGV[1])
local time = tonumber(ARGV[2])
local cycle = tonumber(ARGV[3])

local res = redis.pcall('get',bucket)

if (res == false) then
    -- bucket不存在，初始化为capacity - 1
    local str = string.format("%d %d",cap - 1,time)  
    return redis.pcall('set',bucket,str)
end

local arr_str = mysplit(res, " ")

local num = tonumber(arr_str[1])
local timestamp = tonumber(arr_str[2])


-- 根据时间戳来计算num，这里cycle秒一个token
local append = math.floor((time - timestamp) / cycle)
num = num + append
-- 不超过cap个令牌
if num > cap then
    num = cap
end

if (num > 0) then
    -- 拿到token并减一
    local str = string.format("%d %d",num - 1,timestamp + append * cycle)
    return redis.pcall('set', bucket, str)
else 
    -- 没有拿到token
    return false
end

