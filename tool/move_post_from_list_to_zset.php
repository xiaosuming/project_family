<?php
/**
 * 一个将用list保存的timeline，移到zset保存
 * jiangpengfei
 * @date 2017-11-22
 */
$redis = new \Redis();
$redis->connect("116.62.25.128", 6379);
$redis->auth("flare1111");

$redis->select(0);
$list_key_array = $redis->keys("u*");     //获得list的key数组

var_dump($list_key_array);

foreach($list_key_array as $key){
    $post_id_array = $redis->lRange($key,0,-1);    //推文id数组
    $new_zset_key = "pt".substr($key,1);
    echo "----$key => $new_zset_key\n----";

    //获取最小的时间戳
    $one_item_array = $redis->zRange($new_zset_key,0,0,true);
    $min_timestamp = 10000;
    if(count($one_item_array) > 0){
        $min_timestamp = array_pop($one_item_array);
    }
    
    $array_length = count($post_id_array);

    foreach($post_id_array as $postId){
        $timestamp = $min_timestamp - $array_length;
        $array_length--;
        
        $redis->zAdd($new_zset_key,$timestamp,$postId);
    }


    echo "最小时间戳是:".$min_timestamp;

}