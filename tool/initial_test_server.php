<?php
/**
 * 系统执行之前的初始化操作
 * author： 江鹏飞
 * date： 2018-04-16
 */

function initScript() {
    $redis = new Redis();
    $redis->connect("47.111.172.152");
    $redis->auth('flare1111');
    //不存在脚本，load进去
    $script = file_get_contents('check_and_set.lua');
    $sha = $redis->script('load', $script);
    if($redis->getLastError() !== NULL){
        echo "出错了：".$redis->getLastError().'\n';
    }
    echo "初始化的脚本sha:".$sha.PHP_EOL;
}


initScript();
