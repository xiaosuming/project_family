/* 创建存储邮件激活链接，或者短信验证码的表  */
CREATE TABLE `family`.`gener_auth` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth` VARCHAR(500) NOT NULL,
  `create_time` DATETIME NOT NULL,
  `userId` INT NOT NULL,
  `operateId` INT NOT NULL,
  PRIMARY KEY (`id`));


/* 创建调度事件,每分钟检查一次失效的链接或者验证码,删除所有失效的 */
CREATE EVENT sdr ON SCHEDULE EVERY 1 MINUTE DO DELETE FROM gener_auth WHERE DATE_ADD(time,INTERVAL 30 minute) <= now();

/* 注意mysql服务器有没有开启调度器 */
/*
 show variables like '%scheduler%';
*/

/*
 如果事件调度器是关闭的
 set global event_scheduler = 1;
 */

 /* 查看事件的状态
    show events \G;
 */