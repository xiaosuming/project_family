-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: 120.26.103.174    Database: family
-- ------------------------------------------------------
-- Server version	5.5.55-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gener_action`
--

DROP TABLE IF EXISTS `gener_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_action` (
  `action` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` int(11) NOT NULL,
  PRIMARY KEY (`action`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_gener_action_module_idx` (`module`),
  CONSTRAINT `fk_gener_action_module` FOREIGN KEY (`module`) REFERENCES `gener_module` (`module`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_activity`
--

DROP TABLE IF EXISTS `gener_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(10) unsigned NOT NULL,
  `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `deadline` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_activity_2_idx` (`create_by`),
  KEY `fk_gener_activity_1_idx` (`familyId`),
  KEY `fk_gener_activity_3_idx` (`update_by`),
  KEY `index5` (`start_time`),
  KEY `index6` (`deadline`),
  CONSTRAINT `fk_gener_activity_1` FOREIGN KEY (`familyId`) REFERENCES `gener_family` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_activity_2` FOREIGN KEY (`create_by`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_activity_3` FOREIGN KEY (`update_by`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_activity_participator`
--

DROP TABLE IF EXISTS `gener_activity_participator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_activity_participator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activityId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `cancel` int(11) NOT NULL COMMENT '是否取消，0是未取消，1是取消',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_auth`
--

DROP TABLE IF EXISTS `gener_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auth` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  `operateId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_collection`
--

DROP TABLE IF EXISTS `gener_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_collection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `moduleId` int(11) NOT NULL,
  `recordId` int(11) NOT NULL,
  `record_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `record_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_info1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_info2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_delete` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=317 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_eventEditHistory`
--

DROP TABLE IF EXISTS `gener_eventEditHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_eventEditHistory` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `eventId` int(30) NOT NULL,
  `exp_time` date NOT NULL,
  `exp_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `exp_content` text CHARACTER SET utf8mb4 NOT NULL,
  `edit_type` int(10) NOT NULL,
  `writer` int(30) NOT NULL,
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_eventEditHistory_1_idx` (`eventId`),
  KEY `fk_gener_eventEditHistory_writer_idx` (`writer`),
  CONSTRAINT `fk_gener_eventEditHistory_1` FOREIGN KEY (`eventId`) REFERENCES `gener_significantEvent` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_eventEditHistory_writer` FOREIGN KEY (`writer`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=333 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family`
--

DROP TABLE IF EXISTS `gener_family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family` (
  `id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `min_level` int(10) NOT NULL,
  `max_level` int(10) NOT NULL,
  `permission` int(10) NOT NULL,
  `originator` int(30) NOT NULL,
  `member_count` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity` varchar(100) CHARACTER SET utf8 NOT NULL,
  `qrcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_testFamily_originator_idx` (`originator`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_familyAdmin`
--

DROP TABLE IF EXISTS `gener_familyAdmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_familyAdmin` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `familyId` int(30) unsigned NOT NULL,
  `adminId` int(30) NOT NULL,
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_familyAdmin_1_idx` (`familyId`),
  KEY `fk_gener_familyAdmin_admin_idx` (`adminId`),
  KEY `fk_gener_familyAdmin_user_idx` (`create_by`),
  KEY `fk_gener_familyAdmin_updateBy_idx` (`update_by`),
  CONSTRAINT `fk_gener_familyAdmin_admin` FOREIGN KEY (`adminId`) REFERENCES `gener_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_familyAdmin_createBy` FOREIGN KEY (`create_by`) REFERENCES `gener_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_familyAdmin_family` FOREIGN KEY (`familyId`) REFERENCES `gener_family` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_familyAdmin_updateBy` FOREIGN KEY (`update_by`) REFERENCES `gener_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_familyNews`
--

DROP TABLE IF EXISTS `gener_familyNews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_familyNews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(30) unsigned NOT NULL,
  `module` int(6) NOT NULL,
  `action` int(6) NOT NULL,
  `operatorId` int(30) NOT NULL,
  `thingId` int(30) NOT NULL COMMENT '存储的是被操作的事物的id',
  `thing_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `extra_info1` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '额外信息1，例如某人为某人增加了照片，可以在这里存放照片的路径',
  `extra_info2` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '额外信息2',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_familyNews_familyId_idx` (`familyId`),
  KEY `fk_gener_familyNews_module_idx` (`module`),
  KEY `fk_gener_familyNews_action_idx` (`action`),
  CONSTRAINT `fk_gener_familyNews_action` FOREIGN KEY (`action`) REFERENCES `gener_action` (`action`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_familyNews_family` FOREIGN KEY (`familyId`) REFERENCES `gener_family` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_familyNews_module` FOREIGN KEY (`module`) REFERENCES `gener_module` (`module`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2390 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_heirloom`
--

DROP TABLE IF EXISTS `gener_family_heirloom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_heirloom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_legend`
--

DROP TABLE IF EXISTS `gener_family_legend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_legend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_user`
--

DROP TABLE IF EXISTS `gener_family_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_user` (
  `id` int(13) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(13) unsigned NOT NULL,
  `userId` int(13) unsigned NOT NULL,
  `is_delete` int(1) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_feedback`
--

DROP TABLE IF EXISTS `gener_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feedback_message` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `handle_status` int(11) NOT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `problem_type` int(11) NOT NULL,
  `photo` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_by` char(255) CHARACTER SET latin1 NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` char(255) CHARACTER SET latin1 NOT NULL,
  `update_time` datetime NOT NULL,
  `sender` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_state` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_feedback_userId_idx` (`userId`),
  CONSTRAINT `fk_gener_feedback_userId` FOREIGN KEY (`userId`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_file`
--

DROP TABLE IF EXISTS `gener_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) CHARACTER SET utf8 NOT NULL,
  `module` int(11) NOT NULL,
  `create_by` int(8) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file_path_UNIQUE` (`file_path`)
) ENGINE=InnoDB AUTO_INCREMENT=1891 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_grave`
--

DROP TABLE IF EXISTS `gener_grave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_grave` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `personId` int(8) unsigned NOT NULL,
  `person_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(11) unsigned NOT NULL,
  `coor_x` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coor_y` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_grave_person_idx` (`personId`),
  KEY `fk_gener_grave_family_idx` (`familyId`),
  CONSTRAINT `fk_gener_grave_family` FOREIGN KEY (`familyId`) REFERENCES `gener_family` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_grave_person` FOREIGN KEY (`personId`) REFERENCES `gener_person` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_invitation_code`
--

DROP TABLE IF EXISTS `gener_invitation_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_invitation_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invitation_code` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invitation_code_UNIQUE` (`invitation_code`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_job`
--

DROP TABLE IF EXISTS `gener_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_job` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personId` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL,
  `company` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` date NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` date NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_like`
--

DROP TABLE IF EXISTS `gener_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_like` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `userId` int(30) NOT NULL,
  `status` int(1) NOT NULL,
  `postId` int(30) NOT NULL,
  `module` int(30) NOT NULL,
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_like_user_idx` (`userId`),
  CONSTRAINT `fk_gener_like_user` FOREIGN KEY (`userId`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_loginHistory`
--

DROP TABLE IF EXISTS `gener_loginHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_loginHistory` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `device` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `device_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(30) NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_logout` int(1) NOT NULL DEFAULT '0',
  `login_ip` varchar(56) CHARACTER SET utf8 NOT NULL,
  `login_time` datetime NOT NULL,
  `success` int(1) NOT NULL COMMENT '1代表成功，0代表失败',
  PRIMARY KEY (`id`),
  KEY `fk_gener_loginHistory_user_idx` (`userId`),
  CONSTRAINT `fk_gener_loginHistory_user` FOREIGN KEY (`userId`) REFERENCES `gener_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7863 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_message`
--

DROP TABLE IF EXISTS `gener_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_message` (
  `id` bigint(8) unsigned NOT NULL AUTO_INCREMENT,
  `message_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_user` int(8) NOT NULL,
  `to_user` int(8) NOT NULL,
  `content` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `module` int(3) NOT NULL,
  `action` int(11) NOT NULL DEFAULT '0' COMMENT '当一个模块中有几个消息动作时，可以使用这个字段来区分',
  `recordId` int(8) NOT NULL,
  `isDelete` int(1) NOT NULL DEFAULT '0',
  `isRead` int(1) NOT NULL DEFAULT '0',
  `isAccept` int(11) NOT NULL DEFAULT '0' COMMENT '这个字段在大多数情况下没有作用，在家族邀请成员时，这个字段用来表示是否接受,0代表初始值，1代表接受，-1代表拒绝',
  `create_by` int(8) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(8) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_message_user_idx` (`from_user`),
  KEY `fk_gener_message_toUser_idx` (`to_user`),
  CONSTRAINT `fk_gener_message_toUser` FOREIGN KEY (`to_user`) REFERENCES `gener_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_message_user` FOREIGN KEY (`from_user`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=772 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_module`
--

DROP TABLE IF EXISTS `gener_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_module` (
  `module` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`module`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_person`
--

DROP TABLE IF EXISTS `gener_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_person` (
  `id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(30) unsigned NOT NULL,
  `userId` int(30) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '人物类型，1是代表本家族的人，则可以向外延伸，2是家族成员的配偶，不可以向外添加父母，兄弟姐妹，配偶等等',
  `is_delete` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `birthday` datetime NOT NULL,
  `gender` int(10) NOT NULL COMMENT '0是女性，1是男性',
  `photo` varchar(250) CHARACTER SET utf8 NOT NULL,
  `share_url` varchar(250) CHARACTER SET utf8 NOT NULL,
  `ref_familyId` int(30) unsigned DEFAULT NULL,
  `father` varchar(1000) CHARACTER SET utf8 DEFAULT '',
  `mother` varchar(1000) CHARACTER SET utf8 DEFAULT '',
  `son` varchar(1000) CHARACTER SET utf8 DEFAULT '',
  `daughter` varchar(1000) CHARACTER SET utf8 DEFAULT '',
  `spouse` varchar(1000) CHARACTER SET utf8 DEFAULT '',
  `sister` varchar(1000) CHARACTER SET utf8 DEFAULT '',
  `brother` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `level` int(30) NOT NULL,
  `confirm` int(1) NOT NULL COMMENT 'confirm表示这条记录有没有被管理员确认',
  `blood_type` int(11) DEFAULT NULL,
  `marital_status` int(11) DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qq` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qrcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_testPerson_family_idx` (`familyId`),
  KEY `fk_gener_testPerson_refFamily_idx` (`ref_familyId`),
  KEY `fk_gener_testPerson_user_idx` (`userId`),
  KEY `fk_gener_testPerson_is__delete` (`is_delete`),
  CONSTRAINT `fk_gener_testPerson_family` FOREIGN KEY (`familyId`) REFERENCES `gener_family` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_testPerson_refFamily` FOREIGN KEY (`ref_familyId`) REFERENCES `gener_family` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=741 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_personDetail`
--

DROP TABLE IF EXISTS `gener_personDetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_personDetail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `personId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `info` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT 'type代表Info保存的信息,比如1代表技能skill,2代表爱好favorite,',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personId` (`personId`,`info`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_significantEvent`
--

DROP TABLE IF EXISTS `gener_significantEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_significantEvent` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `personId` int(30) unsigned NOT NULL,
  `userId` int(30) NOT NULL,
  `iconograph` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(30) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '事件类型，1是大事件，2是秘密事件',
  `open_time` date DEFAULT NULL COMMENT '如果是秘密事件，则有一个公开时间的属性',
  `exp_time` date NOT NULL,
  `exp_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `exp_content` text CHARACTER SET utf8mb4 NOT NULL,
  `abstract` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `write_mode` int(10) NOT NULL,
  `participate_num` int(11) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `create_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_significantEvent_person_idx` (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_timeline`
--

DROP TABLE IF EXISTS `gener_timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refId` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `create_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user`
--

DROP TABLE IF EXISTS `gener_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `nickname` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `openid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户绑定微信后会有openid字段',
  `qq_openid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wb_openid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hx_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hx_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gender` int(2) NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `country` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `address_UNIQUE` (`address`)
) ENGINE=InnoDB AUTO_INCREMENT=376 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_profile`
--

DROP TABLE IF EXISTS `gener_user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `post_num` int(10) unsigned NOT NULL,
  `collection_num` int(10) unsigned NOT NULL,
  `family_num` int(10) unsigned NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(10) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=555 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_zone`
--

DROP TABLE IF EXISTS `gener_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_zone` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `userId` int(30) NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `is_delete` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_zone_userId_idx` (`userId`),
  CONSTRAINT `fk_gener_zone_userId` FOREIGN KEY (`userId`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_zoneComment`
--

DROP TABLE IF EXISTS `gener_zoneComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_zoneComment` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `postId` int(30) NOT NULL,
  `userId` int(30) NOT NULL,
  `replyto` int(11) NOT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_zoneComment_post_idx` (`postId`),
  KEY `fk_gener_zoneComment_user_idx` (`userId`),
  CONSTRAINT `fk_gener_zoneComment_post` FOREIGN KEY (`postId`) REFERENCES `gener_zonePost` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_zoneComment_user` FOREIGN KEY (`userId`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_zonePost`
--

DROP TABLE IF EXISTS `gener_zonePost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_zonePost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zoneId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `address` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `comments` int(30) NOT NULL DEFAULT '0',
  `likes` int(30) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1是代表自己发送的,2是代表系统生成',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_zonePost_zone_idx` (`zoneId`),
  KEY `fk_gener_zonePost_userId_idx` (`userId`),
  CONSTRAINT `fk_gener_zonePost_userId` FOREIGN KEY (`userId`) REFERENCES `gener_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_zonePost_zone` FOREIGN KEY (`zoneId`) REFERENCES `gener_zone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1203 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-29 10:41:14
