#!/usr/bin/env python
# encoding: utf-8

import configparser

def readConfig():
    config = configparser.ConfigParser()
    config.read("config.conf")
    return config




