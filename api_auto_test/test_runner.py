#!/usr/bin/env python
# encoding: utf-8

import sys
import os
import csv
import json
import ast
import requests
import markdown
import codecs
import time
import config_parser

testcase_dir = './testcase'
result_dir = './result/' + time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
result_file = result_dir + '/result_report.md'
result_html_file = result_dir + '/result_report.html'

template_file = './template/template.html'

# 运行过程中的缓存dict
dict_cache = {}

# 全局的配置文件
config_dict = {}

# 生成markdown报表的头
def dump_markdown_header():
    write_to_markdown('| 接口测试功能 | 接口地址 | 请求类型 | 参数 | 检查的关键字 | 期望值 | 实际值 | 是否通过 |\n')
    write_to_markdown('| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |\n')

# 生成markdown格式的报表
def dump_markdown_report(testcase_name,
                         url,
                         request_type,
                         params,keyword,
                         expect_value,
                         actual_value,
                         is_pass):
    write_to_markdown('| {} | {} | {} | {} | {} | {} | {} | {} |\n'.format(testcase_name.replace("|","\\|"),url.replace("|","\\|"),
                                              request_type.replace("|","\\|"),str(params).replace("|","\\|"),
                                              keyword.replace("|","\\|"),expect_value,
                                              actual_value,is_pass))
# 将内容写入markdown
def write_to_markdown(content):
    if not os.path.isdir(result_dir):
        os.mkdir(result_dir)
    with open(result_file,"a") as f:
        f.write(content)

# 将markdown转换为html
def markdown_to_html():
    with codecs.open(result_file,"r","utf-8") as f:
        text = f.read()
        html = markdown.Markdown(extensions=['markdown.extensions.tables']).convert(text)
        with codecs.open(result_html_file,"w","utf-8",errors="xmlcharrefreplace") as output_html_file:
            with codecs.open(template_file,"r","utf-8",errors="xmlcharrefreplace") as template:
                output_html_file.write((template.read()).format(html))

# 运行测试用例
# url           测试用例的请求地址
# type          请求类型
# params        dict,参数
# keyword       要检查的关键字
# expect_value  关键字的期望值
# export_value  需要导出的值
def run_testcase(url,type,params,keyword,expect_value,testcase_name,export_value):
    # 解析地址
    r = requests.post(config_dict['server']['host'] + url,data = params)

    if r.status_code != 200:
        # 接口出错
        dump_markdown_report(testcase_name,url,type,params,keyword,expect_value,r.status_code,'ERROR REQ')
        return

    result_json = json.loads(r.text)

    # 判断是否有export_value
    if export_value != "":
        # 导出值
        need_value = result_json
        export_arr = export_value.split(">")
        for item in export_arr:
            if isinstance(need_value,dict) and need_value.get(item) != None:
                need_value = need_value[item]
                dict_cache[export_arr[len(export_arr) - 1]] = need_value
                # 导出值写入文件保存
                with open('env.dict', 'w') as env:
                    env.write(str(dict_cache))

    # 解析keyword
    result_value = result_json
    keyword_arr = keyword.split(">")
    for item in keyword_arr:
        result_value = result_value[item]

    is_pass = ''
    if str(result_value).lower() == str(expect_value).lower():
        is_pass = "SUCCESS"
        print('success')
    else:
        is_pass = "**FAIL**"
        print('fail')
    dump_markdown_report(testcase_name,url,type,params,keyword,expect_value,result_value,is_pass)


# 读取测试用例
# testcase_file 测试用例文件
def read_testcase(testcase_file):
    with open(testcase_file) as csvfile:
        spamreader = csv.reader(csvfile,delimiter=',',quotechar='"')
        for i,row in enumerate(spamreader):
            if len(row) < 8:
                print("error test case,too few filed:", testcase_file, " caseId:", i)
                continue
            if i != 0:
                # 0是测试用例的功能，1是请求地址,2是请求类型，3是请求参数，4是检查的key,5是key的期望值,6是导入,7是导出
                if row[3] == '':
                    print("error test case, empty request params:", testcase_file, " caseId:", i)
                    continue
                data_dict = ast.literal_eval(row[3])

                # 解析import_value,export_value
                # 有import_value的话，从dict_cache中导入
                if row[6] != "":
                    import_arr = row[6].split("|")
                    for item in import_arr:
                        item = item.strip()
                        if dict_cache.get(item) != None:
                            # 遍历data_dict，找出值为item的项
                            for item_key in data_dict:
                                if data_dict[item_key] == item:
                                    data_dict[item_key] = dict_cache[item]
                                    print("替换",item_key,data_dict[item_key])
                        else:
                            print("error test case, empty request params:", testcase_file, " caseId:", i)
                            continue

                run_testcase(row[1],row[2],data_dict,row[4],row[5],row[0],row[7])



# 创建一些需要的文件夹
if not os.path.exists('env.dict'):
    os.mknod('env.dict')

if not os.path.exists('result'):
    os.mkdir('result')

# 先从文件中读取dict_cache, 这个文件是存储了一些环境
with open('env.dict') as env:
    text = env.read()
    if text != '':
        dict_cache = eval(text)

#首先生成markdown表格的头部
dump_markdown_header()

# 读取配置文件
config_dict = config_parser.readConfig()

# 读取索引文件
with open(testcase_dir + '/index.conf','r') as f:
    for line in f.readlines():
        if line.strip() != '' and line[0] != '#':
            read_testcase(testcase_dir + '/' + line.strip())


markdown_to_html()
# 将最新的测试结果目录记录下来
with open('last_result_dir.log','w') as f:
    f.write(result_dir+'\n')
