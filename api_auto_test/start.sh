#!/bin/bash

#step 1,初始化测试数据库
if [ $1 ]; then
    if [ $1 == "dep" ];then
        echo "重新部署数据库"
        sh init_db.sh
    fi
fi

#step 2,执行python测试脚本
python3 test_runner.py

#step 3,启动http服务
cat last_result_dir.log | while read line; do
    result_dir=$line
    # cp result/style.css $result_dir
    http-server $result_dir -o
done