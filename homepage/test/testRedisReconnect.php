<?php
    
    $redis = new \Redis();
    $redis->pconnect("127.0.0.1", 6379);
    
    $count = 0;

    while(true){
        try{
            $result = $redis->keys("*");
            echo "第{$count}次循环\n";
            $count++;
            var_dump($result);
        }catch(RedisException $e){
            if($e->getMessage() == "Connection lost"){
                //尝试重连
                $redis->pconnect("127.0.0.1", 6379);
            }
        }

        sleep(2);
    }