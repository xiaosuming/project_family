<?php
require_once("../php/vendor/autoload.php");

use Util\TextClassification;

$text = '吴克羣
天 R 明 项 目 经 理
LS
上 海 明 月 建筑 装饰 工程 有 限 公司
地 址 : 张扬 北 路 5982 号 ( 近 草 高 支 路 )
电话 : 021-68865226
手机 : 15921313905
邮箱 : mingyuesheji@126.com 网址 : www.sh-mingyue.com
';

$textC = new TextClassification($text, 0);
$textC->analysis();

echo "姓名:\n";
var_dump($textC->name);

echo "电话:\n";
var_dump($textC->phone);

echo "公司:\n";
var_dump($textC->company);

echo "邮箱:\n";
var_dump($textC->email);

echo "网址:\n";
var_dump($textC->companyUrl);

echo "地址:\n";
var_dump($textC->address);

echo "职位:\n";
var_dump($textC->position);


$text = '{"log_id":3788174912046644357,"direction":0,"words_result_num":10,"words_result":[{"words":"刘强","probability":{"variance":1.5e-5,"average":0.996021,"min":0.992098}},{"words":"国际事业部副总经理","probability":{"variance":0.000738,"average":0.985937,"min":0.91071}},{"words":"为的為一各猛侈负","probability":{"variance":0.041214,"average":0.764025,"min":0.456282}},{"words":"17321342252","probability":{"variance":5.0e-6,"average":0.998738,"min":0.992318}},{"words":"2880090338","probability":{"variance":1.0e-6,"average":0.999065,"min":0.996816}},{"words":"LQ2013529","probability":{"variance":0,"average":0.999509,"min":0.997791}},{"words":"Oliug@253.com","probability":{"variance":0.002418,"average":0.951119,"min":0.8683}},{"words":"www.253.com","probability":{"variance":0,"average":1,"min":1}},{"words":"上海创蓝文化传播有限公司","probability":{"variance":0.00024,"average":0.993317,"min":0.942702}},{"words":"上海市松江区中自的路68号启词浸19创大题","probability":{"variance":0.025225,"average":0.895647,"min":0.50615}}],"language":3}';

$textC = new TextClassification($text, 1);
$textC->analysis();

echo "姓名:\n";
var_dump($textC->name);

echo "电话:\n";
var_dump($textC->phone);

echo "公司:\n";
var_dump($textC->company);

echo "邮箱:\n";
var_dump($textC->email);

echo "网址:\n";
var_dump($textC->companyUrl);

echo "地址:\n";
var_dump($textC->address);

echo "职位:\n";
var_dump($textC->position);