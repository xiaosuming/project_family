<?php
/**
 * 用户信息名片
 * @author:jiangpengfei
 * @date:2017-12-01
 */

namespace Model;

class InfoCard
{
    public $id;                     //主键
    public $remark;                 //名片备注
    public $userId;                 //用户id
    public $familyId;               //家族id
    public $personId;               //人物id
    public $socialCircleId;         //圈子id
    public $name;                   //名字
    public $country;                //国家
    public $countryName;            //国家名
    public $province;               //省份
    public $provinceName;           //省份名
    public $city;                   //城市
    public $cityName;               //城市名
    public $area;                   //区
    public $areaName;               //区名
    public $town;                   //镇
    public $townName;               //镇名
    public $address;                //地址
    public $photo;                  //头像
    public $backgroundImg;          //背景图
    public $backgroundColor;        //背景色
    public $backgroundType;         //背景类型
    public $company;                //公司
    public $gender;                 //性别,0女,1男
    public $birthday;               //生日
    public $bloodType;              //血型
    public $maritalStatus;          //婚姻状态
    public $phone;                  //手机号
    public $qq;                     //qq
    public $skills;                 //技能数组
    public $favorites;              //爱好数组
    public $jobs;                   //工作经历
    public $type;                   //名片类型, 0是用户创建,1是通讯录导入,2是家族导入,3是针对于搜索新加的类型,代表收到的名片,4ocr名片
    public $identity;
    public $companyEn;              //公司的英文名
    public $nameEn;                 //英文名字
    public $email;                  //邮箱
    public $companyProfile;         //公司简介
    public $companyUrl;             //公司网址
    public $bankAccount;            //银行账号
    public $longitude;              //经度
    public $latitude;               //纬度
    public $position;               //工作
    public $permission;             //权限判断，标记名片能否被转发 0不允许，1允许。
    public $openScope;              //名片公开范围  0私密 1公开 面向所有用户 2公开 面向所有家族 3 公开 面向部分家族
    public $ocrCardImg;             //ocr名片的地址
    public $taxNumber;              //税号
    public $viewNum;                //浏览量
    public $shareNum;               //转发量
    public $imageOrder;             //微主页图片排序数组
    public $isDefault;              //是否是默认名片
    public $chatPermission;         // 聊天权限, 1是公开，2是付费，3是禁止
    public $chatCost;               // 聊天费用
    public $createTime;             //创建时间
    public $createBy;               //创建者
    public $updateTime;             //更新时间
    public $updateBy;               //更新者


    public function __construct($queryResult = array())
    {
        $this->id = $queryResult['id'] ?? '';
        $this->remark = $queryResult['remark'] ?? '';
        $this->userId = $queryResult['userId'] ?? '';
        $this->familyId = $queryResult['familyId'] ?? '';
        $this->personId = $queryResult['personId'] ?? '';
        $this->socialCircleId = $queryResult['socialCircleId'] ?? '';
        $this->name = $queryResult['name'] ?? '';
        $this->country = $queryResult['country'] ?? '';
        $this->countryName = $queryResult['countryName'] ?? '';
        $this->province = $queryResult['province'] ?? '';
        $this->provinceName = $queryResult['provinceName'] ?? '';
        $this->city = $queryResult['city'] ?? '';
        $this->cityName = $queryResult['cityName'] ?? '';
        $this->area = $queryResult['area'] ?? '';
        $this->areaName = $queryResult['areaName'] ?? '';
        $this->town = $queryResult['town'] ?? '';
        $this->townName = $queryResult['townName'] ?? '';
        $this->address = $queryResult['address'] ?? '';
        $this->gender = $queryResult['gender'] ?? '';
        $this->birthday = $queryResult['birthday'] ?? '';
        $this->bloodType = $queryResult['bloodType'] ?? '';
        $this->maritalStatus = $queryResult['maritalStatus'] ?? '';
        $this->phone = json_decode($queryResult['phone'] ?? json_encode(array()), true);
        $this->qq = $queryResult['qq'] ?? '';
        $this->skills = json_decode($queryResult['skills'] ?? json_encode(array()), true);
        $this->favorites = json_decode($queryResult['favorites'] ?? json_encode(array()), true);
        $this->jobs = json_decode($queryResult['jobs'] ?? json_encode(array()), true);
        $this->identity = $queryResult['identity'] ?? '';
        $this->type = $queryResult['type'] ?? '';
        $this->photo = $queryResult['photo'] ?? '';
        $this->backgroundImg = $queryResult['backgroundImg'] ?? '';
        $this->backgroundColor = $queryResult['backgroundColor'] ?? '';
        $this->backgroundType = $queryResult['backgroundType'] ?? '';
        $this->company = $queryResult['company'] ?? '';
        $this->companyEn = $queryResult['companyEn'] ?? '';
        $this->nameEn = $queryResult['nameEn'] ?? '';
        $this->email = $queryResult['email'] ?? '';
        $this->companyProfile = $queryResult['companyProfile'] ?? '';
        $this->companyUrl = $queryResult['companyUrl'] ?? '';
        $this->bankAccount = $queryResult['bankAccount'] ?? '';
        $this->longitude = $queryResult['longitude'] ?? '';
        $this->latitude = $queryResult['latitude'] ?? '';
        $this->position = $queryResult['position'] ?? '';
        $this->permission = $queryResult['permission'] ?? '';
        $this->openScope = $queryResult['openScope'] ?? '';
        $this->ocrCardImg = $queryResult['ocrCardImg'] ?? '';
        $this->taxNumber = $queryResult['taxNumber'] ?? '';
        $this->shareNum = $queryResult['shareNum'] ?? '';
        $this->viewNum = $queryResult['viewNum'] ?? '';
        $this->imageOrder = json_decode($queryResult['imageOrder'] ?? json_encode(array()), true);
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
        $this->updateBy = $queryResult['updateBy'] ?? '';
        $this->isDefault = $queryResult['isDefault'] ?? '';
        $this->chatPermission = $queryResult['chatPermission'] ?? '1';
        $this->chatCost = $queryResult['chatCost'] ?? '0';
    }
}
