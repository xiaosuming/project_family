<?php
    /**
     * 家族传说人物类
     * @author Jiayao Li
     * @date 2017-11-27
     */
    namespace Model;

    use Util\Util;

    class Celebrity{
        public $id;				  //家族传说人物ID
        public $familyId;		  //家族ID
        public $personId;         //人物ID
        public $personName;       //人物名称
        public $styleName;        //字
        public $pseudonym;		  //号
        public $yearOfBirth;	  //出生年份
        public $yearOfPass;		  //去世年份
        public $profile;          //简介
        public $photo;            //粘片
        public $createTime;       //创建时间
        public $createBy;         //创建人
        public $updateTime;       //更新时间
        public $updateBy;         //更新人

        public function __construct($queryResult = array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
            $this->personId = isset($queryResult['personId']) ? $queryResult['personId'] : '';
            $this->personName = isset($queryResult['personName']) ? $queryResult['personName'] : '';
            $this->styleName = isset($queryResult['styleName']) ? $queryResult['styleName'] : '';
            $this->pseudonym = isset($queryResult['pseudonym']) ? $queryResult['pseudonym'] : '';
            $this->yearOfBirth = isset($queryResult['yearOfBirth']) ? $queryResult['yearOfBirth'] : '';
            $this->yearOfPass = isset($queryResult['yearOfPass']) ? $queryResult['yearOfPass'] : '';
            $this->profile = isset($queryResult['profile']) ? $queryResult['profile'] : '';
            $this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>