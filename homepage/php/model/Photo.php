<?php
/**
 * Created by PhpStorm.
 * User: ICDI
 * Date: 2017/11/14
 * Time: 15:34
 */

namespace Model;


class Photo
{
	public $id;						//照片id
	public $albumId;				//相册id
	public $photo;              	//照片url
	public $description;            //相册描述
	public $country;
	public $countryName;
	public $province;
	public $provinceName;
	public $city;
	public $cityName;
	public $area;
	public $areaName;
	public $town;
	public $townName;
	public $size;               	//照片大小
	public $address;                //地址
	public $createTime;             //创建时间
	public $createBy;               //创建者
	public function __construct($queryResult){
		$this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
		$this->albumId = isset($queryResult['albumId']) ? $queryResult['albumId'] : '';
		$this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
		$this->description = isset($queryResult['description']) ? $queryResult['description']:'';
		$this->country = isset($queryResult['country']) ? $queryResult['country']:'';
		$this->countryName = isset($queryResult['countryName']) ? $queryResult['countryName'] :'';
		$this->province = isset($queryResult['province']) ? $queryResult['province']:'';
		$this->provinceName = isset($queryResult['provinceName']) ? $queryResult['provinceName'] :'';
		$this->city = isset($queryResult['city']) ? $queryResult['city']: '';
		$this->cityName = isset($queryResult['cityName']) ? $queryResult['cityName'] :'';
		$this->area = isset($queryResult['area']) ? $queryResult['area']:'';
		$this->areaName = isset($queryResult['areaName']) ? $queryResult['areaName'] :'';
		$this->town = isset($queryResult['town']) ? $queryResult['town']: '';
		$this->townName = isset($queryResult['townName']) ? $queryResult['townName'] :'';
		$this->size = isset($queryResult['size']) ? $queryResult['size']:'';
		$this->address = isset($queryResult['address']) ? $queryResult['address'] :'';
		$this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
		$this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
	}
}