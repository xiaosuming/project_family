<?php
    /**
     * 家族传说类
     * @author jiangpengfei
     * @date 2017-04-21
     */
    namespace Model;
    class FamilyLegend{
        public $id;						//家族传说id
        public $familyId;				//家族id
        public $title;				    //传说的标题
        public $content;				//传说的内容
        public $createTime;				//创建时间
        public $createBy;				//创建人
        public $updateTime;				//更新时间
        public $updateBy;				//更新人
        
        public function __construct($queryResult){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
            $this->title = isset($queryResult['title']) ? $queryResult['title'] : '';
            $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>