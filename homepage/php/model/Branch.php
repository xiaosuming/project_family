<?php
    /**
     * 家族分支类
     * @author jiangpengfei
     * @date 2017-10-19
     */
    namespace Model;

    use Util\Util;

    class Branch{
        public $id;						//活动id
        public $branchName;				//分支名
        public $branchPersonId;         //分支合并点人物id
        public $type;                   //分支类型，1是主分支，2是子分支
        public $createTime;				//创建时间
        public $createBy;				//创建人
        public $updateTime;				//更新时间
        public $updateBy;				//更新人
        
        public function __construct($queryResult = array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->branchName = isset($queryResult['branchName']) ? $queryResult['branchName'] : '';
            $this->branchPersonId = isset($queryResult['branchPersonId']) ? $queryResult['branchPersonId'] : '';
            $this->type = isset($queryResult['type']) ? $queryResult['type'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>