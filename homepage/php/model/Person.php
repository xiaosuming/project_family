<?php

namespace Model;

use Util\Util;

class Person
{
    public $id;
    public $name;
    public $zpname;
    public $hxUsername; //环信用户名
    public $type;   //人物类型，1是本家族人物，2是本家族人物的配偶
    public $level;
    public $address;
    public $birthday;
    public $gender;
    public $photo;
    public $phone;
    public $bloodType;      //血型
    public $maritalStatus;
    public $qq;
    public $shareUrl;
    public $familyId;
    public $familyName;
    public $groupType;      // 群组类型
    public $familyPhoto;
    public $refFamilyId;
    public $refPersonId;
    public $userId;
    public $father;
    public $mother;
    public $brother;
    public $sister;
    public $spouse;
    public $son;
    public $daughter;
    public $country;
    public $city;
    public $province;
    public $area;
    public $town;
    public $relation;
    public $corePersonId;
    public $branchId;

    public $profileText;        // 详细的输入数据
    public $sideText;           // 侧边栏文本

    public $countryName;
    public $cityName;
    public $provinceName;
    public $areaName;
    public $townName;
    public $createBy;
    public $createTime;
    public $updateBy;
    public $updateTime;
    public $zi;
    public $remark;
    public $isDead;
    public $isAdoption;
    public $deadTime;
    public $samePersonId;       // 标记了过继关系
    public $ranking;            // 兄弟姐妹间的排行
    public $isDelete;
    
    public $familyIndex;   //人物家族内顺序，即坐标
    public $bicolor;   //兼祧
    public $family_index;
    public $birthPlace; // 出生地
    public $burialPlace; // 安厝地

    public function __construct($queryResult = array())
    {
        $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
        $this->name = isset($queryResult['name']) ? $queryResult['name'] : '';
        $this->zpname = isset($queryResult['zpname']) ? $queryResult['zpname'] : '';
        $this->hxUsername = isset($queryResult['hxUsername']) ? $queryResult['hxUsername'] : '';
        $this->type = isset($queryResult['type']) ? $queryResult['type'] : '';
        $this->level = isset($queryResult['level']) ? $queryResult['level'] : '';
        $this->address = isset($queryResult['address']) ? $queryResult['address'] : '';
        $this->birthday = Util::dateFormat(isset($queryResult['birthday']) ? $queryResult['birthday'] : '');
        $this->gender = isset($queryResult['gender']) ? $queryResult['gender'] : '';
        $this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
        $this->shareUrl = isset($queryResult['shareUrl']) ? $queryResult['shareUrl'] : '';
        $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
        $this->groupType = isset($queryResult['groupType']) ? $queryResult['groupType'] : '';
        $this->familyName = isset($queryResult['familyName']) ? $queryResult['familyName'] : '';
        $this->familyPhoto = isset($queryResult['familyPhoto']) ? $queryResult['familyPhoto'] : '';
        $this->refFamilyId = json_decode($queryResult['refFamilyId'] ?? '[]', true);
        $this->refPersonId = json_decode($queryResult['refPersonId'] ?? '[]', true);
        $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : 0;
        $this->father = isset($queryResult['father']) ? $queryResult['father'] : '';
        $this->mother = isset($queryResult['mother']) ? $queryResult['mother'] : '';
        $this->brother = isset($queryResult['brother']) ? $queryResult['brother'] : '';
        $this->sister = isset($queryResult['sister']) ? $queryResult['sister'] : '';
        $this->spouse = isset($queryResult['spouse']) ? $queryResult['spouse'] : '';
        $this->son = isset($queryResult['son']) ? $queryResult['son'] : '';
        $this->daughter = isset($queryResult['daughter']) ? $queryResult['daughter'] : '';
        $this->phone = isset($queryResult['phone']) ? $queryResult['phone'] : '';
        $this->bloodType = isset($queryResult['bloodType']) ? $queryResult['bloodType'] : 0;
        $this->maritalStatus = isset($queryResult['maritalStatus']) ? $queryResult['maritalStatus'] : 0;
        $this->qq = isset($queryResult['qq']) ? $queryResult['qq'] : '';
        $this->country = isset($queryResult['country']) ? $queryResult['country'] : '0';
        $this->area = isset($queryResult['area']) ? $queryResult['area'] : '0';
        $this->town = isset($queryResult['town']) ? $queryResult['town'] : '0';
        $this->province = isset($queryResult['province']) ? $queryResult['province'] : '0';
        $this->city = isset($queryResult['city']) ? $queryResult['city'] : '0';
        $this->relation = isset($queryResult['relation']) ? $queryResult['relation'] : '';
        $this->corePersonId = isset($queryResult['corePersonId']) ? $queryResult['corePersonId'] : '';
        $this->branchId = isset($queryResult['branchId']) ? $queryResult['branchId'] : 0;
        $this->countryName = isset($queryResult['countryName']) ? $queryResult['countryName'] : '';
        $this->provinceName = isset($queryResult['provinceName']) ? $queryResult['provinceName'] : '';
        $this->cityName = isset($queryResult['cityName']) ? $queryResult['cityName'] : '';
        $this->areaName = isset($queryResult['areaName']) ? $queryResult['areaName'] : '';
        $this->townName = isset($queryResult['townName']) ? $queryResult['townName'] : '';
        $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
        $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
        $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
        $this->zi = isset($queryResult['zi']) ? $queryResult['zi'] : '';
        $this->remark = isset($queryResult['remark']) ? $queryResult['remark'] : '';
        $this->isDead = isset($queryResult['isDead']) ? $queryResult['isDead'] : 0;
        $this->isAdoption = isset($queryResult['isAdoption']) ? $queryResult['isAdoption'] : 0;
        $this->deadTime = Util::dateFormat(isset($queryResult['deadTime']) ? $queryResult['deadTime'] : '');
        $this->samePersonId = isset($queryResult['samePersonId']) ? $queryResult['samePersonId'] : 0;
        $this->ranking = $queryResult['ranking'] ?? '0';
        $this->isDelete = $queryResult['isDelete'] ?? '';

        $this->profileText = $queryResult['profileText'] ?? '';
        $this->sideText = $queryResult['sideText'] ?? '';

        $this->birthPlace = $queryResult['birthPlace'] ?? '';
        $this->burialPlace = $queryResult['burialPlace'] ?? '';

        //add for copy 
        $this->infoCardId = $queryResult['infoCardId'] ?? '';
        $this->confirm = $queryResult['confirm'] ?? '';
        $this->qrcode = $queryResult['qrcode'] ?? '';

        $this->familyIndex = $queryResult['familyIndex'] ?? '';
        $this->isNotMain= $queryResult['isNotMain'] ?? 0;
        $this->family_index = $queryResult['family_index'] ?? '';
        $this->bicolor = $queryResult['bicolor'] ?? '0';
        $this->birthPlace = isset($queryResult['birthPlace']) ? $queryResult['birthPlace'] : '';
        $this->burialPlace = isset($queryResult['burialPlace']) ? $queryResult['burialPlace'] : '';
        
    }

}

?>
