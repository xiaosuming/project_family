<?php
/**
 * 名片私信聊天订单
 * @author: jiangpengfei
 * @date:   2019-03-19
 */

namespace Model;

class InfoCardChatOrder extends BaseModel {
    public $id;
    public $userId;         // 用户id
    public $infoCardId;     // 名片id
    public $content;        // 私信内容
    public $orderId;        // 订单id
    public $isDelete;       // 删除标记
    public $createBy;       // 创建人
    public $createTime;     // 创建时间
    public $updateBy;       // 更新人
    public $updateTime;     // 更新时间

    public function __construct($queryResult = []) {
        $this->autoSet($queryResult);
    }
}