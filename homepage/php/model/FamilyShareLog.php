<?php
/**
 * 家族分享记录类
 * @author: jiangpengfei
 * @date: 2018-02-02
 */
namespace Model;

class FamilyShareLog{
    public $id;
    public $shareId;
    public $ip;
    public $times;
    public $createTime;
    public $updateTime;

    public function __construct($queryResult = array()){
        $this->id = $queryResult['id'] ?? '';
        $this->shareId = $queryResult['shareId'] ?? '';
        $this->ip = $queryResult['ip'] ?? '';
        $this->times = $queryResult['times'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
    }
}