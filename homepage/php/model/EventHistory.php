<?php
    /**
     * 大事件历史类
     * @author jiangpengfei
     * @date 2016-12-10
     */
    namespace Model;
    class EventHistory{
        public $id;						//历史id
        public $eventId;				//事件id
        public $expTime;				//经历的事件时间
        public $expName;				//事件姓名
        public $expContent;				//事件内容
        public $editType;               //大事件类型 1是公开 2是秘密
        public $writer;                 //作者
        public $createTime;				//创建时间
        public $createBy;				//创建人
        public $updateTime;				//更新时间
        public $updateBy;				//更新人
        
        public function __construct($queryResult){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->eventId = isset($queryResult['eventId']) ? $queryResult['eventId'] : '';
            $this->editType = isset($queryResult['editType']) ? $queryResult['editType'] : '';
            $this->expTime = isset($queryResult['expTime']) ? $queryResult['expTime'] : '';
            $this->expName = isset($queryResult['expName']) ? $queryResult['expName'] : '';
            $this->expContent = isset($queryResult['expContent']) ? $queryResult['expContent'] : '';
            $this->writer = isset($queryResult['writer']) ? $queryResult['writer'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>