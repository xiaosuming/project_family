<?php
    /**
     * 用户回答类
     */

    namespace Model;
    
    class Answer{
        public $id;					//问题id
        public $userId;				//用户id
        public $questionId;         //问题id
        public $content;			//回答内容
        public $comment_count = 0;	//评论数
        public $like_count = 0;     //点赞数
        public $isBest = 0;         // 是否是最佳答案
        public $createTime;			//创建时间
        public $createBy;			//创建人
        public $updateTime;			//更新时间
        public $updateBy;			//更新人
        
        public function __construct($queryResult = array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
            $this->questionId = isset($queryResult['questionId']) ? $queryResult['questionId'] : '';
            $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
            $this->comment_count = isset($queryResult['comment_count']) ? $queryResult['comment_count'] :0;
            $this->like_count = isset($queryResult['like_count']) ? $queryResult['like_count'] : 0;
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>