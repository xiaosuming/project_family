<?php
/**
 * 家庭绑定推荐
 * @Author: jiangpengfei
 * @Date:   2019-02-27
 */

namespace Model;

class MiniFamilyBindRecommend {
    public $id;
    public $applyId;
    public $familyId;
    public $miniFamilyId;
    public $familyPersonId;
    public $miniFamilyPersonId;
    public $recommendData;
    public $recommendType;      // 1是申请绑定时生成的推荐数据， 2是申请更新时生成的推荐数据
    public $isDelete;
    public $createBy;
    public $createTime;
    public $updateBy;
    public $updateTime;

    public function __construct($queryResult = array())
    {
        $this->id = $queryResult['id'] ?? '';
        $this->applyId = $queryResult['applyId'] ?? '';
        $this->familyId = $queryResult['familyId'] ?? '';
        $this->miniFamilyId = $queryResult['miniFamilyId'] ?? '';
        $this->familyPersonId = $queryResult['familyPersonId'] ?? '';
        $this->miniFamilyPersonId = $queryResult['miniFamilyPersonId'] ?? '';
        $this->recommendData = $queryResult['recommendData'] ?? '';
        $this->recommendType = $queryResult['recommendType'] ?? '';
        $this->isDelete = $queryResult['isDelete'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->updateBy = $queryResult['updateBy'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
    }
}