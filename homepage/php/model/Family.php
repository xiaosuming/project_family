<?php
/**
 * 家族类
 * author 江鹏飞
 * 2016年10月15日20:21:07
 */
     namespace Model;
    Class Family{
        public $id;		    //家族Id
        public $branchId;   //分支id
        public $name;		//家族名
        public $surname;  //姓氏
        public $description;    //家族描述
        public $photo;          //家族照片
        public $qrcode;         //家族二维码
        public $minLevel;	//家族最小的level
        public $maxLevel;	//家族最大的level
        public $startLevel;  //家族起始世代
        public $permission;//家族的访问权限
        public $hxChatRoom; //环信的聊天室
        public $originator;		//家族的管理者
        public $memberCount;	//家族人数
        public $desc;			//家族的简要描述
        public $openType;       // 开放类型
        public $type;           // 分支类型,1是主分支，2是子分支
        public $groupType;      // 群体类型，1是家族，2是家庭，3是同学，4是师徒，5是同事
        public $socialCircleId;  //关联的圈子id
        public $identity;       // 群体的唯一标示码
        public $createTime;//家族的创建时间
        public $createBy;	//家族的创建者
        public $updateTime;//家族的更新时间
        public $updateBy;	//家族的更新者

        public function __construct($queryResult = array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->branchId = isset($queryResult['branchId']) ? $queryResult['branchId'] : '';
            $this->name = isset($queryResult['name']) ? $queryResult['name'] : '';
            $this->surname = isset($queryResult['surname']) ? $queryResult['surname'] : '';
            $this->description = isset($queryResult['description']) ? $queryResult['description'] : '';
            $this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
            $this->qrcode = isset($queryResult['qrcode']) ? $queryResult['qrcode'] : '';
            $this->minLevel = isset($queryResult['minLevel']) ? $queryResult['minLevel'] : '';
            $this->maxLevel = isset($queryResult['maxLevel']) ? $queryResult['maxLevel'] : '';
            $this->startLevel = isset($queryResult['startLevel']) ? $queryResult['startLevel'] : '';
            $this->permission = isset($queryResult['permission']) ? $queryResult['permission'] : '';
            $this->hxChatRoom = isset($queryResult['hxChatRoom']) ? $queryResult['hxChatRoom'] : '';
            $this->memberCount = isset($queryResult['memberCount']) ? $queryResult['memberCount'] : '';
            $this->desc = isset($queryResult['description']) ? $queryResult['description'] : '';
            $this->originator = isset($queryResult['originator']) ? $queryResult['originator'] : '';
            $this->identity = isset($queryResult['identity']) ? $queryResult['identity'] : '';
            $this->groupType = isset($queryResult['groupType']) ? $queryResult['groupType'] : '';
            $this->socialCircleId = isset($queryResult['socialCircleId']) ? $queryResult['socialCircleId'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
            $this->openType = $queryResult['openType'] ?? '';
            $this->type = $queryResult['type'] ?? '';
        }
    }
?>
