<?php
/**
 * 订单类
 * @author: jiangpengfei
 * @date:   2019-03-19
 */

namespace Model;

class Order extends BaseModel {
    public $id;
    public $userId;
    public $status;                 // 订单状态,0是未支付，1是支付待确认，2支付确认成功，3支付确认失败，4是发货，5是订单收货确认，99是取消订单
    public $totalPrice;             // 总价格
    public $realPrice;              // 实际价格
    public $discountStrategyId;     // 折扣优惠策略
    public $userRemark;             // 用户订单备注
    public $failureTime;            // 订单失效时间
    public $province;               // 省份
    public $city;                   // 城市
    public $area;                   // 区镇
    public $town;                   // 乡县
    public $address;                // 具体地址
    public $name;                   // 姓名
    public $phone;                  // 订单手机号
    public $goodsType;              // 订单商品类型, 1是虚拟商品，2是实物商品，3是私信收费
    public $payway;                 // 订单的支付方式
    public $wxPrepayId;             // 微信预支付id
    public $isDelete;               // 是否删除
    public $createBy;               // 创建人
    public $createTime;             // 创建时间
    public $updateBy;               // 更新人
    public $updateTime;             // 更新时间

    public function __construct($queryResult = []) {
        $this->autoSet($queryResult);
    }
}