<?php
/**
 * 名片交换类
 * @Author: jiangpengfei
 * @Date: 2018-10-02
 */
namespace Model;

class InfoCardExchange {
    public $id;                 // 数据库记录id
    public $userId;        // 分享名片的用户id
    public $title;       // 接收名片的用户id
    public $code;        // 分享的名片id
    public $password;           // 密码,经过sha256哈希
    public $isFinish;           // 是否结束
    public $isDelete;           // 是否删除
    public $createTime;         // 记录创建时间,也就是名片被接收的时间
    public $createBy;           // 记录创建者的id
    public $updateTime;         // 更新时间
    public $updateBy;           // 更新人的id

    public function __construct($queryResult = array()) {
        $this->id = $queryResult['id'] ?? '';
        $this->userId = $queryResult['userId'] ?? '';
        $this->title = $queryResult['title'] ?? '';
        $this->code = $queryResult['code'] ?? '';
        $this->password = $queryResult['password'] ?? '';
        $this->isFinish = $queryResult['isFinish'] ?? '';
        $this->isDelete = $queryResult['isDelete'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
        $this->updateBy = $queryResult['updateBy'] ?? '';
    }

}