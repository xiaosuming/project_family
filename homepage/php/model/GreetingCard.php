<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-12-29
 * Time: 下午4:45
 */

namespace Model;

class GreetingCard
{
    public $id;
    public $userId;
    public $infoCardId;
    public $title;
    public $template;
    public $data;
    public $isDelete;
    public $createTime;
    public $createBy;
    public $updateTime;
    public $updateBy;

    public function __construct($queryResult)
    {
        $this->id = $queryResult['id'] ?? '';
        $this->userId = $queryResult['userId'] ?? '';
        $this->infoCardId = $queryResult['infoCardId'] ?? '';
        $this->title = $queryResult['title'] ?? '';
        $this->template = $queryResult['template'] ?? '';
        $this->data = $queryResult['data'] ?? '';
        $this->isDelete = $queryResult['isDelete'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
        $this->updateBy = $queryResult['updateBy'] ?? '';
    }
}
