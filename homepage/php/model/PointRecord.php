<?php
/**
 * 用户积分记录
 * @author: jiangpengfei
 * @date:   2019-03-28
 */

namespace Model;

class PointRecord extends BaseModel {
    public $id;
    public $userId; // 用户id
    public $amount; // 积分数量
    public $type;   // 类型，1是增加，2是减少
    public $module; // 模块id
    public $action; // 动作id
    public $createTime;
    public $createBy;
    public $updateTime;
    public $updateBy;

    public function __construct($res = []) {
        $this->autoSet($res);
    }
}