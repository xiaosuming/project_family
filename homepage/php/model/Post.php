<?php
    /**
     * 用户推文类
     * @author jiangpengfei
     * @date 2017-01-03
     */

    namespace Model;
    
    class Post{
        // pt.id,pt.userId,ut.nickname as author,ut.photo as authorPhoto,pt.content,pt.address,pt.photo,ct.id as collection,lt.status as likeStatus,pt.comments as commentCount,pt.likes as likeCount,pt.type,pt.create_time as createTime,pt.create_by as createBy,pt.update_time as updateTime,pt.update_by as updateBy 
        public $id;					//推文id
        public $zoneId;				//空间id
        public $userId;				//用户id
        public $author;             //昵称
        public $authorPhoto;        //作者头像
        public $familyId;           //家族id，0代表是向全部家族推送，大于0代表向指定家族推送
        public $content;			//推文内容
        public $coorX;              // 推文发表的地址坐标x
        public $coorY;              // 推文发表的地址坐标y
        public $address;			//地址
        public $photo;				//照片
        public $collection;         //收藏id
        public $likeStatus;         //点赞状态
        public $type = 1;           //推文类型，1是用户发表，2是活动,3是系统推送
        public $commentCount;       //评论数
        public $likeCount;          //点赞数
        public $createTime;			//创建时间
        public $createBy;			//创建人
        public $updateTime;			//更新时间
        public $updateBy;			//更新人
        
        public function __construct($queryResult = array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->zoneId = isset($queryResult['zoneId']) ? $queryResult['zoneId'] : '';
            $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
            $this->author = isset($queryResult['author']) ? $queryResult['author'] : '';
            $this->authorPhoto = isset($queryResult['authorPhoto']) ? $queryResult['authorPhoto'] : '';
            $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : 0;
            $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
            $this->coorX = isset($queryResult['coorX']) ? $queryResult['coorX'] : '';
            $this->coorY = isset($queryResult['coorY']) ? $queryResult['coorY'] : '';
            $this->address = isset($queryResult['address']) ? $queryResult['address'] : '';
            $this->photo = json_decode(isset($queryResult['photo']) ? $queryResult['photo'] :json_encode(array()),true);
            $this->type = isset($queryResult['type']) ? $queryResult['type'] : 1;
            $this->collection = isset($queryResult['collection']) ? $queryResult['collection'] : null;
            $this->likeStatus = isset($queryResult['type']) ? $queryResult['likeStatus'] : 0;
            $this->commentCount = isset($queryResult['commentCount']) ? $queryResult['commentCount'] : '';
            $this->likeCount = isset($queryResult['likeCount']) ? $queryResult['likeCount'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>