<?php
/**
 * 用户积分类
 * author: jiangpengfei
 * Date: 2017/11/30
 */

namespace Model;
class Point{
    public $id;                 //积分记录的id
    public $userId;             //用户id
    public $totalPoint;         //总积分
    public $usedPoint;          //使用掉的积分
    public $remainPoint;        //剩余的积分
    public $createBy;           //记录创建者
    public $createTime;         //记录创建时间
    public $updateBy;           //记录更新者
    public $updateTime;         //记录更新时间

    public function __construct($queryResult = array()){
        $this->id = $queryResult['id'] ?? '';
		$this->userId = $queryResult['userId'] ?? '';
		$this->totalPoint = $queryResult['totalPoint'] ?? '';
		$this->usedPoint = $queryResult['usedPoint'] ?? '';
        $this->remainPoint = $queryResult['remainPoint'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->updateBy = $queryResult['updateBy'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
    }
}