<?php

namespace Model;

use Util\Util;

    class PersonByMerge{
        public $id;
        public $name;
        public $type;   //人物类型，1是本家族人物，2是本家族人物的配偶
        public $level;
        public $gender;
        public $photo;
        public $familyId;
        public $userId;
        public $father;
        public $mother;
        public $brother;
        public $sister;
        public $spouse;
        public $son;
        public $daughter;
        public $zi;
        public $remark;

        public function __construct($queryResult=array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->name = isset($queryResult['name']) ? $queryResult['name'] : '';
            $this->type = isset($queryResult['type']) ? $queryResult['type'] : '';
            $this->level = isset($queryResult['level']) ? $queryResult['level'] : '';
            $this->gender = isset($queryResult['gender']) ? $queryResult['gender'] : '';
            $this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
            $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
            $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
            $this->father = isset($queryResult['father']) ? $queryResult['father'] : '';
            $this->mother = isset($queryResult['mother']) ? $queryResult['mother'] : '';
            $this->brother = isset($queryResult['brother']) ? $queryResult['brother'] : '';
            $this->sister = isset($queryResult['sister']) ? $queryResult['sister'] : '';
            $this->spouse = isset($queryResult['spouse']) ? $queryResult['spouse'] : '';
            $this->son = isset($queryResult['son']) ? $queryResult['son'] : '';
            $this->daughter = isset($queryResult['daughter']) ? $queryResult['daughter'] : '';
            $this->zi = isset($queryResult['zi']) ? $queryResult['zi'] : '';
            $this->remark = isset($queryResult['remark']) ? $queryResult['remark'] : '';

        }

    }

?>