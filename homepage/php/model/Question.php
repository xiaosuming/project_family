<?php
    /**
     * 用户问题类
     */

    namespace Model;

    class Question{
        public $id;					//问题id
        public $userId;				//用户id
        public $nickname;
        public $userPhoto;
        public $title;              //问题标题
        public $content;			//问题描述
        public $photo;				//问题照片
        public $addition;           //是否有问题补充
        public $socialCircleId;     //圈子id
        public $socialCircleName;   //圈子名字
        public $avatar ;            //圈子头像
        public $point;              // 额外悬赏的积分, 默认是0
        public $isFinish;           // 问题是否已经结束
        public $createTime;			//创建时间
        public $createBy;			//创建人
        public $updateTime;			//更新时间
        public $updateBy;			//更新人
        public $circleType;         //圈子类型

        public function __construct($queryResult = array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
            $this->nickname = isset($queryResult['nickname']) ? $queryResult['nickname'] : '';
            $this->userPhoto = isset($queryResult['userPhoto']) ? $queryResult['userPhoto'] : '';
            $this->title = isset($queryResult['title']) ? $queryResult['title'] : '';
            $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
            $this->photo = isset($queryResult['photo']) ? $queryResult['photo'] :'';
            $this->addition = isset($queryResult['addition']) ? $queryResult['addition'] : 0;
            $this->socialCircleId = isset($queryResult['socialCircleId']) ? $queryResult['socialCircleId'] : 0;
            $this->socialCircleName = isset($queryResult['socialCircleName']) ? $queryResult['socialCircleName'] : '';
            $this->avatar = isset($queryResult['avatar']) ? $queryResult['avatar'] : '';
            $this->point = $queryResult['point'] ?? 0;
            $this->isFinish = $queryResult['isFinish'] ?? 0;
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
            $this->circleType = isset($queryResult['circleType']) ? $queryResult['circleType'] : '';
        }
    };
?>
