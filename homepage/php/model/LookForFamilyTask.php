<?php
/**
 * 查找家族的任务
 */
namespace Model;

class LookForFamilyTask{
    public $id;                     //任务id
    public $taskContent;            //任务内容
    public $excludeFamilyIds;       //排除的家族id字符串
    public $executeTime;            //执行次数
    public $lastExecuteDateTime;    //上次执行的时间
    public $priority;               //优先级
    public $threshold;              //匹配阈值
    public $email;                  //通知邮箱
    public $cycle;                  //执行周期
    public $isFinish;               //任务是否完成，0未完成，1成功，-1失败
    public $createTime;
    public $createBy;
    public $updateTime;
    public $updateBy;

    public function __construct($queryResult = array()){
        $this->id = $queryResult['id'] ?? '';
        $this->taskContent = $queryResult['taskContent'] ?? '';
        $this->excludeFamilyIds = $queryResult['excludeFamilyIds'] ?? '';
        $this->executeTime = $queryResult['executeTime'] ?? '';
        $this->lastExecuteDateTime = $queryResult['lastExecuteDateTime'] ?? '';
        $this->priority = $queryResult['priority'] ?? '';
        $this->threshold = $queryResult['threshold'] ?? '';
        $this->email = $queryResult['email'] ?? '';
        $this->cycle = $queryResult['cycle'] ?? '';
        $this->isFinish = $queryResult['isFinish'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
        $this->updateBy = $queryResult['updateBy'] ?? '';
        $this->showType = $queryResult['showType'] ?? 0;
    }
}
