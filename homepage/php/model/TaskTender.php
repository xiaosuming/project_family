<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/14 0014
 * Time: 12:01
 */
namespace Model;

class TaskTender {
    public $id;
    public $userId;
    public $taskId;
    public $content;
    public $isAccept;
    public $createTime;
    public $createBy;
    public $updateTime;
    public $updateBy;

    public function __construct($queryResult = array())
    {
        $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
        $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
        $this->taskId = isset($queryResult['taskId']) ? $queryResult['taskId'] : '';
        $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
        $this->isAccept = isset($queryResult['isAccept']) ? $queryResult['isAccept'] : 0;
        $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
        $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
        $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
        $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
    }
}