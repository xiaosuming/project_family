<?php
/**
 * 请求速率限制的定义
 * author: jiangpengfei
 * date: 2018-04-17
 */

namespace Model;

class Limiter {
    public $capacity;
    public $cycle;
    public $extra;
    public $extraCapacity;
    public $extraCycle;

    public function __construct($capacity,$cycle,$extra = null,$extraCapacity = null,$extraCycle = null) {
        $this->capacity = $capacity;
        $this->cycle = $cycle;
        $this->extra = $extra;
        $this->extraCapacity = $extraCapacity;
        $this->extraCycle = $extraCycle;
    }
}