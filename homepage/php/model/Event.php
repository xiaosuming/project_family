<?php
    /**
     * 大事件类
     * @author jiangpengfei
     * @date 2016-12-10
     */
    namespace Model;
    class Event{
        public $id;						//事件id
        public $sourceId;               //大事件的原本
        public $personId;				//人物id
        public $personName;
        public $userId;					//用户id
        public $familyId;				//家族id
        public $iconograph;             //配图
        public $type;                   //大事件类型 1是公开 2是秘密
        public $openTime;               //公布时间
        public $expTime;				//经历的事件时间
        public $expName;				//事件姓名
        public $abstract;               //事件摘要
        public $expContent;				//事件内容
        public $writeMode;				//撰写模式
        public $participateNum;         //撰写人数
        public $version;                //版本号
        public $createTime;				//创建时间
        public $createBy;				//创建人
        public $updateTime;				//更新时间
        public $updateBy;				//更新人
        public $isMd;
        public $resources;

        public function __construct($queryResult = array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->sourceId = isset($queryResult['sourceId']) ? $queryResult['sourceId'] : 0;
            $this->personId = isset($queryResult['personId']) ? $queryResult['personId'] : '';
            $this->personName = isset($queryResult['personName']) ? $queryResult['personName'] : '';
            $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
            $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
            $this->iconograph = isset($queryResult['iconograph']) ? $queryResult['iconograph'] : '';
            $this->type = isset($queryResult['type']) ? $queryResult['type'] : '';
            $this->openTime = isset($queryResult['openTime']) ? $queryResult['openTime'] : '';
            $this->expTime = isset($queryResult['expTime']) ? $queryResult['expTime'] : '';
            $this->abstract = isset($queryResult['abstract']) ? $queryResult['abstract'] : '';
            $this->expName = isset($queryResult['expName']) ? $queryResult['expName'] : '';
            $this->expContent = isset($queryResult['expContent']) ? $queryResult['expContent'] : '';
            $this->writeMode = isset($queryResult['writeMode']) ? $queryResult['writeMode'] : '';
            $this->participateNum =  isset($queryResult['participateNum']) ? $queryResult['participateNum'] : '';
            $this->version =  isset($queryResult['version']) ? $queryResult['version'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
            $this->resources = isset($queryResult['resources']) ? $queryResult['resources'] : '';
            $this->isMd = isset($queryResult['isMd']) ? $queryResult['isMd'] : 1;
        }
    };
?>
