<?php
/**
 * 订单日志记录表
 * @author: jiangpengfei
 * @date:   2019-03-22
 */

namespace Model;

class OrderLogRecord extends BaseModel {
    public $id;
    public $orderId;
    public $remark;
    public $type;               // 操作类型，0是创建订单，1是支付订单但收到确认，2是确认支付成功，3是确认支付失败，4是订单发货，5是订单确认，99是取消订单
    public $isDelete;
    public $createBy;
    public $createTime;
    public $updateTime;
    public $updateBy;

    public function __construct($params = []) {
        $this->autoSet($params);
    }

}