<?php
    /**
     * 祖坟类
     * @author jiangpengfei
     * @date 2017-03-29
     */
    namespace Model;
    class Grave{
        public $id;						//坟墓id
        public $personId;				//人物id
        public $personName;             //人物姓名
        public $familyId;				//家族id
        public $coorX;				    //横坐标
        public $coorY;				    //纵坐标
        public $address;				//地址
        public $photo;				    //照片
        public $description;			//描述
        public $createTime;             //创建时间
        public $createBy;				//创建人
        public $updateTime;				//更新时间
        public $updateBy;				//更新人
        
        public function __construct($queryResult){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->personId = isset($queryResult['personId']) ? $queryResult['personId'] : '';
            $this->personName = isset($queryResult['personName']) ? $queryResult['personName'] : '';
            $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
            $this->coorX = isset($queryResult['coorX']) ? $queryResult['coorX'] : '';
            $this->coorY = isset($queryResult['coorY']) ? $queryResult['coorY'] : '';
            $this->address = isset($queryResult['address']) ? $queryResult['address'] : '';
            $this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
            $this->description = isset($queryResult['description']) ? $queryResult['description'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>