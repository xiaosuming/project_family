<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/9
 * Time: 11:12
 */

namespace Model;

class Album
{
	public $id;						//相册id
	public $familyId;				//家族id
	public $albumName;              //相册名
	public $description;            //相册描述
	public $photo;                  //相册封面
	public $albumSize;              //相册大小
	public $createTime;             //创建时间
	public $createBy;				//创建人
	public $updateTime;				//更新时间
	public $updateBy;				//更新人
	public $photoCount;				//相册中的照片数量
	public function __construct($queryResult){
		$this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
		$this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
		$this->albumName = isset($queryResult['albumName']) ? $queryResult['albumName'] : '';
		$this->description = isset($queryResult['description']) ? $queryResult['description']:'';
		$this->albumSize = isset($queryResult['albumSize']) ? $queryResult['albumSize']:'';
		$this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime']:'';
		$this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy']: '';
		$this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime']:'';
		$this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
		$this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
		$this->photoCount = isset($queryResult['photoCount']) ? $queryResult['photoCount'] : '';
	}
}