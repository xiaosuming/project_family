<?php
    /**
     * 家族活动类
     * @author jiangpengfei
     * @date 2017-04-06
     */
    namespace Model;

    use Util\Util;

    class Activity{
        public $id;						//活动id
        public $familyId;				//家族id
        public $socialCircleId;         //圈子id
        public $socialCircleName;       //圈子名字
        public $familyName;             //家族名
        public $title;                  //活动标题
        public $content;				//活动内容
        public $photo;                  //活动配图
        public $type;                   //活动类型
        public $startTime;				//开始时间
        public $endTime;				//结束时间
        public $deadline;               //报名截止时间
        public $coorX;
        public $coorY;
        public $address;
        public $createByName;           //创建人姓名
        public $createTime;				//创建时间
        public $createBy;				//创建人
        public $updateTime;				//更新时间
        public $updateBy;				//更新人
        public $nickname;               //昵称
        public $userPhoto;
        public $circleType;               //圈子类型


        public function __construct($queryResult){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
            $this->socialCircleId = isset($queryResult['socialCircleId']) ? $queryResult['socialCircleId'] : '';
            $this->socialCircleName = isset($queryResult['socialCircleName']) ? $queryResult['socialCircleName'] : '';
            $this->familyName = isset($queryResult['familyName']) ? $queryResult['familyName'] : '';
            $this->title = isset($queryResult['title']) ? $queryResult['title'] : '';
            $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
            $this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
            $this->type = isset($queryResult['type']) ? $queryResult['type'] : '';
            $this->startTime = isset($queryResult['startTime']) ? $queryResult['startTime'] : '';
            $this->endTime = isset($queryResult['endTime']) ? $queryResult['endTime'] : '';
            $this->deadline = isset($queryResult['deadline']) ? $queryResult['deadline'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->createByName = isset($queryResult['createByName']) ? $queryResult['createByName'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
            $this->nickname = isset($queryResult['nickname']) ? $queryResult['nickname'] : '';
            $this->userPhoto = isset($queryResult['userPhoto']) ? $queryResult['userPhoto'] : '';
            $this->coorX = isset($queryResult['coorX']) ? $queryResult['coorX'] : '';
            $this->coorY = isset($queryResult['coorY']) ? $queryResult['coorY'] : '';
            $this->address = isset($queryResult['address']) ? $queryResult['address'] : '';
            $this->circleType = isset($queryResult['circleType']) ? $queryResult['circleType'] : '';
        }
    };
?>
