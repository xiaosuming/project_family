<?php
    /**
     * 家族内建分支类
     * @author jiangpengfei
     * @date 2019-02-14
     */
    namespace Model;
    class FamilyInnerBranch{
        public $id;						//分支id
        public $familyId;				//家族id
        public $personId;				//分支起点的人物id
        public $branchName;				//分支名
        public $isDelete;               //是否删除
        public $createTime;				//创建时间
        public $createBy;				//创建人
        public $updateTime;				//更新时间
        public $updateBy;				//更新人
        
        public function __construct($queryResult){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
            $this->personId = isset($queryResult['personId']) ? $queryResult['personId'] : '';
            $this->branchName = isset($queryResult['branchName']) ? $queryResult['branchName'] : '';
            $this->isDelete = isset($queryResult['isDelete']) ? $queryResult['isDelete'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>