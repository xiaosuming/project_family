<?php
/**
 * 家族分享类
 * @author: jiangpengfei
 * @date: 2018-02-02
 */
namespace Model;

class FamilyShare{
    public $id;
    public $familyId;           //家族id
    public $familyName;         //家族名
    public $familyPhoto;        //家族头像
    public $shareCode;          //分享码
    public $timeLimit;          //分享有效期限制
    public $levelLimit;         //分享的最低辈分限制
    public $viewsLimit;         //最多多少ip访问限制
    public $createTime;         //创建时间
    public $createBy;           //创建人
    public $updateTime;         //更新时间
    public $updateBy;           //更新时间
    public $samePersonId;

    public function __construct($queryResult = array()){
        $this->id = $queryResult['id'] ?? '';
        $this->familyId = $queryResult['familyId'] ?? '';
        $this->shareCode = $queryResult['shareCode'] ?? '';
        $this->timeLimit = $queryResult['timeLimit'] ?? '';
        $this->levelLimit = $queryResult['levelLimit'] ?? '';
        $this->viewsLimit = $queryResult['viewsLimit'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->updateBy = $queryResult['updateBy'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
        $this->samePersonId = $queryResult['samePersonId'] ?? 0;
    }
}