<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-12-29
 * Time: 下午4:45
 */

namespace Model;

class GreetingCardPage
{
    public $id;
    public $page;
    public $cardId;
    public $content;
    public $title;
    public $photo;
    public $isDelete;
    public $createTime;
    public $createBy;
    public $updateTime;
    public $updateBy;

    public function __construct($queryResult)
    {
        $this->id = $queryResult['id'] ?? '';
        $this->page = $queryResult['page'] ?? '';
        $this->cardId = $queryResult['cardId'] ?? '';
        $this->content = $queryResult['content'] ?? '';
        $this->title = $queryResult['title'] ?? '';
        $this->photo = $queryResult['photo'] ?? '';
        $this->isDelete = $queryResult['isDelete'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
        $this->updateTime = $queryResult['updateTime'] ?? '';
        $this->updateBy = $queryResult['updateBy'] ?? '';
    }
}
