<?php
/**
 * 专门用来查找家族的人物对象
 */
namespace Model;

class LookForFamilyTaskPerson{
    public $name;
    public $birthday;
    public $address;
}