<?php
/**
 * 查找家族的任务内容
 */
namespace Model;

use Model\LookForFamilyTaskPerson;

class LookForFamilyTaskContent{
    public $persons;    //查找的人物数组，最大长度为5
    public $lastName;   //姓氏
    public function __construct(){
        $this->persons = array();
    }

    public function addPerson(LookUpFamilyTaskPerson $person){
        $this->persons[] = $person;
    }

}