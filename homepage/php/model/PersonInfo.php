<?php
/**
 * 精简一点的Person
 * @author yuanxin
 * @date 2020/10/26
 * 
 */
use Model\Person;


namespace Model;
class PersonInfo{
    public $id;                 //personid
    public $name;                 //name
    public $userId;             //用户id
    public $type;   //人物类型，1是本家族人物，2是本家族人物的配偶
    public $level;
    public $gender;
    public $father;
    public $mother;
    public $brother;
    public $sister;
    public $spouse;
    public $son;
    public $daughter;
    public $ranking;            // 兄弟姐妹间的排行
    public $familyIndex;   //人物家族内顺序，即坐标

    public function __construct($queryResult = array()){
        $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
        $this->name = isset($queryResult['name']) ? $queryResult['name'] : '';
        $this->type = isset($queryResult['type']) ? $queryResult['type'] : '';
        $this->level = isset($queryResult['level']) ? $queryResult['level'] : '';
        $this->gender = isset($queryResult['gender']) ? $queryResult['gender'] : '';
        $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : 0;
        $this->father = isset($queryResult['father']) ? $queryResult['father'] : '';
        $this->mother = isset($queryResult['mother']) ? $queryResult['mother'] : '';
        $this->brother = isset($queryResult['brother']) ? $queryResult['brother'] : '';
        $this->sister = isset($queryResult['sister']) ? $queryResult['sister'] : '';
        $this->spouse = isset($queryResult['spouse']) ? $queryResult['spouse'] : '';
        $this->son = isset($queryResult['son']) ? $queryResult['son'] : '';
        $this->daughter = isset($queryResult['daughter']) ? $queryResult['daughter'] : '';
        $this->ranking = $queryResult['ranking'] ?? '0';
        $this->familyIndex = $queryResult['familyIndex'] ?? '';
    }

    public function personChange(Person $person){
        $this->id = $person->id;
        $this->name = $person->name ;
        $this->type = $person->type ;
        $this->level = $person->level ;
        $this->gender = $person->gender ;
        $this->userId = $person->userId ;
        $this->father = $person->father ;
        $this->mother = $person->mother ;
        $this->brother = $person->brother ;
        $this->sister = $person->sister ;
        $this->spouse = $person->spouse ;
        $this->son = $person->son ;
        $this->daughter = $person->daughter ;
        $this->ranking = $person->ranking ;
        $this->familyIndex = $person->familyIndex ;
    }
}