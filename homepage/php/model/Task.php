<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/13 0013
 * Time: 15:44
 */

namespace Model;

class Task
{
    public $id;
    public $userId;
    public $nickname;
    public $title;
    public $content;
    public $photo;
    public $point;
    public $state;
    public $createTime;
    public $createBy;
    public $updateTime;
    public $updateBy;

    public function __construct($queryResult = array())
    {
        $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
        $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
        $this->nickname = isset($queryResult['nickname']) ? $queryResult['nickname'] : '';
        $this->title = isset($queryResult['title']) ? $queryResult['title'] : '';
        $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
        $this->photo = json_decode(isset($queryResult['photo']) ? $queryResult['photo'] : json_encode(array()), true);
        $this->point = isset($queryResult['point']) ? $queryResult['point'] : 1;
        $this->state = isset($queryResult['state']) ? $queryResult['state'] : 1;
        $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
        $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
        $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
        $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
    }
}