<?php
    /**
     * 用户推文评论类
     * @author jiangpengfei
     * @date 2017-01-03
     */
    namespace Model;

    use Util\Util;

    class Comment{
        public $id;					//空间id
        public $postId;				//推文id
        public $userId;				//用户id
        public $replyTo;				//回复的评论id
        public $replyToUserId;          //回复的用户id
        public $comment;				//空间内容
        public $createTime;			//创建时间
        public $createBy;				//创建人
        public $updateTime;			//更新时间
        public $updateBy;				//更新人
        
        public function __construct($queryResult){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->postId = isset($queryResult['postId']) ? $queryResult['postId'] : '';
            $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
            $this->replyTo = isset($queryResult['replyto']) ? $queryResult['replyto'] : '';
            $this->replytoUserId = isset($queryResult['replytoUserId']) ? $queryResult['replytoUserId'] : '';
            $this->comment = isset($queryResult['comment']) ? $queryResult['comment'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>