<?php
    /**
     * 收藏类
     * @author jiangpengfei
     * @date 2017-06-26
     */
    namespace Model;

    use Util\Util;

    class Collection{
        public $id;						//收藏id
        public $userId;                 //用户id
        public $moduleId;				//模块id
        public $recordId;               //记录id
        public $recordTitle;            //记录标题
        public $recordContent;          //记录内容
        public $extraInfo1;             //额外信息1
        public $extraInfo2;             //额外信息２
        public $createTime;				//创建时间
        public $createBy;				//创建人
        public $updateTime;				//更新时间
        public $updateBy;				//更新人
        
        public function __construct($queryResult){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
            $this->moduleId = isset($queryResult['moduleId']) ? $queryResult['moduleId'] : '';
            $this->recordId = isset($queryResult['recordId']) ? $queryResult['recordId'] : '';
            $this->recordTitle = isset($queryResult['recordTitle']) ? $queryResult['recordTitle'] : '';
            $this->extraInfo1 = isset($queryResult['extraInfo1']) ? $queryResult['extraInfo1'] : '';
            $this->extraInfo2 = isset($queryResult['extraInfo2']) ? $queryResult['extraInfo2'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>