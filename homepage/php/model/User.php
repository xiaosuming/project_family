<?php
/**
 * 用户类
 * @author jiangpengfei
 * @date 2017-09-07
 */
namespace Model;

/**
 * 用户类
 */
class User
{
    public $id; //用户id
    public $username;           //用户名
    public $nickname;           //昵称
    public $phone;              //手机号
    public $email;              //邮箱
    public $password;           //密码
    public $openid;             //微信openid
    public $miniOpenid;         //小程序的openid
    public $aiMiniOpenId;       //AI族群小程序的id
    public $unionid;            // 微信的unionId
    public $qqOpenid;           //qq openid
    public $wbOpenid;           //微博openid
    public $hxUsername;         //即时聊天用户名
    public $hxPassword;         //即时聊天密码
    public $photo;              //用户照片
    public $gender = 2;         //用户性别,默认是未知
    public $birthday;           //用户生日
    public $country;            //用户国家
    public $province;           //省份
    public $city;               //城市
    public $address;            //地址
    public $createTime;         //创建时间
    public $updateTime;         //更新时间

    /**
     * 构造函数
     * @param $queryResult sql查询结果
     */
    public function __construct($queryResult = array())
    {
        $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
        $this->username = isset($queryResult['username']) ? $queryResult['username'] : '';
        $this->nickname = isset($queryResult['nickname']) ? $queryResult['nickname'] : '';
        $this->phone = isset($queryResult['phone']) ? $queryResult['phone'] : '';
        $this->email = isset($queryResult['email']) ? $queryResult['email'] : '';
        $this->password = isset($queryResult['password']) ? $queryResult['password'] : '';
        $this->openid = isset($queryResult['openid']) ? $queryResult['openid'] : '';
        $this->miniOpenid = isset($queryResult['miniOpenid']) ? $queryResult['miniOpenid'] : '';
        $this->aiMiniOpenId = $queryResult['aiMiniOpenId'] ?? '';
        $this->unionId = isset($queryResult['unionId']) ? $queryResult['unionId'] : '';
        $this->qqOpenid = isset($queryResult['qqOpenid']) ? $queryResult['qqOpenid'] : '';
        $this->wbOpenid = isset($queryResult['wbOpenid']) ? $queryResult['wbOpenid'] : '';
        $this->hxUsername = isset($queryResult['hxUsername']) ? $queryResult['hxUsername'] : '';
        $this->hxPassword = isset($queryResult['hxPassword']) ? $queryResult['hxPassword'] : '';
        $this->photo = isset($queryResult['photo']) ? $queryResult['photo'] : '';
        $this->gender = isset($queryResult['gender']) ? $queryResult['gender'] : '';
        $this->birthday = isset($queryResult['birthday']) ? $queryResult['birthday'] : '';
        $this->country = isset($queryResult['country']) ? $queryResult['country'] : '';
        $this->province = isset($queryResult['province']) ? $queryResult['province'] : '';
        $this->city = isset($queryResult['city']) ? $queryResult['city'] : '';
        $this->address = isset($queryResult['address']) ? $queryResult['address'] : '';
        $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
        $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
    }
};
