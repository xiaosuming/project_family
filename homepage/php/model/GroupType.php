<?php
/**
 * 族群类型
 * @author: jiangpengfei
 * @date:  2019-02-13
 */

namespace Model;

class GroupType {
    public static $FAMILY = 1;          // 家族
    public static $MINI_FAMILY = 2;     // 家庭
    public static $CLASSMATE = 3;   // 同学
    public static $SHITU = 4;    // 师徒
    public static $COMPANY = 5; // 公司
}