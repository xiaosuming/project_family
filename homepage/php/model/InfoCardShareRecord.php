<?php
/**
 * 名片分享记录model
 * @author: jiangpengfei
 * @date: 2018-06-07
 */

namespace Model;

class InfoCardShareRecord {
    public $id;                 // 数据库记录id
    public $shareUserId;        // 分享名片的用户id
    public $ownerUserId;        // 名片的拥有者id
    public $targetUserId;       // 接收名片的用户id
    public $shareCardId;        // 分享的名片id
    public $shareTime;          // 分享的时间
    public $createTime;         // 记录创建时间,也就是名片被接收的时间
    public $createBy;           // 记录创建者的id

    public function __construct($queryResult = array()) {
        $this->id = $queryResult['id'] ?? '';
        $this->shareUserId = $queryResult['shareUserId'] ?? '';
        $this->ownerUserId = $queryResult['ownerUserId'] ?? '';
        $this->targetUserId = $queryResult['targetUserId'] ?? '';
        $this->shareCardId = $queryResult['shareCardId'] ?? '';
        $this->shareTime = $queryResult['shareTime'] ?? '';
        $this->createTime = $queryResult['createTime'] ?? '';
        $this->createBy = $queryResult['createBy'] ?? '';
    }

}