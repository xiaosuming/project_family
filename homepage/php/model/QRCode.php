<?php
/**
 * 二维码模型
 * @author: jiangpengfei
 * @date:   2019-03-29
 */

namespace Model;

class QRCode extends BaseModel{
    public $id;
    public $code;
    public $do;
    public $data;
    public $userId;
    public $expire;
    public $createBy;
    public $createTime;
    public $updateBy;
    public $updateTime;

    public function __construct($res = [])
    {
        $this->autoSet($res);
    }
}