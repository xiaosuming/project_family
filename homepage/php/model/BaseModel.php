<?php
/**
 * BaseModel, 可以通过BaseModel来自动映射数据库字段
 */

namespace Model;

class BaseModel {
    public function autoSet($params) {
        $r = new \ReflectionClass($this);
        $properties = $r->getProperties();

        foreach($properties as $key) {
            $this->{$key->name} = $params[$key->name] ?? '';
        }
    }
}