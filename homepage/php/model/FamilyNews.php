<?php
/**
 * 家族新鲜事的Model
 * author:jiangpengfei
 * date: 2017-02-08 
 */

namespace Model;

class FamilyNews{
    public $id;             //新鲜事的id
    public $familyId;       //家族id
    public $module;       //新鲜事所属模块
    public $action;         //动作
    public $operatorId;     //新鲜事的操作人
    public $thingId;        //新鲜事的被操作对象
    public $thingName;      //新鲜事的名字
    public $extraInfo1;     //额外信息1
    public $extraInfo2;     //额外信息2
    public $createBy;       //新鲜事的创建人
    public $createTime;     //新鲜事的创建时间
    public $updateBy;       //新鲜事的更新人
    public $updateTime;     //新鲜事的更新时间
    public function __construct($queryResult){
        $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
        $this->familyId = isset($queryResult['familyId']) ? $queryResult['familyId'] : '';
        $this->module = isset($queryResult['module']) ? $queryResult['module'] : '';
        $this->action  = isset($queryResult['action']) ? $queryResult['action'] : '';
        $this->operatorId = isset($queryResult['operatorId']) ? $queryResult['operatorId'] : '';
        $this->thingId = isset($queryResult['thingId']) ? $queryResult['thingId'] : '';
        $this->thingName = isset($queryResult['thingName']) ? $queryResult['thingName'] : '';
        $this->extraInfo1 = isset($queryResult['extraInfo1']) ? $queryResult['extraInfo1'] : '';
        $this->extraInfo2 = isset($queryResult['extraInfo2']) ? $queryResult['extraInfo2'] : '';
        $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
        $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
        $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
    }

};