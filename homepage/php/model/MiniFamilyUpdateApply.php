<?php
/**
 * 家庭更新申请
 * @Author: jiangpengfei
 * @Date:   2019-03-06
 */

namespace Model;

class MiniFamilyUpdateApply extends BaseModel{
    public $id;
    public $recommendId;
    public $remark;
    public $familyId;
    public $miniFamilyId;
    public $isAccept;
    public $isDelete;
    public $createBy;
    public $createTime;
    public $updateBy;
    public $updateTime;

    public function __construct($queryResult = array())
    {
        $this->autoSet($queryResult);
    }
}