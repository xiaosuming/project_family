<?php
    /**
     * 消息类
     * @author jiangpengfei
     * @date 2017-06-12
     */
    namespace Model;
    class Message{
        public $id;						//消息id
        public $messageCode;            //消息码
        public $fromUser;				//发件人id
        public $fromUserName;           //发件人姓名
        public $fromUserPhoto;          // 发件人头像
        public $toUser;				    //接收人id
        public $content;				//内容
        public $module;				    //模块
        public $action;				    //动作
        public $recordId;				//相关记录id
        public $extraRecordId = 0;      //额外的记录id,默认是0
        public $createBy;               //创建人
        public $createTime;             //创建时间
        public $updateBy;               //更新人
        public $updateTime;             //更新时间
        public $isDelete;               //是否删除
        public $isRead;                 //是否已读
        public $isAccept;               //是否已接受   0是未处理,1是接受,-1是拒绝
        public $ext;                    // 额外的json信息
        
        public function __construct($queryResult = array()){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->messageCode = isset($queryResult['messageCode']) ? $queryResult['messageCode'] : '';
            $this->fromUser = isset($queryResult['fromUser']) ? $queryResult['fromUser'] : '';
            $this->fromUserName = isset($queryResult['fromUserName']) ? $queryResult['fromUserName'] : '';
            $this->toUser = isset($queryResult['toUser']) ? $queryResult['toUser'] : '';
            $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
            $this->module = isset($queryResult['module']) ? $queryResult['module'] : '';
            $this->action = isset($queryResult['action']) ? $queryResult['action'] : 0;
            $this->recordId = isset($queryResult['recordId']) ? $queryResult['recordId'] : '';
            $this->extraRecordId = isset($queryResult['extraRecordId']) ? $queryResult['extraRecordId'] : 0;
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->isDelete = isset($queryResult['isDelete']) ? $queryResult['isDelete'] : '';
            $this->isRead = isset($queryResult['isRead']) ? $queryResult['isRead'] : '';
            $this->isAccept = isset($queryResult['isAccept']) ? $queryResult['isAccept'] : '';
            $this->ext = $queryResult['ext'] ?? '';
        }
    };
?>