<?php
    /**
     * 用户空间类
     * @author jiangpengfei
     * @date 2016-12-10
     */
    namespace Model;
    class Zone{
        public $id;					//空间id
        public $userId;				//用户id
        public $content;			//空间内容
        public $canShare;           //能否分享
        public $createTime;			//创建时间
        public $createBy;			//创建人
        public $updateTime;			//更新时间
        public $updateBy;			//更新人
        
        public function __construct($queryResult){
            $this->id = isset($queryResult['id']) ? $queryResult['id'] : '';
            $this->userId = isset($queryResult['userId']) ? $queryResult['userId'] : '';
            $this->content = isset($queryResult['content']) ? $queryResult['content'] : '';
            $this->canShare = isset($queryResult['canShare']) ? $queryResult['canShare'] : '0';
            $this->createTime = isset($queryResult['createTime']) ? $queryResult['createTime'] : '';
            $this->createBy = isset($queryResult['createBy']) ? $queryResult['createBy'] : '';
            $this->updateTime = isset($queryResult['updateTime']) ? $queryResult['updateTime'] : '';
            $this->updateBy = isset($queryResult['updateBy']) ? $queryResult['updateBy'] : '';
        }
    };
?>