<?php
    $version = $params['version'] ?? "1";
    $whiteList['getQRCode'] = 'allow';

    require_once("filter/filter.php");
    $qrcode_action_file_array['addQRCode'] = "logicserver/qrcode_action/{$version}/addQRCode.php";
    $qrcode_action_file_array['getQRCode'] = "logicserver/qrcode_action/{$version}/getQRCode.php";