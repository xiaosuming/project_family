<?php
/**
 * 获取二维码详情
 * @author: jiangpengfei
 * @date:   2019-03-29
 */

use Util\Util;
use Util\Check;
use DB\CDBQRCode;


$code = Check::check($params['code']);

try {
    $qrcodeDB = new CDBQRCode();
    $detail = $qrcodeDB->getQRCode($code);

    if ($detail == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '不存在的二维码');
        exit;
    }

    if ($detail->expire > 0) {
        // 判断二维码的有效期
        $detail->creatTime;
    }

    $data['detail'] = $detail;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}