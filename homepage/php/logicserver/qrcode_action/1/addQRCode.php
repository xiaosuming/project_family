<?php
/**
 * 添加二维码，生成二维码要展示的code
 * @author: jiangpengfei
 * @date:   2019-03-29
 */

use DB\CDBQRCode;
use Model\QRCode;
use Util\Util;
use Util\Check;
use Util\QRCodeAction;

$data = Check::checkJSONFormat($params['data'] ?? '');
$do = Check::check($params['do'] ?? '', 1, -1);
$userId = $GLOBALS['userId'];

if (!QRCodeAction::inQRCodeAction($do, $data)) {
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '不支持的动作');
    exit;
}

try {
    $qrcode = new QRCode();
    $qrcode->id = Util::getNextId();
    $qrcode->do = $do;
    $qrcode->expire = 0;
    $qrcode->data = $data;
    $qrcode->userId = $userId;
    $qrcode->code = md5($qrcode->id.$qrcode->userId);

    $qrcodeDB = new CDBQRCode();
    $qrcodeDB->addQRCode($qrcode);

    $output['qrcode'] = QRCodeAction::prefix($do).$qrcode->code;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $output);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}