<?php
    use Util\Util;
    use DB\CDBLike;
    use DB\CDBZone;
    use DB\CDBMessage;
    use Util\Check;
    use Model\Message;
    
    $postId = Check::check(trim(isset($params['postId']) ? $params['postId'] : ''));				//推文id
    $module = Check::check(trim(isset($params['module']) ? $params['module'] : ''));			    //模块

    try{
        if($postId != "" && $module != ""){
            $likeDB = new CDBLike();
            $userId = $GLOBALS['userId'];

            $isExist = $likeDB->checkLike($userId,$postId,$module);
        
            $isSuccess = $likeDB->like($userId,$postId,$module);
            $data['isSuccess'] = $isSuccess;
            if($isSuccess != 0){
                if($isSuccess == 1 && $isExist==-1){
                    switch($module) {//根据不同模块，发送不同消息
                        case 1:
                            $zoneDB = new CDBZone();
                            //点赞成功，发送消息给用户通知
                            $post = $zoneDB->getPost($postId);
                            if($post->userId != $userId){
                                $messageDB = new CDBMessage();
                                //如果不是给自己的推文点赞，则发消息
                                $message = new Message();
                                $message->fromUser = $userId;
                                $message->toUser = $post->userId;
                                $message->content = $post->content;
                                $message->module = $module;
                                $message->recordId = $postId;
                                $messageDB->addMessage($message);
                            }
                            break;
                    }
                }
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }