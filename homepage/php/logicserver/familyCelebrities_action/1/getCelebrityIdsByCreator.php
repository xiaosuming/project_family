<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/12/19
 * Time: 17:19
 */
use Util\Util;
use DB\CDBFamilyCelebrities;
try{
	$userId = $GLOBALS['userId'];
	$familyCelebrityDB = new CDBFamilyCelebrities();
	$data = $familyCelebrityDB->getCelebrityIdsByCreator($userId);
//	Util::printResult($GLOBALS['ERROR_SUCCESS'],$userId);
	Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e){
	//异常处理
	$logger->error(Util::exceptionFormat($e));
	Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}