<?php

/**
 * 获取某个家族的传说人物
 *
 *
 */

use DB\CDBFamily;
use DB\CDBFamilyCelebrities;
use Util\Check;
use Util\Util;
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : '')); //家族ID

if ($familyId == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $familyCelebrityDB = new CDBFamilyCelebrities();
    $familyDB = new CDBFamily();
    $userId = $GLOBALS['userId'];

    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    if ($family->openType == 0) {
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit;
        }
    }
    $celebrities = $familyCelebrityDB->getCelebrities($familyId);
    $data['celebrities'] = $celebrities;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
