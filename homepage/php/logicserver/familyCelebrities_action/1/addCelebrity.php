<?php

/**
 * 添加家族传奇人物
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBFamilyCelebrities;
use Model\Person;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));//家族ID
$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));//人物ID
$styleName = Check::check(trim(isset($params['styleName']) ? $params['styleName'] : ''), 0, 10);//字
$pseudonym = Check::check(trim(isset($params['pseudonym']) ? $params['pseudonym'] : ''), 0, 10);//号
$yearOfPass = Check::checkInteger(trim(isset($params['yearOfPass']) ? $params['yearOfPass'] : 0));//去世年份，不详填写0
$profile = Check::check(trim(isset($params['profile']) ? $params['profile'] : ''));//简介
$styleName = Util::filterContentReplace($styleName);
$profile = Util::filterContentReplace($profile);

if ($familyId == "" || $personId == "" || $yearOfPass == "" || $profile == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();
    $familyCelebritiesDB = new CDBFamilyCelebrities();

    //检出人物是否存在
    $person = new Person();
    $person = $personDB->getPersonById($personId);
    if ($person == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "人物不存在");
        exit;
    }

    //检查人物是否已经存在
    if ($familyCelebritiesDB->checkCelebrityExist($personId)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "请不要重复添加");
        exit;
    }

    //检查家族是否存在
    if ($familyDB->getFamilyById($familyId) == null) {//家族不存在
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "家族不存在");
        exit;
    } else if ($person->familyId != $familyId) {//家族存在但该人物不在家族内
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "人物不属于该家族");
        exit;
    }

    //获取人物信息
    $userId = $GLOBALS['userId'];
    $personName = $person->name;
    $tmp = explode("-", $person->birthday);
    $yearOfBirth = intval($tmp[0]);//获取年份
    $photo = $person->photo;

    //添加传说人物
    $id = $familyCelebritiesDB->addCelebrity($familyId, $personId, $personName, $styleName, $pseudonym, $yearOfBirth, $yearOfPass, $profile, $photo, $userId);
    if ($id > 0) {
        $data['id'] = $id;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "添加失败");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
