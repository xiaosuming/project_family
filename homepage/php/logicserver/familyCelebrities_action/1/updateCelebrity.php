<?php

/**
 * 更新某个家族的传说人物
 */

use Util\Check;
use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use DB\CDBFamilyCelebrities;
use Model\Person;

$celebrityId = Check::checkInteger(trim(isset($params['celebrityId']) ? $params['celebrityId'] : ''));//家族传说人物ID

if($celebrityId == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();
    $familyCelebrityDB = new CDBFamilyCelebrities();

    //判断传入的celebrityId是否存在
    $familyCelebrity = $familyCelebrityDB->getCelebrityById($celebrityId);
    if(is_null($familyCelebrity)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "传说人物不存在");
        exit;
    }
    
    //获取修改后的人物信息
    $person = new Person();
    $person = $personDB->getPersonById($familyCelebrity->personId);
    $userId = $GLOBALS['userId'];
    $personName = $person->name;
    $tmp = explode("-", $person->birthday);
    $yearOfBirth = intval($tmp[0]);//获取年份
    $photo = $person->photo;

    //获取家族ID
    $familyId = $familyCelebrityDB->getCelebrityById($celebrityId)->familyId;
    
    // 判断用户是否以下角色
    // 1、记录的创建者
    $isCreator = $familyCelebrityDB->isCreaterOfCelebrity($celebrityId, $userId);
    // 2、家族的管理员
    $isAdmin = $familyDB->isAdminForFamily($familyId, $userId);
    
    // 用户是家族管理员或者记录创建者，则可以修改
    if($isAdmin || $isCreator){
        $rows = $familyCelebrityDB->updateCelebrity($celebrityId, $personName, $yearOfBirth, $photo, $userId);
        $data['rows'] = $rows;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {// 否则，用户无权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "没有操作权限");
        exit;
    }

} catch(PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
