<?php

/**
 * 删除某个家族的传说人物
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBFamilyCelebrities;

$celebrityId = Check::checkInteger(trim(isset($params['celebrityId']) ? $params['celebrityId'] : ''));//家族传说人物ID

if($celebrityId == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $userId = $GLOBALS['userId'];
    $familyDB = new CDBFamily();
    $familyCelebrityDB = new CDBFamilyCelebrities();
    
    //判断传入的celebrityId是否存在
    $familyCelebrity = $familyCelebrityDB->getCelebrityById($celebrityId);
    if(is_null($familyCelebrity)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "传说人物不存在");
        exit;
    }
    $familyId = $familyCelebrity->familyId;
    
    // 判断用户是否以下角色
    // 1、记录的创建者
    $isCreator = $familyCelebrityDB->isCreaterOfCelebrity($celebrityId, $userId);
    // 2、家族的管理员
    $isAdmin = $familyDB->isAdminForFamily($familyId, $userId);
    
    // 用户是家族管理员或者记录创建者，则可以删除
    if($isAdmin || $isCreator){
        $rows = $familyCelebrityDB->deleteCelebrity($celebrityId);
        $data['rows'] = $rows;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {// 否则，用户无权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "没有操作权限");
        exit;
    }

} catch(PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
