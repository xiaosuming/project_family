<?php
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $familyCelebrities_action_file_array['addCelebrity'] = "logicserver/familyCelebrities_action/{$version}/addCelebrity.php";
    $familyCelebrities_action_file_array['deleteCelebrity'] ="logicserver/familyCelebrities_action/{$version}/deleteCelebrity.php";
    $familyCelebrities_action_file_array['editCelebrity'] ="logicserver/familyCelebrities_action/{$version}/editCelebrity.php";
    $familyCelebrities_action_file_array['getCelebrities'] ="logicserver/familyCelebrities_action/{$version}/getCelebrities.php";
    $familyCelebrities_action_file_array['updateCelebrity'] = "logicserver/familyCelebrities_action/{$version}/updateCelebrity.php";
	$familyCelebrities_action_file_array['getCelebrityIdsByCreator']  = "logicserver/familyCelebrities_action/{$version}/getCelebrityIdsByCreator.php";
