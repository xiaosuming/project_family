<?php
    require_once("logicserver/like_action/like_action_file.php");

    use Util\Util;

    $sub_action = isset($params['sub_action'])?$params['sub_action']:"0"; 
    
    if (!array_key_exists($sub_action, $like_action_file_array))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "like action not exist ".$sub_action;
        Util::printResult($errorCode, $data);
        exit(-1);
    }
    
    $sub_action_file = $like_action_file_array[$sub_action];
    
    if (!file_exists($sub_action_file))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "like action file not exist ".$sub_action_file;
        Util::printResult($errorCode, $data);
           exit(-1);
    }
    
    require_once($sub_action_file);