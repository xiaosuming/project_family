<?php
/**
 * 撤销管理员
 * author: jiangpengfei
 * date: 2017-02-08
 */
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBAccount;
    use DB\CDBFamilyNews;
    use Util\Check;
    use Util\PostGenerate;

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $adminId = Check::checkInteger(trim(isset($params['adminId']) ? $params['adminId'] : ''));
    $currentUserId = $GLOBALS['userId'];
    try{
        if($familyId!=""&&$adminId!=""){
            $familyDB = new CDBFamily();
            //判断执行操作的用户是否是家族的创始人
            if(!$familyDB->isOriginatorForFamily($familyId,$currentUserId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }else{
                //家族创始人不能撤销自己的管理员权限
                if($adminId == $currentUserId){
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误,创始人不能撤销自己的管理员权限");
                    exit;
                }
            }

            if($familyDB->rescindAdmin($familyId,$adminId)){
                $accountDB = new CDBAccount();
                $user = $accountDB->getUserInfoById($adminId);
                $familyNewsDB = new CDBFamilyNews();
                $familyNewsDB->addFamilyNews($familyId,$GLOBALS['FAMILY_MODULE'],$GLOBALS['RESCINDADMIN'],$userId,$adminId,$user['username'],$userId);

                Util::printResult($GLOBALS['ERROR_SUCCESS'], "撤销管理员成功");
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "撤销管理员失败");
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }		
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }