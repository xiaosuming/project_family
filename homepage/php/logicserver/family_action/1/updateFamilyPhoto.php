<?php
    /**
     * 更新家族描述
     * TODO 权限验证没做
     *
     */

use DB\CDBPushFamily;
use Util\Util;
    use DB\CDBFamily;
    use Util\Check;
    
    $userId = $GLOBALS['userId'];
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));

    try{
        $familyDB = new CDBFamily();

        //检查更新权限,只有家族管理员可以修改
        if(!$familyDB->isAdminForFamily($familyId,$userId)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit();
        }

        $updateCount = $familyDB->updateFamilyPhoto($familyId,$photo);
        if ($updateCount > 0) {
            if ($GLOBALS['TEST_FAMILY_TASK']) {
                $CDBPushFamily = new CDBPushFamily();
                $modelId = $familyId;
                $action = 2;
                $source = [
                    'photo' => $photo
                ];
                $CDBPushFamily->setPushFamilyTask($modelId, $action, $source);
            }
        }
        $data['updateCount'] = $updateCount;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }