<?php
/**
 * 拒绝用户申请加入家族
 * @author jiangpengfei
 * @date 2017-08-22
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;

$messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));                  //接受的消息id
$userId = $GLOBALS['userId'];

if($messageId === ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $messageDB = new CDBMessage();

    //判断message是否存在
    $message = $messageDB->getMessage($messageId);
    if($message == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该申请");
        exit;
    }else if($message->toUser != $userId){
        //检查消息的接收人是不是该用户
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
        exit;
    }

    $familyId = $message->recordId;         //记录的家族id
    $applyUserId = $message->fromUser;      //申请的用户id

    $familyDB = new CDBFamily();
    //检查管理员权限
    if(!$familyDB->isAdminForFamily($familyId,$userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限错误");
        exit();
    }

    $applyResult = $messageDB->refuseApplyJoin($messageId,$userId);
    if($applyResult){
        $data['refuse'] = $applyResult;
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
    }else{
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'],"数据插入错误");
    }
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}