<?php
/**
 * 获取家庭更新数据时的推荐
 * @Author: jiangpengfei
 * @Date:   2019-03-05
 */

use DB\CDBFamily;
use Util\Util;
use Util\Check;
use Model\MiniFamilyBindRecommend;

$bindRecordId = Check::checkInteger($params['bindRecordId']);

try {
    $familyDB = new CDBFamily();
    // 获取绑定的记录
    $record = $familyDB->getBindRootRecordById($bindRecordId);

    if ($record == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '记录不存在');
        exit;
    }

    $familyId = $record['familyId'];
    $miniFamilyId = $record['miniFamilyId'];

    $recommend = $familyDB->getRecommendUpdateInfoFromMiniFamilyToFamily($miniFamilyId, $familyId);

    // 将推荐的数据存下来
    $miniFamilyUpdateRecommend = new MiniFamilyBindRecommend();
    $miniFamilyUpdateRecommend->applyId = 0;
    $miniFamilyUpdateRecommend->familyId = $familyId;
    $miniFamilyUpdateRecommend->miniFamilyId = $miniFamilyId;
    $miniFamilyUpdateRecommend->familyPersonId = $record['familyPersonId'];
    $miniFamilyUpdateRecommend->miniFamilyPersonId = $record['miniFamilyPersonId'];
    $miniFamilyUpdateRecommend->recommendData = json_encode($recommend, JSON_UNESCAPED_UNICODE);
    $miniFamilyUpdateRecommend->recommendType = 1;    // 绑定的推荐类型
    $miniFamilyUpdateRecommend->createBy = $userId;
    $miniFamilyUpdateRecommend->updateBy = $userId;

    $recommendId = $familyDB->addMiniFamilyBindRecommend($miniFamilyUpdateRecommend);

    $data['recommendId'] = $recommendId;
    $data['recommendData'] = $recommend;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}