<?php
/**
 * 设置族群内建分支管理员
 * @Author: yuanxin
 * @Date:   20200702
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;

$familyId = Check::checkInteger($params['familyId'] ?? '');         // 分支id
$branchAdminUserId = Check::checkInteger($params['branchAdminUserId'] ?? '');   // 要设置为分支管理员的用户id
$startUserId = Check::checkInteger($params['startUserId'] ?? '');         // 分支开始的用户id
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    // 检查管理员权限
    if (!$familyDB->isAdminForFamily($familyId, $userId)) {
        // 权限不足
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    if (!$familyDB->isUserForFamily($familyId, $branchAdminUserId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '分支管理员必须是家族内成员');
        exit;
    }

    // 检查用户是否已经是当前分支的管理员
    if ($familyDB->isAdminForBranch2($branchAdminUserId,$startUserId,$familyId)) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '当前用户已经是该分支的管理员');
        exit;
    }

    $recordId = $familyDB->addBranchAdmin($familyId, $branchAdminUserId, $startUserId,$userId);
    
    $data['adminId'] = $recordId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}