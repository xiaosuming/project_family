<?php
/**
 * 添加家族快照
 * @Author: jiangpengfei
 * @Date:  2019-02-15
 */

use DB\CDBFamily;
use Util\Check;
use Util\Util;
use Util\RedLock\RedLock;
use Config\RedisInstanceConfig;
use Util\SysConst;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$tag = Check::check($params['tag'] ?? '', 1, 50);   // 封存的快照版本号
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    // 检查操作的用户是否是家族的管理员
    if (!$familyDB->isAdminForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    // 检查家族的类型
    $familyDetail = $familyDB->getFamilyById($familyId);
    if ($familyDetail == null || $familyDetail->groupType != 1) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '族群类型不正确，权限不足');
        exit;
    }

    // 这里要对家族进行加锁，这个锁是保证在生成快照期间，所有的要封存的数据不可以修改
    $redlockConfig = RedisInstanceConfig::getRedlockConfig();
    $redLock = new RedLock($redlockConfig);

    // 对操作加锁
    $lock = $redLock->lock(SysConst::$FAMILY_SNAPSHOT_LOCK.$familyId);
    if (is_bool($lock) && !$lock) {
        // 没有拿到锁，报错
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '当前正在生成家族快照，不允许更新操作');
        exit;
    }

    // TODO: gener_person,gener_family_album,gener_family_photo,gener_album_person
    // gener_significantEvent,gener_grave 对这些表进行操作时，都需要拿到锁
    $result = $familyDB->addFamilySnapshot($familyId, $tag, $userId);

    // 快照生成后，释放锁
    $redLock->unlock($lock);

    $data = [];
    if (is_bool($result) && !$result) {
        // 封存版本失败
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '封存版本失败，请稍后再试');
    } else {
        $data['version'] = $result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}