<?php

/**
 * 根据临时邀请码获取群体的基本信息
 * @author: jiangpengfei
 * @date:   2018-11-08
 */

use Util\Check;
use DB\CDBFamily;
use Util\TempStorage;
use Util\Util;

$inviteCode = $params['inviteCode'] ?? '';

$tempStorage = new TempStorage();
$groupId = $tempStorage->get($inviteCode);

if (!$groupId) {
    // 失效
    Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '邀请码已失效');
    exit();
}

$familyDB = new CDBFamily();
$family = $familyDB->getFamilyById($groupId);

if ($family == null) {
    Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '群体不存在');
    exit();
}

Util::printResult($GLOBALS['ERROR_SUCCESS'], $family);