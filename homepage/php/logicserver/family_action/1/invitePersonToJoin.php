<?php
/**
 * 邀请用户加入家族，并绑定到某个人物
 * author: jiangpengfei
 * date: 2017-06-12
 */

use Util\Util;
use DB\CDBFamily;
use DB\CDBAccount;
use DB\CDBPerson;
use DB\CDBMessage;
use Util\Check;
use Util\PostGenerate;
use Model\Message;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));                  //要绑定的人物id
$inviteUserId = Check::checkInteger(trim(isset($params['inviteUserId']) ? $params['inviteUserId'] : ''));      //邀请的用户id
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''), 2, 30);                     //备注
$userId = $GLOBALS['userId'];
$content = Util::filterContentReplace($content);

try {
    if ($personId != "" && $personId != "") {
        $accountDB = new CDBAccount();
        //判断userId是否存在
        $user = $accountDB->getUserInfoById($inviteUserId);
        if ($user == null) {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "邀请的用户不存在");
            exit;
        }

        $personDB = new CDBPerson();
        //判断执行操作的用户是否对人物id有管理权限
        if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误$userId,$personId");
            exit;
        }

        //判断人物是否绑定了用户
        $person = $personDB->getPersonById($personId);
        if ($person == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物不存在");
            exit;
        }
        if ($person->userId > 0) {
            //绑定了用户
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物已经绑定了用户");
            exit;
        }

        //判断当前用户是否已绑定家族中某个成员
        $familyDB = new CDBFamily();
        if ($familyDB->isUserForFamily($person->familyId, $inviteUserId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户已存在家族中");
            exit;
        }

        $family = $familyDB->getFamilyById($person->familyId);

        $messageDB = new CDBMessage();
        //生成message发送给受邀请的用户
        $message = new Message();
        $message->fromUser = $userId;
        $message->toUser = $inviteUserId;
        $message->content = $content;
        $message->module = $GLOBALS['FAMILY_MODULE'];
        $message->action = $GLOBALS['INVITETOJOIN'];
        $message->recordId = $personId;
        $message->extraRecordId = $person->familyId;

        $extData = [
            "familyName" => $family->name,
            "personName" => $person->name
        ];
        $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);

        $messageId = $messageDB->addMessage($message);
        if ($messageId > 0) {
            $data['messageId'] = $messageId;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "邀请失败");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
