<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-28
 * Time: 下午5:51
 */

use DB\CDBFamily;
use DB\CDBPdfTask;
use Util\Check;
use Util\Util;

/**
 * @author jiangpengfei
 * @date   2018-06-13
 */
$taskId = Check::checkInteger($params['taskId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $ocrDB = new CDBPdfTask();
    $result = $ocrDB->getPdf($taskId);

    //权限判断
    $familyDB = new CDBFamily();
    if(!$familyDB->isUserForFamily($result['familyId'], $userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $data['result'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
