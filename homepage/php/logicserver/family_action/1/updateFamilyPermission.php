<?php
/**
 * Created by PhpStorm.
 * User: xu
 * Date: 2017/10/17 0017
 * Time: 17:49
 * 修改家族人物权限
 */

use DB\CDBPerson;
use Util\Check;
use Util\Util;
$userId=$GLOBALS['userId'];
$personId = check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : '')); //人物Id
$level = check::checkInteger(trim(isset($params['level']) ? $params['level'] : '2')); // 家族中人物权限水平，默认为2


if ($personId == "" || $level == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {

    $CDBPerson = new CDBPerson();
    $familyId = $CDBPerson->getFamilyIdByPersonId($personId);
    if ($familyId == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '家族不存在');
        exit;
    }
    $row = $CDBPerson->updateFamilyPermission($personId,$level,$userId);
    if ($row > 0) {
        $data['updateCount'] = $row;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }
    Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "更新出现错误");
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}