<?php 
    /**
     * 根据家族id获取群聊列表
     * author:jiangpengfei
     * date:2017-11-24
     */
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBAccount;
    use Util\Check;

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));               //家族id

    $userId = $GLOBALS['userId'];
    try{

        $familyDB = new CDBFamily();
        //检查用户是否是家族成员
        if(!$familyDB->isUserForFamily($familyId,$userId)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }
        
        $chatGroups = $familyDB->getChatGroupsByFamilyId($familyId);

        $data['chatGroups'] = $chatGroups;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }