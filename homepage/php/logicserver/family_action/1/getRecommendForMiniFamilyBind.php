<?php
/**
 * 获取家庭绑定的推荐绑定数据
 * 这是用算法生成的推荐，所以需要用户确认（可能还要修改）
 * @Author: jiangpengfei
 * @Date:   2019-02-27
 */

use DB\CDBFamily;
use Util\Util;
use Util\Check;
use Model\MiniFamilyBindRecommend;

$applyId = Check::checkInteger($params['applyId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    $apply = $familyDB->getBindMiniFamilyApply($applyId);

    if ($apply == null) {
        // 申请不存在
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '申请不存在');
        exit;
    }

    // 检查当前用户是否是家族的管理员
    if (!$familyDB->isAdminForFamily($apply['familyId'], $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    // 获取推荐的绑定
    list($pair, $allPersons) = $familyDB->recommendPersonBindByPersonId($apply['miniFamilyId'], $apply['familyPersonId'], $apply['miniFamilyPersonId']);

    if ($pair == null && $allPersons == null) {
        // 推荐时出现无法解决的异常情况，这里可以记录下来
        $logger->error('生成推荐数据出错, 申请的id是:', $applyId);

        Util::printResult($GLOBALS['ERROR_EXCEPTION'], '人物关联推荐出现异常，请联系管理员');
        exit;
    }

    $recommendData = [
        'pair' => $pair,
        'persons' => $allPersons
    ];

    // 将推荐的数据存下来
    $miniFamilyBindRecommend = new MiniFamilyBindRecommend();
    $miniFamilyBindRecommend->applyId = $applyId;
    $miniFamilyBindRecommend->familyId = $apply['familyId'];
    $miniFamilyBindRecommend->miniFamilyId = $apply['miniFamilyId'];
    $miniFamilyBindRecommend->familyPersonId = $apply['familyPersonId'];
    $miniFamilyBindRecommend->miniFamilyPersonId = $apply['miniFamilyPersonId'];
    $miniFamilyBindRecommend->recommendData = json_encode($recommendData, JSON_UNESCAPED_UNICODE);
    $miniFamilyBindRecommend->recommendType = 1;    // 绑定的推荐类型
    $miniFamilyBindRecommend->createBy = $userId;
    $miniFamilyBindRecommend->updateBy = $userId;

    $recommendId = $familyDB->addMiniFamilyBindRecommend($miniFamilyBindRecommend);

    $data['recommendId'] = $recommendId;
    $data['recommendData'] = $recommendData;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

}  catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}