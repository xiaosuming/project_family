<?php
/**
 * 获取临时邀请码
 * 临时邀请码有一定的时限，但是可以不经过管理员同意加入群体
 * 临时邀请码只有管理员可以生成
 * 家族不应该使用这个接口
 * @author： jiangpengfei
 * @date:    2018-11-08
 */

use Util\Check;
use Util\TempStorage;
use Util\Util;
use DB\CDBFamily;
use Util\GroupType;

$groupId = Check::checkInteger($params['groupId'] ?? '');
$timeout = Check::checkInteger($params['timeout'] ?? 3600*3);   // 默认是3个小时的有效期
$userId = $GLOBALS['userId'];

// 为了安全起见，不允许设置的过长
if ($timeout > 3600*24) {
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '时间设置过长');
    exit();
}

$familyDB = new CDBFamily();


// 检查用户对群体的管理权限
if (!$familyDB->isAdminForFamily($groupId, $userId)) {
    Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
    exit();
}

$group = $familyDB->getFamilyById($groupId);

if ($group == null) {
    Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '群体不存在');
    exit();
}

if ($group->groupType ==  GroupType::BIG_FAMILY) {
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '不允许的群体类型');
    exit();
}


$inviteCode = md5($groupId.time().Util::generateRandomCode(24));

$tempStorage = new TempStorage();
$tempStorage->setTemp($inviteCode, $groupId, $timeout);

$data['inviteCode'] = $inviteCode;

Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
