<?php
/**
 * 获取家族的所有分支
 * @author jiangpengfei
 * @date 2017-10-20
 */

 use Util\Util;
 use DB\CDBFamily;
 use DB\CDBPerson;
 use Util\Check;

 $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

 try{
     if($familyId!=""){
         $familyDB = new CDBFamily();
         $userId = $GLOBALS['userId'];
         //检查权限
         if(!$familyDB->isUserForFamily($familyId, $userId)){
             Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
             exit;
         }
         
         $branches = $familyDB->getBranchesByFamilyId($familyId);
         $data['branches'] = $branches;
         Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
     }else{
         Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
         exit;
     }
 }catch(PDOException $e){
     //异常处理
     $logger->error(Util::exceptionFormat($e));
     Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
 }