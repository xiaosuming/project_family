<?php
/**
 * 获取家族管理员id， 有新版本接口
 * author:jiangpengfei
 * date: 2017-04-24
 */
use DB\CDBFamily;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

try {
    if ($familyId != "") {
        $familyDB = new CDBFamily();
        $userId = $GLOBALS['userId'];

        $family = $familyDB->getFamilyById($familyId);

        if ($family == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
            exit;
        }

        if ($family->openType == 0) {

            //检查权限
            if (!$familyDB->isUserForFamily($familyId, $userId)) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
                exit;
            }
        }

        $adminId = $familyDB->getFamilyAdminId($familyId);
        $data = $adminId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
