<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-7-30
 * Time: 上午11:22
 * 修改家族名字
 */

use DB\CDBPushFamily;
use Util\Util;
use DB\CDBFamily;
use Util\Check;
use ThirdParty\MiniProgram;

$userId = $GLOBALS['userId'];
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$familyName = Check::check(trim(isset($params['familyName']) ? $params['familyName'] : '')); //家族名字

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$familyName]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    $familyDB = new CDBFamily();

    //检查更新权限,只有家族管理员可以修改
    if (!$familyDB->isAdminForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit();
    }

    $updateCount = $familyDB->updateFamilyName($familyId, $familyName);
    if ($updateCount > 0) {
        if ($GLOBALS['TEST_FAMILY_TASK']) {
            $CDBPushFamily = new CDBPushFamily();
            $modelId = $familyId;
            $action = 2;
            $source = [
                'name' => $familyName
            ];
            $CDBPushFamily->setPushFamilyTask($modelId, $action, $source);
        }
    }
    $data['updateCount'] = $updateCount;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
