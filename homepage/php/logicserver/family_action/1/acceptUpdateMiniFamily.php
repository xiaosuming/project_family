<?php
/**
 * 接受更新家庭的申请
 * @Author: jiangpengfei
 * @Date:   2019-03-06
 */

use DB\CDBFamily;
use Util\Util;
use Util\Check;

$applyId = Check::checkInteger($params['applyId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    $apply = $familyDB->getMiniFamilyUpdateApply($applyId);

    if ($apply == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '记录不存在');
        exit;
    }

    if ($apply->isAccept != 0) {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '该申请已经处理');
        exit;
    }

    $recommendId = $apply->recommendId;

    $recommend = $familyDB->getMiniFamilyBindRecommend($recommendId);

    if ($recommend == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '记录不存在');
        exit;
    }

    $result = $familyDB->updateInfoFromMiniFamilyToFamily($applyId, $recommend, $userId);

    $data['result'] = $result;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);


} catch(PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}