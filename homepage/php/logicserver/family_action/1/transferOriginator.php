<?php
/**
 * 移交创始人身份
 * author: jiangpengfei
 * date: 2017-02-08
 */
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBAccount;
    use DB\CDBFamilyNews;
    use Util\Check;
    use Util\PostGenerate;

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $originatorId = Check::checkInteger(trim(isset($params['originatorId']) ? $params['originatorId'] : ''));
    $currentUserId = $GLOBALS['userId'];
    try{
        if($familyId!=""&&$originatorId!=""){
            $familyDB = new CDBFamily();
            //判断执行操作的用户是否是家族的创始人
            if(!$familyDB->isOriginatorForFamily($familyId,$currentUserId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }
            if($familyDB->transferOriginator($familyId,$originatorId)){
                $accountDB = new CDBAccount();
                $user = $accountDB->getUserInfoById($originatorId);
                $familyNewsDB = new CDBFamilyNews();
                //动作更新的新鲜事
                $familyNewsDB->addFamilyNews($familyId,$GLOBALS['FAMILY_MODULE'],$GLOBALS['TRANSFERORIGINATOR'],$userId,$originatorId,$user['username'],$userId);

                Util::printResult($GLOBALS['ERROR_SUCCESS'], "移交创始人身份成功");
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "移交创始人身份失败");
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }		
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }