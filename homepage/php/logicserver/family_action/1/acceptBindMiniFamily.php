<?php
/**
 * 接受绑定家庭的申请
 * @Author: jiangpengfei
 * @Date:   2019-02-28
 */

use DB\CDBFamily;
use Util\Util;
use Util\Check;

$recommendId = Check::checkInteger($params['recommendId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();

    $recommend = $familyDB->getMiniFamilyBindRecommend($recommendId);

    if ($recommend == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '记录不存在');
        exit;
    }

    // 检查用户是不是家族的管理员
    if (!$familyDB->isAdminForFamily($recommend->familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    

    // 将recommend中的数据更新
    $recommendData = json_decode($recommend->recommendData, true);

    $result = $familyDB->addMiniFamilyBindRecord($recommend->familyId, $recommend->miniFamilyId, $recommend->familyPersonId, $recommend->miniFamilyPersonId, $recommendData, $recommend->applyId, $userId);

    if ($result) {
        $data['result'] = true;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        // 添加数据失败
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加数据失败');
    }

} catch(PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}