<?php
/**
 * 用户申请合并分支
 * @author jiangpengfei
 * @date 2017-10-19
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;
use DB\CDBPerson;

$userId = $GLOBALS['userId'];

$fromFamilyId = Check::check(trim(isset($params['fromFamilyId']) ? $params['fromFamilyId'] : ''));             //家族合并的请求方id
$toFamilyId = Check::check(trim(isset($params['toFamilyId']) ? $params['toFamilyId'] : ''));                 //被申请的家族id
$fromPersonId = Check::check(trim(isset($params['fromPersonId']) ? $params['fromPersonId'] : ''));               //申请方的合并人物
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : '申请合并分支'),1,500);             //申请信息备注

if($fromFamilyId === "" || $toFamilyId === "" || $fromPersonId === ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $familyDB = new CDBFamily();
    $fromFamily = $familyDB->getFamilyById($fromFamilyId);

    if($fromFamily === null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "申请合并的家族不存在");
        exit();
    }

    //检查该用户是否是家族创始人
    if($fromFamily->originator != $userId){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足，需要创始人权限");
        exit();
    }

    //检查请求方人物是否存在，是否和fromFamily对应，是否已经绑定了家族
    $personDB = new CDBPerson();
    $person = $personDB->getPersonById($fromPersonId);
    if($person == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "人物不存在");
        exit();
    }

    if($person->familyId != $fromFamilyId){
        //人物不在家族中
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "人物不在家族中");
        exit();
    }

    //向被请求方发送消息
    $messageDB = new CDBMessage();
    $messageId = $messageDB->sendMergeBranchMessage($fromFamilyId,$toFamilyId,$fromPersonId,$userId,$remark);
    if($messageId == -1){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "申请合并的家族不存在");
        exit();
    }

    $data['messageId'] = $messageId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}