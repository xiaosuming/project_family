<?php
/**
 * 获取临时加入的家族（不绑定人物的用户）
 * @author jiangpengfei
 * @date   2017-08-24
 */
    use Util\Util;
    use DB\CDBFamily;
    use Util\Check;
    
    $userId = $GLOBALS['userId'];
    $data = array();

    try{
        $familyDB = new CDBFamily();
        $result = $familyDB->getJoinFamilyForTmp($userId);
        $data['familyList'] = $result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }