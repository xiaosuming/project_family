<?php 
    /**
     * 获取家族信息
     * author:jiangpengfei
     * date: 2017-04-24
     *
     */
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBPerson;
    use Util\Check;

    $familyIds = Check::checkIdArray(trim(isset($params['familyIds']) ? $params['familyIds'] : ''));

    if($familyIds === ''){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    try{
        $familyDB = new CDBFamily();
        $userId = $GLOBALS['userId'];

        $familyIds = explode(',',$familyIds);

        foreach($familyIds as $familyId) {
            //检查权限
            if(!$familyDB->verifyUserReadPermissionByMerge($userId, $familyId)) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
                exit;
            }
        }

        $familys = $familyDB->getFamilyByIds($familyIds);
        $data['familys'] = $familys;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }