<?php
/**
 * 申请将家庭中的信息更新的家族中
 * @Author: jiangpengfei
 * @Date:   2019-03-05
 */

use DB\CDBFamily;
use Util\Util;
use Util\Check;
use Model\MiniFamilyUpdateApply;

$updateRecommendId = Check::checkInteger($params['updateRecommendId'] ?? '');
$remark = Check::check($params['remark'] ?? '', 0, 200);
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    $recommend = $familyDB->getMiniFamilyBindRecommend($updateRecommendId);
    
    if ($recommend == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '记录不存在');
        exit;
    }

    $miniFamilyUpdateApply = new MiniFamilyUpdateApply();
    $miniFamilyUpdateApply->recommendId = $updateRecommendId;
    $miniFamilyUpdateApply->remark = $remark;
    $miniFamilyUpdateApply->familyId = $recommend->familyId;
    $miniFamilyUpdateApply->miniFamilyId = $recommend->miniFamilyId;
    $miniFamilyUpdateApply->isAccept = 0;
    $miniFamilyUpdateApply->isDelete = 0;
    $miniFamilyUpdateApply->createBy = $userId;
    $miniFamilyUpdateApply->updateBy = $userId;

    $insertId = $familyDB->addMiniFamilyUpdateApply($miniFamilyUpdateApply);

    $data['applyId'] = $insertId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}