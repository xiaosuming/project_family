<?php
/**
 * 获取公开的族群列表
 * @author jiangpengfei
 * @date   2018-07-05
 */

use DB\CDBFamily;
use Util\Check;
use Util\Util;

try {
    $familyDB = new CDBFamily();
    $familys = $familyDB->getOpenFamilys();

    $data['familys'] = $familys;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
