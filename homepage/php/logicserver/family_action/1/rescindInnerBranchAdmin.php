<?php
/**
 * 撤销分支管理员
 * @Author: jiangpengfei
 * @Date:   2019-02-15
 */

use Util\Util;
use DB\CDBFamily;
use DB\CDBFamilyNews;
use Util\Check;

$branchId = Check::checkInteger($params['branchId'] ?? '');
$adminUserId = Check::checkInteger($params['adminUserId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();

    // 获取分支资料
    $branch = $familyDB->getInnerBranch($branchId);

    if ($branch == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '分支不存在');
        exit;
    }

    $familyId = $branch->familyId;

    // 检查用户的权限
    if (!$familyDB->isAdminForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    // 检查撤销用户是否是分支的管理员
    if (!$familyDB->isAdminForBranch($adminUserId, $branchId)) {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '用户不是分支管理员');
        exit;
    }

    // 撤销管理员
    $rows = $familyDB->rescindInnerBranchAdmin($branchId, $adminUserId, $userId);

    $data['record'] = $rows;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}