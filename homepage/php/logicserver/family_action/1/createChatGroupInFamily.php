<?php 
    /**
     * 创建家族
     * author:jiangpengfei
     * date:2017-11-23
     */
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBAccount;
    use Util\Check;

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));               //家族id
    $groupName = Check::check(trim(isset($params['groupName']) ? $params['groupName'] : ''),1,80);                        //群聊名
    $description = Check::check(trim(isset($params['description']) ? $params['description'] : ''),1,200);                 //群聊的描述
    $maxusers = Check::checkInteger(trim(isset($params['maxusers']) ? $params['maxusers'] : ''));                 //群聊的最大人数
    $needPermit = Check::checkInteger(trim(isset($params['needPermit']) ? $params['needPermit'] : ''));             //是否需要管理员的同意
    $isPublic = Check::checkInteger(trim(isset($params['isPublic']) ? $params['isPublic'] : ''));                     //群是否公开
    $allowInvites = Check::checkInteger(trim(isset($params['allowInvites']) ? $params['allowInvites'] : ''));   //是否允许群成员邀请
    $members = trim(isset($params['members']) ? $params['members'] : '');                                       //群成员
    $customInfo = $params['customInfo'] ?? null;

    $userId = $GLOBALS['userId'];
    try{
        if($groupName == "" || $description == "" ){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }

        $familyDB = new CDBFamily();
        //检查用户是否是家族成员
        if(!$familyDB->isUserForFamily($familyId,$userId)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }

        //获取用户在环信的账号
        $accountDB = new CDBAccount();
        $huanxinInfo = $accountDB->getHuanxinInfoById($userId);
        if($huanxinInfo == null){
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "当前用户无环信信息");
            exit;
        }

        $members = explode(",",$members);
        if(count($members) <= 0){
            Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "成员格式错误");
            exit;
        }

        $hxUsername = $huanxinInfo['hxUsername'];
        
        $chatGroup = $familyDB->createChatGroupInFamily($familyId,$groupName,$description,$maxusers,$isPublic,$needPermit,$allowInvites,$hxUsername,$userId,$members, $customInfo);

        if($chatGroup == -1){
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "群聊创建失败");
            exit;
        }

        $data['chatGroup'] = $chatGroup;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }