<?php

use DB\CDBPushFamily;
use Util\Util;
use DB\CDBFamily;
use Util\Check;
use ThirdParty\InstantMessage;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

try {
    if ($familyId != "") {
        $familyDB = new CDBFamily();
        //权限检查，只有是家族创始人才可以删除家族
        $userId = $GLOBALS['userId'];
        if (!$familyDB->isOriginatorForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
            exit;
        }

        //获取家族的环信聊天室id
        $family = $familyDB->getFamilyById($familyId);
        if ($family == null) {
            //家族不存在
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
            exit;
        }
        $hxChatRoom = $family->hxChatRoom;
        $socialCircleId = $family->socialCircleId;
        $count = $familyDB->deleteFamily($familyId,$socialCircleId);

        if ($count > 0) {
            //家族删除成功，去环信删除聊天室
            $im = new InstantMessage();
            $im->deleteChatRoom($hxChatRoom);

            if ($GLOBALS['TEST_FAMILY_TASK']) {
                $CDBPushFamily = new CDBPushFamily();
                $modelId = $familyId;
                $action = 3;
                $source = [];
                $CDBPushFamily->setPushFamilyTask($modelId, $action, $source);
            }
            $data['deleteCount'] = $count;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除家族失败");
            exit();
        }


    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
