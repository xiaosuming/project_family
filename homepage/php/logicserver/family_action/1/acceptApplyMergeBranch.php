<?php
/**
 * 接受用户申请合并分支
 * @author jiangpengfei
 * @date 2017-10-19
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBMessage;

$messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));                  //接受的消息id
$toPersonId = Check::checkInteger(trim($params['toPersonId'] ?? ''));
$branchName = Check::check(trim($params['branchName'] ?? ''));
$userId = $GLOBALS['userId'];
use Model\Message;

if($messageId === ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $messageDB = new CDBMessage();

    //判断message是否存在
    $message = $messageDB->getMessage($messageId);
    if($message == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该消息");
        exit;
    }else if($message->toUser != $userId){
        //检查消息的接收人是不是该用户
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
        exit;
    }

    //根据toPersonId获取familyId
    $personDB = new CDBPerson();
    $person = $personDB->getPersonById($toPersonId);

    if($person == null){
        //人物不存在
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
        exit;
    }

    $familyDB = new CDBFamily();
    //检查创始人权限
    if(!$familyDB->isOriginatorForFamily($person->familyId,$userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限错误");
        exit();
    }

    $updateRecord = $messageDB->acceptMergeBranch($message,$toPersonId,$branchName,$userId);
    if($updateRecord > 0){

        //发送消息通知申请的管理员
        $newMessage = new Message();
        $newMessage->fromUser = $userId;
        $newMessage->toUser = $message->fromUser;
        $newMessage->content = "家族分支合并申请已完成！";
        $newMessage->module = 9;
        $newMessage->action = $GLOBALS['MESSAGE_MERGE_BRANCH_REPLY_ACCEPT'];
        $newMessage->recordId = $messageId;    //记录Id是回复的消息id
        $messageDB->addMessage($newMessage);

        $data['updateRecord'] = $updateRecord;
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        exit();
    }else{
        //-1代表人物一存在子代，不能合并,-2代表人物不存在,-3是sql执行异常
        $errorMsg = null;
        switch($updateRecord){
            case -1:
                $errorMsg = "人物一存在子代，不能合并";
                break;
            case -2:
                $errorMsg = "人物不存在";
                break;
            case -3:
                $errorMsg = "sql执行异常";
                break;
            default:
                $errorMsg = "未知错误";
        }
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'],$errorMsg);
        exit();
    }
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}