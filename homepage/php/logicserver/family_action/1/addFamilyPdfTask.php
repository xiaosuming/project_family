<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-28
 * Time: 下午5:51
 */

use DB\CDBFamily;
use DB\CDBPdfTask;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

/**
 * 添加带出pdf任务。每一个任务都会有一个编号。添加任务时会获取编号。用户之后根据编号取回导出的结果
 * @author jiangpengfei
 * @date   2018-06-13
 */
$familyId = Check::checkInteger($params['familyId'] ?? '');
$familyTreeName = $params['name'] ?? '';        // 输入宗谱名,测试阶段只支持4字
$direction = $params['direction'] ?? 1;         // 默认从右往左
$userId = $GLOBALS['userId'];

try {
    //权限判断
    $familyDB = new CDBFamily();
    if(!$familyDB->isUserForFamily($familyId, $userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $accountDB = new CDBAccount();
    $userInfo = $accountDB->getUserInfoV3($userId);
    if ($userInfo['pdfPermission'] != 1){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户无权限");
        exit;
    }

    $ocrDB = new CDBPdfTask();
    $taskId = $ocrDB->addPdfTask($userId, $familyId,$familyTreeName, $direction);

    if ($taskId > 0) {
        $data['taskId'] = $taskId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
