<?php
/**
 * 拒绝邀请成为家族成员
 * author: jiangpengfei
 * date: 2017-06-12
 */
    use Util\Util;
    use DB\CDBMessage;
    use DB\CDBFamily;
    use Util\Check;
    use Util\PostGenerate;

    $messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));                  //接受的消息id
    $userId = $GLOBALS['userId'];


    try{
        if($messageId != ""){
            $messageDB = new CDBMessage();
            //判断message是否存在
            $message = $messageDB->getMessage($messageId);
            if($message == null){
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该邀请");
                exit;
            }else if($message->toUser != $userId){
                //检查消息的接收人是不是该用户
                Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
                exit;
            }

            if($messageDB->refuseInvite($messageId)){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], "拒绝成功");
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "拒绝失败");
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }		
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }