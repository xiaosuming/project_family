<?php
    /**
     * 获取指定家族的统计信息
     */
    use Util\Util;
    use DB\CDBFamily;
    // use DB\CDBPerson;
    use DB\CDBArea;
    use Util\Check;
    use Util\HttpContext;
    
    // 家族ID
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    // 统计参数，用于指定统计类型
    $statisticNumber = Check::checkInteger(trim(isset($params['statisticNumber']) ? $params['statisticNumber'] : ''));
    $userId = $GLOBALS['userId'];
    $data = array();
    // 临时解决每次都有private_db_host 的问题
    HttpContext::getInstance()->setEnv('private_db_host', false);

    try{    
        $familyDB = new CDBFamily();
        $areaDB = new CDBArea();
        $family = $familyDB->getFamilyById($familyId);

        if($family == null) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "家族ID不存在");
        } else if($statisticNumber>6 || $statisticNumber<0) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "统计类型不正确");
        } else {

            if ($family->openType == 0) {

                //检查权限
                if (!$familyDB->isUserForFamily($familyId, $userId)) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
                    exit;
                }
            }

            switch($statisticNumber) {
                case 0://性别统计
                    $genderStatistics = $familyDB->getFamilyGenderInfo($familyId);
                    $data['gender'] = $genderStatistics;
                    break;
                case 1://年龄统计
                    $ageStatistics = $familyDB->getFamilyAgeInfo($familyId);
                    $data['age'] = $ageStatistics;
                    break;
                case 2://血型统计
                    $bloodTypeStatistics = $familyDB->getFamilyBloodTypeInfo($familyId);
                    $data['bloodType'] = $bloodTypeStatistics;
                    break;
                case 3://婚姻状况
                    $maritalStatistics = $familyDB->getFamilyMaritalInfo($familyId);
                    $data['maritalStatus'] = $maritalStatistics;
                    break;
                case 4://辈分统计
                    $levelStatistics = $familyDB->getFamilyLevelInfo($familyId);
                    $data['level'] = $levelStatistics;
                    break;
                case 5://地域统计
                //统计国家
                    $country = $familyDB->getFamilyAreaInfo($familyId, -1, -1, -1, -1);
                    $countryStatistic = array();
                    foreach($country as $i) {
                        $countryName = $areaDB->getAreaNameById($i['country']);
                        if(!$countryName) {
                            $countryName['areaName'] = "未知国家";
                        }
                        $tmp['name'] = $countryName['areaName'];
                        $tmp['sum'] = intval($i['sum']);
                        $province = $familyDB->getFamilyAreaInfo($familyId, $i['country'], -1, -1, -1);
                        $provinceStatistic = array();
                        foreach ($province as $j) {
                            $provinceName = $areaDB->getAreaNameById($j['province']);
                            if(!$provinceName) {
                                $provinceName['areaName'] = "未知省份";
                            }
                            $temp['name'] = $provinceName['areaName'];
                            $temp['sum'] = intval($j['sum']);
                            array_push($provinceStatistic, $temp);
                        }
                        $tmp['provinces'] = $provinceStatistic;
                        array_push($countryStatistic, $tmp);
                    }
                    $data['area'] = $countryStatistic;
                    break;
                case 6://爱好统计
                    $hobbyStatistics = $familyDB->getFamilyHobbyInfo($familyId);
                    $data['hobby'] = $hobbyStatistics;
                    break;
            }
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
?>