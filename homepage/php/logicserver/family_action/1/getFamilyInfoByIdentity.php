<?php
/**
 * 获取家族信息
 * author:jiangpengfei
 * date: 2017-04-24
 *
 */

use Util\Util;
use DB\CDBFamily;
use Util\Check;

$identity = Check::check(trim(isset($params['identity']) ? $params['identity'] : ''));

try {
    if ($identity == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $familyDB = new CDBFamily();

    $info = $familyDB->getFamilyInfoByIdentity($identity);

    $data['info'] = $info;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}