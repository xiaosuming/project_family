<?php

    /**
     * 根据家族id和人物id删除人物及其分支
     * author:lucky
     * date:2019-6-25
     */


    use Util\Util;
    use DB\CDBFamily;
    use Util\Check;
    
    $personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    
    
    $userId = $GLOBALS['userId'];
    if($personId > 0){
    
        $familyDB = new CDBFamily();
        //判断被删除的personId是否在家族中
        if(!$familyDB->verifyPersonIdAndFamilyId($personId, $familyId)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户与家族id不匹配或者用户不存在");
            exit;
        }
        
        //检查用户是否是族群管理员，或者是族群创建者
        if(!$familyDB->isAdminForFamily($familyId, $userId) && !$familyDB->isOriginatorForFamily($familyId, $userId)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }
    
        $res = $familyDB->deleteBranch($personId, $familyId, $userId);
        Util::printResult($res['error_code'], $res['error_msg']);
    
    }else{
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "personId 不正确");
        exit;
    }