<?php
/**
 * 申请将家庭绑定到家族
 * @Author: jiangpengfei
 * @Date:   2019-02-25
 */

// 家庭绑定家族的流程是，家庭先申请绑定，家族选择绑定的人物，然后系统生成推荐的绑定关系，
// 家族管理员查看并修改绑定关系，最后确认绑定

use DB\CDBFamily;
use Util\Util;
use Util\Check;

$miniFamilyId = Check::checkInteger($params['miniFamilyId'] ?? '');
$miniFamilyPersonId = Check::checkInteger($params['miniFamilyPersonId'] ?? '');
$familyId = Check::checkInteger($params['familyId'] ?? '');
$familyPersonId = Check::checkInteger($params['familyPersonId'] ?? '');
$remark = Check::check($params['remark'] ?? '', 0, 200);
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();

    // 检查这个用户是不是当前家族的 && 检查这个用户是不是当前家庭的
    if ( ! ($familyDB->isUserForFamily($familyId, $userId) && $familyDB->isOriginatorForFamily($miniFamilyId, $userId))) {
      Util::printResult($GLOBALS['ERROR_PERMISSION'], '当前用户无权限发起绑定请求');
      exit;  
    }

    $applyId = $familyDB->applyBindMiniFamilyToFamily($miniFamilyId, $miniFamilyPersonId, $familyId, $familyPersonId, $remark, $userId);

    // TODO: 要用消息通知管理员

    $data['applyId'] = $applyId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
