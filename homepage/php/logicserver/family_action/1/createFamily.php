<?php
/**
 * 创建家族(有version 2接口)
 * author:jiangpengfei
 * date:2017-04-26
 */

use DB\CDBPushFamily;
use DB\CDBPushPerson;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBAccount;
use Util\Check;
use Util\PhotoGenerator;
use ThirdParty\InstantMessage;
use Util\HttpContext;

$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''), 1, 30);                           //家族管理员的姓名
$gender = Check::checkGender(trim(isset($params['gender']) ? $params['gender'] : ''));                     //家族管理员的性别
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));                         //家族管理员的住址
$birthday = Check::check(trim(isset($params['birthday']) ? $params['birthday'] : ''));                      //家族管理员的生日
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));                               //家族管理员的照片
$familyPhoto = Check::check(trim(isset($params['familyPhoto']) ? $params['familyPhoto'] : ''));             //家族照片
$description = Check::check(trim(isset($params['description']) ? $params['description'] : ''), 0, 100);             //家族描述
$familyName = Check::check(trim(isset($params['familyName']) ? $params['familyName'] : ''), 1, 14);                //家族姓名
$surname = trim(isset($params['surname']) ? $params['surname'] : '');                          //姓氏
$groupType = Check::checkInteger($params['groupType'] ?? 1);        // 群体类型，默认是1，1是家族，2是家庭，3是师生，4是师徒 5是公司同事
$data = array();

$userId = $GLOBALS['userId'];
try {

    if ($familyPhoto == '') {
        $familyPhoto = PhotoGenerator::generateFamilyPhotoFromResource();
    }

    if ($photo == '') {
        $photo = PhotoGenerator::generatePersonPhotoFromResource();
    }

    if ($name != "" && $gender != "" && $familyName != "") {
        $familyDB = new CDBFamily();
        // if(!$GLOBALS['DEBUG_MODE']){
        //     //如果不是在调试模式下，则需要检查创建的家族数
        //     if($familyDB->getUserCreateFamilyCount($userId) > $GLOBALS['MAX_FAMILY_COUNT']){
        //         Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "一个用户最多只能创建5个家族");
        //         exit;
        //     }
        // }

        if ($gender != 0 && $gender != 1) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "用户性别选择不正确");
            exit;
        }

        if($surname == ""){
            $surname=mb_substr($name,0,1);
        }

        list($familyId,$personId) = $familyDB->createFamily($GLOBALS['userId'], $familyName, $name,$surname, $gender, $address, $birthday, $description, $photo, $familyPhoto, '', $groupType);

        // 不是私有数据库的情况下，数据进行索引
        if (!HttpContext::getInstance()->getEnv('private_db_host')) {

            // 索引家族信息
            $CDBPushFamily = new CDBPushFamily();
            $modelId = $familyId;
            $action = 1;
            $source = [
                'familyId' => $familyId,
                'name' => $familyName,
                'photo' => $photo,
                'description' => $description
            ];
            $CDBPushFamily->setPushFamilyTask($modelId, $action, $source);

            // 索引人物信息
            $personDB = new CDBPerson();
            $person = $personDB->getPersonById($personId);

            $CDBPushPerson = new CDBPushPerson();
            $modelId = $personId;
            $action = 1;
            $source = $person;
            $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
        }

        $data['familyId'] = $familyId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
