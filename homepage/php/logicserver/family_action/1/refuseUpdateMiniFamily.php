<?php
/**
 * 拒绝更新家庭
 * @Author: jiangpengfei
 * @Date:   2019-03-07
 */
use Util\Util;
use Util\Check;
use DB\CDBFamily;

$applyId = Check::checkInteger($params['applyId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    
    $familyDB = new CDBFamily();
    $apply = $familyDB->getMiniFamilyUpdateApply($applyId);

    if ($apply == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '该申请不存在');
        exit;
    }

    // 检查userId是否是家族的管理员
    if (!$familyDB->isAdminForFamily($apply->familyId) ) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $result = $familyDB->refuseUpdateMiniFamilyApply($applyId);

    $data['update'] = $result;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}