<?php
/**
 * 复制家族分支
 * 
 *
 */

use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;

$userId = $GLOBALS['userId'];
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$startLevel = Check::checkInteger(trim(isset($params['startLevel']) ? $params['startLevel'] : ''));


try {
    $personDB = new CDBPerson();
    $familyDB = new CDBFamily();

    //检查用户是否是族群管理员，或者是族群创建者
    if(!$familyDB->isAdminForFamily($familyId, $userId) && !$familyDB->isOriginatorForFamily($familyId, $userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
        exit;
    }

    $updateMsg = $familyDB->updateFamilyStartLevel($familyId, $userId, $startLevel);
    if($updateMsg['error_code'] == true){
        $updateMsg['error_code'] = $GLOBALS['ERROR_SUCCESS'];
    }
    Util::printResult($updateMsg['error_code'], $updateMsg['error_msg']);
    
    
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}