<?php
/**
 * 设置族群内建分支管理员
 * @Author: jiangpengfei
 * @Date:   2019-02-15
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;

$branchId = Check::checkInteger($params['branchId'] ?? '');         // 分支id
$adminUserId = Check::checkInteger($params['adminUserId'] ?? '');   // 管理员的用户id
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    // 检查分支和管理员是否属于同一个家族
    $branch = $familyDB->getInnerBranch($branchId);
    if ($branch == null) {
        // 分支不存在
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '分支不存在');
        exit;
    }

    $familyId = $branch->familyId;

    // 检查管理员权限
    if (!$familyDB->isAdminForFamily($familyId, $userId)) {
        // 权限不足
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    if (!$familyDB->isUserForFamily($familyId, $adminUserId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '分支管理员必须是家族内成员');
        exit;
    }

    // 检查用户是否已经是当前分支的管理员
    if ($familyDB->isAdminForBranch($adminUserId, $branchId)) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '当前用户已经是管理员');
        exit;
    }

    $recordId = $familyDB->addInnerBranchAdmin($branchId, $adminUserId, $userId);
    
    $data['adminId'] = $recordId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}