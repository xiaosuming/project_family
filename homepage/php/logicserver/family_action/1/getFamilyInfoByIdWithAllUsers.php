<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-5-5
 * Time: 下午12:58
 */

use DB\CDBFamily;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

try {
    if ($familyId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $familyDB = new CDBFamily();

    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '家族不存在');
        exit;
    }

    $data['family'] = $family;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
