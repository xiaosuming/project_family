<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/27 0027
 * Time: 11:09
 */

//获取通过申请的用户信息

use DB\CDBFamily;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];
$familyId = check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : '')); //申请加入的家族Id
if ($familyId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}

try {
    $CDBFamily = new CDBFamily();

    //判断用户的创始人权限，或者管理员权限
    if ($CDBFamily->isOriginatorForFamily($familyId, $userId) || $CDBFamily->isAdminForFamily($familyId, $userId)) {
        $result = $CDBFamily->getApplyUserList($familyId);
        $data['userList']=$result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }

    Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
    exit;

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
