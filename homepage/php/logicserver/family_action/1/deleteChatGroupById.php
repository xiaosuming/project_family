<?php 
    /**
     * 根据环信群聊id删除群聊
     * author:jiangpengfei
     * date:2017-11-24
     */
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBAccount;
    use Util\Check;
    use ThirdParty\InstantMessage;

    $hxChatGroupId = trim(isset($params['hxChatGroupId']) ? $params['hxChatGroupId'] : ''); //环信群聊id
    $systemId = trim(isset($params['systemId']) ? $params['systemId'] : '');               //系统id
    

    $userId = $GLOBALS['userId'];
    try{
        $familyDB = new CDBFamily();
        $chatGroup = null;
        if($hxChatGroupId != ''){
            $chatGroup = $familyDB->getChatGroupByHxId($hxChatGroupId);
        }else if($systemId != ''){
            $chatGroup = $familyDB->getChatGroupBySystemId($systemId);
        }
        if($chatGroup == null){
            //不存在
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "群聊不存在");
            exit;
        }
        
        //检查用户是否是族群管理员，或者是族群创建者
        if(!$familyDB->isAdminForFamily($chatGroup['familyId'],$userId) && !$chatGroup['createBy'] != $userId){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }

        $chatGroupId = $chatGroup['chatGroupId'];

        // 删除腾讯的chatGroup
        $im = new InstantMessage();
        $delteRes = $im->deleteChatRoom($chatGroupId);

        if ($delteRes) {
            //删除
            $deleteRow = $familyDB->deleteChatGroupByHxId($hxChatGroupId);
        }


        $data['deleteRow'] = $deleteRow;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }