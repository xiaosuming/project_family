<?php
/**
 * 创建家族内部分支
 * @author: jiangpengfei
 * @date:   2019-02-13
 */

use DB\CDBFamily;
use Util\Util;
use DB\CDBPerson;
use DB\CDBAccount;
use Util\Check;
use Model\GroupType;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$branchName = Check::check($params['branchName'] ?? '', 1, 50);
$personId = Check::check($params['personId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    // 判断权限，只有创始人可以创建分支
    if (!$familyDB->isOriginatorForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    // 判断该人物下有没有分支
    if ($familyDB->getBranchByLeadPersonId($personId) != null) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '该人物已经是分支的起点了');
        exit;
    }

    // 检查family的类型

    $family = $familyDB->getFamilyById($familyId);

    if ($family->groupType != GroupType::$FAMILY) {
        // 家族类型
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '不允许的族群类型');
        exit;
    }

    // 判断人物是否是家族中的
    $personDB = new CDBPerson();
    $person = $personDB->getPersonById($personId);

    if ($person->familyId != $familyId) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '人物不在族群中');
        exit;
    }

    // 创建分支
    $createBranchId = $familyDB->createFamilyInnerBranch($familyId, $branchName, $personId, $userId);

    $data['branchId'] = $createBranchId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}