<?php
/**
 * 分享家族
 * @author: jiangpengfei
 * @date: 2018-02-02
 */
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use DB\CDBFamilyShare;
use Model\FamilyShare;

$familyId = Check::checkInteger($params['familyId'] ?? '');         //家族id
$timeLimit = Check::checkInteger($params['timeLimit'] ?? '3');       //时间限制
$levelLimit = Check::checkInteger($params['levelLimit'] ?? '');     //辈分限制
$viewsLimit = Check::checkInteger($params['viewsLimit'] ?? '3');     //查看的人数限制

$userId = $GLOBALS['userId'];

if($familyId == ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{

    $familyDB = new CDBFamily();
    if($timeLimit > 3){
        //检查是不是管理员
        if($familyDB->isAdminForFamily($familyId,$userId)){
            //是家族管理员，时间限制放开到3~24小时
            if($timeLimit > 24){
                Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "管理员设置的时间限制不得超过24小时");
                exit;
            }
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "普通用户设置的时间限制不得超过3小时");
            exit;
        }
    }

    //查询最低level
    $familyMinLevel = $familyDB->getFamilyMinLevelWithUserId($familyId);
    if(is_null($familyMinLevel)){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "家族不存在");
        exit;
    }

    //查询用户自己的level
    $personDB = new CDBPerson();
    $userPerson = $personDB->getPersonByFamilyIdAndUserId($familyId,$userId);

    $userMinLevel = $userPerson->level - 3;
    $familyMinLevel = $familyMinLevel - 2;

    $allowMinLevel = $userMinLevel < $familyMinLevel ? $userMinLevel : $familyMinLevel;

    if($levelLimit > $allowMinLevel){
        //设定的辈分不允许
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "设定的公开辈分不在允许范围内");
        exit;
    }

    $familyShare = new FamilyShare();
    $familyShare->familyId = $familyId;
    $familyShare->shareCode = md5($familyId.$userId.Util::generateRandomCode(6));
    $familyShare->timeLimit = $timeLimit;
    $familyShare->levelLimit = $levelLimit;
    $familyShare->viewsLimit = $viewsLimit;
    $familyShare->createBy = $userId;
    $familyShare->updateBy = $userId;

    $familyShareDB = new CDBFamilyShare();
    $familyShareId = $familyShareDB->createFamilyShare($familyShare);
    
    $data['familyShareCode'] = $familyShare->shareCode;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}