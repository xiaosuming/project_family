<?php
    use Util\Util;
    use DB\CDBFamily;
    use Util\Check;
    
    $userId = $GLOBALS['userId'];
    
    $data = array();

    try{
        $familyDB = new CDBFamily();
        $result = $familyDB->getMyManageFamily($userId);
        $data['familyList'] = $result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }