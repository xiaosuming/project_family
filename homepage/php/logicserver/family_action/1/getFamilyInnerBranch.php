<?php
/**
 * 获取家族内建的分支
 * @Author: jiangpengfei
 * @Date:   2019-02-14
 */

use DB\CDBFamily;
use Util\Util;
use DB\CDBPerson;
use DB\CDBAccount;
use Util\Check;
use Model\GroupType;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $branch = $familyDB->getFamilyInnerBranch($familyId);
    $data['branch'] = $branch;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}