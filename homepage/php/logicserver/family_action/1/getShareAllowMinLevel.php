<?php
/**
 * 获取分享家族允许最小的辈分
 * @author jiangpengfei
 * @date 2018-02-05
 */

use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;

$familyId = Check::checkInteger($params['familyId'] ?? '');         //家族id

$userId = $GLOBALS['userId'];

if($familyId == ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{

    $familyDB = new CDBFamily();

    //查询最低level
    $familyMinLevel = $familyDB->getFamilyMinLevelWithUserId($familyId);
    $family = $familyDB->getFamilyById($familyId);
    if(is_null($familyMinLevel)){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "家族不存在");
        exit;
    }

    //查询用户自己的level
    $personDB = new CDBPerson();
    $userPerson = $personDB->getPersonByFamilyIdAndUserId($familyId,$userId);

    $userMinLevel = $userPerson->level - 3;
    $familyMinLevel = $familyMinLevel - 2;

    $allowMinLevel = $userMinLevel < $familyMinLevel ? $userMinLevel : $familyMinLevel;

    $data['allowMinLevel'] = $allowMinLevel;
    $data['minLevel'] = $family->minLevel;
    $data['maxLevel'] = $family->maxLevel;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}