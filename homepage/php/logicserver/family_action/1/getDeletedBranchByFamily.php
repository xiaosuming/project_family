<?php

    /**
     * 根据家族id获取已删除人物及其分支日志列表
     * author:lucky
     * date:2019-6-25
     */
    
    use Util\Util;
    use DB\CDBFamily;
    use Util\Check;
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $userId = $GLOBALS['userId'] ?? 0;
    
    try{
        $familyDB = new CDBFamily();
    
        //检查用户是否是族群管理员，或者是族群创建者
        if(!$familyDB->isAdminForFamily($familyId, $userId) && !$familyDB->isOriginatorForFamily($familyId, $userId)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }
    
        $res = $familyDB->readBranchLogs($familyId);
        $data['lists'] = $res;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }