<?php
    $version = $params['version'] ?? "1";
    $whiteList['getOpenFamilys'] = 'allow';
    $whiteList['getExportFamilyPdfPagingNoLogin'] = 'allow';
    
    require_once("filter/filter.php");
    $family_action_file_array['createFamily'] = "logicserver/family_action/{$version}/createFamily.php";
    $family_action_file_array['deleteFamily'] = "logicserver/family_action/{$version}/deleteFamily.php";
    $family_action_file_array['getFamilyInfo'] = "logicserver/family_action/{$version}/getFamilyInfo.php";
    $family_action_file_array['getFamilyInfoByIdentity'] = "logicserver/family_action/{$version}/getFamilyInfoByIdentity.php";
    $family_action_file_array['getManageFamily'] = "logicserver/family_action/{$version}/getManageFamily.php";
    $family_action_file_array['getJoinFamily'] = "logicserver/family_action/{$version}/getJoinFamily.php";
    $family_action_file_array['getJoinFamilyForTmp'] = "logicserver/family_action/{$version}/getJoinFamilyForTmp.php";
    $family_action_file_array['getJoinFamilyList'] = "logicserver/family_action/{$version}/getJoinFamilyList.php";
    $family_action_file_array['addAdmin'] = "logicserver/family_action/{$version}/addAdmin.php";
    $family_action_file_array['rescindAdmin'] = "logicserver/family_action/{$version}/rescindAdmin.php";
    $family_action_file_array['transferOriginator'] = "logicserver/family_action/{$version}/transferOriginator.php";
    $family_action_file_array['updateFamilyDescription'] = "logicserver/family_action/{$version}/updateFamilyDescription.php";
    $family_action_file_array['getFamilyOriginatorId'] = "logicserver/family_action/{$version}/getFamilyOriginatorId.php";
    $family_action_file_array['getFamilyAdminId'] = "logicserver/family_action/{$version}/getFamilyAdminId.php";
    $family_action_file_array['invitePersonToJoin'] = "logicserver/family_action/{$version}/invitePersonToJoin.php";
    $family_action_file_array['acceptInvite'] = "logicserver/family_action/{$version}/acceptInvite.php";
    $family_action_file_array['refuseInvite'] = "logicserver/family_action/{$version}/refuseInvite.php";
    $family_action_file_array['getFamilyQRCode'] = "logicserver/family_action/{$version}/getFamilyQRCode.php";
    $family_action_file_array['applyJoinFamily'] = "logicserver/family_action/{$version}/applyJoinFamily.php";
    $family_action_file_array['acceptApplyJoin'] = "logicserver/family_action/{$version}/acceptApplyJoin.php";
    $family_action_file_array['refuseApplyJoin'] = "logicserver/family_action/{$version}/refuseApplyJoin.php";
    $family_action_file_array['updateFamilyPhoto'] = "logicserver/family_action/{$version}/updateFamilyPhoto.php";
    $family_action_file_array['applyMergeFamily'] = "logicserver/family_action/{$version}/applyMergeFamily.php";
    $family_action_file_array['acceptApplyMerge'] = "logicserver/family_action/{$version}/acceptApplyMerge.php";
    $family_action_file_array['applyMergeBranch'] = "logicserver/family_action/{$version}/applyMergeBranch.php";
    $family_action_file_array['acceptApplyMergeBranch'] = "logicserver/family_action/{$version}/acceptApplyMergeBranch.php";
    $family_action_file_array['getFamilyBranch'] = "logicserver/family_action/{$version}/getFamilyBranch.php";
    $family_action_file_array['updateFamilyPermission'] = "logicserver/family_action/{$version}/updateFamilyPermission.php";
    $family_action_file_array['bindApplyUserToPerson'] = "logicserver/family_action/{$version}/bindApplyUserToPerson.php";
    $family_action_file_array['getApplyUserList'] = "logicserver/family_action/{$version}/getApplyUserList.php";
    $family_action_file_array['getApplyingUserList'] = "logicserver/family_action/{$version}/getApplyingUserList.php";
    $family_action_file_array['getFamilyStatistics'] = "logicserver/family_action/{$version}/getFamilyStatistics.php";
    $family_action_file_array['createChatGroupInFamily'] = "logicserver/family_action/{$version}/createChatGroupInFamily.php";
    $family_action_file_array['getChatGroupsByFamilyId'] = "logicserver/family_action/{$version}/getChatGroupsByFamilyId.php";
    $family_action_file_array['getChatGroupById'] = "logicserver/family_action/{$version}/getChatGroupById.php";
    $family_action_file_array['deleteChatGroupById'] = "logicserver/family_action/{$version}/deleteChatGroupById.php";
    $family_action_file_array['shareFamily'] = "logicserver/family_action/{$version}/shareFamily.php";
    $family_action_file_array['getShareAllowMinLevel'] = "logicserver/family_action/{$version}/getShareAllowMinLevel.php";
    $family_action_file_array['getFamilysInfoByMerge'] = "logicserver/family_action/{$version}/getFamilysInfoByMerge.php";
    $family_action_file_array['addFamilyPdfTask'] = "logicserver/family_action/{$version}/addFamilyPdfTask.php";
    $family_action_file_array['getPdfTaskResult'] = "logicserver/family_action/{$version}/getPdfTaskResult.php";
    $family_action_file_array['getExportFamilyPdfPaging'] = "logicserver/family_action/{$version}/getExportFamilyPdfPaging.php";
    $family_action_file_array['getOpenFamilys'] = "logicserver/family_action/{$version}/getOpenFamilys.php";
    $family_action_file_array['createRelationStudentWithTeacher'] = "logicserver/family_action/{$version}/createRelationStudentWithTeacher.php";
    $family_action_file_array['createHobbiesAndPeers'] = "logicserver/family_action/{$version}/createHobbiesAndPeers.php";
    $family_action_file_array['updateFamilyName'] = "logicserver/family_action/{$version}/updateFamilyName.php";
    $family_action_file_array['getFamilyPdfTaskDetail'] = "logicserver/family_action/{$version}/getFamilyPdfTaskDetail.php";
    $family_action_file_array['getBaseInfoByInviteCode'] = "logicserver/family_action/{$version}/getBaseInfoByInviteCode.php";
    $family_action_file_array['getTempInviteCode'] = "logicserver/family_action/{$version}/getTempInviteCode.php";
    $family_action_file_array['createFamilyInnerBranch'] = "logicserver/family_action/{$version}/createFamilyInnerBranch.php";
    $family_action_file_array['getFamilyInnerBranch'] = "logicserver/family_action/{$version}/getFamilyInnerBranch.php";
    $family_action_file_array['addInnerBranchAdmin'] = "logicserver/family_action/{$version}/addInnerBranchAdmin.php";
    
    $family_action_file_array['addBranchAdmin'] = "logicserver/family_action/{$version}/addBranchAdmin.php";
    $family_action_file_array['rescindInnerBranchAdmin'] = "logicserver/family_action/{$version}/rescindInnerBranchAdmin.php";

    $family_action_file_array['addFamilySnapshot'] = "logicserver/family_action/{$version}/addFamilySnapshot.php";
    $family_action_file_array['getRecommendForMiniFamilyBind'] = "logicserver/family_action/{$version}/getRecommendForMiniFamilyBind.php";
    $family_action_file_array['applyBindMiniFamilyToFamily'] = "logicserver/family_action/{$version}/applyBindMiniFamilyToFamily.php";
    $family_action_file_array['acceptBindMiniFamily'] = "logicserver/family_action/{$version}/acceptBindMiniFamily.php";

    $family_action_file_array['getUserFollowCircleDynamics'] = "logicserver/family_action/{$version}/getUserFollowCircleDynamics.php";
    $family_action_file_array['leaveFamily'] = "logicserver/family_action/{$version}/leaveFamily.php";
    $family_action_file_array['getRecommendForMiniFamilyUpdate'] = "logicserver/family_action/{$version}/getRecommendForMiniFamilyUpdate.php";
    $family_action_file_array['applyUpdateMiniFamilyToFamily'] = "logicserver/family_action/{$version}/applyUpdateMiniFamilyToFamily.php";
    $family_action_file_array['acceptUpdateMiniFamily'] = "logicserver/family_action/{$version}/acceptUpdateMiniFamily.php";
    $family_action_file_array['getPersonBindUserRecord'] = "logicserver/family_action/{$version}/getPersonBindUserRecord.php";
    $family_action_file_array['getAllPersonBindUserRecords'] = "logicserver/family_action/{$version}/getAllPersonBindUserRecords.php";
    $family_action_file_array['getExportFamilyPdfPagingNoLogin'] = "logicserver/family_action/{$version}/getExportFamilyPdfPagingNoLogin.php";
    $family_action_file_array['getFamilyInfoByIdWithAllUsers'] = "logicserver/family_action/{$version}/getFamilyInfoByIdWithAllUsers.php";

    
    $family_action_file_array['deleteFamilyBranchByPerson'] = "logicserver/family_action/{$version}/deleteFamilyBranchByPerson.php";
    $family_action_file_array['reSetFamilyBranchByPerson'] = "logicserver/family_action/{$version}/reSetFamilyBranchByPerson.php";
    $family_action_file_array['getDeletedBranchByFamily'] = "logicserver/family_action/{$version}/getDeletedBranchByFamily.php";
    $family_action_file_array['copyFamilyBranchByPerson'] = "logicserver/family_action/{$version}/copyFamilyBranchByPerson.php";
    $family_action_file_array['updateFamilyStartLevelById'] = "logicserver/family_action/{$version}/updateFamilyStartLevelById.php";

    #v3
    $family_action_file_array['updateFamilyInfo'] = "logicserver/family_action/{$version}/updateFamilyInfo.php";
    $family_action_file_array['getFamilyListByUser'] = "logicserver/family_action/{$version}/getFamilyListByUser.php";
    $family_action_file_array['getOriginatorFamilyList'] = "logicserver/family_action/{$version}/getOriginatorFamilyList.php";
    $family_action_file_array['getFamilyPermission'] = "logicserver/family_action/{$version}/getFamilyPermission.php";
    $family_action_file_array['applyBindPerson'] = "logicserver/family_action/{$version}/applyBindPerson.php";
    $family_action_file_array['acceptBindPerson'] = "logicserver/family_action/{$version}/acceptBindPerson.php";
    $family_action_file_array['refuseBindPerson'] = "logicserver/family_action/{$version}/refuseBindPerson.php";
    $family_action_file_array['getFamilyMergeInfo'] = "logicserver/family_action/{$version}/getFamilyMergeInfo.php";
    $family_action_file_array['cancelRefPerson'] = "logicserver/family_action/{$version}/cancelRefPerson.php";
    $family_action_file_array['deletePdfTaskHistory'] = "logicserver/family_action/{$version}/deletePdfTaskHistory.php";
    $family_action_file_array['applyBindPersonByUsername'] = "logicserver/family_action/{$version}/applyBindPersonByUsername.php";
    $family_action_file_array['getDeletedBranchPeopleByLogId'] = "logicserver/family_action/{$version}/getDeletedBranchPeopleByLogId.php";
    $family_action_file_array['refuseApplyMerge'] = "logicserver/family_action/{$version}/refuseApplyMerge.php";
    $family_action_file_array['getTwoPersonSourceByDifferentFamily'] = "logicserver/family_action/{$version}/getTwoPersonSourceByDifferentFamily.php";





