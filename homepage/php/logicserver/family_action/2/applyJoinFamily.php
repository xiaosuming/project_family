<?php
/**
 * 用户申请加入家族
 * @author jiangpengfei
 * @date 2017-08-21
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;
use Model\Message;

$userId = $GLOBALS['userId'];

$inviteUserId = Check::checkInteger($params['inviteUserId'] ?? '');  //邀请人的用户id
$identity = Check::check(trim(isset($params['identity']) ? $params['identity'] : ''));//家族id
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : '申请加入家族'),0,500);             //申请信息备注
$remark = Util::filterContentReplace($remark);

if($identity === ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $familyDB = new CDBFamily();

    $familyInfo = $familyDB->getFamilyInfoByIdentity($identity);


    if($familyInfo === null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "申请加入的家族不存在");
        exit();
    }

    $familyId = $familyInfo['id'];

    //检查该用户是否已经在家族中存在
    if($familyDB->isUserForFamily($familyId,$userId)){
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "用户已经加入该家族");
        exit();
    }


    //向所有的家族管理员都发送这条消息
    $adminsId = $familyDB->getFamilyAdminId($familyId);
    //生成随机消息码
    $messageCode = Util::generateRandomCode(36);

    $messageDB = new CDBMessage();
    foreach($adminsId as $adminId){
        $message = new Message();
        $message->fromUser = $userId;
        $message->toUser = $adminId['adminId'];
        $message->content = $remark;
        $message->module = $GLOBALS['FAMILY_MODULE'];
        $message->action = $GLOBALS['APPLYJOIN'];
        $message->recordId = $familyId;
        $message->messageCode = $messageCode;

        $extData = [
            "familyName" => $familyInfo['name'],
            "inviteUserId"=>$inviteUserId
        ];

        $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
        $messageId = $messageDB->addMessage($message);
    }

    Util::printResult($GLOBALS['ERROR_SUCCESS'], "申请成功");

}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
