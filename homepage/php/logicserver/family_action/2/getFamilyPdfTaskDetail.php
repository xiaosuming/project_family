<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-28
 * Time: 下午5:51
 */

use DB\CDBFamily;
use DB\CDBPdfTask;
use Util\Check;
use Util\Util;

$taskId = Check::checkInteger($params['taskId'] ?? '');
$userId = $GLOBALS['userId'];

try {

    $pdfDB = new CDBPdfTask();
    $result = $pdfDB->getFamilyPdfTaskDetail($taskId);
    //权限判断
    $familyDB = new CDBFamily();
    if(!$familyDB->isUserForFamily($result['familyId'], $userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $result['eventIds'] = json_decode($result['eventIds']);
    $result['photoIdOnes'] = json_decode($result['photoIdOnes']);
    $result['photoIds'] = json_decode($result['photoIds']);
    $result['graveIds'] = json_decode($result['graveIds']);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
