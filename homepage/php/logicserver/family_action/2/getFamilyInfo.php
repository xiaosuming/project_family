<?php 
    /**
     * 获取家族信息
     * author:jiangpengfei
     * date: 2017-04-24
     *
     */
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBPerson;
    use Util\Check;

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

    try{
        if($familyId!=""){
            $familyDB = new CDBFamily();
            $userId = $GLOBALS['userId'];
            $family = $familyDB->getFamilyById($familyId);

            if ($family == null) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
                exit;
            }

            if ($family->openType == 0) {
                // 私密族群
                //检查权限
                if(!$familyDB->isUserForFamily($familyId, $userId)){
                    //没有权限，则只返回基本信息
                    $newFamily['id'] = $family->id;
                    $newFamily['name'] = $family->name;
                    $newFamily['photo'] = $family->photo;
                    $data['family'] = $newFamily;
                    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                    exit;
                }
            }

            $url = 'm.izuqun.com';
            $identity = $family->identity;
            $content = "https://".$url . $GLOBALS['ROUTER_TYPE']."/family/pt-in-family/".$identity;

            $family->qrcode = $content;
            $data['family'] = $family;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }		
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }