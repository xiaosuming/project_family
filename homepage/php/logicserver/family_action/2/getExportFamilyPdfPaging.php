<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-7-2
 * Time: 下午2:31
 */

use DB\CDBFamily;
use Util\Check;
use Util\Util;




$familyId = Check::checkInteger($params['familyId'] ?? '');
$pageIndex = Check::checkInteger($params['pageIndex'] ?? 1);
$pageSize = Check::checkInteger($params['pageSize'] ?? 10);
$userId = $GLOBALS['userId'] ?? 0;



try {

    $familyDB = new CDBFamily();

    $family = $familyDB->getFamilyById($familyId);


    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }


    if ($family->openType == 0) {
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
            exit;
        }
    }

    $result = $familyDB->getExportFamilyPdfPaging($familyId, $pageIndex, $pageSize);
    $data['paging'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}