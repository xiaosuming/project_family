<?php
/**
 * 接受邀请成为家族成员 并自动关注圈子
 * author: jiangpengfei
 * date: 2017-06-12
 */
    use Util\Util;
    use DB\CDBMessage;
    use DB\CDBPerson;
    use DB\CDBAccount;
    use DB\CDBFamily;
    use Util\Check;
    use Util\PostGenerate;
    use ThirdParty\InstantMessage;
    use Model\Message;

    $messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));                  //接受的消息id
    $userId = $GLOBALS['userId'];
    $type = 4; // 1分享链接 2 二维码 3 申请 4 接受邀请

    try{
        if($messageId != ""){
            $messageDB = new CDBMessage();
            //判断message是否存在
            $message = $messageDB->getMessage($messageId);
            if($message == null){
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该邀请");
                exit;
            }else if($message->toUser != $userId){
                //检查消息的接收人是不是该用户
                Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
                exit;
            }

            //检查用户是否在家族中
            $familyDB = new CDBFamily();
            if($familyDB->isUserForFamily($message->extraRecordId,$message->toUser)){
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'],"用户已经在家族中");
                exit();
            }

            $bindPersonId = $message->recordId;     //要绑定的人物id
            $fromUserId = $message->fromUser;

            $personDB = new CDBPerson();
            //检查这个人物是否存在
            $person = $personDB->getPersonById($bindPersonId);
            if($person == null){
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '人物不存在');
                exit;
            }

            //获取圈子id
            $familyId = $person->familyId;
            $family = $familyDB->getFamilyById($familyId);
            $socialCircleId = $family->socialCircleId;
            //将用户绑定到人物并自动关注圈子
            $rowsCount = $personDB->bindUserToPersonWithPersonIdAndCircle($userId,$socialCircleId,$person,$fromUserId,$type);

            if($rowsCount > 0){
                //发送消息通知邀请的管理员
                $newMessage = new Message();
                $newMessage->fromUser = $userId;
                $newMessage->toUser = $message->fromUser;
                $newMessage->content = "邀请的用户已接受！绑定到人物[$person->name]";
                $newMessage->module = 9;
                $newMessage->action = $GLOBALS['MESSAGE_INVITE_REPLY_ACCEPT'];
                $newMessage->recordId = $messageId;    //记录Id是回复的消息id
                $newMessage->extraRecordId = $message->extraRecordId;
                $messageDB->addMessage($newMessage);

                // 聊天室不需要默认帮他加入进去
                // //根据人物id获取聊天室
                // $chatRoom = $personDB->getChatRoomByPersonId($bindPersonId);

                // //根据用户id获取环信用户名
                // $accountDB = new CDBAccount();
                // $user = $accountDB->getHuanxinInfoById($userId);
                // $hxUsername = $user['hxUsername'];

                // //将这个用户加入聊天室
                // $im = new InstantMessage();
                // $im->addUserToChatRoom($chatRoom,$hxUsername);

                //更新消息的接受状态
                $messageDB->acceptInvite($messageId);
                $data['bindPersonCount'] = $rowsCount;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "绑定失败,记录未更新");
            }

        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
