<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-2-22
 * Time: 下午2:13
 */

use DB\CDBFamily;
use DB\CDBInformation;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

$maxId = Check::checkInteger($params['maxId'] ?? 0); // 最大id
$sinceId = Check::checkInteger($params['sinceId'] ?? 0); //
$count = Check::checkInteger($params['count'] ?? 10);
$dynamicsNum = Check::checkInteger($params['dynamicsNum'] ?? 3); // 动态数量 默认3条
$userId = $GLOBALS['userId'];

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $familyDB = new CDBFamily();

    $total = $familyDB->getCountUserFollowCircles($userId);

    if (!$total) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '用户未关注圈子');
        exit;
    }

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $circleList = $familyDB->getCircleListPaging($userId, $maxId, $sinceId, $count);

    $infoDB = new CDBInformation();
    foreach ($circleList as $k=>$circle) {
        $circleId = $circle['categoryId'];
        $infoList = $infoDB->getInformationListByCategoryIdWithLimit($circleId,$dynamicsNum);
        foreach ($infoList as $infoKey=>$info){
            if ($info['isArticle'] == 0){
                $infoList[$infoKey]['photo'] = json_decode($info['photo']);
            }
        }
        $circleList[$k]['dynamics'] = $infoList;
    }

    $len = count($circleList);

    $nextSinceId = $circleList[0]['id'];

    $nextMaxId = $circleList[$len - 1]['id'];

    if ($len == 0 || $len>= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $circleList, $nextMaxId, $nextSinceId, $lastPage);

    $paginator->printPage();

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
