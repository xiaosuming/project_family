<?php

/**
 * 更新家族信息
 *  @author liuzhenhao
 *  @date 2020-07-06
 */

use DB\CDBPushFamily;
use Util\Util;
use DB\CDBFamily;
use Util\Check;

$userId = $GLOBALS['userId'];
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$familyName = Check::check(trim(isset($params['familyName']) ? $params['familyName'] : '')); //家族名字
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));
$description = Check::check(trim(isset($params['description'])) ? trim($params['description']) : null, 0, 100);

$origin_data = [
    'name'=>$familyName,
    'photo' => $photo,
    'description' => $description
];

$filter_data = [];
foreach ($origin_data as $k=>$v){
    if (($v !== '' && $v !== null) || ($k ==='description' && $v === '')){
        $filter_data[$k] = $v;
    }
}


if(!$filter_data){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "不能所有参数都为空");
    exit;
}
try{
    $familyDB = new CDBFamily();

    //检查更新权限,只有家族管理员可以修改
    if(!$familyDB->isAdminForFamily($familyId,$userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit();
    }

    $updateCount = $familyDB->updateFamilyInfo($familyId, $filter_data);
    if ($updateCount > 0) {
        if ($GLOBALS['TEST_FAMILY_TASK']) {
            $CDBPushFamily = new CDBPushFamily();
            $modelId = $familyId;
            $action = 2;
            $source = $filter_data;
            $CDBPushFamily->setPushFamilyTask($modelId, $action, $source);
        }
    }
//    $data['updateCount'] = $updateCount;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], '更新成功');
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}