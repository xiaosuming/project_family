<?php
/**
 * 获取人物在该家族权限
 * @author liuzhenhao
 * @date   2018-08-013
 */

use DB\CDBFamily;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''), 'familyId');
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    $family = $familyDB->getFamilyById($familyId);
    if (!$family) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '家族不存在');
        exit;
    }
    if ($family->openType == 0) {
        $result = $familyDB->getUserPermission($userId, $familyId, true);
        $data['permission'] = $result;
        if (!$result) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '用户无权限');
            exit;
        }
    } else {
        $data['permission'] = [
            'branchPerson' => null,
            'endIndex' => '0',
            'familyIndex' => '0',
            'level' => '0',
            'name' => '',
            'permission' => 10000, #公开家族的权限是10000
            'personId' => '0',
            'startIndex' => '0',
            'userId' => $userId
        ];
    }

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
