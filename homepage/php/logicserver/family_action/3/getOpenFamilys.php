<?php
/**
 * 获取公开的族群列表
 * @author jiangpengfei
 * @date   2018-07-05
 */

use DB\CDBFamily;
use Util\Check;
use Util\Util;

$text = Check::check(trim(isset($params['text']) ? $params['text'] : null));

try {
    $familyDB = new CDBFamily();
    $familys = $familyDB->getOpenFamilyList();

    if ($text !== null && $text !== '') {

        $data_filter = array_filter($familys, function ($value) {
            global $text;
            if (strpos($value['id'], $text) === false && strpos($value['name'], $text) === false) {
                return false;
            }
            return true;
        });

        $result['familyList'] = array_values($data_filter);

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
        return;
    }

    $data['familyList'] = $familys;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
