<?php
/**
 * 修改权限
 * @author liuzhenhao
 * @date 2020-08-14
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBAccount;
use DB\CDBMessage;
use Model\Message;

$permission = Check::checkInteger(isset($params['permission']) ? trim($params['permission']) : '','permission');
$toUser = Check::checkInteger(trim(isset($params['toUser']) ? $params['toUser'] : ''),'toUser');
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''),'familyId');
//$startIndex = Check::checkInteger(trim(isset($params['startIndex']) ? $params['startIndex'] : -1),'startIndex');
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : '修改权限'), 0, 500);
$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : 0),'personId');
$userId = $GLOBALS['userId'];

if (!in_array($permission,['200','300','400']) || ($permission === '300' && $personId == 0)){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}


try {
    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();
    $accountDB = new CDBAccount();

    if ($userId == $toUser){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '修改者不能与被修改者相同');
        exit;
    }

    $family = $familyDB->getFamilyById($familyId);

    if (!$family){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '家族不存在');
        exit;
    }

    $person = $personDB->getPersonV3($personId);
    if ($permission == '300' && !$person){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '人物不存在');
        exit;
    }

    $userPermission = $familyDB->getUserPermission($userId, $familyId);
    $toUserPermission = $familyDB->getUserPermission($toUser, $familyId, true,false);

    $userBranchPerson = $userPermission['branchPerson'] ? $personDB->getPersonSimpleInfoById($userPermission['branchPerson']) : null;
    $toUserBranchPerson = $toUserPermission['branchPerson'] ? $personDB->getPersonSimpleInfoById($toUserPermission['branchPerson']) : null;
//    $beChangeBranchPerson = $personId ? $personDB->getPersonSimpleInfoById($personId) : null;

    if (!$userPermission || !$toUserPermission){
        print_r($toUserPermission);
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }

    $userPermissionNum = Util::checkPermission($userPermission, [$userBranchPerson->familyIndex]); #修改者的权限
    $toUserPermissionNum = Util::checkPermission($toUserPermission, [$toUserBranchPerson->familyIndex]); #被修改者的权限
    $beChangePermissionNum = ['400'=>4, '300'=>3, '200'=>2][$permission]; #被修改成的权限

    if ($toUserPermissionNum >= $userPermissionNum || $userPermissionNum < 3 || $beChangePermissionNum >= $userPermissionNum){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }

//    if ($toUserPermission['permission'] >= $userPermission['permission']  &&  #判断修改者的权限是否满足
//        ($userPermission['permission'] === 300 && ($toUserPermission['familyIndex'] < $userPermission['startIndex'] || #判断如果是分支管理，familyIndex是否在管理区间内
//        $toUserPermission['familyIndex'] > $userPermission['endIndex'])) &&
//        $userPermission['permission'] <= intval($permission) && #判断要修改的权限是否超出管理员的权限
//        !in_array($userPermission['permission'], [300,400,500]))
//    {
//        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
//        exit;
//    }


    $updatePermission = $familyDB->modifyPermission($familyId,$permission,$toUser,0,0,$userId, $personId);

    //生成随机消息码
    $messageCode = Util::generateRandomCode(36);

    $messageDB = new CDBMessage();
    foreach ([$toUserPermission] as $adminId) {
        $message = new Message();
        $message->fromUser = $userId;
        $message->toUser = $adminId['userId'];
        $message->content = $remark;
        $message->module = $GLOBALS['FAMILY_MODULE'];
        $message->action = $GLOBALS['ADDADMIN'];
        $message->recordId = $familyId;
        $message->messageCode = $messageCode;

        $extAction = $permission >= $toUserPermission['permission'] ? 1:0;
        $extData = [
            "level" => $person ? $person['level']:'',
            "name" => $person ? $person['name']:'',
            "familyName" =>$family->name,
            "minLevel" => $family->minLevel,
            "maxLevel" => $family->maxLevel,
            "action" => $extAction,
            "permission" => $extAction ? $permission:$toUserPermission['permission']
        ];

        $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
        $messageId = $messageDB->addMessage($message);
    }

    Util::printResult($GLOBALS['ERROR_SUCCESS'], ["accept"=>true]);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
