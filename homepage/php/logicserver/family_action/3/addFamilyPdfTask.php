<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-28
 * Time: 下午5:51
 */

use DB\CDBFamily;
use DB\CDBPdfTask;
use DB\CDBAccount;
use Util\Check;
use Util\Util;
use Util\Pinyin;


/**
 * 添加带出pdf任务。每一个任务都会有一个编号。添加任务时会获取编号。用户之后根据编号取回导出的结果
 * @author jiangpengfei
 * @date   2018-06-13
 */
$familyId = Check::checkInteger($params['familyId'] ?? '');
$familyTreeName = Check::check($params['familyTreeName'] ?? '');        // 分支名字
$name = Check::check($params['name'] ?? '');        // 总族谱名字
$direction = Check::checkInteger($params['direction'] ?? 1);         // 默认从右往左
$eventIds = Check::check(trim($params['eventIds'] ?? ''));         // 大事件id集合
$photoIdOnes = Check::check(trim($params['photoIdOnes'] ?? ''));           // 相册中的图片id集合 一页一张
$photoIds = Check::check(trim($params['photoIds'] ?? ''));           // 相册中的图片id集合 一页两张
$graveIds = Check::check(trim($params['graveIds'] ?? ''));           // 宗祠id集合
$startPersonId = Check::checkInteger(trim($params['startPersonId'] ?? '0'));
$isHD = Check::checkInteger(trim($params['isHD'] ?? '0'));
$options = trim($params['options'] ?? '');
$userId = $GLOBALS['userId'];

try {
    //权限判断
    $familyDB = new CDBFamily();
    if (!$familyDB->isAdminForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $accountDB = new CDBAccount();
    $userInfo = $accountDB->getUserInfoV3($userId);
    if ($userInfo['pdfPermission'] != 1){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户无权限");
        exit;
    }

    if ($eventIds == '') {
        $jsonEventIds = json_encode(array());
    } else {
        $eventIdsArr = explode(',', $eventIds);
        $eventIdsArr = $familyDB->checkEventIdIsExists($eventIdsArr, $familyId);
        $eventIdsArr = array_column($eventIdsArr,'id');
        $jsonEventIds = json_encode($eventIdsArr);
    }

    if ($photoIdOnes == '') {
        $jsonPhotoIdOnes = json_encode(array());
        $photoSum1 = 0;
    } else {
        $photoIdOnesArr = explode(',', $photoIdOnes);
        $photoIdOnesArr = $familyDB->checkPhotoIdIsExists($photoIdOnesArr, $familyId);
        $photoIdOnesArr = array_column($photoIdOnesArr,'id');
        $jsonPhotoIdOnes = json_encode($photoIdOnesArr);
        $photoSum1 = count($photoIdOnesArr);
    }

    if ($photoIds == '') {
        $jsonPhotoIds = json_encode(array());
        $photoSum2 = 0;
    } else {
        $photoIdsArr = explode(',', $photoIds);
        $photoIdsArr = $familyDB->checkPhotoIdIsExists($photoIdsArr, $familyId);
        $photoIdsArr = array_column($photoIdsArr,'id');
        $jsonPhotoIds = json_encode($photoIdsArr);
        $photoSum2 = count($photoIdsArr);
    }

    if ($graveIds == '') {
        $jsonGraveIds = json_encode(array());
    } else {
        $graveIdsArr = explode(',', $graveIds);
        $graveIdsArr = $familyDB->checkGraveIdIsExists($graveIdsArr, $familyId);
        $graveIdsArr = array_column($graveIdsArr,'id');
        $jsonGraveIds = json_encode($graveIdsArr);
    }

    if ($photoSum1 + $photoSum2 > 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '图片数量不能超过50张');
        exit;
    }


    //验证参数
    $check_options = json_decode($options, true);
    if(!isset($check_options['surname']) || !isset($check_options['coverId'])){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "姓氏或封面为空"); //-20000018
        exit;
    }
    if(mb_strlen($check_options['surname']) > 2){ //最长2个汉字
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "姓氏长度错误"); //-20000018
        exit;
    }
    if(intval($check_options['coverId']) <= 0){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "未选择封面");
        exit;
    }

    if($check_options['coverId'] == 2 ){//如果是白色封面
        if(isset($check_options['pinyin']) && strlen($check_options['pinyin']) > 0)
        {
            if(strlen($check_options['pinyin']) > 12){ //最长2个汉字的拼音
                Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "拼音长度错误");
                exit;
            }
    
        }else{
            $py = new Pinyin();
            $check_options['pinyin'] = $py->getpy($check_options['surname'], true);
            if(strlen($check_options['pinyin']) == 0){
                Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "可能是个生僻字，系统无法识别文字, 请手动输入拼音");
               exit;
            }
        }
    }
    
    $options = json_encode($check_options, JSON_UNESCAPED_UNICODE);
    $pdfDB = new CDBPdfTask();
    $taskId = $pdfDB->addFamilyPdfTask($userId, $familyId, $name,$familyTreeName,$direction, 
    $jsonEventIds, $jsonPhotoIdOnes, $jsonPhotoIds, $jsonGraveIds, $startPersonId,$isHD, $options);

    if ($taskId > 0) {
        $data['taskId'] = $taskId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
