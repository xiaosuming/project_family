<?php
use Util\Util;
use DB\CDBFamily;
use Util\Check;

$userId = $GLOBALS['userId'];
$data = [];
$text = Check::check(trim(isset($params['text'])? $params['text']: null));

try{
    $familyDB = new CDBFamily();
    $originatorFamilyList = $familyDB->getOriginatorFamilyList($userId);

    if ($text !== null && $text !== ''){

        $data_filter = array_filter($originatorFamilyList,function ($value){
            global $text;
            if (strpos($value['id'], $text) === false && strpos($value['name'], $text) === false){
                return false;
            }
            return true;
        });

        $result['familyList'] = array_values($data_filter);

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
        return;
    }

    $data['familyList'] = $originatorFamilyList;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}