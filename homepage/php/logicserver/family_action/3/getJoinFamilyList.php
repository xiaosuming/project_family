 <?php

use Util\Util;
use DB\CDBFamily;
use Util\Check;

$userId = $GLOBALS['userId'];
$permission =  Check::checkInteger(trim(isset($params['permission']) ? $params['permission'] : 0));
$text = Check::check(trim(isset($params['text'])? $params['text']: null));

try {
    $familyDB = new CDBFamily();
//    $joinFamilyList = $familyDB->getJoinFamilyListV3($userId);
//    $joinFamilyTempList = $familyDB->getJoinFamilyForTempV3($userId);
    $permission = " < 500 ";
    $data = $familyDB->getFamilyListByPermission($userId,$permission ? $permission:"",'join');

//    $data = array_merge($joinFamilyList,$joinFamilyTempList);

    # 按时间排序，现在也不需要了
//    usort($data,function($a,$b){
//        if ($a['createTime'] === $b['createTime']){
//            return 0;
//        }
//        return $a['createTime'] > $b['createTime'] ? -1:1;
//    });


    #为什么以下这段筛选的代码呢，当然是你猜了，猜对了就告诉你
    if ($text !== null && $text !== ''){

        $data_filter = array_filter($data,function ($value){
            global $text;
            if (strpos($value['id'], $text) === false && strpos($value['name'], $text) === false){
                return false;
            }
            return true;
        });

        $result['familyList'] = array_values($data_filter);

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
        return;
    }
    $result['familyList'] = $data;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}