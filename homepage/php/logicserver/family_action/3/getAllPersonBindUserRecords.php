<?php
/**
 * 获取所有的用户分享绑定的记录
 * @author: jiangpengfei
 * @date:   2019-03-26
 */

use Util\Util;
use Util\Check;
use DB\CDBFamily;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    // 判断用户是不是族群中的
    $familyDB = new CDBFamily();
    if (!$familyDB->getUserPermission($userId, $familyId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $records = $familyDB->getAllPersonBindUserRecord($familyId);

    $data['records'] = $records;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
