<?php
/**
 * 用户通过微信获取家族的二维码，用户可以通过扫描二维码加入家族
 * author: 刘振豪
 * date: 2020-07-09
 *
 */

use Util\Util;
use Util\Check;
use DB\CDBFamily;
use DB\CDBPerson;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId']:''));
$url = Check::check(trim(isset($params['url']) ? $params['url'] : ''));
$userId = $GLOBALS['userId'];

// 定死url

$url = 'm.izuqun.com';

if($familyId == "" || $url == ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $familyDB = new CDBFamily();
    //检查权限
    if(!$familyDB->isUserForFamily($familyId, $userId) && !$familyDB->isOriginatorForFamily($familyId,$userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $qrCode = $familyDB->getFamilyQRCode($familyId);
    if($qrCode === null || $qrCode['qrcode'] === null || $qrCode['qrcode'] === "" || $qrCode['qrcode'] < 0){
        $identity = $familyDB->getFamilyIdentity($familyId);
        if($identity != null){
            //生成二维码
            $content = "https://".$url . $GLOBALS['ROUTER_TYPE']."/family/pt-in-family/".$identity;

            $qrPath = "";
            $retryTime = 0;
            while($qrPath === "" && $retryTime <= 3){
                $qrPath = Util::generateQRCode($content);
                if(is_numeric($qrPath) && $qrPath < 0){
                     //记录错误日志
                     $logger->error("生成二维码出错",array($qrPath));
                     $qrPath = "";
                     $retryTime++;   //重试次数+1
                     continue;       //重试
                }
                //更新二维码
                $familyDB->updateFamilyQRCode($familyId,$qrPath);
                $data['qrcode'] = $qrPath;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                exit();
            }
            
            Util::printResult($GLOBALS['ERROR_EXCEPTION'], "二维码生成出现异常，请重试");

        }else{
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "查询错误");	
            exit();
        }
    }else{
        $data['qrcode'] = $qrCode['qrcode'];
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
    }
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}