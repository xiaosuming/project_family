<?php
/**
 * Created by PhpStorm.
 * User: liuzhenhao
 * Date: 2020/08/06 0027
 * Time: 10:26
 */

use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBMessage;
use Model\Message;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];             //操作者的用户Id
$applyUserId = check::checkInteger(trim(isset($params['applyUserId']) ? $params['applyUserId'] : '')); //申请的用户Id
$personId = check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : '')); //绑定人物的人物Id

if ($applyUserId == '' || $personId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}

try {
    $personDB = new CDBPerson();
    //检查这个人物是否存在
    $person = $personDB->getPersonById($personId);
    if($person == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '人物不存在');
        exit;
    }

    $familyId = $person->familyId;

    $familyDB = new CDBFamily();
    $family = $familyDB->getFamilyById($familyId);
    if($family == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '家族不存在');
        exit;
    }

    //检查操作用户是否是家族成员
    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $permission = $familyDB->getUserPermission($userId,$familyId);
    $applyPermission = $familyDB->getUserPermission($applyUserId,$familyId,true);

    //检查用户是否是家族创始人或管理员
    if ($familyDB->isOriginatorForFamily($familyId, $userId) || $familyDB->isAdminForFamily($familyId, $userId)) {

        //检查要绑定的用户是否是申请列表中的用户
        if ($applyPermission['personId']) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '绑定的用户不在申请列表中');
            exit;
        }
        //绑定人物和用户

        $count = $personDB->bindUserToPersonWithPersonIdV3($applyUserId, $person,$permission,$userId);

        if ($count == '-1') {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '用户已绑定人物');
            exit;
        }

        if ($count > 0) {
            //发送消息通知申请的用户
            $newMessage = new Message();
            $newMessage->fromUser = $userId;
            $newMessage->toUser = $applyUserId;
            $newMessage->content = "[$family->name]：绑定到人物[$person->name]成功";
            $newMessage->module = $GLOBALS['FAMILY_MODULE'];
            $newMessage->action = $GLOBALS['USER_PERSON_BIND'];
            $newMessage->recordId = $personId;    //记录Id是绑定人物的id
            $newMessage->extraRecordId = $familyId; //家族id

            $messageDB = new CDBMessage();
            $messageDB->addMessage($newMessage);

            $data['updateCount'] = $count;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        }
    }
    Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
    exit;

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
