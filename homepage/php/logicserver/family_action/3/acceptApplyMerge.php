<?php
/**
 * 接受用户申请合并家族
 * @author  yuanxin
 * @version 3
 * @date 2020/08/31
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBMessage;
use Model\Message;

$messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));                  //接受的消息id

$toPersonId = Check::checkInteger(trim($params['toPersonId'] ?? ''));
$userId = $GLOBALS['userId'];
if($messageId === ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $messageDB = new CDBMessage();

    //判断message是否存在
    $message = $messageDB->getMessage($messageId);
    if($message == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该消息");
        exit;
    }else if($message->toUser != $userId){
        //检查消息的接收人是不是该用户
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
        exit;
    }

    //根据toPersonId获取familyId
    $personDB = new CDBPerson();
    $person = $personDB->getPersonById($toPersonId);

    if($person == null){
        //人物不存在
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
        exit;
    }

    $familyDB = new CDBFamily();
    //检查创始人权限
    if(!$familyDB->isOriginatorForFamily($person->familyId,$userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限错误");
        exit();
    }

    $updateRecord = $messageDB->acceptMergeFamily($message,$toPersonId);
    
    if($updateRecord > 0){
        $actionDict = [
            $GLOBALS['MERGE_FAMILY_BY_MARRIAGE']=>$GLOBALS['REPLY_MERGE_FAMILY_BY_MARRIAGE'],
            $GLOBALS['MERGE_FAMILY_BY_PERSON']=>$GLOBALS['REPLY_MERGE_FAMILY_BY_PERSON'],
            $GLOBALS['MERGE_FAMILYS_TO_NEW_FAMILY']=>$GLOBALS['REPLY_MERGE_FAMILYS_TO_NEW_FAMILY']
        ];
        //发送消息通知申请的管理员
        $newMessage = new Message();
        $newMessage->fromUser = $userId;
        $newMessage->toUser = $message->fromUser;
        $newMessage->content = "家族合并申请已完成！";
        $newMessage->module = 9;
        $newMessage->action = $actionDict[$message->action];
        $newMessage->recordId = $messageId;    //记录Id是回复的消息id

        $oldMessage  =  $messageDB->getMessage($messageId);
        $extData  = json_decode($oldMessage->ext,true);
        $extData['toPersnId'] = $toPersonId;
        $newMessage->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);

        $messageDB->addMessage($newMessage);

        $data['updateRecord'] = $updateRecord;
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        exit();
    }else{
        $errorMsg = null;
        switch($updateRecord){
            case 0:
                $errorMsg = "没有更新任何记录";
                break;
            case -1:
                $errorMsg = "人物不存在";
                break;
            case -2:
                $errorMsg = "双方人物是同一个家族的";
                break;
            case -3:
                $errorMsg = "该人物已绑定合并的家族，请解绑后重试";
                break;
            case -4:
                $errorMsg = '不存在的合并方式';
                break;
            case -5:
                $errorMsg = '数据库执行出错';
                break;
            default:
                $errorMsg = "未知错误";
        }
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'],$errorMsg);
        exit();
    }
}catch(PDOException $e){
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
