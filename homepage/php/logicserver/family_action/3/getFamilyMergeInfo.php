<?php
/**
 * 获取关联合并后的家族信息
 * @author  yuanxin
 * @version 3
 * @date 2020/09/02
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;

$familyId = Check::check(isset($params['familyId']) ? $params['familyId'] : '');             //家族id

$userId = $GLOBALS['userId'];


if($familyId === ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    
    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();
    $family = $familyDB->getFamilyById($familyId);
    $minLevel = $family->minLevel;
    if($family === null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "申请合并的家族不存在");
        exit();
    }

    $result = $familyDB->getFamilyMergeInfo($familyId);
    $data = array();
    foreach($result as $key => $family){
        if($familyId == $family['fromFamilyId']){
            $person =  $personDB->getPersonById($family['fromPersonId']);
            $data[$key]['status']  =  "from";
            $data[$key]['personId']  =  $family['fromPersonId'];          
            $data[$key]['familyId']  =  $family['toFamilyId'];

        }else{
            $person = $personDB->getPersonById($family['toPersonId']);
            $data[$key]['status']  =  "to";
            $data[$key]['personId']  =  $family['toPersonId'];
            $data[$key]['familyId']  =  $person->familyId;
            $data[$key]['familyId']  =  $family['fromFamilyId'];
        }
        // $data[$key]['level']  =   Util::number2chinese($person->level - $minLevel);
        $data[$key]['level']  =  $person->level - $minLevel;
        $data[$key]['name']  =  $person->name;
        $data[$key]['mergeWay']  =  $family['mergeWay'];
        $data[$key]['id']  =  $family['id'];

    }

   

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

}catch(PDOException $e){
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
