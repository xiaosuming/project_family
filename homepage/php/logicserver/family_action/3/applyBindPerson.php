<?php


/**
 * 用户申请加入家族或管理员邀请绑定
 * @author liuzhenhao
 * @date 2020-07-14
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBAccount;
use DB\CDBMessage;
use Model\Message;
use Model\Person;

$userId = $GLOBALS['userId'];

//$inviteUserId = Check::checkInteger($params['inviteUserId'] ?? '');  //邀请人的用户id
$identity = Check::check(trim(isset($params['identity']) ? $params['identity'] : ''));//家族id
$personId = Check::checkInteger(isset($params['personId']) ? trim($params['personId']) : '','personId');
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
$fatherName = Check::check(trim(isset($params['fatherName']) ? $params['fatherName'] : ''));
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : '申请绑定人物'), 0, 500);
$bindUser = Check::checkInteger(isset($params['bindUser']) ? trim($params['bindUser']) : '','bindUser');
$userId = $GLOBALS['userId'];
//申请信息备注
//$remark = Util::filterContentReplace($remark);

if ($identity === "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();
    $accountDB = new CDBAccount();

    $familyInfo = $familyDB->getFamilyInfoByIdentity($identity);


    if ($familyInfo === null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "家族不存在");
        exit();
    }

    $person = $personDB->getPersonV3($personId);
    $father = $personDB->getPersonV3($personId['father']);

    if (!$person){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "该人物不存在");
        exit();
    }

    if ($person['userId']){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "该人物已绑定");
        exit();
    }

    $user = $accountDB->getUserInfo($bindUser);

    if (!$user){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "该用户不存在");
        exit();
    }

    $familyId = $familyInfo['id'];

    if ($bindUser === $userId){
        # 被绑定者为自己的情况，就是自己申请的情况

        //检查该用户是否已经在家族中存在
        $userPermission = $familyDB->getUserPermission($bindUser, $familyId,true);

        if (!$userPermission) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "用户未加入该家族");
            exit();
        }

        if ($userPermission['personId'] !== null) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "该用户已绑定");
            exit();
        }

        #判断自己的权限，如果自己是管理员或者分支管理员自己管辖的区域，可以直接进行绑定
        if ($userPermission['personId'] === null &&
            (in_array($userPermission['permission'],[400,500]) ||
                ($userPermission['permission'] === 300 && ($person['familyIndex'] >= $userPermission['startIndex'] || $person['familyIndex'] <= $permission['endIndex'])))
        )
        {
            $acceptBindResult = $personDB->bindUserToPersonWithPersonId($bindUser,new Person($person));
            if ($acceptBindResult > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], "绑定成功");
                exit();
            }
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "绑定失败");
            exit;
        }


        //向所有的家族管理员都发送这条消息
        $chiefAdminsId = $familyDB->getFamilyChiefAdminId($familyId);
        $branchAdminsId = $familyDB->getFamilyBranchAdminId($familyId, $person['familyIndex'],$person['level']);
        $adminsId = array_merge($chiefAdminsId, $branchAdminsId);
        //生成随机消息码
        $messageCode = Util::generateRandomCode(36);

        $messageDB = new CDBMessage();
        foreach ($adminsId as $adminId) {
            $message = new Message();
            $message->fromUser = $userId;
            $message->toUser = $adminId['userId'];
            $message->content = $remark;
            $message->module = $GLOBALS['FAMILY_MODULE'];
            $message->action = $GLOBALS['USER_PERSON_BIND'];
            $message->recordId = $familyId;
            $message->messageCode = $messageCode;

            $extData = [
                "inviteUserId" => 0,
                "username" => $user['username'],
                'familyId' => $familyId,
                "familyName" => $familyInfo['name'],
                "applyUserId" => $userId,
                "personId" => $person['id'],
                "name" => $name,
                "personLevel" => $person['level'],
                "fatherName" => $fatherName,
                "fatherId" => $father ? $father['id']:0,
                "fatherisBind" => $father ? intval(boolval($father['userId'])):0
            ];

            $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
            $messageId = $messageDB->addMessage($message);
        }

        Util::printResult($GLOBALS['ERROR_SUCCESS'], "申请成功");
    }else{
        # 管理申请绑定人物

        $permission = $familyDB->getUserPermission($userId, $familyId,true);

        if (!$permission) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "权限错误");
            exit();
        }

        //检查管理员权限 条件一 判断是否创建者或者管理员， 条件二 判断绑定人物的familyIndex是否在分支管理员的管辖范围
//        if (!in_array($permission['permission'],[400,500]) &&
//            ($permission['permission'] === 300 && ($person['familyIndex'] < $permission['startIndex'] || $person['familyIndex'] > $permission['endIndex'])))
        if (Util::checkPermission($permission,[$person['familyIndex']]) < 3)
        {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit();
        }


        //检查被用户是否已经在家族中存在
        $userPermission = $familyDB->getUserPermission($bindUser, $familyId,true);

        if (!$userPermission) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "用户未加入该家族");
            exit();
        }

        if ($userPermission['personId'] !== null) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "该用户已绑定");
            exit();
        }

        //生成随机消息码
        $messageCode = Util::generateRandomCode(36);

        //管理员发送消息通知未绑定的用户
        $messageDB = new CDBMessage();
        $newMessage = new Message();
        $newMessage->fromUser = $userId;
        $newMessage->toUser = $bindUser;
        $newMessage->content = "邀请绑定人物！";
        $newMessage->module = 9;
        $newMessage->action = $GLOBALS['INVITE_TO_BIND'];
        $newMessage->recordId = $familyId;    //记录Id是回复的消息id
        $newMessage->messageCode = $messageCode;
//        $newMessage->extraRecordId = $familyId;

        $extData = [
            "familyName" => $familyInfo['name'],
            "inviteUserId" => $userId,
            "name" => $person['name'],
            "fatherName" => $person['fatherName'],
            'level' => $person['level'],
            "invitePermission" => $permission['permission'],
            "inviteName" => $permission['name'],
            "personId" => $personId
        ];
        $newMessage->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
        $messageDB->addMessage($newMessage);

        Util::printResult($GLOBALS['ERROR_SUCCESS'], "申请成功");
    }



} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
