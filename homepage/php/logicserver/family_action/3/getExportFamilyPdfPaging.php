<?php
/**
 * Created by PhpStorm.
 * User: liuzhenhao
 * Date: 20-08-30
 * Time: 下午2:31
 */

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$pageIndex = Check::checkInteger($params['pageIndex'] ?? 1);
$pageSize = Check::checkInteger($params['pageSize'] ?? 10);
$userId = $GLOBALS['userId'] ?? 0;

try {

    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();

    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    if ($family->openType == 0) {
        if (!$familyDB->getUserPermission($userId,$familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
            exit;
        }
    }

    $result = $familyDB->getExportFamilyPdfPaging($familyId, $pageIndex, $pageSize);
    $data = array();
    // $options  = json_decode($result['option'],true);
    foreach($result as  $key => $value){
        $options  = json_decode($value['options'],true);
        $data[$key]['coverId'] = $options['coverId'];
        $data[$key]['options'] = $options;
        $data[$key]['familyTreeName'] = $value['familyTreeName'];
        $data[$key]['startPersonId'] = $value['startPersonId'];
        if($value['startPersonId']>0){
            $person = $personDB->getPersonById2($value['startPersonId']);
            $data[$key]['familyName'] = $person->name;            
        }else{
            $data[$key]['familyName'] = '';            
        }
        $data[$key]['familyId'] = $value['familyId'];
        $data[$key]['id'] = $value['id'];
        $data[$key]['familyName'] = $value['familyName'];
        $data[$key]['previewPdf'] = $value['preview_pdf'];
        $data[$key]['printPdf'] = $value['print_pdf'];
        $data[$key]['createTime'] = $value['create_time'];
    }
 
   
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
