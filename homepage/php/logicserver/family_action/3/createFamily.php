<?php
/**
 * 创建家族
 * author:jiangpengfei
 * date:2017-04-26
 */

use DB\CDBPushFamily;
use DB\CDBPushPerson;
use Util\SysLogger;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBAccount;
use Util\Check;
use Util\PhotoGenerator;
use ThirdParty\InstantMessage;
use Util\HttpContext;

//$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''), 1, 30);                           //家族管理员的姓名
//$gender = Check::checkGender(trim(isset($params['gender']) ? $params['gender'] : ''));                     //家族管理员的性别
//$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));                         //家族管理员的住址
//$birthday = Check::check(trim(isset($params['birthday']) ? $params['birthday'] : ''));                      //家族管理员的生日
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));                               //家族管理员的照片
$familyPhoto = Check::check(trim(isset($params['familyPhoto']) ? $params['familyPhoto'] : ''));             //家族照片
$description = Check::check(trim(isset($params['description']) ? $params['description'] : ''), 0, 100);             //家族描述
$familyName = Check::check(trim(isset($params['familyName']) ? $params['familyName'] : ''), 1, 14);                //家族姓名
$surname = Check::check(trim(isset($params['surname']) ? $params['surname'] : ''),1, 14);                         //姓氏
$groupType = Check::checkInteger($params['groupType'] ?? 1);        // 群体类型，默认是1，1是家族，2是家庭，3是师生，4是师徒 5是公司同事
$data = array();
//$groupType = 1;

$userId = $GLOBALS['userId'];
try {

    if ($familyPhoto == '') {
        $familyPhoto = PhotoGenerator::generateFamilyPhotoFromResource();
    }

    if ($photo == '') {
        $photo = PhotoGenerator::generatePersonPhotoFromResource();
    }

    if ($familyName != "") {

        if ($gender != 0 && $gender != 1) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "用户性别选择不正确");
            exit;
        }

        $familyDB = new CDBFamily();

        if ($surname == "") {
            $surname = mb_substr($name, 0, 1);
        }
        

        list($familyId, $personId, $socialCircleId) = $familyDB->createFamilyAndCircleV3($GLOBALS['userId'], $familyName, $surname, $description, $familyPhoto, $photo, '', $groupType);

        if (!HttpContext::getInstance()->getEnv('private_db_host')) {
            // 族群索引到ES中
            $CDBPushFamily = new CDBPushFamily();
            $modelId = $familyId;
            $action = 1;
            $source = [
                'familyId' => $familyId,
                'name' => $familyName,
                'photo' => $photo,
                'description' => $description
            ];
            $CDBPushFamily->setPushFamilyTask($modelId, $action, $source);

            // 人物索引到ES中
            $personDB = new CDBPerson();
            $person = $personDB->getPersonById($personId);
            $CDBPushPerson = new CDBPushPerson();
            $modelId = $personId;
            $action = 1;
            $source = $person;
            $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
        }


        $data['familyId'] = $familyId;
        $data['socialCircleId'] = $socialCircleId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
