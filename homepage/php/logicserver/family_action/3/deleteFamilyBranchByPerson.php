<?php

    /**
     * 根据家族id和人物id删除人物及其分支
     * author:liuzhenhao
     * date:2020-8-28
     */


    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBPerson;
    use Util\Check;

    $personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

try {

    $userId = $GLOBALS['userId'];
    if ($personId > 0) {

        $familyDB = new CDBFamily();
        //判断被删除的personId是否在家族中
        if (!$familyDB->verifyPersonIdAndFamilyId($personId, $familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户与家族id不匹配或者用户不存在");
            exit;
        }
        $personDB = new CDBPerson();
        $person = $personDB->getPersonObjectById($personId);
        $family = $familyDB->getFamilyById($familyId);

        if (!$family || $family->minLevel == $person->level) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "不能从顶点删除");
            exit;
        }
        //检查用户权限
        $permission = $familyDB->getUserPermission($userId, $familyId, true);
        if (Util::checkPermission($permission, [$person->familyIndex]) < 3 || $permission['branchPerson'] == $personId) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }

        $res = $familyDB->deleteBranchV3($personId, $familyId, $userId);

        Util::printResult($res['error_code'], $res['error_msg']);

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "personId 不正确");
        exit;
    }
}catch (PDOException $e) {
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
