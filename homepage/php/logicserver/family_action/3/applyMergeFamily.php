<?php
/**
 * 用户申请合并家族
 * @author  yuanxin
 * @version 3
 * @date 2020/08/31
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;
use DB\CDBPerson;

$userId = $GLOBALS['userId'];

$fromFamilyId = Check::check(isset($params['fromFamilyId']) ? $params['fromFamilyId'] : '');             //家族合并的请求方id
$toFamilyId = Check::check(isset($params['toFamilyId']) ? $params['toFamilyId'] : '');                 //被申请的家族id
$mergeType = Check::checkInteger($params['mergeType'] ?? '');                                           //合并类型，1是联姻合并，2是通过同一个人合并  3.合并生成新家族
$remark = Check::check(isset($params['remark']) ? $params['remark'] : '申请合并家族');             //申请信息备注
$fromPersonId = Check::check(isset($params['fromPersonId']) ? $params['fromPersonId'] : '');               //申请方的合并起始人物

if($fromFamilyId === "" || $toFamilyId === "" || $fromPersonId === "" || $mergeType === ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{


    $familyDB = new CDBFamily();
    $fromFamily = $familyDB->getFamilyById($fromFamilyId);

    if($fromFamily === null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "申请合并的家族不存在");
        exit();
    }

    //检查该用户是否是家族创始人
    if($fromFamily->originator != $userId){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足，需要创始人权限");
        exit();
    }

    //检查请求方人物是否存在，是否和fromFamily对应，是否已经绑定了家族
    $personDB = new CDBPerson();
    $person = $personDB->getPersonById($fromPersonId);

    if($person == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "人物不存在");
        exit();
    }

    if($person->familyId != $fromFamilyId){
        //人物不在家族中
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "人物不在家族中");
        exit();
    }


    if($person->refFamilyId != array()){
        //检查人物绑定的族群中有没有当前族群
        foreach($person->refFamilyId as $checkFamilyId ) {
            if ($toFamilyId == $checkFamilyId) {
                //家族已经存在，直接退出
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物已经绑定了当前家族");
                exit();
            }
        }
    }

    //向被请求方发送消息
    $messageDB = new CDBMessage();
    $messageId = $messageDB->sendMergeFamilyMessage($fromFamilyId,$toFamilyId,$fromPersonId,$userId,$remark,$mergeType);
    if($messageId == -1){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "申请合并的家族不存在");
        exit();
    }

    $data['messageId'] = $messageId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}