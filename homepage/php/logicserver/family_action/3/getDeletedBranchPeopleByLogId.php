<?php

    /**
     * 根据家族id获取已删除人物及其分支日志列表
     * author:liuzhenhao
     * date:2020-8-30
     */
    
    use Util\Util;
    use DB\CDBFamily;
    use Util\Check;
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $logId = Check::checkInteger(trim(isset($params['logId']) ? $params['logId'] : ''));
    $userId = $GLOBALS['userId'] ?? 0;
    $page = Check::checkInteger(trim(isset($params['page']) ? $params['page'] : 1));
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 20));


    if ($pageSize > 99){
        $pageSize = 99;
    }

    
    try{
        $familyDB = new CDBFamily();

        //检查用户权限
        if(Util::checkPermission($familyDB->getUserPermission($userId,$familyId),[],false) < 1){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }
    
        $res = $familyDB->readPeopleByBranchId($familyId,$logId,$page,$pageSize);
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $res);

    }catch(PDOException $e){
        //异常处理
//        print_r($e);
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }