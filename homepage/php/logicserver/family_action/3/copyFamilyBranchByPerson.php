<?php
ini_set('memory_limit', '512M');

/**
 * 复制家族分支
 *
 *
 */

use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;

$newFamilyId = Check::checkInteger(trim(isset($params['newFamilyId']) ? $params['newFamilyId'] : ''));
$oldPersonId = Check::checkInteger(trim(isset($params['oldPersonId']) ? $params['oldPersonId'] : ''));
$newParentId = Check::checkInteger(trim(isset($params['newParentId']) ? $params['newParentId'] : ''));
$isSetUserId = (isset($params['isSetUserId']) && intval($params['isSetUserId']) > 0) ? intval($params['isSetUserId']) : 0 ;
//$isSetUserId = 0;
$userId = $GLOBALS['userId'];

try {
    $personDB = new CDBPerson();
    $familyDB = new CDBFamily();

    $oldPerson = $personDB->getPersonObjectById($oldPersonId);
    $newParent = $personDB->getPersonObjectById($newParentId);

    if($oldPerson == NULL){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "复制的人物不存在");
        exit();
    }

    if($oldPerson->familyId == $newFamilyId){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "家族相同。必须是不同的家族");
        exit();
    }

    //检查复制权限,只有同时是两家家族管理员或者创始人可以复制，或者分支管理员都在管辖范围
    $permissionInOld = $familyDB->getUserPermission($userId,$oldPerson->familyId,true);
    $permissionInNew = $familyDB->getUserPermission($userId,$newFamilyId,true);
    if (Util::checkPermission($permissionInOld,[$oldPerson->familyIndex]) < 3  ||
        Util::checkPermission($permissionInNew,[$newParent->familyIndex]) < 3)
    {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误,必须是两个家族的分支管理员或管理员或者创始人");
        exit();
    }


    //判断需要复制到的 newParentId 是否在家族中
    if(!$familyDB->verifyPersonIdAndFamilyId($newParentId, $newFamilyId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户与家族id不匹配或者用户不存在");
        exit;
    }
    $copyMsg = $familyDB->copyFamilyBranchByPersonV3_1($oldPerson->familyId, $oldPersonId, $newFamilyId, $newParentId, $userId, $isSetUserId);
    
    Util::printResult($copyMsg['error_code'], $copyMsg['error_msg']);

}  catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '数据出现异常');
}
