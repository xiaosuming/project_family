<?php
/**
 * 拒绝用户申请加入家族
 * @author lizuhenhao
 * @date 2020-08-18
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;
use DB\CDBPerson;

$messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));                  //接受的消息id
$userId = $GLOBALS['userId'];

if($messageId === ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $messageDB = new CDBMessage();
    $personDB = new CDBPerson();

    //判断message是否存在
    $message = $messageDB->getMessageAndExt($messageId);
    if($message == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该申请");
        exit;
    }else if($message->toUser != $userId){
        //检查消息的接收人是不是该用户
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
        exit;
    }

    $familyId = $message->recordId;         //记录的家族id
    $applyUserId = $message->fromUser;      //申请的用户id
    $bindPerson= $personDB->getPersonV3(($message->ext)['personId']);

    $familyDB = new CDBFamily();

    if ($message->action == $GLOBALS['USER_PERSON_BIND']){
        $permission = $familyDB->getUserPermission($userId, $familyId);
        //检查管理员权限 条件一 判断是否创建者或者管理员， 条件二 判断绑定人物的familyIndex是否在分支管理员的管辖范围
        if (!in_array($permission['permission'],[400,500]) &&
            ($permission['permission'] === 300 && ($bindPerson['familyIndex'] < $permission['startIndex'] || $bindPerson['familyIndex'] > $permission['endIndex'])))
        {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit();
        }

        $applyResult = $messageDB->refuseApplyJoin($messageId,$userId);
        if($applyResult){
            $data['refuse'] = $applyResult;
            Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'],"数据插入错误");
        }
    }elseif ($message->action == $GLOBALS['INVITE_TO_BIND']){

        $applyResult = $messageDB->refuseApplyJoin($messageId,$userId);
        if($applyResult){
            $data['refuse'] = $applyResult;
            Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'],"数据插入错误");
        }

    }else{
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该申请");
        exit;
    }


}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}