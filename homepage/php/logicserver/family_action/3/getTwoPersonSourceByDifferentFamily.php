<?php
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;

/**
 * 根据两个不同家族的人物查询有可能的共同祖先
 * 需要人为设定两个家族相同人物的id
 * @author yuanxin
 * @date 2020/10/26
 * @version 3
 */
$userId = $GLOBALS['userId'];
$personIdA = Check::checkInteger(trim(isset($params['personIdA']) ? $params['personIdA'] : ''));
$personIdB = Check::checkInteger(trim(isset($params['personIdB']) ? $params['personIdB'] : ''));
$familyIdA = Check::checkInteger(trim(isset($params['familyIdA']) ? $params['familyIdA'] : ''));
$familyIdB = Check::checkInteger(trim(isset($params['familyIdB']) ? $params['familyIdB'] : ''));
$samePersonIdA = Check::checkInteger(trim(isset($params['samePersonIdA']) ? $params['samePersonIdA'] : ''));
$samePersonIdB = Check::checkInteger(trim(isset($params['samePersonIdB']) ? $params['samePersonIdB'] : ''));

try{
    if ($personIdA == '' || $personIdB == ''){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少人物id');
        exit;
    }

    if ($samePersonIdA == '' || $samePersonIdB == ''){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少相同人物id');
        exit;
    }

    $familyDB = new CDBFamily();
    if (!$familyDB->getUserPermission($userId,$familyIdA)){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '没有第一个家族的访问权限');
        exit;
    }
    if (!$familyDB->getUserPermission($userId,$familyIdB)){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '没有第二个家族的访问权限');
        exit;
    }

    $personDB = new CDBPerson(); 
    $samePersonA = $personDB->getPersonById2($samePersonIdA);
    $samePersonB = $personDB->getPersonById2($samePersonIdB);


    if($samePersonA == null){
        Util::printResult($GLOBALS['ERROR_SQL_ERROR'], '第一个相同人物查不到');
        exit;
    }

    if($samePersonB == null){
        Util::printResult($GLOBALS['ERROR_SQL_ERROR'], '第二个相同人物查不到');
        exit;
    }
    // 辈份修正
    $dLevel  = $samePersonA->level - $samePersonB->level;
    $ancestorsA  = $personDB->getPersonAncestorsInCurrentFamily($personIdA,$samePersonA->familyIndex);
    $ancestorsB  = $personDB->getPersonAncestorsInCurrentFamily($personIdB,$samePersonB->familyIndex);
    
    if($ancestorsA == null){
        Util::printResult($GLOBALS['ERROR_SQL_ERROR'], '第一个人物查不到');
        exit;
    }
   if($ancestorsB == null){
        Util::printResult($GLOBALS['ERROR_SQL_ERROR'], '第二个人物查不到');
        exit;
    }

    $samePersonIndexA = 0;
    $samePersonIndexB = 0;
    foreach($ancestorsA as $key => $person){
        if($person->id ==  $samePersonIdA){
            $samePersonIndexA = $key;
        break;
        }
    }
    foreach($ancestorsB as $key => $person){
        if($person->id ==  $samePersonIdB){
            $samePersonIndexB = $key;
        break;
        }
    }
    $resultA = array();
    $rateA = array();
    $rateB = array();
    $lastProbability = 1;
    do{
        $probability = 0;
        if($ancestorsA[$samePersonIndexA]->userId == $ancestorsB[$samePersonIndexB]->userId  && ($ancestorsA[$samePersonIndexA]->userId>0)){
            $probability = 1;
            $rate['name'] = $ancestorsA[$samePersonIndexA]->name;
            $rate['rate'] = $probability;
            array_push($rateA,$rate);
            $rate['name'] = $ancestorsB[$samePersonIndexB]->name;
            array_push($rateB,$rate);
            $samePersonIndexB--;
            $samePersonIndexA--;
            $lastProbability = $probability;
            continue;
        }
        if($ancestorsA[$samePersonIndexA]->name == $ancestorsB[$samePersonIndexB]->name ){
            $probability += 0.4;
        }
        if($ancestorsA[$samePersonIndexA]->gender != $ancestorsB[$samePersonIndexB]->gender ){
            $probability =  0.1;
            $rate['name'] = $ancestorsA[$samePersonIndexA]->name;
            $rate['rate'] = $probability;
            array_push($rateA,$rate);
            $rate['name'] = $ancestorsB[$samePersonIndexB]->name;
            array_push($rateB,$rate);
            $samePersonIndexB--;
            $samePersonIndexA--;
            $lastProbability = $probability;
            continue;
        }
        if(count($ancestorsA[$samePersonIndexA]->son) == count($ancestorsB[$samePersonIndexB]->son )){
            $probability += 0.1;
        }
        if(count($ancestorsA[$samePersonIndexA]->spouse) == count($ancestorsB[$samePersonIndexB]->spouse )){
            $probability += 0.1;
        }
        if(count($ancestorsA[$samePersonIndexA]->daughter) == count($ancestorsB[$samePersonIndexB]->daughter )){
            $probability += 0.1;
        }
        if(count($ancestorsA[$samePersonIndexA]->ranking) == count($ancestorsB[$samePersonIndexB]->ranking )){
            $probability += 0.1;
        }
        if(count($ancestorsA[$samePersonIndexA]->brother) == count($ancestorsB[$samePersonIndexB]->brother )){
            $probability += 0.1;
        }
        if(count($ancestorsA[$samePersonIndexA]->sister) == count($ancestorsB[$samePersonIndexB]->sister )){
            $probability += 0.1;
        }
        $rate['name'] = $ancestorsA[$samePersonIndexA]->name;
        $rate['rate'] = $probability * $lastProbability;
        array_push($rateA,$rate);
        $rate['name'] = $ancestorsB[$samePersonIndexB]->name;
        array_push($rateB,$rate);
        $samePersonIndexB--;
        $samePersonIndexA--;
        $lastProbability = $rate['rate'];
    }while($samePersonIndexB > 0  &&  $samePersonIndexA > 0);
    $result['familyA'] = $rateA;
    $result['familyB'] = $rateB;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
}catch(PDOException $e){
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}