<?php
/**
 * 接受用户申请加入家族
 * @author liuzhenhao
 * @date 2020-08-18
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;
use DB\CDBAccount;
use DB\CDBPerson;
use ThirdParty\InstantMessage;
use Model\Message;
//$type = 3; // 1分享链接 2 二维码 3 申请 4 接受邀请
$messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));                  //接受的消息id
$userId = $GLOBALS['userId'];

if ($messageId === "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $messageDB = new CDBMessage();
    $personDB = new CDBPerson();

    //判断message是否存在
    $message = $messageDB->getMessage($messageId);
    if ($message == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该申请");
        exit;
    } else if ($message->toUser != $userId) {
        //检查消息的接收人是不是该用户
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $familyId = $message->recordId;         //记录的家族id
    $applyUserId = $message->fromUser;      //申请的用户id


    $familyDB = new CDBFamily();
    $permission = $familyDB->getUserPermission($userId, $familyId);
    //检查管理员权限
    if (!in_array($permission['permission'],[400,500])) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit();
    }

    //检查用户是否在家族中
    if ($familyDB->getUserPermission($message->fromUser, $familyId)) {
        if (!$messageDB->acceptInvite($messageId)){
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "修改失败");
            exit;
        }
        $data['accept'] = true;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit();
    }

    $family = $familyDB->getFamilyById($familyId);

    $socialCircleId = $family->socialCircleId;

    $applyResult = $messageDB->acceptApplyJoinAndFollowCircleV3($messageId, $userId,$socialCircleId);

    if ($applyResult) {
        //发送消息通知申请的用户
        $newMessage = new Message();
        $newMessage->fromUser = $userId;
        $newMessage->toUser = $applyUserId;
        $newMessage->content = "加入家族的申请已通过！";
        $newMessage->module = 9;
        $newMessage->action = $GLOBALS['MESSAGE_APPLY_REPLY_ACCEPT'];
        $newMessage->recordId = $messageId;    //记录Id是回复的消息id
        $newMessage->extraRecordId = $familyId;
        $newMessage->ext = $message->ext;  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
        $messageDB->addMessage($newMessage);

        $data['accept'] = $applyResult;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据插入错误");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
