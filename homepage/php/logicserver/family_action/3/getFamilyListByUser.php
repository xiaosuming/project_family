<?php

use Util\Util;
use DB\CDBFamily;
use Util\Check;

$userId = $GLOBALS['userId'];
$showOpenFamily = Check::checkInteger(trim(isset($params['showOpenFamily']) ? $params['showOpenFamily'] : 1), 'showOpenFamily');
$text = Check::check(trim(isset($params['text']) ? $params['text'] : null));

try {
    $familyDB = new CDBFamily();
//    $joinFamilyList = $familyDB->getJoinFamilyListV3($userId);
//    $joinFamilyTempList = $familyDB->getJoinFamilyForTempV3($userId);
//    $originatorFamilyList = $familyDB->getOriginatorFamilyList($userId);
    $joinFamilyList = $familyDB->getFamilyListByPermission($userId);
    if ($showOpenFamily) {
        $openFamilyList = $familyDB->getOpenFamilysV3();
    }else{
        $openFamilyList = [];
    }

    $data = $joinFamilyList;

    # 数据去重
    $tmp_arr = [];

    foreach ($data as $k => $v) {
        if (in_array($v['id'], $tmp_arr)) {
            unset($data[$k]);
        }
        $tmp_arr[] = $v['id'];
    }

    $data = array_values($data);

//    usort($data, function($a,$b){
//        if ($a['createTime'] === $b['createTime']){
//            return 0;
//        }
//        return $a['createTime'] > $b['createTime'] ? -1:1;
//    });

    $data = array_values(array_merge($data, $openFamilyList));

    #为什么以下这段筛选的代码呢，当然是你猜了，猜对了就告诉你
    if ($text !== null && $text !== '') {

        $data_filter = array_filter($data, function ($value) {
            global $text;
            if (strpos($value['id'], $text) === false && strpos($value['name'], $text) === false) {
                return false;
            }
            return true;
        });

        $result['familyList'] = array_values($data_filter);

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
        return;
    }

    $result['familyList'] = $data;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
