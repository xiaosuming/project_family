<?php


/**
 * 管理员通过用户名查找邀请绑定并加入
 * @author liuzhenhao
 * @date 2020-07-14
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBAccount;
use DB\CDBMessage;
use Model\Message;
use Model\Person;

$userId = $GLOBALS['userId'];

//$inviteUserId = Check::checkInteger($params['inviteUserId'] ?? '');  //邀请人的用户id
$identity = Check::check(trim(isset($params['identity']) ? $params['identity'] : ''));//家族id
$personId = Check::checkInteger(isset($params['personId']) ? trim($params['personId']) : '','personId');
$username = Check::check(trim(isset($params['username']) ? $params['username'] : ''));
$phone = isset($params['phone']) ? trim($params['phone']) : null;;
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : '申请绑定人物'), 0, 500);
$userId = $GLOBALS['userId'];
//申请信息备注
//$remark = Util::filterContentReplace($remark);

if ($identity === "" || ($username == '' && $phone == '')) {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}
if ($phone) {
    Check::checkPhoneNumber($phone);
}

try {
    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();
    $accountDB = new CDBAccount();

    $familyInfo = $familyDB->getFamilyInfoByIdentity($identity);


    if ($familyInfo === null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "家族不存在");
        exit();
    }

    $person = $personDB->getPersonV3($personId);
    $father = $personDB->getPersonV3($personId['father']);

    if (!$person){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "该人物不存在");
        exit();
    }

    if ($person['userId']){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "该人物已绑定");
        exit();
    }

    $user = $accountDB->getUserList($username ? ['username'=>$username]:['phone'=>$phone])['data'][0];

    if (!$user){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "该用户不存在");
        exit();
    }

    $familyId = $familyInfo['id'];

    $permission = $familyDB->getUserPermission($userId, $familyId,true);

    if (!$permission) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "权限错误");
        exit();
    }

    //检查管理员权限
    if (Util::checkPermission($permission,[$person['familyIndex']]) < 4)
    {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit();
    }


    //检查被用户是否已经在家族中存在
    $userPermission = $familyDB->getUserPermission($user['id'], $familyId,true);

    if ($userPermission && $userPermission['personId'] !== null) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "该用户已绑定");
        exit();
    }

    //生成随机消息码
    $messageCode = Util::generateRandomCode(36);

    //管理员发送消息通知未绑定的用户
    $messageDB = new CDBMessage();
    $newMessage = new Message();
    $newMessage->fromUser = $userId;
    $newMessage->toUser = $user['id'];
    $newMessage->content = "邀请绑定人物！";
    $newMessage->module = 9;
    $newMessage->action = $GLOBALS['INVITE_TO_BIND'];
    $newMessage->recordId = $familyId;    //记录Id是回复的消息id
    $newMessage->messageCode = $messageCode;
//        $newMessage->extraRecordId = $familyId;

    $extData = [
        "familyName" => $familyInfo['name'],
        "inviteUserId" => $userId,
        "name" => $person['name'],
        "fatherName" => $person['fatherName'],
        'level' => $person['level'],
        "invitePermission" => $permission['permission'],
        "inviteName" => $permission['name'],
        "personId" => $personId
    ];
    $newMessage->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
    $messageDB->addMessage($newMessage);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], "申请成功");


} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
