<?php
/**
 * 删除pdf导出历史记录
 * deletePdfTaskHistory
 * @author  yuanxin
 * @version 3
 * @date 2020/09/03
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPdfTask;




$familyId = Check::checkInteger($params['familyId'] ?? '');
$taskId = Check::checkInteger($params['taskId'] ?? '');

// $familyId = Check::checkInteger(isset($params['familyId']) ?? 0);             //家族ids
// $mergeId = Check::checkInteger(isset($params['mergeId']) ??  0); s   //关联信息id
// $personId = Check::checkInteger(isset($params['personId'])  ??  0);    //取消人物id
$userId = $GLOBALS['userId'];
if($familyId === '' || $taskId === ''  ){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    
    $familyDB = new CDBFamily();
    $pdfTaskDB = new CDBPdfTask();

    $result = $pdfTaskDB->deletePdfTask($taskId);
   

    if($result > 0){
        Util::printResult($GLOBALS['ERROR_SUCCESS'], "删除成功");
    }else{
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "删除失败");
        exit;
    }
   


}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}