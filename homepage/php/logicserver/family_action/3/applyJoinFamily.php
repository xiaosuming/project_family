<?php

/**
 * 用户申请加入家族
 * @author liuzhenhao
 * @date 2020-07-14
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;
use Model\Message;
use ThirdParty\MiniProgram;

$userId = $GLOBALS['userId'];

//$inviteUserId = Check::checkInteger($params['inviteUserId'] ?? '');  //邀请人的用户id
$identity = Check::check(trim(isset($params['identity']) ? $params['identity'] : ''));//家族id
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
$fatherName =  Check::check(trim(isset($params['fatherName']) ? $params['fatherName'] : ''));
$userId = $GLOBALS['userId'];
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : '申请加入家族'), 0, 500);             //申请信息备注
//$remark = Util::filterContentReplace($remark);

if ($identity === "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$name, $fatherName, $remark]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    $familyDB = new CDBFamily();

    $familyInfo = $familyDB->getFamilyInfoByIdentity($identity);

    if ($familyInfo === null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "申请加入的家族不存在");
        exit();
    }


    $familyId = $familyInfo['id'];

    //检查该用户是否已经在家族中存在
    if ($familyDB->getUserPermission($userId, $familyId)) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "用户已经加入该家族");
        exit();
    }

    //向所有的家族管理员都发送这条消息
    $adminsId = $familyDB->getFamilyChiefAdminId($familyId);
    //生成随机消息码
    $messageCode = Util::generateRandomCode(36);

    $messageDB = new CDBMessage();
    foreach ($adminsId as $adminId) {
        $message = new Message();
        $message->fromUser = $userId;
        $message->toUser = $adminId['userId'];
        $message->content = $remark;
        $message->module = $GLOBALS['FAMILY_MODULE'];
        $message->action = $GLOBALS['APPLYJOIN'];
        $message->recordId = $familyId;
        $message->messageCode = $messageCode;

        $extData = [
            "inviteUserId" => 0,
            "familyName" => $familyInfo['name'],
            "applyUserId" => $userId,
            "name" => $name,
            "fatherName" => $fatherName,
        ];

        $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
        $messageId = $messageDB->addMessage($message);
    }

    Util::printResult($GLOBALS['ERROR_SUCCESS'], "申请成功");

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
