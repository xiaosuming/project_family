<?php

    /**
     * 根据家族id获取已删除人物及其分支日志列表
     * author:liuzhenhao
     * date:2020-8-30
     */
    
    use Util\Util;
    use DB\CDBFamily;
    use Util\Check;
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $userId = $GLOBALS['userId'] ?? 0;
    $name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
    $orderBy = Check::check(trim(isset($params['orderBy']) ? $params['orderBy'] : ''));
    
    try{
        $familyDB = new CDBFamily();
    
        //检查用户权限
        if(Util::checkPermission($familyDB->getUserPermission($userId,$familyId),[],false) < 1){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }
    
        $res = $familyDB->readBranchLogsV3($familyId,$name,$orderBy);
        $data['lists'] = $res;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    }catch(PDOException $e){
        //异常处理
//        print_r($e);
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
