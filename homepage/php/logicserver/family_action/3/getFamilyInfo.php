<?php 
    /**
     * 获取家族信息
     * author:刘振豪
     * date: 2020-07-31
     *
     */
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBPerson;
    use Util\Check;

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

    try{
        if($familyId!=""){
            $familyDB = new CDBFamily();
            $userId = $GLOBALS['userId'];
            $family = $familyDB->getFamilyById($familyId);

            if ($family == null) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
                exit;
            }

            if ($family->openType == 0) {
                if (!$familyDB->getUserPermission($userId,$familyId)){
                    // 私密族群
                    //检查权限
                    $newFamily['id'] = $family->id;
                    $newFamily['name'] = $family->name;
                    $newFamily['photo'] = $family->photo;
                    $newFamily['minLevel'] = $family->minLevel;
                    $data['family'] = $newFamily;
                    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                    exit;
                }
            }

            $url = 'm.izuqun.com';
            $identity = $family->identity;
            $content = "https://".$url . $GLOBALS['ROUTER_TYPE']."/family/pt-in-family/".$identity;
            $family->qrcode = $content;

            # 新版本添加该家族的在世人数
            $family->alivePeople = $familyDB->getAlivePeopleNumberByFamily($familyId);

            $data['family'] = $family;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }		
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
