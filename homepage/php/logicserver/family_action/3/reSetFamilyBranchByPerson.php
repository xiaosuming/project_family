<?php

    /**
     * 根据家族id和人物id 日志id 还原已删除人物及其分支
     * author:liuzhenhao
     * date:2020-8-30
     */
    
    use Util\Util;
    use DB\CDBFamily;
    use DB\CDBPerson;
    use Util\Check;
    
    $logId = Check::checkInteger(trim(isset($params['logId']) ? $params['logId'] : ''));
    $personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $relation = isset($params['parentId']) ? intval($params['parentId']) : 0;
    
    
    $userId = $GLOBALS['userId'] ?? 0;
    if($personId > 0){
    
        $familyDB = new CDBFamily();
        //判断要还原的personId是否在家族中
        if(!$familyDB->verifyPersonIdAndFamilyId($personId, $familyId, '-1')){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户与家族id不匹配或者用户不存在");
            exit;
        }

        $personDB = new CDBPerson();
        $person = $personDB->getPersonObjectById($personId);
        //检查用户权限
        $permission = $familyDB->getUserPermission($userId, $familyId, true);
        if (Util::checkPermission($permission, [$person->familyIndex]) < 3 || $permission['branchPerson'] == $personId) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
            exit;
        }
//        if(!$familyDB->isAdminForFamily($familyId, $userId) && !$familyDB->isOriginatorForFamily($familyId, $userId)){
//            Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
//            exit;
//        }
//
        $res = $familyDB->reSetBranchV3($logId, $userId, $relation);
        Util::printResult($res['error_code'], $res['error_msg']);
    
    }else{
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "personId 不正确");
        exit;
    }

    
    