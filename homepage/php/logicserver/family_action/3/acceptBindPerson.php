<?php
/**
 * 接受用户申请加入家族
 * @author liuzhenhao
 * @date 2020-08-18
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;
use DB\CDBAccount;
use DB\CDBPerson;
use ThirdParty\InstantMessage;
use Model\Message;
use Model\Person;
//$type = 3; // 1分享链接 2 二维码 3 申请 4 接受邀请
$messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));                  //接受的消息id
$userId = $GLOBALS['userId'];

if ($messageId === "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $messageDB = new CDBMessage();
    $personDB = new CDBPerson();

    //判断message是否存在
    $message = $messageDB->getMessageAndExt($messageId);
    if ($message == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该申请");
        exit;
    } else if ($message->toUser != $userId) {
        //检查消息的接收人是不是该用户
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $familyId = $message->recordId;         //记录的家族id
    $applyUserId = $message->fromUser;      //申请的用户id
    $bindPerson= $personDB->getPersonV3(($message->ext)['personId']);

    if (!$bindPerson){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "被绑定人物不存在");
        exit;
    }

    if ($message->action == $GLOBALS['USER_PERSON_BIND']){
        #这是自己主动申请绑定人物的消息

        $familyDB = new CDBFamily();
        $permission = $familyDB->getUserPermission($userId, $familyId);
        //检查管理员权限 条件一 判断是否创建者或者管理员， 条件二 判断绑定人物的familyIndex是否在分支管理员的管辖范围
        if (!in_array($permission['permission'],[400,500]) &&
            ($permission['permission'] === 300 && ($bindPerson['familyIndex'] < $permission['startIndex'] || $bindPerson['familyIndex'] > $permission['endIndex'])))
        {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit();
        }

        //检查用户是否未加入家族状态
        $userPermission = $familyDB->getUserPermission($message->fromUser, $familyId,true);
        if (!$userPermission) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "用户未加入家族");
            exit();
        }

        if ($userPermission['personId']){
            if ($message->fromUser === $bindPerson['userId']){
                if (!$messageDB->acceptInvite($messageId)){
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "修改失败");
                    exit;
                }
                $data['accept'] = true;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                exit();
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "人物已被其他用户绑定");
                exit();
            }
        }


        $acceptBindResult = $personDB->joinAndBindUserToPersonWithPersonId($message->fromUser,new Person($bindPerson),$userId,$userPermission);
        if ($acceptBindResult <= 0){
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据插入错误");
            exit();
        }

//        $updatePermissionResult = $familyDB->modifyPermission($message->recordId,  $userPermission['permission'] === 100 ? 200:null, $message->fromUser,
//            0,0,$userId,0);


        if ($acceptBindResult > 0) {
            if (!$messageDB->acceptInvite($messageId)){
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "修改失败");
                exit;
            }
            //发送消息通知申请的用户
            $newMessage = new Message();
            $newMessage->fromUser = $userId;
            $newMessage->toUser = $applyUserId;
            $newMessage->content = "绑定人物的申请已通过！";
            $newMessage->module = 9;
            $newMessage->action = $GLOBALS['REPLY_BIND_USER'];
            $newMessage->recordId = $messageId;    //记录Id是回复的消息id
            $newMessage->extraRecordId = $familyId;
            $newMessage->ext = json_encode($message->ext, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
            $messageDB->addMessage($newMessage);

            $data['accept'] = boolval($acceptBindResult);
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据插入错误");
        }
    }elseif ($message->action == $GLOBALS['INVITE_TO_BIND']){

        #这是管理员邀请绑定的消息

        $familyDB = new CDBFamily();
        if ($message->toUser !== $userId){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit();
        }

        //检查用户是否未加入家族状态
        $userPermission = $familyDB->getUserPermission($userId, $familyId,true);

//        if (!$userPermission) {
//            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "用户未加入家族");
//            exit();
//        }

        if ($userPermission['personId']){
            if ($userId === $bindPerson['userId']){
                if (!$messageDB->acceptInvite($messageId)){
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "修改失败");
                    exit;
                }
                $data['accept'] = true;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                exit();
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "人物已被其他用户绑定");
                exit();
            }
        }

        if ($userPermission){
            $acceptBindResult = $personDB->joinAndBindUserToPersonWithPersonId($userId,new Person($bindPerson),$message->toUser,$userPermission);
        }else{
            $acceptBindResult = $personDB->joinAndBindUserToPersonWithPersonId($userId,new Person($bindPerson),$message->toUser,$userPermission,false);
        }

        if ($acceptBindResult <= 0){
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据插入错误");
            exit();
        }

//        $updatePermissionResult = $familyDB->modifyPermission($message->recordId, $userPermission['permission'] === 100 ? 200:null, $message->toUser, 0,
//            0,$userId,0);

        if ($acceptBindResult > 0) {
            //发送消息通知申请的用户
            if (!$messageDB->acceptInvite($messageId)){
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "修改失败");
                exit;
            }
            $newMessage = new Message();
            $newMessage->fromUser = $userId;
            $newMessage->toUser = $applyUserId;
            $newMessage->content = "用户已同意绑定人物！";
            $newMessage->module = 9;
            $newMessage->action = $GLOBALS['REPLY_INVITE_BIND'];
            $newMessage->recordId = $messageId;    //记录Id是回复的消息id
            $newMessage->extraRecordId = $familyId;
            $newMessage->ext = json_encode($message->ext, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
            $messageDB->addMessage($newMessage);

            $data['accept'] =  boolval($acceptBindResult);;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据插入错误");
        }

    }else{
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该申请");
        exit;
    }



} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
