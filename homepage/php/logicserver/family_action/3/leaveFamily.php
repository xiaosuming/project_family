<?php
/**
 * 取消人物与用户之间的绑定（踢人）
 * author:liuzhenaho
 * date:2020-08-25
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use DB\CDBAccount;
use DB\CDBMessage;
use Model\Message;
use Util\Check;
use Util\PostGenerate;

//$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
$toUser = Check::checkInteger(trim(isset($params['toUser']) ? $params['toUser'] : ''));
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

try {

    if ($toUser === "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $personDB = new CDBPerson();
    $accountDB = new CDBAccount();
    $familyDB = new CDBFamily();
    $userId = $GLOBALS['userId'];

    $family = $familyDB->getFamilyById($familyId);

    if (!$family){
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "家族不存在");
        exit();
    }
//    $messageCode = Util::generateRandomCode(36);
    if ($toUser === $userId){

        $beCanceledUserPermission = $familyDB->getUserPermission($toUser,$familyId,true);

        $beCanceledUserPermissionNum = Util::checkPermission($beCanceledUserPermission);
        if ($beCanceledUserPermissionNum === 5){
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "创建者不能离开家族");
            exit();
        }

        if (!$beCanceledUserPermission){
            $data['accept'] = true;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit();
        }

        $operation = $personDB->leaveFamilyAndUnbindPerson($userId, $familyId);

        if ($operation) {
            //向所有的家族管理员都发送这条消息
            if ($beCanceledUserPermissionNum = 4){
                $adminsId = ['userId'=>$familyDB->getFamilyOriginatorId($familyId)];
            }else{
                $adminsId = $familyDB->getFamilyChiefAdminId($person->familyId);
            }
                $userInfo = $accountDB->getUserInfoV3($userId);

                $messageDB = new CDBMessage();
                foreach ($adminsId as $adminId) {
                    $message = new Message();
                    $message->fromUser = $userId;
                    $message->toUser = $adminId['userId'];
                    $message->content = '用户离开家族';
                    $message->module = $GLOBALS['FAMILY_MODULE'];
                    $message->action = $GLOBALS['LEAVE_FAMILY'];
                    $message->recordId = $familyId;
//                    $message->messageCode = $messageCode;

                    $extData = [
                        "username" => $userInfo['username'],
                        "familyId" => $familyId,
                        "familyName" => $family->name
                    ];

                    $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
                    $messageId = $messageDB->addMessage($message);
                }

                $data['accept'] = $operation;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else {
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "取消绑定失败");
        }
    }else{

        $beCanceledUserPermission = $familyDB->getUserPermission($toUser,$familyId,true);
        $beCanceledUserPermissionNum = Util::checkPermission($beCanceledUserPermission);
        //检查操作的权限i
        $userPermission = $familyDB->getUserPermission($userId,$familyId,true);
        $userPermissionNum = Util::checkPermission($userPermission);
        if ($userPermissionNum < 4 || $userPermissionNum <= $beCanceledUserPermissionNum){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        if (!$beCanceledUserPermission){
            $data['accept'] = true;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit();
        }

        $operation = $personDB->leaveFamilyAndUnbindPerson($toUser, $familyId);

        if ($operation){
            # 发送消息通知被踢掉的用户

            $userInfo = $accountDB->getUserInfoV3($toUser);

            $messageDB = new CDBMessage();
            $message = new Message();
            $message->fromUser = $userId;
            $message->toUser = $toUser;
            $message->content = '管理员解解绑用户';
            $message->module = $GLOBALS['FAMILY_MODULE'];
            $message->action = $GLOBALS['LEAVE_FAMILY'];
            $message->recordId = $familyId;
//            $message->messageCode = $messageCode;

            $extData = [
                "username" => $userInfo['username'],
                "familyId" => $familyId,
                "familyName" => $family->name
            ];

            $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
            $messageId = $messageDB->addMessage($message);
            $data['accept'] = $operation;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else {
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "取消绑定失败");
        }

    }


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
