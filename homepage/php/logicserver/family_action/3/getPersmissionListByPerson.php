<?php
use Util\Util;
use DB\CDBFamily;
use Util\Check;

$userId = $GLOBALS['userId'];
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

try{
    $familyDB = new CDBFamily();

    if (!$familyDB->getUserPermission($userId,$familyId)){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '权限错误');
        exit;
    }

//    $result = $familyDB->getUserPermission()

    $result['familyList'] = $data;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}