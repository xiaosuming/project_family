<?php
/**
 * 取消关联家族
 * @author  yuanxin
 * @version 3
 * @date 2020/09/03
 */
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;



$familyId = Check::checkInteger($params['familyId'] ?? '');
$mergeId = Check::checkInteger($params['mergeId'] ?? '');

// $familyId = Check::checkInteger(isset($params['familyId']) ?? 0);             //家族id
// $mergeId = Check::checkInteger(isset($params['mergeId']) ??  0);    //关联信息id
// $personId = Check::checkInteger(isset($params['personId'])  ??  0);    //取消人物id
$userId = $GLOBALS['userId'];


if($familyId === '' || $mergeId === ''  ){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    
    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();

    $mergeInfo = $familyDB->getFamilyMergeInfoById($mergeId);
    if(array() != $mergeInfo){
        $result = $personDB->cancelRefPerson($mergeInfo);
    }else{
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "没有该信息");
        exit;
    }

    if($result){
        Util::printResult($GLOBALS['ERROR_SUCCESS'], "删除成功");
    }else{
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "删除失败");
        exit;
    }
   


}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}