<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午3:04
 */

require_once("logicserver/information_action/information_action_file.php");

use Util\Util;

$sub_action = isset($params['sub_action']) ? $params['sub_action'] : "0";

if (!array_key_exists($sub_action, $information_action_file_array)) {
    $errorCode = $GLOBALS['ERROR_REQUEST'];
    $data = "information action not exist " . $sub_action;
    Util::printResult($errorCode, $data);
    exit(-1);
}

$sub_action_file = $information_action_file_array[$sub_action];

if (!file_exists($sub_action_file)) {
    $errorCode = $GLOBALS['ERROR_REQUEST'];
    $data = "information action file not exist " . $sub_action_file;
    Util::printResult($errorCode, $data);
    exit(-1);
}

require_once($sub_action_file);