<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-12-29
 * Time: 下午4:17
 */

require_once("logicserver/greetingCard_action/greetingCard_action_file.php");

use Util\Util;

$sub_action = isset($params['sub_action']) ? $params['sub_action'] : "0";

if (!array_key_exists($sub_action, $greetingCard_action_file_array)) {
    $errorCode = $GLOBALS['ERROR_REQUEST'];
    $data = "greetingCard action not exist " . $sub_action;
    Util::printResult($errorCode, $data);
    exit(-1);
}

$sub_action_file = $greetingCard_action_file_array[$sub_action];

if (!file_exists($sub_action_file)) {
    $errorCode = $GLOBALS['ERROR_REQUEST'];
    $data = "greetingCard action file not exist " . $sub_action_file;
    Util::printResult($errorCode, $data);
    exit(-1);
}

require_once($sub_action_file);
