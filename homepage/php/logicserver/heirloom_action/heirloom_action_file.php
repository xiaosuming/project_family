<?php
    /**
     * 传家宝索引文件
     */

    /**
     * whiteList是白名单，用来允许某些请求通过验证
     */
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $heirloom_action_file_array['addHeirloom'] = "logicserver/heirloom_action/{$version}/addHeirloom.php";
    $heirloom_action_file_array['deleteHeirloom'] = "logicserver/heirloom_action/{$version}/deleteHeirloom.php";
    $heirloom_action_file_array['updateHeirloom'] = "logicserver/heirloom_action/{$version}/updateHeirloom.php";
    $heirloom_action_file_array['getHeirloom'] = "logicserver/heirloom_action/{$version}/getHeirloom.php";
    $heirloom_action_file_array['getHeirloomsPaging'] = "logicserver/heirloom_action/{$version}/getHeirloomsPaging.php";