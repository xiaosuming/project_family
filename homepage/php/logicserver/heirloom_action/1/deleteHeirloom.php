<?php
/**
 * 删除传家宝
 * @auther jiangpengfei
 * @date 2017-04-21
 */
    use Util\Util;
    use DB\CDBFamilyHeirloom;
    use Util\Check;

    $heirloomId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));	    //传家宝id

    try{
        if($heirloomId != ""){

            $heirloomDB = new CDBFamilyHeirloom();
            $userId = $GLOBALS['userId'];

            if(!$heirloomDB->verifyUserIdAndHeirloomId($userId,$heirloomId)){
                //如果没有权限
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
                exit;
            }
            $deleteCount = $heirloomDB->deleteHeirloom($heirloomId);

            if($deleteCount > 0){
                $data['deleteCount'] = $deleteCount;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }