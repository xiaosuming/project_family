<?php
/**
 * 获取一页传家宝信息
 * @auther jiangpengfei
 * @date 2017-04-21
 */
    use Util\Util;
    use DB\CDBFamilyHeirloom;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    use Util\Pager;

    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));	        //页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 10));	        //页大小
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));	        //家族id


    try{
        if($familyId != ""){

            $familyDB = new CDBFamily();
            $userId = $GLOBALS['userId'];

            $family = $familyDB->getFamilyById($familyId);

            if ($family == null) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
                exit;
            }

            if ($family->openType == 0) {

                //验证用户属于家族,即属于家族的用户都有权限查看祖坟
                if(!$familyDB->isUserForFamily($familyId, $userId)){
                    //如果没有权限
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
                    exit;
                }
            }

            $heirloomDB = new CDBFamilyHeirloom();
            $count = $heirloomDB->getHeirloomsTotal($familyId);
            $paging = $heirloomDB->getHeirloomsPaging($familyId,$pageIndex,$pageSize);
            foreach($paging as $key=>$item){
                $photo=$item['photo'];
                $paging[$key]['photo']=explode(',',$photo);
            }
            $page = new Pager($count,$paging,$pageIndex,$pageSize);
            $page->printPage();
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }