<?php
/**
 * 添加传家宝
 * @auther jiangpengfei
 * @date 2017-04-21
 */

use Util\Util;
use DB\CDBFamilyHeirloom;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));                        //文件
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));        //家族id
$title = Check::check(trim(isset($params['title']) ? $params['title'] : ''), 1, 50);                       //标题
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''));                 //内容
$title = Util::filterContentReplace($title);
$content = Util::filterContentReplace($content);

try {
    if ($familyId != "" && $photo != "" && $title != "" && $content != "") {
        $familyDB = new CDBFamily();
        $userId = $GLOBALS['userId'];
        //验证用户属于家族,即属于家族的用户都有权限添加传家宝
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            //如果没有权限
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
            exit;
        }
        //这里将photo解析成image数组
        $pathArray = explode(",", $photo);
        if (count($pathArray) < 1) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量至少一张");
            exit();
        }
        if (count($pathArray) > 9) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量超过限制");
            exit();
        }
        $heirloomDB = new CDBFamilyHeirloom();
        $heirloomId = $heirloomDB->addHeirloom($familyId, $title, $content, $photo, $userId);

        if ($heirloomId > 0) {
            $data['heirloomId'] = $heirloomId;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '插入出错');
            exit;
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
