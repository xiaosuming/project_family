<?php
/**
 * 获取传家宝
 * @auther jiangpengfei
 * @date 2017-04-21
 */

use DB\CDBFamily;
use DB\CDBFamilyHeirloom;
use Util\Check;
use Util\Util;

$heirloomId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));	    //传家宝id

    try{
        if($heirloomId != ""){

            $heirloomDB = new CDBFamilyHeirloom();
            $userId = $GLOBALS['userId'];
            $heirloom = $heirloomDB->getHeirloom($heirloomId);
            $photo=$heirloom->photo;
            $heirloom->photo=explode(',',$photo);
            $familyDB = new CDBFamily();
            //验证用户属于家族,即属于家族的用户都有权限查看传家宝
            if($heirloom == null || !$familyDB->isUserForFamily($heirloom->familyId, $userId)){
                //如果没有权限
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
                exit;
            }
            $data['heirloom'] = $heirloom;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }