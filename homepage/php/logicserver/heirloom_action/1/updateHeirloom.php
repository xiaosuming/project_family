<?php
    use Util\Util;
    use DB\CDBFamilyHeirloom;
    use Util\Check;

    $photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));				                                //照片
    $heirloomId = Check::checkInteger(trim(isset($params['heirloomId']) ? $params['heirloomId'] : ''));	    //人物id
    $title = Check::check(trim(isset($params['title']) ? $params['title'] : ''),1,50);                      		//标题
    $content = Check::check(trim(isset($params['content']) ? $params['content'] : ''));                      		//内容
    $title = Util::filterContentReplace($title);
    $content = Util::filterContentReplace($content);

    try{
        if($photo != "" && $heirloomId != "" && $title != "" && $content != ""){
            $heirloomDB = new CDBFamilyHeirloom();
            $userId = $GLOBALS['userId'];

            if(!$heirloomDB->verifyUserIdAndHeirloomId($userId,$heirloomId)){
                //如果没有权限
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
                exit;
            }
            //这里将photo解析成image数组
            $pathArray = explode(",", $photo);
            if (count($pathArray) < 1){
                Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量至少一张");
                exit();
            }
            if (count($pathArray) > 9) {
                Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量超过限制");
                exit();
            }
            $updateCount = $heirloomDB->updateHeirloom($heirloomId,$title, $content, $photo, $userId);
            if($updateCount > 0){
                $data['updateCount'] = $updateCount;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "无变化");
            }

        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
