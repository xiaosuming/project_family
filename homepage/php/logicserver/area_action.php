<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/19 0019
 * Time: 13:35
 */

    require_once("logicserver/area_action/area_action_file.php");

    use Util\Util;

    $sub_action = isset($params['sub_action'])?$params['sub_action']:"0";

    if (!array_key_exists($sub_action, $area_action_file_array))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "area action not exist".$sub_action;
        Util::printResult($errorCode, $data);
        exit(-1);
    }

    $sub_action_file = $area_action_file_array[$sub_action];

    if (!file_exists($sub_action_file))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "area action file not exist".$sub_action_file;
        Util::printResult($errorCode, $data);
        exit(-1);
    }

    require_once($sub_action_file);