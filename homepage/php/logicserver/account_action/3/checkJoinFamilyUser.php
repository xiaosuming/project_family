<?php
/**
 * 更新家族信息
 *  @author liuzhenhao
 *  @date 2020-08-06
 */

use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBAccount;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : '')); //必须
$userName = Check::check(trim(isset($params['familyId']) ? $params['familyId'] : ''),1); //必须
$userId = $GLOBALS['userId'];

try {

    if ($familyId != "") {

        $userId = $GLOBALS['userId'];
        $accountDB = new CDBAccount();
        $familyDB = new CDBFamily();

        if (!$familyDB->getUserPermission($userId, $familyId)){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "用户权限错误");
            exit;
        }

        $family = $accountDB->checkFamilyUser($familyId,$userName);

//        var_dump($family);

//        if ($family == null) {
//            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
//            exit;
//        }
//
//        if ($family->openType == 0) {
//            //检查查询的权限
//            if (!$familyDB->isUserForFamily($familyId, $userId) && !$familyDB->isAdminForFamily($familyId, $userId) && !$familyDB->isOriginatorForFamily($familyId, $userId)) {
//                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
//                exit;
//            }
//        }
//        $personDB = new CDBPerson();
//        $personList = $personDB->getFamilyPersonList($familyId);
//        $data['personList'] = $personList;
//
        $data = $family;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
