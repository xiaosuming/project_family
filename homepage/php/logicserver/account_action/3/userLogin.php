<?php

use Util\Util;
use DB\CDBAccount;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
use Util\RateLimit;
use Util\TempStorage;

$loginName = Check::check(trim(isset($params['loginName']) ? $params['loginName'] : ''));
$password = Check::check(trim(isset($params['password']) ? $params['password'] : ''));
$loginDevice = Check::check(trim(isset($params['loginDevice']) ? $params['loginDevice'] : ''));
$deviceCode = Check::check(trim(isset($params['deviceCode']) ? $params['deviceCode'] : ''));
$shareCode = Check::check(trim($params['sharecode'] ?? ''));     //登录参数中也可以携带code值
$fromUserId = Check::checkInteger($params['fromUserId'] ?? 0); //邀请人的用户id
$type = 1; //1分享链接 2 二维码
$openid = trim($params['openid'] ?? '');
$requestIp = Util::getRequestIp();
if ($openid && !in_array($requestIp, ['47.104.149.49', '116.62.25.128', '47.101.137.78'])){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数错误");
    exit();
}


if ($loginName == "" || $password == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit();
}

if ($loginDevice == "") {
    //如果没有传递loginDevice参数，则默认是网页登录
    $loginDevice = 1;
}

$id = -1;
$token = '';

try {
    $accountDB = new CDBAccount();

    ##增加验证登录失败次数
    $loginFalseCount = $accountDB->checkLoginFalseTime($loginName, 600);
    //超过五次暂时锁定账号
    if($loginFalseCount > 5){
        Util::printResult($GLOBALS['ERROR_LOGIN'], '您的账号登陆失败超过五次，已经被锁定，请十分钟后再试');
        exit;
    }elseif($loginFalseCount > 3){
        //超过三次需要验证码
        //判断有没有验证码
            if (!isset($params['captcha']) || !isset($params['captchaSession']) ) {
                //没有captcha
                Util::printResult($GLOBALS['ERROR_OVER_SPEED'], "登录失败超过三次，请输入验证码验证您的身份");
                exit;
            } else {

                $tempStorage = new TempStorage();
                $rightCaptcha = $tempStorage->get($params['captchaSession']);

                if ($rightCaptcha !== strtolower($params['captcha']) ){
                    // 图形验证码错误
                    $tempStorage->del($params['captchaSession']);
                    Util::printResult($GLOBALS['ERROR_CAPTCHA'], "验证码输入错误");
                    exit;
                }
                // 删除session
                $tempStorage->del($params['captchaSession']);
            }
    }
    //登录失败小于3次，则继续执行下面的程序
    ##增加验证登录失败次数

    $id = $accountDB->userLogin($loginName, hash("sha256", $password . $GLOBALS['SHA256_SALT'], false));

    // if($id < 0)
    //     //TODO, 适配原来的错误,正式运行时可删除这个id
    //     $id = $accountDB->userLogin($loginName, hash("sha256",$password,false));

    if ($id > 0) {
        //如果是通过分享链接登录，则自动绑定身份
        if ($shareCode != null && $shareCode != "") {
            $personDB = new CDBPerson();

            if ($personDB->bindUserToPerson($id, $shareCode, $fromUserId, $type) <= 0) {
                //绑定人物失败,关闭报错
                // Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "绑定人物失败");
                // exit();
            }
        }
        //生成token
        $token = hash("sha256", $id . Util::generateRandomCode(24) . $password . $GLOBALS['SHA256_SALT'], false);


        if ($accountDB->insertLoginHistory($loginName, $loginDevice, $id, $token, Util::getRequestIp(), Util::getCurrentTime(), 1, $deviceCode) > 0) {

            // 登录成功后，重置限速的bucket
            $requestIP = $_SERVER["REMOTE_ADDR"];
            $limiter = $rateLimitList[$params['sub_action']];
            $rateLimit = new RateLimit($GLOBALS['RATE_LIMIT_SHA']);
            $rateLimit->resetBucket('rl_' . $params['sub_action'] . $requestIP, $limiter);

            //设置token 添加到redis 用于登陆身份识别
            $TempStorage = new TempStorage();
            $TempStorage->setTemp($token, $id,7200); //两小时
            //设置token 添加到redis 用于登陆身份识别


            $info = $accountDB->getUserInfoById($id);
            $info['id_token'] = $id . "|" . $token . "|" . $loginDevice;
            $info['passwordStrength'] = Check::passwordStrength($password, false);

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据库出错，登录失败");
        }
    } else if ($id == -2) {
        Util::printResult($GLOBALS['ERROR_LOGIN'], "账号不存在");
    } else if ($id == -1) {
        //登录失败
        $id = $accountDB->getUserIdByLoginName($loginName);
        $accountDB->insertLoginHistory($loginName, $loginDevice, $id, $token, Util::getRequestIp(), Util::getCurrentTime(), 0, $deviceCode);
        Util::printResult($GLOBALS['ERROR_LOGIN'], "登录失败,密码错误");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
