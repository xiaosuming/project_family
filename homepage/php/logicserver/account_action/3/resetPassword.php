<?php
/* 通过邮件找回密码
 * author:liuzhenhao
 * date:2020-08-14
 */

use Util\Util;
use Util\Check;
use DB\CDBAuth;
use DB\CDBAccount;

$oldPassword = Check::check(trim(isset($params["oldPassword"]) ? $params["oldPassword"] : ''));
$newPassWord = Check::check(trim(isset($params['newPassword']) ? $params['newPassword'] : ''), 6);
$userId = $GLOBALS['userId'];

try {
    if ($newPassWord != "" && $newPassWord != "") {

        $accountDB = new CDBAccount();
        $password = $accountDB->getPassword($userId);
        if ($password !== hash("sha256", $oldPassword . $GLOBALS['SHA256_SALT'], false)){
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "密码错误");
            exit;
        }

        $newPassWordHash = hash("sha256", $newPassWord . $GLOBALS['SHA256_SALT'], false);

        if ($password === $newPassWordHash){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "新密码和旧密码不能相同");
            exit;
        }

        $result = $accountDB->updateUserPassword($userId,$newPassWordHash);


        if ($result >= 0){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], "更改密码成功");
            exit;
        }else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "更改密码失败");
            exit;
        }
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}