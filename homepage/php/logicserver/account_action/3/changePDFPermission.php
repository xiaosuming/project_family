<?php
/*实现修改用户信息
* @author liuzhenhao
*date:2020/8/14
*/

use DB\CDBAccount;
use DB\CDBArea;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];

$phoneList = Check::checkArray(json_decode(trim($params['phoneList']) ?? null));
$pdfPermission = Check::checkInteger(trim(isset($params['pdfPermission'])) ? trim($params['pdfPermission']) : null,'pdfPermission',true,true);

foreach ($beUpdateUserId as $id){
    Check::checkArray(trim(isset($id)) ? trim($id) : '');
}

try {

    $updateData = array_filter([
        'pdf_permission'=>$pdfPermission,
    ],function ($v){
        if ($v === null){
            return false;
        }
        return true;
    });

    if (!$updateData || !$phoneList || !in_array($pdfPermission, ['0','1',0,1])){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $accountDB = new CDBAccount();
    $useInfo = $accountDB->getUserInfoV3($userId);
    $allowAccountPhone = ['15502127740', '15921755359'];
    if (!in_array($useInfo['phone'], $allowAccountPhone)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户无权限");
        exit;
    }
    $beUpdateUserId = $accountDB->getUserIdsByPhone($phoneList);
    if (sizeof($beUpdateUserId) !== sizeof($phoneList)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户手机错误");
        exit;
    }
//    print_r($updateData);
    $count = $accountDB->updatePdfPermission($userId, $updateData, array_column($beUpdateUserId, 'id'));
    $data['count'] = $count;
    if ($count >= 0) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "更新出错");
        exit;
    }

} catch (PDOException $e) {
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
