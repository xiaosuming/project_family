<?php
/*实现修改用户信息
* @author liuzhenhao
*date:2020/8/14
*/

use DB\CDBAccount;
use DB\CDBArea;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];

$birthday = Check::checkDateV3(trim(isset($params['birthday'])) ? trim($params['birthday']) : null,true,true);
$address = Check::check(trim(isset($params['address'])) ? trim($params['address']) : null, 0, 100);
$birthPlace = Check::check(trim(isset($params['birthPlace'])) ? trim($params['birthPlace']) : null, 0, 100);
$gender = Check::checkInteger(trim(isset($params['gender']) ? $params['gender'] : null),'gender',true,true);
$maritalStatus = Check::checkInteger(trim(isset($params['maritalStatus']) ? $params['maritalStatus'] : null),'maritalStatus',true,true);
$realName = Check::check(trim(isset($params['realName'])) ? trim($params['realName']) : null, 0, 30);
$nickname = Check::check(trim(isset($params['nickname'])) ? trim($params['nickname']) : null, 0, 30);
$photo = Check::check(trim(isset($params['photo'])) ? trim($params['photo']) : null, 0, 255);
$defaultFamilyId = Check::checkInteger(trim(isset($params['defaultFamilyId']) ? $params['defaultFamilyId'] : null),'defaultFamilyId',true,true);

try {

    $updateData = array_filter([
        'name'=>$name,
        'birthday'=>$birthday,
        'maritalStatus'=>$maritalStatus,
        'birthPlace'=>$birthPlace,
        'address'=>$address,
        'gender'=>$gender,
        'realName'=>$realName,
        'nickname'=>$nickname,
        'defaultFamilyId'=>$defaultFamilyId
    ],function ($v){
        if ($v === null){
            return false;
        }
        return true;
    });

    if (!$updateData){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $accountDB = new CDBAccount();
//    print_r($updateData);
    $count = $accountDB->updateUserInfoV3($userId, $updateData);
    $data['count'] = $count;
    if ($count >= 0) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "更新出错");
        exit;
    }

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
