<?php
/** @noinspection ALL */

/**
 * 更新家族信息
 * @author liuzhenhao
 * @date 2020-09-03
 */

use DB\CDBFamily;
use DB\CDBAccount;
use Util\Check;
use Util\Util;

$username = Check::check(trim(isset($params['username'])) ? trim($params['username']) : null);
$page = Check::checkInteger(trim(isset($params['page']) ? $params['page'] : 1));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 20));

if ($pageSize > 99) {
    $pageSize = 99;
}


try {


        $userId = $GLOBALS['userId'];
        $accountDB = new CDBAccount();

        $filterData = array_filter([
            'username' => $username,
        ],function ($v){
            if ($v != null){
                return true;
            }
        });
        $userList = $accountDB->getUserList($filterData,$page,$pageSize);

        $data = $userList;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
