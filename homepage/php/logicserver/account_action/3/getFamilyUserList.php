<?php /** @noinspection ALL */

/**
 * 更新家族信息
 *  @author liuzhenhao
 *  @date 2020-07-13
 */

use DB\CDBFamily;
use DB\CDBAccount;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : '')); //必须
$permission = Check::checkInteger(trim(isset($params['permission']) ? $params['permission'] : null),'permission',true,true);
$createTime = Check::checkInteger(trim(isset($params['createTime']) ? $params['createTime'] : null),'createTime',true,true);
$isBind = Check::checkInteger(trim(isset($params['isBind']) ? $params['isBind'] : null),'isBind',true,true);
$name = Check::check(trim(isset($params['name'])) ? trim($params['name']) : null);
$nameRange = Check::checkInteger(trim(isset($params['nameRange']) ? $params['nameRange'] : null),'nameRange',true,true);
$page = Check::checkInteger(trim(isset($params['page']) ? $params['page'] : 1));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 20));

if ($pageSize > 99){
    $pageSize = 99;
}

if (($name && !$nameRange) || (!$name && $nameRange) && !in_array($nameRange,['2','3'])){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "传参错误");
    exit;
}


try {

    if ($familyId != "") {

        $userId = $GLOBALS['userId'];
        $accountDB = new CDBAccount();
//        $family = $accountDB->getUserByFamily($familyId);

        $familyDB = new CDBFamily();

        $family = $familyDB->getFamilyById($familyId);
        if (!$family){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "家族不存在");
            exit;
        }

        if (!$familyDB->getUserPermission($userId, $familyId)){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "用户权限错误");
            exit;
        }

        $userList = $accountDB->getFamilyUserList($familyId, $page,$pageSize,$permission,$isBind,$createTime,$name,$nameRange);

        $data = $userList;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
