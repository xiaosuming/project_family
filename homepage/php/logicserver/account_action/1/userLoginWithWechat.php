<?php

use Util\Util;
use DB\CDBAccount;
use DB\CDBPerson;
use Util\Check;
use Util\TempStorage;

$auth = Check::check(trim(isset($params['auth']) ? $params['auth'] : ''));
$shareCode = Check::check(trim($params['sharecode'] ?? ''));     //登录参数中也可以携带code值

$fromUserId = Check::checkInteger($params['fromUserId'] ?? 0); //邀请人的用户id
$type = 1; //1分享链接 2 二维码

if ($auth == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit();
}

if (!isset($loginDevice) || $loginDevice == "") {
    //如果没有传递loginDevice参数，则默认是网页登录
    $loginDevice = $GLOBALS['LOGIN_DEVICE_WECHAT'];
}

$id = -1;
$token = '';

try {
    $accountDB = new CDBAccount();

    //从临时存储中获取用户资料
    $storage = new TempStorage();
    $userInfo = $storage->get($auth);

    if ($userInfo) {
        $userInfo = json_decode($userInfo, true);
    } else {
        Util::printResult($GLOBALS['ERROR_LOGIN'], "账号不存在");
        exit();
    }

    if ($userInfo['unionid'] == '') {
        // unionid为空，通过openid登录
        $id = $accountDB->userLoginWithWechat($userInfo['openid']);
    } else {
        $id = $accountDB->userLoginWithUnionId($userInfo['unionid']);
    }


    if ($id > 0) {
        //如果是通过分享链接登录，则自动绑定身份
        if ($shareCode != null && $shareCode != "") {

            $personDB = new CDBPerson();

            if ($personDB->bindUserToPerson($id, $shareCode, $fromUserId, $type) <= 0) {
                //绑定人物失败
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "绑定人物失败");
                exit();
            }


        }
        //生成token
        $token = hash("sha256", $id . Util::generateRandomCode(24) . $auth . $GLOBALS['SHA256_SALT'], false);


        if ($accountDB->insertLoginHistory("微信登录", $loginDevice, $id, $token, Util::getRequestIp(), Util::getCurrentTime(), 1) > 0) {
            $info = $accountDB->getUserInfoById($id);
            $info['id_token'] = $id . "|" . $token . "|" . $loginDevice;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据库出错，登录失败");
        }
    } else {
        Util::printResult($GLOBALS['ERROR_LOGIN'], "账号不存在");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
