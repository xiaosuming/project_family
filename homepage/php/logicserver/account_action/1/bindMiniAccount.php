<?php
/**
 * 绑定小程序账号
 * author jiangpengfei
 * date 2018-05-15
 */

use Util\Util;
use Util\Check;
use DB\CDBAccount;

$loginName = Check::check(trim(isset($params['loginName']) ? $params['loginName'] : ''), 1);
$password = Check::check(trim(isset($params['password']) ? $params['password'] : ''), 1);

$userId = $GLOBALS['userId'];

try {
    $accountDB = new CDBAccount();
    $result = $accountDB->moveMiniAccount($loginName, $password, $userId);

    if ($result < 0) {
        $msg = "";
        switch($result) {
            case -1:
                $msg = "账号密码错误或者不存在";
            break;
            case -2:
                $msg = "账号已绑定小程序";
            break;
            case -3:
                $msg = "出现异常";
            break;
            case -4:
                $msg = "小程序账号已经绑定了手机或邮箱";
            break;
        }

        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], $msg);
    } else {
        $data['success'] = $result;

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }


} catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}