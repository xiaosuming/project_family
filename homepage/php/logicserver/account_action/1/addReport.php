<?php 
/**
 * 用户举报
 */
    use Util\Util;
    use Util\ImageUpload;
    use DB\CDBReport;
    use Util\Check;
    use DB\CDBZone;

    $userId = $GLOBALS["userId"];
    $moduleId = Check::checkModule(isset($params["moduleId"]) ? $params["moduleId"] : '');
    $recordId = Check::checkInteger(isset($params["recordId"]) ? $params["recordId"] : '');
    $reportType = Check::checkInteger(isset($params["reportType"]) ? $params["reportType"] : '');

    try{
        if($moduleId == "" || $recordId == "" || $reportType == ""){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }

        $reportDB = new CDBReport();

        if(($reportId = $reportDB->checkUserReportExist($moduleId,$recordId,$reportType,$userId)) > 0){
            $data['reportId'] = $reportId;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        }

        $reportId = $reportDB->addReport($moduleId,$recordId,$reportType,$userId);

        switch($moduleId){
            case 5:
                if($reportDB->getReportsTotal($moduleId,$recordId,-1) >= $GLOBALS['MAX_REPORT']){
                    //暂时删除用户的推文
                    $zoneDB = new CDBZone();
                    $zoneDB->setPostOverReport($recordId);
                }
                break;
        }

        $data['reportId'] = $reportId;
        if ($reportId > 0)
        { 
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '举报失败，系统出现异常');
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }