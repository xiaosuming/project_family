<?php
use Util\Util;
use Util\Check;
use DB\CDBAccount;
use DB\CDBAuth;
use DB\InstantMessageDB;
use Util\TempStorage;
use Util\Http;
use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;
use Model\User;
use ThirdParty\InstantMessage;
use DB\CDBInvitationCode;
use DB\CDBUserInvitationCode;
use Util\PhotoGenerator;

$phone = Check::checkPhoneNumber(trim(isset($params['phone']) ? $params['phone'] : ''));
$vcode = trim(isset($params['vcode']) ? $params['vcode'] : '');     //验证码
$password = Check::check(trim(isset($params['password']) ? $params['password'] : ''), 6, -1);
$invitationCode = trim($params['invitationCode'] ?? '');

//下面是微信特有的参数
$auth = Check::check(trim(isset($params['auth']) ? $params['auth'] : ''));

$invitationDB = null;

if ($GLOBALS['TEST_MODE']) {
    if ($invitationCode === "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "邀请码不存在");
        exit();
    }
    //检查邀请码
    $codeLen = strlen($invitationCode);

    if ($codeLen == 6) {
        $invitationDB = new CDBInvitationCode();
        if (!$invitationDB->checkInvitationCode($invitationCode)) {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "邀请码不正确");
            exit();
        }
    } elseif ($codeLen == 8) {
        $invitationDB = new CDBUserInvitationCode();
        if (!$invitationDB->checkInvitationCode($invitationCode)) {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "邀请码不正确");
            exit();
        }
    } else {
        Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "邀请码格式不正确");
        exit();
    }
}

//根据凭证获取用户信息
$storage = new TempStorage();
$userInfo = $storage->get($auth);
$username = null;
$openid = null;
$unionId = null;
$photo = null;
$gender = null;
$country = null;
$province = null;
$city = null;

if ($userInfo) {
    $userInfo = json_decode($userInfo, true);
    $username = '';
    $usernameExist = true;
    while ($usernameExist) {
        $username = $userInfo['username']. Util::generateRandomCode(8);
        if ($accountDB->checkUserLoginName($username)){
            //已经存在则继续重复生成
            $usernameExist = true;
        }else{
            $usernameExist = false;
        }
    }
    $openid = $userInfo['openid'];
    $unionId = $userInfo['unionid'];
    $photo = $userInfo['photo'];
    $gender = $userInfo['gender'];
    $country = $userInfo['country'];
    $province = $userInfo['province'];
    $city = $userInfo['city'];
} else {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit();
}

$shareCode = Check::check(trim($params['sharecode'] ?? ''));     //注册参数中也可以携带sharecode值;
$fromUserId = Check::checkInteger($params['fromUserId'] ?? 0); //邀请人的用户id
$type = 1; //1分享链接 2 二维码
//email设置为空
$email = "";

try {
    $accountDB = new CDBAccount();

    if ($phone == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "手机号缺少");
        exit();
    }

    if ($password == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "密码缺少");
        exit();
    }

    if ($vcode == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "验证码缺少");
        exit();
    }
    if ($username == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "用户名缺少");
        exit();
    }
    if ($openid == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "openid缺少");
        exit();
    }
    if ($gender == "") {
        $gender = 0;        //性别缺少默认为女
    }
    if ($photo == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "照片缺少");
        exit();
    }


    //该手机是否存在的判断
    if ($accountDB->checkUserLoginName($phone)) {
        Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEPHONE'], "手机号重复");
        exit();
    }

    //检查微信账号是否绑定
    if ($accountDB->checkWechatAccountExist($openid)) {
        Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEWECHAT'], "微信已绑定");
        exit();
    }



    $authDao = new CDBAuth();

    //手机号注册,验证码
    //$rightVcode = $authDao->getPhoneAuth($phone);
    $jsonStr = $authDao->getPhoneAuth($phone);
    $arr = json_decode($jsonStr, true);
    $rightVcode = $arr['v'];
    $recordId = $arr['r'];

    if (!$rightVcode || $rightVcode != $vcode) {
        //验证码不存在，或者验证码错误
        Util::printResult($GLOBALS['ERROR_VCODE'], "注册失败，验证码错误");
        exit;
    } else {
        //成功则删除
        $authDao->deletePhoneAuth($phone);  //从redis删除这个验证码
        $accountDB->updatePhoneVcodeRecord($recordId);
    }

    //构造用户对象
    $user = new User();
    $user->username = $username;
    $user->email = $email;
    $user->phone = $phone;
    $user->password = hash("sha256", $password.$GLOBALS['SHA256_SALT'], false);
    $user->openid = $openid;
    $user->gender = $gender;
    $user->unionid = $unionId;
    //TODO 将地址变成int保存
    //$user->country = $country;
    //$user->province = $province;
    //$user->city = $city;
    $user->country = 0;
    $user->province = 0;
    $user->city = 0;

    //将微信上的头像下载下来并传到图片服务器
    $buffer = Http::downloadToBuffer($photo);
    $storageClient = new StorageFacadeClient();
    $storageClient->setBucket(StorageBucket::$IMAGE);
    $uploadResult = $storageClient->uploadBlob($buffer, "png");
    $user->photo = $uploadResult->location;

    /*已弃用
    //注册环信的用户
    $imUsername = md5("izuqun_".$user->username . "_" . Util::generateRandomCode(10));
    $imPassword = Util::generateRandomCode(20);
    //将用户名和密码推到任务队列中
    $imdb = new InstantMessageDB();
    if (!$imdb->pushUserTask($imUsername, $imPassword, $user->photo, $username)) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
        exit();
    }*/
    //获取腾讯云通讯账号
    $IM = $accountDB->getTencentIMName();

    $user->hxUsername = $IM['name'];
    $user->hxPassword = '';

    //是否通过分享注册的判断
    $registerId = -1;
    if ($shareCode != null && $shareCode != "") {
        $registerId=$accountDB->userRegisterWithShareCode($user, $shareCode,$fromUserId,$type);
    } else {
        $registerId=$accountDB->userRegister($user);
    }

    //绑定腾讯云通讯账号
    $accountDB->updateTencentIMUserId($IM['id'], $registerId);

    if ($GLOBALS['TEST_MODE']) {
        //更改邀请码的状态
        $invitationDB->bindUserIdToInvitationCode($registerId, $invitationCode);
    }
    $data['success'] = true;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
