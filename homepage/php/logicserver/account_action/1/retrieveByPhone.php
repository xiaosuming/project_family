<?php
/* 通过手机号找回密码
 * author:jiangpengfei
 * data:2017-04-17
 */

use Util\Util;
use Util\Check;
use DB\CDBAuth;
use DB\CDBAccount;

$vcode = Check::check(isset($params["vcode"]) ? $params["vcode"] : '');
$phone = Check::checkPhoneNumber($params['phone'] ?? '');
$newPassWord = Check::check(trim(isset($params['newPassword']) ? $params['newPassword'] : ''), 6);
try {
    if ($vcode != "" && $newPassWord != "" && $phone != "") {
        $authDB = new CDBAuth();

        //$rightVcode = $authDB->getPhoneAuth($phone);     //获取正确的vcode
        $jsonStr = $authDB->getPhoneAuth($phone);
        $arr = json_decode($jsonStr, true);
        $rightVcode = $arr['v'];
        $recordId = $arr['r'];
        if ($vcode != $rightVcode) {
            //如果验证码不同
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "更改密码失败,验证错误");
            exit();
        }

        $accountDB = new CDBAccount();
        $userId = $accountDB->getUserIdByPhone($phone);

        if ($userId == null) {
            //用户id不存在
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "更改密码失败,手机账户不存在");
            exit;
        }

        
        ##添加密码复杂度验证
        $newPassWord = Check::passwordStrength($newPassWord);
        ##添加密码复杂度验证

        if ($accountDB->updateUserPassword($userId, hash("sha256", $newPassWord . $GLOBALS['SHA256_SALT'], false)) >= 0) {
            $authDB->deletePhoneAuth($phone);
            $accountDB->updatePhoneVcodeRecord($recordId);
            Util::printResult($GLOBALS['ERROR_SUCCESS'], "更改密码成功");
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "更改密码失败");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
