<?php
    use Util\Util;
    use Util\Check;
    $sharecode = Check::check(trim(isset($params['sharecode']) ? $params['sharecode'] : ''));
    if($sharecode != ""){
        setcookie("sharecode",$sharecode,time()+60*30);        //有效期30分钟
        Util::printResult($GLOBALS['ERROR_SUCCESS'],"设置cookie成功");
    }