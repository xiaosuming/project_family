<?php
/**
 * 获取用户积分
 * author:jiangpengfei
 * date: 2017-11-30
 */
    use DB\CDBPoint;
    use Util\Util;
    use Util\Check;

    $userId = $GLOBALS['userId'];
    
    try{
        $pointDB = new CDBPoint();
        $userPoint = $pointDB->getUserPointInfo($userId);
        if($userPoint != null){
            $data['point'] = $userPoint;
            Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'],"查询错误");
        }
    }catch(PDOException $e){
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'],"出现异常");
    }