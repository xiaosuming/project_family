<?php 
    use Util\Util;
    use Util\ImageUpload;
    use DB\CDBAccount;
    $id = $GLOBALS["userId"];
    $photo = isset($params["photo"]) ? $params["photo"] : '';

    try{
        if($photo != ""){
            $accountDB = new CDBAccount();

            $updateRow = $accountDB->addPhotoforUser($id,$photo);
            $data['updateRow'] = $updateRow;
            if ($updateRow > 0)
            { 
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], null);
            }
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }