<?php

use Minho\Captcha\CaptchaBuilder;
use Util\TempStorage;
use Util\Util;

$captch = new CaptchaBuilder();

$captch->initialize([
    'width' => 150,     // 宽度
    'height' => 50,     // 高度
    'line' => false,    // 直线
    'curve' => true,    // 曲线
    'noise' => 1,       // 噪点背景
    'fonts' => []       // 字体
]);

session_start();
$captch->create();

$tempStorage = new TempStorage();
$code = Util::generateRandomCode(20);
$tempStorage->setTemp($code,$captch->getText(),300);
header("captcha_session:$code");
header("Access-Control-Expose-Headers:captcha_session");

$captch->output(1);
