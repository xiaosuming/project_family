<?php
/**
 * 通过验证码来绑定小程序账号
 * @Author: jiangpengfei
 * @Date: 2018-10-10
 */

use Util\Util;
use Util\Check;
use DB\CDBAccount;
use DB\CDBAuth;

$phone = Check::check(trim(isset($params['phone']) ? $params['phone'] : ''), 1);
$vcode = Check::check(trim(isset($params['vcode']) ? $params['vcode'] : ''), 1);

$userId = $GLOBALS['userId'];

try {

    // 检查phone是否存在用户
    $accountDB = new CDBAccount();

    $tmpUserId = $accountDB->getUserIdByPhone($phone);

    if ($tmpUserId == null) {
        // 不存在该手机号
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '手机号不存在');
        exit;
    }

    // 检查手机号和验证码的正确性
    $authDao = new CDBAuth();
//    $rightVcode = $authDao->getPhoneAuth($phone);
    $jsonStr = $authDao->getPhoneAuth($phone);
    $arr = json_decode($jsonStr, true);
    $rightVcode = $arr['v'];
    $recordId = $arr['r'];

    if (!$rightVcode || $rightVcode !== $vcode) {
        //验证码不存在，或者验证码错误
        Util::printResult($GLOBALS['ERROR_VCODE'], "登录失败，验证码错误");
        exit;
    } else {
        //成功则删除
        $authDao->deletePhoneAuth($phone);  //从redis删除这个验证码
        $accountDB->updatePhoneVcodeRecord($recordId);
    }

    $result = $accountDB->moveMiniAccountWithPhone($phone, $userId);

    if ($result < 0) {
        $msg = "";
        switch ($result) {
            case -1:
                $msg = "账号密码错误或者不存在";
                break;
            case -2:
                $msg = "账号已绑定小程序";
                break;
            case -3:
                $msg = "出现异常";
                break;
            case -4:
                $msg = "小程序账号已经绑定了手机或邮箱";
                break;
        }

        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], $msg);
    } else {
        $data['success'] = $result;

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
