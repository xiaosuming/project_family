<?php

use Util\Util;
use Util\Check;
use DB\CDBAccount;
use DB\CDBAuth;

$userId = $GLOBALS["userId"];
$phone = Check::checkPhoneNumber(trim(isset($params['phone']) ? $params['phone'] : ''));
$vcode = trim(isset($params['vcode']) ? $params['vcode'] : '');
$password = trim(isset($params['password']) ? $params['password'] : '');

try {
    $accountDB = new CDBAccount();
    if ($phone === "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "2缺少参数");
        exit();
    } else if ($accountDB->checkUserLoginName($phone)) {
        Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEPHONE'], "手机号重复");
        exit();
    }
    $authDao = new CDBAuth();
    //$rightVcode = $authDao->getPhoneAuth($phone);
    $jsonStr = $authDao->getPhoneAuth($phone);
    $arr = json_decode($jsonStr, true);
    $rightVcode = $arr['v'];
    $recordId = $arr['r'];

    if (!$rightVcode || $rightVcode !== $vcode) {
        //验证码不存在，或者验证码错误
        Util::printResult($GLOBALS['ERROR_VCODE'], "注册失败，验证码错误");
        exit;
    } else {
        //成功则删除
        $authDao->deletePhoneAuth($phone);  //从redis删除这个验证码
        $accountDB->updatePhoneVcodeRecord($recordId);
    }

    $password = hash("sha256", $password . $GLOBALS['SHA256_SALT'], false);
    $accountDB->updatePhoneAndPassword($phone, $password, $userId);
    Util::printResult($GLOBALS['ERROR_SUCCESS'], "绑定成功");
    exit();
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
?>
