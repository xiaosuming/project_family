<?php
/**
 * Php version 7.0
 * @author jiangpengfei
 * 用户邮箱注册账号激活
 */
use Util\Util;
use DB\CDBAuth;
use Model\User;
use DB\CDBAccount;
use DB\InstantMessageDB;
use Util\PhotoGenerator;

$auth = isset($params["auth"]) ? $params["auth"] : '';
$fromUserId = Check::checkInteger($params['fromUserId'] ?? 0); //邀请人的用户id
$type = 1; //1分享链接 2 二维码

try {
    if ($auth != "") {
        $accountDB = new CDBAccount();
        $authDao = new CDBAuth();

        $userInfoStr = $authDao->getEmailAuth($auth);
        if ($userInfoStr) {
            //拿到后删除
            $userInfo = json_decode($userInfoStr, true);
            $shareCode = $userInfo['s'];
            $registerId = 0;

            //构造用户对象
            $user = new User();
            $user->username = $userInfo['u'];
            $user->email = $userInfo['e'];
            $user->password = $userInfo['p'];
            $user->photo = PhotoGenerator::generateUserPhotoFromResource();

            //注册即时聊天的用户
            $imUsername = md5("izuqun_".$user->username . "_" . Util::generateRandomCode(10));
            $imPassword = Util::generateRandomCode(20);
            //将用户名和密码推到任务队列中
            $imdb = new InstantMessageDB();
            if (!$imdb->pushUserTask($imUsername, $imPassword, $user->photo, $user->username)) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
                exit();
            }

            $user->hxUsername = $imUsername;
            $user->hxPassword = $imPassword;
            

            if ($shareCode != null && $shareCode != "") {
                //通过分享码注册
                $registerId = $accountDB->userRegisterWithShareCode($user, $shareCode, $fromUserId, 1);
            } else {
                //正常注册
                $registerId = $accountDB->userRegister($user);
            }
            if ($registerId > 0) {
                //注册成功后删除邮箱验证
                $authDao->deleteEmailAuth($auth);
                Util::printResult($GLOBALS['ERROR_SUCCESS'], "注册成功");
                exit;
            } else {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
                exit;
            }
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "注册信息不存在");
            exit;
        }
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
