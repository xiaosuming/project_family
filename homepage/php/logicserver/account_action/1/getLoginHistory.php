<?php 
    /**
     * 分页获取用户登录历史
     * @author jiangpengfei
     * @date 2017-01-04
     */
    use Util\Util;
    use Util\Pager;
    use Util\Check;
    use DB\CDBAccount;
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 10));
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));

    try{
        $accountDB = new CDBAccount();
        $userId = $GLOBALS['userId'];
        
        $total = $accountDB->getLoginTimes($userId);
        $paging = $accountDB->getLoginHistory($userId, $pageIndex, $pageSize);
        
        $pager = new Pager($total,$paging,$pageIndex,$pageSize);
        $pager->printPage();

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }