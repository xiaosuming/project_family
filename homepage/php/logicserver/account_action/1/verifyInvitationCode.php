<?php

/**
 * 验证邀请码是否正确
 */

    use Util\Util;
    use Util\Check;
    use DB\CDBInvitationCode;
    use DB\CDBUserInvitationCode;

    $invitationCode = Check::check(trim(isset($params['invitationCode']) ? $params['invitationCode'] : ''));

    $codeLen = strlen($invitationCode);

    if($codeLen == 6){
        $invitationDB = new CDBInvitationCode();
        if(!$invitationDB->checkInvitationCode($invitationCode)){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "邀请码不正确");
            exit();
        }
    }else if($codeLen == 8){
        $invitationDB = new CDBUserInvitationCode();
        if(!$invitationDB->checkInvitationCode($invitationCode)){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "邀请码不正确");
            exit();
        }
    }else{
        Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "邀请码格式不正确");
        exit();
    }

    Util::printResult($GLOBALS['ERROR_SUCCESS'], "邀请码验证成功");


?>