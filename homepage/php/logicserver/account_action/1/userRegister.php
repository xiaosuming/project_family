<?php

use Util\Util;
use Util\Check;
use DB\CDBAccount;
use DB\CDBAuth;
use DB\MailDB;
use DB\InstantMessageDB;
use DB\CDBInvitationCode;
use Model\User;
use DB\CDBUserInvitationCode;
use Util\PhotoGenerator;

$registerType = Check::check(trim(isset($params['type']) ? $params['type'] : ''));        //注册方式，1为邮箱，2为手机
if ($registerType === $GLOBALS['REGISTER_TYPE_EMAIL']) {
    $email = Check::checkEmail(trim(isset($params['email']) ? $params['email'] : ''));
} elseif ($registerType === $GLOBALS['REGISTER_TYPE_PHONE']) {
    $phone = Check::checkPhoneNumber(trim(isset($params['phone']) ? $params['phone'] : ''));
    $vcode = trim(isset($params['vcode']) ? $params['vcode'] : '');
} else {
    //不存在的注册方式
    Util::printResult($GLOBALS['ERROR_REGISTER_TYPE'], "不存在的注册方式");
    exit();
}
$username = isset($params['username']) ? $params['username'] : '';

$password = Check::check(trim(isset($params['password']) ? $params['password'] : ''), 6, -1);

$invitationCode = trim($params['invitationCode'] ?? '');


$invitationDB = null;
if ($GLOBALS['TEST_MODE'] || $invitationCode != "") {
    if ($GLOBALS['TEST_MODE'] && $invitationCode === "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "邀请码不存在");
        exit();
    }
    //检查邀请码
    $codeLen = strlen($invitationCode);

    if ($codeLen == 6) {
        $invitationDB = new CDBInvitationCode();
        if (!$invitationDB->checkInvitationCode($invitationCode)) {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "邀请码不正确");
            exit();
        }
    } elseif ($codeLen == 8) {
        $invitationDB = new CDBUserInvitationCode();
        if (!$invitationDB->checkInvitationCode($invitationCode)) {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "邀请码不正确");
            exit();
        }
    } else {
        Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "邀请码格式不正确");
        exit();
    }
}

$shareCode = Check::check(trim($params['sharecode'] ?? ''));     //注册参数中也可以携带sharecode值;
$fromUserId = Check::checkInteger($params['fromUserId'] ?? 0); //邀请人的用户id
$type = 1; //1分享链接 2 二维码

try {
    $accountDB = new CDBAccount();


    if ($username === '') {
        //由后台生成
        $usernameExist = true;
        while ($usernameExist) {
            $username = 'user_' . Util::generateRandomCode(10);
            if ($accountDB->checkUserLoginName($username)) {
                //已经存在则继续重复生成
                $usernameExist = true;
            } else {
                $usernameExist = false;
            }
        }
    } else {
        //检查规则
        Check::checkUserName($username);
    }

    if ($GLOBALS['REGISTER_TYPE_EMAIL'] == $registerType) {
        //通过邮箱注册
        if ($email === "" || $password === "") {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit();
        }

        $phone = "";
        //该邮箱是否存在的判断
        if ($accountDB->checkUserLoginName($email)) {
            Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEEMAIL'], "邮箱重复");
            exit();
        }
    } elseif ($GLOBALS['REGISTER_TYPE_PHONE'] == $registerType) {
        //通过手机注册
        if ($phone === "" || $password === "") {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit();
        }

        $email = "";
        //该手机是否存在的判断
        if ($accountDB->checkUserLoginName($phone)) {
            Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEPHONE'], "手机号重复");
            exit();
        }
    } else {
        //不存在的注册方式
        Util::printResult($GLOBALS['ERROR_REGISTER_TYPE'], "不存在的注册方式");
        exit();
    }

    //如果填写了用户名
    if ($username !== "") {
        //检查用户名是否注册
        if ($accountDB->checkUserLoginName($username)) {
            Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEUSERNAME'], "用户名重复");
            exit();
        }
    }

    $authDao = new CDBAuth();
    if ($registerType === $GLOBALS['REGISTER_TYPE_EMAIL']) {
        //邮箱注册
        $auth = Util::generateRandomCode(60);
        if ($authDao->addEmailAuth($auth, $username, hash("sha256", $password . $GLOBALS['SHA256_SALT'], false), $email, $shareCode)) {
            $mailDB = new MailDB();
            if ($mailDB->pushMail($email, Util::generateRegisterEmail($auth, $username))) {
                $data['success'] = true;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                exit();
            } else {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
                exit();
            }
        } else {
            Util::printResult($GLOBALS['ERROR_INSERT_REDIS'], "插入失败");
            exit();
        }
    } elseif ($registerType === $GLOBALS['REGISTER_TYPE_PHONE']) {
        //手机号注册,验证码
        //$rightVcode = $authDao->getPhoneAuth($phone);
        $jsonStr = $authDao->getPhoneAuth($phone);
        $arr = json_decode($jsonStr, true);
        $rightVcode = $arr['v'];
        $recordId = $arr['r'];

        if (!$rightVcode || $rightVcode !== $vcode) {
            //验证码不存在，或者验证码错误
            Util::printResult($GLOBALS['ERROR_VCODE'], "注册失败，验证码错误");
            exit;
        } else {
            //成功则删除
            $authDao->deletePhoneAuth($phone);  //从redis删除这个验证码
            $accountDB->updatePhoneVcodeRecord($recordId);
        }

        ##添加注册时密码复杂度验证
        $password = Check::passwordStrength($password);
        ##添加注册时密码复杂度验证


        //构造用户对象
        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->phone = $phone;
        $user->password = hash("sha256", $password . $GLOBALS['SHA256_SALT'], false);
        $user->photo = PhotoGenerator::generateUserPhotoFromResource();
        
        /*已弃用
        //生成环信的用户名和密码 
        $imUsername = md5("izuqun_" . $user->username . "_" . Util::generateRandomCode(10));
        $imPassword = Util::generateRandomCode(20);
        //将用户名和密码推到任务队列中
        $imdb = new InstantMessageDB();
        if (!$imdb->pushUserTask($imUsername, $imPassword, $user->photo, $username)) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
            exit();
        }*/
        //获取腾讯云通讯账号
        $IM = $accountDB->getTencentIMName();

        $user->hxUsername = $IM['name'];
        $user->hxPassword = '';


        $registerId = -1;
        //是否通过分享注册的判断
        if ($shareCode !== null && $shareCode !== "") {
            $registerId = $accountDB->userRegisterWithShareCode($user, $shareCode,$fromUserId,$type);
        } else {
            $registerId = $accountDB->userRegister($user);
        }

        if ($registerId > 0) {

            //绑定腾讯云通讯账号
            $accountDB->updateTencentIMUserId($IM['id'], $registerId);

            if ($GLOBALS['TEST_MODE'] || $invitationCode != '') {
                //更改邀请码的状态
                $invitationDB->bindUserIdToInvitationCode($registerId, $invitationCode);
            }
            $data['success'] = true;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit();
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
            exit();
        }
    } else {
        //不存在的注册方式
        Util::printResult($GLOBALS['ERROR_REGISTER_TYPE'], "不存在的注册方式");
        exit();
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
