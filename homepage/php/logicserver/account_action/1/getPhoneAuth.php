<?php

use Util\Util;
use Util\Check;
use DB\CDBAuth;
use DB\CDBAccount;
use DB\PhoneDB;

$phone = Check::checkPhoneNumber(trim($params["phone"] ?? ''));
$authType = Check::check(trim($params['type'] ?? '1'));         // 1注册，2找回密码，3登录,4是解绑 5绑定

if ($phone != "") {
    $accountDB = new CDBAccount();

    if ($authType === "1") {
        //注册时要保证账号不存在
        if ($accountDB->checkUserLoginName($phone)) {
            Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEPHONE'], "手机号重复");
            exit();
        }
    } else if ($authType === '2') {
        //找回密码时要保证账号存在
        if (!$accountDB->checkUserLoginName($phone)) {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "手机号不存在");
            exit();
        }
    } else if ($authType === "3") {
        //
    } else if ($authType === "4") {
        if (!$accountDB->checkUserLoginName($phone)) {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "和之前的手机号不符合");
            exit();
        }
    } else if($authType === "5"){

    }else {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "操作类型不存在");
        exit();
    }

    $authDao = new CDBAuth();
    $vcode = Util::generateVcode(6);    //生成6位验证码
    $order = Util::generateOrder(3);    //生成3位认证序号

    //$vcode = 123456;

    //手机号和验证码添加到记录中去
    $recordId = $accountDB->addPhoneVcodeRecord($phone, $vcode, $authType);
    if ($recordId <= 0) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '记录添加错误');
        exit;
    }
    if ($authDao->setPhoneAuthAndRecord($phone, $vcode,$recordId)) {
        //将该短信推送到短信的任务队列中
        $phoneDB = new PhoneDB();
        $result = $phoneDB->pushMessage($phone, $vcode, $order);
        if ($result) {
            $data['order'] = $order;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        } else {
            Util::printResult($GLOBALS['ERROR_PHONE_USETIMES_LIMITED'], '该电话使用次数过多，请一天之后尝试');
            exit;
        }
    }

    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "发送失败");
    exit;

} else {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数确少");
}
