<?php
/**
 * 获取用户粉丝的数组
 * @Author: jiangpengfei
 * @Date:   2018-12-06
 */

use Util\Util;
use DB\UserFollowDB;
use Util\Check;

$userId = $GLOBALS['userId'];
$pageIndex = Check::checkInteger($params['pageIndex']);
$pageSize = Check::checkInteger($params['pageSize']);

try {
    $userFollowDB = new UserFollowDB();
    $follows = $userFollowDB->getUserFollowPaging($pageIndex, $pageSize, $userId);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $follows);

} catch(PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}