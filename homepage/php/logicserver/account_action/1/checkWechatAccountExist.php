<?php 
    use Util\Util;
    use DB\CDBAccount;
    use Util\Check;
    use Util\TempStorage;

    $wechatAuth = Check::check(trim(isset($params['wechatAuth']) ? $params['wechatAuth'] : ''));      //微信凭证

    if($wechatAuth === ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit();
    }

    try{
        $account = new CDBAccount();
        $storage = new TempStorage();
        $wechatInfo = $storage->get($wechatAuth);
        $data = null;
        if($wechatInfo){
            $wechatInfo = json_decode($wechatInfo,true);    //true为map，不加为object
            $openid = $wechatInfo['openid'];
            if($account->checkWechatAccountExist($openid)){
                $data['exist'] = true;
            }else{
                $data['exist'] = false;
                $data['reason'] = "用户尚未注册";
            }
        }else{
            $data['exist'] = false;
            $data['reason'] = "用户没有通过微信获取资料";
        }

        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
