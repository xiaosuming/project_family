<?php 
    /* 发送找回密码邮件
     * author:jiangpengfei
     * data:2017-04-17
     */
    use Util\Util;
    use Util\Check;
    use DB\CDBAuth;
    use DB\CDBAccount;
    use DB\MailDB;
    $email = Check::checkEmail(isset($params["email"]) ? $params["email"] : '');
    
    try{
        if($email != ""){
            $accountDB = new CDBAccount();
            $userId = $accountDB->getUserIdByEmail($email);
            if($userId === null){
                //用户不存在
                Util::printResult($GLOBALS['ERROR_ACCOUNT_NOT_EXIST'], "邮箱不存在");
                exit;
            }
            $username = $accountDB->getUserInfoById($userId)['username'];
            $auth = Util::generateRandomCode(100);

            $authDB = new CDBAuth();
            if($authDB->addEmailAuthForRetrieve($auth,$userId,$email)){
                $mailDb = new MailDB();
                $mailDb->pushMail($email, Util::generateRetrieveEmail($auth, $username));
                Util::printResult($GLOBALS['ERROR_SUCCESS'],"邮件已发送");
                exit();
            }


            Util::printResult($GLOBALS['ERROR_SQL_INSERT'],"数据库出错");
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }