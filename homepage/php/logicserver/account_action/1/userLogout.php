<?php
    use DB\CDBAccount;
    use Util\Util;

    $userId = $GLOBALS['userId'];
    $token = $GLOBALS['token'];

    try{
        $accountDB = new CDBAccount();
        $logoutResult = $accountDB->userLogout($userId,$token);

        if($logoutResult > 0){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], "退出登录成功");
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "退出登录失败");
        }

        
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
    