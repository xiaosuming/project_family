<?php
use Util\Util;
use DB\CDBAccount;
use DB\CDBPerson;
use Util\Check;
use Util\TempStorage;
use Model\User;
use DB\InstantMessageDB;

$auth = Check::check(trim(isset($params['auth']) ? $params['auth'] : ''));
$shareCode = Check::check(trim($params['sharecode'] ?? ''));     //登录参数中也可以携带code值
$miniType = Check::checkInteger($params['miniType'] ?? '1');     // 1是递名帖，2是AI族群

$fromUserId = Check::checkInteger($params['fromUserId'] ?? 0); //邀请人的用户id
$type = 1; //1分享链接 2 二维码

if ($auth == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit();
}

if (!isset($loginDevice) || $loginDevice == "") {
        //如果没有传递loginDevice参数，则默认是网页登录
    $loginDevice = $GLOBALS['LOGIN_DEVICE_WECHAT'];
}

$id = -1;
$token = '';

try {
    $accountDB = new CDBAccount();

        //从临时存储中获取用户资料
    $storage = new TempStorage();
    $userInfo = $storage->get($auth);

    if ($userInfo) {
        $userInfo = json_decode($userInfo, true);
    } else {
        Util::printResult($GLOBALS['ERROR_LOGIN'], "账号不存在");
        exit();
    }

    if ($userInfo['unionid'] == '') {
            // 没有unionid就通过openid登录
        $id = $accountDB->userLoginWithMini($userInfo['openid'],$miniType);
    } else {
        $id = $accountDB->userLoginWithUnionId($userInfo['unionid']);

            // unionId无法登录，则使用openid再尝试一次
        if (!$id) {
            $id = $accountDB->userLoginWithMini($userInfo['openid'], $miniType);
        }
    }

    if ($id <= 0) {

        $username = '';
        $usernameExist = true;
        while ($usernameExist) {
            $username = Util::generateRandomCode(8);
            if ($accountDB->checkUserLoginName($username)) {
                    //已经存在则继续重复生成
                $usernameExist = true;
            } else {
                $usernameExist = false;
            }
        }
        $openid = $userInfo['openid'];
        $unionId = $userInfo['unionid'];


            //email设置为空
        $email = "";

        if (!isset($phone) || $phone == "") {
            $phone = "";
        }

        if (!isset($password) || $password == "") {
            $password = Util::generateRandomCode(8);
        }

        if (!isset($password) || $username == "") {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "用户名缺少");
            exit();
        }
        if (!isset($openid) || $openid == "") {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "openid缺少");
            exit();
        }

        if (!isset($gender) || $gender == "") {
            $gender = 2;        //性别缺少默认为未知 2
        }

        //构造用户对象
        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->phone = $phone;
        $user->password = hash("sha256", $password . $GLOBALS['SHA256_SALT'], false);
        if ($miniType == 1) {
            $user->miniOpenid = $openid;
            $user->aiMiniOpenId = '';
        } else if ($miniType == 2) {
            $user->miniOpenid = '';
            $user->aiMiniOpenId = $openid;
        }

        $user->gender = $gender;
        $user->unionid = $unionId;
        $user->country = 0;
        $user->province = 0;
        $user->city = 0;


        //注册即时聊天的用户
        $imUsername = md5("izuqun_" . $user->username . "_" . Util::generateRandomCode(10));
        $imPassword = Util::generateRandomCode(20);
                //将用户名和密码推到任务队列中
        $imdb = new InstantMessageDB();
        if (!$imdb->pushUserTask($imUsername, $imPassword, $user->photo, $username)) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
            exit();
        }

        $user->hxUsername = $imUsername;
        $user->hxPassword = $imPassword;


        $registerId = $accountDB->userRegister($user);

        if ($registerId > 0) {

            if ($shareCode != null && $shareCode != "") {
                $personDB = new CDBPerson();

                if ($personDB->bindUserToPerson($id, $shareCode,$fromUserId,$type) <= 0) {
                        //绑定人物失败
                    Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "绑定人物失败");
                    exit();
                }
            }
                //生成token
            $token = hash("sha256", $registerId . Util::generateRandomCode(24) . $auth . $GLOBALS['SHA256_SALT'], false);


            if ($accountDB->insertLoginHistory("小程序登录", $loginDevice, $registerId, $token, Util::getRequestIp(), Util::getCurrentTime(), 1) > 0) {

                //设置token 添加到redis 用于登陆身份识别
                $TempStorage = new TempStorage();
                $TempStorage->setTemp($token, $registerId,7200); //两小时
                //设置token 添加到redis 用于登陆身份识别
                
                $info = $accountDB->getUserInfoById($registerId);
                $info['id_token'] = $registerId . "|" . $token . "|" . $loginDevice;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);
            } else {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据库出错，登录失败");
            }

        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
        }
    } else if ($id > 0) {
            //如果是通过分享链接登录，则自动绑定身份
        if ($shareCode != null && $shareCode != "") {
            $personDB = new CDBPerson();

            if ($personDB->bindUserToPerson($id, $shareCode) <= 0) {
                    //绑定人物失败
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "绑定人物失败");
                exit();
            }
        }
            //生成token
        $token = hash("sha256", $id . Util::generateRandomCode(24) . $auth . $GLOBALS['SHA256_SALT'], false);


        if ($accountDB->insertLoginHistory("小程序登录", $loginDevice, $id, $token, Util::getRequestIp(), Util::getCurrentTime(), 1) > 0) {
            $info = $accountDB->getUserInfoById($id);
            $info['id_token'] = $id . "|" . $token . "|" . $loginDevice;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据库出错，登录失败");
        }
    } else {
        Util::printResult($GLOBALS['ERROR_LOGIN'], "账号不存在");
    }
} catch (PDOException $e) {
        //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
