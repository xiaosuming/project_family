<?php 
    /* 通过邮件找回密码
     * author:jiangpengfei
     * data:2017-04-17
     */
    use Util\Util;
    use Util\Check;
    use DB\CDBAuth;
    use DB\CDBAccount;

    $auth = Check::check(isset($params["auth"]) ? $params["auth"] : '');
    $newPassWord = Check::check(trim(isset($params['newPassword']) ? $params['newPassword'] : ''),6);
    try{
        if($auth != "" && $newPassWord != ""){
            $authDB = new CDBAuth();

            $result = $authDB->getEmailAuthForRetrieve($auth);
            
            if ($result){ 

                ##添加密码复杂度验证
                $newPassWord = Check::passwordStrength($newPassWord);
                ##添加密码复杂度验证

                $accountDB = new CDBAccount();
                $result = json_decode($result,true);
                if($accountDB->updateUserPassword($result['u'], hash("sha256",$newPassWord.$GLOBALS['SHA256_SALT'],false)) >= 0){
                    $authDB->deleteEmailAuth($auth);
                    Util::printResult($GLOBALS['ERROR_SUCCESS'], "更改密码成功");
                }else{
                    Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "更改密码失败");
                }
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "更改密码失败，此验证会话不存在");
            }
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }