<?php
/*实现修改用户昵称
* @author php
*date:2017/2/19
*/

use DB\CDBAccount;
use Util\Check;
use Util\Util;
use ThirdParty\InstantMessage;
use ThirdParty\MiniProgram;

$id = $GLOBALS['userId'];
$nickname = Check::check(trim(isset($params['nickname']) ? $params['nickname'] : ''),1,45);

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$nickname]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    if ( $nickname == '') {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "昵称不能为空");
        exit();
    }

    $accountDB = new CDBAccount();
    // 获取用户信息
    $userIMInfo = $accountDB->getHuanxinInfoById($id);
    if ($userIMInfo == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '获取用户信息失败');
        exit;
    }

    // 更新nickname
    $imClient = new InstantMessage();
    if (!$imClient->updateNickName($userIMInfo['hxUsername'], $nickname)) {
//        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新出现异常，请稍候再试');
//        exit;
    }

    $count = $accountDB->updateNickname($nickname, $id);
    $data['count'] = $count;
    if ($count >= 0) {

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "更新出错");
        exit;
    }

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
