<?php
    use DB\CDBAccount;
    use Util\Util;
    
    try{
        $account_db = new CDBAccount();
        $userInfo = $account_db->getHuanxinInfoById($GLOBALS['userId']);
        if($userInfo!=null){
            $data['hx'] = $userInfo;
            Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'],"查询错误");
        }
    }catch(PDOException $e){
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'],"出现异常");
    }