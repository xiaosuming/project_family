<?php
/*实现修改用户信息
* @author kellen
*date:2017/2/19
*/

use DB\CDBAccount;
use DB\CDBArea;
use Util\Check;
use Util\Util;

$id = $GLOBALS['userId'];

$birthday = trim(isset($params['birthday']) ? $params['birthday'] : null);
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''),0,100);
$gender = Check::checkInteger(trim(isset($params['gender']) ? $params['gender'] : ''));
$country = Check::checkInteger(trim(isset($params['country']) ? $params['country'] : '0')); //国家 默认为 0 中国
$province = Check::checkInteger(trim(isset($params['province']) ? $params['province'] : '0')); //省 没有默认为 0
$city = Check::checkInteger(trim(isset($params['city']) ? $params['city'] : '0')); //市 没有默认为 0
$area = Check::checkInteger(trim(isset($params['area']) ? $params['area'] : '0')); //区 没有默认为 0
$town = Check::checkInteger(trim(isset($params['town']) ? $params['town'] : '0')); //乡 没有默认为 0

try {

    if (!empty($birthday)) {
        $birthday = Check::checkDate($birthday);
    }

    if (!empty($address)) {
        $address = Check::check($address);
    }

    if ($gender !== "") {
        $gender = Check::checkInteger($gender);
    }
    $CDBArea = new CDBArea();
    $result = $CDBArea->getAreaNameById($country);
    $countryName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($province);
    $provinceName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($city);
    $cityName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($area);
    $areaName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($town);
    $townName = $result['areaName'] ?? '0';

    $accountDB = new CDBAccount();
    $count = $accountDB->updateUserInfo($gender, $birthday, $country,$countryName, $province,$provinceName, $city,$cityName, $area,$areaName, $town,$townName, $address, $id);
    $data['count'] = $count;
    if ($count >= 0) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "更新出错");
        exit;
    }

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}