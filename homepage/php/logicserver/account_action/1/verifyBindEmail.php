<?php 

/**
*绑定邮箱验证
*@Coded by zch
*@日期 2017年8月18日
*/

    use Util\Util;
    use DB\CDBAccount;
    use DB\CDBAuth;
    use Util\Check;
	
    

	$auth =  Check::check(isset($params["auth"]) ? $params["auth"] : '');
	
	try{
		if($auth != ''){
            $authDB = new CDBAuth();
            
			$userInfoStr = $authDB->getEmailAuth($auth);
			if($userInfoStr){
                //拿到后删除
                $authDB->deleteEmailAuth($auth);
                $userInfo = json_decode($userInfoStr,true);
               
                //使用函数	bindUserEmail		@uesrId	@email
                $accountDB = new CDBAccount();
                if($accountDB->bindUserEmail($userInfo['u'], $userInfo['e'])){
                	$data['email'] = $userInfo['e'];
                    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                    exit;
                }else{
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "绑定失败");
                    exit;
                }
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "信息不存在");
                exit;
            }
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
        
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }