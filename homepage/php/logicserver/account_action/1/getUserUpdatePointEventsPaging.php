<?php 
    /**
     * 分页获取用户更新积分的事件
     * @author jiangpengfei
     * @date 2017-11-30
     */
    use Util\Util;
    use Util\Pager;
    use Util\Check;
    use DB\CDBPoint;

    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : ''));
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : ''));
    
    if($pageSize == ""){
        $pageSize = 10;
    }
    
    if($pageIndex == ""){
        $pageIndex  = 1;
    }
    try{
        $pointDB = new CDBPoint();
        $userId = $GLOBALS['userId'];
        
        $total = $pointDB->getUserPointUpdateEventsCount($userId);
        $paging = $pointDB->getUserPointUpdateEventsPaging($pageIndex, $pageSize,$userId);
        
        $pager = new Pager($total,$paging,$pageIndex,$pageSize);
        $pager->printPage();

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }