<?php


use Util\Util;
use Util\Check;
$string = isset($params["string"]) ? $params["string"] : '';

$data['string'] = $string;
$data['complicate'] = Check::passwordStrength($string, false);

Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);