<?php 
    /**
     * 用户登录名唯一性检查接口
     * @author jiangpengfei
     * @date 2016-12-10
     */

    use Util\Util;
    use Util\Check;
    use DB\CDBAccount;
    $loginName = Check::check(trim(isset($params['loginName']) ? $params['loginName'] : ''));
    try{
        if($loginName!=""){
            $accountDB = new CDBAccount();
            $data['exist'] = $accountDB->checkUserLoginName($loginName);
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }		

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }