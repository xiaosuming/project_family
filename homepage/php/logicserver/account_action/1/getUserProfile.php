<?php
/**
 * 获取用户资料，比如说推文数，收藏数，家族数
 * author:jiangpengfei
 * date: 2017-07-04
 */
    use DB\CDBAccount;
    use Util\Util;
    use Util\Check;

    $tmpUserId = $GLOBALS['userId'];
    $userId = Check::checkInteger(isset($params['userId']) ? $params['userId'] : '','',false);
    
    if($userId == false){
        $userId = $tmpUserId;
    }else{
    }
    
    try{
        $accountDB = new CDBAccount();
        $userProfile = $accountDB->getUserProfile($userId);
        if($userProfile!=null){
            $data['profile'] = $userProfile;
            Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'],"查询错误");
        }
    }catch(PDOException $e){
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'],"出现异常");
    }