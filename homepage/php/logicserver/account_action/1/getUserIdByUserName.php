<?php
    use DB\CDBAccount;
    use Util\Util;
    use Util\Check;

    $userName = Check::check(trim(isset($params['userName']) ? $params['userName'] : ''));

    try{
        $account_db = new CDBAccount();
        $userId = $account_db->getUserIdByUserName($userName);

        if($userId == null){
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'],"用户不存在");
            exit();
        }

        $data['userId'] = $userId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
    }catch(PDOException $e){
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'],"出现异常");
    }