<?php
/**
 * 开通私有数据库业务
 * @author: jiangpengfei
 * @date:   2019-04-30
 */

use DB\CDBAccount;
use Util\Util;

$userId = $GLOBALS['userId'];

try {
    $accountDB = new CDBAccount();
    $result = $accountDB->enablePrivateDB($userId);

    if ($result == 0) {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '开通私有数据库业务失败');
    } else {
        $data['result'] = $result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }

} catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}