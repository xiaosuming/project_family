<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-10-10
 * Time: 下午4:17
 */

use DB\CDBAuth;
use DB\InstantMessageDB;
use Model\User;
use Util\PhotoGenerator;
use Util\Util;
use DB\CDBAccount;
use DB\CDBPerson;
use Util\Check;
use Util\TempStorage;

$phone = Check::checkPhoneNumber(trim(isset($params['phone']) ? $params['phone'] : ''));
$vcode = trim(isset($params['vcode']) ? $params['vcode'] : '');     //验证码
$loginDevice = Check::check(trim(isset($params['loginDevice']) ? $params['loginDevice'] : ''));
$wechatAuth = Check::check(trim(isset($params['wechatAuth']) ? $params['wechatAuth'] : ''));      //微信凭证
$shareCode = Check::check(trim($params['sharecode'] ?? ''));     //登录参数中也可以携带code值
$fromUserId = Check::checkInteger($params['fromUserId'] ?? 0); //邀请人的用户id
$type = 1; //1分享链接 2 二维码

if ($phone === "" || $vcode == "" || $wechatAuth === "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit();
}

if ($loginDevice === "") {
    //如果没有传递loginDevice参数，则默认是网页登录
    $loginDevice = 1;
}

$id = -1;
$token = '';

try {
    $accountDB = new CDBAccount();

    //手机号注册,验证码
    $authDao = new CDBAuth();
    //$rightVcode = $authDao->getPhoneAuth($phone);
    $jsonStr = $authDao->getPhoneAuth($phone);
    $arr = json_decode($jsonStr, true);
    $rightVcode = $arr['v'];
    $recordId = $arr['r'];

    if (!$rightVcode || $rightVcode !== $vcode) {
        //验证码不存在，或者验证码错误
        Util::printResult($GLOBALS['ERROR_VCODE'], "登录失败，验证码错误");
        exit;
    } else {
        //成功则删除
        $authDao->deletePhoneAuth($phone);  //从redis删除这个验证码
        $accountDB->updatePhoneVcodeRecord($recordId);
    }

    $id = $accountDB->getUserIdByPhone($phone);

    if ($id == null) {
        // 手机号没有注册，直接注册
        //构造用户对象
        $usernameExist = true;
        while ($usernameExist) {
            $username = 'user_' . Util::generateRandomCode(10);
            if ($accountDB->checkUserLoginName($username)) {
                //已经存在则继续重复生成
                $usernameExist = true;
            } else {
                $usernameExist = false;
            }
        }
        $user = new User();
        $user->username = $username;
        $user->email = '';
        $user->phone = $phone;
        $user->password = hash("sha256", Util::generateRandomCode(10) . $GLOBALS['SHA256_SALT'], false);
        $user->photo = PhotoGenerator::generateUserPhotoFromResource();
        //生成环信的用户名和密码
        $imUsername = md5("izuqun_" . $user->username . "_" . Util::generateRandomCode(10));
        $imPassword = Util::generateRandomCode(20);
        //将用户名和密码推到任务队列中
        $imdb = new InstantMessageDB();
        if (!$imdb->pushUserTask($imUsername, $imPassword, $user->photo, $username)) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
            exit();
        }

        $user->hxUsername = $imUsername;
        $user->hxPassword = $imPassword;

        $registerId = -1;
        //是否通过分享注册的判断
        if ($shareCode !== null && $shareCode !== "") {
            $registerId = $accountDB->userRegisterWithShareCode($user, $shareCode, $fromUserId, $type);
            $personDB = new CDBPerson();
            if ($personDB->bindUserToPerson($registerId, $shareCode,$fromUserId,$type) <= 0) {
                //绑定人物失败
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "绑定人物失败");
                exit();
            }
        } else {
            $registerId = $accountDB->userRegister($user);
        }

        $id = $registerId;

        //获取微信的相关信息
        $storage = new TempStorage();
        $wechatInfo = $storage->get($wechatAuth);
        if ($wechatInfo) {
            $wechatInfo = json_decode($wechatInfo, true);
            $openid = $wechatInfo['openid'];
            $unionId = $wechatInfo['unionid'];
            $accountDB->bindWechat($id, $openid, $unionId);
        }

        //绑定微信公众号

        //生成token
        $token = hash("sha256", $id . Util::generateRandomCode(24) . $GLOBALS['SHA256_SALT'], false);

        if ($accountDB->insertLoginHistory($phone, $loginDevice, $id, $token, Util::getRequestIp(), Util::getCurrentTime(), 1) > 0) {
            $info = $accountDB->getUserInfoById($id);
            $info['id_token'] = $id . "|" . $token . "|" . $loginDevice;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据库出错，登录失败");
        }
    } else if ($id > 0) {
        //如果是通过分享链接登录，则自动绑定身份
        if ($shareCode != null && $shareCode != "") {
            $personDB = new CDBPerson();
            if ($personDB->bindUserToPerson($id, $shareCode) <= 0) {
                //绑定人物失败
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "绑定人物失败");
                exit();
            }
        }

        //获取微信的相关信息
        $storage = new TempStorage();
        $wechatInfo = $storage->get($wechatAuth);
        if ($wechatInfo) {
            $wechatInfo = json_decode($wechatInfo, true);
            $openid = $wechatInfo['openid'];
            $unionId = $wechatInfo['unionid'];
            $accountDB->bindWechat($id, $openid, $unionId);
        }

        //绑定微信公众号

        //生成token
        $token = hash("sha256", $id . Util::generateRandomCode(24) . $GLOBALS['SHA256_SALT'], false);

        if ($accountDB->insertLoginHistory($phone, $loginDevice, $id, $token, Util::getRequestIp(), Util::getCurrentTime(), 1) > 0) {
            $info = $accountDB->getUserInfoById($id);
            $info['id_token'] = $id . "|" . $token . "|" . $loginDevice;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据库出错，登录失败");
        }
    } else if ($id == -2) {
        Util::printResult($GLOBALS['ERROR_LOGIN'], "账号不存在");
    } else if ($id == -1) {
        //登录失败
        $id = $accountDB->getUserIdByLoginName($phone);
        $accountDB->insertLoginHistory($phone, $loginDevice, $id, $token, Util::getRequestIp(), Util::getCurrentTime(), 0);
        Util::printResult($GLOBALS['ERROR_LOGIN'], "登录失败,密码错误");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
