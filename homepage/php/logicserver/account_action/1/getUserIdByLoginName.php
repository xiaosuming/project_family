<?php
    use DB\CDBAccount;
    use Util\Util;
    use Util\Check;

    $loginName = Check::check(trim(isset($params['loginName']) ? $params['loginName'] : ''));

    try{
        $account_db = new CDBAccount();
        $userId = $account_db->getUserIdByLoginName($loginName);

        $data['userId'] = $userId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
    }catch(PDOException $e){
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'],"出现异常");
    }