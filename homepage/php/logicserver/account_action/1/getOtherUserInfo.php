<?php
    use DB\CDBAccount;
    use Util\Util;
    use Util\Check;

    $userId = Check::checkInteger(isset($params['userId']) ? $params['userId'] : '');

    try{
        $accountDB = new CDBAccount();
        $userInfo = $accountDB->getUserInfoById($userId);
        if($userInfo!=null){
            $data['userInfo'] = $userInfo;
            Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'],"查询错误");
        }
    }catch(PDOException $e){
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'],"出现异常");
    }