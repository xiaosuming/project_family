<?php
/**
 * 取消关注用户
 * @Author: jiangpengfei
 * @Date:   2018-12-06
 */

use DB\UserFollowDB;
use DB\CDBAccount;
use Util\Util;
use Util\Check;

$followUserId = Check::checkInteger($params['followUserId']);   // 要关注的用户id
$userId = $GLOBALS['userId'];

try {

    // 检查要关注的用户是否存在
    $accountDB = new CDBAccount();
    if (!$accountDB->isUserIdExist($followUserId)) {
        // 用户不存在
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户不存在");
    }

    $userFollowDB = new UserFollowDB();
    
    if ($userFollowDB->removeUserFollow($followUserId, [$userId])) {

        $data['remove'] = true;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '关注失败');
    }
} catch(PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}