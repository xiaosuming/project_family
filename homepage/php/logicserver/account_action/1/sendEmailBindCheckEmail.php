<?php
/**
*发送绑定验证邮件
*@Coded by zch
*@日期 2017年8月18日
*/

    use Util\Util;
    use Util\Check;
    use DB\CDBAccount;
    use DB\CDBAuth;
    use DB\MailDB;
	
	$userId = $GLOBALS['userId'];
	$email = Check::checkEmail(trim(isset($params['email']) ? $params['email'] : ''));
	
	try{
        $accountDB = new CDBAccount();
		//该邮箱是否存在的判断
		if($email == ""){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit();
        }
            
        if($accountDB->checkUserLoginName($email)){
            Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEEMAIL'], "邮箱重复");
            exit();
		}
		
		$auth = Util::generateRandomCode(60);
        $username = $accountDB->getUserInfoById($userId)['username'];
        $authDB = new CDBAuth();
        if($authDB->addEmailAuthForBinding($auth,$userId,$email)){
			$mailDB = new MailDB();
            $mailDB->pushMail($email,Util::generateEmailBindingEmail($auth, $username));
        }else{
            Util::printResult($GLOBALS['ERROR_INSERT_REDIS'], "插入失败");
        }
		
        Util::printResult($GLOBALS['ERROR_SUCCESS'], "成功");
		
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
