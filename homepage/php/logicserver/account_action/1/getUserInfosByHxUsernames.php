<?php
    use DB\CDBAccount;
    use Util\Util;
    use Util\Check;

    $hxUsernames = isset($params['hxUsernames']) ? $params['hxUsernames'] : '';

    $array = explode(",",$hxUsernames);

    try{

        $accountDB = new CDBAccount();
        $userInfos = $accountDB->getUserInfosByHxUsernames($array);
        $data['userInfos'] = $userInfos;
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);

    }catch(PDOException $e){
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'],"出现异常");
    }