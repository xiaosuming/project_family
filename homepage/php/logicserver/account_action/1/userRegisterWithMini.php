<?php 
use Util\Util;
use Util\Check;
use DB\CDBAccount;
use DB\CDBAuth;
use DB\InstantMessageDB;
use Util\TempStorage;
use Util\Http;
use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;
use Model\User;
use ThirdParty\InstantMessage;
use DB\CDBInvitationCode;
use DB\CDBUserInvitationCode;
use Util\PhotoGenerator;

//小程序特有的参数
$auth = Check::check(trim(isset($params['auth']) ? $params['auth'] : ''));
$miniType = Check::checkInteger($params['miniType'] ?? 1);

//根据凭证获取用户信息
$storage = new TempStorage();
$userInfo = $storage->get($auth);
$openid = null;
$unionId = null;

if ($userInfo) {
    $userInfo = json_decode($userInfo, true);
    $username = '';
    $usernameExist = true;

    $accountDB = new CDBAccount();
    while ($usernameExist) {
        $username = Util::generateRandomCode(8);
        if ($accountDB->checkUserLoginName($username)){
            //已经存在则继续重复生成
            $usernameExist = true;
        }else{
            $usernameExist = false;
        }
    }
    $openid = $userInfo['openid'];
    $unionId = $userInfo['unionid'];

} else {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit();
}

//email设置为空
$email = "";

try {
    $accountDB = new CDBAccount();

    if (!isset($phone) || $phone == "") {
        $phone = "";
    }

    if (!isset($password) || $password == "") {
        $password = Util::generateRandomCode(8);
    }

    if (!isset($password) || $username == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "用户名缺少");
        exit();
    }
    if (!isset($openid) || $openid == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "openid缺少");
        exit();
    }

    if (!isset($gender) || $gender == "") {
        $gender = 2;        //性别缺少默认为未知 2
    }

    if (!empty($openid) && !empty($unionId)) {
        // 如果有UnionId，则不用openId
        $openid = "";
    }

    //构造用户对象
    $user = new User();
    $user->username = $username;
    $user->email = $email;
    $user->phone = $phone;
    $user->password = hash("sha256", $password.$GLOBALS['SHA256_SALT'], false);
    if ($miniType == 1) {
        $user->miniOpenid = $openid;
        $user->aiMiniOpenId = '';
    } else if ($miniType == 2) {
        $user->miniOpenid = '';
        $user->aiMiniOpenId = $openid;
    }
    $user->gender = $gender;
    $user->unionid = $unionId;
    $user->country = 0;
    $user->province = 0;
    $user->city = 0;


    /*已弃用
    //注册即时聊天的用户
    $imUsername = md5("izuqun_".$user->username . "_" . Util::generateRandomCode(10));
    $imPassword = Util::generateRandomCode(20);
    //将用户名和密码推到任务队列中
    $imdb = new InstantMessageDB();
    if (!$imdb->pushUserTask($imUsername, $imPassword, $user->photo, $username)) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
        exit();
    }*/
    //获取腾讯云通讯账号
    $IM = $accountDB->getTencentIMName();

    $user->hxUsername = $IM['name'];
    $user->hxPassword = '';


    $registerId=$accountDB->userRegister($user);

    if ($registerId > 0) {
        
        //绑定腾讯云通讯账号
        $accountDB->updateTencentIMUserId($IM['id'], $registerId);

        $data['success'] = true;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
