<?php 
    /**
     * 检查子系统的令牌是否正确
     * @author jiangpengfei
     * @date 2018-04-09
     */

    use Util\Util;
    use Util\Check;
    use DB\CDBAccount;
    $systemUrl = Check::check(trim(isset($params['systemUrl']) ? $params['systemUrl'] : ''));
    $subToken = Check::check(trim($params['subToken'] ?? ''));

    try{
        if ($subToken == '' || $systemUrl == '') {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
        
        $accountDB = new CDBAccount();
        $result =  $accountDB->checkSubSystemToken($systemUrl,$subToken);

        if ($result == null) {
            //令牌无效
            Util::printResult($GLOBALS['ERROR_SUBTOKEN'], "令牌无效");
        } else {
            //令牌有效
            $data['user'] = $result;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }