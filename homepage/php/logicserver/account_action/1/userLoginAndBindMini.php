<?php
    use Util\Util;
    use DB\CDBAccount;
    use DB\CDBPerson;
    use Util\Check;
    use Util\TempStorage;
    use Util\RateLimit;

    $loginName = Check::check(trim(isset($params['loginName']) ? $params['loginName'] : ''));
    $password = Check::check(trim(isset($params['password']) ? $params['password'] : ''));
    $loginDevice = Check::check(trim(isset($params['loginDevice']) ? $params['loginDevice'] : ''));
    $wechatAuth = Check::check(trim(isset($params['wechatAuth']) ? $params['wechatAuth'] : ''));      //微信凭证
    $shareCode = Check::check(trim($params['sharecode'] ?? ''));     //登录参数中也可以携带code值
    $miniType = Check::checkInteger($params['miniType'] ?? 1);  // 小程序类型，1是递名帖，2是AI族群

    $fromUserId = Check::checkInteger($params['fromUserId'] ?? 0); //邀请人的用户id
    $type = 1; //1分享链接 2 二维码

    if($loginName === "" || $password === "" || $wechatAuth === ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit();
    }

    if($loginDevice === ""){
        //如果没有传递loginDevice参数，则默认是网页登录
        $loginDevice = 1;
    }

    $id = -1;
    $token = '';

    try{
        $accountDB = new CDBAccount();

        $id = $accountDB->userLogin($loginName, hash("sha256",$password.$GLOBALS['SHA256_SALT'],false));

        if($id>0){
            //如果是通过分享链接登录，则自动绑定身份
            if($shareCode != null && $shareCode != ""){
                $personDB = new CDBPerson();
                if($personDB->bindUserToPerson($id,$shareCode,$fromUserId,$type) <= 0){
                    //绑定人物失败
                    Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "绑定人物失败");
                    exit();
                }
            }

            //获取微信的相关信息
            $storage = new TempStorage();
            $wechatInfo = $storage->get($wechatAuth);
            if($wechatInfo){
                $wechatInfo = json_decode($wechatInfo,true);
                $openid = $wechatInfo['openid'];
                $unionId = $wechatInfo['unionid'];
                $accountDB->bindMini($id,$openid,$unionId, $miniType);
            }



            //生成token
            $token = hash("sha256",$id.Util::generateRandomCode(24).$password.$GLOBALS['SHA256_SALT'],false);


            if($accountDB->insertLoginHistory($loginName,$loginDevice, $id, $token,Util::getRequestIp(), Util::getCurrentTime(), 1) > 0){
                $requestIP = $_SERVER["REMOTE_ADDR"];
                $limiter = $rateLimitList[$params['sub_action']];
                $rateLimit = new RateLimit($GLOBALS['RATE_LIMIT_SHA']);
                $rateLimit->resetBucket('rl_' . $params['sub_action'] . $requestIP, $limiter);


                //设置token 添加到redis 用于登陆身份识别
                $TempStorage = new TempStorage();
                $TempStorage->setTemp($token, $id,7200); //两小时
                //设置token 添加到redis 用于登陆身份识别

                $info = $accountDB->getUserInfoById($id);
                $info['id_token'] = $id."|".$token."|".$loginDevice;
//                $info['passwordStrength'] = Check::passwordStrength($password, false);

                Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据库出错，登录失败");
            }
        }else if($id == -2){
            Util::printResult($GLOBALS['ERROR_LOGIN'], "账号不存在");
        }else if($id == -1){
                    //登录失败
            $id = $accountDB->getUserIdByLoginName($loginName);
            $accountDB->insertLoginHistory($loginName, $loginDevice, $id, $token,Util::getRequestIp(), Util::getCurrentTime(), 0);
            Util::printResult($GLOBALS['ERROR_LOGIN'], "登录失败,密码错误");
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }

