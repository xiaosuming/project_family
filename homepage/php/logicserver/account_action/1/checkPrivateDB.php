<?php
/**
 * 检查私有数据库是否在线
 * @author: jiangpengfei
 * @date:   2019-04-30
 */

use GuzzleHttp\Client;
use Util\Util;

$userId = $GLOBALS['userId'];

try {
    $client = new Client([
        'base_uri' => $GLOBALS['PRIVATE_DB_PROXY'],
        'timeout' => 3.0
    ]);

    $response = $client->request('POST', '/Check', [
        'form_params' => [
            'userId' => $userId
        ]
    ]);
    $contents = $response->getBody()->getContents();
    $response = json_decode($contents, true);

    if ($response != null && $response['code'] == 0) {
        $exist = $response['data']['exist'];
        
        $data['exist'] = $exist;

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    } else {
        $logger->error('检查私有数据库是否在线出错', $response);
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], '检查私有数据库是否在线出错');
    }
} catch(\GuzzleHttp\Exception\ConnectException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '私有数据库服务代理异常');
}