<?php

    use Model\Limiter;

    /**
     * whiteList是白名单，用来允许某些请求通过验证
     */
    $whiteList['captcha'] = "allow";
    $whiteList['userLogin'] = "allow";
    $whiteList['SSOUserLogin'] = "allow";
    $whiteList['userRegister'] = "allow";
    $whiteList['userRegisterWithWechat'] = "allow";
    $whiteList['userRegisterWithMini'] = "allow";
    $whiteList['isLogin'] = 'allow';
    $whiteList['checkUserLoginName'] = 'allow';
    $whiteList['active'] = 'allow';
    $whiteList['retrieve'] = 'allow';
    $whiteList['retrieveByPhone'] = 'allow';
    $whiteList['sendRetrieveEmail'] = 'allow';
    $whiteList['verifyBindEmail'] = 'allow';
    $whiteList['userCode'] = 'allow';
    $whiteList['getPhoneAuth'] = 'allow';
    $whiteList['userLoginWithMini'] = 'allow';
    $whiteList['userLoginWithWechat'] = 'allow';
    $whiteList['userLoginAndBindWechat'] = 'allow';
    $whiteList['userLoginAndBindMini'] = 'allow';
    $whiteList['checkWechatAccountExist'] = 'allow';
    $whiteList['verifyInvitationCode'] = 'allow';
    $whiteList['checkSubSystemToken'] = 'allow';
    $whiteList['userLoginWithVCode'] = 'allow';
    $whiteList['userLoginByPhoneAndBindMini'] = 'allow';
    $whiteList['getStringComplicate'] = 'allow';


    $version = $params['version'] ?? "1";

    //0是capacity，1是cycle，2如果存在，代表额外限制项,3代表额外限制项的capacity，4代表额外限制项的cycle

    //TODO: 这里暂时注释掉
    //$rateLimitList['getPhoneAuth'] = new Limiter(6,1200,$params["phone"]??'',10,3600);
    $rateLimitList['userLogin'] = new Limiter(6,360);
    $rateLimitList['userLoginWithVCode'] = new Limiter(6,360);
    $rateLimitList['SSOUserLogin'] = new Limiter(6,360);
    $rateLimitList['userLoginByPhoneAndBindMini'] = new Limiter(6,360);

    require_once("filter/filter.php");
    $account_action_file_array['userLogin'] = "logicserver/account_action/{$version}/userLogin.php";
    $account_action_file_array['userLoginWithMini'] = "logicserver/account_action/{$version}/userLoginWithMini.php";
    $account_action_file_array['userLoginWithWechat'] = "logicserver/account_action/{$version}/userLoginWithWechat.php";
    $account_action_file_array['userLoginAndBindWechat'] = "logicserver/account_action/{$version}/userLoginAndBindWechat.php";
    $account_action_file_array['userLoginAndBindMini'] = "logicserver/account_action/{$version}/userLoginAndBindMini.php";
    $account_action_file_array['userRegister'] = "logicserver/account_action/{$version}/userRegister.php";
    $account_action_file_array['userRegisterWithWechat'] = "logicserver/account_action/{$version}/userRegisterWithWechat.php";
    $account_action_file_array['userRegisterWithMini'] = "logicserver/account_action/{$version}/userRegisterWithMini.php";
    $account_action_file_array['checkUserLoginName'] = "logicserver/account_action/{$version}/checkUserLoginName.php";
    $account_action_file_array['isLogin'] = "logicserver/account_action/{$version}/isLogin.php";
    $account_action_file_array['userLogout'] = "logicserver/account_action/{$version}/userLogout.php";
    $account_action_file_array['getUserInfo'] = "logicserver/account_action/{$version}/getUserInfo.php";
    $account_action_file_array['getHuanxinInfo'] = "logicserver/account_action/{$version}/getHuanxinInfo.php";
    $account_action_file_array['getOtherUserInfo'] = "logicserver/account_action/{$version}/getOtherUserInfo.php";
    $account_action_file_array['addPhotoForUser'] = "logicserver/account_action/{$version}/addPhotoForUser.php";
    $account_action_file_array['getLoginHistory'] = "logicserver/account_action/{$version}/getLoginHistory.php";
    $account_action_file_array['changeUserInfo'] = "logicserver/account_action/{$version}/changeUserInfo.php";
    $account_action_file_array['active'] = "logicserver/account_action/{$version}/active.php";
    $account_action_file_array['retrieve'] = "logicserver/account_action/{$version}/retrieve.php";
    $account_action_file_array['retrieveByPhone'] = "logicserver/account_action/{$version}/retrieveByPhone.php";
    $account_action_file_array['sendRetrieveEmail'] = "logicserver/account_action/{$version}/sendRetrieveEmail.php";
    $account_action_file_array['userCode'] = "logicserver/account_action/{$version}/userCode.php";
    $account_action_file_array['bindUserEmail'] = "logicserver/account_action/{$version}/bindUserEmail.php";
    $account_action_file_array['bindUserPhone'] = "logicserver/account_action/{$version}/bindUserPhone.php";
    $account_action_file_array['getUserIdByUserName'] = "logicserver/account_action/{$version}/getUserIdByUserName.php";
    $account_action_file_array['getUserIdByLoginName'] = "logicserver/account_action/{$version}/getUserIdByLoginName.php";
    $account_action_file_array['getUserProfile'] = "logicserver/account_action/{$version}/getUserProfile.php";
    $account_action_file_array['getPhoneAuth'] = "logicserver/account_action/{$version}/getPhoneAuth.php";
    $account_action_file_array['checkWechatAccountExist'] = "logicserver/account_action/{$version}/checkWechatAccountExist.php";
    $account_action_file_array['verifyBindEmail'] = "logicserver/account_action/{$version}/verifyBindEmail.php";
    $account_action_file_array['sendEmailBindCheckEmail'] = "logicserver/account_action/{$version}/sendEmailBindCheckEmail.php";
    $account_action_file_array['changeNickname'] = "logicserver/account_action/{$version}/changeNickname.php";
    $account_action_file_array['verifyInvitationCode'] = "logicserver/account_action/{$version}/verifyInvitationCode.php";
    $account_action_file_array['getUserInfosByHxUsernames'] = "logicserver/account_action/{$version}/getUserInfosByHxUsernames.php";
    $account_action_file_array['getUserPointInfo'] = "logicserver/account_action/{$version}/getUserPointInfo.php";
    $account_action_file_array['getUserUpdatePointEventsPaging'] = "logicserver/account_action/{$version}/getUserUpdatePointEventsPaging.php";
    $account_action_file_array['addReport'] = "logicserver/account_action/{$version}/addReport.php";
    $account_action_file_array['SSOUserLogin'] = "logicserver/account_action/{$version}/SSOUserLogin.php";
    $account_action_file_array['checkSubSystemToken'] = "logicserver/account_action/{$version}/checkSubSystemToken.php";
    $account_action_file_array['captcha'] = "logicserver/account_action/{$version}/captcha.php";
    $account_action_file_array['bindMiniAccount'] = "logicserver/account_action/{$version}/bindMiniAccount.php";
    $account_action_file_array['userBindPhoneAndPass'] = "logicserver/account_action/{$version}/userBindPhoneAndPass.php";
    $account_action_file_array['userLoginWithVCode'] = "logicserver/account_action/{$version}/userLoginWithVCode.php";
    $account_action_file_array['bindMiniAccountWithVcode'] = "logicserver/account_action/{$version}/bindMiniAccountWithVcode.php";
    $account_action_file_array['userLoginByPhoneAndBindWechat'] = "logicserver/account_action/{$version}/userLoginByPhoneAndBindWechat.php";
    $account_action_file_array['userLoginByPhoneAndBindMini'] = "logicserver/account_action/{$version}/userLoginByPhoneAndBindMini.php";
    $account_action_file_array['follow'] = "logicserver/account_action/{$version}/follow.php";
    $account_action_file_array['getUserFollowerPaging'] = "logicserver/account_action/{$version}/getUserFollowerPaging.php";
    $account_action_file_array['cancelUserFollow'] = "logicserver/account_action/{$version}/cancelUserFollow.php";
    $account_action_file_array['editBindUserPhone'] = "logicserver/account_action/{$version}/editBindUserPhone.php";
    $account_action_file_array['verifyUntiedPhone'] = "logicserver/account_action/{$version}/verifyUntiedPhone.php";
    $account_action_file_array['checkPrivateDB'] = "logicserver/account_action/{$version}/checkPrivateDB.php";
    $account_action_file_array['enablePrivateDB'] = "logicserver/account_action/{$version}/enablePrivateDB.php";


    $account_action_file_array['getStringComplicate'] = "logicserver/account_action/{$version}/getStringComplicate.php";
    $account_action_file_array['getFamilyUserList'] = "logicserver/account_action/{$version}/getFamilyUserList.php";
    $account_action_file_array['resetPassword'] = "logicserver/account_action/{$version}/resetPassword.php";
    $account_action_file_array['getUserList'] = "logicserver/account_action/{$version}/getUserList.php";
    $account_action_file_array['changePdfPermission'] = "logicserver/account_action/{$version}/changePDFPermission.php";
