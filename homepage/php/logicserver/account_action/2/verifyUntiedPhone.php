<?php
/**
 * 验证需要解绑的手机号
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-4
 * Time: 下午5:55
 */

use DB\CDBAccount;
use DB\CDBAuth;
use Util\Check;
use Util\Util;

$userId = $GLOBALS["userId"];
$phone = Check::checkPhoneNumber(trim($params['phone'] ?? ''));
$vcode = trim($params['vcode'] ?? '');

try {

    if ($phone === "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit();
    }

    $accountDB = new CDBAccount();
    $user = $accountDB->getUserInfo($userId);
    $userPhone = $user['phone'];

    if ($phone != $userPhone) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '解绑手机号错误');
        exit;
    }

    if (!$accountDB->checkUserLoginName($phone)) {
        Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEPHONE'], "该手机号未绑定");
        exit();
    }
    $authDao = new CDBAuth();
    $jsonStr = $authDao->getPhoneAuth($phone);
    $arr = json_decode($jsonStr, true);
    $rightVcode = $arr['v'];
    $recordId = $arr['r'];

    if (!$rightVcode || $rightVcode !== $vcode) {
        //验证码不存在，或者验证码错误
        Util::printResult($GLOBALS['ERROR_VCODE'], "验证解绑手机号失败，验证码错误");
        exit;
    } else {
        //成功则删除
        $authDao->deletePhoneAuth($phone);  //从redis删除这个验证码
        $updateRow = $accountDB->updatePhoneVcodeRecord($recordId);
        $data['updateRow'] = $updateRow;
        $data['recordId'] = $recordId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
