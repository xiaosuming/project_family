<?php

use DB\CDBAccount;
use Util\Util;
use ThirdParty\TencentIM;

try {
    $account_db = new CDBAccount();
    $userInfo = $account_db->getHuanxinInfoById($GLOBALS['userId']);
    if ($userInfo!=null) {

        $identifier = $userInfo['hxUsername'];
        $identifier_key = Util::generateSigKey($userInfo['hxUsername']);

        $imClient = new TencentIM();
        $userSig = $imClient->getUserSig($identifier_key);
        if (!$userSig) {
            $userSig = $imClient->signature($identifier);   //生成签名
            $imClient->saveUserSig($identifier_key, $userSig);
        }
        $userInfo['hxPassword'] = $userSig;
        $data['hx'] = $userInfo;

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "查询错误");
    }
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
