<?php 
use Util\Util;
use Util\RateLimit;
use DB\CDBAccount;
use Util\TempStorage;

$whiteList = [];

    //检查当前请求中是否存在白名单并且请求是否在白名单里面    
if (!isset($whiteList) || isset($whiteList) && !array_key_exists($params['sub_action'], $whiteList)) {
    $accountDB = new CDBAccount();

    if (isset($params['id_token'])) {
        $res = $params['id_token'];
        $arrRes = explode("|", $res);

        if (count($arrRes) != 3) {
            $data['login'] = false;

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        }


        $GLOBALS['userId'] = $arrRes[0];
        $GLOBALS['token'] = $arrRes[1];
        $GLOBALS['usingDevice'] = $arrRes[2];		//
        $deviceCode = isset($params['deviceCode']) ? $params['deviceCode'] : '';
        try {
            $GLOBALS['deviceCode'] = $deviceCode;
            if (!$accountDB->verifyUserIdAndTokenWithDevice($GLOBALS['userId'], $GLOBALS['token'], $GLOBALS['usingDevice'], $deviceCode)) {
                $data['login'] = false;

                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                exit;
            }
        } catch (PDOException $e) {
            $logger->error(Util::exceptionFormat($e));
            $data['login'] = false;

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        }
    } else {
        $data['login'] = false;

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }
}

$data['login'] = true;

Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);