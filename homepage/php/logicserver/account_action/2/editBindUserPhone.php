<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-4
 * Time: 下午4:54
 */

use Util\Util;
use Util\Check;
use DB\CDBAccount;
use DB\CDBAuth;

$userId = $GLOBALS["userId"];

$recordId = Check::checkInteger($params['recordId'] ?? '');
$phone = Check::checkPhoneNumber(trim($params['phone'] ?? ''));
$vcode = trim($params['vcode'] ?? '');

try {

    $accountDB = new CDBAccount();

    $phoneVcodeRecord = $accountDB->getPhoneVcodeRecord($recordId);
    if ($phoneVcodeRecord == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '记录不存在');
        exit;
    }

    if ($phoneVcodeRecord['isUse'] != 1) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '解绑验证未通过');
        exit;
    }

    $authDao = new CDBAuth();

    if ($phone === "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少新手机号参数");
        exit();
    }

    if ($accountDB->checkUserLoginName($phone)) {
        Util::printResult($GLOBALS['ERROR_REGISTER_DUPLICATEPHONE'], "手机号重复");
        exit();
    }

    $jsonStr = $authDao->getPhoneAuth($phone);
    $arr = json_decode($jsonStr, true);
    $rightVcode = $arr['v'];
    $recordId = $arr['r'];

    if (!$rightVcode || $rightVcode !== $vcode) {
        //验证码不存在，或者验证码错误
        Util::printResult($GLOBALS['ERROR_VCODE'], "绑定失败，验证码错误");
        exit;
    } else {
        //成功则删除
        $authDao->deletePhoneAuth($phone);  //从redis删除这个验证码
        $accountDB->updatePhoneVcodeRecord($recordId);
        $accountDB->updatePhone($phone, $userId);
        Util::printResult($GLOBALS['ERROR_SUCCESS'], "修改绑定手机号成功");
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
