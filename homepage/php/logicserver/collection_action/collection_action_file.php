<?php
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $collection_action_file_array['addCollection'] = "logicserver/collection_action/{$version}/addCollection.php";
    $collection_action_file_array['deleteCollection'] = "logicserver/collection_action/{$version}/deleteCollection.php";
    $collection_action_file_array['getCollectionsPaging'] = "logicserver/collection_action/{$version}/getCollectionsPaging.php";
  