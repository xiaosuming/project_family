<?php
    use Util\Util;
    use DB\CDBCollection;
    use Util\Check;

    $collectionId = Check::checkInteger(trim(isset($params['collectionId']) ? $params['collectionId'] : ''));        //模块id
    $userId = $GLOBALS['userId'];       //用户id

    try{
        if($collectionId != "" ){
            $collectionDB = new CDBCollection();
            $deleteRows = $collectionDB->deleteCollection($collectionId);
            $data = null;
            if($deleteRows > 0){
                $data['deleteRows'] = $deleteRows;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_DELETE'], $data);
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }