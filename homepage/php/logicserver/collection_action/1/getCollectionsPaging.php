<?php
    /**
     * 获取分页的收藏
     */	
    use Util\Util;
    use Util\Pager;
    use DB\CDBCollection;
    use Util\Check;

    $userId = $GLOBALS['userId'];													                //操作用户id
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1'));		//页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '10'));		//每页数量

    try{
        $collectionDB = new CDBCollection();
        
        $total = $collectionDB->getCollectionsTotal($userId);
        $collections = $collectionDB->getCollectionsPaging($pageIndex, $pageSize, $userId);

        $pager = new Pager($total,$collections,$pageIndex,$pageSize);
        $pager->printPage();		//以分页的格式输出
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }