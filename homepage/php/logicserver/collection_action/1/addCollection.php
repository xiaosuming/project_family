<?php
    use Util\Util;
    use DB\CDBCollection;
    use Util\Check;

    $moduleId = Check::checkModule(trim(isset($params['moduleId']) ? $params['moduleId'] : ''));        //模块id
    $recordId = Check::checkInteger(trim(isset($params['recordId']) ? $params['recordId'] : ''));       //记录id
    $recordTitle  = Check::check(trim(isset($params['recordTitle']) ? $params['recordTitle'] : ''),1,255);    //记录标题
    $recordContent = Check::check(trim(isset($params['recordContent']) ? $params['recordContent'] : ''),1,255);   //记录内容
    $extraInfo1 = Check::check(trim(isset($params['extraInfo1']) ? $params['extraInfo1'] : ''),0,255);        //额外信息1
    $extraInfo2 = Check::check(trim(isset($params['extraInfo2']) ? $params['extraInfo2'] : ''),0,255);        //额外信息2
    $recordTitle = Util::filterContentReplace($recordTitle);
    $recordContent = Util::filterContentReplace($recordContent);
    $extraInfo1 = Util::filterContentReplace($extraInfo1);
    $extraInfo2 = Util::filterContentReplace($extraInfo2);

    $userId = $GLOBALS['userId'];       //用户id

    try{
        if($moduleId != "" && $recordId != "" && $recordTitle != ""){
            $collectionDB = new CDBCollection();
            //先检查收藏是否已经存在
            if($collectionDB->checkCollectionExists($moduleId,$recordId,$userId)){
                Util::printResult($GLOBALS['ERROR_COLLECTION_EXIST'],"收藏已经存在");
                exit;
            }

            $collectionId = $collectionDB->addCollection($moduleId,$recordId,$recordTitle,$recordContent,$extraInfo1,$extraInfo2,$userId);
            if($collectionId > 0){
                $data['collectionId'] = $collectionId;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
            }

        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
