<?php
/**
 * 用户竞标悬赏任务的接口
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 17:05
 */

use DB\CDBTask;
use Util\Check;
use Util\Util;
use ThirdParty\MiniProgram;

$userId = $GLOBALS['userId'];

$initiatorId = Check::checkInteger(trim(isset($params['initiatorId']) ? $params['initiatorId'] : '')); //发起悬赏任务的人id
$taskId = Check::checkInteger(trim(isset($params['taskId']) ? $params['taskId'] : ''));
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''),1,500);

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$content]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    if ($taskId == '' || $content == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $CDBTask = new CDBTask();
    if (!$CDBTask->existsTaskId($taskId)) {
        Util::printResult($GLOBALS['ERROR_EXISTS_TASKID'], "悬赏任务不存在");
        exit;
    }
    $arr = $CDBTask->getUsers($initiatorId);
    $userArr = [];
    foreach ($arr as $v) {
        array_push($userArr, $v['userId']);
    }
    if (!in_array($userId, $userArr)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }

    if ($CDBTask->existsUserIdANDTaskIdInTender($userId, $taskId)) {
        Util::printResult($GLOBALS['ERROR_EXISTS_TASKID'], "已经竞标过该悬赏任务");
        exit;
    }
    $result = $CDBTask->addUserTaskTender($userId, $taskId, $content);
    if ($result) {
        $data['taskTenderId'] = $result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }
    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
    exit;
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    exit;
}
