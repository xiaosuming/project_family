<?php
/**
 * 用户更新悬赏任务接口
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 15:58
 */

use DB\CDBTask;
use Util\Check;
use Util\Util;
use ThirdParty\MiniProgram;

$taskId = Check::checkInteger(trim(isset($params['taskId']) ? $params['taskId'] : ''));
$title = Check::check(trim(isset($params['title']) ? $params['title'] : ''));
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''),1,500);
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));
$point = Check::checkInteger(trim(isset($params['point']) ? $params['point'] : ''));

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$content, $title]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    if ($taskId == '' || $content == '' || $point == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $CDBTask = new CDBTask();
    $userId = $GLOBALS['userId'];
    if (!$CDBTask->existsTaskId($taskId)) {
        Util::printResult($GLOBALS['ERROR_EXISTS_TASKID'], "悬赏任务不存在");
        exit;
    }
    if (!$CDBTask->verifyTaskIdAndUserId($taskId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }

    if ($point < $CDBTask->getTaskPoint($taskId)) {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '悬赏分只能增加,不能减少');
        exit;
    }
    $pathJsonStr = "";
    if ($photo != "") {
        //这里将photo解析成image数组
        $pathArray = explode(",", $photo);
        if (count($pathArray) > 9) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量超过限制");
            exit();
        }
        $pathJsonStr = json_encode($pathArray);
    } else {
        $pathJsonStr = json_encode(array());
    }
    $photo = $pathJsonStr;
    $updateTime = Util::getCurrentTime();
    $updateBy = $userId;
    $rows = $CDBTask->updateTask($taskId, $title, $content, $photo, $point, $updateTime, $updateBy);
    if ($rows > 0) {
        $data['updateTaskCount'] = $rows;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }
    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
    exit;
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    exit;
}
