<?php
/**
 *
 * 分页获取所有竞标内容的接口
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/13 0013
 * Time: 10:15
 */

use DB\CDBTask;
use Util\Check;
use Util\Pager;
use Util\Util;

$taskId = Check::checkInteger(trim(isset($params['taskId']) ? $params['taskId'] : ''));
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 10));

if($taskId == ''){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {
    $CDBTask = new CDBTask();
    $result = $CDBTask->getUserTenderPaging($taskId, $pageIndex, $pageSize);
    $total = $CDBTask->getUserTenderTotal($taskId);
    $page = new Pager($total, $result, $pageIndex, $pageSize);
    $page->printPage();

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    exit;
}