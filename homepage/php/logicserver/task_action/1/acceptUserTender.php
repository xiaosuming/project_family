<?php
/**
 * 发起用户接受某个用户竞标
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 18:02
 */

use DB\CDBTask;
use Util\Check;
use Util\Util;

$acceptUserId = Check::checkInteger(trim(isset($params['acceptUserId']) ? $params['acceptUserId'] : ''));
$taskId = Check::checkInteger(trim(isset($params['taskId']) ? $params['taskId'] : ''));

try {
    if ($acceptUserId == '' || $taskId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $CDBTask = new CDBTask();
    $userId = $GLOBALS['userId'];
    if (!$CDBTask->existsTaskId($taskId)) {
        Util::printResult($GLOBALS['ERROR_EXISTS_TASKID'], "悬赏任务不存在");
        exit;
    }
    if (!$CDBTask->verifyTaskIdAndUserId($taskId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
    $result = $CDBTask->updateTenderAccept($taskId, $acceptUserId, $userId);
    if ($result > 0) {
        $data['updateCount'] = $result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }
    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], 'null');
    exit;

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    exit;
}