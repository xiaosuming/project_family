<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 16:30
 */

use DB\CDBTask;
use Util\Check;
use Util\Util;

$taskId = Check::checkInteger(trim(isset($params['taskId']) ? $params['taskId'] : ''));

try {

    if ($taskId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }

    $CDBTask = new CDBTask();
    $userId = $GLOBALS['userId'];
    if (!$CDBTask->existsTaskId($taskId)) {
        Util::printResult($GLOBALS['ERROR_EXISTS_TASKID'], "悬赏任务不存在");
        exit;
    }
    if (!$CDBTask->verifyTaskIdAndUserId($taskId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
    $result = $CDBTask->deleteTask($taskId, $userId);
    if (!$result) {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], null);
        exit;
    }
    $data['deleteCount'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    exit;
}