<?php
/**
 * 获取悬赏任务详情接口
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 18:18
 */

use DB\CDBTask;
use Util\Check;
use Util\Util;

$taskId = Check::checkInteger(trim(isset($params['taskId']) ? $params['taskId'] : ''));

try {
    $CDBTask = new CDBTask();
    $result = $CDBTask->getTaskDetail($taskId);
    if ($result) {
        $result['photo'] = json_decode($result['photo'], true);
        $data['taskDetail'] = $result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }
    Util::printResult($GLOBALS['ERROR_SQL_QUERY'], null);
    exit;
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    exit;
}