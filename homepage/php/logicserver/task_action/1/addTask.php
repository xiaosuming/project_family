<?php
/**
 * 用户发起悬赏任务接口
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 10:02
 */

use DB\CDBTask;
use Util\Check;
use Util\Util;

$title = Check::check(trim(isset($params['title']) ? $params['title'] : ''),1,45);
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''),1,500);
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));
$point = Check::checkInteger(trim(isset($params['point']) ? $params['point'] : ''));



try {
    if ($title == '' || $content == "" || $point == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }

    $pathJsonStr = "";
    if ($photo != "") {
        //这里将photo解析成image数组
        $pathArray = explode(",", $photo);
        if (count($pathArray) > 9) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量超过限制");
            exit();
        }
        $pathJsonStr = json_encode($pathArray);
    } else {
        $pathJsonStr = json_encode(array());
    }
    $photo = $pathJsonStr;

    $userId = $GLOBALS['userId'];
    $CDBTask = new CDBTask();
    $taskId = $CDBTask->addTask($userId, $title, $content, $photo, $point);
    if ($taskId > 0) {
        $data['TaskId'] = $taskId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }
    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
    exit;
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    exit;
}
