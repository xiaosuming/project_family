<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-22
 * Time: 下午4:03
 */

use DB\CDBTask;
use Util\Check;
use Util\SysLogger;
use Util\Util;

$tenderId = Check::checkInteger($params['tenderId'] ?? ''); //用户竞标id
$userId = $GLOBALS['userId'];


try {
    $taskDB = new CDBTask();
    $tenderDetail = $taskDB->getTenderDetail($tenderId);
    if ($tenderDetail == null || $tenderDetail['create_by'] != $userId || $tenderDetail['is_accept'] == 1) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $delRow = $taskDB->deleteTender($tenderId);
    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
