<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 10:01
 */
$version = $params['version'] ?? "1";

require_once("filter/filter.php");
$task_action_file_array['addTask'] = "logicserver/task_action/{$version}/addTask.php";
$task_action_file_array['updateTask'] = "logicserver/task_action/{$version}/updateTask.php";
$task_action_file_array['deleteTask'] = "logicserver/task_action/{$version}/deleteTask.php";
$task_action_file_array['addUserTaskTender'] = "logicserver/task_action/{$version}/addUserTaskTender.php";
$task_action_file_array['acceptUserTender'] = "logicserver/task_action/{$version}/acceptUserTender.php";
$task_action_file_array['getTaskDetail'] = "logicserver/task_action/{$version}/getTaskDetail.php";
$task_action_file_array['getUserTenderPaging'] = "logicserver/task_action/{$version}/getUserTenderPaging.php";
$task_action_file_array['deleteTaskTender'] = "logicserver/task_action/{$version}/deleteTaskTender.php";
