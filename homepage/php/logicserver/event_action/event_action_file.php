<?php

    /**
     * whiteList是白名单，用来允许某些请求通过验证
     */

    $version = $params['version'] ?? "1";
    $whiteList['getEventsPagingNoLogin'] = 'allow';
    $whiteList['getEventNoLogin'] = 'allow';

    require_once("filter/filter.php");
    $event_action_file_array['addComment'] = "logicserver/event_action/{$version}/addComment.php";
    $event_action_file_array['addEvent'] = "logicserver/event_action/{$version}/addEvent.php";
    $event_action_file_array['getEvent'] = "logicserver/event_action/{$version}/getEvent.php";
    $event_action_file_array['deleteComment'] = "logicserver/event_action/{$version}/deleteComment.php";
    $event_action_file_array['deleteEvent'] = "logicserver/event_action/{$version}/deleteEvent.php";
    $event_action_file_array['getComment'] = "logicserver/event_action/{$version}/getComment.php";
    $event_action_file_array['getEventsPaging'] = "logicserver/event_action/{$version}/getEventsPaging.php";
    $event_action_file_array['getSecretEventsPaging'] = "logicserver/event_action/{$version}/getSecretEventsPaging.php";
    $event_action_file_array['updateEvent'] = "logicserver/event_action/{$version}/updateEvent.php";
    $event_action_file_array['getEventEditHistory'] = "logicserver/event_action/{$version}/getEventEditHistory.php";
    $event_action_file_array['getMyEditEventsPaging'] = "logicserver/event_action/{$version}/getMyEditEventsPaging.php";
    $event_action_file_array['checkUpdatePermission'] = "logicserver/event_action/{$version}/checkUpdatePermission.php";
    $event_action_file_array['getEventWriters'] = "logicserver/event_action/{$version}/getEventWriters.php";
    $event_action_file_array['getEventHistory'] = "logicserver/event_action/{$version}/getEventHistory.php";
    $event_action_file_array['linkEventToPerson'] = "logicserver/event_action/{$version}/linkEventToPerson.php";
    $event_action_file_array['copyEventToAnotherPerson'] = "logicserver/event_action/{$version}/copyEventToAnotherPerson.php";
    $event_action_file_array['uploadFile'] = "logicserver/event_action/{$version}/uploadFile.php";
    $event_action_file_array['getPersonPagingWithEvent'] = "logicserver/event_action/{$version}/getPersonPagingWithEvent.php";
    $event_action_file_array['getEventsPagingNoLogin'] = "logicserver/event_action/{$version}/getEventsPaging.php";
    $event_action_file_array['getEventNoLogin'] = "logicserver/event_action/{$version}/getEventNoLogin.php";
