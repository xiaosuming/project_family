<?php
    use Util\Util;
    use DB\CDBEvent;
    use Util\Check;

    $eventId = Check::checkInteger(trim(isset($params['eventId']) ? $params['eventId'] : ''));//大事件id

    if($eventId == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    try{
        $eventDB = new CDBEvent();
        
        $comments = $eventDB->getEventCommentsTotal($eventId);

        $data['comments'] = $comments;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);        
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }