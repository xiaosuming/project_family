<?php
/**
 * 获取人物列表的大事件分页
 * @author: jiangpengfei
 * @date:   2019-03-15 
 */

use DB\CDBEvent;
use Util\Util;
use Util\Check;
use Util\Paginator;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$count = Check::checkInteger($params['count'] ?? 10);

try {
    $eventDB = new CDBEvent();
    $persons = $eventDB->getPersonPagingWithEvent($familyId, $maxId, $sinceId, $count);
    $total = $eventDB->getPersonWithEventTotal($familyId);

    $len = count($persons);

    $isLast = false;
    if ($len < $count) {
        $isLast = true;
    }

    if ($len > 0) {
        $maxId = $persons[$len - 1]['id'];
        $sinceId = $persons[0]['id'];
    }

    $paginator = new Paginator($total, $persons, $maxId, $sinceId, $isLast);

    $paginator->printPage();
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}