<?php
/**
 * 将属于用户的大事件复制到另一个家族的人物下
 * @author jiangpengfei
 * @date   2017-11-15
 */
use Util\Util;
use DB\CDBEvent;
use DB\CDBFamilyNews;
use Util\Check;
use Util\PostGenerate;
use DB\CDBPerson;

$userId = $GLOBALS['userId'];													//操作用户id
$eventId = Check::checkInteger(trim($params['eventId'] ?? ''));					    //大事件id
$toPersonId = Check::checkInteger(trim($params['toPersonId'] ?? ''));                   //人物id

try{
    $eventDB = new CDBEvent();
    //检查当前用户是否有对事件的操作权限,只有大事件的创建者有复制关联
    $event = $eventDB->getEvent($eventId);
    if($event == null){
        //否则检查操作权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    //检查操作的权限
    if($event->userId != $userId){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    //检查用户是否绑定了这个人物
    $personDB = new CDBPerson();
    $linkPerson = $personDB->getPersonById($toPersonId);
    
    if($linkPerson == null){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常,人物不存在");
        exit;
    }

    if($userId != $linkPerson->userId){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    /* 检查这个人物是否拥有这个大事件 */
    if(!$eventDB->checkEventCanCopyToPerson($eventId,$toPersonId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物已拥有此大事件");
        exit;
    }


    $rows = $eventDB->copyEventToAnotherPerson($eventId,$toPersonId);
    $data['linkCount'] = $rows;
    if($rows > 0){
        $familyNewsDB = new CDBFamilyNews();
        $familyNewsDB->addFamilyNews($linkPerson->familyId,$GLOBALS['EVENT_MODULE'],$GLOBALS['UPDATEEVENT'],$userId,$event->id,$event->expName,$userId,$linkPerson->familyId,$toPersonId);
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
    }else{
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], $data);	
    }
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}