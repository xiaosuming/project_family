<?php
    use Util\Util;
    use DB\CDBEvent;
    //use DB\CDBMessage;
    use Util\Check;
    //use Model\Message;

    $eventId = Check::checkInteger(trim(isset($params['eventId']) ? $params['eventId'] : ''));				//推文id
    $comment = Check::check(trim(isset($params['comment']) ? $params['comment'] : ''),1,255);			//评论内容
    $replyTo = Check::checkInteger(trim(isset($params['replyTo']) ? $params['replyTo'] : ''));
    $replyToUserId = Check::checkInteger(trim(isset($params['replyToUserId']) ? $params['replyToUserId'] : ''));

    if($eventId == "" || $comment == "" || $replyTo == "" || $replyToUserId == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    try{
        $eventDB = new CDBEvent();
        $userId = $GLOBALS['userId'];
    
        $commentId = $eventDB->addEventComment($eventId, $userId, $comment, $replyTo, $replyToUserId);
        
        $data['addedCommentId'] = $commentId;
        
        if($commentId > 0){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "评论失败");
        }
    }catch(PDOException $e){

        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }