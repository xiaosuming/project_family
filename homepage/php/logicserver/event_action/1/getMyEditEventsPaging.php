<?php
    /**
     * 获取用户编辑的分页的大事件
     */	
    use Util\Util;
    use Util\Pager;
    use DB\CDBEvent;
    use Util\Check;
    $myUserId = $GLOBALS['userId'];													                //操作用户id
    
    $personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : '-1'));		                //人物id
    $searchUserId = Check::checkInteger(trim(isset($params['searchUserId']) ? $params['searchUserId'] : '-1'));			//用户id
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : '-1'));		                //家族id
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1'));		                //页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '-1'));		                //每页数量

    try{
        $eventDB = new CDBEvent();
        
        $total = $eventDB->getMyEditEventsCount($personId,$searchUserId,$familyId,$myUserId);
        if($pageSize == -1){
            //-1代表要全部加载
            $pageSize = $total;
        }
        $events = $eventDB->getMyEditEventsPaging($pageIndex,$pageSize,$personId,$searchUserId,$familyId,$myUserId);
        $json_event = array();
        foreach ($events as $v) {
            $v['resources'] = json_decode($v['resources'], true);
            array_push($json_event, $v);
        }
        $pager = new Pager($total,$json_event,$pageIndex,$pageSize);
        $pager->printPage();		//以分页的格式输出
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }