<?php
    /**
     * 获取大事件参与的撰写人
     */
    use Util\Util;
    use DB\CDBEvent;
    use Util\Check;
    $eventId = Check::checkInteger(trim(isset($params['eventId']) ? $params['eventId'] : ''));		//事件id
    $userId = $GLOBALS['userId'];

    $data = array();
    
    //检查是否存在参数缺少的情况
    if(empty($eventId)){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    try{
        $eventDB = new CDBEvent();

        if(!$eventDB->verifyUserIdAndEventId($userId,$eventId)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit;
        }

        $writers = $eventDB->getEventWriters($eventId);
        $data['writers'] = $writers;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }