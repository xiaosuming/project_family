<?php
    /**
     * 检查更新大事件的权限
     */
    use Util\Util;
    use DB\CDBEvent;
    use Util\Check;
    $userId = $GLOBALS['userId'];													//操作用户id
    
    $eventId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));	    //要编辑的事件id

    
    //检查参数是否缺少
    if($eventId == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    try{
        $eventDB = new CDBEvent();
        if($eventDB->checkUpdateEventPermission($userId,$eventId)){
            $data['permission'] = "1";
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
        }else{
            $data['permission'] = "0";
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }