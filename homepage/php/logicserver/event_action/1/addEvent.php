<?php
/**
 * 添加大事件
 */

use DB\CDBFamily;
use DB\CDBPushEvent;
use Util\Util;
use DB\CDBEvent;
use DB\CDBPerson;
use DB\CDBFamilyNews;
use Util\Check;
use Util\PostGenerate;
use DB\DocumentDB;
use Model\Document;
use Model\Event;

$userId = $GLOBALS['userId'];        //操作用户id
$userIdTmp = $userId;                //这个人物对应的id暂时设置为当前用户，如果没有指明personId，则指关联自己
$familyId = -1;                    //最初的家族id不存在

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : '-1'));        //要添加的事件的人物id
$iconograph = Check::check(trim(isset($params['iconograph']) ? $params['iconograph'] : ''));    //事件配图
$type = Check::checkInteger(trim(isset($params['type']) ? $params['type'] : 1));                //事件类型 1是公开，2是秘密
$openTime = Check::checkDate(trim(isset($params['openTime']) ? $params['openTime'] : '1970-01-01'));                        //事件的公开时间
$expTime = Check::checkDate(trim(isset($params['expTime']) ? $params['expTime'] : ''));                //事件的经历时间
$expName = Check::check(trim(isset($params['expName']) ? $params['expName'] : ''), 1, 30);            //事件标题
$expContent = trim(isset($params['expContent']) ? $params['expContent'] : '');    //事件的经历内容
$writeMode = Check::checkInteger(trim(isset($params['writeMode']) ? $params['writeMode'] : ''));        //撰写模式，0是单独撰写，1是联合撰写
$resources = isset($params['resources']) ? $params['resources'] : '';  //传来的json格式的字符串
$isMd = Check::checkInteger(trim(isset($params['isMd']) ? $params['isMd'] : 1));

//检查是否存在参数缺少的情况
if (empty($iconograph) || empty($personId) || empty($expTime) || empty($expName) || empty($expContent) || $writeMode == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失1");
    exit;
}
//如果是秘密事件，则必须有公开时间
if ($type == 2 && $openTime == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}
try {
    $personDB = new CDBPerson();
    if ($personId != -1) {
        //首先查询这个人物id的信息
        $person = $personDB->getPersonById($personId);
        if ($person == null) {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "人物不存在");
            exit;
        }

        $userIdTmp = $person->userId;
        $familyId = $person->familyId;
        $CDBFamily = new CDBFamily();
        $family = $CDBFamily->getFamilyById($familyId);
        $familyName = $family->name;
        if (empty($userIdTmp)) {
            //如果不存在对应的用户

            //检查操作用户对当前人物是否有管理权限
            if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
                exit;
            }

        } else {
            //如果存在对应用户

            //检查操作的用户是不是这个人物自己
            if ($userId != $userIdTmp) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
                exit;
            }
        }
    }

    $eventDB = new CDBEvent();

    //如果$userIdTmp为空，则指为-1
    if ($userIdTmp == "")
        $userIdTmp = 0;

    $event = new Event();
    $event->personId = $personId;
    $event->userId = $userIdTmp;
    $event->familyId = $familyId;
    $event->iconograph = $iconograph;
    $event->type = $type;
    $event->openTime = $openTime;
    $event->expTime = $expTime;
    $event->expName = $expName;
    $event->expContent = $expContent;

    $event->writeMode = $writeMode;
    $event->isMd = $isMd;
    $event->createBy = $userId;
    $event->updateBy = $userId;

    if ($resources == ''){
        $resources=json_encode(array());
    }

    $resourceArr = json_decode($resources, true);
    $error = json_last_error();
    if ($error != JSON_ERROR_NONE) {
        Util::printResult($GLOBALS['ERROR_JSON_FORMAT'], 'JSON格式错误');
        exit;
    }
    foreach ($resourceArr as &$resource) {
        if (!array_key_exists('type', $resource) || !array_key_exists('src', $resource) || $resource['type'] == '' || $resource['src'] == '') {
            Util::printResult($GLOBALS['ERROR_JSON_VALUE'], 'JSON字段不正确');
            exit;
        }
    }
    $event->resources = $resources;
    $eventId = $eventDB->addEvent($event);
    $data['eventId'] = $eventId;
    if ($eventId > 0) {
        if ($type == 1 && $familyId != -1) {
            $familyNewsDB = new CDBFamilyNews();
            $familyNewsDB->addFamilyNews($familyId, $GLOBALS['EVENT_MODULE'], $GLOBALS['ADDEVENT'], $userId, $eventId, $expName, $userId, $familyId, $personId);
        }
        if ($GLOBALS['TEST_EVENT_TASK']) {
            
            if ($personId != -1) {
                $CDBPushEvent = new CDBPushEvent();
                $modelId = $eventId;
                $action = 1;
                $source = [
                    'id' => $eventId,
                    'personId' => $personId,
                    'familyId' => $familyId,
                    'familyName' => $familyName,
                    'photo' => $iconograph,
                    'expName' => $expName,
                    'expContent' => $expContent,
                    'isMd' => $isMd
                ];
                $CDBPushEvent->setPushEventTask($modelId, $action, $source);
            }

        }
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}