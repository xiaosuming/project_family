<?php
    /**
     * 获取大事件
     */
    use Util\Util;
    use DB\CDBEvent;
    use DB\CDBFamily;
    use Util\Check;
    $userId = $GLOBALS['userId'] ?? 0;		//操作用户id
    
    $eventId = Check::checkInteger(trim(isset($params['eventId']) ? $params['eventId'] : ''));		//事件id

    $data = array();
    
    //检查是否存在参数缺少的情况
    if(empty($eventId)){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    try{
        $eventDB = new CDBEvent();
        $event = $eventDB->getEvent($eventId);

        if ($event == null) {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该事件");	
            exit();
        }

        $familyId = $event->familyId;
        $family = null;

        if (is_numeric($familyId)) {
            $familyDB = new CDBFamily();
            $family = $familyDB->getFamilyById($familyId);
        }

        if ($family != null && $family->openType == 0) {
            if(!$eventDB->verifyUserIdAndEventId($userId,$eventId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");	
                exit();
            }
        }

        $resources=$event->resources;
        $resources=json_decode($resources,true);
        $event->resources=$resources;
        $event->expContent = htmlspecialchars_decode($event->expContent);

        $data['event'] = $event;
        if($event != null){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }