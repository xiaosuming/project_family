<?php
/**
 * 更新大事件,这个更新策略只能由创建记录的人更新
 */

use DB\CDBPushEvent;
use Util\Util;
use DB\CDBEvent;
use DB\CDBFamilyNews;
use Util\Check;
use Util\PostGenerate;

$userId = $GLOBALS['userId'];                                                    //操作用户id
$iconograph = Check::check(trim(isset($params['iconograph']) ? $params['iconograph'] : ''));    //事件配图
$eventId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));                        //要编辑的事件id
$expTime = Check::checkDate(trim(isset($params['expTime']) ? $params['expTime'] : ''));                    //事件的经历时间
$expName = Check::check(trim(isset($params['expName']) ? $params['expName'] : ''), 1, 30);                //事件标题
$expContent = trim(isset($params['expContent']) ? $params['expContent'] : '');    //事件的经历内容
$writeMode = Check::checkInteger(trim(isset($params['writeMode']) ? $params['writeMode'] : ''));        //撰写模式，0是单独撰写，1是联合撰写
$syncAll = Check::checkInteger(trim(isset($params['syncAll']) ? $params['syncAll'] : '1'));             //是否同步更新所有副本
$updateBy = $userId;                                                                                    //更新记录的用户id
$updateTime = Util::getCurrentTime();                                                                    //记录更新时间
$resources = isset($params['resources']) ? $params['resources'] : '';  //传来的json格式的字符串
$isMd = Check::checkInteger(trim(isset($params['isMd']) ? $params['isMd'] : 1));


//检查参数是否缺少
if ($eventId == "" || $expTime == "" || $expName == "" || $expContent == "" || $writeMode == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {
    $eventDB = new CDBEvent();
    //检查当前用户是否有对事件的操作权限(谁建立谁就有权限更新所有属性，如果是联合撰写，则允许其他家族人员更改事件时间和内容)
    $event = $eventDB->getEvent($eventId);
    if ($event == null) {
        //否则检查操作权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    //检查编辑的权限
    if (!$eventDB->checkUpdateEventPermission($userId, $eventId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($syncAll == 1) {
        $eventsId = $eventDB->getEventAllCopy($eventId, $event->sourceId);
    } else {
        $eventsId[]['id'] = $eventId;
    }
    $data['updateEventCount'] = 0;

    if ($resources == '') {
        $resources = json_encode(array());
    }
    $resourceArr = json_decode($resources, true);
    $error = json_last_error();
    if ($error != JSON_ERROR_NONE) {
        Util::printResult($GLOBALS['ERROR_JSON_FORMAT'], 'JSON格式错误');
        exit;
    }
    foreach ($resourceArr as $resource) {
        if (!array_key_exists('type', $resource) || !array_key_exists('src', $resource) || $resource['type'] == '' || $resource['src'] == '') {
            Util::printResult($GLOBALS['ERROR_JSON_VALUE'], 'JSON字段不正确');
            exit;
        }
    }

    foreach ($eventsId as $tmpEvent) {
        $tmpEventId = $tmpEvent['id'];
        $rows = $eventDB->updateEvent($tmpEventId, $expTime, $expName, $expContent, $writeMode, $updateTime, $updateBy, $resources, $isMd, $iconograph);
        $data['updateEventCount'] += $rows;
    }
    if ($rows > 0) {
        if ($event->familyId != -1) {
            $familyNewsDB = new CDBFamilyNews();
            $familyNewsDB->addFamilyNews($event->familyId, $GLOBALS['EVENT_MODULE'], $GLOBALS['UPDATEEVENT'], $userId, $eventId, $expName, $userId, $event->familyId, $event->personId);
        }
        if ($GLOBALS['TEST_EVENT_TASK']) {
            $CDBPushEvent = new CDBPushEvent();
            $modelId = $eventId;
            $action = 2;
            $source = [
                'id' => $eventId,
                'expName' => $expName,
                'expContent' => $expContent
            ];
            $CDBPushEvent->setPushEventTask($modelId, $action, $source);

        }
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], $data);
    }


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}