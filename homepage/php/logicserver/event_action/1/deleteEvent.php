<?php
    /**
     * 删除大事件
     */

use DB\CDBPushEvent;
use Util\Util;
    use DB\CDBEvent;
    use DB\CDBFamilyNews;
    use Util\Check;
    use Util\PostGenerate;
    $userId = $GLOBALS['userId'];		//操作用户id
    
    $eventId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));	//要删除的事件id
    
    //先检查参数缺少
    if(empty($eventId)){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    
    try{
        $eventDB = new CDBEvent();
        //检查当前用户是否有对事件的操作权限(谁建立谁就有权限删除)
        $event = $eventDB->getEvent($eventId);
        if($event != null && $event->createBy != $userId){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
        
        $rows = $eventDB->deleteEvent($eventId);
        $data['deleteEventCount'] = $rows;
        if($rows > 0){
            //如果这个大事件存在家族，则向家族中推送消息
            if($event->familyId > 0){
                $familyNewsDB = new CDBFamilyNews();
                $familyNewsDB->addFamilyNews($event->familyId,$GLOBALS['EVENT_MODULE'],$GLOBALS['DELETEEVENT'],$userId,$eventId,$event->expName,$userId);
            }
            if ($GLOBALS['TEST_EVENT_TASK']) {
                $CDBPushEvent = new CDBPushEvent();
                $modelId = $eventId;
                $action = 3;
                $source = [];
                $CDBPushEvent->setPushEventTask($modelId, $action, $source);

            }
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], $data);	
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }