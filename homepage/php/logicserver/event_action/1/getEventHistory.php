<?php
    /**
     * 获取大事件历史
     */
    use Util\Util;
    use DB\CDBEvent;
    use Util\Check;
    $userId = $GLOBALS['userId'];		//操作用户id
    
    $historyId = Check::checkInteger(trim(isset($params['historyId']) ? $params['historyId'] : ''));		//事件id
    
    //检查是否存在参数缺少的情况
    if(empty($historyId)){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    try{
        $eventDB = new CDBEvent();
        $eventHistory = $eventDB->getEventHistory($historyId);
        $eventHistory->expContent = htmlspecialchars_decode($eventHistory->expContent);
        $data['eventHistory'] = $eventHistory;
        if($eventHistory != null){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "不存在该事件");	
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }