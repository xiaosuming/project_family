<?php
    /**
     * 获取大事件的编辑历史
     */
    
    use Util\Util;
    use DB\CDBEvent;
    use Util\Check;

    $userId = $GLOBALS['userId'];			//用户id
    $eventId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));			//要编辑的事件id
    
    //检查参数是否缺少
    if(empty($eventId)){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    try{
        $eventDB = new CDBEvent();
        //检查用户id是否有对编辑历史的查看权限（权限设置为同一个家族的用户可以查看家族事件的编辑历史）
        if(!$eventDB->verifyUserIdAndEventId($userId,$eventId)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
        
        $history = $eventDB->getEventEditHistory($eventId);		//获取事件的编辑历史
        $data['history'] = $history;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);			//打印
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }