<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/22 0022
 * Time: 17:40
 */

use DB\CDBFile;
use Util\Check;
use Util\FileUtil;
use Util\Util;
use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;

// require_once($_SERVER['DOCUMENT_ROOT'] . "/php/config/ip_map.php");

$file = Check::checkArray(isset($_FILES['file']) ? $_FILES['file'] : '');
$module = Check::checkInteger(isset($params['module']) ? $params['module'] : '');
$userId = $GLOBALS['userId'];
$client = new StorageFacadeClient();
$client->setBucket(StorageBucket::$IMAGE);

$strArray = explode(".", $file['name']);

if (count($strArray) - 1 >= 0) {
    //获取后缀
    $ext_name = $strArray[count($strArray) - 1];
    $fileInfo = null;
    //根据文件名上传
    $fileInfo = $client->upload($file['tmp_name'], strtolower($ext_name));

    if ($fileInfo->success) {
        // $source_ip = $fileInfo['source_ip'];
        // $group_name = $fileInfo['group_name'];
        // $remote_filename = $fileInfo['remote_filename'];
        $file_url = $fileInfo->location;
        $size = $fileInfo->size;
        $file_type = FileUtil::getFileType($file);
        $data = [
            'location' => $file_url,
            'size' => $size,
            'fileType' => $file_type
        ];
        $CDBFile = new CDBFile();
        $result = $CDBFile->addFile($file_url, $module, $userId);
        if ($result > 0) {
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_INSET'], '数据插入出错');
            exit;
        }

    } else {
        Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], '文件上传失败');
        exit;
    }
} else {
    Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], "图片上传错误,暂时不支持无后缀名上传");
    exit();
}