<?php
/**
 * 获取分页的大事件
 */

use Util\Util;
use Util\Pager;
use DB\CDBEvent;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

$userId = $GLOBALS['userId'] ?? 0;                                                    //操作用户id

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : -1));        //人物id
$userIdTmp = Check::checkInteger(trim(isset($params['userId']) ? $params['userId'] : -1));            //用户id
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : -1));        //家族id
$startTime = Check::check(trim(isset($params['startTime']) ? $params['startTime'] : -1));        //开始时间
$endTime = Check::check(trim(isset($params['endTime']) ? $params['endTime'] : -1));            //结束时间
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : ''));        //页码
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '-1'));        //每页数量

$isUserSelf = false;

if ($familyId == -1 && $personId == -1 && $userIdTmp == -1) {
    //都没指定,默认看自己所有的大事件
    $userIdTmp = $userId;
    $isUserSelf = true;     //获取用户自己的大事件
}

try {
    $personDB = new CDBPerson();
    if ($familyId != -1) {
        //检查用户是否有权限查看家族大事件
        $familyDB = new CDBFamily();

        $family = $familyDB->getFamilyById($familyId);

        if ($family == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }

        if ($family->openType == 0) {

            if (!$familyDB->isUserForFamily($familyId, $userId)) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
                exit;
            }
        }
    } else if ($personId != -1) {
        //没有指定家族，但是指定了人物id，不允许查看
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    } else if ($userId != $userIdTmp && $userIdTmp != -1) {
        //单独指定要查看别的用户的大事件，不允许查看
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    $eventDB = new CDBEvent();
    $total = $eventDB->getEventsTotal($personId, $userIdTmp, $familyId, $startTime, $endTime);
    if ($pageSize == -1) {
        //-1代表要全部加载
        $pageSize = $total;
    }
    
    $events = $eventDB->getEventsPaging($personId, $userIdTmp, $familyId, $startTime, $endTime, $pageIndex, $pageSize,$isUserSelf);
    $json_event = array();
    foreach ($events as $v) {
        $v['resources'] = json_decode($v['resources'], true);
        array_push($json_event, $v);
    }
    $pager = new Pager($total, $json_event, $pageIndex, $pageSize);
    $pager->printPage();        //以分页的格式输出
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}