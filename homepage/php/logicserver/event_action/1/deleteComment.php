<?php
    use Util\Util;
    use DB\CDBEvent;
    use Util\Check;

    $commentId = Check::checkInteger(trim(isset($params['commentId']) ? $params['commentId'] : ''));				//评论id

    try{
        if($commentId == "")
        {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }

        $eventDB = new CDBEvent();
        $userId = $GLOBALS['userId'];

        //检查权限
        if($eventDB->verifyCommentIdAndUserId($userId, $commentId)){//有权限
            $rows = $eventDB->deleteComment($commentId);
            $data['deletedComments'] = $rows;

            if($rows > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
            }
        }else{//无权限
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }