<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/15 0015
 * Time: 13:34
 */
require_once("logicserver/family_storage_action/family_storage_action_file.php");

use Util\Util;

$sub_action = isset($params['sub_action']) ? $params['sub_action'] : "0";

if (!array_key_exists($sub_action, $family_storage_action_file_array)) {
    $errorCode = $GLOBALS['ERROR_REQUEST'];
    $data = "family storage action not exist" . $sub_action;
    Util::printResult($errorCode, $data);
    exit(-1);
}

$sub_action_file = $family_storage_action_file_array[$sub_action];

if (!file_exists($sub_action_file)) {
    $errorCode = $GLOBALS['ERROR_REQUEST'];
    $data = "family storage action file not exist" . $sub_action_file;
    Util::printResult($errorCode, $data);
    exit(-1);
}

require_once($sub_action_file);