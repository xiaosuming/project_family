<?php
    require_once("logicserver/event_action/event_action_file.php");
    
    use Util\Util;

    $sub_action = isset($params['sub_action'])?$params['sub_action']:""; 
    
    if (!array_key_exists($sub_action, $event_action_file_array))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "event action not exist".$sub_action;
        Util::printResult($errorCode, $data);
        exit;
    }
    
    $sub_action_file = $event_action_file_array[$sub_action];
    
    if (!file_exists($sub_action_file))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "event action file not exist".$sub_action_file;
        Util::printResult($errorCode, $data);
        exit;
    }
    
    require_once($sub_action_file);