<?php	
    use Util\Util;
    use Util\Pager;
    use DB\CDBMessage;
    use Util\Check;

    $isRead = 0;	//查询是否读取的　-1全部 0未读 1已读
    $module = Check::checkModule(trim(isset($params['module']) ? $params['module'] : -1));

    try{
        $messageDB = new CDBMessage();
        $userId = $GLOBALS['userId'];

        $count = $messageDB->getMessagesCount($userId,$module,$isRead);
        $data['count'] = $count;

        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }