<?php	
    use Util\Util;
    use Util\Check;
    use DB\CDBMessage;

    $messageIds = Check::checkIdArray(trim(isset($params['messageIds']) ? $params['messageIds'] : ''));
    $isRead = Check::checkInteger(trim(isset($params['isRead']) ? $params['isRead'] : ''));

    if($messageIds == "" || $isRead == "" ){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    try{
        $messageDB = new CDBMessage();
        $userId = $GLOBALS['userId'];
        $updatedRows = $messageDB->updateMessageStatus($messageIds,$isRead,$userId);
        $data['updatedCount'] = $updatedRows;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }