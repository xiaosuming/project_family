<?php	
    use Util\Util;
    use Util\Check;
    use DB\CDBMessage;

    $messageIds = Check::checkIdArray(trim(isset($params['messageIds']) ? $params['messageIds'] : ''));

    if($messageIds == ''){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    try{
        $messageDB = new CDBMessage();
        $userId = $GLOBALS['userId'];
        $deletedRows = $messageDB->deleteMessage($messageIds,$userId);
        $data['deleteCount'] = $deletedRows;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }