<?php	
    use Util\Util;
    use Util\Pager;
    use DB\CDBMessage;
    use Util\Check;


    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));		//页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 10));		//页面大小
    $isRead = Check::checkInteger(trim(isset($params['isRead']) ? $params['isRead'] : -1));            	//查询是否读取的　-1全部 0未读 1已读
    $module = Check::checkModule(trim(isset($params['module']) ? $params['module'] : -1));

    try{
        $messageDB = new CDBMessage();
        $userId = $GLOBALS['userId'];

        $messages = $messageDB->getMessagesPaging($pageIndex,$pageSize,$userId,$module,$isRead);
        $count = $messageDB->getMessagesCount($userId,$module,$isRead);
        
        $page = new Pager($count,$messages,$pageIndex,$pageSize);
        $page->printPage();
        
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }