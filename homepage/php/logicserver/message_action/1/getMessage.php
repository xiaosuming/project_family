<?php	
    use Util\Util;
    use Util\Pager;
    use DB\CDBMessage;
    use Util\Check;


    $messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));		//消息id

    try{
        $messageDB = new CDBMessage();
        $userId = $GLOBALS['userId'];
        //检查该消息的查看权限
        $message = $messageDB->getMessage($messageId);
        if($message == null){
            Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
            exit;
        }else if($message->toUser != $userId){
            Util::printResult($GLOBALS['ERROR_PERMISSION'],"权限不足");
            exit;
        }
        $data['message'] = $message;
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
        exit;
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }