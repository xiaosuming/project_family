<?php
    /**
     * 消息操作索引文件
     */

    /**
     * whiteList是白名单，用来允许某些请求通过验证
     */
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $message_action_file_array['addMessage'] = "logicserver/message_action/{$version}/addMessage.php";
    $message_action_file_array['deleteMessage'] = "logicserver/message_action/{$version}/deleteMessage.php";
    $message_action_file_array['getMessagesPaging'] = "logicserver/message_action/{$version}/getMessagesPaging.php";
    $message_action_file_array['updateMessageStatus'] = "logicserver/message_action/{$version}/updateMessageStatus.php";
    $message_action_file_array['getMessage'] = "logicserver/message_action/{$version}/getMessage.php";
    $message_action_file_array['getUnReadMessageCount'] = "logicserver/message_action/{$version}/getUnReadMessageCount.php";