<?php

use Util\Util;
use DB\CDBInfoCard;
use DB\CDBMessage;
use Util\Check;
use DB\CDBAccount;

$messageId = Check::checkInteger(trim(isset($params['messageId']) ? $params['messageId'] : ''));        //消息id

$userId = $GLOBALS['userId'];

try {

    $messageDB = new CDBMessage();
    $message = $messageDB->getMessage($messageId);

    $accountDB = new CDBAccount();
    if ($message == null || !$accountDB->checkUserPermission($message->toUser, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $infoCardId = $message->recordId;

    $infoCardDB = new CDBInfoCard();
    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    if ($infoCard == null) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "名片不存在");
        exit;
    }


    if (!$accountDB->checkUserPermission($infoCard->userId, $userId)) {
        unset($infoCard->identity);
    }

    $statistic = $infoCardDB->getInfoCardStatistic($infoCardId);
    if ($statistic) {   // redis中如果存在该名片的统计信息
        $infoCard->viewNum = $statistic['v'] ?? 0;
        $infoCard->shareNum = $statistic['s'] ?? 0;
    } else {
        $infoCard->viewNum = 0;
        $infoCard->shareNum = 0;
    }

    $data['infoCard'] = $infoCard;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}