<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/13 0013
 * Time: 10:04
 */

use DB\CDBInfoCard;
use Util\Util;

$userId = $GLOBALS['userId'];

try {

    $cdbInfoCard = new CDBInfoCard();
    $familyList = $cdbInfoCard->getImportFamilyList($userId);

    foreach ($familyList as $key => $v) {
        $v = join(',', $v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串
        $temp[$key] = $v;
    }
    $temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组
    foreach ($temp as $k => $v) {
        $array = explode(',', $v); //再将拆开的数组重新组装
        $temp2[$k]['familyId'] = $array[0];
        $temp2[$k]['type'] = $array[1];
        $temp2[$k]['name'] = $array[2];
        $temp2[$k]['surname'] = $array[3];
        $temp2[$k]['description'] = $array[4];
        $temp2[$k]['photo'] = $array[5];
    }

    $data['familyList'] = array_values($temp2);
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->err(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}