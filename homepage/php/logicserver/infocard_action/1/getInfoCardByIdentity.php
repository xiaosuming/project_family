<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2018/3/5 0005
 * Time: 17:25
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;

$identity = Check::check($params['identity'] ?? '');

if ($identity == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}
try {

    $infoCardDB = new CDBInfoCard();
    $result = $infoCardDB->getInfoCardByIdentity($identity);

    if ($result === null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '名片不存在');
        exit;
    }

    $result['phone'] = json_decode($result['phone'],true);
    $result['skills'] = json_decode($result['skills'],true);
    $result['favorites'] = json_decode($result['favorites'],true);
    $result['jobs'] = json_decode($result['jobs'], true);

    $data['infoCard'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}