<?php

/**
 * 获取该名片的传递网络
 * @Author: jiangpengfei
 * @Date:  2018-11-19
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    // 检查用户对名片的权限
    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    if ($infoCard == null) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $accountDB = new CDBAccount();
    if (!$accountDB->checkUserPermission($infoCard->userId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    // 获取该名片的所有传输途径

    $records = $infoCardDB->getInfoCardShareRecords($infoCardId);
    $nodes = [];    // 点的集合
    $edges = [];    // 边的集合

    $nodeExist = [];
    $edgeExist = [];

    foreach($records as $record) {

        if (!isset($nodeExist[$record['share_userId']])) {
            $node['id'] = $record['share_userId'];
            $node['labels'] = [];
            $node['labels']['nickname'] = $record['srcNickname'];
            $node['labels']['photo'] = $record['srcPhoto'];
            $nodes[] = $node;
            $nodeExist[$record['share_userId']] = true;
        }

        if (!isset($nodeExist[$record['target_userId']])) {
            $node['id'] = $record['target_userId'];
            $node['labels'] = [];
            $node['labels']['nickname'] = $record['dstNickname'];
            $node['labels']['photo'] = $record['dstPhoto'];
            $nodes[] = $node;
            $nodeExist[$record['target_userId']] = true;
        }

        if (!isset($edgeExist[$record['share_userId'].'@'.$record['target_userId']])) {
            $edge['src'] = $record['share_userId'];
            $edge['dst'] = $record['target_userId'];
            $edge['labels'] = [];
            $edge['labels']['time'] = $record['create_time'];
            $edges[] = $edge;

            $edgeExist[$record['share_userId'].'@'.$record['target_userId']] = true;
        }
    }


    $data['graph']['nodes'] = $nodes;
    $data['graph']['edges'] = $edges;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
 //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}