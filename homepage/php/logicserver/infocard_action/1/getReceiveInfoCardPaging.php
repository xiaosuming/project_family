<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/6 0006
 * Time: 12:39
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Pager;
use Util\Util;

$userId = $GLOBALS['userId'];
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));        //页码
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 6));

try {
    $infoCardDB = new CDBInfoCard();
    $total = $infoCardDB->countReceiveInfoCard($userId);
    if ($total == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '没有接收到的名片');
        exit;
    }
    $receiveInfoCard = $infoCardDB->getReceiveInfoCardPaging($userId, $pageIndex, $pageSize);

    foreach ($receiveInfoCard as $key => $v) {
        $receiveInfoCard[$key]['phone'] = json_decode($v['phone'],true);
        $jobs = json_decode($v['jobs'],true);
        if (empty($jobs)){
            $receiveInfoCard["$key"]['jobs'] = array();
        }else{
            $arr = array_pop($jobs);
            $receiveInfoCard["$key"]['jobs'] = $arr;
        }
    }

    $pager = new Pager($total, $receiveInfoCard, $pageIndex, $pageSize);
    $pager->printPage();

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}