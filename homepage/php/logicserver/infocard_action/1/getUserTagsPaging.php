<?php
/**
 * 分页获取用户标签
 * @author: jiangpengfei
 * @date:   2019-03-13
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;
use Util\Paginator;

$userId = $GLOBALS['userId'];
$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$count = Check::checkInteger($params['count'] ?? 10);

try {
    $infoCardDB = new CDBInfoCard();
    $paging = $infoCardDB->getUserTagsPaging($userId, $maxId, $sinceId, $count);

    $total = $infoCardDB->getUserTagsTotal($userId);

    $maxId = 0;
    $sinceId = 0;
    if (count($paging) > 0) {
        $maxId = $paging[count($paging) - 1]['id'];
        $sinceId = $paging[0]['id'];
    }

    $isLast = count($paging) == $count ? false : true;

    $paginator = new Paginator($total, $paging, $maxId, $sinceId, $isLast);

    $paginator->printPage();

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}