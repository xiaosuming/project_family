<?php
/**
 * 接收交换名片的请求
 * 从message中读取recordId，recordId就是要接收的名片id
 * 
 * @author jiangpengfei
 * @date   2018-07-23
 */

use DB\CDBMessage;
use DB\CDBInfoCard;
use DB\CDBPushInfoCard;
use Model\InfoCardShareRecord;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$messageId = Check::checkInteger($params['messageId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    $messageDB = new CDBMessage();
    // 获取消息详情
    $message = $messageDB->getMessage($messageId);
    
    $accountDB = new CDBAccount();
    // 检查消息的权限
    if($message == null || !$accountDB->checkUserPermission($message->toUser, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }
    
    $infoCardId = $message->recordId;    // 要接收的记录id



    $count = $infoCardDB->existsCardIdOnReceive($infoCardId, $userId);

    if ($count > 0) {
        // 将消息置位接受状态
        $messageDB->acceptInvite($messageId);
        $data['cardId'] = $infoCardId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }

    $infoCardDB->pdo->beginTransaction();

    $receiveId = $infoCardDB->receiveInfoCardById($infoCardId, $userId);

    if ($receiveId > 0) {

        // 接收成功，转发量+1
        $infoCardDB->increShare($infoCardId);

        // 将此次转发记录存储
        $record = new InfoCardShareRecord();
        $record->shareTime = date('Y-m-d H:i:s',$time);
        $record->shareUserId = $message->fromUser;
        $record->shareCardId = $infoCardId;
        $record->ownerUserId = $message->fromUser;
        $record->targetUserId = $userId;
        $infoCardDB->addShareRecord($record);

        // 将消息置位接受状态
        $messageDB->acceptInvite($messageId);

        // 接收名片成功,将该名片也作为索引任务推送到队列中
        $pushInfoCardDB = new CDBPushInfoCard();
        $modelId =  $message->fromUser.'_'.$infoCardId;
        $action = 2;    // 更新操作，这里只更新该名片的访问权限
        $source = [
            "script" => [
              "source" => "ctx._source.access.add(params.access)",
              "params" => ["access" => $userId]
            ]
        ];
        $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);

        $infoCardDB->pdo->commit();
        $data['receiveId'] = $receiveId;
        $data['cardId'] = $infoCardId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    } else {
        $infoCardDB->pdo->rollback();
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "未成功接收到名片");
        exit;
    }


} catch (PDOException $e) {
    $infoCardDB->pdo->rollback();
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
