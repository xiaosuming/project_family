<?php
/**
 * 获取群体名片交换的详情
 * @Author: jiangpengfei
 * @Date: 2018-10-02
 */

use DB\CDBInfoCard;
use Model\InfoCardExchange;
use Util\Check;
use Util\Util;

$exchangeId = Check::checkInteger($params['exchangeId'] ?? '');
$userId = $GLOBALS['userId'];

try {

    $infoCardDB = new CDBInfoCard();

    // 检查用户是否参加过
    if (!$infoCardDB->checkUserExistInExchange($exchangeId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $detail = $infoCardDB->getInfocardExchange($exchangeId);

    $records = $infoCardDB->getJoinExchangeRecords($exchangeId);

    $count = count($records);

    foreach($records as $key => $record) {
        $records[$key]['phone'] = json_decode($record['phone'], true);
    }

    $data['detail'] = $detail;
    $data['total'] = $count;
    $data['infocards'] = $records;



    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}