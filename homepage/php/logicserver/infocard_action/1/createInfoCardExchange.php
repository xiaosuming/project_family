<?php
/**
 * 创建名片交换
 * @Author: jiangpengfei
 * @Date: 2018-10-02
 */

use DB\CDBInfoCard;
use Model\InfoCardExchange;
use Util\Check;
use Util\Util;
use DB\CDBAccount;
use ThirdParty\MiniProgram;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$title = Check::check($params['title'] ?? '', 1, 20);
$password = Check::check($params['password'], 0, 20);
$title = Util::filterContentReplace($title);
$userId = $GLOBALS['userId'];

try {
    // 检查名片是不是用户自己的
    $infoCardDB = new CDBInfoCard();
    $infocard = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();

    if ($infocard == null || !$accountDB->checkUserPermission($infocard->userId, $userId)) {
        // 权限不足
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $exchange = new InfoCardExchange();
    $exchange->userId = $userId;
    $exchange->title = $title;
    $exchange->code = md5($userId . time() . Util::generateRandomCode(10));

    if ($password != '') {
        $password = hash('sha256', $password);
    }
    $exchange->password = $password;

    $wxApp = new MiniProgram();
//    foreach ([$title,$password] as $content){
//    }
    $checkMsg = $wxApp->msgSecCheck([$title]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }

    $exchangeId = $infoCardDB->createInfoCardExchange($exchange, $infoCardId);

    $data['exchangeId'] = $exchangeId;
    $data['code'] = $exchange->code;
    $data['hasPassword'] = $password != '';

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
