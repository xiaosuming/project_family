<?php
/**
 * 根据tagId获取所有的名片
 * @author: jiangpengfei
 * @date:   2019-03-14
 */

use Util\Util;
use Util\Check;
use DB\CDBInfoCard;

$tagId = Check::checkInteger($params['tagId'] ?? '');

try {
    $infoCardDB = new CDBInfoCard();
    
    $infoCards = $infoCardDB->getAllInfoCardsByTagId($tagId);
    $data['infoCards'] = $infoCards;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}