<?php
/**
 * 获取用户创建的名片交换
 * @Author: jiangpengfei
 * @Date: 2018-10-02
 */

use DB\CDBInfoCard;
use Model\InfoCardExchange;
use Util\Check;
use Util\Util;

$pageIndex = Check::checkInteger($params['pageIndex'] ?? '1');
$pageSize = Check::checkInteger($params['pageSize'] ?? '10');
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    $exchanges = $infoCardDB->getUserInfoCardExchange($pageIndex, $pageSize, $userId);

    $data['exchanges'] = $exchanges;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
