<?php

use DB\CDBInfoCard;
use DB\CDBPushInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$infoCardId = Check::checkInteger(trim(isset($params['infoCardId']) ? $params['infoCardId'] : ''));        //用户名片id

try {
    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];
    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();

    //验证用户对名片的权限
    if ($infoCard == null || !$accountDB->checkUserPermission($infoCard->userId, $userId)) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $deleteRow = $infoCardDB->deleteInfoCardAndCircle($infoCardId,$infoCard->socialCircleId);

    if ($deleteRow>0){
        //search 添加进搜索任务
        if ($GLOBALS['TEST_INFO_CARD_TASK']) {
            $CDBPushInfoCard = new CDBPushInfoCard();
            $modelId =  $userId.'_'.$infoCardId;
            $action = 3;
            $source = [];
            $CDBPushInfoCard->setPushInfoCardTask($modelId, $action, $source);
        }
        $data['deleteRow'] = $deleteRow;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }else{
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], '删除出错');
    }


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
