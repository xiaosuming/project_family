<?php
/**
 * 获取其他用户名片的转发码
 * 
 * 这个接口将其他用户的名片identity和分享时间戳加密,生成分享码
 * @author jiangpengfei
 * @date 2018-06-07
 */
use DB\CDBInfoCard;
use Util\Util;
use Util\Check;

$userId = $GLOBALS['userId'];
$key = $GLOBALS['INFOCARD_SHARE_CODE_KEY'];     // 加密算法中要用到的key
$iv = $GLOBALS['INFOCARD_SHARE_CODE_IV'];       // 加密算法中要用到的iv
$shareCardId = Check::checkInteger($params['infocardId']);             // 要分享转发的名片id


try {

    $infocardDB = new CDBInfoCard();
    $infocard = $infocardDB->getInfoCard($shareCardId);
    
    // 这里判断用户对名片的权限
    if ($infocard == null || $infocard->permission == 0) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    // 判断名片是不是属于用户的接收名片
    if (!$infocardDB->verifyUserIdInReceive($shareCardId,$userId)){
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $srcText['identity'] = $infocard->identity; // 名片的identity
    $srcText['time'] = time();                   // 分享的时间戳
    $srcText['shareUserId'] = $userId;          // 分享用户id

    $method = 'aes128';
    $secretData = openssl_encrypt(json_encode($srcText), $method, $key, true, $iv);

    $data['sharecode'] = base64_encode($secretData);
    $data['shareUserId'] = $userId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}