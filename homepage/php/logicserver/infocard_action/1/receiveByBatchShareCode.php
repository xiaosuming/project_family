<?php
/**
 * 根据批量分享码来接收, 比如说用户将一个tag内的
 * @author: jiangpengfei
 * @date:   2019-03-14
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;

$shareCode = Check::check($params['shareCode'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    $receiveNum = $infoCardDB->receiveInfoCardByBatchShareCode($shareCode, $userId);

    $data['receive'] = $receiveNum;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}