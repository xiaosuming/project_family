<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-30
 * Time: 上午11:06
 */

use DB\CDBInfoCard;
use Model\InfoCard;
use Util\Check;
use Util\Util;
use ThirdParty\MiniProgram;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$company = Check::check($params['company'] ?? '');
$companyEn = Check::check($params['companyEn'] ?? '');  //公司英文名
$companyProfile = Check::check($params['companyProfile'] ?? '',0,500); //公司
$bankAccount = Check::check($params['bankAccount'] ?? ''); //银行账号
if (!isset($params['companyUrl'])) {
    $companyUrl = '';
} else {
    $companyUrl = Check::checkUrl($params['companyUrl']);  //公司网址
}

try{

    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $wxApp = new MiniProgram();
//    foreach ([$company,$companyEn,$companyProfile,$bankAccount] as $content){
//
//    }
    $checkMsg = $wxApp->msgSecCheck([$company,$companyEn,$companyProfile,$bankAccount]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }

    $userId = $GLOBALS['userId'];
    $infoCard = new InfoCard();
    $infoCard->id = $infoCardId;
    $infoCard->company = $company;
    $infoCard->companyEn = $companyEn;
    $infoCard->companyProfile = $companyProfile;
    $infoCard->companyUrl = $companyUrl;
    $infoCard->bankAccount = $bankAccount;
    $infoCard->createBy = $userId;
    $infoCard->updateBy = $userId;

    $infoCardDB = new CDBInfoCard();
    $companyId = $infoCardDB->addCompany($infoCard);

    $data['companyId'] = $companyId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);

}catch (PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
