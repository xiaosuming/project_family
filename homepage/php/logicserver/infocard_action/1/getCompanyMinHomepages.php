<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-31
 * Time: 下午4:39
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');  //名片id

try {
    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $infoCardDB = new CDBInfoCard();

    $pageList = $infoCardDB->getCompanyMinHomepagesByInfoCardId($infoCardId);

    $newPageList = array();
    $pageMap = array();
    foreach($pageList as $page) {
        $pageMap[$page['id']] = $page;
    }

    $infoCardModel = $infoCardDB->getInfoCard($infoCardId);
    $order = $infoCardModel->imageOrder;

    foreach($order as $pageId) {
        if (isset($pageMap[$pageId])) {
            $newPageList[] = $pageMap[$pageId];
        }
    }

    $result['list'] = $newPageList;


    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}