<?php
/**
 * 批量添加名片到标签中
 * @author: jiangpengfei
 * @date:   2019-03-13
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;
use DB\CDBAccount;

$tagId = Check::checkInteger($params['tagId'] ?? '');
$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    
    $tag = $infoCardDB->getTagById($tagId);

    if ($tag == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $accountDB = new CDBAccount();
    if (!$accountDB->checkUserPermission($tag['userId'], $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $deleteRow = $infoCardDB->removeInfoCardFromTag($infoCardId, $tagId, $userId);

    $data['delete'] = $deleteRow;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
