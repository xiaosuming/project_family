<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/6 0006
 * Time: 12:47
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBPushInfoCard;

$userId = $GLOBALS['userId'];
$cardId = Check::checkInteger(trim(isset($params['cardId']) ? $params['cardId'] : ''));        //用户名片id

try {

    $infoCardDB = new CDBInfoCard();
    $deleteRow = $infoCardDB->deleteReceiveInfoCard($userId,$cardId);
    if ($deleteRow>0){

        $infocard = $infoCardDB->getInfoCard($cardId);

        if ($infocard != null) {
            // 推送删除任务
            $pushInfoCardDB = new CDBPushInfoCard();
            $modelId =  $infocard->userId.'_'.$cardId;
            $action = 2;
            $source = [
                "script"=>[
                    "source"=>"ctx._source.access.remove(ctx._source.access.indexOf(params.access))",
                    "params"=>["access"=>$userId]
                ]
            ];
            $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);
        }

        $data['deleteRow'] = $deleteRow;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }else{
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], '删除出错');
        exit;
    }

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}