<?php
/**
 * 设置聊天权限
 * @author: jiangpengfei
 * @date:   2019-03-18
 */

use Util\Util;
use Util\Check;
use DB\CDBInfoCard;
use DB\CDBAccount;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$chatPermission = Check::checkInteger($params['chatPermission'] ?? ''); // 1公开，2付费，3禁止
$chatCost = Check::checkInteger($params['chatCost'] ?? 0);
$userId = $GLOBALS['userId'];

if ($chatPermission == 1 || $chatPermission == 3) {
    $chatCost = 0;
} else if ($chatPermission == 2) {
    if ($chatCost <= 0) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '费用需要大于0');
        exit;
    }
} else {
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '私信权限值不正确');
    exit;
}

try {
    $infoCardDB = new CDBInfoCard();
    
    // 检查用户对名片的权限
    $infoCard = $infoCardDB->getInfoCard($infoCardId);
    $accountDB = new CDBAccount();
    if ($infoCard == null || !$accountDB->checkUserPermission($infoCard->userId, $userId) ) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $update = $infoCardDB->setChatPermission($infoCardId, $chatPermission, $chatCost);

    $data['update'] = $update;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}