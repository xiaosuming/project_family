<?php
/**
 * 根据批量分享码获取名片
 * @author: jiangpengfei
 * @date:  2019-03-13
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;

$shareCode = Check::check($params['shareCode'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    $infoCards = $infoCardDB->getInfoCardsByBatchShareCode($shareCode);

    $data['infoCards'] = $infoCards;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $infoCards);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}