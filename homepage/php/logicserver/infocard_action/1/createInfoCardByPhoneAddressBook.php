<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-3
 * Time: 下午5:27
 */

use DB\CDBInfoCard;
use DB\CDBPushInfoCard;
use Model\InfoCard;
use Util\Check;
use Util\PhotoGenerator;
use Util\Util;
use ThirdParty\MiniProgram;

//var_dump(json_encode($a,JSON_UNESCAPED_UNICODE));
//phone格式 [{"phone":"11111111111","name":"aaa","company":"1","address":"1"},{"phone":"22222222222","name":"aaa","company":"1","address":"1"}]
$jsonStr = isset($params['jsonStr']) ? $params['jsonStr'] : '';

if (get_magic_quotes_gpc() == 1) {
    $jsonStr = stripslashes($jsonStr);
}

if ($jsonStr == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}

$userId = $GLOBALS['userId'];
$arr = json_decode($jsonStr, true);

try {

    $infoCardDB = new CDBInfoCard();
    $result = $infoCardDB->getPhoneAndNameFromAddressBook($userId);

    $res = array();
    foreach ($result as $item) {
        $p = json_decode($item['phone']);
        $phone = Check::check(trim(isset($p[0]) ? $p[0] : ''));
        $tmpArr[$phone] = $item;
    }
    foreach ($arr as $v) {
        $phone = Check::check(trim(isset($v['phone']) ? $v['phone'] : ''));
        if (!isset($tmpArr[$phone])) {
            $res[] = $v;
        }
    }

    $infoCard = new InfoCard();
    $infoCard->userId = $userId;
    $infoCard->createBy = $userId;
    $infoCard->updateBy = $userId;
    $infoCard->photo = PhotoGenerator::generateUserPhotoFromResource();
    $infoCard->skills = json_encode(array());
    $infoCard->favorites = json_encode(array());
    $infoCard->jobs = json_encode(array());

    $array = array();
    foreach ($res as $v) {
        //检查identity是否存在
        $identityExists = true;
        while ($identityExists) {
            $identity = md5($userId . time() . Util::generateRandomCode(8));
            if ($infoCardDB->verifyIdentity($identity)) {
                $identityExists = true;
            } else {
                $identityExists = false;
            }
        }

        $infoCard->identity = $identity;
        $infoCard->phone = json_encode(array($v['phone'])) ?? json_encode(array());
        $infoCard->name = Check::check($v['name'] ?? '');
        $infoCard->remark = Check::check($v['name'] ?? '');
        $infoCard->company = Check::check($v['company'] ?? '');
        $infoCard->address = Check::check($v['address'] ?? '');

        $wxApp = new MiniProgram();
//        foreach ([$infoCard->phone, $infoCard->name, $infoCard->remark,$infoCard->company,$infoCard->address] as $content){
//
//        }
        $checkMsg = $wxApp->msgSecCheck((array)$infoCard);
        if (!$checkMsg){
            Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
            exit();
        }

        $infoCardId = $infoCardDB->addInfoCardByAddressList($infoCard);
        if ($infoCardId > 0) {
            //search 添加进搜索任务
            if ($GLOBALS['TEST_INFO_CARD_TASK']) {
                $jobStr = Util::getJobStrFormJobs($infoCard->jobs);

                $CDBPushInfoCard = new CDBPushInfoCard();
                $modelId = $userId . '_' . $infoCardId;
                $action = 1;
                $access = [$userId];
                $source = [
                    'id' => intval($infoCardId),
                    'remark' => $infoCard->remark,
                    'userId' => intval($userId),
                    'familyId' => 0,
                    'personId' => 0,
                    'name' => $infoCard->name,
                    'nameEn' => '',
                    'address' => $infoCard->address,
                    'photo' => $infoCard->photo,
                    'backgroundImg' => '',
                    'backgroundColor' => '#ccccdd',
                    'backgroundType' => 0,
                    'company' => $infoCard->company,
                    'companyEn' => '',
                    'phone' => Util::arrayToString($infoCard->phone),
                    'jobs' => $jobStr,
                    'type' => 1,
                    'email' => '',
                    'companyProfile' => '',
                    'access' => $access
                ];
                $CDBPushInfoCard->setPushInfoCardTask($modelId, $action, $source);
            }
        }
        array_push($array, $infoCardId);
    }
    $countInsertRow = count($array);
    $data['countInsertRow'] = $countInsertRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
