<?php
use DB\CDBOCRTask;
use Util\Util;
use Util\Check;
/**
 * 添加OCR任务。每一个任务都会有一个编号。添加任务时会获取编号。用户之后根据编号取回识别结果
 * @author jiangpengfei
 * @date   2018-06-13
 */
$photo = Check::checkUrlWithLimit($params['photo'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $ocrDB = new CDBOCRTask();
    $taskId = $ocrDB->addOCRTask($userId, $photo);

    if ($taskId > 0) {
        $data['taskId'] = $taskId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错");
    }
}catch(PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
