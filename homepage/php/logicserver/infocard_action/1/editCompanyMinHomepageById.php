<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-31
 * Time: 下午4:27
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use ThirdParty\MiniProgram;

$minHomepageId = Check::checkInteger($params['minHomepageId'] ?? ''); //微主页的id
$userId = $GLOBALS['userId'];

try {
    if ($minHomepageId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $infoCardDb = new CDBInfoCard();
    $result = $infoCardDb->getCompanyMinHomepageById($minHomepageId);
    if ($userId != $result['createBy'] || $result == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }
    $arr = ['imgUrl', 'imgProfile'];
    foreach ($arr as $v) {
        if (isset($params[$v])) {
            switch ($v) {
                case 'imgUrl':
                    $imgUrl = $imgUrl = Check::check($params['imgUrl'] ?? '');  //图片地址
                    break;
                case 'imgProfile':
                    $imgProfile = Check::check($params['imgProfile'] ?? '', 0, 300); //图片简介
                    break;
            }

        } else {
            switch ($v) {
                case 'imgUrl':
                    $imgUrl = Check::check($result['imgUrl']);
                    break;
                case 'imgProfile':
                    $imgProfile = Check::check($result['imgProfile'], 0, 300);
                    break;
            }
        }
    }

    $wxApp = new MiniProgram();
    $checkMsg = $wxApp->msgSecCheck([$imgProfile]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }

    $updateRow = $infoCardDb->editCompanyMinHomepageById($minHomepageId, $imgUrl, $imgProfile, $userId);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
