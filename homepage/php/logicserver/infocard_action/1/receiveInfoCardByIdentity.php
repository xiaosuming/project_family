<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2018/3/5 0005
 * Time: 17:25
 */

use DB\CDBInfoCard;
use Util\Util;
use Model\InfoCard;
use DB\CDBPushInfoCard;
use Model\InfoCardShareRecord;

$identity = $params['identity'] ?? '';
$userId = $GLOBALS['userId'];

if ($identity == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}
try {

    $infoCardDB = new CDBInfoCard();
    $result = $infoCardDB->getInfoCardByIdentity($identity);
    if ($result == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "查询出错");
        exit;
    }

    $cardId = $result['id'];
    $count = $infoCardDB->existsCardIdOnReceive($cardId, $userId);

    if ($count > 0) {
        $data['cardId'] = $cardId;
        $data['msg']='成功接收到名片';
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }

    $receiveId = $infoCardDB->receiveInfoCardById($cardId, $userId);
    if ($receiveId > 0) {

        // 转发成功，转发量+1
        $infoCardDB->increShare($cardId);

        // 将此次转发记录存储
        $record = new InfoCardShareRecord();
        $record->shareTime = date('Y-m-d H:i:s',$time);
        $record->shareUserId = $result['userId'];
        $record->shareCardId = $cardId;
        $record->ownerUserId = $result['userId'];
        $record->targetUserId = $userId;
        $infoCardDB->addShareRecord($record);

        // 接收名片成功,将该名片也作为索引任务推送到队列中
        $pushInfoCardDB = new CDBPushInfoCard();
        $modelId =  $result['userId'].'_'.$cardId;
        $action = 2;    // 更新操作
        $source = [
            "script" => [
              "source" => "ctx._source.access.add(params.access)",
              "params" => ["access" => $userId]
            ]
        ];
        $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);

        $data['cardId'] = $cardId;
        $data['msg']='成功接收到名片';
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "未成功接收到名片");
        exit;
    }


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}