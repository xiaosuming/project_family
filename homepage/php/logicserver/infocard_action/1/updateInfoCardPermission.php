<?php
/**
 * 更新名片能否被转发的权限，
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-5
 * Time: 下午2:33
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\SysLogger;
use Util\Util;
use DB\CDBAccount;

$permission = Check::checkInteger($params['permission'] ?? 0); // 0 私密 不允许转发 1 公开 允许转发
$infoCardId = Check::checkInteger($params['infoCardId'] ?? ''); // 名片id

try {
    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];

    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();
    //验证用户对名片的权限
    if ($infoCard == null || !$accountDB->checkUserPermission($infoCard->userId, $userId )) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $updateRow = $infoCardDB->updateInfoCardPermission($permission, $infoCardId);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
