<?php
/**
 * 根据code值检查用户是否参加过交换
 * @Author: jiangpengfei
 * @Date:   2019-03-01
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;

$code = Check::check($params['code'] ?? '', 1);
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();

    $data['join'] = $infoCardDB->checkUserExistInExchangeByCode($code, $userId);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}  catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}