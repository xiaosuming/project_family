<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/6 0006
 * Time: 12:29
 */

use DB\CDBInfoCard;
use Util\Util;

try{

    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];

    $infoCards = $infoCardDB->getAllInfoCardFromAddressList($userId);

    foreach ($infoCards as $key=>$v){
        $infoCards[$key]['phone'] = json_decode($v['phone'],true);
    }
    $data['infoCards'] = $infoCards;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}