<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-5
 * Time: 下午3:50
 */

use DB\CDBFamily;
use DB\CDBInfoCard;
use Util\Check;
use Util\SysLogger;
use Util\Util;

// 名片公开范围  0私密 1公开 面向所有用户 2公开 面向所有家族 3 公开 面向部分家族
$familyId = Check::checkInteger($params['familyId'] ?? '');
$izqUserId = Check::checkInteger($params['izqUserId'] ?? '');

$userId = $GLOBALS['userId'];

try {

    $familyDB = new CDBFamily();

    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $infoCardDB = new CDBInfoCard();
    $infoCard = $infoCardDB->getInfoCardIdByFamilyIdAndUserId($familyId, $izqUserId);

    if ($infoCard['infoCardId'] == null) {
        $infoCardDetail = $infoCardDB->getUserDefaultInfoCard($izqUserId);

        //var_dump($infoCardDetail);
        $openScope = $infoCardDetail->openScope;
        if ($openScope == 0) {
            $infoCardDetail = array();
        }
        if ($openScope == 3) {
            if (!$infoCardDB->verifyFamilyIdAndUserIdPermission($familyId, $infoCardDetail->id)) {
                $infoCardDetail = array();
            }
        }
    } else {
        $infoCardDetail = $infoCardDB->getInfoCard($infoCard['infoCardId']);
    }

    $data['infoCard'] = $infoCardDetail;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
