<?php
/**
 * 发送交换名片的请求
 * 权限如下：
 * 请求的用户有被请求用户的名片
 * 流程如下：
 * 1.检查请求用户是否有该收到的名片，以及要发送的名片id
 * 2.得到收到的名片的用户
 * 3.向收到的名片的用户发送交换请求，请求中附带要交换的名片id
 * author: jiangpengfei
 * date: 2018-07-20
 */
use DB\CDBMessage;
use DB\CDBInfoCard;
use Model\Message;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');               // 用户收到的名片id
$sendInfoCardId = Check::checkInteger($params['sendInfoCardId'] ?? '');       // 用户要发送的名片id
$userId = $GLOBALS['userId'];

// 检查收到的名片
try{
    $infoCardDB = new CDBInfoCard();

    if (!$infoCardDB->verifyUserIdInReceive($infoCardId, $userId) ) {
        // 不是导入的名片
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足，用户没有收到该名片");
        exit;
    }

    $receiveInfoCard = $infoCardDB->getInfoCard($infoCardId);   // 收到的名片

    $userInfoCard = $infoCardDB->getInfoCard($sendInfoCardId);  // 要发送的名片

    $accountDB = new CDBAccount();
    if ($userInfoCard == null || !$accountDB->checkUserPermission($userInfoCard->userId, $userId)) {
        // 不是用户自己的名片
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足，不是用户的名片");
        exit;
    }

    // 权限校验结束，发送交换信息
    $messageDB = new CDBMessage();

    $message = new Message();
    $message->fromUser = $userId;
    $message->toUser = $receiveInfoCard->userId;
    $message->content = "收到一张来自".$userInfoCard->name."的名片";
    $message->module = $GLOBALS['INFO_CARD_MODULE'];
    $message->action = $GLOBALS['EXCHANGE_INFOCARD'];
    $message->recordId = $sendInfoCardId;
    $message->extraRecordId = $infoCardId;

    $messageId = $messageDB->addMessage($message);

    $data['messageId'] = $messageId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}