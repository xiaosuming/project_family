<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-22
 * Time: 上午10:41
 */

use DB\CDBInfoCard;
use Util\Util;

$count = $params['count'] ?? 1;

try {

    $infoCardDB = new CDBInfoCard();
    $infoCard = $infoCardDB->getRandomInfoCardNoLogin($count);

    $data['infoCard'] = $infoCard;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
