<?php
/**
 * 根据标签id分页获取名片
 * @author: jiangpengfei
 * @date:   2019-03-13
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;
use Util\Paginator;
use DB\CDBAccount;

$tagId = Check::checkInteger($params['tagId'] ?? '');
$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$count = Check::checkInteger($params['count'] ?? 10);

$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    $tag = $infoCardDB->getTagById($tagId);

    $accountDB = new CDBAccount();
    if ($tag == null || !$accountDB->checkUserPermission($tag['userId'], $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $paging = $infoCardDB->getInfoCardsPagingByTagId($tagId, $maxId, $sinceId, $count);
    $total = $tag['total'];

    $maxId = 0;
    $sinceId = 0;

    if (count($paging) > 0) {
        $maxId = $paging[count($paging) - 1]['tagRecordId'];
        $sinceId = $paging[0]['tagRecordId'];
    }

    $isLast = count($paging) == $count ? false : true;

    $paginator = new Paginator($total, $paging, $maxId, $sinceId, $isLast);
    $paginator->printPage();

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
