<?php
/**
 * 删除群体名片交换
 * @Author: jiangpengfei
 * @Date:   2019-03-05
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;
use DB\CDBAccount;

$exchangeId = Check::checkInteger($params['exchangeId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    $exchange = $infoCardDB->getInfocardExchange($exchangeId);

    if ($exchange == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '资源不存在');
        exit;
    }

    $accountDB = new CDBAccount();
    if (!$accountDB->checkUserPermission($exchange['userId'], $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $delete = $infoCardDB->deleteGroupExchange($exchangeId, $userId);

    $data['delete'] = $delete;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}