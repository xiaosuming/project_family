<?php

use Util\Util;
use DB\CDBInfoCard;
use Util\Check;
use DB\CDBAccount;

$infoCardId = Check::checkInteger(trim(isset($params['infoCardId']) ? $params['infoCardId'] : ''));        //用户名片id

try {
    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];
    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    if ($infoCard == null) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $accountDB = new CDBAccount();

    //验证用户对名片的权限
    if ($accountDB->checkUserPermission($infoCard->userId, $userId) || $infoCardDB->verifyUserIdInReceive($infoCardId,$userId)){

        $statistic = $infoCardDB->getInfoCardStatistic($infoCardId);
        if ($statistic) {   // redis中如果存在该名片的统计信息
            $infoCard->viewNum = $statistic['v'] ?? 0;
            $infoCard->shareNum = $statistic['s'] ?? 0;
        } else {
            $infoCard->viewNum = 0;
            $infoCard->shareNum = 0;
        }

        $data['infoCard'] = $infoCard;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }else{
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}