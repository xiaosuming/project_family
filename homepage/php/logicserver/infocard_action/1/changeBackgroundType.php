<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-10
 * Time: 下午5:14
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$backgroundImg =  Check::check($params['backgroundImg'] ?? '');
$backgroundColor = Check::checkColor($params['backgroundColor'] ?? '#ccccdd');
$backgroundType = Check::checkInteger($params['backgroundType'] ?? 0);
$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');

try {
    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];

    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();
    //验证用户对名片的权限
    if ($infoCard == null || !$accountDB->checkUserPermission($infoCard->userId, $userId)) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $updateRow = $infoCardDB->changeBackgroundType($backgroundImg,$backgroundColor,$backgroundType,$infoCardId);

    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
