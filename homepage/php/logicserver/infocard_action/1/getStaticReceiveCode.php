<?php
/**
 * 获取用户的静态接收码
 * @author jiangpengfei
 * @date 2018-05-21
 */
use DB\CDBAccount;
use Util\Util;

$userId = $GLOBALS['userId'];
$iv = $GLOBALS['INFOCARD_CODE_IV'];       // 加密算法中要用到的iv

try {

    $accountDB = new CDBAccount();
    $pass = md5($accountDB->getPassword($userId));

    $srcText['uid'] = $userId; // 用户id
    $srcText['static'] = true; // 时间戳

    $method = 'aes128';
    $secretData = openssl_encrypt(json_encode($srcText), $method, $pass, true, $iv);

    $data['code'] = base64_encode($secretData);
    $data['userId'] = $userId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}