<?php
/**
 * 添加名片标签
 * @author: jiangpengfei
 * @date:   2019-03-13
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;
use ThirdParty\MiniProgram;

$tag = Check::check($params['tag'] ?? '', 1, 18);
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();

    $wxApp = new MiniProgram();
    foreach ([$tag] as $content){
        $checkMsg = $wxApp->msgSecCheck($content);
        if (!$checkMsg){
            Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
            exit();
        }
    }

    $tagId = $infoCardDB->addInfoCardTag($tag, $userId);

    $data['tagId'] = $tagId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
