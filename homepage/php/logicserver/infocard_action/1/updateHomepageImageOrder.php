<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-7-25
 * Time: 上午11:37
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$imageOrder = Check::checkIdArray($params['imageOrder'] ?? '');

try {

    if ($infoCardId == '' || $imageOrder == '' || empty($imageOrder)) {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $infoCardDB = new CDBInfoCard();
    $infoCardModel = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();
    if ($infoCardModel == null || !$accountDB->checkUserPermission($infoCardModel->userId, $userId)) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $imageOrderArr = explode(',', $imageOrder);
    $imageOrderArr = json_encode($imageOrderArr);

    $updateRow = $infoCardDB->updateHomepageImageOrder($infoCardId, $imageOrderArr);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
