<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/6 0006
 * Time: 13:42
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;
$show = $params['isShow'] ?? 1; //1 展示 0 不展示
$infoCardId = Check::checkInteger(trim(isset($params['infoCardId']) ? $params['infoCardId'] : ''));        //用户名片id
try {
    if ($infoCardId == '' || $show == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];

    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();
    //验证用户对名片的权限
    if ($infoCard == null || !$accountDB->checkUserPermission($infoCard->userId, $userId) || $infoCard->type != 0) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $updateRow = $infoCardDB->updateIsShowForMyInfoCard($show, $infoCardId);

    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}