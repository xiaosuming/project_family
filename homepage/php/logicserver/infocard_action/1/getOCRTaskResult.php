<?php
use DB\CDBOCRTask;
use Util\Util;
use Util\Check;
use Util\TextClassification;
use DB\CDBAccount;

/**
 * 根据ocrId获取ocr的结果
 * @author jiangpengfei
 * @date   2018-06-13
 */
$ocrId = Check::checkInteger($params['ocrId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $ocrDB = new CDBOCRTask();
    $ocrResult = $ocrDB->getOCRResult($ocrId);

    if ($ocrResult == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "任务不存在");
        exit;
    } else {

        $accountDB = new CDBAccount();
        if (!$accountDB->checkUserPermission($ocrResult['userId'], $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit;
        }
    }

    if ($ocrResult['finish'] == 0) {
        // 没有完成
        $data['finish'] = false;
    } else {
        $data['finish'] = true;

        // 调用ocr识别
        $textC = new TextClassification($ocrResult['result'], $ocrResult['type']);
        $textC->analysis();
    
        $data['result']['name'] = $textC->name;
        $data['result']['phone'] = $textC->phone;
        $data['result']['company'] = $textC->company;
        $data['result']['companyUrl'] = $textC->companyUrl;
        $data['result']['position'] = $textC->position;
        $data['result']['address'] = $textC->address;
        $data['result']['email'] = $textC->email;
    }


    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
