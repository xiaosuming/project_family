<?php
/**
 * 参加名片交换
 * @Author: jiangpengfei
 * @Date: 2018-10-02
 */

use DB\CDBInfoCard;
use Model\InfoCardExchange;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$code = Check::check($params['code'] ?? '');         // 交换时的校验码
$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$password = Check::check($params['password'] ?? '');
$userId = $GLOBALS['userId'];

try {
    // 检查名片
    $infoCardDB = new CDBInfoCard();
    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();
    if ($infoCard == null || !$accountDB->checkUserPermission($infoCard->userId, $userId)) {
        // 权限不足
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足1");
        exit;
    }

    // 获取交换
    $infoCardExchange = $infoCardDB->getInfoCardExchangeByCode($code);
    if ($infoCardExchange == null) {
        // 权限不足
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足2");
        exit;
    }

    // 检查密码
    if ($infoCardExchange['password'] != '' && $infoCardExchange['password'] != hash('sha256', $password)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "密码错误");
        exit;
    }

    // 检查是否已经结束
    if ($infoCardExchange['isFinish'] == 1) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "交换已经结束");
        exit;
    }

    // 检查用户是否参加过
    if ($infoCardDB->checkUserExistInExchange($infoCardExchange['id'], $userId)) {
        $data['recordId'] = 0;
        $data['exchangeId'] = $infoCardExchange['id'];

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }

    $recordId = $infoCardDB->joinInfoCardExchange($infoCardExchange['id'], $userId, $infoCardId);

    $data['recordId'] = $recordId;
    $data['exchangeId'] = $infoCardExchange['id'];

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}