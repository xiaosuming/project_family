<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-14
 * Time: 下午1:27
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;

$pageIndex = Check::checkInteger($params['pageIndex'] ?? 1); // 当前页
$pageSize = Check::checkInteger($params['pageSize'] ?? 6); // 每页显示数量

try {
    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];
    $infoCards = $infoCardDB->getAllTypeInfoCardsPageByImport($pageIndex, $pageSize, $userId);
    $data['infoCardPage'] = $infoCards;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}