<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-6
 * Time: 上午10:36
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\SysLogger;
use Util\Util;
use DB\CDBAccount;

$familyIds = Check::check($params['familyIds'] ?? '');  // 3 传的家族 字符串形式 逗号分割
$openScope = Check::checkInteger($params['openScope'] ?? ''); // 名片公开范围  0私密 1公开 面向所有用户 2公开 面向所有家族 3 公开 面向部分家族
$infoCardId = Check::checkInteger($params['infoCardId'] ?? ''); // 名片id
$userId = $GLOBALS['userId'];

if ($infoCardId == '' || $openScope == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {

    $infoCardDB = new CDBInfoCard();

    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();
    //验证用户对名片的权限
    if ($infoCard == null || !$accountDB->checkUserPermission($infoCard->userId, $userId)) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $familyIdArr = array();

    if ($openScope == 3 && $familyIds != '') {
        $familyIdArr = explode(',', $familyIds);
    }

    $updateRow = $infoCardDB->updateInfoCardOpenScope($openScope, $infoCardId, $userId, $familyIdArr);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
