<?php

use DB\CDBArea;
use DB\CDBInfoCard;
use DB\CDBPushInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;
use ThirdParty\MiniProgram;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $arr = ['remark', 'name', 'country', 'province', 'city', 'area', 'town', 'address', 'gender', 'birthday', 'bloodType',
        'maritalStatus', 'phone', 'qq', 'skills', 'favorites', 'jobs', 'photo', 'backgroundImg', 'backgroundColor', 'backgroundType', 'company',
        'companyEn', 'nameEn', 'companyProfile', 'companyUrl', 'email', 'bankAccount', 'longitude', 'latitude', 'position', 'permission','taxNumber'];

    $infoCardDB = new CDBInfoCard();
    $infoCardModel = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();
    if ($infoCardModel == null || !$accountDB->checkUserPermission($infoCardModel->userId, $userId)) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    foreach ($arr as $v) {
        if (!isset($params[$v])) {

            if ($v == 'phone') {
                $infoCardModel->phone = json_encode($infoCardModel->phone);
            }
            if ($v == 'skills') {
                $infoCardModel->skills = json_encode($infoCardModel->skills, JSON_UNESCAPED_UNICODE);
            }
            if ($v == 'favorites') {
                $infoCardModel->favorites = json_encode($infoCardModel->favorites, JSON_UNESCAPED_UNICODE);
            }
            if ($v == 'jobs') {
                $infoCardModel->jobs = json_encode($infoCardModel->jobs, JSON_UNESCAPED_UNICODE);
            }

        } else {

            switch ($v) {
                case 'remark':
                    $infoCardModel->remark = Check::check($params['remark'], 1, 45);
                    break;
                case 'name':
                    $infoCardModel->name = Check::check($params['name'], 0, 45);
                    break;
                case 'country':
                    if ($params['country'] == '') {
                        $infoCardModel->country = 0;
                    } else {
                        $infoCardModel->country = Check::checkInteger($params['country']);
                    }
                    break;
                case 'province':
                    if ($params['province'] == '') {
                        $infoCardModel->province = 0;
                    } else {
                        $infoCardModel->province = Check::checkInteger($params['province']);
                    }
                    break;
                case 'city':
                    if ($params['city'] == '') {
                        $infoCardModel->city = 0;
                    } else {
                        $infoCardModel->city = Check::checkInteger($params['city']);
                    }
                    break;
                case 'area':
                    if ($params['area'] == '') {
                        $infoCardModel->area = 0;
                    } else {
                        $infoCardModel->area = Check::checkInteger($params['area']);
                    }
                    break;
                case 'town':
                    if ($params['town'] == '') {
                        $infoCardModel->town = 0;
                    } else {
                        $infoCardModel->town = Check::checkInteger($params['town']);
                    }
                    break;
                case 'address':
                    $infoCardModel->address = Check::check($params['address']);
                    break;
                case 'gender':
                    if ($params['gender'] == '') {
                        $infoCardModel->gender = 2;
                    } else {
                        $infoCardModel->gender = Check::checkInteger($params['gender']);
                    }
                    break;
                case 'birthday':
                    if ($params['birthday'] == '') {
                        $infoCardModel->birthday = null;
                    } else {
                        $infoCardModel->birthday = Check::checkDate($params['birthday']);
                    }
                    break;
                case 'bloodType':
                    if ($params['bloodType'] == '') {
                        $infoCardModel->bloodType = 0;
                    } else {
                        $infoCardModel->bloodType = Check::checkInteger($params['bloodType']);
                    }
                    break;
                case 'maritalStatus':
                    if ($params['maritalStatus'] == '') {
                        $infoCardModel->maritalStatus = 0;
                    } else {
                        $infoCardModel->maritalStatus = Check::checkInteger($params['maritalStatus']);
                    }
                    break;
                case 'qq':
                    if ($params['qq'] == '') {
                        $infoCardModel->qq = '';
                    } else {
                        $infoCardModel->qq = Check::checkInteger($params['qq']);
                    }
                    break;
                case 'photo':
                    $infoCardModel->photo = Check::check($params['photo']);
                    break;
                case 'backgroundImg':
                    $infoCardModel->backgroundImg = Check::check($params['backgroundImg']);
                    break;
                case 'backgroundColor':
                    if ($params['backgroundColor'] == '') {
                        $infoCardModel->backgroundColor = '#ccccdd';
                    } else {
                        $infoCardModel->backgroundColor = Check::checkColor($params['backgroundColor']);
                    }
                    break;
                case 'backgroundType':
                    if ($params['backgroundType'] == '') {
                        $infoCardModel->backgroundType = 0;
                    } else {
                        $infoCardModel->backgroundType = Check::checkInteger($params['backgroundType']);
                    }
                    break;
                case 'company':
                    $infoCardModel->company = Check::check($params['company']);
                    break;
                case 'companyEn':
                    $infoCardModel->companyEn = Check::check($params['companyEn']);
                    break;
                case 'nameEn':
                    $infoCardModel->nameEn = Check::check($params['nameEn']);
                    break;
                case 'companyProfile':
                    $infoCardModel->companyProfile = Check::check($params['companyProfile'], 0, 500);
                    break;
                case 'companyUrl':
                    if ($params['companyUrl'] == '') {
                        $infoCardModel->companyUrl = '';
                    } else {
                        $infoCardModel->companyUrl = Check::checkUrl($params['companyUrl']);
                    }
                    break;
                case 'email':
                    if ($params['email'] == '') {
                        $infoCardModel->email = '';
                    } else {
                        $infoCardModel->email = Check::checkEmail($params['email']);
                    }
                    break;
                case 'bankAccount':
                    $infoCardModel->bankAccount = Check::check($params['bankAccount']);
                    break;
                case 'longitude':
                    if ($params['longitude'] == '') {
                        $infoCardModel->longitude = '';
                    } else {
                        $infoCardModel->longitude = Check::checkFloat($params['longitude'], -180, 180);
                    }
                    break;
                case 'latitude':
                    if ($params['latitude'] == '') {
                        $infoCardModel->latitude = '';
                    } else {
                        $infoCardModel->latitude = Check::checkFloat($params['latitude'], -90, 90);
                    }
                    break;
                case 'position':
                    if ($params['position'] == '') {
                        $infoCardModel->position = '';
                    } else {
                        $infoCardModel->position = Check::check($params['position']);
                    }
                    break;
                case 'permission':
                    if ($params['permission'] == '') {
                        $infoCardModel->permission = 1;
                    } else {
                        $infoCardModel->permission = Check::checkInteger($params['permission']);
                    }
                    break;
                case 'taxNumber':
                    if ($params['taxNumber'] == ''){
                        $infoCardModel->taxNumber = '';
                    }else{
                        $infoCardModel->taxNumber = Check::check($params['taxNumber']);
                    }
                    break;
                case 'phone':
                    if ($params['phone'] != '') {
                        //将手机号解析成数组
                        $phoneArray = explode(',', $params['phone']);
                        if (count($phoneArray) > 5) {
                            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '手机号最多为5个');
                            exit;
                        }
                        if (strlen(json_encode($phoneArray)) > 100) {
                            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], 'phone字符串长度不能大于100字节');
                            exit;
                        }
                        $phoneArray1 = array_filter($phoneArray); //传空字符串就去掉
                        foreach ($phoneArray1 as $vv) {
                            Check::checkNumber($vv);
                        }
                        $phoneArray1 = array_values($phoneArray1);
                        $infoCardModel->phone = json_encode($phoneArray1);
                    } else {
                        $infoCardModel->phone = json_encode(array());
                    }
                    break;
                case 'skills':
                    if ($params['skills'] != '') {
                        $skillArray = explode(",", $params['skills']);
                        $skillArray1 = array_filter($skillArray); //传空字符串就去掉
                        $skillArray1 = array_values($skillArray1);
                        $infoCardModel->skills = json_encode($skillArray1, JSON_UNESCAPED_UNICODE);
                    } else {
                        $infoCardModel->skills = json_encode(array());
                    }
                    break;
                case 'favorites':
                    if ($params['favorites'] != '') {
                        $favoriteArray = explode(",", $params['favorites']);
                        $favoriteArray1 = array_filter($favoriteArray);//传空字符串就去掉
                        $favoriteArray1 = array_values($favoriteArray1);
                        $infoCardModel->favorites = json_encode($favoriteArray1, JSON_UNESCAPED_UNICODE);
                    } else {
                        $infoCardModel->favorites = json_encode(array());
                    }
                    break;
                case 'jobs':
                    if ($params['jobs'] != '') {
                        $jobArray = explode(",", $params['jobs']);
                        $length = count($jobArray);
                        $a = array();
                        for ($i = 0; $i < $length; $i = $i + 3) {
                            $arr = array('startTime' => $jobArray[$i + 0], 'endTime' => $jobArray[$i + 1], 'job' => $jobArray[$i + 2]);
                            if ($arr != array('startTime' => '', 'endTime' => '', 'job' => '')) {
                                array_push($a, $arr);
                            }
                        }
                        $infoCardModel->jobs = json_encode($a, JSON_UNESCAPED_UNICODE);
                    } else {
                        $infoCardModel->jobs = json_encode(array());
                    }
                    break;
            }
        }
    }

    $CDBArea = new CDBArea();
    $result = $CDBArea->getAreaNameById($infoCardModel->country);
    $countryName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($infoCardModel->province);
    $provinceName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($infoCardModel->city);
    $cityName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($infoCardModel->area);
    $areaName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($infoCardModel->town);
    $townName = $result['areaName'] ?? '0';

    $infoCardModel->countryName = $countryName;
    $infoCardModel->provinceName = $provinceName;
    $infoCardModel->cityName = $cityName;
    $infoCardModel->areaName = $areaName;
    $infoCardModel->townName = $townName;
    $infoCardModel->createBy = $userId;
    $infoCardModel->updateBy = $userId;

    $wxApp = new MiniProgram();
//    foreach ($arr as $content){
//        $checkMsg = $wxApp->msgSecCheck($params[$content]);
//        if (!$checkMsg){
//            Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
//            exit();
//        }
//    }
    $checkMsg = $wxApp->msgSecCheck((array)$infoCardModel);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }


    $updateRow = $infoCardDB->editInfoCard($infoCardModel);
    if ($updateRow > 0) {
        //search 添加进搜索任务
        if ($GLOBALS['TEST_INFO_CARD_TASK']) {
            $CDBPushInfoCard = new CDBPushInfoCard();
            $modelId = $userId . '_' . $infoCardId;;
            $action = 2;
            $source = [
                'id' => intval($infoCardId),
                'remark' => $infoCardModel->remark,
                'userId' => intval($userId),
                'familyId' => intval($infoCardModel->familyId),
                'personId' => intval($infoCardModel->personId),
                'name' => $infoCardModel->name,
                'nameEn' => $infoCardModel->nameEn,
                'address' => $infoCardModel->address,
                'photo' => $infoCardModel->photo,
                'backgroundImg' => $infoCardModel->backgroundImg,
                'backgroundColor' => $infoCardModel->backgroundColor,
                'backgroundType' => $infoCardModel->backgroundType,
                'company' => $infoCardModel->company,
                'companyEn' => $infoCardModel->companyEn,
                'phone' => Util::arrayToString($infoCardModel->phone),
                'jobs' => $infoCardModel->position,
                'type' => 0,
                'email' => $infoCardModel->email,
                'companyProfile' => $infoCardModel->companyProfile
            ];
            $CDBPushInfoCard->setPushInfoCardTask($modelId, $action, $source);
        }
        $data['updateRow'] = $updateRow;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "编辑无变化");
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
