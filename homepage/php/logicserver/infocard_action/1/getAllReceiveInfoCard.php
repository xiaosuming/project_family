<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/6 0006
 * Time: 12:39
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Pager;
use Util\Util;
use DB\CDBAccount;

$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    $total = $infoCardDB->countReceiveInfoCard($userId);
    if ($total == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '没有接收到的名片');
        exit;
    }
    $receiveInfoCard = $infoCardDB->getAllReceiveInfoCard($userId);

    $accountDB = new CDBAccount();
    $userIdAlias = $accountDB->getUserAlias($userId);

    foreach ($receiveInfoCard as $key => $v) {
        //转发权限判断
        if (in_array($receiveInfoCard[$key]['userId'], $userIdAlias)) {
            unset($receiveInfoCard[$key]['identity']);
        }
        $receiveInfoCard[$key]['phone'] = json_decode($v['phone'], true);
        $jobs = json_decode($v['jobs'], true);
        if (empty($jobs)) {
            $receiveInfoCard[$key]['jobs'] = array();
        } else {
            $arr = array_pop($jobs);
            $receiveInfoCard[$key]['jobs'] = $arr;
        }
    }

    $data['infoCards'] = $receiveInfoCard;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}