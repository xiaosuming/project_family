<?php
/**
 * 分享多张名片
 * @author: jiangpengfei
 * @date:   2019-03-13
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;


$infoCardIds = Check::checkIdArray($params['infoCardIds'] ?? '');
$userId = $GLOBALS['userId'];
$shareName = Check::check($params['shareName'] ?? '', 1, 18);

try {
    $infoCardIds = explode(',', $infoCardIds);

    $infoCardDB = new CDBInfoCard();
    $shareCode = $infoCardDB->shareMultiInfoCards($shareName, $infoCardIds, $userId);

    $data['shareCode'] = $shareCode;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}