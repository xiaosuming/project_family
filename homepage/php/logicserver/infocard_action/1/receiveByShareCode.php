<?php
/**
 * 通过名片的分享码接收名片，也就是接收转发过来的名片
 * author: jiangpengfei
 * Date: 2018-06-07
 */

use DB\CDBInfoCard;
use Util\Util;
use Model\InfoCard;
use Model\InfoCardShareRecord;
use DB\CDBPushInfoCard;

$key = $GLOBALS['INFOCARD_SHARE_CODE_KEY'];     // 加密算法中要用到的key
$iv = $GLOBALS['INFOCARD_SHARE_CODE_IV'];       // 加密算法中要用到的iv
$shareCode = $params['shareCode'] ?? '';
$userId = $GLOBALS['userId'];

if ($shareCode == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {

    $method = 'aes128';
    $info = openssl_decrypt(base64_decode($shareCode), $method, $key, true, $iv);

    // $info中的字段有:identity,time,shareUserId
    $info = json_decode($info, true);

    if (!$info || !isset($info['identity']) || !isset($info['time']) || !isset($info['shareUserId']) ) {    // 数据格式不对
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "数据格式错误");
        exit;
    }

    $identity = $info['identity'];
    $time = $info['time'];

    $shareUserId = $info['shareUserId'];

    $infoCardDB = new CDBInfoCard();
    $result = $infoCardDB->getInfoCardByIdentity($identity);
    if ($result == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "查询出错");
        exit;
    }

    $ownerUserId = $result['userId'];
    $cardId = $result['id'];

    $count = $infoCardDB->existsCardIdOnReceive($cardId, $userId);

    if ($count > 0) {
        $data['cardId'] = $cardId;
        $data['msg']='成功接收到名片';
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }

    $receiveId = $infoCardDB->receiveInfoCardById($cardId, $userId);
    if ($receiveId > 0) {

        // 转发成功，转发量+1
        $infoCardDB->increShare($cardId);

        // 将此次转发记录存储
        $record = new InfoCardShareRecord();
        $record->shareTime = date('Y-m-d H:i:s',$time);
        $record->shareUserId = $shareUserId;
        $record->shareCardId = $cardId;
        $record->ownerUserId = $ownerUserId;
        $record->targetUserId = $userId;
        $infoCardDB->addShareRecord($record);

        // 接收名片成功,将该名片也作为索引任务推送到队列中
        $pushInfoCardDB = new CDBPushInfoCard();
        $modelId =  $ownerUserId.'_'.$cardId;
        $action = 2;    // 添加操作
        $source = [
            "script" => [
              "source" => "ctx._source.access.add(params.access)",
              "params" => ["access" => $userId]
            ]
        ];
        $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);

        $data['cardId'] = $cardId;
        $data['msg']='成功接收到名片';
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "未成功接收到名片");
        exit;
    }


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}