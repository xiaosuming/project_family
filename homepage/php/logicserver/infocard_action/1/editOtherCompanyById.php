<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-30
 * Time: 下午3:22
 */

use DB\CDBInfoCard;
use Model\InfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;
use ThirdParty\MiniProgram;

$companyId = Check::checkInteger($params['companyId'] ?? '');
$userId = $GLOBALS['userId'];
if ($companyId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $infoCardDB = new CDBInfoCard();
    $result = $infoCardDB->getCompanyByOtherCompanyId($companyId);


    $accountDB = new CDBAccount();
    if ($result == null || !$accountDB->checkUserPermission($result['createBy'], $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'],'权限错误');
        exit;
    }
    $arr = ['company','companyEn','companyProfile','companyUrl','bankAccount'];
    $infoCard = new InfoCard();
    $infoCard->company = $result['company'];
    $infoCard->companyEn = $result['companyEn'];
    $infoCard->companyProfile = $result['companyProfile'];
    $infoCard->companyUrl = $result['companyUrl'];
    $infoCard->bankAccount = $result['bankAccount'];
    $infoCard->updateBy = $userId;
    foreach ($arr as $v){
        if (isset($params[$v])){
            $infoCard->$v = $params[$v];
        }
    }

    $wxApp = new MiniProgram();
    $content = [];
    foreach ($arr as $i){
        $content[$i] = $params[$i];
    }
    $checkMsg = $wxApp->msgSecCheck($content);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }

    $updateRow = $infoCardDB->editOtherCompanyByCompanyId($infoCard,$companyId);
    $data['updaeRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
