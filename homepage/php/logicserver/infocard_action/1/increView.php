<?php
/**
 * 增加名片的浏览量
 * @author jiangpengfei
 * @date   2018-07-11
 * @TODO 这个接口没有做任何验证
 */
use DB\CDBInfoCard;
use Util\Check;
use Util\Util;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');

$infoCardDB = new CDBInfoCard();
$viewNum = $infoCardDB->increView($infoCardId);
$data['view'] = $viewNum;
Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);