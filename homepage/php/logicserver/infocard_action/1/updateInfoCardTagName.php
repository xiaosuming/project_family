<?php
/**
 * 更新名片标签
 * @author: jiangpengfei
 * @date:   2019-03-13
 */

use DB\CDBInfoCard;
use Util\Util;
use Util\Check;
use DB\CDBAccount;
use ThirdParty\MiniProgram;

$tagId = Check::checkInteger($params['tagId'] ?? '');
$tagName = Check::check($params['tag'] ?? '', 1, 18);
$userId = $GLOBALS['userId'];

try {
    $infoCardDB = new CDBInfoCard();
    $tag = $infoCardDB->getTagById($tagId);

    if ($tag == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $accountDB = new CDBAccount();
    if (!$accountDB->checkUserPermission($tag['userId'], $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $wxApp = new MiniProgram();
    foreach ([$tagName] as $content){
        $checkMsg = $wxApp->msgSecCheck($content);
        if (!$checkMsg){
            Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
            exit();
        }
    }

    $update = $infoCardDB->updateInfoCardTagName($tagId, $tagName, $userId);

    $data['update'] = $update;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
