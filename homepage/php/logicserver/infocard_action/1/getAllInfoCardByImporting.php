<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2018/3/12 0012
 * Time: 15:57
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Pager;
use Util\Util;

$userId = $GLOBALS['userId'];
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : 0));
try {

    $infoCardDB = new CDBInfoCard();
    $importInfoCard = $infoCardDB->getAllImportInfoCards($familyId, $userId);

    foreach($importInfoCard as $key=>$v){
        $importInfoCard[$key]['phone'] = json_decode($v['phone'],true);
        $importInfoCard[$key]['skills']=json_decode($v['skills'],true);
        $importInfoCard[$key]['favorites']=json_decode($v['favorites'],true);
        $importInfoCard[$key]['jobs']=json_decode($v['jobs'],true);
    }
    $data['infoCards'] = $importInfoCard;
    Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}