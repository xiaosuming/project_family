<?php
/**
 * 获取用户的默认名片
 * @author: jiangepengfei
 * @date:   2019-03-22
 */

use Util\Util;
use Util\Check;
use DB\CDBInfoCard;

$userId = $GLOBALS['userId'];

try {

    $infoCardDB = new CDBInfoCard();
    $infoCard = $infoCardDB->getUserDefaultInfoCardOrFirst($userId);

    $data['infoCard'] = $infoCard;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}