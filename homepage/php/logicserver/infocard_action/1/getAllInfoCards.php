<?php

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;

$type = Check::checkInteger($params['type'] ?? 0); //0用户自己创建的名片  4ocr名片
try {
    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];
    $infoCards = $infoCardDB->getAllInfoCards($userId,$type);
    $data['infoCards'] = $infoCards;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}