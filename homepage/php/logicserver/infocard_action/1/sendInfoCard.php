<?php
/**
 * 发名片的接口
 * @author jiangpengfei
 * @date 2018-05-14
 */

use Util\Util;
use Util\Check;
use DB\CDBAccount;
use DB\CDBInfoCard;
use Model\InfoCard;
use DB\CDBPushInfoCard;

$code = $params['code'] ?? '';
$sendUserId = Check::checkInteger($params['sendUserId'] ?? '');
$cardId = Check::checkInteger($params['cardId'] ?? '');
$userId = $GLOBALS['userId'];

if ($code == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {

    $infoCardDB = new CDBInfoCard();
    $infoCardModel = $infoCardDB->getInfoCard($cardId);

    $accountDB = new CDBAccount();
    if ($infoCardModel == null || !$accountDB->checkUserPermission($infoCardModel->userId, $userId)) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $accountDB = new CDBAccount();
    $pass = $accountDB->getPassword($sendUserId);

    if ($pass == null) {
        // 要发送的用户不存在
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "要发送的用户不存在");
        exit;
    }

    $method = 'aes128';
    $iv = $GLOBALS['INFOCARD_CODE_IV'];       // 定义在getUserReceiveCode中
    $info = openssl_decrypt(base64_decode($code), $method, md5($pass), true, $iv);

    $info = json_decode($info, true);

    if (!$info || !isset($info['uid']) || $info['uid'] != $sendUserId) {    // 数据格式不对
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "数据格式错误");
        exit;
    }

    // 检查是否是静态码
    if (!isset($info['static'])) {
        // 比较时间戳检验有效性
        $current = time();
        $timestamp = $info['time'];
        if ($current - $timestamp > 300) {  // 300秒过期
            Util::printResult($GLOBALS['ERROR_DATA_EXPIRE'], "信息已过期");
            exit;
        }
    }



    // 检查sendUserId中是否有这张名片
    $count = $infoCardDB->existsCardIdOnReceive($cardId, $sendUserId);

    if ($count > 0) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "名片已发送");
        exit;
    }

    // 向sendUserId中存入名片
    $receiveId = $infoCardDB->receiveInfoCardById($cardId, $sendUserId);
    if ($receiveId > 0) {
        // 发送名片成功,将该名片也作为索引任务推送到队列中
        $pushInfoCardDB = new CDBPushInfoCard();
        $modelId =  $userId.'_'.$cardId;
        $action = 2;    // 更新操作
        $source = [
            "script" => [
              "source" => "ctx._source.access.add(params.access)",
              "params" => ["access" => $sendUserId]
            ]
        ];
        $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);
        $data['msg']='发送名片成功';
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "发送名片失败");
        exit;
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}