<?php
/**
 * 检查其他用户是否有当前的名片
 * @Author: jiangpengfei
 * @Date:   2018-02-25
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;

$otherUserId = Check::checkInteger($params['otherUserId'] ?? '');
$userId = $GLOBALS['userId'];

try {

    $infoCardDB = new CDBInfoCard();

    $data['result'] = $infoCardDB->checkOtherHasUserInfoCard($otherUserId, $userId);
        
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

}  catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
