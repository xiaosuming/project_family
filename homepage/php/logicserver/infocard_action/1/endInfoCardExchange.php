<?php
/**
 * 结束名片交换
 * @Author: jiangpengfei
 * @Date: 2018-10-02
 */
use DB\CDBInfoCard;
use Model\InfoCardExchange;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$exchangeId = Check::checkInteger($params['exchangeId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    // 检查名片是不是用户自己的
    $infoCardDB = new CDBInfoCard();
    $infoCardExchange = $infoCardDB->getInfocardExchange($exchangeId);

    $accountDB = new CDBAccount();
    if ($infoCardExchange == null || !$accountDB->checkUserPermission($infoCardExchange['userId'], $userId)) {
        // 权限不足
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    // 检查是否已经结束
    if ($infoCardExchange['isFinish'] == 1) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "交换已经结束");
        exit;
    }

    if ($infoCardDB->endInfoCardExchange($exchangeId)) {
        $data['success'] = true;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '结束名片交换失败');
    }

    
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}