<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-31
 * Time: 下午3:23
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use ThirdParty\MiniProgram;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');  //名片id
$imgUrl = Check::check($params['imgUrl'] ?? '');  //图片地址
$imgProfile = Check::check($params['imgProfile'] ?? '', 0, 300); //图片简介
$userId = $GLOBALS['userId'];
try {
    $infoCardDb = new CDBInfoCard();
    $result = $infoCardDb->getInfoCard($infoCardId);
    $imageOrder = $result->imageOrder;

    $wxApp = new MiniProgram();
    foreach ([$imgProfile] as $content){
        $checkMsg = $wxApp->msgSecCheck($content);
        if (!$checkMsg){
            Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
            exit();
        }
    }
    $minHomepageId = $infoCardDb->addCompanyMinHomepage($infoCardId, $imgUrl, $imgProfile, $userId);
    array_push($imageOrder, $minHomepageId);
    $imageOrder = json_encode($imageOrder);
    $infoCardDb->updateHomepageImageOrder($infoCardId, $imageOrder);

    $data['minHomepageId'] = $minHomepageId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
