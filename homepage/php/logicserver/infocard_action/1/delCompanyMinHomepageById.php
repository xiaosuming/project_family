<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-31
 * Time: 下午4:12
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;

$minHomepageId = Check::checkInteger($params['minHomepageId'] ?? ''); //微主页的id
$userId = $GLOBALS['userId'];
try {
    if ($minHomepageId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $infoCardDb = new CDBInfoCard();
    $result = $infoCardDb->getCompanyMinHomepageById($minHomepageId);
    if ($userId != $result['createBy'] || $result == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }
    $infoCardId = $result['infoCardId'];
    $infoCardArr = $infoCardDb->getInfoCard($infoCardId);
    $imageOrder = $infoCardArr->imageOrder;
    $delRow = $infoCardDb->delCompanyMinHomepageById($minHomepageId);
    if ($delRow > 0) {
        $key = array_search($minHomepageId, $imageOrder);
        if ($key !== false) {
            array_splice($imageOrder, $key, 1);
            $infoCardDb->updateHomepageImageOrder($infoCardId, json_encode($imageOrder));
        }
    }
    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}