<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-30
 * Time: 下午3:15
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$companyId = Check::checkInteger($params['companyId'] ?? '');
$userId = $GLOBALS['userId'];
if ($companyId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}
try {
    $infoCardDB = new CDBInfoCard();
    $result = $infoCardDB->getCompanyByOtherCompanyId($companyId);
    
    $accountDB = new CDBAccount();
    if ($result == null || !$accountDB->checkUserPermission($result['createBy'], $userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],'权限错误');
        exit;
    }
    $delRow = $infoCardDB->delOtherCompanyById($companyId);
    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
