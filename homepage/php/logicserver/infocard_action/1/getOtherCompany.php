<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-30
 * Time: 下午2:34
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;

$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
if ($infoCardId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}
try {
    $infoCardDB = new CDBInfoCard();
    $result = $infoCardDB->getOtherCompanyById($infoCardId);
    $data['list'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
