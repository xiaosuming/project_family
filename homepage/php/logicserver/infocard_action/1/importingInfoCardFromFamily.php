<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2018/3/12 0012
 * Time: 10:17
 */

use DB\CDBFamily;
use DB\CDBInfoCard;
use DB\CDBPerson;
use DB\CDBPushInfoCard;
use Model\InfoCard;
use Util\Check;
use Util\PhotoGenerator;
use Util\Util;

$familyId = Check::checkInteger(trim($params['familyId'] ?? ''));
$userId = $GLOBALS['userId'];

try {
    // 家族权限判断
    $cdbFamily = new CDBFamily();
    if (!$cdbFamily->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }
    $cdbPerson = new CDBPerson();
    $cdbInfoCard = new CDBInfoCard();
    $infoCard = new InfoCard();
    $personList = $cdbPerson->getFamilyPersonListWithPhone($familyId);
    if ($personList == array()) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '有手机号的家族人物为空');
        exit;
    }
    $data = array();
    $updateArr = array();
    foreach ($personList as $v) {
        $result = $cdbInfoCard->verifyPersonIdExists($v['id'], $userId);
        if ($result['personId'] == $v['id']) {
            // 这里要加上更新操作
            $infoCard->userId = $userId;
            $infoCard->familyId = $familyId;
            $infoCard->personId = $v['id'];
            $infoCard->remark = $v['name'];
            $infoCard->name = $v['name'];
            $infoCard->country = $v['country'];
            $infoCard->countryName = $v['country_name'];
            $infoCard->province = $v['province'];
            $infoCard->provinceName = $v['province_name'];
            $infoCard->city = $v['city'];
            $infoCard->cityName = $v['city_name'];
            $infoCard->area = $v['area'];
            $infoCard->areaName = $v['area_name'];
            $infoCard->town = $v['town'];
            $infoCard->townName = $v['town_name'];
            $infoCard->address = $v['address'];
            $infoCard->gender = $v['gender'];
            $infoCard->bloodType = $v['blood_type'];
            $infoCard->maritalStatus = ($v['marital_status'] === null) ? 0 : ($v['marital_status']);
            $infoCard->phone = json_encode(array($v['phone']));
            $infoCard->qq = $v['qq'];
            $infoCard->birthday = $v['birthday'];
            $infoCard->updateBy = $userId;
            if ($v['blood_type'] === null) {
                $infoCard->bloodType = '0';
            }
            $updateCount = $cdbInfoCard->updateImportInfoCard($infoCard);
            if ($updateCount > 0) {
                //search 添加进搜索任务
                if ($GLOBALS['TEST_INFO_CARD_TASK']) {
                    $CDBPushInfoCard = new CDBPushInfoCard();
                    $modelId = $userId . '_' . $result['id'];
                    $action = 1;
                    $access = [$userId];
                    $source = [
                        'id' => intval($result['id']),
                        'remark' => $infoCard->remark,
                        'userId' => intval($userId),
                        'familyId' => intval($infoCard->familyId),
                        'personId' => intval($infoCard->personId),
                        'name' => $infoCard->name,
                        'nameEn' => '',
                        'address' => $infoCard->address,
                        'photo' => $infoCard->photo,
                        'backgroundImg' => '',
                        'backgroundColor' => $infoCard->backgroundColor,
                        'backgroundType' => $infoCard->backgroundType,
                        'company' => '',
                        'companyEn' => '',
                        'phone' => Util::arrayToString($infoCard->phone),
                        'jobs' => '',
                        'type' => 2,
                        'email' => '',
                        'companyProfile' => '',
                        'access' => $access
                    ];
                    $CDBPushInfoCard->setPushInfoCardTask($modelId, $action, $source);
                }
            }
            array_push($updateArr, $updateCount);
            continue;
        } else {
            //检查identity是否存在
            $identityExists = true;
            while($identityExists){
                $identity = md5($userId . time() . Util::generateRandomCode(8));
                if ($cdbInfoCard->verifyIdentity($identity)){
                    $identityExists = true;
                }else{
                    $identityExists = false;
                }
            }
            $infoCard->userId = $userId;
            $infoCard->familyId = $familyId;
            $infoCard->personId = $v['id'];
            $infoCard->remark = $v['name'];
            $infoCard->name = $v['name'];
            $infoCard->country = $v['country'];
            $infoCard->countryName = $v['country_name'];
            $infoCard->province = $v['province'];
            $infoCard->provinceName = $v['province_name'];
            $infoCard->city = $v['city'];
            $infoCard->cityName = $v['city_name'];
            $infoCard->area = $v['area'];
            $infoCard->areaName = $v['area_name'];
            $infoCard->town = $v['town'];
            $infoCard->townName = $v['town_name'];
            $infoCard->address = $v['address'];
            $infoCard->gender = $v['gender'];
            $infoCard->bloodType = $v['blood_type'];
            $infoCard->maritalStatus = ($v['marital_status'] === null) ? 0 : ($v['marital_status']);
            $infoCard->phone = json_encode(array($v['phone']));
            $infoCard->qq = $v['qq'];
            $infoCard->birthday = $v['birthday'];
            $infoCard->createBy = $userId;
            $infoCard->updateBy = $userId;
            $infoCard->type = 2;
            $infoCard->jobs = json_encode(array());
            $infoCard->favorites = json_encode(array());
            $infoCard->skills = json_encode(array());
            $infoCard->photo = PhotoGenerator::generateUserPhotoFromResource();
            $infoCard->backgroundColor = '#ccccdd';
            $infoCard->backgroundType = 0;
            $infoCard->identity = $identity;
            $infoCard->isDefault = 0;

            if ($v['blood_type'] === null) {
                $infoCard->bloodType = '0';
            }
            $insertId = $cdbInfoCard->addInfoCard($infoCard);
            //search 添加进搜索任务
            if ($GLOBALS['TEST_INFO_CARD_TASK']) {
                $jobStr = Util::getJobStrFormJobs($infoCard->jobs);
                $CDBPushInfoCard = new CDBPushInfoCard();
                $modelId = $userId . '_' . $insertId;
                $action = 1;
                $source = [
                    'id' => intval($insertId),
                    'remark' => $infoCard->remark,
                    'userId' => intval($userId),
                    'familyId' => intval($infoCard->familyId),
                    'personId' => intval($infoCard->personId),
                    'name' => $infoCard->name,
                    'nameEn' => '',
                    'address' => $infoCard->address,
                    'photo' => $infoCard->photo,
                    'backgroundImg' => '',
                    'backgroundColor' => $infoCard->backgroundColor,
                    'backgroundType' => $infoCard->backgroundType,
                    'company' => '',
                    'companyEn' => '',
                    'phone' => Util::arrayToString($infoCard->phone),
                    'jobs' => $jobStr,
                    'type' => 2,
                    'email' => '',
                    'companyProfile' => ''
                ];
                $CDBPushInfoCard->setPushInfoCardTask($modelId, $action, $source);
            }
            array_push($data, $insertId);
        }
    }

    $insertCount = count($data);
    $updateCount = count($updateArr);
    $arr['updateCount'] = $updateCount;
    $arr['insertCount'] = $insertCount;
    $arr['msg'] = "更新成功";
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $arr);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}