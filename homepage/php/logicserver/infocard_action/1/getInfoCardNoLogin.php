<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-22
 * Time: 上午11:51
 */

use Util\Util;
use DB\CDBInfoCard;
use Util\Check;
use DB\CDBAccount;

$infoCardId = Check::checkInteger(trim(isset($params['infoCardId']) ? $params['infoCardId'] : ''));        //用户名片id

try {
    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $infoCardDB = new CDBInfoCard();

    $infoCard = $infoCardDB->getInfoCard($infoCardId);
    if ($infoCard->type != 5) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '不是官方创建的名片,无权限');
        exit;
    }
    $data['infoCard'] = $infoCard;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
