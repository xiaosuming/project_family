<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2018/3/5 0005
 * Time: 13:39
 */

use DB\CDBInfoCard;
use Util\Check;
use Util\Util;
use DB\CDBAccount;

$infoCardId = Check::checkInteger(trim(isset($params['infoCardId']) ? $params['infoCardId'] : ''));        //用户名片id

try {
    if ($infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $infoCardDB = new CDBInfoCard();
    $userId = $GLOBALS['userId'];
    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    $accountDB = new CDBAccount();
    //验证用户对名片的权限
    if ($infoCard == null || !$accountDB->checkUserPermission($infoCard->userId, $userId)) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }
    $sectionInfoCard = $infoCardDB->getSectionInfoFromCard($infoCardId);
    if ($sectionInfoCard == null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '查询出错');
        exit;
    }
    $phone = json_decode($sectionInfoCard['phone'],true);
    if (empty($phone)){
        $sectionInfoCard['phone'] = array();
    }else{
        $sectionInfoCard['phone'] = $phone;
    }
    $jobs = json_decode($sectionInfoCard['jobs'],true);
    if (empty($jobs)){
        $sectionInfoCard['jobs'] = array();
    }else{
        $arr = array_pop($jobs);
        $sectionInfoCard['jobs'] = $arr;
    }

    $data['sectionInfoCard'] = $sectionInfoCard;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}