<?php
/*添加名片并自动创建圈子*/

use DB\CDBAccount;
use DB\CDBArea;
use DB\CDBInfoCard;
use DB\CDBPushInfoCard;
use Model\InfoCard;
use Util\Check;
use Util\PhotoGenerator;
use Util\Util;
use ThirdParty\MiniProgram;

$remark = Check::check(trim($params['remark'] ?? ''), 1, 45);
$name = Check::check(trim($params['name'] ?? ''), 0, 45);
$country = Check::checkInteger($params['country'] ?? 0);
$province = Check::checkInteger($params['province'] ?? 0);
$city = Check::checkInteger($params['city'] ?? 0);
$area = Check::checkInteger($params['area'] ?? 0);
$town = Check::checkInteger($params['town'] ?? 0);
$address = Check::check($params['address'] ?? '');
$gender = Check::checkInteger($params['gender'] ?? 2);        //2是未知
$bloodType = Check::checkInteger($params['bloodType'] ?? 0);
$maritalStatus = Check::checkInteger($params['maritalStatus'] ?? 0);
$phone = Check::check($params['phone'] ?? ''); //phone  ,隔开
$skills = Check::check($params['skills'] ?? '');
$favorites = Check::check($params['favorites'] ?? '');
$jobs = Check::check($params['jobs'] ?? '');
$photo = Check::check($params['photo'] ?? PhotoGenerator::generateUserPhotoFromResource());
$backgroundImg = Check::check($params['backgroundImg'] ?? '');
$backgroundColor = Check::checkColor($params['backgroundColor'] ?? '#ccccdd');
$backgroundType = Check::checkInteger($params['backgroundType'] ?? 0); //0 背景颜色 1 背景图片
$company = Check::check($params['company'] ?? '');
$companyEn = Check::check($params['companyEn'] ?? '');  //公司英文名
$nameEn = Check::check($params['nameEn'] ?? ''); //英文名字
$companyProfile = Check::check($params['companyProfile'] ?? '', 0, 500); //公司
$bankAccount = Check::check($params['bankAccount'] ?? ''); //银行账号
$position = Check::check($params['position'] ?? ''); //工作
$ocrCardImg = Check::check($params['ocrCardImg'] ?? ''); //扫描的图片地址
$type = Check::checkInteger($params['type'] ?? 0); //名片类型 默认为0用户名片  4是ocr名片
$taxNumber = Check::check($params['taxNumber'] ?? ''); //税号
$userId = $GLOBALS['userId'];

if (!isset($params['birthday'])) {
    $birthday = null;
} else {
    $birthday = Check::checkDate($params['birthday']);    //生日
}
if (!isset($params['qq'])) {
    $qq = '';
} else {
    $qq = Check::checkInteger($params['qq']);
}
if (!isset($params['email'])) {
    $email = '';
} else {
    $email = Check::checkEmail($params['email']); // 邮箱
}
if (!isset($params['companyUrl'])) {
    $companyUrl = '';
} else {
    $companyUrl = Check::checkUrl($params['companyUrl']);  //公司网址
}
if (!isset($params['longitude'])) {
    $longitude = '';
} else {
    $longitude = Check::checkFloat($params['longitude'], -180, 180);//经度  -180-180  负数表示西经
}
if (!isset($params['latitude'])) {
    $latitude = '';
} else {
    $latitude = Check::checkFloat($params['latitude'], -90, 90);//纬度  -90-90  负数表示南纬
}

try {

    $CDBArea = new CDBArea();
    $result = $CDBArea->getAreaNameById($country);
    $countryName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($province);
    $provinceName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($city);
    $cityName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($area);
    $areaName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($town);
    $townName = $result['areaName'] ?? '0';
    $userId = $GLOBALS['userId'];

    $infoCard = new InfoCard();

    $accountDB = new CDBAccount();
    if ($type == 5) {
        if (!$accountDB->isSystemUser($userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '无权限创建官方名片');
            exit;
        }else{
            $infoCard->type = 5;
        }
    }

    $infoCard->remark = $remark;
    $infoCard->userId = $userId;
    $infoCard->name = $name;
    $infoCard->country = $country;
    $infoCard->countryName = $countryName;
    $infoCard->province = $province;
    $infoCard->provinceName = $provinceName;
    $infoCard->city = $city;
    $infoCard->cityName = $cityName;
    $infoCard->area = $area;
    $infoCard->areaName = $areaName;
    $infoCard->town = $town;
    $infoCard->townName = $townName;
    $infoCard->address = $address;
    $infoCard->gender = $gender;
    $infoCard->birthday = $birthday;
    $infoCard->bloodType = $bloodType;
    $infoCard->maritalStatus = $maritalStatus;
    $infoCard->phone = $phone;
    $infoCard->qq = $qq;
    $infoCard->familyId = 0;
    $infoCard->personId = 0;
    $infoCard->photo = $photo;
    $infoCard->backgroundImg = $backgroundImg;
    $infoCard->backgroundColor = $backgroundColor;
    $infoCard->backgroundType = $backgroundType;
    $infoCard->company = $company;
    $infoCard->companyEn = $companyEn;
    $infoCard->nameEn = $nameEn;
    $infoCard->email = $email;
    $infoCard->companyProfile = $companyProfile;
    $infoCard->companyUrl = $companyUrl;
    $infoCard->bankAccount = $bankAccount;
    $infoCard->longitude = $longitude;
    $infoCard->latitude = $latitude;
    $infoCard->position = $position;
    $infoCard->ocrCardImg = $ocrCardImg;
    $infoCard->type = $type;
    $infoCard->taxNumber = $taxNumber;

    if (trim($phone) != "") {
        //将手机号解析成数组
        $phoneArray = explode(',', $phone);
        if (count($phoneArray) > 5) {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '手机号最多为5个');
            exit;
        }
        if (strlen(json_encode($phoneArray)) > 100) {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '字符串长度不能大于100字节');
            exit;
        }
        foreach ($phoneArray as $v) {
            Check::checkNumber($v);
        }
        $infoCard->phone = json_encode($phoneArray);

    } else {
        $infoCard->phone = json_encode(array());
    }

    if (trim($skills) != "") {
        //这里将技能解析成数组
        $skillArray = explode(",", $skills);
        $infoCard->skills = json_encode($skillArray, JSON_UNESCAPED_UNICODE);
    } else {
        $infoCard->skills = json_encode(array());
    }

    if (trim($favorites) != "") {
        //这里将技能解析成数组
        $favoriteArray = explode(",", $favorites);
        $infoCard->favorites = json_encode($favoriteArray, JSON_UNESCAPED_UNICODE);
    } else {
        $infoCard->favorites = json_encode(array());
    }

    if (trim($jobs) != "") {
        $jobArray = explode(",", $jobs);
        $length = count($jobArray);
        $a = array();
        for ($i = 0; $i < $length; $i = $i + 3) {
            $arr = array('startTime' => $jobArray[$i + 0], 'endTime' => $jobArray[$i + 1], 'job' => $jobArray[$i + 2]);
            array_push($a, $arr);
        }
        $infoCard->jobs = json_encode($a, JSON_UNESCAPED_UNICODE);
    } else {
        $infoCard->jobs = json_encode(array());
    }

    $infoCardDB = new CDBInfoCard();

    //检查identity是否存在
    $identityExists = true;
    while ($identityExists) {
        $identity = md5($userId . time() . Util::generateRandomCode(8));
        if ($infoCardDB->verifyIdentity($identity)) {
            $identityExists = true;
        } else {
            $identityExists = false;
        }
    }

    $infoCard->identity = $identity;
    $infoCard->createBy = $userId;
    $infoCard->updateBy = $userId;

    //如果是创建的第一张名片，设置为默认
    if ($infoCardDB->checkInfoCardIdExistsByUserId($userId)) {
        $infoCard->isDefault = 0;
    } else {
        $infoCard->isDefault = 1;
    }
    $wxApp = new MiniProgram();
//    foreach ((array)$infoCard as $content){
//
//    }
    $checkMsg = $wxApp->msgSecCheck((array)$infoCard);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }


    list($socialCircleId, $infoCardId) = $infoCardDB->addInfoCardAndCircle($infoCard);

    if ($infoCardId > 0) {
        //search 添加进搜索任务
        $CDBPushInfoCard = new CDBPushInfoCard();
        $modelId = $userId . '_' . $infoCardId;
        $action = 1;
        $access = [$userId];
        $source = [
            'id' => intval($infoCardId),
            'remark' => $infoCard->remark,
            'userId' => intval($userId),
            'familyId' => 0,
            'personId' => 0,
            'name' => $infoCard->name,
            'nameEn' => $infoCard->nameEn,
            'address' => $infoCard->address,
            'photo' => $infoCard->photo,
            'backgroundImg' => $infoCard->backgroundImg,
            'backgroundColor' => $infoCard->backgroundColor,
            'backgroundType' => $infoCard->backgroundType,
            'company' => $infoCard->company,
            'companyEn' => $infoCard->companyEn,
            'phone' => Util::arrayToString($infoCard->phone),
            'jobs' => $infoCard->position,
            'type' => $type,
            'email' => $infoCard->email,
            'companyProfile' => $infoCard->companyProfile,
            'access' => $access
        ];
        $CDBPushInfoCard->setPushInfoCardTask($modelId, $action, $source);


        $data['infoCardId'] = $infoCardId;
        $data['socialCircleId'] = $socialCircleId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错");
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
