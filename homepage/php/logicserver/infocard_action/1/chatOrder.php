<?php
/**
 * 生成私信收费的订单
 * @author:  jiangpengfei
 * @date:    2019-03-19
 */

use DB\CDBOrder;
use Util\Util;
use Util\Check;
use Model\Order;
use DB\CDBInfoCard;
use Model\InfoCardChatOrder;
use Util\SysConst;
use ThirdParty\Wxpay\Lib\WxPayApi;
use ThirdParty\Wxpay\Lib\WxPayUnifiedOrder;
use ThirdParty\Wxpay\WxPayConfig;
use Util\SysLogger;


$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');
$content = Check::check($params['content'] ?? '');
$payway = Check::checkInteger($params['payway'] ?? '');
$userId = $GLOBALS['userId'];


try {

    if ($payway != SysConst::$ORDER_PAYWAY_WEIXIN && $payway != SysConst::$ORDER_PAYWAY_ALIPAY) {
        // 不合法的支付方式
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '不合法的支付方式');
        exit;
    }

    // 查询名片的信息
    $infoCardDB = new CDBInfoCard();
    $infoCard = $infoCardDB->getInfoCard($infoCardId);

    // 检查chatPermission
    if ($infoCard->chatPermission != 2) {
        // 不允许付费私信
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $order = new Order();
    $order->id = Util::getNextId();
    $order->userId = $userId;
    $order->status = SysConst::$ORDER_STATUS_UNPAY;
    $order->totalPrice = $infoCard->chatCost;
    $order->realPrice = $infoCard->chatCost;
    $order->discountStrategyId = 0; // 优惠策略id，没有的话就是0
    $order->userRemark = '';
    $order->failureTime = date("Y-m-d H:i:s", time() + 1800);   // 30分钟的订单失效时间
    $order->province = '';
    $order->city = '';
    $order->town = '';
    $order->address = '';
    $order->name = '';
    $order->phone = '';
    $order->goodsType = SysConst::$ORDER_GOODS_TYPE_INFOCARD_CHAT;
    $order->payway = $payway;
    $order->isDelete = 0;
    $order->createBy = $userId;
    $order->updateBy = $userId;

    // 开启事务
    $infoCardDB->pdo->beginTransaction();
    
    $prepayId = 0;
    $orderDB = new CDBOrder();
    $orderId = $orderDB->addOrder($order, $prepayId);

    if (!$orderId) {
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "添加订单失败");
        exit;
    }

    $infoCardChatOrder = new InfoCardChatOrder();
    $infoCardChatOrder->id = Util::getNextId();
    $infoCardChatOrder->userId = $userId;
    $infoCardChatOrder->infoCardId = $infoCardId;
    $infoCardChatOrder->content = $content;
    $infoCardChatOrder->orderId = $orderId;
    $infoCardChatOrder->isDelete = 0;
    $infoCardChatOrder->createBy = $userId;
    $infoCardChatOrder->updateBy = $userId;

    $infoCardDB->addInfoCardChatOrder($infoCardChatOrder);

    $infoCardDB->pdo->commit();

    $data['prepayId'] = $prepayId;   // 微信预支付id
    $data['sysOrderId'] = $order->id;           // 系统内的订单id

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $infoCardDB->pdo->rollback();
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
