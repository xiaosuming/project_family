<?php
/**
 * 用户名片索引文件
 */

/**
 * whiteList是白名单，用来允许某些请求通过验证
 */
$whiteList['getRandomInfoCardNoLogin'] = 'allow';
$whiteList['getInfoCardNoLogin'] = 'allow';
//$whiteList['getInfoCardByIdentity'] = 'allow';
$version = $params['version'] ?? "1";
require_once("filter/filter.php");
$infocard_action_file_array['addInfoCard'] = "logicserver/infocard_action/{$version}/addInfoCard.php";
$infocard_action_file_array['deleteInfoCard'] = "logicserver/infocard_action/{$version}/deleteInfoCard.php";
$infocard_action_file_array['editInfoCard'] = "logicserver/infocard_action/{$version}/editInfoCard.php";
$infocard_action_file_array['getInfoCard'] = "logicserver/infocard_action/{$version}/getInfoCard.php";
$infocard_action_file_array['getAllInfoCards'] = "logicserver/infocard_action/{$version}/getAllInfoCards.php";
$infocard_action_file_array['getSectionInfoFromCard'] = "logicserver/infocard_action/{$version}/getSectionInfoFromCard.php";
$infocard_action_file_array['addInfoCardByPhoneAddressBook'] = "logicserver/infocard_action/{$version}/addInfoCardByPhoneAddressBook.php";
$infocard_action_file_array['getInfoCardByIdentity'] = "logicserver/infocard_action/{$version}/getInfoCardByIdentity.php";
$infocard_action_file_array['receiveInfoCardByIdentity'] = "logicserver/infocard_action/{$version}/receiveInfoCardByIdentity.php";
$infocard_action_file_array['getAllInfoCardFromPhoneAddressBook'] = "logicserver/infocard_action/{$version}/getAllInfoCardFromPhoneAddressBook.php";
$infocard_action_file_array['getReceiveInfoCardPaging'] = "logicserver/infocard_action/{$version}/getReceiveInfoCardPaging.php";
$infocard_action_file_array['deleteReceiveInfoCard'] = "logicserver/infocard_action/{$version}/deleteReceiveInfoCard.php";
$infocard_action_file_array['updateIsShowForMyInfoCard'] = "logicserver/infocard_action/{$version}/updateIsShowForMyInfoCard.php";
$infocard_action_file_array['importingInfoCardFromFamily'] = "logicserver/infocard_action/{$version}/importingInfoCardFromFamily.php";
$infocard_action_file_array['getInfoCardPageByImporting'] = "logicserver/infocard_action/{$version}/getInfoCardPageByImporting.php";
$infocard_action_file_array['getImportFamilyList'] = "logicserver/infocard_action/{$version}/getImportFamilyList.php";
$infocard_action_file_array['createInfoCardByPhoneAddressBook'] = "logicserver/infocard_action/{$version}/createInfoCardByPhoneAddressBook.php";
$infocard_action_file_array['getAllReceiveInfoCard'] = "logicserver/infocard_action/{$version}/getAllReceiveInfoCard.php";
$infocard_action_file_array['getAllInfoCardByImporting'] = "logicserver/infocard_action/{$version}/getAllInfoCardByImporting.php";
$infocard_action_file_array['changeBackgroundType'] = "logicserver/infocard_action/{$version}/changeBackgroundType.php";
$infocard_action_file_array['getUserReceiveCode'] = "logicserver/infocard_action/{$version}/getUserReceiveCode.php";
$infocard_action_file_array['sendInfoCard'] = "logicserver/infocard_action/{$version}/sendInfoCard.php";
$infocard_action_file_array['getStaticReceiveCode'] = "logicserver/infocard_action/{$version}/getStaticReceiveCode.php";
$infocard_action_file_array['addOtherCompanyByInfoCardId'] = "logicserver/infocard_action/{$version}/addOtherCompanyByInfoCardId.php";
$infocard_action_file_array['getOtherCompany'] = "logicserver/infocard_action/{$version}/getOtherCompany.php";
$infocard_action_file_array['delOtherCompanyById'] = "logicserver/infocard_action/{$version}/delOtherCompanyById.php";
$infocard_action_file_array['editOtherCompanyById'] = "logicserver/infocard_action/{$version}/editOtherCompanyById.php";
$infocard_action_file_array['addCompanyMinHomepage'] = "logicserver/infocard_action/{$version}/addCompanyMinHomepage.php";
$infocard_action_file_array['delCompanyMinHomepageById'] = "logicserver/infocard_action/{$version}/delCompanyMinHomepageById.php";
$infocard_action_file_array['editCompanyMinHomepageById'] = "logicserver/infocard_action/{$version}/editCompanyMinHomepageById.php";
$infocard_action_file_array['getCompanyMinHomepages'] = "logicserver/infocard_action/{$version}/getCompanyMinHomepages.php";
$infocard_action_file_array['updateInfoCardPermission'] = "logicserver/infocard_action/{$version}/updateInfoCardPermission.php";
$infocard_action_file_array['getShareCode'] = "logicserver/infocard_action/{$version}/getShareCode.php";
$infocard_action_file_array['receiveByShareCode'] = "logicserver/infocard_action/{$version}/receiveByShareCode.php";
$infocard_action_file_array['addOCRTask'] = "logicserver/infocard_action/{$version}/addOCRTask.php";
$infocard_action_file_array['getOCRTaskResult'] = "logicserver/infocard_action/{$version}/getOCRTaskResult.php";
$infocard_action_file_array['getAllTypeInfoCardsPageByImport'] = "logicserver/infocard_action/{$version}/getAllTypeInfoCardsPageByImport.php";
$infocard_action_file_array['increView'] = "logicserver/infocard_action/{$version}/increView.php";
$infocard_action_file_array['sendExchangeMsg'] = "logicserver/infocard_action/{$version}/sendExchangeMsg.php";
$infocard_action_file_array['acceptExchangeInfoCard'] = "logicserver/infocard_action/{$version}/acceptExchangeInfoCard.php";
$infocard_action_file_array['getInfoCardByMessageId'] = "logicserver/infocard_action/{$version}/getInfoCardByMessageId.php";
$infocard_action_file_array['updateHomepageImageOrder'] = "logicserver/infocard_action/{$version}/updateHomepageImageOrder.php";
$infocard_action_file_array['updateIsDefaultForMyInfoCard'] = "logicserver/infocard_action/{$version}/updateIsDefaultForMyInfoCard.php";
$infocard_action_file_array['createInfoCardExchange'] = "logicserver/infocard_action/{$version}/createInfoCardExchange.php";
$infocard_action_file_array['endInfoCardExchange'] = "logicserver/infocard_action/{$version}/endInfoCardExchange.php";
$infocard_action_file_array['joinInfoCardExchange'] = "logicserver/infocard_action/{$version}/joinInfoCardExchange.php";
$infocard_action_file_array['getUserCreateInfoCardExchangePaging'] = "logicserver/infocard_action/{$version}/getUserCreateInfoCardExchangePaging.php";
$infocard_action_file_array['getExchangeDetail'] = "logicserver/infocard_action/{$version}/getExchangeDetail.php";
$infocard_action_file_array['getUserJoinInfoCardExchangePaging'] = "logicserver/infocard_action/{$version}/getUserJoinInfoCardExchangePaging.php";
$infocard_action_file_array['getInfoCardNet'] = "logicserver/infocard_action/{$version}/getInfoCardNet.php";
$infocard_action_file_array['checkOtherHasInfoCard'] = "logicserver/infocard_action/{$version}/checkOtherHasInfoCard.php";
$infocard_action_file_array['checkUserExistInExchangeByCode'] = "logicserver/infocard_action/{$version}/checkUserExistInExchangeByCode.php";
$infocard_action_file_array['deleteGroupExchange'] = "logicserver/infocard_action/{$version}/deleteGroupExchange.php";
$infocard_action_file_array['updateInfoCardOpenScope'] = "logicserver/infocard_action/{$version}/updateInfoCardOpenScope.php";
$infocard_action_file_array['getUserInfoCardInFamily'] = "logicserver/infocard_action/{$version}/getUserInfoCardInFamily.php";
$infocard_action_file_array['addInfoCardTag'] = "logicserver/infocard_action/{$version}/addInfoCardTag.php";
$infocard_action_file_array['updateInfoCardTagName'] = "logicserver/infocard_action/{$version}/updateInfoCardTagName.php";
$infocard_action_file_array['deleteInfoCardTag'] = "logicserver/infocard_action/{$version}/deleteInfoCardTag.php";


$infocard_action_file_array['getUserTagsPaging'] = "logicserver/infocard_action/{$version}/getUserTagsPaging.php";
$infocard_action_file_array['getInfoCardsPagingByTagId'] = "logicserver/infocard_action/{$version}/getInfoCardsPagingByTagId.php";
$infocard_action_file_array['addInfoCardsToTag'] = "logicserver/infocard_action/{$version}/addInfoCardsToTag.php";
$infocard_action_file_array['removeInfoCardFromTag'] = "logicserver/infocard_action/{$version}/removeInfoCardFromTag.php";
$infocard_action_file_array['getInfoCardsByBatchShareCode'] = "logicserver/infocard_action/{$version}/getInfoCardsByBatchShareCode.php";
$infocard_action_file_array['shareMultiInfoCards'] = "logicserver/infocard_action/{$version}/shareMultiInfoCards.php";
$infocard_action_file_array['receiveByBatchShareCode'] = "logicserver/infocard_action/{$version}/receiveByBatchShareCode.php";

$infocard_action_file_array['getAllInfoCardsByTagId'] = "logicserver/infocard_action/{$version}/getAllInfoCardsByTagId.php";
$infocard_action_file_array['setChatPermission'] = "logicserver/infocard_action/{$version}/setChatPermission.php";
$infocard_action_file_array['chatOrder'] = "logicserver/infocard_action/{$version}/chatOrder.php";
$infocard_action_file_array['getUserDefaultInfoCardOrFirst'] = "logicserver/infocard_action/{$version}/getUserDefaultInfoCardOrFirst.php";
$infocard_action_file_array['getRandomInfoCardNoLogin'] = "logicserver/infocard_action/{$version}/getRandomInfoCardNoLogin.php";
$infocard_action_file_array['getInfoCardNoLogin'] = "logicserver/infocard_action/{$version}/getInfoCardNoLogin.php";
