<?php

    use Util\Util;
    use Util\ImageAddtext;
    use Util\Pinyin;
    use DB\CDBAccount;

    //验证参数
    if(!isset($params['surname']) || !isset($params['id'])){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "姓氏或封面为空"); //-20000018
        exit;
    }
    if(mb_strlen($params['surname']) > 2){ //最长2个汉字
    	Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "姓氏长度错误"); //-20000018
        exit;
    }
    if(intval($params['id']) <= 0){
    	Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "未选择封面");
        exit;
    }

    if(isset($params['pinyin']) && strlen($params['pinyin']) > 0)
    {
    	if(strlen($params['pinyin']) > 12){ //最长2个汉字的拼音
    		Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "拼音长度错误");
            exit;
    	}

    }else{
        $py = new Pinyin();
        $params['pinyin'] = $py->getpy($params['surname'], true);
        if(strlen($params['pinyin']) == 0){
        	Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "可能是个生僻字，系统无法识别文字, 请手动输入拼音");
           exit;
        }
    }


    $ImageAddtext = new ImageAddtext();
    $ImageAddtext->getCoverImage($params);