<?php
    
$version = $params['version'] ?? "1";
$whiteList['getImage'] = 'allow';
$whiteList['getTest'] = 'allow';

require_once("filter/filter.php");


$familycover_action_file_array['getImage'] = "logicserver/familycover_action/{$version}/getImage.php";
$familycover_action_file_array['getTest'] = "logicserver/familycover_action/{$version}/getTest.php";
