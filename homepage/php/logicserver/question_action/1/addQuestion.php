<?php

use DB\CDBAccount;
use DB\CDBInformation;
use DB\CDBQuestion;
use Util\Check;
use Util\PostGenerate;
use Util\Util;
use DB\CDBPoint;
use ThirdParty\MiniProgram;

$title = Check::check(trim(isset($params['title']) ? $params['title'] : ''), 1, 45);           //标题
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''), 1, 500);     //内容
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));           //图片
$socialCircleId = Check::checkInteger($params['socialCircleId'] ?? ''); //圈子id
$point = Check::checkInteger($params['point'] ?? 0);
$userId = $GLOBALS['userId'];
$title = Util::filterContentReplace($title);
$content = Util::filterContentReplace($content);

if ($point < 0) {
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '参数错误');
    exit;
}

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$title, $content]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}


try {
    if ($title == '' || $content == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    $informationDB = new CDBInformation();
    //检查用户对活动的查看权限
    if (!$informationDB->checkUserFollowExist($userId, $socialCircleId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $circleDetail = $informationDB->getInfoCategoryDetail($socialCircleId);

    if (is_null($circleDetail)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '圈子不存在');
        exit;
    }

    $accountDB = new CDBAccount();
    $user = $accountDB->getUserInfo($userId);

    if ($circleDetail['circleType'] == 5) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '用户不可以向名片圈子发布问题');
        exit;
    }

    if ($circleDetail['circleType'] == 2 && $user['user_type'] != 2) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '该圈子只有系统用户可以发布问题');
        exit;
    }

    if ($photo != '') {
        //这里将photo解析成image数组
        $pathArray = explode(",", $photo);
        if (count($pathArray) > 9) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量超过限制");
            exit();
        }
        $pathJsonStr = json_encode($pathArray);
    } else {
        $pathJsonStr = json_encode(array());
    }

    $pointDB = new CDBPoint();
    if (!$pointDB->checkUserPoint($userId, $point)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '积分不足');
        exit;
    }


    $questionDB = new CDBQuestion();
    $questionId = $questionDB->addQuestion($socialCircleId, $userId, $title, $content, $pathJsonStr, $point);

    if ($questionId > 0) {

        $questionContent = PostGenerate::generateAddQuestionPost($questionId, $title);

        // 普通用户
        if ($user['user_type'] == 1) {
            $type = 4;        //推文类型 1是用户发表，2是活动,3是系统推送,4是发布问题
            $postId = $informationDB->addPost($type, '', '', '', $userId, $socialCircleId, $pathJsonStr, $questionContent);
            $data['addedPostId'] = $postId;

            // 系统推送用户
        } else if ($user['user_type'] == 2) {
            $type = 4;
            $postId = $informationDB->addSystemPost($type, '', '', '', $userId, $socialCircleId, $pathJsonStr, $questionContent);
            $data['addedPostId'] = $postId;
        }

        $data['id'] = $questionId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "问题提交失败");
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
