<?php
/**
 * 设置最佳答案
 * @author jiangpengfei
 * @date   2019-03-28
 */

use Util\Util;
use Util\Check;
use DB\CDBQuestion;

$answerId = Check::checkInteger($params['answerId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $questionDB = new CDBQuestion();
    $answer = $questionDB->getAnswerById($answerId);
    if ($answer == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }


    $question = $questionDB->getQuestionByQuestionId($answer->questionId);

    if ($question == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }


    if ($question->isFinish == '1') {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '问题已经结束，不能重复设置');
        exit;
    }

    if ($question->userId != $userId) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
    }

    $res = $questionDB->setBestAnswer($answer->questionId, $question->point, $answerId, $answer->userId, $userId);

    if (!$res) {
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], '设置最佳答案失败');
        exit;
    }

    $data['result'] = $res;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}