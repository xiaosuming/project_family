<?php

use Util\Util;
use Util\Check;
use DB\CDBQuestion;

$answerId = Check::checkInteger(trim(isset($params['answerId']) ? $params['answerId'] : ''));
$userId = $GLOBALS['userId'];

try {
    if($answerId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    $questionDB = new CDBQuestion();
    //check permission
    $permission = $questionDB->checkUserIdAndAnswerId($userId, $answerId);
    if(!$permission) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }
    //delete
    $rows = $questionDB->deleteAnswer($answerId);
    if($rows > 0){
        $data['rows'] = $rows;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else if($rows == -1){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
        exit;
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
        exit;
    }
} catch(PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}