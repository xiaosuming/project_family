<?php

// 接口弃用

use Util\Util;
use Util\Check;
use Util\Pager;
use DB\CDBTask;
use DB\CDBQuestion;
use DB\CDBPushQuestion;

$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));        //页码
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 10));        //页面大小
$userId = $GLOBALS['userId'];

try {
    $questionDB = new CDBQuestion();
    $taskDB = new CDBTask();
    //get user timeline
    $pushQuestion = new CDBPushQuestion();
    $pushes = $pushQuestion->getTimeline($userId, $pageIndex, $pageSize);

    $total = $pushQuestion->getTimelinePostCount($userId);
    //divide question and task
    $questionIdSet = array();
    $taskIdSet = array();
    foreach ($pushes as $id) {
        //get type, 1->question; 2->task
        $type = $id[0];
        //get id
        $id = substr($id, 1, strlen($id) - 1);

        if ($type === "1") {//question
            //add to questionId set
            array_push($questionIdSet, $id);
        } else if ($type === "2") {//task
            //add to taskId set
            array_push($taskIdSet, $id);
        }
    }
    $questionIdSet = "(" . implode(",", $questionIdSet) . ")";
    $taskIdSet = "(" . implode(",", $taskIdSet) . ")";

    //get detail
    $questionsDetail = array(); //detail set
    $tasksDetail = array();     //detail set
    $timeline = array();
    if ($questionIdSet !== "()") {
        //get question detail
        $questionsDetail = $questionDB->getQuestionsDetail($questionIdSet);
        //add to timeline
        foreach ($questionsDetail as $k => $i) {

            if (is_null($i['answerCount'])) {
                $i['answerCount'] = 0;
            }
            $i['type'] = '1';
            $createTime = strtotime($i['createTime']);
            $id = $i['id'];

            $answerData = $questionDB->getAnswerInfoByQuestionId($id);
            $i['answerInfo'] = $answerData;

            $key = $createTime . '#' . $id . '#1';//排序键
            $i['photo'] = json_decode($i['photo'], true);//解析照片数组
            $timeline[$key] = $i;
        }
    }
    if ($taskIdSet != "()") {
        //get task detail
        $tasksDetail = $taskDB->getTasksDetail($taskIdSet);
        //add to timeline
        foreach ($tasksDetail as $i) {
            if (is_null($i['tenderCount'])) {
                $i['tenderCount'] = 0;
            }
            $i['type'] = '2';
            $createTime = strtotime($i['createTime']);
            $id = $i['id'];

            $taskData = $taskDB->getTaskTenderInfoByTaskId($id);
            $i['taskTenderInfo'] = $taskData;

            $key = $createTime . '#' . $id . '#2';//排序键
            $i['photo'] = json_decode($i['photo'], true);//解析照片数组
            $timeline[$key] = $i;
        }
    }

    krsort($timeline);

    $result = array();
    $keys = array_keys($timeline);

    foreach ($keys as $i) {
        array_push($result, $timeline[$i]);
    }
    $page = new Pager($total, $result, $pageIndex, $pageSize);
    $page->printPage();
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
