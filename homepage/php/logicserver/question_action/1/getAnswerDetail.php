<?php

use Util\Util;
use Util\Check;
use Model\Answer;
use DB\CDBQuestion;

$answerId = Check::checkInteger(trim(isset($params['answerId']) ? $params['answerId'] : ''));

try {
    if($answerId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    $questionDB = new CDBQuestion();
    $answer = $questionDB->getAnswerDetail($answerId);
    if(is_null($answer)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "回答不存在");
    } else {
        $data["id"] = $answer->id;;
        $data["userId"] = $answer->userId;
        $data["questionId"] = $answer->questionId;
        $data["content"] = $answer->content;
        $data["comment_count"] = $answer->comment_count;
        $data["like_count"] = $answer->like_count;
        $data["createTime"] = $answer->createTime;
        $data["updateTime"] = $answer->updateTime;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }
} catch(PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}