<?php

use DB\CDBInformation;
use Util\Util;
use Util\Check;
use Model\Qusetion;
use DB\CDBQuestion;

$questionId = Check::checkInteger(trim(isset($params['questionId']) ? $params['questionId'] : ''));
$userId = $GLOBALS['userId'];
try {
    if ($questionId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    $questionDB = new CDBQuestion();
    $question = $questionDB->getQuestionByQuestionId($questionId);

    if (is_null($question)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "此问题不存在");
        exit;
    }

    $informationDB = new CDBInformation();
    //检查用户对问题的查看权限
    if (!$informationDB->checkUserFollowExist($userId, $question->socialCircleId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $salt = 'shareQuestion';
    $shareCode = md5($questionId . $question->socialCircleId . $userId . $salt);

    $data['id'] = $question->id;
    $data['userId'] = $question->userId;
    $data['title'] = $question->title;
    $data['content'] = $question->content;
    $data['photo'] = json_decode($question->photo);
    $data['addition'] = $question->addition;
    $data['socialCircleId'] = $question->socialCircleId;
    $data['createTime'] = $question->createTime;
    $data['createBy'] = $question->createBy;
    $data['updateTime'] = $question->updateTime;
    $data['updateBy'] = $question->updateBy;

    $data['shareCode'] = $shareCode;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

