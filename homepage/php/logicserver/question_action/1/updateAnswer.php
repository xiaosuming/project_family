<?php

use Util\Util;
use Util\Check;
use DB\CDBQuestion;
use ThirdParty\MiniProgram;

$answerId = Check::checkInteger(trim(isset($params['answerId']) ? $params['answerId'] : ''));
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''));
$userId = $GLOBALS['userId'];
$content = Util::filterContentReplace($content);

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$content]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    if ($answerId == '' || $content == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    $questionDB = new CDBQuestion();
    //check permission
    $permission = $questionDB->checkUserIdAndAnswerId($userId, $answerId);
    if (!$permission) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }
    //update
    $rows = $questionDB->updateAnswerContent($answerId, $content, $userId);
    if ($rows > 0) {//success
        $data['rows'] = $rows;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {//fail
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "更新失败");
        exit;
    }
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
