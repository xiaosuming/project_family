<?php

use Util\Util;
use Util\Check;
use DB\CDBQuestion;
use ThirdParty\MiniProgram;

$questionId = Check::checkInteger(trim(isset($params['questionId']) ? $params['questionId'] : ''));
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''));
$content = Util::filterContentReplace($content);
$userId = $GLOBALS['userId'];

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$content]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    if ($questionId == '' || $content == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    $questionDB = new CDBQuestion();
    //check question exist
    $question = $questionDB->getQuestionByQuestionId($questionId);
    if (is_null($question)) {//question not exist
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "记录不存在");
        exit;
    }
    //add answer
    $answerId = $questionDB->addAnswer($userId, $questionId, $content);
    if ($answerId > 0) {
        $data['id'] = $answerId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入失败");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
