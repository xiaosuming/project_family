<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-29
 * Time: 上午10:26
 */

use Util\Util;
use Util\Check;
use Model\Qusetion;
use DB\CDBQuestion;
use DB\CDBInformation;
use DB\CDBFamily;

$questionId = Check::checkInteger(trim(isset($params['questionId']) ? $params['questionId'] : ''));
$shareCode = Check::check($params['shareCode'] ?? '');
$fromUserId = Check::checkInteger($params['fromUserId'] ?? '');

try {
    if ($questionId == '' || $shareCode == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    $questionDB = new CDBQuestion();
    $question = $questionDB->getQuestionByQuestionId($questionId);

    if (is_null($question)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "此问题不存在");
        exit;
    }

    $salt = 'shareQuestion';
    $verifyShareCode = md5($questionId . $question->socialCircleId . $fromUserId. $salt);

    if ($verifyShareCode != $shareCode) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '无权查看');
        exit;
    }

    $question->photo = json_decode($question->photo);
    $data['question'] = $question;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

