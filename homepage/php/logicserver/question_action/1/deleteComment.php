<?php

use Util\Util;
use Util\Check;
use DB\CDBQuestion;

$commentId = Check::checkInteger(trim(isset($params['commentId']) ? $params['commentId'] : ''));
$userId = $GLOBALS['userId'];

try {
    if($commentId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    $questionDB = new CDBQuestion();
    //check permission
    $permission = $questionDB->checkUserIdAndCommentId($userId, $commentId);
    if(!$permission) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }
    //delete
    $rows = $questionDB->deleteComment($commentId);
    if($rows > 0){
        $data['rows'] = $rows;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else if($rows == -1){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
        exit;
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
        exit;
    }
} catch(PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}