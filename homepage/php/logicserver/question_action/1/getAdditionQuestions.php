<?php

use Util\Util;
use Util\Check;
use DB\CDBQuestion;

$questionId = Check::checkInteger(trim(isset($params['questionId']) ? $params['questionId'] : ''));

if($questionId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {
    $questionDB = new CDBQuestion();
    $res = $questionDB->getAdditionQuestions($questionId);
    foreach ($res as $key => $i) {
        $res[$key]['photo'] = json_decode($i['photo'], true);
    }
    $data['additions'] = $res;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch(PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}