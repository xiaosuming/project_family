<?php

use Util\Util;
use Util\Check;
use DB\CDBQuestion;

$answerId = Check::checkInteger(trim(isset($params['answerId']) ? $params['answerId'] : ''));

try {
    if($answerId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    $questionDB = new CDBQuestion();
    $res = $questionDB->getComments($answerId);
    $data['comments'] = $res;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch(PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}