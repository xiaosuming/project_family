<?php
/**
 * 分页获取问题列表
 * @author: jiangpengfei
 * @date:   2019-03-26
 */

use Util\Util;
use Util\Check;
use DB\CDBQuestion;
use DB\CDBInformation;
use Util\Paginator;

$maxId = Check::checkInteger($params['maxId'] ?? '0');
$sinceId = Check::checkInteger($params['sinceId'] ?? '0');
$count = Check::checkInteger($params['count'] ?? '10');
$userId = $GLOBALS['userId'];

try {

    $informationDB = new CDBInformation();
    $list = $informationDB->getUserFollowCategoryForQuery($userId);

    $list = array_column($list, 'categoryId');

    if (count($list) == 0) {
        // 没有圈子
        $paginator = new Paginator(0, [], 0, 0, true);
        $paginator->printPage();
        exit;
    }

    $questionDB = new CDBQuestion();
    $questions = $questionDB->getQuestionPaging($list, $maxId, $sinceId, $count);
    $total = $questionDB->getQuestionTotal($list);

    $isLastPage = false;

    $len = count($questions);
    if ($len < $count) {
        $isLastPage = true;
    }

    if ($len > 0) {
        $maxId = $questions[$len-1]['id'];
        $sinceId = $questions[0]['id'];
    } else {
        $maxId = 0;
        $sinceId = 0;
    }

    foreach($questions as $key => $question) {
        $questions[$key]['photo'] = json_decode($question['photo']);
    }


    $paginator = new Paginator($total, $questions, $maxId, $sinceId, $isLastPage);
    $paginator->printPage();

} catch(PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}