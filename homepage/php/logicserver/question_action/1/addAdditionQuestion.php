<?php

use Util\Util;
use Util\Check;
use DB\CDBQuestion;
use ThirdParty\MiniProgram;

$questionId = Check::checkInteger(trim(isset($params['questionId']) ? $params['questionId'] : ''));
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''),1,500);
$content = Util::filterContentReplace($content);
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));
$userId = $GLOBALS['userId'];

if($questionId == '' || $content == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$content]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {

    $pathJsonStr = "";
    if ($photo != '') {
        //这里将photo解析成image数组
        $pathArray = explode(",", $photo);
        if (count($pathArray) > 9) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量超过限制");
            exit();
        }
        $pathJsonStr = json_encode($pathArray);
    } else {
        $pathJsonStr = json_encode(array());
    }
    $photo = $pathJsonStr;

    $questionDB = new CDBQuestion();

    //check question exist
    $question = $questionDB->getQuestionByQuestionId($questionId);
    if($question == null) {//not exist
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "问题不存在");
        exit;
    }

    //check permission
    $permission = $questionDB->checkUserIdAndQuestionId($userId, $questionId);
    if(!$permission) {//user has no permission
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    //add addition
    $additionId = $questionDB->addAdditionQuestion($userId, $questionId, $content, $photo);
    if($additionId == -1) {//exceoption
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
        exit;
    } else {//success
        $data['id'] = $additionId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }
} catch(PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
