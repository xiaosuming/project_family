<?php

use Util\Util;
use Util\Check;
use Util\Pager;
use DB\CDBQuestion;

$questionId = Check::check(trim(isset($params['questionId']) ? $params['questionId'] : ''));
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 10));

try {
    if($questionId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    $questionDB = new CDBQuestion();

    //判断问题是否存在
    $question = $questionDB->getQuestionByQuestionId($questionId);
    if(is_null($question)) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "问题不存在");
        exit;
    }
    //获取回答总数
    $total = $questionDB->getAnswersCount($questionId);
    if(($pageIndex-1)*$pageSize > $total) {//页码不正确
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "页码不正确");
        exit;
    }
    //获取分页
    $result = $questionDB->getAnswersPaging($questionId, $pageIndex, $pageSize);
    //创建分页并输出
    $page = new Pager($total, $result, $pageIndex, $pageSize);
    $page->printPage();
} catch(PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}