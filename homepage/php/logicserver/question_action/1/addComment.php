<?php

use Util\Util;
use Util\Check;
use DB\CDBQuestion;
use Model\Answer;
use ThirdParty\MiniProgram;

$userId = $GLOBALS['userId'];
$answerId = Check::checkInteger(trim(isset($params['answerId']) ? $params['answerId'] : ''));
$replyTo = Check::checkInteger(trim(isset($params['replyTo']) ? $params['replyTo'] : -1));
$replyToUserId = Check::checkInteger(trim(isset($params['replyToUserId']) ? $params['replyToUserId'] : $userId));
$comment = Check::check(trim(isset($params['comment']) ? $params['comment'] : ''), 1, 500);
$comment = Util::filterContentReplace($comment);

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$comment]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    if ($answerId == '' || $comment == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    $questionDB = new CDBQuestion();

    //check answer exist
    $answer = $questionDB->getAnswerById($answerId);
    if (is_null($answer)) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "回答不存在");
        exit;
    }
    //add answer
    $commentId = $questionDB->addComment($answerId, $userId, $replyTo, $replyToUserId, $comment);
    if ($commentId == -1) {//exception
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
        exit;
    } else {//success
        $data['id'] = $commentId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
