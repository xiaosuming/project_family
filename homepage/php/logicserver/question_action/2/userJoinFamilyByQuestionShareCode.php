<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-29
 * Time: 下午3:28
 */

use DB\CDBFamily;
use DB\CDBInformation;
use DB\CDBQuestion;
use Util\Check;
use Util\Util;

$shareCode = Check::check($params['shareCode'] ?? '');
$fromUserId = Check::checkInteger($params['fromUserId'] ?? '');
$questionId = Check::checkInteger(trim($params['questionId'] ?? ''));        //问题id
$userId = $GLOBALS['userId'];

if ($questionId == '' || $shareCode == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}
try {

    $questionDB = new CDBQuestion();
    $question = $questionDB->getQuestionByQuestionId($questionId);

    if (is_null($question)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "此问题不存在");
        exit;
    }

    $salt = 'shareQuestion';
    $verifyShareCode = md5($questionId . $question->socialCircleId . $fromUserId . $salt);

    if ($verifyShareCode != $shareCode) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '无权查看');
        exit;
    }

    $informationDB = new CDBInformation();
    //检查用户对活动的查看权限
    if ($informationDB->checkUserFollowExist($userId, $question->socialCircleId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户已经关注了圈子");
        exit;
    }

    $familyDB = new CDBFamily();
    $familyId = $familyDB->getFamilyIdBySocialCircleId($question->socialCircleId);
    if (!$familyId) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '家族信息不存在');
        exit;
    }

    if ($familyDB->acceptTemp2UserJoin($familyId, $question->socialCircleId, $userId, $fromUserId)) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], '加入家族成功');
        exit;
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
