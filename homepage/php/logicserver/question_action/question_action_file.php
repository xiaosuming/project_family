<?php
$whiteList['getQuestionDetailWithoutLogin'] = 'allow';
$version = $params['version'] ?? "1";
require_once("filter/filter.php");

// 提问
$question_action_file_array['addQuestion'] = "logicserver/question_action/{$version}/addQuestion.php";
$question_action_file_array['getQuestionDetail'] = "logicserver/question_action/{$version}/getQuestionDetail.php";
$question_action_file_array['deleteQuestion'] = "logicserver/question_action/{$version}/deleteQuestion.php";
// 追问
$question_action_file_array['addAdditionQuestion'] = "logicserver/question_action/{$version}/addAdditionQuestion.php";
$question_action_file_array['getAdditionQuestions'] = "logicserver/question_action/{$version}/getAdditionQuestions.php";
// 回答
$question_action_file_array['addAnswer'] = "logicserver/question_action/{$version}/addAnswer.php";
$question_action_file_array['getAnswersPaging'] = "logicserver/question_action/{$version}/getAnswersPaging.php";
$question_action_file_array['getAnswerDetail'] = "logicserver/question_action/{$version}/getAnswerDetail.php";
$question_action_file_array['updateAnswer'] = "logicserver/question_action/{$version}/updateAnswer.php";
// 评论
$question_action_file_array['addComment'] = "logicserver/question_action/{$version}/addComment.php";
$question_action_file_array['getComments'] = "logicserver/question_action/{$version}/getComments.php";
$question_action_file_array['deleteComment'] = "logicserver/question_action/{$version}/deleteComment.php";


$question_action_file_array['getQuestionAndTaskPaging'] = "logicserver/question_action/{$version}/getQuestionAndTaskPaging.php";
// $question_action_file_array[''] = "logicserver/question_action/{$version}/";
$question_action_file_array['deleteAnswer'] = "logicserver/question_action/{$version}/deleteAnswer.php";
$question_action_file_array['getQuestionPaging'] = "logicserver/question_action/{$version}/getQuestionPaging.php";
$question_action_file_array['getQuestionDetailWithoutLogin'] = "logicserver/question_action/{$version}/getQuestionDetailWithoutLogin.php";

$question_action_file_array['setBestAnswer'] = "logicserver/question_action/{$version}/setBestAnswer.php";
$question_action_file_array['userJoinFamilyByQuestionShareCode'] = "logicserver/question_action/{$version}/userJoinFamilyByQuestionShareCode.php";
