<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 上午11:44
 */

/**
 * whiteList是白名单，用来允许某些请求通过验证
 */
$whiteList['getInfoCategoryList'] = 'allow';
$whiteList['getInfoByCategoryId'] = 'allow';
$whiteList['getInfoDetailById'] = 'allow';
$whiteList['getInfoLikeList'] = 'allow';
$whiteList['getInfoShareList'] = 'allow';
$whiteList['getCommentListByInfoId'] = 'allow';
$whiteList['getReplyCommentListByCommentId'] = 'allow';
$whiteList['getInfoByCategoryIdPagingNoLogin'] = 'allow';
$whiteList['getOfficialCircles'] = 'allow';
$whiteList['getAllOfficialInfoPaging'] = 'allow';

$version = $params['version'] ?? "1";
require_once("filter/filter.php");
$information_action_file_array['addInfoCategory'] = "logicserver/information_action/{$version}/addInfoCategory.php";
$information_action_file_array['getInfoCategoryList'] = "logicserver/information_action/{$version}/getInfoCategoryList.php";
$information_action_file_array['getInfoCategoryListByUserId'] = "logicserver/information_action/{$version}/getInfoCategoryListByUserId.php";
$information_action_file_array['getUserPublishInfoCategoryList'] = "logicserver/information_action/{$version}/getUserPublishInfoCategoryList.php";
$information_action_file_array['delInfoCategoryById'] = "logicserver/information_action/{$version}/delInfoCategoryById.php";
$information_action_file_array['updateInfoCategoryById'] = "logicserver/information_action/{$version}/updateInfoCategoryById.php";
$information_action_file_array['addInformation'] = "logicserver/information_action/{$version}/addInformation.php";
$information_action_file_array['updateIsAcceptForInformation'] = "logicserver/information_action/{$version}/updateIsAcceptForInformation.php";
$information_action_file_array['getInfoByCategoryId'] = "logicserver/information_action/{$version}/getInfoByCategoryId.php";
$information_action_file_array['getInfoByCategoryIdPaging'] = "logicserver/information_action/{$version}/getInfoByCategoryIdPaging.php";
$information_action_file_array['getInfoByCategoryIdAndUserId'] = "logicserver/information_action/{$version}/getInfoByCategoryIdAndUserId.php";
$information_action_file_array['getInfoByCategoryIdAndUserIdPaging'] = "logicserver/information_action/{$version}/getInfoByCategoryIdAndUserIdPaging.php";
$information_action_file_array['getInfoByUserId'] = "logicserver/information_action/{$version}/getInfoByUserId.php";
$information_action_file_array['getInfoByUserIdPaging'] = "logicserver/information_action/{$version}/getInfoByUserIdPaging.php";
$information_action_file_array['getInfoDetailById'] = "logicserver/information_action/{$version}/getInfoDetailById.php";
$information_action_file_array['getInfoDetailByIdWithUserId'] = "logicserver/information_action/{$version}/getInfoDetailByIdWithUserId.php";
$information_action_file_array['updateInfoById'] = "logicserver/information_action/{$version}/updateInfoById.php";
$information_action_file_array['delInfoById'] = "logicserver/information_action/{$version}/delInfoById.php";
$information_action_file_array['addUserFollowCategory'] = "logicserver/information_action/{$version}/addUserFollowCategory.php";
$information_action_file_array['cancelUserFollowCategory'] = "logicserver/information_action/{$version}/cancelUserFollowCategory.php";
$information_action_file_array['getUserFollowCategoryList'] = "logicserver/information_action/{$version}/getUserFollowCategoryList.php";
$information_action_file_array['addComment'] = "logicserver/information_action/{$version}/addComment.php";
$information_action_file_array['getCommentListByInfoId'] = "logicserver/information_action/{$version}/getCommentListByInfoId.php";
$information_action_file_array['getReplyCommentListByCommentId'] = "logicserver/information_action/{$version}/getReplyCommentListByCommentId.php";
$information_action_file_array['addInfoLike'] = "logicserver/information_action/{$version}/addInfoLike.php";
$information_action_file_array['getUserLikeList'] = "logicserver/information_action/{$version}/getUserLikeList.php";
$information_action_file_array['delUserInfoLike'] = "logicserver/information_action/{$version}/delUserInfoLike.php";
$information_action_file_array['addInfoShare'] = "logicserver/information_action/{$version}/addInfoShare.php";
$information_action_file_array['getUserShareList'] = "logicserver/information_action/{$version}/getUserShareList.php";
$information_action_file_array['addInfoResource'] = "logicserver/information_action/{$version}/addInfoResource.php";
$information_action_file_array['getResourceListByInfoId'] = "logicserver/information_action/{$version}/getResourceListByInfoId.php";
$information_action_file_array['getUserSubscribePaging'] = "logicserver/information_action/{$version}/getUserSubscribePaging.php";
$information_action_file_array['updateInfoResourceById'] = "logicserver/information_action/{$version}/updateInfoResourceById.php";
$information_action_file_array['getReviewInformationPagingByAdmin'] = "logicserver/information_action/{$version}/getReviewInformationPagingByAdmin.php";
$information_action_file_array['getInfoLikeList'] = "logicserver/information_action/{$version}/getInfoLikeList.php";
$information_action_file_array['getInfoShareList'] = "logicserver/information_action/{$version}/getInfoShareList.php";
$information_action_file_array['delCommentById'] = "logicserver/information_action/{$version}/delCommentById.php";
$information_action_file_array['followCircle'] = "logicserver/information_action/{$version}/followCircle.php";
$information_action_file_array['cancelFollowCircle'] = "logicserver/information_action/{$version}/cancelFollowCircle.php";
$information_action_file_array['addPost'] = "logicserver/information_action/{$version}/addPost.php";
$information_action_file_array['getPost'] = "logicserver/information_action/{$version}/getPost.php";
$information_action_file_array['getPostsPaging'] = "logicserver/information_action/{$version}/getPostsPaging.php";
$information_action_file_array['delPost'] = "logicserver/information_action/{$version}/delPost.php";
$information_action_file_array['addPostArticle'] = "logicserver/information_action/{$version}/addPostArticle.php";
$information_action_file_array['getUserCanPublishActivityAndQuestionCircleList'] = "logicserver/information_action/{$version}/getUserCanPublishActivityAndQuestionCircleList.php";
$information_action_file_array['checkUserFollowCircleStatus'] = "logicserver/information_action/{$version}/checkUserFollowCircleStatus.php";
$information_action_file_array['getInfoByCategoryIdPagingNoLogin'] = "logicserver/information_action/{$version}/getInfoByCategoryIdPagingNoLogin.php";
$information_action_file_array['getOfficialCircles'] = "logicserver/information_action/{$version}/getOfficialCircles.php";
$information_action_file_array['getAllOfficialInfoPaging'] = "logicserver/information_action/{$version}/getAllOfficialInfoPaging.php";
$information_action_file_array['getUserCanPublishSocialCircleList'] = "logicserver/information_action/{$version}/getUserCanPublishSocialCircleList.php";


