<?php
/**
 * 删除推文 动态or文章
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-4
 * Time: 下午3:53
 */

use DB\CDBAccount;
use DB\CDBInformation;
use Util\Check;
use Util\SysLogger;
use Util\Util;

$postId = Check::checkInteger($params['postId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $informationDB = new CDBInformation();
    $infoDetail = $informationDB->getInformationById($postId);
    if ($infoDetail == null || $infoDetail['createBy'] != $userId) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $categoryId = $infoDetail['categoryId'];

    $accountDB = new CDBAccount();
    $user = $accountDB->getUserInfo($userId);
    $userType = $user['user_type'];

    $delRow = $informationDB->delInformationByIdInCircle($postId, $categoryId, $userId, $userType);

    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
