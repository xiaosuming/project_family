<?php
/**
 * 发表文章
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午5:49
 */

use DB\CDBAccount;
use DB\CDBInformation;
use Util\Check;
use Util\SysLogger;
use Util\Util;

$userId = $GLOBALS['userId'];
$socialCircleId = Check::checkInteger($params['socialCircleId'] ?? ''); //圈子
$photo = Check::check(trim($params['photo'] ?? ''));
$title = Check::check($params['title'] ?? '');
$title = Util::filterContentReplace($title);
$content = addslashes($params['content'] ?? '');
$content = Util::filterContentReplace($content);
$isMd = Check::checkInteger($params['isMd'] ?? 0);
$jsonResource = $params['jsonResource'] ?? json_encode(array()); //资源json字符串

if ($title == '' || $content == '' || $photo == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {

    $informationDB = new CDBInformation();

    if (!$informationDB->checkUserHasPermissionToPublishPostInCircle($socialCircleId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $photos = array();
    //照片换成json格式
    if ($photo != '') {
        array_push($photos, $photo);
        $photoStr = json_encode($photos);
    } else {
        $photoStr = json_encode($photos);
    }

    $html_str = htmlspecialchars_decode($content);
    $subContent = mb_substr(strip_tags($html_str), 0, 100);

    $resourceData = json_decode($jsonResource, true, JSON_UNESCAPED_UNICODE);

    $accountDB = new CDBAccount();
    $user = $accountDB->getUserInfo($userId);

    // 普通用户
    if ($user['user_type'] == 1) {
        $type = 1;        //推文类型 1是用户发表，2是活动,3是系统推送
        $postId = $informationDB->addPostArticle($type, $userId, $socialCircleId, $photoStr, $title, $content, $subContent, $isMd, $resourceData);
        $data['addedPostId'] = $postId;

        // 系统推送用户
    } else if ($user['user_type'] == 2) {
        $type = 3; // 系统推送
        $postId = $informationDB->addSystemPostArticle($type, $userId, $socialCircleId, $photoStr, $title, $content, $subContent, $isMd, $resourceData);
        $data['addedPostId'] = $postId;
    }

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
