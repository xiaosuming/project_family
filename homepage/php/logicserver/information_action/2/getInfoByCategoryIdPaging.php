<?php
/**
 * 根据分类id分页获取资讯列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午2:37
 */

use DB\CDBInformation;
use Util\Check;
use Util\Paginator;
use Util\Util;

$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$count = Check::checkInteger($params['count'] ?? 10);
$categoryId = Check::checkInteger($params['categoryId'] ?? ''); //分类id
$userId = $GLOBALS['userId'];

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $infoDB = new CDBInformation();
    $total = $infoDB->getCountInformationListByCategoryId($categoryId);

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $list = $infoDB->getInformationListByCategoryIdWithMaxIdAndSinceId($userId, $categoryId, $maxId, $sinceId, $count);
    foreach ($list as $k => $v) {
        if ($v['isLike'] === null) {
            $list[$k]['isLike'] = false;
        } else {
            $list[$k]['isLike'] = true;
        }

        //兼容之前的数据
        if (!Util::isJson($v['photo'])) {
            $photos = array();
            if ($v['photo'] != '') {
                array_push($photos, $v['photo']);
            }
        } else {
            $photos = json_decode($v['photo']);
        }
        $list[$k]['photo'] = $photos;
    }

    $len = count($list);

    $nextSinceId = $list[0]['id'];

    $nextMaxId = $list[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $list, $nextMaxId, $nextSinceId, $lastPage);
    $paginator->printPage();

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
