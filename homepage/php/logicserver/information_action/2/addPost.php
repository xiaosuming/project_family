<?php
/**
 * 发表动态
 * Created by PhpStorm.
 * User: xll
 * Date: 19-2-28
 * Time: 上午10:15
 */

use DB\CDBAccount;
use DB\CDBInformation;
use DB\CDBMessage;
use Model\Message;
use Util\Check;
use Util\SysLogger;
use Util\Util;


$socialCircleId = Check::checkInteger($params['socialCircleId'] ?? ''); //圈子
$content = Check::check(trim($params['content'] ?? ''), 1, 255);
$content = Util::filterContentReplace($content);
$photo = Check::check($params['photo'] ?? '');
$coorX = Check::check(trim(isset($params['coorX']) ? $params['coorX'] : ''));
$coorY = Check::check(trim(isset($params['coorY']) ? $params['coorY'] : ''));
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''), 0, 100);

$userId = $GLOBALS['userId'];

// $type  //  1是用户发的动态  2是活动动态  3是系统动态

try {

    $informationDB = new CDBInformation();

    //circleType 是2 只有官方可以发  用户关注圈子才可以发动态
    if (!$informationDB->checkUserHasPermissionToPublishPostInCircle($socialCircleId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    if ($photo != "") {
        //这里将photo解析成image数组
        $pathArray = explode(",", $photo);
        if (count($pathArray) > 9) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "图片数量超过限制");
            exit();
        }
        $pathJsonStr = json_encode($pathArray);
    } else {
        $pathJsonStr = json_encode(array());
    }

    $accountDB = new CDBAccount();
    $user = $accountDB->getUserInfo($userId);

    // 普通用户
    if ($user['user_type'] == 1) {
        $type = 1;        //推文类型 1是用户发表，2是活动,3是系统推送
        $postId = $informationDB->addPost($type, $coorX, $coorY, $address, $userId, $socialCircleId, $pathJsonStr, $content);
        $data['addedPostId'] = $postId;

        // 系统推送用户
    } else if ($user['user_type'] == 2) {
        $type = 3; // 系统推送
        $postId = $informationDB->addSystemPost($type, $coorX, $coorY, $address, $userId, $socialCircleId, $pathJsonStr, $content);
        $data['addedPostId'] = $postId;
    }

    if ($postId > 0) {
        $atUsers = Util::getAtUser($content);   //获取推文中at的用户
        if (count($atUsers) > 0) {
            //发送message
            $messageDB = new CDBMessage();

            foreach ($atUsers as $atUserId => $atUserNickname) {
                $msg = new Message();
                $msg->fromUser = $userId;                        //发件人id
                $msg->toUser = $atUserId;                        //接收人id
                $msg->content = '有人在推文中提到了你';                //内容
                $msg->module = $GLOBALS['INFO_POST_MODULE'];        //模块
                $msg->action = $GLOBALS['AT_USER'];                //动作
                $msg->recordId = $postId;                        //相关记录id
                $msg->createBy = $userId;               //创建人
                $msg->updateBy = $userId;               //更新人
                $messageDB->addMessage($msg);
            }
        }

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据插入错误");
    }


} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
