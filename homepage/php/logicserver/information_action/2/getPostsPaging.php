<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-4
 * Time: 下午2:21
 */

use DB\CDBInformation;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

$userId = $GLOBALS['userId'];
$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);

$maxTimeStamp = $params['maxTimeStamp'] ?? '0';
$sinceTimeStamp = $params['sinceTimeStamp'] ?? '0';
$count = Check::checkInteger($params['count'] ?? '');

if ($count <= 0 || $count >= 50) {
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
    exit;
}
try {
    $informationDB = new CDBInformation();
    $total = $informationDB->getPostsPagingTotal($userId);

    $isFinish = false;
    $nextSinceTimeStamp = $sinceTimeStamp;
    $nextMaxId = $maxId;
    $nextMaxTimeStamp = $maxTimeStamp;
    $finalPosts = [];
    $lastPage = false;
    $isSinceIdEdit = false;

    while (!$isFinish) {

        list($timeline, $posts, $endIndex) =
            $informationDB->getPostsPagingByMaxIdAndSinceId($userId, $nextMaxId, $sinceId,
                $count * 2, $nextMaxTimeStamp, $sinceTimeStamp);

        $len = count($posts);

        if ($len == 0) {
            $lastPage = true;
            break;
        }

        if (!$isSinceIdEdit) {
            // 如果nextSinceId还没有修改
            $nextSinceId = $posts[0]['id'];
            $nextSinceTimeStamp = $timeline[$nextSinceId];

            $isSinceIdEdit = true;
            $sinceId = 0;       // 防止下一次循环sinceId生效
            $sinceTimeStamp = 0; // 防止下一次循环sinceTimeStamp生效
        }

        $nextMaxId = $posts[$len - 1]['id'];
        $nextMaxTimeStamp = $timeline[$nextMaxId];

        for ($i = 0; $i < $len; $i++) {

            $posts[$i]['photo'] = json_decode($posts[$i]['photo'], true);
            $finalPosts[] = $posts[$i];
            if (count($finalPosts) == $count) {
                $nextMaxId = $posts[$i]['id'];
                $nextMaxTimeStamp = $timeline[$nextMaxId];
                break;  // 推文数量已经够了
            }

        }

        $lastPage = false;
        if (count($finalPosts) == $count) {
            $isFinish = true;
            // 判断是不是最后一页
            if ($endIndex - $count * 2 + $i + 1 >= $total) {
                $lastPage = true;
            }
        } else {
            // 还没有获取完所需数量的推文

            // 判断是不是最后一页
            if ($endIndex + 1 >= $total) {
                $lastPage = true;
                $isFinish = true;
            }
        }
    }

    $paginator = new Paginator($total, $finalPosts, $nextMaxId, $nextSinceId, $lastPage, $nextMaxTimeStamp, $nextSinceTimeStamp);
    $paginator->printPage();
} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
