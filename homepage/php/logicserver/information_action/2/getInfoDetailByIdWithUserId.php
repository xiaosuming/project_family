<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午2:49
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$informationId = Check::checkInteger($params['informationId'] ?? '');
$userId = $GLOBALS['userId'];
try {
    $infoDB = new CDBInformation();
    $detail = $infoDB->getInformationByIdWithUserId($informationId, $userId);
    if ($detail['isLike'] === null) {
        $detail['isLike'] = false;
    } else {
        $detail['isLike'] = true;
    }
    //兼容之前的数据
    if (!Util::isJson($detail['photo'])) {
        $photos = array();
        if ($detail['photo'] != '') {
            array_push($photos, $detail['photo']);
        }
    } else {
        $photos = json_decode($detail['photo']);
    }
    $detail['photo'] = $photos;

    $resourceList = $infoDB->getResourceListByInfoId($informationId);
    $detail['sourceList'] = $resourceList;

    $data['detail'] = $detail;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
