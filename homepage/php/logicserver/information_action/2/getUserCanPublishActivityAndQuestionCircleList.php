<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-29
 * Time: 下午1:33
 */

use DB\CDBInformation;
use Util\Util;

$userId = $GLOBALS['userId'];

try {

    $informationDB = new CDBInformation();
    $list = $informationDB->getUserPublishActivityAndQuestionInfoCategoryList($userId);
    $data['list'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    \Util\SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
