<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午5:16
 */

use DB\CDBInformation;
use Util\Util;

$userId = $GLOBALS['userId'];

try {
    $infoDB = new CDBInformation();
    $list = $infoDB->getUserShareList($userId);

    foreach ($list as $k => $v) {
        if ($v['isLike'] === null) {
            $list[$k]['isLike'] = false;
        } else {
            $list[$k]['isLike'] = true;
        }

        //兼容之前的数据
        if (!Util::isJson($v['photo'])) {
            $photos = array();
            if ($v['photo'] != '') {
                array_push($photos, $v['photo']);
            }
        } else {
            $photos = json_decode($v['photo']);
        }
        $list[$k]['photo'] = $photos;
    }

    $data['list'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
