<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-29
 * Time: 下午2:24
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];
$socialCircleId = Check::checkInteger($params['socialCircleId'] ?? '');

try {

    $informationDB = new CDBInformation();
    $result = $informationDB->checkUserFollowExist($userId, $socialCircleId);
    $data['status'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    \Util\SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
