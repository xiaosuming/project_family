<?php
/**
 * 获取资讯评论列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-31
 * Time: 下午2:46
 */

use DB\CDBInformation;
use Util\Check;
use Util\Paginator;
use Util\Util;

$infoId = Check::checkInteger($params['infoId'] ?? '');
$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$count = Check::checkInteger(trim($params['count'] ?? 10));     // 默认是10条数据

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $infoDB = new CDBInformation();

    $total = $infoDB->getCountInfoCommentByInfoId($infoId);

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $comments = $infoDB->getInfoCommentPagingByInfoId($infoId, $maxId, $sinceId, $count);

    foreach ($comments as $k => $v) {
        $list = $infoDB->getReplyCommentListByCommentId($v['id']);
        $comments[$k]['son'] = $list;
    }

    $len = count($comments);

    $nextSinceId = $comments[0]['id'];

    $nextMaxId = $comments[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $comments, $nextMaxId, $nextSinceId, $lastPage);
    $paginator->printPage();

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
