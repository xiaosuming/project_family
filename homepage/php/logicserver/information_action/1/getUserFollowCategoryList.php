<?php
/**
 * 获取用户关注的分类列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午3:39
 */

use DB\CDBInformation;
use Util\Util;

$userId = $GLOBALS['userId'];

try {
    $infoDB = new CDBInformation();
    $list = $infoDB->getUserFollowCategoryList($userId);
    $data['list'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}