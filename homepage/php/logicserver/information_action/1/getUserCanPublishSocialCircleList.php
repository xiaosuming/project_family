<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-5-9
 * Time: 上午10:56
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$postType = Check::checkInteger($params['postType'] ?? ''); // 1 文章动态  2 问题活动
$userId = $GLOBALS['userId'];

try {
    $infoDB = new CDBInformation();
    if ($postType == 1) {
        $list = $infoDB->getUserPublishInfoCategoryList($userId);
    } else if ($postType == 2) {
        $list = $infoDB->getUserPublishActivityAndQuestionInfoCategoryList($userId);
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '参数错误');
        exit;
    }

    $data['list'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    \Util\SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
