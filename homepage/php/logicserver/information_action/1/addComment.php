<?php
/**
 * 添加评论
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午4:15
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;
use ThirdParty\MiniProgram;

$infoId = Check::checkInteger($params['infoId'] ?? '');
$replyToCommentId = Check::checkInteger($params['replyToCommentId'] ?? ''); //回复的评论id  -1代表文章
$replyToUserId = Check::checkInteger($params['replyToUserId'] ?? ''); //回复的用户id
$comment = Check::check($params['comment'] ?? '');
$comment = Util::filterContentReplace($comment);
$userId = $GLOBALS['userId'];

if ($comment == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$comment]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    $infoDB = new CDBInformation();
    $commentId = $infoDB->addComment($userId, $infoId, $replyToCommentId, $replyToUserId, $comment);
    $data['commentId'] = $commentId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
