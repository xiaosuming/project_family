<?php
/**
 * 获取用户创建的资讯列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午2:37
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];
$pageIndex = Check::checkIntegerUnsigned($params['pageIndex'] ?? 1);
$pageSize = Check::checkIntegerUnsigned($params['pageSize'] ?? 10);

try {
    $infoDB = new CDBInformation();
    $list = $infoDB->getInformationListByUserIdWithPage($userId, $pageIndex, $pageSize);

    foreach ($list as $k => $v) {
        if ($v['isLike'] === null) {
            $list[$k]['isLike'] = false;
        } else {
            $list[$k]['isLike'] = true;
        }
    }
    $data['page'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
