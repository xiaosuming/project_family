<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-22
 * Time: 下午1:37
 */

use DB\CDBInformation;
use Util\Check;
use Util\Paginator;
use Util\Util;

$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$count = Check::checkInteger($params['count'] ?? 10);

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $infoDB = new CDBInformation();
    $total = $infoDB->getCountAllOfficialInformation();

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $list = $infoDB->getAllOfficialInformationListPagingNoLogin($maxId, $sinceId, $count);
    foreach ($list as $k => $v) {
        //兼容之前的数据
        if (!Util::isJson($v['photo'])) {
            $photos = array();
            if ($v['photo'] != '') {
                array_push($photos, $v['photo']);
            }
        } else {
            $photos = json_decode($v['photo']);
        }
        $list[$k]['photo'] = $photos;
    }

    $len = count($list);

    $nextSinceId = $list[0]['id'];

    $nextMaxId = $list[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $list, $nextMaxId, $nextSinceId, $lastPage);
    $paginator->printPage();

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
