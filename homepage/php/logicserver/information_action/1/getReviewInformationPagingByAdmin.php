<?php
/**
 * 管理员分页获取待审核的资讯列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 上午11:30
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$categoryId = Check::checkInteger($params['categoryId'] ?? ''); //分类id
$pageIndex = Check::checkIntegerUnsigned($params['pageIndex'] ?? 1);
$pageSize = Check::checkIntegerUnsigned($params['pageSize'] ?? 10);

$userId = $GLOBALS['userId'];
try {

    $infoDB = new CDBInformation();

    if (!$infoDB->isSystemUser($userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '不是系统用户，权限错误');
        exit;
    }
    $list = $infoDB->getReviewInformationListByCategoryIdWithPageByAdmin($categoryId, $pageIndex, $pageSize);
    foreach ($list as $k => $v) {
        if ($v['isLike'] === null) {
            $list[$k]['isLike'] = false;
        } else {
            $list[$k]['isLike'] = true;
        }
        //兼容之前的数据
        if (!Util::isJson($v['photo'])) {
            $photos = array();
            if ($v['photo'] != '') {
                array_push($photos, $v['photo']);
            }
        } else {
            $photos = json_decode($v['photo']);
        }
        $list[$k]['photo'] = $photos;
    }
    $data['page'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
