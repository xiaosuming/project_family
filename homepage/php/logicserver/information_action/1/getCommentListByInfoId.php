<?php
/**
 * 获取资讯评论列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-31
 * Time: 下午2:46
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$infoId = Check::checkInteger($params['infoId'] ?? '');

try {
    $infoDB = new CDBInformation();
    $list = $infoDB->getInfoCommentListByInfoId($infoId);
    $data['list'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
