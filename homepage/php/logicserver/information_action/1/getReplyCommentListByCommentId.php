<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-31
 * Time: 下午3:33
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$commentId = Check::checkInteger($params['commentId'] ?? '');

try {
    $infoDB = new CDBInformation();
    $replyList = $infoDB->getReplyCommentListByCommentId($commentId);
    $data['list'] = $replyList;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}