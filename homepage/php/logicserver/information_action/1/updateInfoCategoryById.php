<?php
/**
 * 编辑分类
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午5:35
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$categoryId = Check::checkInteger($params['categoryId'] ?? '');//分类id
$avatar = Check::check($params['avatar'] ?? '');
$name = Util::filterContentReplace(Check::check($params['name'] ?? ''));
$userId = $GLOBALS['userId'];

if ($avatar == '' || $name == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}

try {

    $infoDB = new CDBInformation();

    $infoCategoryDetail = $infoDB->getInfoCategoryDetail($categoryId);

    if ($infoCategoryDetail == null || $infoCategoryDetail['createBy'] != $userId) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $result = $infoDB->updateInformationCategoryById($categoryId, $avatar, $name, $userId);
    $data['updateRow'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
