<?php
/**
 * 删除分类
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午4:59
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];
try {
    $categoryId = Check::checkInteger($params['categoryId'] ?? '');

    $infoDB = new CDBInformation();

    if (!$infoDB->isSystemUser($userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '不是系统用户，权限错误');
        exit;
    }

    $list = $infoDB->getInformationCategoryList();
    $childIdArr = Util::getChildId($list, $categoryId);
    array_push($childIdArr, $categoryId);
    $childs = implode(',', $childIdArr);

    $delRow = $infoDB->delInformationCategoryById($childs);
    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}