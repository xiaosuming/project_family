<?php
/**
 * 删除资讯
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午2:49
 */

use DB\CDBInformation;
use DB\CDBPushInformation;
use Util\Check;
use Util\Util;

$informationId = Check::checkInteger($params['informationId'] ?? '');
$userId = $GLOBALS['userId'];


try {
    $infoDB = new CDBInformation();

    // 获取资讯详情
    $info = $infoDB->getInformationById($informationId);

    if (!$infoDB->isUserOfInformation($informationId, $userId) && !$infoDB->isSystemUser($userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $delRow = $infoDB->delInformationById($informationId);

    if ($delRow > 0) {
        // 1.获取当前分类下所有关注的用户
        $userIdsMap = $infoDB->getCategoryFollowers($info['categoryId']);

        $userIds = [];
        foreach ($userIdsMap as $userIdMap) {
            $tmpUserId = $userIdMap['userId'];

            // 排除用户自己
            if ($tmpUserId != $userId) {
                $userIds[] = $tmpUserId;
            }
        }

        if (count($userIds) > 0) {
            $pushInfoDB = new CDBPushInformation();
            $pushInfoDB->setDeleteInfoTask($userIds, $informationId, $userId);
        }
        $data['delRow'] = $delRow;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], '删除失败');
        exit;
    }


} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
