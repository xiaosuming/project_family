<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-1-22
 * Time: 下午4:02
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$commentId = Check::checkInteger($params['commentId'] ?? '');
$userId = $GLOBALS['userId'];
try {

    $infoDB = new CDBInformation();
    $detail = $infoDB->getCommentDetailById($commentId);
    if ($detail == null || $detail['userId'] != $userId) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $delRow = $infoDB->delComment($detail['infoId'], $commentId);
    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
