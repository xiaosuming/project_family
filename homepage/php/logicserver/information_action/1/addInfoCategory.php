<?php
/**
 * 添加资讯分类
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午2:17
 */


use DB\CDBInformation;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];
$avatar = Check::check($params['avatar'] ?? ''); //分类icon
$name = Check::check($params['name'] ?? ''); //分类名
$name = Util::filterContentReplace($name);
$pid = Check::checkInteger($params['pid'] ?? 0);//父id  0代表一级分类
$circleType = Check::checkInteger($params['circleType'] ?? 2); //官方创建用户可发布1,官方创建用户不可发布2， 族群自动创建3，用户创建4，名片圈子5

if ($avatar == '' || $name == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}

try {

    $infoDB = new CDBInformation();

    if ($circleType == 1 || $circleType == 2) {
        if (!$infoDB->isSystemUser($userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '不是系统用户，权限错误');
            exit;
        }
    }

    $result = $infoDB->addInformationCategory($userId, $avatar, $name, $pid, $circleType);
    $data['categoryId'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
