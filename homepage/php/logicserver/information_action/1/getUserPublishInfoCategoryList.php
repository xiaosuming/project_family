<?php
/**
 * 获取普通用户的可以发布资讯的分类列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午4:45
 */

use DB\CDBInformation;
use Util\Util;

$userId = $GLOBALS['userId'];

try {
    $infoDB = new CDBInformation();
    $list = $infoDB->getUserPublishInfoCategoryList($userId);
    $data['list'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
