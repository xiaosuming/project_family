<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-31
 * Time: 上午11:29
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$infoId = Check::checkInteger($params['infoId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $infoDB = new CDBInformation();
    if (!$infoDB->checkInformationPermissionWithUserId($infoId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }
    $resourceList = $infoDB->getResourceListByInfoId($infoId);
    $data['resourceList'] = $resourceList;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}