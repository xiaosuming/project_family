<?php
/**
 * 获取分类列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午4:45
 */

use DB\CDBInformation;
use Util\Util;

try {
    $infoDB = new CDBInformation();
    $list = $infoDB->getInformationCategoryList();
    $data['list'] = Util::generateTree($list,'pid');
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
