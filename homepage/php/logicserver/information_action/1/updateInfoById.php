<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午2:49
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$informationId = Check::checkInteger($params['informationId'] ?? '');
$categoryId = Check::checkInteger($params['categoryId'] ?? '');
$photo = Check::check($params['photo'] ?? '');
$title = Check::check($params['title'] ?? '');
$title = Util::filterContentReplace($title);
$content = addslashes($params['content'] ?? '');
$content = Util::filterContentReplace($content);
$isMd = Check::checkInteger($params['isMd'] ?? '');
$jsonResource = $params['jsonResource'] ?? json_encode(array());

$userId = $GLOBALS['userId'];

if ($title == '' || $content == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}

try {
    $infoDB = new CDBInformation();
    $resourceData = json_decode($jsonResource, true, JSON_UNESCAPED_UNICODE);

    if (!$infoDB->checkPermissionUserCanPublishCategory($categoryId, $userId) || !$infoDB->isUserOfInformation($informationId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $html_str = htmlspecialchars_decode($content);
    $subContent = mb_substr(strip_tags($html_str), 0, 100);

    $updateRow = $infoDB->updateInformation($informationId, $userId, $categoryId, $photo, $title, $content, $subContent, $isMd, $resourceData);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
