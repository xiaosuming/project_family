<?php
/**
 * 取消用户点赞
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午4:35
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$infoId = Check::checkInteger($params['infoId'] ?? '');

$userId = $GLOBALS['userId'];
try {
    $infoDB = new CDBInformation();
    if (!$infoDB->checkUserInfoLikeExist($userId, $infoId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '未点赞该资讯');
        exit;
    }
    $delRow = $infoDB->cancelUserInfoLike($userId, $infoId);
    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
