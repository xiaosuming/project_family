<?php
/**
 * 添加资讯
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午5:49
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;
use DB\CDBPushInformation;
use DB\UserFollowDB;

$userId = $GLOBALS['userId'];
$categoryId = Check::checkInteger($params['categoryId'] ?? ''); //分类id
$photo = Check::check($params['photo'] ?? '');
$title = Check::check($params['title'] ?? '');
$title = Util::filterContentReplace($title);
$content = addslashes($params['content'] ?? '');
$content = Util::filterContentReplace($content);
$isMd = Check::checkInteger($params['isMd'] ?? 0);
$jsonResource = $params['jsonResource'] ?? json_encode(array()); //资源json字符串

//[{"remark":"beizhu","url":"url...","type":"1"},{"remark":"beizhu","url":"url...","type":"2"},{"remark":"beizhu","url":"url...","type":"3"}]

if ($title == '' || $content == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {

    $infoDB = new CDBInformation();
    if (!$infoDB->checkPermissionUserCanPublishCategory($categoryId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限出错');
        exit;
    }

    $html_str = htmlspecialchars_decode($content);
    $subContent = mb_substr(strip_tags($html_str), 0, 100);

    $resourceData = json_decode($jsonResource, true, JSON_UNESCAPED_UNICODE);

    $informationId = $infoDB->insertInformation($userId, $categoryId, $photo, $title, $content, $subContent, $isMd, $resourceData);

    if ($informationId > 0) {
        // 资讯添加成功，添加异步推送任务

        // 1.获取当前分类下所有关注的用户
        $userIdsMap = $infoDB->getCategoryFollowers($categoryId);

        $userIds = [];
        $isAppend = [];
        foreach ($userIdsMap as $userIdMap) {
            $tmpUserId = $userIdMap['userId'];

            // 排除用户自己
            if ($tmpUserId != $userId) {
                $isAppend[$tmpUserId] = true;
                $userIds[] = $tmpUserId;
            }
        }

        // 2.获取关注了该用户的粉丝
        $userFollowDB = new UserFollowDB();
        $followerIds = $userFollowDB->getUserFollow($userId);

        // 将followerIds 和 $userIds 合并
        foreach ($followerIds as $followerId) {
            if (!isset($isAppend[$followerId])) {
                $userIds[] = $followerId;
            }
        }


        if (count($userIds) > 0) {
            // 订阅的人数大于0，添加推送任务
            $pushInfoDB = new CDBPushInformation();
            $pushInfoDB->setPushInfoTask($userIds, $informationId, Util::getCurrentTime(), $userId);
        }
        $data['informationId'] = $informationId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加失败');
        exit;
    }

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
