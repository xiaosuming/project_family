<?php
/**
 * 添加资讯分享
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午5:51
 */
use DB\CDBInformation;
use Util\Check;
use Util\Util;

$infoId = Check::checkInteger($params['infoId'] ?? '');

$userId = $GLOBALS['userId'];
try {
    $infoDB = new CDBInformation();

    $shareId = $infoDB->addInfoShare($userId, $infoId);
    $data['shareId'] = $shareId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
