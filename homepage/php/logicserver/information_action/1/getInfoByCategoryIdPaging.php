<?php
/**
 * 根据分类id分页获取资讯列表
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午2:37
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$categoryId = Check::checkInteger($params['categoryId'] ?? ''); //分类id
$pageIndex = Check::checkIntegerUnsigned($params['pageIndex'] ?? 1);
$pageSize = Check::checkIntegerUnsigned($params['pageSize'] ?? 10);
$userId = $GLOBALS['userId'] ?? 0;
try {
    $infoDB = new CDBInformation();
    $list = $infoDB->getInformationListByCategoryIdWithPage($userId, $categoryId, $pageIndex, $pageSize);
    foreach ($list as $k => $v) {
        if ($v['isLike'] === null) {
            $list[$k]['isLike'] = false;
        } else {
            $list[$k]['isLike'] = true;
        }
    }

    $data['page'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
