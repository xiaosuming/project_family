<?php
/**
 * 添加资讯资源
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-31
 * Time: 上午10:31
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$infoId = Check::checkInteger($params['infoId'] ?? ''); //资讯id
$remark = Check::check($params['remark'] ?? ''); //资源备注
$remark = Util::filterContentReplace($remark);
$url = Check::check($params['url'] ?? '');
$type = Check::checkInteger($params['type'] ?? ''); //资源类型，1是图片，2是视频，3是音频
$userId = $GLOBALS['userId'];
try {
    $infoDB = new CDBInformation();
    if (!$infoDB->checkInformationPermissionWithUserId($infoId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }
    $infoSourceId = $infoDB->addInformationResource($userId, $infoId, $remark, $url, $type);
    $data['infoResourceId'] = $infoSourceId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
