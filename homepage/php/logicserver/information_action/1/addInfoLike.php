<?php
/**
 * 添加用户收藏
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午4:35
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$infoId = Check::checkInteger($params['infoId'] ?? '');

$userId = $GLOBALS['userId'];
try {
    $infoDB = new CDBInformation();
    if ($infoDB->checkUserInfoLikeExist($userId, $infoId)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '已经点过赞了');
        exit;
    }
    $likeId = $infoDB->addInfoLike($userId, $infoId);
    $data['likeId'] = $likeId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
