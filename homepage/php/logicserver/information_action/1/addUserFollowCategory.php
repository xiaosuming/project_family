<?php
/**
 * 添加用户关注分类
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午2:58
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$categoryId = Check::checkInteger($params['categoryId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $infoDB = new CDBInformation();

    $categoryDetail = $infoDB->getInfoCategoryDetail($categoryId);
    $circleType = $categoryDetail['circleType'];
    if ($circleType == 3) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '是族群的圈子，用户不可主动关注');
        exit;
    }

    if ($infoDB->checkUserFollowExist($userId,$categoryId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],'该分类已经关注过了');
        exit;
    }
    $followId = $infoDB->addUserFollowCategory($userId, $categoryId);
    $data['followId'] = $followId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
