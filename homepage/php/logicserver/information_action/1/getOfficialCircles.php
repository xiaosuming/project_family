<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-15
 * Time: 下午1:00
 */

use DB\CDBInformation;
use Util\Util;

try {

    $informationDB = new CDBInformation();
    $list = $informationDB->getOfficialInfoCategoryList();
    $data['list'] = $list;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    \Util\SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
