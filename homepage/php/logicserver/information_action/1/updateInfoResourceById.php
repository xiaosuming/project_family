<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-11-27
 * Time: 下午2:34
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$resourceId = Check::checkInteger($params['resourceId'] ?? ''); //资讯id
$remark = Check::check($params['remark'] ?? ''); //资源备注
$remark = Util::filterContentReplace($remark);
$url = Check::check($params['url'] ?? '');
$type = Check::checkInteger($params['type'] ?? ''); //资源类型，1是图片，2是视频，3是音频
$userId = $GLOBALS['userId'];
try {
    $infoDB = new CDBInformation();
    $result = $infoDB->getInfoIdByResourceId($resourceId);

    if ($result == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '查询错误');
        exit;
    }

    $infoId = $result['infoId'];

    if (!$infoDB->checkInformationPermissionWithUserId($infoId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }
    $updateRow = $infoDB->updateResourceById($resourceId, $remark, $url, $type, $userId);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
