<?php
/**
 * 分页获取用户订阅的资讯
 * @Author: jiangpengfei
 * @Date:  2018-11-26
 */

use Util\Util;
use DB\CDBInformation;
use DB\CDBPushInformation;
use Util\Check;

$pageIndex = Check::checkInteger($params['pageIndex'] ?? 1);
$pageSize = Check::checkInteger($params['pageSize'] ?? 10);
$userId = $GLOBALS['userId'];

try {
    $pushInfoDB = new CDBPushInformation();
    $infoIds = $pushInfoDB->getTimeline($userId, $pageIndex, $pageSize);

    $infoDB = new CDBInformation();
    $infos = $infoDB->getInfoByInfoIds($infoIds);

    foreach ($infos as $k => $v) {
        if ($v['isLike'] === null) {
            $infos[$k]['isLike'] = false;
        } else {
            $infos[$k]['isLike'] = true;
        }
    }

    $data['list'] = $infos;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
