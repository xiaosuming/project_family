<?php
/**
 * 审核资讯信息
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 上午11:30
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$informationId = Check::checkInteger($params['informationId'] ?? '');

$isAccept = Check::checkInteger($params['isAccept'] ?? 1); //1：通过  -1：未通过
$userId = $GLOBALS['userId'];
try {

    $infoDB = new CDBInformation();

    if (!$infoDB->isSystemUser($userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '不是系统用户，权限错误');
        exit;
    }
    $informationDetail = $infoDB->getInformationById($informationId);
    if ($informationDetail == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '查询出错');
        exit;
    }

    $categoryId = $informationDetail['categoryId'];

    if ($isAccept == 1) {
        $updateRow = $infoDB->acceptForInformation($informationId, $categoryId);
    }
    if ($isAccept == -1) {
        $updateRow = $infoDB->notAcceptForInformation($informationId);
    }
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
