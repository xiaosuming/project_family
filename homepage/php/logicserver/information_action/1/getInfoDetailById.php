<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-30
 * Time: 下午2:49
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$informationId = Check::checkInteger($params['informationId'] ?? '');

try {
    $infoDB = new CDBInformation();
    $detail = $infoDB->getInformationById($informationId);
    $resourceList = $infoDB->getResourceListByInfoId($informationId);
    $detail['sourceList'] = $resourceList;
    $data['detail'] = $detail;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
