<?php
/**
 * 获取资讯的分享列表
 * @Author: jiangpengfei
 * @Date: 2018-12-12
 */

use DB\CDBInformation;
use Util\Check;
use Util\Util;

$informationId = Check::checkInteger($params['informationId'] ?? '');

try {
    $infoDB = new CDBInformation();
    $shareList = $infoDB->getInfoShareList($informationId);

    $data['list'] = $shareList;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
