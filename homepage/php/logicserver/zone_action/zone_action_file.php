<?php
    /**
     * 空间操作索引文件
     */

    /**
     * whiteList是白名单，用来允许某些请求通过验证
     */
    $version = $params['version'] ?? "1";

    require_once("filter/filter.php");
    $zone_action_file_array['addZone'] = "logicserver/zone_action/{$version}/addZone.php";
    $zone_action_file_array['updateZone'] = "logicserver/zone_action/{$version}/updateZone.php";
    $zone_action_file_array['deleteZone'] = "logicserver/zone_action/{$version}/deleteZone.php";
    $zone_action_file_array['getZoneByUserId'] = "logicserver/zone_action/{$version}/getZoneByUserId.php";
    $zone_action_file_array['getZoneByZoneId'] = "logicserver/zone_action/{$version}/getZoneByZoneId.php";
    $zone_action_file_array['isZoneExist'] = "logicserver/zone_action/{$version}/isZoneExist.php";
    $zone_action_file_array['addPost'] = "logicserver/zone_action/{$version}/addPost.php";
    $zone_action_file_array['updatePost'] = "logicserver/zone_action/{$version}/updatePost.php";
    $zone_action_file_array['deletePost'] = "logicserver/zone_action/{$version}/deletePost.php";
    $zone_action_file_array['getPostsPaging'] = "logicserver/zone_action/{$version}/getPostsPaging.php";
    $zone_action_file_array['getRelatePostsPaging'] = "logicserver/zone_action/{$version}/getRelatePostsPaging.php";
    $zone_action_file_array['addComment'] = "logicserver/zone_action/{$version}/addComment.php";
    $zone_action_file_array['deleteComment'] = "logicserver/zone_action/{$version}/deleteComment.php";
    $zone_action_file_array['getComment'] = "logicserver/zone_action/{$version}/getComment.php";
    $zone_action_file_array['getPost'] = "logicserver/zone_action/{$version}/getPost.php";
    $zone_action_file_array['getAllReplyComment'] = "logicserver/zone_action/{$version}/getAllReplyComment.php";
    $zone_action_file_array['addReply'] = "logicserver/zone_action/{$version}/addReply.php";
    $zone_action_file_array['getPostCountByTimestamp'] = "logicserver/zone_action/{$version}/getPostCountByTimestamp.php";
    $zone_action_file_array['pushWikiPostsByTimestamp'] = "logicserver/zone_action/{$version}/pushWikiPostsByTimestamp.php";
    $zone_action_file_array['addBlockUser'] = "logicserver/zone_action/{$version}/addBlockUser.php";
    $zone_action_file_array['deleteBlockUser'] = "logicserver/zone_action/{$version}/deleteBlockUser.php";
    $zone_action_file_array['getUserBlackList'] = "logicserver/zone_action/{$version}/getUserBlackList.php";
    $zone_action_file_array['getFamilyPostPaging'] = "logicserver/zone_action/{$version}/getFamilyPostPaging.php";
    