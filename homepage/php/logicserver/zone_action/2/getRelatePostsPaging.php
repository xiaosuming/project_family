<?php	
    use Util\Util;
    use Util\Pager;
    use DB\CDBZone;
    use DB\CDBFamily;
    use Util\Check;
    use Util\Paginator;

    $userId = $GLOBALS['userId'];
    $maxId = Check::checkInteger(trim(isset($params['maxId']) ? $params['maxId'] : 0));		        // 最大的id
    $sinceId = Check::checkInteger(trim(isset($params['sinceId']) ? $params['sinceId'] : 0));		// 起始的id
    $maxTimeStamp = $params['maxTimeStamp'] ?? '0';
    $sinceTimeStamp = $params['sinceTimeStamp'] ?? '0';

    $count = Check::checkInteger(trim($params['count'] ?? 10));     // 默认是10条数据
    $type = Check::checkInteger(trim(isset($params['type']) ? $params['type'] : 1));           //0不看系统生成,1看系统生成

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    try{
        $familyDB = new CDBFamily();
        //先检查这个人有没有家族
        $familys = $familyDB->getJoinFamily($userId);

        $zoneDB = new CDBZone();

        $blackList = $zoneDB->getUserBlackList($userId);
        $blackMap = array();    //构建黑名单Map
        foreach($blackList as $block){
            $blackMap[$block['block_userId']] = 1;
        }

        $zone = $zoneDB->getZoneByUserId($userId);

        $total = $zoneDB->getRelatePostsPagingTotal($userId, $type);

        $isFinish = false;
        $nextSinceTimeStamp = $sinceTimeStamp;
        $nextMaxId = $maxId;
        $nextMaxTimeStamp = $maxTimeStamp;
        $finalPosts = [];
        $lastPage = false;
        $isSinceIdEdit = false;

        while(!$isFinish) {

            list($timeline, $posts, $endIndex) = 
            $zoneDB->getRelatePostsPagingByMaxIdAndSinceId($userId, $nextMaxId, $sinceId, 
            $count * 2, $nextMaxTimeStamp, $sinceTimeStamp, $type);

            // var_dump($timeline);
            // var_dump($posts);
            // var_dump($endIndex);

            // return;

            $len = count($posts);
            
            if ($len == 0) {
                $lastPage = true;
                break;
            }

            if (!$isSinceIdEdit) {
                // 如果nextSinceId还没有修改
                $nextSinceId = $posts[0]['id'];
                $nextSinceTimeStamp = $timeline[$nextSinceId];

                $isSinceIdEdit = true;
                $sinceId = 0;       // 防止下一次循环sinceId生效
                $sinceTimeStamp = 0; // 防止下一次循环sinceTimeStamp生效
            }

            $nextMaxId = $posts[$len - 1]['id'];
            $nextMaxTimeStamp = $timeline[$nextMaxId];

            for($i = 0; $i < $len; $i++){
                if(isset($blackMap[$posts[$i]['userId']])){
                    //在黑名单里
                    unset($posts[$i]);
                }else{
                    $posts[$i]['photo'] = json_decode($posts[$i]['photo'],true);
                    $finalPosts[] = $posts[$i];
                    if (count($finalPosts) == $count) {
                        $nextMaxId = $posts[$i]['id'];
                        $nextMaxTimeStamp = $timeline[$nextMaxId];
                        break;  // 推文数量已经够了
                    }
                }
            }

            $lastPage = false;
            if (count($finalPosts) == $count) {
                $isFinish = true;
                // 判断是不是最后一页
                if ($endIndex - $count * 2 + $i + 1 >= $total) {
                    $lastPage = true;
                }
            } else {
                // 还没有获取完所需数量的推文

                // 判断是不是最后一页
                if ($endIndex + 1 >= $total) {
                    $lastPage = true;
                    $isFinish = true;
                }
            }
        }

        $paginator = new Paginator($total, $finalPosts, $nextMaxId, $nextSinceId, $lastPage, $nextMaxTimeStamp, $nextSinceTimeStamp);
        $paginator->printPage();

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }