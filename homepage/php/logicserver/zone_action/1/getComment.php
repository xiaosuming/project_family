<?php
    use Util\Util;
    use DB\CDBZone;
    use Util\Check;

    $postId = Check::checkInteger(trim(isset($params['postId']) ? $params['postId'] : ''));				//推文id
    $module = Check::checkModule($params['module'] ?? '5');     // 默认是推文

    if($postId == "" ){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    try{
        $zoneDB = new CDBZone();
        $userId = $GLOBALS['userId'];
    
        $comments = $zoneDB->getPostCommentsTotal($postId, $module);
        
        $data['comments'] = $comments;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);


    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }