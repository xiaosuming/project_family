<?php
    use Util\Util;
    use DB\CDBZone;
    use Util\Check;
    use DB\CDBFamilyNews;
    //addZone($userId,$content,$createTime,$createBy,$updateTime,$updateBy)
    
    $content = Check::check(trim(isset($params['content']) ? $params['content'] : ''));				//必须

    try{
        if($content!=""){
            $zoneDB = new CDBZone();
            $userId = $GLOBALS['userId'];
            $createTime = Util::getCurrentTime();
            $createBy = $userId;
            $updateTime = Util::getCurrentTime();
            $updateBy = $userId;
        
            if($zoneDB->isZoneExist($userId)){
                Util::printResult($GLOBALS['ERROR_ZONE_EXIST'], "已经存在空间，无法创建");
                exit;
            }
        
            $id = $zoneDB->addZone($userId,$content,$createTime,$createBy,$updateTime,$updateBy);
            $data['addedZoneId'] = $id;
            if($id > 0){
                $familyNewsDB = new CDBFamilyNews();
                $familyNewsDB->addMultiFamilyNews($insertValue);
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "addZone缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }