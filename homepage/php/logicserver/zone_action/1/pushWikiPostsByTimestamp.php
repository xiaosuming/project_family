<?php
    use Util\Util;
    use DB\CDBPost;
    use Util\Check;

    $userId = $GLOBALS['userId'];
    $pageSize = Check::checkInteger(trim(isset($params['size']) ? $params['size'] : 10));		//页面大小
    $timestamp = Check::check(trim($params['timestamp'] ?? '0'));

    $postDB = new CDBPost();
    list($postIdCount,$maxTimestamp) = $postDB->pushPostWikiByTimestamp($userId,$timestamp,$pageSize);

    $data['count'] = $postIdCount;
    $data['maxTimestamp'] = $maxTimestamp;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    
