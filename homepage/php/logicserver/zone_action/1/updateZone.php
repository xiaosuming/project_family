<?php
    use Util\Util;
    use DB\CDBZone;
    use Util\Check;

    $zoneId = Check::checkInteger(trim(isset($params['zoneId']) ? $params['zoneId'] : ''));				//必须
    $content = Check::check(trim(isset($params['content']) ? $params['content'] : ''));					//空间内容
    $canShare = Check::checkInteger(trim(isset($params['canShare']) ? $params['canShare'] : '0'));			//是否刻意分享,默认不能分享

    try{
        if($zoneId!=""&&$content!=""){
            
            $zoneDB = new CDBZone();
            $userId = $GLOBALS['userId'];
            $updateTime = Util::getCurrentTime();
            $updateBy = $userId;
            //检查操作权限
            if(!$zoneDB->verifyZoneIdAndUserId($zoneId,$userId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }
            
            $rows = $zoneDB->updateZone($zoneId, $content,$canShare,$updateBy);
            $data['updateZones'] = $rows;
            if($rows > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], null);
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }