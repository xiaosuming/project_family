<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/12/19
 * Time: 10:00
 */
use Util\Util;
use DB\CDBZone;
use DB\CDBAccount;
use Util\Check;
use Model\Post;
$timestamp = Check::checkInteger(trim($params['timestamp'] ?? 0 ));
$userId = $GLOBALS['userId'];
try{
	if($timestamp > 0){
		$zoneDB = new CDBZone();
		$endTimestamp = time();
		$data['number']= $zoneDB->getPostCountByTimestamp($userId,$timestamp,$endTimestamp);
		Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
	}
	else{
		Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
		exit;
	}
}catch (PDOException $e){
	$logger->error(Util::exceptionFormat($e));
	Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}