<?php
    use Util\Util;
    use DB\CDBZone;
    use DB\CDBAccount;
    use DB\CDBMessage;
    use Util\Check;
    use Model\Post;
    use Model\Message;

    $content = Check::check(trim(isset($params['content']) ? $params['content'] : ''),1,255);				//必须
    $address = Check::check(trim(isset($params['address']) ? $params['address'] : ''),0,100);
    $photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));
    $familyId = Check::checkInteger(trim($params['familyId'] ?? 0 ));
    $coorX = Check::check(trim(isset($params['coorX']) ? $params['coorX'] : ''));
    $coorY = Check::check(trim(isset($params['coorY']) ? $params['coorY'] : ''));
    $all = Check::checkInteger(trim($params['all'] ?? 0));      //是否向所有人推送，0是三代以内，1是所有人

    try{
        if($content!=""){
            $zoneDB = new CDBZone();
            $userId = $GLOBALS['userId'];
            $zone = $zoneDB->getZoneByUserId($userId);
            $zoneId = 0;
            if($zone == null){
                $zoneId  = $zoneDB->addZone($userId,"");
            }else{
                $zoneId= $zone->id;
            }

            $pathJsonStr = "";
            if($photo != ""){
                //这里将photo解析成image数组
                $pathArray = explode(",",$photo);
                if(count($pathArray) > 9){
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"图片数量超过限制");
                    exit();
                }
                $pathJsonStr = json_encode($pathArray);
            }else{
                $pathJsonStr = json_encode(array());
            }

            $post = new Post();
            $post->zoneId = $zoneId;
            $post->userId = $userId;
            $post->familyId = $familyId;
            $post->content = $content;
            $post->address = $address;
            $post->photo = $pathJsonStr;
            $post->coorX = $coorX;
            $post->coorY = $coorY;

            $accountDB = new CDBAccount();
            $user = $accountDB->getUserInfo($userId);

                /* 系统普通用户 */
            if($user['user_type'] == 1){
                $post->type = 1;        //系统推文 1是用户发表，2是活动,3是系统推送
                $postId = $zoneDB->addPost($post,$familyId,$all);

                $data['addedPostId'] = $postId;
            }else if($user['user_type'] == 2){
                /* 系统推送用户 */
                $post->type = 3;
                $postId = $zoneDB->addSystemPost($post);
                $data['addedPostId'] = $postId;
            }

            if($postId > 0){
                $atUsers = Util::getAtUser($content);   //获取推文中at的用户
                if(count($atUsers) > 0){
                    //发送message
                    $messageDB = new CDBMessage();

                    foreach($atUsers as $atUserId => $atUserNickname){
                        $msg = new Message();
                        $msg->fromUser = $userId;		                //发件人id
                        $msg->toUser = $atUserId;				        //接收人id
                        $msg->content = '有人在推文中提到了你';			    //内容
                        $msg->module = $GLOBALS['POST_MODULE'];		    //模块
                        $msg->action = $GLOBALS['AT_USER'];				//动作
                        $msg->recordId = $postId;				        //相关记录id
                        $msg->createBy = $userId;               //创建人
                        $msg->updateBy = $userId;               //更新人
                        $messageDB->addMessage($msg);
                    }
                }

                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "数据插入错误");
            }

        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "addPost缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
