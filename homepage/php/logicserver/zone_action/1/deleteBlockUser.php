<?php
    /**
     * 删除要屏蔽的用户
     * @author jiangpengfei
     * @date 2017-02-07
     */
    use Util\Util;
    use DB\CDBZone;
    use Util\Check;
    use DB\CDBAccount;
    use ThirdParty\InstantMessage;
    
    $blockId = Check::checkInteger(trim(isset($params['blockId']) ? $params['blockId'] : ''));				//黑名单id
    $userId = $GLOBALS['userId'];

    if($blockId == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }	

    try{

        $zoneDB = new CDBZone();

        $block = $zoneDB->getBlockByBlockId($blockId);

        if($block == null){
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "该黑名单不存在");
            exit;
        }

        $accountDB = new CDBAccount();
        $ownerUser = $accountDB->getHuanxinInfoById($block['userId']);
        $blockUser = $accountDB->getHuanxinInfoById($block['block_userId']);

        if($ownerUser == null || $blockUser == null){
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "参数错误");
            exit();
        }

        //操作环信删除黑名单
        $im = new InstantMessage();
        $blockResult = $im->deleteBlockUser($ownerUser['hxUsername'],$blockUser['hxUsername']);

        if(!$blockResult){
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "删除黑名单出错，请重试");
            exit();
        }

        $deleteRow = $zoneDB->deleteUserFromBlackList($blockId);
        $data['deleteRow'] = $deleteRow;
        if($deleteRow > 0){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }