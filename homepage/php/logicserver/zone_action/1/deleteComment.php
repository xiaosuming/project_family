<?php
use DB\CDBZone;
use Util\Check;
use Util\Util;
$commentId = Check::checkInteger(trim(isset($params['commentId']) ? $params['commentId'] : '')); //评论id

try {

    $zoneDB = new CDBZone();
    $userId = $GLOBALS['userId'];

    //检查操作权限
    if (!$zoneDB->verifyCommentIdAndUserId($commentId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }

    $rows = $zoneDB->deletePostComment($commentId);
    $data['deleteComments'] = $rows;
    if ($rows > 0) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], null);
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
