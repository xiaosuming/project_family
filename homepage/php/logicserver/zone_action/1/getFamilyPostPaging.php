<?php

/**
 * 分页获取族群中的推文
 * @Author: jiangpengfei
 * @Date:   2018-11-23
 */

use Util\Util;
use DB\CDBZone;
use DB\CDBFamily;
use Util\Check;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$pageIndex = Check::checkInteger($params['pageIndex'] ?? 1);
$pageSize = Check::checkInteger($params['pageSize'] ?? 10);

$userId = $GLOBALS['userId'];

try {
    // 检查用户和族群的权限
    $familyDB = new CDBFamily();
    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '用户权限不足');
        exit;
    }

    $zoneDB = new CDBZone();
    $paging = $zoneDB->getFamilyPostsPaging($familyId, $pageIndex, $pageSize, $userId);

    $data['paging'] = $paging;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}