<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/9/18 0018
 * Time: 12:17
 */

use Util\Check;
use Util\Util;
use DB\CDBZone;

$commentId=check::checkInteger(trim(isset($params['commentId']) ? $params['commentId'] : ''));
//$replyTo=check::checkInteger(trim(isset($params['replyTo']) ? $params['replyTo'] : ''));

if($commentId == "" ){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $zoneDB=new CDBZone();
    $comments = $zoneDB->getAllReplyComment($commentId);
    $data['comments'] = $comments;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);


}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
