<?php
    /**
     * 添加要屏蔽的用户
     * @author jiangpengfei
     * @date 2017-02-07
     */
    use Util\Util;
    use DB\CDBZone;
    use DB\CDBAccount;
    use Util\Check;
    use ThirdParty\InstantMessage;

    $blockUserId = Check::checkInteger(trim(isset($params['blockUserId']) ? $params['blockUserId'] : ''));				//要屏蔽的用户id
    $userId = $GLOBALS['userId'];

    if($blockUserId == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }	

    try{

        $zoneDB = new CDBZone();

        if($zoneDB->checkUserInBlackList($userId,$blockUserId)){
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "用户已经在黑名单中");
            exit();
        }

        $accountDB = new CDBAccount();
        $ownerUser = $accountDB->getHuanxinInfoById($userId);
        $blockUser = $accountDB->getHuanxinInfoById($blockUserId);

        if($ownerUser == null || $blockUser == null){
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "参数错误");
            exit();
        }
        //操作环信加黑名单
        $im = new InstantMessage();
        $blockResult = $im->blockUser($ownerUser['hxUsername'],array($blockUser['hxUsername']));

        if(!$blockResult){
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "加入黑名单出错，请重试");
            exit();
        }

        $blockId = $zoneDB->addUserToBlackList($userId,$blockUserId);
        $data['blockId'] = $blockId;
        if($blockId > 0){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "添加失败");
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }