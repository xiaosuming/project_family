<?php	
    use Util\Util;
    use DB\CDBZone;	
    use Util\Check;

    $zoneId = Check::checkInteger(trim(isset($params['zoneId']) ? $params['zoneId'] : ''));				//必须

    try{
        if($zoneId!=""){
            $zoneDB = new CDBZone();
            $userId = $GLOBALS['userId'];
            //检查操作权限
            if(!$zoneDB->verifyZoneIdAndUserId($zoneId,$userId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }
            
            $rows = $zoneDB->deleteZone($zoneId);
            $data['deleteZones'] = $rows;
            if($rows > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_DELETE'], null);
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }