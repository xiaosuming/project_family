<?php
    /**
     * 获取用户的黑名单
     * @author jiangpengfei
     * @date 2017-02-07
     */
    use Util\Util;
    use DB\CDBZone;
    use Util\Check;
    
    $userId = $GLOBALS['userId'];
    try{

        $zoneDB = new CDBZone();
        $blackList = $zoneDB->getUserBlackList($userId);
        $data['blackList'] = $blackList;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }