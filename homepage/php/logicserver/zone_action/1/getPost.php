<?php

use Util\Util;
use DB\CDBZone;
use Util\Check;

$postId = Check::checkInteger(trim(isset($params['postId']) ? $params['postId'] : ''));                //推文id

if ($postId == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {
    $zoneDB = new CDBZone();

    $post = $zoneDB->getPost($postId);
    if ($post == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '查询出现错误');
        exit;
    }
    $data['post'] = $post;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}