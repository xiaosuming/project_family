<?php	
    use Util\Util;
    use Util\Pager;
    use DB\CDBZone;
    use DB\CDBFamily;
    use Util\Check;

    $userId = $GLOBALS['userId'];
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));		//页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 10));		//页面大小
    $type = Check::checkInteger(trim(isset($params['type']) ? $params['type'] : 1));           //0不看系统生成,1看系统生成

    try{
        if($pageIndex != "" && $pageSize != ""){
            $posts = array();
            $count = 0;
            $familyDB = new CDBFamily();
            //先检查这个人有没有家族
            $familys = $familyDB->getJoinFamily($userId);

            $zoneDB = new CDBZone();

            $blackList = $zoneDB->getUserBlackList($userId);
            $blackMap = array();    //构建黑名单Map
            foreach($blackList as $block){
                $blackMap[$block['block_userId']] = 1;
            }

            $zone = $zoneDB->getZoneByUserId($userId);


            $count = $zoneDB->getRelatePostsPagingTotal($userId,$type);

            
            $posts = $zoneDB->getRelatePostsPaging($userId, $pageIndex, $pageSize,$type);
               
            $len = count($posts);

            for($i = 0; $i < $len; $i++){
                if(isset($blackMap[$posts[$i]['userId']])){
                    //在黑名单里
                    unset($posts[$i]);
                }else{
                    $posts[$i]['photo'] = json_decode($posts[$i]['photo'],true);
                }
            }

            $page = new Pager($count,array_values($posts),$pageIndex,$pageSize);
            $page->printPage();
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }