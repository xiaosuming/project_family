<?php	
    use Util\Util;
    use Util\Pager;
    use DB\CDBZone;
    use Util\Check;

    $zoneId = Check::check(trim(isset($params['zoneId']) ? $params['zoneId'] : ''));			//非必须，空间id，不传则表示获取自己空间的推文
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));	//页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 10));		//页面大小

    try{
        $zoneDB = new CDBZone();
        $userId = $GLOBALS['userId'];

        if($zoneId == ""){
            $zone = $zoneDB->getZoneByUserId($userId);
            if($zone === null){
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "空间不存在");
                exit();
            }
            $zoneId = $zone->id;
        }

        $posts = $zoneDB->getZonePostsPaging($zoneId, $pageIndex, $pageSize);
        $count = $zoneDB->getZonePostsTotalCount($zoneId);
        
        foreach($posts as $key => $post){
            $posts[$key]['photo'] = json_decode($posts[$key]['photo'],true);
        }


        $page = new Pager($count,$posts,$pageIndex,$pageSize);
        $page->printPage();
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }