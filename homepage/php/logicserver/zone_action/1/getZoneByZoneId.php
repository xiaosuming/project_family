<?php	
    use Util\Util;
    use DB\CDBZone;
    use Util\Check;
    
    $zoneId = Check::checkInteger(trim(isset($params['zoneId']) ? $params['zoneId'] : ''));

    try{
        if($zoneId != ""){
            $zoneDB = new CDBZone();
            $zone = $zoneDB->getZoneByZoneId($zoneId);
            $data['zone'] = $zone;
            if(count($zone) > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], null);
            }
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }