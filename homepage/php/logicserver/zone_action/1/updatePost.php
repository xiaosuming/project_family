<?php

use Util\Util;
use DB\CDBZone;
use Util\Check;

$postId = Check::checkInteger(trim(isset($params['postId']) ? $params['postId'] : ''));                    //必须，推文id
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''));                        //必须，推文内容
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));                                    //发布地址
$coorX = Check::check(trim(isset($params['coorX']) ? $params['coorX'] : ''));
$coorY = Check::check(trim(isset($params['coorY']) ? $params['coorY'] : ''));
try {

    if ($postId != "" && $content != "") {

        $zoneDB = new CDBZone();
        $userId = $GLOBALS['userId'];

        if (!$zoneDB->verifyPostIdAndUserId($postId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $updateTime = Util::getCurrentTime();
        $updateBy = $userId;

        $rows = $zoneDB->updatePost($postId, $content, $coorX, $coorY, $address, $updateTime, $updateBy);
        $data['updatedPosts'] = $rows;
        if ($postId > 0) {
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}