<?php
    use Util\Util;
    use DB\CDBZone;
    use DB\CDBMessage;
    use DB\CDBAlbum;
    use Util\Check;
    use Model\Message;
    use ThirdParty\MiniProgram;
    //addPostComment($postId,$userId,$replyTo,$comment,$createTime,$createBy,$updateTime,$updateBy)

    $postId = Check::checkInteger(trim(isset($params['postId']) ? $params['postId'] : ''));				//推文id,相片id
    $replyTo = Check::checkInteger(trim(isset($params['replyTo']) ? $params['replyTo'] : ''));			//回复的评论id
    $comment = Check::check(trim(isset($params['comment']) ? $params['comment'] : ''),1,255);			        //评论内容
    $replyToUserId = Check::checkInteger(trim($params['replyToUserId'] ?? ''));                         //回复的用户Id
    $module = Check::checkModule($params['module'] ?? '5');      // 模块id

    $type = Check::checkInteger($params['type'] ?? 0);
    if ($type != 0){
        $wxApp = new MiniProgram($type);
        $checkMsg = $wxApp->msgSecCheck([$comment]);
        if (!$checkMsg){
            Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
            exit();
        }
    }

    if ($module != 13 && $module != 5) {
        // 13是相册，5是推文
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "模块错误");
        exit;
    }

    if($postId == "" || $replyTo == "" || $comment == "" || $replyToUserId == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    try{
        $zoneDB = new CDBZone();
        $userId = $GLOBALS['userId'];

        $commentId = $zoneDB->addPostComment($postId, $userId, $replyTo,$replyToUserId, $comment, $module);

        $data['addedCommentId'] = $commentId;
        if($commentId > 0){
            $messageContent = '';
            $toUserId = 0;
            switch($module) {
                case 5:
                    $post = $zoneDB->getPost($postId);
                    $messageContent = $post->content;
                    $toUserId = $post->userId;
                    break;
                case 13:        // 相片评论不推送消息
                    $albumDB = new CDBAlbum();
                    $photo = $albumDB->getPhotoById($postId);
                    $messageContent = '照片评论';
                    $toUserId = $photo->createBy;
                    break;
            }

            //如果不是评论自己的推文,则发消息
            if($module == 5 && $toUserId != $userId){
                $messageDB = new CDBMessage();
                $message = new Message();
                $message->fromUser = $userId;
                $message->toUser = $toUserId;
                $message->content = $messageContent;
                $message->module = $GLOBALS['COMMENT_MODULE'];
                $message->recordId = $postId;
                $messageDB->addMessage($message);
            }
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
