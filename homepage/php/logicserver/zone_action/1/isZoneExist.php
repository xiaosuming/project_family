<?php	
    use Util\Util;
    use DB\CDBZone;
    
    try{
        $zoneDB = new CDBZone();
        
        $userId = $GLOBALS['userId'];

        $data['exist'] = $zoneDB->isZoneExist($userId);
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }