<?php	
    use Util\Util;
    use DB\CDBZone;
    use Util\Check;


    $userId = Check::checkInteger(isset($params['userId']) ? $params['userId'] : '',false);

    if($userId === ""){
        $userId = $GLOBALS['userId'];
    }

    try{
        $zoneDB = new CDBZone();

        $zone = $zoneDB->getZoneByUserId($userId);
        $data['zone'] = $zone;
        if($zone != null){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], null);
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }