<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/19 0019
 * Time: 13:33
 */
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $area_action_file_array['getArea'] = "logicserver/area_action/{$version}/getArea.php";
    $area_action_file_array['getAreaNameById'] = "logicserver/area_action/{$version}/getAreaNameById.php";