<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/18 0018
 * Time: 15:43
 */
//获得省市级地址

use DB\CDBArea;
use Util\Check;
use Util\Util;

$areaId = Check::checkInteger(trim(isset($params['areaId']) ? $params['areaId'] : ''), 'areaId',false); //区域Id

try {

    $CDBArea = new CDBArea();

    if ($areaId == "") {
        //获得一级列表
        $result = $CDBArea->getFirstArea();
        if (!$result) {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '查询出错');
            exit;
        }
        $data['areaInfo']=$result;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }

    $result = $CDBArea->getOtherArea($areaId);

    $data['areaInfo']=$result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
    exit;

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}


