<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/30 0030
 * Time: 16:01
 */

use DB\CDBArea;
use DB\CDBFamily;
use Util\Check;
use Util\Util;

$areaId = check::checkInteger(trim(isset($params['areaId']) ? $params['areaId'] : ''));

if ($areaId == ''){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'],'参数缺失');
    exit;
}

try{
    $CDBArea=new CDBArea();
    $result=$CDBArea->getAreaNameById($areaId);

    $data['areaInfo']=$result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
    exit;
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
