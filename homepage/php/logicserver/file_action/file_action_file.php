<?php
    $version = $params['version'] ?? "1";
    $whiteList['getRandomPhoto'] = 'allow';
    require_once("filter/filter.php");
    $file_action_file_array['addFile'] = "logicserver/file_action/{$version}/addImage.php";
    $file_action_file_array['addMultiFile'] = "logicserver/file_action/{$version}/addMultiImage.php";
    $file_action_file_array['getRandomPhoto'] = "logicserver/file_action/{$version}/getRandomPhoto.php";
    $file_action_file_array['addImageForAlbum'] = "logicserver/file_action/{$version}/addImageForAlbum.php";
    $file_action_file_array['getFilesPaging'] = "logicserver/file_action/{$version}/getFilesPaging.php";
    $file_action_file_array['addFileForWxApp'] = "logicserver/file_action/{$version}/addImageForWxApp.php";
