<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/9
 * Time: 14:01
 */

    // require_once($_SERVER['DOCUMENT_ROOT']."/php/config/ip_map.php");
	use Util\Storage\StorageFacadeClient;
	use Util\Storage\StorageBucket;
    use Util\Util;
    use DB\CDBFile;
    use Util\Check;

    $file = Check::checkImageType(isset($_FILES['file']) ? $_FILES['file'] : '');                                  //上传的文件
    $module = Check::checkModule(trim(isset($params['module']) ? $params['module'] : ''));	//模块

    $userId = $GLOBALS['userId'] ?? 0;
    try{
		if($file != ''){
			$client = new StorageFacadeClient();
			$client->setBucket(StorageBucket::$IMAGE);
			$strArray = explode(".",$file['name']);

			if(count($strArray) - 1 >= 0){
				//获取后缀
				$ext_name = $strArray[count($strArray) - 1];
				$fileInfo = null;
				$imageinfo = array();
				//根据文件名上传
				$fileInfo = $client->upload($file['tmp_name'],strtolower($ext_name));

				if($fileInfo->success){
					$path = $fileInfo->location;
					$fileDB = new CDBFile();
					//保存到数据库中作为文件记录
					$fileId = $fileDB->addFile($path,$module,$userId);
					if($fileId > 0){
						$data['location'] = $path;
						$data['size']=ceil($file['size']/1024);
						Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
					}else{
						Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
					}
				}else{

					Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'],"图片上传错误");
					exit();
				}
			}else{
				Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'],"图片上传错误,暂时不支持无后缀名上传");
				exit();
			}
		}else{
			Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'],"图片不存在");
			exit();
		}
	}catch(\Exception $e){
		$logger->error(Util::exceptionFormat($e));
		Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
	}catch(PDOException $e){
		//异常处理
		$logger->error(Util::exceptionFormat($e));
		Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
	}

