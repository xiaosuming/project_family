<?php
    /**
     * 获取分页的文件
     */	
    use Util\Util;
    use Util\Pager;
    use DB\CDBFile;
    use Util\Check;

    $userId = $GLOBALS['userId'];													//操作用户id
    
    $module = Check::checkModule(trim(isset($params['module']) ? $params['module'] : -1));		        //模块id
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));		//页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 6));		    //每页数量

    try{
        $fileDB = new CDBFile();		
        $total = $fileDB->getFilesCount($module,$userId);
        $files = $fileDB->getFilesPaging($pageIndex,$pageSize,$module,$userId);

        $pager = new Pager($total,$files,$pageIndex,$pageSize);
        $pager->printPage();		//以分页的格式输出
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
