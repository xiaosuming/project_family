<?php
    use Util\Util;
    use Util\PhotoGenerator;

    $type = isset($params['type']) ? $params['type'] : 0;   //获取请求的头像类型，0是family 1是person,2是activity

    $location = "";

    if($type == 0){
        $location = PhotoGenerator::generateFamilyPhotoFromResource();
    }else if($type == 1){
        $location = PhotoGenerator::generatePersonPhotoFromResource();
    }else if($type == 2){
        $location = PhotoGenerator::generateActivityPhotoFromResource();
    }else{
        $location = PhotoGenerator::generateFamilyPhotoFromResource();
    }

    $data['location'] = $location;

    Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
    