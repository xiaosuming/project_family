<?php
    // require_once($_SERVER['DOCUMENT_ROOT']."/php/config/ip_map.php");
    use Util\Storage\StorageFacadeClient;
    use Util\Storage\StorageBucket;
    use Util\Util;
    use DB\CDBFile;
    use Util\Check;
    use Util\ImageProcess;

    $files = isset($_FILES) ? $_FILES : '';                                  //上传的文件
    $module = Check::checkModule(trim(isset($params['module']) ? $params['module'] : ''));	//模块
    //$needSize = isset($params['needSize']) ? $params['needSize'] : '';                      //是否需要回传图片尺寸
    $needCrop = $params['needCrop'] ?? '';
    $cropWidth = $params['cropWidth'] ?? '';
    $cropHeight = $params['cropHeight'] ?? '';
    $cropX = $params['cropX'] ?? '';
    $cropY = $params['cropY'] ?? '';
    $needCompression = $params['needCompression'] ?? '';

    $userId = $GLOBALS['userId'] ?? 0;
    try{

        /* 文件不存在 */
        if($files == ''){
            Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'],"文件不存在");
            exit();
        }

        $fileDB = new CDBFile();
        $client = new StorageFacadeClient();
        $client->setBucket(StorageBucket::$IMAGE);
        $data['locations'] = array();

        foreach($files as $file){
            $imageInfo = ImageProcess::processImageToBlobStrategy($file, $needCompression == 1,$needCrop == 1,$cropWidth,$cropHeight,$cropX,$cropY);
            
            if(!$imageInfo){
                Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'],"文件格式不支持");
                exit();
            }
            
            //直接上传二进制到服务器上
            $fileInfo = $client->uploadBlob($imageInfo['blob'],strtolower($imageInfo['extName']));

            if(!$fileInfo->success){
                Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'],"文件上传错误");
                exit();
            }
            
            $path = $fileInfo->location;

            //检查是否需要返回尺寸
            //if($needSize === "1"){
            $path = $path . "?width=".$imageInfo['width']."&height=".$imageInfo['height'];
            //}

            $fileId = $fileDB->addFile($path,$module,$userId);
            if($fileId > 0){
                array_push($data['locations'],$path);    //保存地址
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
                exit();
            }
        }//end for foreach
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        
    }catch(\Exception $e){
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }

    