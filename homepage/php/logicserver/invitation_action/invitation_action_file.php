<?php
    /**
     * 邀请码索引文件
     */

    /**
     * whiteList是白名单，用来允许某些请求通过验证
     */
    $whiteList['getNewInvitationCode'] = 'allow';
    $whiteList['getInvitationCodePaging'] = 'allow';
    $whiteList['getInvitationCodeViaOpenid'] = 'allow';
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $invitation_action_file_array['getNewInvitationCode'] = "logicserver/invitation_action/{$version}/getNewInvitationCode.php";
    $invitation_action_file_array['getInvitationCodePaging'] = "logicserver/invitation_action/{$version}/getInvitationCodePaging.php";
    $invitation_action_file_array['getUserInvitationCode'] = "logicserver/invitation_action/{$version}/getUserInvitationCode.php";
    $invitation_action_file_array['getInvitationCodeViaOpenid'] = "logicserver/invitation_action/{$version}/getInvitationCodeViaOpenid.php";