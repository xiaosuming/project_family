<?php
/**
 * 系统生成新的邀请码
 * @auther jiangpengfei
 * @date 2017-09-28
 */
    use Util\Util;
    use DB\CDBInvitationCode;
    use Util\Check;

    try{

        $invitationDB = new CDBInvitationCode();
        $code = $invitationDB->getNewInvitationCode();
        $data['code'] = $code;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }