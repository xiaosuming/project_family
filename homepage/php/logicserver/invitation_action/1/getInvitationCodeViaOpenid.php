<?php
    use Util\Util;
    use DB\CDBInvitationCode;
    use Util\Check;
    use Util\TempStorage;

    $auth = Check::check(trim(isset($params['auth']) ? $params['auth'] : '')); 
    $openid = '';

    if($auth == ''){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    try{
        //get openid
        $tempStorage = new TempStorage();
        $result = $tempStorage->get($auth);
        if(!$result){
            Util::printResult($GLOBALS['ERROR_VCODE'], "auth不存在或已过期");
            exit;
        }

        $tempRes = json_decode($result,true);

        if($tempRes != null){
            $openid = $tempRes['openid'];
        } else {
            Util::printResult($GLOBALS['ERROR_VCODE'], "已过期");
            exit;
        }

        if($openid == ''){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "未获取微信授权，无法得到openid");
            exit;
        }

        //check openid bind with code
        $invitationDB = new CDBInvitationCode();
        $result = $invitationDB->checkOpenidHasCode($openid);
        if($result == false) {//has no code
            $code = $invitationDB->bindOpenidWithCode($openid);
            if($code == -1) {
                //异常处理
                $logger->error(Util::exceptionFormat($e));
                Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
            } else {
                $data['code'] = $code;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }
        } else {//has code
            $data['code'] = $result;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        echo $e.message;
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }