<?php
/**
 * 获取新的邀请码
 * @auther jiangpengfei
 * @date 2017-09-28
 */
    use Util\Util;
    use DB\CDBInvitationCode;
    use Util\Check;
    use Util\Pager;

    $pageIndex = Check::checkInteger($params['pageIndex'] ?? '1');
    $pageSize = Check::checkInteger($params['pageSize'] ?? '10');
    $isUsed = Check::checkInteger($params['isUsed'] ?? '-1');

    try{

        $invitationDB = new CDBInvitationCode();
        $paging = $invitationDB->getInvitationCodePaging($pageIndex,$pageSize,$isUsed);
        $total = $invitationDB->getInvitationCodeTotal($isUsed);

        $pager = new Pager($total,$paging,$pageIndex,$pageSize);
        $pager->printPage();
            
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }