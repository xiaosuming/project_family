<?php
/**
 * 生成用户的邀请码
 * @auther jiangpengfei
 * @date 2017-12-04
 */
    use Util\Util;
    use DB\CDBUserInvitationCode;
    use Util\Check;

    try{

        $userId = $GLOBALS['userId'];

        $invitationDB = new CDBUserInvitationCode();

        $code = "";
        if(($result = $invitationDB->checkUserHasCode($userId)) != false ){
            //用户有二维码了
            $code = $result['code'];
        }else{
            //用户没有二维码
            $code = $invitationDB->addUserInvitationCode($userId);
        }
        
        $data['code'] = $code;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }