<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/26 0026
 * Time: 15:40
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();

    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }

    if ($family->openType == 0) {
        // 私密族群，检查权限
        if (!$familyDB->getUserPermission($userId, $familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
            exit;
        }
    }

    
    $CDBFamilyStorage = new CDBFamilyStorage();
    $result = $CDBFamilyStorage->getContentFromStorageLimit($familyId);
    $data['content'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
