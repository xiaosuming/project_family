<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/16 0016
 * Time: 10:21
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$fileId = Check::checkInteger(trim(isset($params['fileId']) ? $params['fileId'] : ''));
$userId = $GLOBALS['userId'];
if ($fileId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {

    $CDBFamily = new CDBFamily();
    $CDBFamilyStorage = new CDBFamilyStorage();
    $count = $CDBFamilyStorage->checkfileExists($fileId);
    if ($count > 0) {

        $familyId = $CDBFamilyStorage->getFamilyIdById($fileId);

        if ($CDBFamilyStorage->isCreatorForStorage($userId, $familyId, $fileId) || $CDBFamily->isOriginatorForFamily($familyId, $userId) || $CDBFamily->isAdminForFamily($familyId, $userId)) {
            $fileSize = $CDBFamilyStorage->getFileSizeByFileId($fileId, $familyId);
            $changeCount = $CDBFamilyStorage->changeFileStatus($fileId);

            if ($changeCount > 0) {
                $content = $CDBFamilyStorage->getContentFromStorageLimit($familyId);
                $oldUsedSize = $content['used_size'];
                $oldRecycleSize = $content['recycle_size'];
                $usedSize = $oldUsedSize - $fileSize;
                $recycleSize = $oldRecycleSize + $fileSize;
                $updateCount = $CDBFamilyStorage->updateUsedSizeForDel($familyId, $usedSize, $recycleSize);

                if ($updateCount > 0) {
                    $data['changeCount'] = $changeCount;
                    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                    exit;
                }

            }
        } else {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
            exit;
        }
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '文件不存在');
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}



