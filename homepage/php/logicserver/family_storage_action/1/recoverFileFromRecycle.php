<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/16 0016
 * Time: 12:06
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$fileId = Check::checkInteger(trim(isset($params['fileId']) ? $params['fileId'] : ''));
$userId = $GLOBALS['userId'];

try {
    $CDBFamily = new CDBFamily();
    $CDBFamilyStorage = new CDBFamilyStorage();
    $count = $CDBFamilyStorage->checkFileExistsInRecycle($fileId);
    if ($count > 0) {
        $familyId = $CDBFamilyStorage->getFamilyIdByIdInRecycle($fileId);
        if ($CDBFamilyStorage->isCreatorForStorage($userId, $familyId, $fileId) || $CDBFamily->isOriginatorForFamily($familyId, $userId) || $CDBFamily->isAdminForFamily($familyId, $userId)) {

            $result = $CDBFamilyStorage->getParentAndNameInRecycle($fileId);
            $parentId = $result['parent'];
            $file_name = $result['name'];
            $isDir = $result['is_dir'];

            if ($isDir == 0) {
                $nameArr = $CDBFamilyStorage->getFileNameByParentPath($parentId, $familyId);
                $size = $CDBFamilyStorage->getFileSizeByFileIdInRecycle($fileId, $familyId);
                foreach ($nameArr as $name) {
                    if ($name['name'] == $file_name) {
                        $len = mb_strlen($file_name);
                        $p = mb_strripos($file_name,'.');
                        $base_name = mb_substr($file_name, 0, $p );
                        $ext_name = mb_substr($file_name, $p, $len - 1);
                        $name = $base_name . '_副本' . $ext_name;
                        if (mb_strlen($name) > 256) {
                            $name = mb_substr($name, 3, mb_strlen($name) - 1);
                        }
                        $CDBFamilyStorage->rename($name, $fileId);
                    }
                }
            }
            if ($isDir == 1) {
                $nameArr = $CDBFamilyStorage->getDirNameByParentPath($parentId, $familyId);
                $size = $CDBFamilyStorage->getSumSizeByDirIdInRecycle($fileId, $familyId);
                foreach ($nameArr as $name) {
                    if ($name['name'] == $file_name) {
                        $name = $file_name . '_副本';
                        if (mb_strlen($name) > 256) {
                            $name = mb_substr($name, 3, mb_strlen($name) - 1);
                        }
                        $CDBFamilyStorage->rename($name, $fileId);
                    }
                }
            }

            $updateCount = $CDBFamilyStorage->recoverFileFromRecycle($fileId);
            if ($updateCount > 0) {
                $updateChildrenCount = $CDBFamilyStorage->recoverChildrenFileFromRecycle($fileId);
                if ($updateChildrenCount > 0) {
                    $updateCount = $updateCount + $updateChildrenCount;
                }
            }

            $content = $CDBFamilyStorage->getContentFromStorageLimit($familyId);
            $oldUsedSize = $content['used_size'];
            $oldRecycleSize = $content['recycle_size'];
            $maxSize = $content['max_size'];

            $usedSize = $oldUsedSize + $size;
            $recycleSize = $oldRecycleSize - $size;

            $updateSizeCount = $CDBFamilyStorage->updateUsedSizeForDel($familyId, $usedSize, $recycleSize);
            if ($updateSizeCount > 0) {
                $data['updateCount'] = $updateCount;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                exit;
            }

        } else {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
            exit;
        }

    } else {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '文件不存在');
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
