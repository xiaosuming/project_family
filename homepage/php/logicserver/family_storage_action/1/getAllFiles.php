<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/16 0016
 * Time: 10:33
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$userId = $GLOBALS['userId'];
try {
    $CDBFamily = new CDBFamily();
    if (!$CDBFamily->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }
    $CDBFamilyStorage = new CDBFamilyStorage();
    $result = $CDBFamilyStorage->getAllFiles($familyId);
    $data['list']=$result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}