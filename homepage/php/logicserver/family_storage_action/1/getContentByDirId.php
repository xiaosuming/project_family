<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/18 0018
 * Time: 9:59
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$dirId = Check::checkInteger(trim(isset($params['dirId']) ? $params['dirId'] : ''));
$userId = $GLOBALS['userId'];
if ($dirId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}

try {
    $CDBFamilyStorage = new CDBFamilyStorage();
    $CDBFamily = new CDBFamily();
    $familyId = $CDBFamilyStorage->getFamilyIdById($dirId);
    if (!$CDBFamily->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }
    $result = $CDBFamilyStorage->getContentByDirId($dirId);
    $data['content'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}