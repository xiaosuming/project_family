<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/16 0016
 * Time: 12:59
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;
use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$userId = $GLOBALS['userId'];
try {
    $CDBFamily = new CDBFamily();
    $CDBFamilyStorage = new CDBFamilyStorage();
    if ($CDBFamily->isOriginatorForFamily($familyId, $userId) || $CDBFamily->isAdminForFamily($familyId, $userId)) {
        $recycleFiles = $CDBFamilyStorage->getFilesByRecycle($familyId);
        $storageClient = new StorageFacadeClient();
        $storageClient->setBucket(StorageBucket::$STORAGE);
        foreach ($recycleFiles as $v) {
            if ($v['is_dir'] == 0) {
                $group_name = $v['group_name'];
                $remote_filename = $v['remote_filename'];
                $file_url = $v['file_url'];
                $count = $CDBFamilyStorage->checkFileUrlExists($file_url, $familyId);
                if ($count != null && $count == 0) {
                    $a = $storageClient->delete($group_name, $remote_filename);
                    $storageClient->exists($group_name, $remote_filename);
                }
            }
        }

        $deleteCount = $CDBFamilyStorage->deleteFilesFromRecycle();
        $updateCount = $CDBFamilyStorage->cleanRecycleSizeByFamilyId($familyId);
        $data['deleteCount'] = $deleteCount;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;

    } else {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
