<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/18 0018
 * Time: 17:04
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$userId = $GLOBALS['userId'];

try {
    $CDBFamily = new CDBFamily();

    $family = $CDBFamily->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }

    if ($family->openType == 0) {

        if (!$CDBFamily->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
            exit;
        }
    }
    $CDBFamilyStorage = new CDBFamilyStorage();
    $result = $CDBFamilyStorage->getRootDir($familyId);
    $data['rootList'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
