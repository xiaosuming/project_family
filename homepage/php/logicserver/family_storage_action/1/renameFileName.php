<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/16 0016
 * Time: 11:15
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$fileName = Check::check(trim(isset($params['fileName']) ? $params['fileName'] : ''));
$fileId = Check::checkInteger(trim(isset($params['fileId']) ? $params['fileId'] : ''));
$userId = $GLOBALS['userId'];
$fileName = Util::filterContentReplace($fileName);

if ($fileId == '' || $fileName == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {
    $CDBFamily = new CDBFamily();
    $CDBFamilyStorage = new CDBFamilyStorage();
    $count = $CDBFamilyStorage->checkfileExists($fileId);
    if ($count > 0) {
        $familyId = $CDBFamilyStorage->getFamilyIdById($fileId);
        if ($CDBFamilyStorage->isCreatorForStorage($userId, $familyId, $fileId) || $CDBFamily->isOriginatorForFamily($familyId, $userId) || $CDBFamily->isAdminForFamily($familyId, $userId)) {
            $result = $CDBFamilyStorage->getParentIdAndName($fileId);
            $parentId = $result['parent'];
            $isDir = $result['is_dir'];
            if ($isDir == 0) {
                $nameArr = $CDBFamilyStorage->getFileNameByParentPath($parentId,$familyId);
            }
            if ($isDir == 1) {
                $nameArr = $CDBFamilyStorage->getDirNameByParentPath($parentId,$familyId);
            }
            foreach ($nameArr as $name) {
                if ($name['name'] == $fileName) {
                    Util::printResult($GLOBALS['ERROR_HAVE_UPLOAD'], '名字已经存在，请重新命名');
                    exit;
                }
            }
            $updateCount = $CDBFamilyStorage->renameFileName($fileName, $fileId);
            $data['updateCount'] = $updateCount;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        } else {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
            exit;
        }

    } else {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '文件不存在');
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
