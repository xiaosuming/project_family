<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/16 0016
 * Time: 14:16
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$fileId = Check::checkInteger(trim(isset($params['fileId']) ? $params['fileId'] : ''));
if ($fileId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {
    $CDBFamilyStorage = new CDBFamilyStorage();
    $CDBFamily = new CDBFamily();
    $familyId = $CDBFamilyStorage->getFamilyIdById($fileId);
    if (!$CDBFamily->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }
    $result = $CDBFamilyStorage->getFilesByFileId($fileId);
    $data['detail'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}