<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/17 0017
 * Time: 13:54
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$parentId = Check::checkInteger(trim(isset($params['parentId']) ? $params['parentId'] : ''));
$dirName = Check::check(trim(isset($params['dirName']) ? $params['dirName'] : ''));
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$userId = $GLOBALS['userId'];
$dirName = Util::filterContentReplace($dirName);

if ($dirName == '' || $familyId == '' || $parentId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {
    $CDBFamily = new CDBFamily();
    if (!$CDBFamily->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }
    $CDBFamilyStorage = new CDBFamilyStorage();
    $count = $CDBFamilyStorage->checkDirNameExistsInFamily($familyId, $dirName, $parentId);
    if ($count && $count > 0) {
        Util::printResult($GLOBALS['ERROR_DIR_EXISTS'], '目录已经存在');
        exit;
    }

    $content = $CDBFamilyStorage->getContentFromStorageLimit($familyId);
    $oldUsedSize = $content['used_size'];
    $oldRecycleSize = $content['recycle_size'];
    $maxSize = $content['max_size'];
    $usedSize = $oldUsedSize + 1;
    $total_size = $usedSize + $oldRecycleSize;
    if ($maxSize < $total_size) {
        Util::printResult($GLOBALS['ERROR_FILE_SIZE'], '超过了家族存储的最大值');
        exit;
    }

    if ($parentId != 0) {
        $result = $CDBFamilyStorage->getDirNameById($parentId);
        if ($result) {
            $parent_path = $result['parent_path'] . ',' . $parentId;
        } else {
            Util::printResult($GLOBALS['ERROR_DIR_EXISTS'], '父目录不存在');
            exit;
        }
    } else {
        $parent_path = 0;
    }

    $size = 1;
    $md5 = '';
    $file_type = 0;
    $data['dirId'] = $CDBFamilyStorage->createDir($familyId, $dirName, $parentId, $parent_path, $size, $file_type, $md5, $userId);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
