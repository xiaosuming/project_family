<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/15 0015
 * Time: 17:58
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\Util;

$dirId = Check::checkInteger(trim(isset($params['dirId']) ? $params['dirId'] : ''));
$userId = $GLOBALS['userId'];

try {
    $CDBFamily = new CDBFamily();
    $CDBFamilyStorage = new CDBFamilyStorage();
    $count = $CDBFamilyStorage->checkDirExists($dirId);
    if ($count > 0) {
        $familyId = $CDBFamilyStorage->getFamilyIdById($dirId);
        if ($CDBFamilyStorage->isCreatorForStorage($userId, $familyId, $dirId) || $CDBFamily->isOriginatorForFamily($familyId, $userId) || $CDBFamily->isAdminForFamily($familyId, $userId)) {

            $totalSize = $CDBFamilyStorage->getSumSizeByDirId($dirId, $familyId);

            $changeCount = $CDBFamilyStorage->changeDirStatus($dirId);
            if ($changeCount > 0) {
                $changeCounts = $CDBFamilyStorage->changeChildrenDirStatus($dirId);
                if ($changeCounts > 0) {
                    $changeCount = $changeCounts + $changeCount;
                }

                $content = $CDBFamilyStorage->getContentFromStorageLimit($familyId);
                $oldUsedSize = $content['used_size'];
                $oldRecycleSize = $content['recycle_size'];

                $usedSize = $oldUsedSize - $totalSize;
                $recycleSize = $oldRecycleSize + $totalSize;
                $updateCount = $CDBFamilyStorage->updateUsedSizeForDel($familyId, $usedSize, $recycleSize);
                if ($updateCount > 0) {
                    $data['changeCounts'] = $changeCount;
                    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                    exit;
                }
            }
        } else {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
            exit;
        }
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '目录不存在');
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}



