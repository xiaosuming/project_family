<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/16 0016
 * Time: 18:29
 */

use DB\CDBFamily;
use DB\CDBFamilyStorage;
use Util\Check;
use Util\FileUtil;
use Util\Util;
use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;

// require_once($_SERVER['DOCUMENT_ROOT'] . "/php/config/ip_map.php");

$file = Check::checkArray(isset($_FILES['file']) ? $_FILES['file'] : '');
$dirId = Check::checkInteger(trim(isset($params['dirId']) ? $params['dirId'] : ''));
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$userId = $GLOBALS['userId'];

/* 文件不存在 */
if ($file == '') {
    Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], "文件不存在");
    exit();
}
if ($dirId == '' || $familyId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {
    $CDBFamily = new CDBFamily();
    if (!$CDBFamily->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '操作权限错误');
        exit;
    }
    $CDBFamilyStorage = new CDBFamilyStorage();

    if ($dirId == 0) {
        $parent_path = 0;
    }
    if ($dirId != 0) {
        $count = $CDBFamilyStorage->checkDirExists($dirId);
        if ($count > 0) {
            $res = $CDBFamilyStorage->getDirNameById($dirId);
            $path = $res['name'];
            $parent_path = $res['parent_path'] . ',' . $dirId;
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '目录不存在');
            exit;
        }
    }

    $content = $CDBFamilyStorage->getContentFromStorageLimit($familyId);
    $oldUsedSize = $content['used_size'];
    $oldRecycleSize = $content['recycle_size'];
    $maxSize = $content['max_size'];
    $usedSize = $oldUsedSize + $file['size'];
    $total_size = $usedSize + $oldRecycleSize;
    if ($maxSize < $total_size) {
        Util::printResult($GLOBALS['ERROR_FILE_SIZE'], '超过了家族存储的最大值');
        exit;
    }

    $oldName = $file['name'];
    $md5 = md5($oldName);
    $result = $CDBFamilyStorage->getContentsInDirByMd5($dirId,$familyId,$md5);
    $count = count($result);
    if ($count > 0 ){
        $source_ip = $result[0]['source_ip'];
        $group_name = $result[0]['group_name'];
        $remote_filename = $result[0]['remote_filename'];
        $file_url = $result[0]['file_url'];
        $size=$result[0]['size'];
        $file_type = $result[0]['file_type'];

        $len = mb_strlen($oldName);
        $p = mb_strripos($oldName,'.');
        $base_name = mb_substr($oldName, 0, $p );
        $ext_name = mb_substr($oldName, $p, $len-1);
        $filename = $base_name . "_{$count}" . $ext_name;

        $data['id'] = $CDBFamilyStorage->uploadFileToDir($familyId, $filename, $dirId, $parent_path, $source_ip, $group_name, $remote_filename, $file_url, $size, $file_type, $md5, $userId);
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    };

    $client = new StorageFacadeClient();
    $client->setBucket(StorageBucket::$STORAGE);
    $strArray = explode(".", $file['name']);
    if (count($strArray) - 1 >= 0) {
        //获取后缀
        $ext_name = $strArray[count($strArray) - 1];
        $fileInfo = null;
        //根据文件名上传
        $fileInfo = $client->upload($file['tmp_name'], strtolower($ext_name));
        if ($fileInfo->success) {
            
            $file_url = $fileInfo->location;
            $size = $fileInfo->size;
            $file_type = FileUtil::getFileType($file);
            $remote_filename = $fileInfo->remoteFileName;
            $source_ip = $fileInfo->sourceIp;
            $group_name = $fileInfo->bucketName;

            $data['id'] = $CDBFamilyStorage->uploadFileToDir($familyId, $oldName, $dirId, $parent_path, $source_ip, $group_name, $remote_filename, $file_url, $size, $file_type, $md5, $userId);
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            exit;
        } else {
            Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], '文件上传失败');
            exit;
        }
    } else {
        Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], "文件上传错误,暂时不支持无后缀名上传");
        exit();
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}