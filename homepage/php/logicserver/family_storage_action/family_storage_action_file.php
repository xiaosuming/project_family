<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/15 0015
 * Time: 13:36
 */

    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $family_storage_action_file_array['createDirByName'] = "logicserver/family_storage_action/{$version}/createDirByName.php";
    $family_storage_action_file_array['uploadFileToDir'] = "logicserver/family_storage_action/{$version}/uploadFileToDir.php";
    $family_storage_action_file_array['deleteDir'] = "logicserver/family_storage_action/{$version}/deleteDir.php";
    $family_storage_action_file_array['deleteFile'] = "logicserver/family_storage_action/{$version}/deleteFile.php";
    $family_storage_action_file_array['getAllFiles'] = "logicserver/family_storage_action/{$version}/getAllFiles.php";
    $family_storage_action_file_array['renameFileName'] = "logicserver/family_storage_action/{$version}/renameFileName.php";
    $family_storage_action_file_array['getFilesByRecycle'] = "logicserver/family_storage_action/{$version}/getFilesByRecycle.php";
    $family_storage_action_file_array['recoverFileFromRecycle'] = "logicserver/family_storage_action/{$version}/recoverFileFromRecycle.php";
    $family_storage_action_file_array['cleanFilesFromRecycle'] = "logicserver/family_storage_action/{$version}/cleanFilesFromRecycle.php";
    $family_storage_action_file_array['getFileDetails'] = "logicserver/family_storage_action/{$version}/getFileDetails.php";
    $family_storage_action_file_array['getContentByDirId'] = "logicserver/family_storage_action/{$version}/getContentByDirId.php";
    $family_storage_action_file_array['getRootDir'] = "logicserver/family_storage_action/{$version}/getRootDir.php";
    $family_storage_action_file_array['getFamilyStorageLimit'] = "logicserver/family_storage_action/{$version}/getFamilyStorageLimit.php";
