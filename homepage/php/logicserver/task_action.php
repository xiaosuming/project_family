<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 9:59
 */
    require_once("logicserver/task_action/task_action_file.php");

    use Util\Util;

    $sub_action = isset($params['sub_action'])?$params['sub_action']:"0";

    if (!array_key_exists($sub_action, $task_action_file_array))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "task action not exist".$sub_action;
        Util::printResult($errorCode, $data);
        exit(-1);
    }

    $sub_action_file = $task_action_file_array[$sub_action];

    if (!file_exists($sub_action_file))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "task action file not exist".$sub_action_file;
        Util::printResult($errorCode, $data);
        exit(-1);
    }

    require_once($sub_action_file);