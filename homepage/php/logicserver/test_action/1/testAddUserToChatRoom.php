<?php 
    use Util\Util;
    use Util\Check;
    use DB\CDBAuth;
    use DB\MailDB;
    use Model\User;
    use ThirdParty\Huanxin;

    $chatroom = $params['chatRoom'] ?? '';
    $hxUsername = $params['hxUsername'] ?? '';

    if($chatroom == '' || $hxUsername == ''){
        echo "传参缺少";
        exit;
    }

    //注册环信的用户
    $huanxin = new Huanxin($GLOBALS['hx_client_id'],$GLOBALS['hx_client_secret']);
    echo $huanxin->addUserToChatRoom($chatroom,$hxUsername);