<?php


    $whiteList['testImageUpload'] = "allow";
    $whiteList['testHuanxin'] = "allow";
    $whiteList['testRegisterHuanxin'] = "allow";
    $whiteList['testBuffUpload'] = "allow";
    $whiteList['testAddUserToChatRoom'] = "allow";

    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $test_action_file_array['testImageUpload'] = "logicserver/test_action/{$version}/testImageUpload.php";
    $test_action_file_array['testHuanxin'] = "logicserver/test_action/{$version}/testHuanxin.php";
    $test_action_file_array['testRegisterHuanxin'] = "logicserver/test_action/{$version}/testRegisterHuanxin.php";
    $test_action_file_array['testBuffUpload'] = "logicserver/test_action/{$version}/testBuffUpload.php";
    $test_action_file_array['testAddUserToChatRoom'] = "logicserver/test_action/{$version}/testAddUserToChatRoom.php";