<?php

    $whiteList['getCurrentTime'] = 'allow';
    $whiteList['getFamilyAlbumByShare'] = "allow";
    $whiteList['getFamilyAlbumNoLogin'] = 'allow';
    $whiteList['getAlbumPhotoNoLogin'] = 'allow';
    $version = $params['version'] ?? "1";

    require_once("filter/filter.php");
	$activity_action_file_array['addAlbum'] = "logicserver/album_action/{$version}/addAlbum.php";
	$activity_action_file_array['editAlbum'] = "logicserver/album_action/{$version}/editAlbum.php";
	$activity_action_file_array['deleteAlbum'] = "logicserver/album_action/{$version}/deleteAlbum.php";
	$activity_action_file_array['addPhotoToAlbum'] = "logicserver/album_action/{$version}/addPhotoToAlbum.php";
	$activity_action_file_array['editPhoto'] = "logicserver/album_action/{$version}/editPhoto.php";
	$activity_action_file_array['deletePhotoFromAlbum'] = "logicserver/album_action/{$version}/deletePhotoFromAlbum.php";
	$activity_action_file_array['getFamilyAlbum'] = "logicserver/album_action/{$version}/getFamilyAlbum.php";
	$activity_action_file_array['getAlbumPhoto'] = "logicserver/album_action/{$version}/getAlbumPhoto.php";
	$activity_action_file_array['searchPersonFromAlbum'] = "logicserver/album_action/{$version}/searchPersonFromAlbum.php";
	$activity_action_file_array['getAllPhotosDetail'] = "logicserver/album_action/{$version}/getAllPhotosDetail.php";
    $activity_action_file_array['getPersonInPhoto'] = "logicserver/album_action/{$version}/getPersonInPhoto.php";
    $activity_action_file_array['getAlbumInfo'] = "logicserver/album_action/{$version}/getAlbumInfo.php";
    $activity_action_file_array['batchDeletePhotoFromAlbum'] = "logicserver/album_action/{$version}/batchDeletePhotoFromAlbum.php";
    $activity_action_file_array['shareAlbum'] = "logicserver/album_action/{$version}/shareAlbum.php";
    $activity_action_file_array['getFamilyAlbumByShare'] = "logicserver/album_action/{$version}/getFamilyAlbumByShare.php";
    $activity_action_file_array['getUserFamilyAlbumPaging'] = "logicserver/album_action/{$version}/getUserFamilyAlbumPaging.php";
    $activity_action_file_array['getUserFamilyAlbum'] = "logicserver/album_action/{$version}/getUserFamilyAlbum.php";
    $activity_action_file_array['getPrevAndNextPhoto'] = "logicserver/album_action/{$version}/getPrevAndNextPhoto.php";
    $activity_action_file_array['getFamilyAlbumNoLogin'] = "logicserver/album_action/{$version}/getFamilyAlbum.php";
    $activity_action_file_array['getAlbumPhotoNoLogin'] = "logicserver/album_action/{$version}/getAlbumPhotoNoLogin.php";
