<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/12/6
 * Time: 15:02
 */
use DB\CDBAlbum;
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use Util\Pager;
$photoId = Check::checkInteger(trim(isset($params['photoId']) ? $params['photoId'] : ''));          //photo id
$userId = $GLOBALS['userId'];
try {
	$albumDB = new CDBAlbum();
	$result = $albumDB->getPersonInPhoto($photoId);
	Util::printResult($GLOBALS['ERROR_SUCCESS'],$result);
}
catch (PDOException $e){
	$logger->error(Util::exceptionFormat($e));
	Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}