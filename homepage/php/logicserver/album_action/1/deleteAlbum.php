<?php
/**
 * 删除相册
 */

use Util\Util;
use Util\Check;
use DB\CDBAlbum;
use DB\CDBFamily;


$userId = $GLOBALS['userId'];//操作用户id
$albumId = Check::checkInteger(trim(isset($params['albumId']) ? $params['albumId'] : ''));    //要删除的相册id

//检查参数缺少
if (empty($albumId)) {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}
try {
    $albumDB = new CDBAlbum();

    //检查当前用户是否有相册的操作权限
    $album = $albumDB->getAlbumById($albumId);
    $familyDB = new CDBFamily();

    if ($album == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($album->familyId != 0) {
        if ($album->createBy != $userId && !$familyDB->isOriginatorForFamily($album->familyId, $userId)
            && !$familyDB->isAdminForFamily($album->familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
    }

    if ($album->familyId == 0) {
        if ($album->createBy != $userId) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
    }

    if ($albumDB->deleteAlbum($albumId)) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], "删除成功");
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
