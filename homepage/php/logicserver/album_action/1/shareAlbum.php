<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-2-20
 * Time: 上午10:07
 */

use DB\CDBAlbum;
use DB\CDBFamily;
use Util\Check;
use Util\Util;

$albumIdOrPhotoId = Check::check($params['albumIdOrPhotoId'] ?? ''); //相册id or photoId
$timeLimit = Check::checkInteger($params['timeLimit'] ?? ''); //分享限制时间  0无时间限制
$type = Check::checkInteger($params['type'] ?? 1); //1 album  2 photo  3 多张相片
$userId = $GLOBALS['userId'];

try {

    $albumDB = new CDBAlbum();

    if ($type == 1) {
        $albumDetail = $albumDB->getAlbumById($albumIdOrPhotoId);
        $familyId = $albumDetail->familyId;
    }

    if ($type == 2) {

        $photoDetail = $albumDB->getPhotoById($albumIdOrPhotoId);
        $albumId = $photoDetail->albumId;
        $albumDetail = $albumDB->getAlbumById($albumId);
        $familyId = $albumDetail->familyId;
    }

    if ($type == 3) {

        $photoIdArr = explode(',', $albumIdOrPhotoId);

        if (count($photoIdArr ) == 1 ){

            $photoDetail = $albumDB->getPhotoById($photoIdArr[0]);
            $albumId = $photoDetail->albumId;
            $albumDetail = $albumDB->getAlbumById($albumId);
            $familyId = $albumDetail->familyId;
        }

        if (count($photoIdArr) > 1){

            $familyIdArr = array();
            foreach ($photoIdArr as $photoId) {
                $photoDetail = $albumDB->getPhotoById($photoId);
                $albumId = $photoDetail->albumId;
                $albumDetail = $albumDB->getAlbumById($albumId);
                if (!in_array($albumDetail->familyId, $familyIdArr)) {
                    $familyIdArr[] = $albumDetail->familyId;
                }
            }

            if (count($familyIdArr) > 1) {
                Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '参数错误');
                exit;
            }
            $familyId = $familyIdArr[0];

        }
        $timeLimit = 0;  //无时间限制
        $albumIdOrPhotoId = json_encode($photoIdArr);
    }

    $familyDB = new CDBFamily();

    if ($familyId != 0){

        $family = $familyDB->getFamilyById($familyId);

        if ($family->openType == 0) {
            if (!$familyDB->isUserForFamily($familyId, $userId)) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
                exit;
            }
        }
    }

    $shareCode = md5($albumIdOrPhotoId . $userId . Util::generateRandomCode(6));
    $shareAlbumId = $albumDB->addShareAlbum($albumIdOrPhotoId, $type, $shareCode, $timeLimit, $userId);

    if ($shareAlbumId > 0) {
        $data['shareCode'] = $shareCode;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加失败');
    }

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

