<?php
/**
 * 编辑相册
 */

use Util\Util;
use Util\Check;
use DB\CDBAlbum;
use DB\CDBFamily;
use ThirdParty\MiniProgram;

$userId = $GLOBALS['userId'];//操作用户id
$albumId = Check::checkInteger(trim(isset($params['albumId']) ? $params['albumId'] : ''));    //要删除的相册id
$albumName = Check::check(trim($params['albumName'] ?? ''));
$description = Check::check(trim($params['description'] ?? ''));

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$albumName, $description]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

//检查参数缺少
if (empty($albumId) || $albumName == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {
    $albumDB = new CDBAlbum();

    //检查当前用户是否有相册的操作权限
    $album = $albumDB->getAlbumById($albumId);
    $familyDB = new CDBFamily();

    if ($album == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    $familyId = $album->familyId;

    if ($familyId != 0) {
        if ($album->createBy != $userId && !$familyDB->isAdminForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
    }

    if ($familyId == 0) {
        if ($album->createBy != $userId) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
    }

    $updateRow = $albumDB->editAlbum($albumId, $albumName, $description, $userId);

    $data['update'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
