<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/12/6
 * Time: 11:03
 */
use DB\CDBAlbum;
use Util\Check;
use Util\Util;
use DB\CDBFamily;
$albumId = Check::checkInteger(trim(isset($params['albumId']) ? $params['albumId'] : ''));
$userId = $GLOBALS['userId'];
try {
	if ($albumId == "") {
		Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
		exit;
	}
	$albumDB = new CDBAlbum();
	$data['album'] = $albumDB->getAlbumById($albumId);
	Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
}
catch (PDOException $e){
	$logger->error(Util::exceptionFormat($e));
	Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}