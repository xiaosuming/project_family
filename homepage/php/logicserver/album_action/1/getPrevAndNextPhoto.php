<?php
/**
 * 获取上一张和下一张相片
 * @author: jiangpengfei
 * @date:   2019-04-02
 */

use Util\Util;
use Util\Check;
use DB\CDBAlbum;
use DB\CDBFamily;

$photoId = Check::checkInteger($params['photoId'] ?? '');
$userId = $GLOBALS['userId'];

try {

    $albumDB = new CDBAlbum();
    $photo = $albumDB->getPhotoById($photoId);

    if ($photo == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($photo->createBy != $userId) {
        $album = $albumDB->getAlbumById($photo->albumId);
        $familyId = $album->familyId;
        $familyDB = new CDBFamily();
        if ($familyId != 0) {
            if (!$familyDB->isAdminForFamily($familyId, $userId)) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
                exit;
            }
        } else {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
    }

    $detail = $albumDB->getPrevAndNextPhoto($photo->albumId, $photoId);

    $data['detail'] = $detail;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
	Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}