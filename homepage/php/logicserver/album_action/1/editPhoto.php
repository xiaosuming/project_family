<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/9
 * Time: 13:38
 */

use DB\CDBPerson;
use Util\Check;
use DB\CDBAlbum;
use Util\Util;
use DB\CDBFamily;
use DB\CDBArea;
use ThirdParty\MiniProgram;

$photoId = Check::checkInteger(trim(isset($params['photoId']) ? $params['photoId'] : ''));
$description = Check::check(trim(isset($params['description']) ? $params['description'] : ''), 0, 500);        //照片描述
$country = Check::checkInteger(trim(isset($params['country']) ? $params['country'] : '0'));  //国家 默认为0 中国
$province = Check::checkInteger(trim(isset($params['province']) ? $params['province'] : '0')); //省 没有默认为 0
$city = Check::checkInteger(trim(isset($params['city']) ? $params['city'] : '0')); //市 没有默认为 0
$area = Check::checkInteger(trim(isset($params['area']) ? $params['area'] : '0')); //区 没有默认为 0
$town = Check::checkInteger(trim(isset($params['town']) ? $params['town'] : '0')); //乡 没有默认为0
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''), 0, 100);
$personIds = trim(isset($params['personIds']) ? $params['personIds'] : '');
if ($personIds != '') {
    $personIds = Check::checkIdArray($personIds);
}
$userId = $GLOBALS['userId'];

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$address, $description]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    $familyDB = new CDBFamily();
    $albumDB = new CDBAlbum();

    $photo = $albumDB->getPhotoById($photoId);

    if ($photo == null) {
        //查询出错，照片不存在
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "照片不存在");
        exit();
    }

    $albumId = $photo->albumId;

    $familyId = $albumDB->getAlbumById($albumId)->familyId;

    if ($familyId != 0) {
        if ($photo->createBy != $userId && !$familyDB->isAdminForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
    }

    if ($familyId == 0) {
        if ($photo->createBy != $userId) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
    }

    $CDBArea = new CDBArea();
    $result = $CDBArea->getAreaNameById($country) ?? '0';
    $countryName = $result['areaName'];
    $result = $CDBArea->getAreaNameById($province) ?? '0';
    $provinceName = $result['areaName'];
    $result = $CDBArea->getAreaNameById($city) ?? '0';
    $cityName = $result['areaName'];
    $result = $CDBArea->getAreaNameById($area) ?? '0';
    $areaName = $result['areaName'];
    $result = $CDBArea->getAreaNameById($town) ?? '0';
    $townName = $result['areaName'];

    $CDBPerson = new CDBPerson();
    if ($personIds != '') {
        $personIdArr = explode(',', $personIds);
        $len = count($personIdArr);
        $lastValue = $personIdArr[$len - 1];
        $str = ' ';
        foreach ($personIdArr as $personId) {
            if ($personId == $lastValue) {
                $str .= " id='$personId'";
            } else {
                $str .= " id='$personId' OR";
            }
        }
        $count = $CDBPerson->verifyPersonExistsInFamily($str, $familyId);
        if (!$count || $count != $len) {
            Util::printResult($GLOBALS['ERROR_PERSON_EXISTS'], '相关人物不存在');
            exit;
        }
    }

    $updateRow = $albumDB->editPhoto($photoId, $description, $country, $countryName, $province, $provinceName, $city, $cityName, $area, $areaName, $town, $townName, $address);

    if ($albumDB->updatePhotoPerson($photoId, $personIds, $userId)) {
        $data['update'] = $updateRow;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "更新失败");
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
