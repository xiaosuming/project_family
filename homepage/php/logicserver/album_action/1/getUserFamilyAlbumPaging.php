<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-5
 * Time: 下午4:38
 */

use DB\CDBAlbum;
use DB\CDBFamily;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;


$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$count = Check::checkInteger(trim($params['count'] ?? 10));     // 默认是10条数据
$userId = $GLOBALS['userId'];

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $familyDB = new CDBFamily();
    $joinFamilyList = $familyDB->getJoinFamily($userId);

    $total = count($joinFamilyList);

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $familyList = $familyDB->getJoinFamilyPaging($userId,$maxId,$sinceId,$count);

    $albumDB = new CDBAlbum();
    foreach ($familyList as $k => $family) {
        $familyId = $family['id'];
        $familyAlbum = $albumDB->getFamilyAlbumByFamilyId($familyId);
        $familyList[$k]['albumList'] = $familyAlbum;
    }

    $len = count($familyList);

    $nextSinceId = $familyList[0]['id'];

    $nextMaxId = $familyList[$len - 1]['id'];

    if ($len == 0 || $len>=$total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $familyList, $nextMaxId, $nextSinceId, $lastPage);
    $paginator->printPage();

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
