<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/8
 * Time: 13:48
 */

use DB\CDBAlbum;
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use ThirdParty\MiniProgram;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));          //家族id
$userId = $GLOBALS['userId'];
$albumName = Check::check(trim(isset($params['albumName']) ? $params['albumName'] : ''), 1, 20);            //相册标题
$description = Check::check(trim(isset($params['description']) ? $params['description'] : ''), 0, 200);            //相册标题

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$albumName, $description]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {

    if ($familyId == '' || $albumName == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $familyDB = new CDBFamily();
    //检查用户是否是家族中的成员

    //familyId 是0  创建个人相册
    if ($familyId != 0) {
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
    }

    $albumDB = new CDBAlbum();
    $albumId = $albumDB->createAlbum($albumName, $description, $familyId, $userId, $userId);
    if ($albumId > 0) {
        $data['albumId'] = $albumId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加失败');
    }


} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
