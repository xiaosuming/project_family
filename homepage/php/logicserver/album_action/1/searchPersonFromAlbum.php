<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/12/6
 * Time: 11:03
 */
use DB\CDBAlbum;
use Util\Check;
use Util\Util;
use DB\CDBFamily;
use Util\Pager;
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));          //家族id
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1'));		//页码
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '10'));		//每页数量
$albumId = Check::checkInteger(trim(isset($params['albumId']) ? $params['albumId'] : ''));
$name = Check::check(trim(isset($params['name'])?$params['name']:''));
$userId = $GLOBALS['userId'];
try {
	if ($albumId == "" || $familyId =="") {
		Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
		exit;
	}
	$albumDB = new CDBAlbum();
	$familyDB = new CDBFamily();
	if (!$familyDB->isUserForFamily($familyId, $userId)) {
		Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
		exit;
	}
	$total = $albumDB->getPersonPhotoCount($familyId,$albumId,$name);
	$photos = $albumDB->getPersonPhotoPaging($familyId,$albumId,$pageIndex,$pageSize,$name);
	$pager = new Pager($total, $photos, $pageIndex, $pageSize);
	$pager->printPage();        //以分页的格式输出
}
catch (PDOException $e){
	$logger->error(Util::exceptionFormat($e));
	Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}