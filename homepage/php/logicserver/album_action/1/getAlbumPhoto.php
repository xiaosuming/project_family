<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/14
 * Time: 10:13
 */

use DB\CDBAlbum;
use DB\CDBFamily;
use Util\Check;
use Util\Pager;
use Util\Util;

$albumId = Check::checkInteger(trim(isset($params['albumId']) ? $params['albumId'] : '')); //家族id
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1')); //页码
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '10')); //每页数量
$userId = $GLOBALS['userId'] ?? 0;
try {
    if ($albumId == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    $albumDB = new CDBAlbum();
    $familyalbum = $albumDB->getAlbumById($albumId);
    if ($familyalbum == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "相册不存在");
        exit;
    }

    $familyId = $familyalbum->familyId;
    //检查权限
    $familyDB = new CDBFamily();

    if ($familyId != 0) {

        $family = $familyDB->getFamilyById($familyId);

        if ($family == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }

        if ($family->openType == 0) {
            if (!$familyDB->isUserForFamily($familyId, $userId)) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
                exit;
            }
        }
    }

    $total = $albumDB->getPhotoCount($albumId);
    $photos = $albumDB->getPhotoPaging($pageIndex, $pageSize, $albumId);
    $pager = new Pager($total, $photos, $pageIndex, $pageSize);
    $pager->printPage(); //以分页的格式输出
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
