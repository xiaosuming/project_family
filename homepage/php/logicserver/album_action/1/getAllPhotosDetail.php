<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/12/6
 * Time: 14:15
 */
use DB\CDBAlbum;
use DB\CDBFamily;
use Util\Check;
use Util\Util;
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : '')); //家族id
$albumId = Check::checkInteger(trim(isset($params['albumId']) ? $params['albumId'] : ''));
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
$userId = $GLOBALS['userId'];
try {
    if ($albumId == "" || $familyId == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }
    $albumDB = new CDBAlbum();
    $familyDB = new CDBFamily();
    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($family->openType == 0) {
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit;
        }
    }
    $result = $albumDB->getAllPhotosDetail($familyId, $albumId, $name);
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
