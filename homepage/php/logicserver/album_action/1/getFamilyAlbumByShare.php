<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-2-20
 * Time: 上午10:34
 */

use DB\CDBAlbum;
use Util\Check;
use Util\Pager;
use Util\Util;

$shareCode = Check::check($params['shareCode'] ?? '');
$pageIndex = Check::checkInteger($params['pageIndex'] ?? 1);
$pageSize = Check::checkInteger($params['pageSize'] ?? 10);

if ($shareCode == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try {

    $albumDB = new CDBAlbum();
    $albumShare = $albumDB->getAlbumShareByCode($shareCode);
    if ($albumShare == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '分享数据不存在');
        exit;
    }
    $shareTime = strtotime($albumShare['createTime']);
    $currentTime = time();
    $timeLimit = $albumShare['timeLimit'];

    if($timeLimit != 0){
        //检查这个分享是否有效
        if ($currentTime - $shareTime > 60 * 60 * $timeLimit) {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "分享已过期");
            exit;
        }
    }

    $type = $albumShare['type'];

    if ($type == 1) {
        $albumId = $albumShare['albumIdOrPhotoId'];
        $total = $albumDB->getPhotoCount($albumId);
        $photos = $albumDB->getPhotoPaging($pageIndex, $pageSize, $albumId);
        $pager = new Pager($total, $photos, $pageIndex, $pageSize);
        $pager->printPage(); //以分页的格式输出
    }

    if ($type == 2) {
        $photoId = $albumShare['albumIdOrPhotoId'];
        $shareDetail = $albumDB->getPhotoById($photoId);
        $data['shareDetail'] = $shareDetail;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }

    if ($type == 3){
        $jsonPhoto = $albumShare['albumIdOrPhotoId'];
        $photoIdArr = json_decode($jsonPhoto,true);
        $shareDetail = $albumDB->getBatchPhotosById($photoIdArr);
        $data['shareDetail'] = $shareDetail;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
