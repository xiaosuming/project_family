<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/9
 * Time: 16:42
 */

use DB\CDBAlbum;
use DB\CDBFamily;
use Util\Util;
use Util\Check;

$photoId = Check::checkInteger(trim(isset($params['photoId']) ? $params['photoId'] : '0')); //乡 没有默认为0
$userId = $GLOBALS['userId'];

try {

    $albumDB = new CDBAlbum();
    $photo = $albumDB->getPhotoById($photoId);

    if ($photo == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($photo->createBy != $userId) {
        $album = $albumDB->getAlbumById($photo->albumId);
        $familyId = $album->familyId;
        $familyDB = new CDBFamily();
        if ($familyId != 0) {
            if (!$familyDB->isOriginatorForFamily($familyId, $userId) && !$familyDB->isAdminForFamily($familyId, $userId)) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
                exit;
            }
        }
        if ($familyId == 0) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }

    }
    if ($albumDB->deletePhotoFromAlbum($photoId, $userId)) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], "删除成功");
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
