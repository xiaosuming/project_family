<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-25
 * Time: 下午2:03
 */
use DB\CDBAlbum;
use DB\CDBFamily;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

$userId = $GLOBALS['userId'];

try {

    $familyDB = new CDBFamily();
    $familyList = $familyDB->getJoinFamily($userId);

    $albumDB = new CDBAlbum();
    foreach ($familyList as $k => $family) {
        $familyId = $family['id'];
        $familyAlbum = $albumDB->getFamilyAlbumByFamilyId($familyId);
        $familyList[$k]['albumList'] = $familyAlbum;
    }

    $data['list'] = $familyList;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
