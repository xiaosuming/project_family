<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-26
 * Time: 上午9:54
 */

use DB\CDBAlbum;
use DB\CDBFamily;
use Util\Check;
use Util\Util;

// photoStr 格式 1,2,3,4
$photoStr = Check::check($params['photoStr'] ?? '');
$userId = $GLOBALS['userId'];

if ($photoStr == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
    exit;
}
try {

    $albumDB = new CDBAlbum();
    $photoArr = explode(',', $photoStr);

    $familyDB = new CDBFamily();

    foreach ($photoArr as $v) {
        $photo = $albumDB->getPhotoById($v);
        if ($photo == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
        if ($photo->createBy != $userId) {
            $album = $albumDB->getAlbumById($photo->albumId);
            $familyId = $album->familyId;

            if ($familyId != 0) {
                if (!$familyDB->isOriginatorForFamily($familyId, $userId) && !$familyDB->isAdminForFamily($familyId, $userId)) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
                    exit;
                }
            }

            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;

        }
    }

    $updateRow = $albumDB->deleteBatchPhotoFromAlbum($photoStr, $userId);

    if ($updateRow > 0) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], "删除成功");
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
        exit;
    }

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
