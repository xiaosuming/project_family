<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/14
 * Time: 9:50
 */
use DB\CDBAlbum;
use DB\CDBFamily;
use Util\Check;
use Util\Pager;
use Util\Util;
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : '')); //家族id
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1')); //页码
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '10')); //每页数量
$userId = $GLOBALS['userId'] ?? 0;  // 未登录可以看公开家族的相册,默认用户id是0
if ($familyId == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}
try {
    $familyDB = new CDBFamily();

    if ($familyId != -1){

        if ($familyId !=0){
            $family = $familyDB->getFamilyById($familyId);

            if ($family == null) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
                exit;
            }

            if ($family->openType == 0) {

                if (!$familyDB->isUserForFamily($familyId, $userId)) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
                    exit;
                }
            }
        }
    }

    $albumDB = new CDBAlbum();
    $total = $albumDB->getAlbumCount($familyId, $userId);
    $albums = $albumDB->getAlbumPaging($pageIndex, $pageSize, $familyId, $userId);
    $pager = new Pager($total, $albums, $pageIndex, $pageSize);
    $pager->printPage(); //以分页的格式输出

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
