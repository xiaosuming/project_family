<?php

/**
 * 创建回复
 * @date 2020-12-07
 * @author liuzhenhao
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;

$replyId = Check::checkInteger($params['id'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $lookForFamilyDB = new CDBLookForFamily();

    $reply = $lookForFamilyDB->getReplyById($replyId);
    if (!$reply){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '回复不存在');
        exit;
    }

    $comment = $lookForFamilyDB->getCommentById($reply['comment_id'], true);
    if (!$comment){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '评论不存在');
        exit;
    }

    if ($userId !== $comment['createBy'] || $userId !== $comment['authorId']){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $data = [
        'is_delete'=>1,
        'update_by'=>$userId
    ];

    $result = $lookForFamilyDB->updateReply($replyId ,$data);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], ['accept'=>$result]);

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
