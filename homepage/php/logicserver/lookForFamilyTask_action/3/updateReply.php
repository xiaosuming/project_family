<?php

/**
 * 更新回复
 * @date 2020-12-07
 * @author liuzhenhao
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;

$replyId = Check::checkInteger($params['id'] ?? '');
$content =  Check::check(trim(isset($params['content']) ? trim($params['content']) : ''),0,500);
$userId = $GLOBALS['userId'];

try {
    $lookForFamilyDB = new CDBLookForFamily();

    $reply = $lookForFamilyDB->getReplyById($replyId);
    if (!$reply){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '回复不存在');
        exit;
    }

    $comment = $lookForFamilyDB->getCommentById($reply['comment_id'], true);
    if (!$comment){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '评论不存在');
        exit;
    }

    if ($userId !== $comment['createBy'] || $userId !== $comment['authorId']){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $data = [
        'content'=>$content,
        'update_by'=>$userId
    ];

    $result = $lookForFamilyDB->updateReply($replyId ,$data);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], ['accept'=>$result]);

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
