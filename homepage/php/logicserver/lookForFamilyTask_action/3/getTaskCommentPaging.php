<?php

/**
 * 分页获取回复与评论
 * @date 2020-12-07
 * @author jiangpengfei
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Util\Pager;
$taskId = Check::checkInteger($params['taskId'] ?? '');
$pageIndex = Check::checkInteger($params['page'] ?? 1);
$pageSize = Check::checkInteger($params['pageSize'] ?? 10);
$replyNumber = Check::checkInteger($params['replyNumber'] ?? 3);
$userId = $GLOBALS['userId'];

$pageSize = $pageSize < 100 ?$pageSize : 99;
$replyNumber = $replyNumber < 21 ? $replyNumber : 20;

try {
    $lookForFamilyDB = new CDBLookForFamily();

    $task = $lookForFamilyDB->getTaskById($taskId);

    if (!$task){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '数据不存在');
        exit;
    }
    $data = $lookForFamilyDB->getCommentAndReplyByTaskId($taskId, $replyNumber, $pageIndex, $pageSize, $userId, $task['createBy']);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
