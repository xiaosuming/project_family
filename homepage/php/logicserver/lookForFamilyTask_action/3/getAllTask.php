<?php
/**
 * 获取所有任务
 * @date 2020-12-08
 * @author liuzhenhao
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Model\LookForFamilyTask;

$isFinish = Check::check($params['isFinish'] ?? null);
$userId = $GLOBALS['userId'];
$isOwner = Check::checkInteger(trim(isset($params['isOwner']) ? trim($params['isOwner']) : 0));
$page = Check::checkInteger(trim(isset($params['page']) ? trim($params['page']) : 1));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? trim($params['pageSize']) : 20));

if ($pageSize > 99){
    $pageSize = 99;
}
$isFinish = $isOwner ? $isFinish : 0;

try{

    $lookForFamilyDB = new CDBLookForFamily();

    $tasks = $lookForFamilyDB->getTaskList($userId, $page, $pageSize, $isOwner, $isFinish);

    foreach($tasks['tasks'] as &$task){
        $task['taskContent'] = json_decode($task['taskContent'],true);
//        $array = explode(",",$task['excludeFamilyIds']);
//        array_pop($array);
//        $task['excludeFamilyIds'] = $array;
    }
    

//    $data['tasks'] = $tasks;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $tasks);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
