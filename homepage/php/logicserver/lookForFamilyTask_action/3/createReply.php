<?php

/**
 * 创建回复
 * @date 2020-12-07
 * @author liuzhenhao
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;

$commentId = Check::checkInteger($params['commentId'] ?? '');
$content =  Check::check(trim(isset($params['content']) ? trim($params['content']) : ''),0,500);
$toUser = Check::checkInteger($params['toUser'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $lookForFamilyDB = new CDBLookForFamily();

    $comment = $lookForFamilyDB->getCommentById($commentId);
    if (!$comment){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '任务不存在');
        exit;
    }

    $data = [
        'comment_id'=>$commentId,
        'content'=>$content,
        'from_user'=>$userId,
        'to_user'=> $toUser,
        'create_by'=>$userId,
        'update_by'=>$userId
    ];

    $result = $lookForFamilyDB->createReply($data);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], ['accept'=>$result]);

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
