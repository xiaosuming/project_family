<?php
/**
 * 更新任务设置 
 * @date 2020-12-09
 * @author liuzhenhao
 */
use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Model\LookForFamilyTask;

//updateTaskSetting($taskId,$priority,$threshold,$cycle,$email,$userId){

$taskId = Check::checkInteger($params['taskId'] ?? '');
$priority = Check::checkInteger($params['priority'] ?? 5);
$threshold = Check::checkInteger($params['threshold'] ?? 70);
$cycle = Check::checkInteger($params['cycle'] ?? 28);
$email = Check::checkEmail($params['email'] ?? '');
$showType = Check::checkInteger($params['showType'] ?? 0);
$userId = $GLOBALS['userId'];


if($threshold < 70 || $threshold > 100){
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"阈值的大小错误");
    exit();
}

if($priority < 1 || $priority > 5){
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"任务优先级大小错误");
    exit();
}


try{

    $lookForFamilyDB = new CDBLookForFamily();

    if(!$lookForFamilyDB->verifyTaskIdAndUserId($taskId,$userId)){
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $updateRow = $lookForFamilyDB->updateTaskSettingV3($taskId,$priority,$threshold,$cycle,$email,$userId, $showType);

    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
