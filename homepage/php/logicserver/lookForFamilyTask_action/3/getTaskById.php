<?php
/**
 * 获取人物详情
 * @date 2020-12-08
 * @author liuzhenhao
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;

$taskId = Check::checkInteger($params['taskId'] ?? '');
$userId = $GLOBALS['userId'];
$addVisitTimes = Check::checkInteger($params['addVisitTimes'] ?? 0);

try {
    $lookForFamilyDB = new CDBLookForFamily();
    $task = $lookForFamilyDB->getTaskById($taskId,true);

    if ($task == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '数据不存在');
        exit;
    }

    if ($task['createBy'] !== $userId) {
        if ($task['isFinish'] != 0){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
            exit;
        }

        $data = [];
        if ($addVisitTimes){
            $data['addVisitTimes'] = $lookForFamilyDB->updateTaskReadNum($taskId);
        }

        $data['taskContent'] = json_decode($task['taskContent'], true);
        $array = explode(",",$data['excludeFamilyIds']);
        array_pop($array);
        $data['excludeFamilyIds'] = $array;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit;
    }

    $array = explode(",",$task['excludeFamilyIds']);
    array_pop($array);
    $task['excludeFamilyIds'] = $array;
    $task['taskContent'] = json_decode($task['taskContent'], true);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $task);

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
