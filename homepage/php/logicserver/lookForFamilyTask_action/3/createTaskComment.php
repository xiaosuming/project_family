<?php

/**
 * 分页获取回复与评论
 * @date 2020-12-07
 * @author jiangpengfei
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;

$taskId = Check::checkInteger($params['taskId'] ?? '');
$content =  Check::check(trim(isset($params['content']) ? trim($params['content']) : ''),1,500);
$userId = $GLOBALS['userId'];


try {
    $lookForFamilyDB = new CDBLookForFamily();

    $task = $lookForFamilyDB->getTaskById($taskId);
    if (!$task){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '任务不存在');
        exit;
    }

    $data = [
        'task_id'=>$taskId,
        'content'=>$content,
        'author_id'=>$userId,
        'create_by'=>$userId,
        'update_by'=>$userId
    ];

    $result = $lookForFamilyDB->createComment($data);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], ['accept'=>$result]);

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
