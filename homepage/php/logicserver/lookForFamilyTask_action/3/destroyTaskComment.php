<?php

/**
 * 分页获取回复与评论
 * @date 2020-12-07
 * @author jiangpengfei
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;

$commentId = Check::checkInteger($params['id'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $lookForFamilyDB = new CDBLookForFamily();

    $comment = $lookForFamilyDB->getCommentById($commentId, true);
    if (!$comment){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '评论不存在');
        exit;
    }

    if ($userId !== $comment['createBy'] || $userId !== $comment['authorId']){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $data = [
        'is_delete'=>1,
        'update_by'=>$userId
    ];

    $result = $lookForFamilyDB->updateComment($commentId, $data);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], ['accept'=>$result]);

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
