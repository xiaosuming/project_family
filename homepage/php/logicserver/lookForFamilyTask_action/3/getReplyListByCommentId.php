<?php

/**
 * 分页获取回复
 * @date 2020-12-07
 * @author liuzhenhao
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;

$commentId = Check::checkInteger($params['commentId'] ?? '');
$pageIndex = Check::checkInteger($params['pageIndex'] ?? 1);
$pageSize = Check::checkInteger($params['pageSize'] ?? 10);
$userId = $GLOBALS['userId'];
if ($pageSize > 99){
    $pageSize = 99;
}


try {
    $lookForFamilyDB = new CDBLookForFamily();

    $comment = $lookForFamilyDB->getCommentById($commentId, true);

    $data = $lookForFamilyDB->getReplyListByCommentId($commentId, $pageIndex, $pageSize, $userId, $comment['createBy']);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
