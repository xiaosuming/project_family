<?php
/**
 * 获取所有任务
 * @date 2018-01-17
 * @author jiangpengfei 
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use DB\CDBFamily;

$taskResultId = Check::checkInteger($params['taskResultId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $lookForFamilyDB = new CDBLookForFamily();
    $taskResult = $lookForFamilyDB->getTaskResult($taskResultId);

    if ($taskResult == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '数据不存在');
        exit;
    }

    if (!$lookForFamilyDB->verifyTaskResultIdAndUserId($taskResultId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $taskResult['findContents'] = json_decode($taskResult['findContents'], true);
    if ($taskResult['findContents']){
        $familyDict = [];
        $familyIds = [];
        foreach ($taskResult['findContents'] as $family){
            $familyDict[$family['familyId']] = $family;
            $familyIds[] = $family['familyId'];
        }
        $familyDB = new CDBFamily();
        $familysInfo = $familyDB->getFamilyByIds($familyIds);
        foreach ($familysInfo as $k => $v){
            $familyDict[$v['id']]['familySurname'] = $v['surname'];
            $familyDict[$v['id']]['familyPhoto'] = $v['photo'];
        }
        $taskResult['findContents'] = array_values($familyDict);
    }


    $data['task'] = $taskResult;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
