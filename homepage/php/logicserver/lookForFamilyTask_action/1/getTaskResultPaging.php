<?php
/**
 * 分页获取任务的执行历史
 * @date 2018-01-17
 * @author jiangpengfei 
 */
use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Util\Pager;
use Model\LookForFamilyTask;
use DB\CDBFamily;

$taskId = Check::checkInteger($params['taskId'] ?? '');
$pageIndex = Check::checkInteger($params['pageIndex'] ?? 1);
$pageSize = Check::checkInteger($params['pageSize'] ?? 10);
$userId = $GLOBALS['userId'];

try{
    $lookForFamilyDB = new CDBLookForFamily();

    if(!$lookForFamilyDB->verifyTaskIdAndUserId($taskId,$userId)){
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $paging = $lookForFamilyDB->getTaskResultPaging($taskId,$pageIndex,$pageSize);

    $familyDB = new CDBFamily();
    foreach($paging as &$result){
        $result['findContents'] = json_decode($result['findContents'],true);

        foreach($result['findContents'] as $key => $family) {
            if (isset($family['familyId'])) {
                $familyInfo = $familyDB->getFamilyById($family['familyId']);
                if ($familyInfo != null) {
                    $result['findContents'][$key]['familyPhoto'] = $familyInfo->photo;
                }
            }
        }
    }


    $total = $lookForFamilyDB->getTaskResultTotal($taskId);
    $pager = new Pager($total,$paging,$pageIndex,$pageSize);
    $pager->printPage();
    
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
