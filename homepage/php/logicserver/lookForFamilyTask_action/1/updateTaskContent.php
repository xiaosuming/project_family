<?php
/**
 * 更新任务内容
 * @date 2018-01-17
 * @author jiangpengfei 
 */
use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Model\LookForFamilyTask;

$taskId = Check::checkInteger($params['taskId'] ?? '');
$taskContent = $params['taskContent'] ?? '';    //任务内容
$userId = $GLOBALS['userId'];

$content = json_decode($taskContent,true);
if($content == NULL){
    //格式不正确
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"任务内容格式不正确");
    exit();
}else{
    if(!isset($content['persons']) || count($content['persons']) <= 0){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"至少有一个人物");
        exit();
    }

    if(!isset($content['lastname'])){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"必须包含家族姓氏");
        exit();
    }

    foreach($content['persons'] as $person) {
        if (!isset($person['name']) || $person['name'] == '') {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"人物必须包含姓名");
            exit();
        }

        if (!isset($person['address']) || $person['address'] == '') {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"地址提交字段不能为空字符串");
            exit();
        }

        if (!isset($person['birthday']) || $person['birthday'] == '') {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"生日提交字段不能为空字符串");
            exit();
        }
    }
}

try{

    $lookForFamilyDB = new CDBLookForFamily();

    if(!$lookForFamilyDB->verifyTaskIdAndUserId($taskId,$userId)){
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $updateRow = $lookForFamilyDB->updateTaskContent($taskId,$taskContent,$userId);

    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
