<?php
/**
 * 获取所有任务
 * @date 2018-01-17
 * @author jiangpengfei 
 */

use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Model\LookForFamilyTask;

$isFinish = Check::check($params['isFinish'] ?? null);
$userId = $GLOBALS['userId'];   

try{

    $lookForFamilyDB = new CDBLookForFamily();

    $tasks = $lookForFamilyDB->getAllTask($userId,$isFinish);

    foreach($tasks as &$task){
        $task['taskContent'] = json_decode($task['taskContent'],true);
        $array = explode(",",$task['excludeFamilyIds']);
        array_pop($array);
        $task['excludeFamilyIds'] = $array;
    }

    

    $data['tasks'] = $tasks;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
