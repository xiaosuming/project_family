<?php
/**
 * 重新提交任务
 * @date 2018-01-17
 * @author jiangpengfei 
 */
use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Model\LookForFamilyTask;

//updateTaskSetting($taskId,$priority,$threshold,$cycle,$email,$userId){

$taskId = Check::checkInteger($params['taskId'] ?? '');
$excludeFamilyIds = Check::checkIdArray($params['excludeFamilyIds'] ?? '');
$userId = $GLOBALS['userId'];   

$excludeFamilyIds = explode(",",$excludeFamilyIds);

try{

    $lookForFamilyDB = new CDBLookForFamily();

    if(!$lookForFamilyDB->verifyTaskIdAndUserId($taskId,$userId)){
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $tasks = $lookForFamilyDB->getAllTask($userId,0);

    if(count($tasks) > 0){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"一个用户同时只能有一个运行中的任务");
        exit();
    }

    $updateRow = $lookForFamilyDB->recommitTask($taskId,$excludeFamilyIds,$userId);

    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
