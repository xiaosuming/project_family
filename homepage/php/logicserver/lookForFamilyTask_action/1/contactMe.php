<?php
/**
 * 用户请求家族管理员联系
 * @author jiangpengfei
 * @date 2018-01-29
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBMessage;
use DB\CDBLookForFamily;
use Model\Message;


$userId = $GLOBALS['userId'];

$taskResultId = Check::checkInteger(trim(isset($params['taskResultId']) ? $params['taskResultId'] : ''));   //任务结果id
$familyIndex = Check::checkInteger(trim(isset($params['familyIndex']) ? $params['familyIndex'] : ''));      //任务结果中的第几个家族
$phone = Check::checkPhoneNumber(trim(isset($params['phone']) ? $params['phone'] : ''));
$messageContent = Check::check(trim(isset($params['message']) ? $params['message'] : ''), 1, 500);    //留言信息
$messageContent = Util::filterContentReplace($messageContent);

try {
    $lookForFamilyDB = new CDBLookForFamily();

    $taskResult = $lookForFamilyDB->getTaskResult($taskResultId);
    if ($taskResult === null) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    if (!$lookForFamilyDB->verifyTaskIdAndUserId($taskResult['taskId'], $userId)) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $findContents = $taskResult['findContents'];

    $findContents = json_decode($findContents, true);

    if (count($findContents) <= $familyIndex) {
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "该家族索引不存在");
        exit;
    }

    $familyId = $findContents[$familyIndex]['familyId'];        //这是要联系的家族id

    $familyDB = new CDBFamily();

    //向所有的家族管理员都发送这条消息
    $adminsId = $familyDB->getFamilyAdminId($familyId);
    //生成随机消息码
    $messageCode = Util::generateRandomCode(36);

    $messageDB = new CDBMessage();
    foreach ($adminsId as $adminId) {
        $message = new Message();
        $message->fromUser = $userId;
        $message->toUser = $adminId['adminId'];
        $message->content = $phone . "|" . $messageContent;
        $message->module = $GLOBALS['LOOK_FOR_FAMILY_MODULE'];
        $message->action = $GLOBALS['LOOK_FOR_FAMILY_CONTACT'];
        $message->recordId = $familyId;
        $message->messageCode = $messageCode;
        $messageId = $messageDB->addMessage($message);
    }

    $data['info'] = "请求发送成功";

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
