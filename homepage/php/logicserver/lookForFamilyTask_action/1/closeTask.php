<?php
/**
 * 关闭任务
 * @date 2018-01-17
 * @author jiangpengfei 
 */
use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Model\LookForFamilyTask;

//updateTaskSetting($taskId,$priority,$threshold,$cycle,$email,$userId){

$taskId = Check::checkInteger($params['taskId'] ?? '');
$isFinish = -1;
$userId = $GLOBALS['userId'];   


try{

    $lookForFamilyDB = new CDBLookForFamily();

    if(!$lookForFamilyDB->verifyTaskIdAndUserId($taskId,$userId)){
        //如果没有权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
        exit;
    }

    $updateRow = $lookForFamilyDB->updateTaskStatus($taskId,$isFinish,$userId);

    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
