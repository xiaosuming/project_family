<?php
/**
 * 添加任务
 * @date 2018-01-17
 * @author jiangpengfei 
 */
use DB\CDBLookForFamily;
use Util\Util;
use Util\Check;
use Model\LookForFamilyTask;
use DB\CDBFamily;

//(task_content,priority,threshold,email,is_finish,create_by,create_time,update_by,update_time) 

$taskContent = $params['taskContent'] ?? '';    //任务内容
$threshold = Check::checkInteger($params['threshold'] ?? 70);   //阈值
$email = Check::checkEmail($params['email'] ?? '');             //通知邮箱
$priority = Check::checkInteger($params['priority'] ?? 5);      //优先级
$cycle = Check::checkInteger($params['cycle'] ?? 5);            //周期

$userId = $GLOBALS['userId'];   

if($threshold < 70 || $threshold > 100){
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"阈值的大小错误");
    exit();
}

if($priority < 1 || $priority > 5){
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"任务优先级大小错误");
    exit();
}

if($cycle < 1 || $cycle > 28){
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"任务周期大小错误");
    exit();
}

//一个账号只能提交一次寻亲任务
$content = json_decode($taskContent,true);

if($content == NULL){
    //格式不正确
    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"任务内容格式不正确");
    exit();
}else{
    if(!isset($content['persons']) || count($content['persons']) <= 0 || !isset($content['persons'][0]['name']) || $content['persons'][0]['name'] === "" ) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"至少有一个人物");
        exit();
    }

    if(count($content['persons']) > 5){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"最多只能5个人物");
        exit();
    }

    if(!isset($content['lastname'])){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"必须包含家族姓氏");
        exit();
    }

    foreach($content['persons'] as $person) {
        if (!isset($person['name']) || $person['name'] == '') {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"人物必须包含姓名");
            exit();
        }

        if (isset($person['address']) && $person['address'] == '') {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"地址提交字段不能为空字符串");
            exit();
        }

        if (isset($person['birthday']) && $person['birthday'] == '') {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'],"生日提交字段不能为空字符串");
            exit();
        }
    }
}

try{
    $lookForFamilyDB = new CDBLookForFamily();
    $tasks = $lookForFamilyDB->getAllTask($userId,0);

    if(count($tasks) > 0){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],"一个用户同时只能有一个运行中的任务");
        exit();
    }

    $task = new LookForFamilyTask();
    $task->taskContent = $taskContent;
    $task->threshold = $threshold;
    $task->priority = $priority;
    $task->cycle = $cycle;
    $task->email = $email;
    $task->isFinish = 0;
    $task->createBy = $userId;
    $task->updateBy = $userId;

    //查询出用户所在的家族，放入exclude_familyIds中
    $familyDB = new CDBFamily();
    $familys = $familyDB->getJoinFamilyList($userId);
    $familyIdStr = "";
    foreach($familys as &$family){
        $familyIdStr .= $family['id'].',';
    }

    $task->excludeFamilyIds = $familyIdStr;

    $taskId = $lookForFamilyDB->addTask($task);

    $data['taskId'] = $taskId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
