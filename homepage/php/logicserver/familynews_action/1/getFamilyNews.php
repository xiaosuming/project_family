<?php 
    use Util\Util;
    use Util\Check;
    use DB\CDBFamilyNews;

    $familyNewsId = Check::checkInteger(trim(isset($params['familyNewsId']) ? $params['familyNewsId'] : ''));

    try{
        if($familyNewsId!=""){
            $familyNewsDB = new CDBFamilyNews();
            //只有家族成员才能获取本家族新鲜事
            $userId = $GLOBALS['userId'];
            if(!$familyNewsDB->verifyUserIdAndFamilyNewsIdReadPermission($userId,$familyNewsId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $news = $familyNewsDB->getFamilyNews($familyNewsId);
            $data['familyNews'] = $news;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }		
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }