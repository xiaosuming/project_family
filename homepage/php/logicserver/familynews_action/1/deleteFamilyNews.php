<?php 
    use Util\Util;
    use Util\Check;
    use DB\CDBFamilyNews;

    $familyNewsId = Check::checkInteger(trim(isset($params['familyNewsId']) ? $params['familyNewsId'] : ''));

    try{
        if($familyNewsId!=""){
            $familyNewsDB = new CDBFamilyNews();
            //只有家族管理员和创始人有权限删除家族新鲜事
            $userId = $GLOBALS['userId'];
            if(!$familyNewsDB->verifyUserIdAndFamilyNewsIdManagePermission($userId,$familyNewsId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $deleteRows = $familyNewsDB->deleteFamilyNews($familyNewsId);
            $data['isDeleted'] = $deleteRows > 0 ? true : false;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }		
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }