<?php 
    use Util\Util;
    use Util\Pager;
    use Util\Check;
    use DB\CDBFamilyNews;

    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : ''));
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : ''));
    $userId = $GLOBALS['userId'];
    try{
        $familyNewsDB = new CDBFamilyNews();
        if($pageIndex != "" && $pageSize != ""){
            $newsCount = $familyNewsDB->getMyFamilyNewsCount($userId);
            $news = $familyNewsDB->getMyFamilyNewsPaging($pageIndex,$pageSize,$userId);
            $paging = new Pager($newsCount,$news,$pageIndex,$pageSize);
            $paging->printPage();
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }