<?php 
    use Util\Util;
    use Util\Check;
    use DB\CDBFamilyNews;
    use DB\CDBPerson;
    use DB\CDBFamily;

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

    try{
        $familyDB = new CDBFamily();	
        if($familyId != ""){
            //只有家族成员才能获取本家族新鲜事
            $userId = $GLOBALS['userId'];
            if(!$familyDB->isUserForFamily($familyId, $userId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $familyNewsDB = new CDBFamilyNews();
            $newsCount = $familyNewsDB->queryFamilyNewsCount($familyId);
            $data['newsCount'] = $newsCount;
            Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
            exit;
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }