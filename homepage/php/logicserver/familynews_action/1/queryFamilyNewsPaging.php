<?php 
    use Util\Util;
    use Util\Pager;
    use Util\Check;
    use DB\CDBFamilyNews;
    use DB\CDBPerson;
    use DB\CDBFamily;

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : ''));
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : ''));

    try{
        if($familyId != "" && $pageIndex != "" && $pageSize != ""){
            //只有家族成员才能获取本家族新鲜事
            $userId = $GLOBALS['userId'];
            $familyDB = new CDBFamily();
            if(!$familyDB->isUserForFamily($familyId, $userId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $familyNewsDB = new CDBFamilyNews();
            $newsCount = $familyNewsDB->queryFamilyNewsCount($familyId);
            $news = $familyNewsDB->queryFamilyNewsPaging($pageIndex,$pageSize,$familyId);
            $paging = new Pager($newsCount,$news,$pageIndex,$pageSize);
            $paging->printPage();
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }