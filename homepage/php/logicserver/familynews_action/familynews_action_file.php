<?php
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $familynews_action_file_array['deleteFamilyNews'] = "logicserver/familynews_action/{$version}/deleteFamilyNews.php";
    $familynews_action_file_array['queryFamilyNewsPaging'] = "logicserver/familynews_action/{$version}/queryFamilyNewsPaging.php";
    $familynews_action_file_array['getFamilyNewsCount'] = "logicserver/familynews_action/{$version}/getFamilyNewsCount.php";
    $familynews_action_file_array['getFamilyNews'] = "logicserver/familynews_action/{$version}/getFamilyNews.php";
    $familynews_action_file_array['getMyFamilyNewsPaging'] = "logicserver/familynews_action/{$version}/getMyFamilyNewsPaging.php";