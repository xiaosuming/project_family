<?php

use Util\Util;
use Util\Pager;
use Util\Check;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBFamily;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
$pageIndex = Check::checkInteger(trim(isset($params['page']) ? $params['page'] : 1));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 3));
$nickname = Check::check(isset($params['nickname']) ? trim($params['nickname']) : null,1,30);

if ($pageSize >= 100){
    $pageSize = 100;
}

try {
    if ($familyId != "" && $pageIndex != "" && $pageSize != "") {
        //只有家族成员才能获取本家族新鲜事
        $userId = $GLOBALS['userId'];
        $familyDB = new CDBFamily();
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $familyNewsDB = new CDBFamilyNews();
        $result = $familyNewsDB->queryFamilyNewsPagingV3($pageIndex, $pageSize, $familyId, $nickname);
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
