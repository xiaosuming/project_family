<?php
    /**
     * 地图操作索引文件
     */

    /**
     * whiteList是白名单，用来允许某些请求通过验证
     */
    $version = $params['version'] ?? "1";
    $whiteList['getGraveNoLogin'] = 'allow';
    $whiteList['getGraveInfoPagingNoLogin'] = 'allow';

    require_once("filter/filter.php");
    $grave_action_file_array['addGrave'] = "logicserver/grave_action/{$version}/addGrave.php";
    $grave_action_file_array['deleteGrave'] = "logicserver/grave_action/{$version}/deleteGrave.php";
    $grave_action_file_array['updateGrave'] = "logicserver/grave_action/{$version}/updateGrave.php";
    $grave_action_file_array['getGrave'] = "logicserver/grave_action/{$version}/getGrave.php";
    $grave_action_file_array['getGravesInfoPaging']="logicserver/grave_action/{$version}/getGravesInfoPaging.php";
    $grave_action_file_array['getGraveInfoByPersonId'] = "logicserver/grave_action/{$version}/getGraveInfoByPersonId.php";
    $grave_action_file_array['getUnCreateGravePersonList'] = "logicserver/grave_action/{$version}/getUnCreateGravePersonList.php";
    $grave_action_file_array['getGraveNoLogin'] = "logicserver/grave_action/{$version}/getGraveNoLogin.php";
    $grave_action_file_array['getGraveInfoPagingNoLogin'] = "logicserver/grave_action/{$version}/getGraveInfoPagingNoLogin.php";