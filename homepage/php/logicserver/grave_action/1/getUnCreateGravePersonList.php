<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-8-23
 * Time: 下午3:15
 */

use DB\CDBFamily;
use DB\CDBGrave;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger($params['familyId'] ?? '');

try {
    //权限判断
    $familyDB = new CDBFamily();
    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }
    $graveDB = new CDBGrave();
    $result = $graveDB->getUnCreateGravePersonListByFamilyId($familyId);
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}