<?php
use DB\CDBFamily;
use DB\CDBGrave;
use Util\Check;
use Util\Util;

$graveId = Check::checkInteger(trim(isset($params['graveId']) ? $params['graveId'] : '')); //祖坟id

try {
    if ($graveId != "") {

        $graveDB = new CDBGrave();
        $userId = $GLOBALS['userId'] ?? 0;
        $grave = $graveDB->getGrave($graveId);

        if ($grave == null){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }
        $familyDB = new CDBFamily();
        $family = $familyDB->getFamilyById($grave->familyId);

        if ($family == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
            exit;
        }

        if ($family->openType == 0) {
            //验证用户属于家族,即属于家族的用户都有权限查看祖坟
            if (!$familyDB->isUserForFamily($grave->familyId, $userId)) {
                //如果没有权限
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
                exit;
            }
        }
        $photo = $grave->photo;
        $grave->photo = json_decode($photo);

        $data['grave'] = $grave;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
