<?php

use DB\CDBPushGrave;
use Util\Util;
use DB\CDBGrave;
use Util\Check;

$graveId = Check::checkInteger(trim(isset($params['graveId']) ? $params['graveId'] : ''));        //祖坟id

try {
    if ($graveId != "") {

        $graveDB = new CDBGrave();
        $userId = $GLOBALS['userId'];

        if (!$graveDB->verifyUserIdAndGraveId($userId, $graveId)) {
            //如果没有权限
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
            exit;
        }
        $deleteCount = $graveDB->deleteGrave($graveId);

        if ($deleteCount > 0) {
            if ($GLOBALS['TEST_GRAVE_TASK']) {
                $CDBPushGrave = new CDBPushGrave();
                $modelId = $graveId;
                $action = 3;
                $source = [];
                $CDBPushGrave->setPushGraveTask($modelId, $action, $source);
            }
            $data['deleteCount'] = $deleteCount;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}