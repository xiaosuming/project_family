<?php

use DB\CDBPushGrave;
use Util\Util;
use DB\CDBGrave;
use Util\Check;

$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));                        //文件
$graveId = Check::checkInteger(trim(isset($params['graveId']) ? $params['graveId'] : ''));        //人物id
$coorX = Check::check(trim(isset($params['coorX']) ? $params['coorX'] : ''));                      //横坐标
$coorY = Check::check(trim(isset($params['coorY']) ? $params['coorY'] : ''));                      //纵坐标
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''), 1, 100);                 //地名
$description = Check::check(trim(isset($params['description']) ? $params['description'] : ''), 0, 500);     //描述

try {
    if ($graveId != "" && $coorX != "" && $coorY != "" && $address != "") {
        $graveDB = new CDBGrave();
        $userId = $GLOBALS['userId'];

        if (!$graveDB->verifyUserIdAndGraveId($userId, $graveId)) {
            //如果没有权限
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
            exit;
        }
        if ($photo != '') {
            $photoArr = explode(',', $photo);
            $photoJsonStr = json_encode($photoArr);
        } else {
            $photoJsonStr = json_encode(array());
        }

        $updateCount = $graveDB->updateGrave($graveId, $coorX, $coorY, $address, $photoJsonStr, $description, $userId);

        if ($updateCount > 0) {
            if ($GLOBALS['TEST_GRAVE_TASK']) {
                $CDBPushGrave = new CDBPushGrave();
                $modelId = $graveId;
                $action = 2;
                $source = [
                    'id' => $graveId,
                    'photo' => $photo,
                    'address' => $address,
                    'description' => $description
                ];
                $CDBPushGrave->setPushGraveTask($modelId, $action, $source);
            }
            $data['updateCount'] = $updateCount;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}