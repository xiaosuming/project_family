<?php

use DB\CDBPushGrave;
use Util\Util;
use DB\CDBGrave;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : 'https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIDKAVXKmAACCQrzaxdw195.jpg'));                        //配图地址
$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));        //人物id
$coorX = Check::check(trim(isset($params['coorX']) ? $params['coorX'] : ''));                      //横坐标
$coorY = Check::check(trim(isset($params['coorY']) ? $params['coorY'] : ''));                      //纵坐标
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''), 1, 100);                 //地名
$description = Check::check(trim(isset($params['description']) ? $params['description'] : ''), 0, 500);     //描述

try {
    if ($personId != "" && $coorX != "" && $coorY != "" && $address != "") {
        $personDB = new CDBPerson();
        $userId = $GLOBALS['userId'];
        $person = $personDB->getPersonById($personId);      //先获取人物id

        if ($person == null) {
            //如果人物不存在
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
            exit;
        }

        //验证用户属于家族,即属于家族的用户都有权限创建祖坟
        $familyDB = new CDBFamily();
        if (!$familyDB->isUserForFamily($person->familyId, $userId)) {
            //如果没有权限
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限不足");
            exit;
        }

        $graveDB = new CDBGrave();
        //检查该人物是否有祖坟,防止重复创建
        if ($graveDB->checkPersonGraveExist($personId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "不要重复创建");
            exit;
        }

        if ($photo != '') {
            $photoArr = explode(',', $photo);
            $photoJsonStr = json_encode($photoArr);
        } else {
            $photoJsonStr = json_encode(array());
        }

        $graveId = $graveDB->addGrave($personId, $person->name, $person->familyId, $coorX, $coorY, $address, $photoJsonStr, $description, $userId);

        if ($graveId > 0) {
            if ($GLOBALS['TEST_GRAVE_TASK']) {
                $CDBFamily = new CDBFamily();
                $family = $CDBFamily->getFamilyById($person->familyId);
                $familyName = $family->name;
                $CDBPushGrave = new CDBPushGrave();
                $modelId = $graveId;
                $action = 1;
                $source = [
                    'id' => $graveId,
                    'familyId' => $person->familyId,
                    'familyName' => $familyName,
                    'photo' => $photo,
                    'address' => $address,
                    'description' => $description
                ];
                $CDBPushGrave->setPushGraveTask($modelId, $action, $source);
            }
            $data['graveId'] = $graveId;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}