<?php

use Util\Util;
use Util\Check;
use DB\CDBGrave;
use Model\Grave;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));	    //人物id

try{
    if($personId == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $graveDB = new CDBGrave();
    $grave = $graveDB->getGraveInfoByPersonId($personId);
    foreach($grave as $key=>$item){
        $photo=$item['photo'];
        $grave[$key]['photo']=json_decode($photo);
    }
    $data['grave'] = $grave;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}