<?php
/**
 * 
 */

use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;
use Util\Util;
use DB\CDBImageProcess;
use Util\Check;
use Util\ImageProcess;
use Util\FileUtil;
use Util\TaskQueue\Producer;

$file = Check::checkImageType(isset($_FILES['file']) ? $_FILES['file'] : '');           //上传的文件
$needSize = isset($params['needSize']) ? $params['needSize'] : '';                      //是否需要回传图片尺寸,1代表需要
$familyId = Check::checkInteger($params['familyId'] ?? '');
$needCrop = $params['needCrop'] ?? '';                                                  //是否需要裁剪,1代表需要
$cropWidth = $params['cropWidth'] ?? '';                                                //裁剪的宽度
$cropHeight = $params['cropHeight'] ?? '';                                              //裁剪的高度
$cropX = $params['cropX'] ?? '';                                                        //裁剪的起点x
$cropY = $params['cropY'] ?? '';                                                        //裁剪的起点y

$needCompression = $params['needCompression'] ?? '';                                    //是否需要压缩,1代表需要

$userId = $GLOBALS['userId'] ?? 0;                                                      //用户id
$result = $params['result'] ?? '';


try {

    if ($file == '') {
        Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], "图片不存在");
        exit();
    }

    $type = FileUtil::getFileType($file);

    if ($type == 6) {

        $client = new StorageFacadeClient();
        $client->setBucket(StorageBucket::$AI_IMAGE);
        $fileInfo = $client->upload($file['tmp_name'], 'gif');

        if (!$fileInfo->success) {
            Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], "图片上传错误");
            exit();
        }

        $path = $fileInfo->location;


    } else {

        $imageInfo = ImageProcess::processImageToBlobStrategy($file, $needCompression == 1, $needCrop == 1, $cropWidth, $cropHeight, $cropX, $cropY);

        if (!$imageInfo) {
            Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], "文件格式不支持");
            exit();
        }

        /* 实例化文件客户端 */
        $client = new StorageFacadeClient();
        $client->setBucket(StorageBucket::$AI_IMAGE);

        //直接上传二进制到服务器上
        $fileInfo = $client->uploadBlob($imageInfo['blob'], strtolower($imageInfo['extName']));

        if (!$fileInfo->success) {
            Util::printResult($GLOBALS['ERROR_FILE_UPLOAD'], "图片上传错误");
            exit();
        }

        $path = $fileInfo->location;

        //是否需要尺寸
        //if($needSize === "1"){
        $path = $path . "?width=" . $imageInfo['width'] . "&height=" . $imageInfo['height'];
        //}

    }


    $imageProcessDB = new CDBImageProcess();
    //保存到数据库中作为文件记录
    $taskId = $imageProcessDB->addAIImageProcess($familyId, $path, $result, $userId);

    if ($taskId > 0) {

        $task['id'] = $taskId;
        $task['url'] = $path;
        // 把任务添加到rabbitmq
        $producer = new Producer();
        $producer->produce('ai_image_process', json_encode($task));

        $fileType = FileUtil::getFileType($file);
        $data['location'] = $path;
        $data['fileType'] = $fileType;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], $data);
    }

} catch (\Exception $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

    