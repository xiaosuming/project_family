<?php
/**
 * 更新处理的结果
 * @author: jiangpengfei
 * @date:   2019-03-13
 */

use DB\CDBImageProcess;
use Util\Util;
use Util\Check;

$taskId = Check::checkInteger($params['taskId'] ?? '');
$result = Check::check($params['result'] ?? '');

try {
    $imageProcessDB = new CDBImageProcess();
    $update = $imageProcessDB->updateImageProcessResult($taskId, $result);

    $data['update'] = $update;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}