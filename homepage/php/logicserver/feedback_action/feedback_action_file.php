<?php
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $feedback_action_file_array['commitFeedback'] = "logicserver/feedback_action/{$version}/commitFeedback.php";
    $feedback_action_file_array['handleFeedback'] = "logicserver/feedback_action/{$version}/handleFeedback.php";
    $feedback_action_file_array['getFeedback'] = "logicserver/feedback_action/{$version}/getFeedback.php";
    $feedback_action_file_array['getFeedbackPaging'] = "logicserver/feedback_action/{$version}/getFeedbackPaging.php";