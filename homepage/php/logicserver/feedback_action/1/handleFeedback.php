<?php
    /**
    *提交用户反馈
    *@Coded by zch
    *@time 2017年8月10日
    */
    use Util\Util;
    use DB\CDBFeedback;
    use Util\Check;

    $userId = $GLOBALS['userId'];  //获得用户id
    $feedbackId = Check::checkInteger(trim(isset($params['feedbackId']) ? $params['feedbackId'] : ''));     //反馈事件ID
    $status = Check::checkInteger(trim(isset($params['status']) ? $params['status'] : ''));     //获取反馈状态
    $remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : ''));     //备注

    if($feedbackId == "" || $status == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'] , '反馈信息为空');
        exit;
    }

    try{
        $feedbackDB = new CDBFeedback();
        $updateCount = $feedbackDB->handleFeedbackMessage($userId, $feedbackId, $status, $remark);
        
        if($updateCount > 0){
            $data['updateCount'] = $updateCount;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }
        else{
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'] , "提交出错");
            exit;
        }
    }catch(PDOException $e){
            $logger->error(Util::exceptionFormat($e));
            Util::printResult($GLOBALS['ERROR_EXCEPTION'] , '出现异常');
    }
    