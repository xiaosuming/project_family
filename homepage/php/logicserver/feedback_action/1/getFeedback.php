<?php
    /**
    *获取反馈信息
    *@Coded by zch
    *@日期 2017年8月10日
    */
    use Util\Util;
    use DB\CDBFeedback;
    use Util\Check;

    $userId = $GLOBALS['userId'];  //获得用户id
    $feedbackId = Check::checkInteger(trim(isset($params['feedbackId']) ? $params['feedbackId'] : ''));     //反馈事件ID

     try{
        $feedbackDB = new CDBFeedback();

        $message = $feedbackDB->getFeedbackMessage($feedbackId);
        
        if($message != null)
        {
            $data['userId'] = $message['userId'];
            $data['contact'] = $message['contact'];
            $data['feedbackMessage'] = $message['feedbackMessage'];
            $data['handleStatus'] = $message['handleStatus'];
            $data['remark'] = $message['remark'];
            $data['problemType'] = $message['problemType'];
            $data['photo'] = $message['photo'];
            $data['createBy'] = $message['createBy'];
            $data['createTime'] = $message['createTime'];
            $data['updateBy'] = $message['updateBy'];
            $data['updateTime'] = $message['updateTime'];
            $data['clientFrom'] = $message['clientFrom'];
            $data['versionNumber'] = $message['versionNumber'];
            
            Util::printResult($GLOBALS['ERROR_SUCCESS'] , $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'] , "查询出错");
            exit;
        }
    }catch(PDOException $e){
            $logger->error(Util::exceptionFormat($e));
            Util::printResult($GLOBALS['ERROR_EXCEPTION'] , '出现异常');
    }
    