<?php
    /**
    *提交用户反馈
    *@author kellen
    *@time 2017.3.14
    */
    use Util\Util;
    use DB\CDBFeedback;
    use Util\Check;

    $userId = $GLOBALS['userId'];  //获得用户id
    $contact = Check::check(trim(isset($params['contact']) ? $params['contact'] : ''));     //联系方式
    $message = Check::check(trim(isset($params['message']) ? $params['message'] : ''),10,500);     //获取反馈信息
    $problemType = Check::checkInteger(trim(isset($params['problemType']) ? $params['problemType'] : ''));     //问题类型
    $uploadPhoto = trim(isset($params['uploadPhoto']) ? $params['uploadPhoto'] : '');     //图片url
    $clientFrom=Check::checkInteger(trim(isset($params['clientFrom']) ? $params['clientFrom'] : 0)); //来自于什么端 1.web端 2.手机网页端 3.安卓端 4.ios端
    $versionNumber = Check::check(trim($params['versionNumber'] ?? 0)); //版本号 默认为0

    if($contact == "" || $message == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'] , '缺少参数');
        exit;
    }

    try{

        $feedbackDB = new CDBFeedback();
        /*五个参数
        /*@contact
        /*@message
        /*@problemtype
        /*@uploadPhoto

         */

        $pathJsonStr = "";
        if($uploadPhoto != ""){
            //这里将photo解析成image数组
            $pathArray = explode(",",$uploadPhoto);
            $pathJsonStr = json_encode($pathArray);
        }else{
            $pathJsonStr = json_encode(array());
        }

        $feedbackId = $feedbackDB->addFeedback($userId,$contact,$message,$problemType,$pathJsonStr,$clientFrom,$versionNumber);

        if($feedbackId > 0)
        {
            $data['feedbackId'] = $feedbackId;
            Util::printResult($GLOBALS['ERROR_SUCCESS'] , $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'] , "提交出错");
            exit;
        }
    }catch(PDOException $e){
            $logger->error(Util::exceptionFormat($e));
            Util::printResult($GLOBALS['ERROR_EXCEPTION'] , '出现异常');
    }
    