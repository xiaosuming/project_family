<?php
    /**
    *获取反馈信息分页
    *@Coded by zch
    *@日期 2017年8月10日
    */
    use Util\Util;
    use DB\CDBFeedback;
    use Util\Check;
    use Util\Pager;

    $userId = $GLOBALS['userId'];  //获得用户id
    $status = Check::checkInteger(trim(isset($params['status']) ? $params['status'] : ''));     //反馈事件状态
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1'));     //页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '10'));     //每页大小


     try{
        $feedbackDB = new CDBFeedback();

        $total = $feedbackDB->getFeedbackMessageCountByStatus($status);
        $feedbackMessage = $feedbackDB->getFeedbackMessagePaging($status,$pageIndex,$pageSize);
                
        $paper = new Pager($total, $feedbackMessage, $pageIndex, $pageSize);
        $paper->printPage();
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }