<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-12-29
 * Time: 下午4:15
 */

/**
 * whiteList是白名单，用来允许某些请求通过验证
 */
$whiteList['getGreetingCard'] = 'allow';
$version = $params['version'] ?? "1";
require_once("filter/filter.php");
$greetingCard_action_file_array['addGreetingCard'] = "logicserver/greetingCard_action/{$version}/addGreetingCard.php";
$greetingCard_action_file_array['getUserGreetingCard'] = "logicserver/greetingCard_action/{$version}/getUserGreetingCard.php";
$greetingCard_action_file_array['getGreetingCard'] = "logicserver/greetingCard_action/{$version}/getGreetingCard.php";
$greetingCard_action_file_array['editGreetingCard'] = "logicserver/greetingCard_action/{$version}/editGreetingCard.php";
$greetingCard_action_file_array['delGreetingCard'] = "logicserver/greetingCard_action/{$version}/delGreetingCard.php";

