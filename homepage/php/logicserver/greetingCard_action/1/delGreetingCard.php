<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-1-2
 * Time: 上午10:34
 */

use DB\CDBGreetingCard;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];

$greetingCardId = Check::checkInteger($params['greetingCardId'] ?? ''); //贺卡id


try {

    // 检查用户是否有权限修改贺卡
    $cdbGreetingCard = new CDBGreetingCard();
    if (!$cdbGreetingCard->checkUserHasPermissionForGreetingCard($userId, $greetingCardId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '用户权限错误');
        exit;
    }

    $delRow = $cdbGreetingCard->deleteGreetingCard($greetingCardId);
    $info['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

