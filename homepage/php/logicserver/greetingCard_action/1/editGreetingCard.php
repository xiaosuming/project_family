<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-1-2
 * Time: 上午10:33
 */

use DB\CDBGreetingCard;
use DB\CDBInfoCard;
use Model\GreetingCard;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];

$greetingCardId = Check::checkInteger($params['greetingCardId'] ?? ''); //贺卡id
$infoCardId = Check::checkInteger($params['infoCardId'] ?? 0);
$title = Check::check($params['title'] ?? '', 1, 20);
$template = Check::checkInteger($params['template'] ?? '');
$data = $params['data'] ?? '';
$title = Util::filterContentReplace($title);

try {

    // 检查用户是否有权限修改贺卡
    $cdbGreetingCard = new CDBGreetingCard();
    if (!$cdbGreetingCard->checkUserHasPermissionForGreetingCard($userId, $greetingCardId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '用户权限错误');
        exit;
    }
    $greetingCardModel = new GreetingCard(array());
    $greetingCardModel->userId = $userId;
    $greetingCardModel->infoCardId = $infoCardId;
    $greetingCardModel->title = $title;
    $greetingCardModel->template = $template;
    $greetingCardModel->data = $data;
    $greetingCardModel->updateBy = $userId;

    $updateRow = $cdbGreetingCard->updateGreetingCardById($greetingCardModel,$greetingCardId);
    $info['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

