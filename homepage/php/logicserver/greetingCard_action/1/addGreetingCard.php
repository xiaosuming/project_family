<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-12-29
 * Time: 下午4:21
 */

use DB\CDBGreetingCard;
use DB\CDBInfoCard;
use Model\GreetingCard;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];

$infoCardId = Check::checkInteger($params['infoCardId'] ?? 0);
$title = Check::check($params['title'] ?? '', 1, 20);
$template = Check::checkInteger($params['template'] ?? '');
$data = $params['data'] ?? '';
$title = Util::filterContentReplace($title);

try {

    $cdbGreetingCard = new CDBGreetingCard();
    $greetingCardModel = new GreetingCard(array());
    $greetingCardModel->userId = $userId;
    $greetingCardModel->infoCardId = $infoCardId;
    $greetingCardModel->title = $title;
    $greetingCardModel->template = $template;
    $greetingCardModel->data = $data;

    $insertRow = $cdbGreetingCard->addGreetingCard($greetingCardModel);
    $info['insertRow'] = $insertRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $info);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

