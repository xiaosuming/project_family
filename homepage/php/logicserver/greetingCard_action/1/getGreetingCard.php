<?php
/**
 * Author: jiangpengfei
 * Date: 18-12-30
 */

use DB\CDBGreetingCard;
use DB\CDBInfoCard;
use Model\GreetingCard;
use Util\Check;
use Util\Util;

$greetingCardId = Check::checkInteger($params['greetingCardId']);

try {
    
    $greetingCardDB = new CDBGreetingCard();
    $result = $greetingCardDB->getGreetingCard($greetingCardId);

    $result->data = str_replace("\n", '\\n', $result->data);

    $result->data = json_decode($result->data, true);


    $data['card'] = $result;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

