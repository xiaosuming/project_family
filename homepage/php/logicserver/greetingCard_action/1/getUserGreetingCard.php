<?php
/**
 * Author: jiangpengfei
 * Date: 18-12-30
 */

use DB\CDBGreetingCard;
use DB\CDBInfoCard;
use Model\GreetingCard;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];

try {
    
    $greetingCardDB = new CDBGreetingCard();
    $result = $greetingCardDB->getUserGreetingCard($userId);

    $data['list'] = $result;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

