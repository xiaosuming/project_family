<?php
    /**
     * 微信操作索引文件
     */

    /**
     * whiteList是白名单，用来允许某些请求通过验证
     */

    $whiteList['getWechatUserInfo'] = "allow";
    $whiteList['getSignPackage'] = "allow";
    $whiteList['getMiniUserInfo'] = "allow";
    $whiteList['getMiniQrcode'] = "allow";
    $whiteList['decryptData'] = "allow";
    $version = $params['version'] ?? "1";
    require_once("filter/filter.php");
    $wechat_action_file_array['getMiniUserInfo'] = "logicserver/wechat_action/{$version}/getMiniUserInfo.php";
    $wechat_action_file_array['getWechatUserInfo'] = "logicserver/wechat_action/{$version}/getWechatUserInfo.php";
    $wechat_action_file_array['getSignPackage'] = "logicserver/wechat_action/{$version}/getSignPackage.php";
    $wechat_action_file_array['getMiniQrcode'] = "logicserver/wechat_action/{$version}/getMiniQrcode.php";
    $wechat_action_file_array['decryptData'] = "logicserver/wechat_action/{$version}/decryptData.php";