<?php
use Util\Util;
use ThirdParty\JSSDK;

$url = $params['url'] ?? '';

if ($url == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'],'参数缺失');
    exit();
}

$jssdk = new JSSDK();

$result = $jssdk->getSignPackage($url);
$data['sign'] = $result;
if($result){   
    Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
}else{
    Util::printResult($GLOBALS['ERROR_WECHAT_USERINFO'],"获取签名出错");
}