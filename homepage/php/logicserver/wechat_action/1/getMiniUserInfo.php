<?php
/**
 * 获取小程序的用户信息
 */
use Util\Util;
use ThirdParty\MiniProgram;
use Util\TempStorage;
use DB\CDBAccount;

$code = isset($params['code']) ? $params['code'] : "";
$type = $params['type'] ?? '1';     // 默认是递名帖

$mini = new MiniProgram($type);

$result = $mini->getUserInfo($code);
if($result){
    $res = json_decode($result,true);

    if(isset($res['errcode'])) {
        Util::printResult($GLOBALS['ERROR_WECHAT_USERINFO'],$res['errmsg']);
        exit;
    } else {
        $data['openid'] = $res['openid'];
        $data['session_key'] = $res['session_key'];       //微信的性别定义是1是男，2是女，本项目的性别定义是1是男，0是女
        $data['unionid'] = $res['unionid'] ?? '';
        //这里放到redis中，提供一个凭证给用户，用户提供凭证由服务器进行操作
        $auth = md5($data['openid']);
        $tempStorage = new TempStorage();
        if(!$tempStorage->setTemp($auth,json_encode($data),600)){    //设置10分钟的有效期
            $logger->error("redis插入数据异常");
            Util::printResult($GLOBALS['ERROR_INSERT'],"插入数据错误");
            exit();
        }

        // 检查openId是否已经有账号
        $accountDB = new CDBAccount();
        $accountExist = $accountDB->checkMiniOpenIdExist($data['openid'], $type);

        if (!$accountExist && $data['unionid'] != '') {
            // 检查unionid是否存在
            $accountExist = $accountDB->checkUnionIdExist($data['unionid']);
        }

        $printData['auth'] = $auth;
        $printData['hasUnionId'] = $data['unionid'] != '';
        $printData['accountExist'] = $accountExist;     // 账号是否存在
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$printData);
    }
}else{
    Util::printResult($GLOBALS['ERROR_WECHAT_USERINFO'],"获取用户信息出错");
    $logger->error($result.$code);
}