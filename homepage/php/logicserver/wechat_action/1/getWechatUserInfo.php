<?php
use Util\Util;
use Util\Wechat;
use Util\TempStorage;

$code = isset($params['code']) ? $params['code'] : "";

$wechat = new Wechat();
$wechat->setConfig($GLOBALS['appid'],$GLOBALS['secret']);

$result = $wechat->requestUserInfo($code);
if($result){
    $res = json_decode($result,true);

    if(isset($res['errcode']) && $res['errcode'] == 40003)
        Util::printResult($GLOBALS['ERROR_WECHAT_USERINFO'],$res['errmsg']);
    else{
        $data['username'] = $res['nickname'];
        $data['gender'] = $res['sex'] == 2 ? 0 : $res['sex'];       //微信的性别定义是1是男，2是女，本项目的性别定义是1是男，0是女
        $data['province'] = $res['province'];
        $data['city'] = $res['city'];
        $data['country'] = $res['country'];
        $data['photo'] = $res['headimgurl'];
        $data['openid'] = $res['openid'];
        $data['unionid'] = $res['unionid'] ?? '';
        //这里放到redis中，提供一个凭证给用户，用户提供凭证由服务器进行操作
        $auth = md5($data['openid']);
        $tempStorage = new TempStorage();
        if(!$tempStorage->setTemp($auth,json_encode($data),600)){    //设置10分钟的有效期
            $logger->error("redis插入数据异常");
            Util::printResult($GLOBALS['ERROR_INSERT'],"插入数据错误");
            exit();
        }

        $printData['photo'] = $data['photo'];
        $printData['auth'] = $auth;
        $printData['username'] = $data['username'];
        $printData['hasUnionId'] = $data['unionid'] == '';
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$printData);
    }
}else{
    Util::printResult($GLOBALS['ERROR_WECHAT_USERINFO'],"获取用户信息出错");
}