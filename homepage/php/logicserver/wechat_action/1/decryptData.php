<?php

use Util\Util;
use ThirdParty\MiniProgram;
use Util\TempStorage;

$encryptData = $params['encryptData'] ?? '';
$iv = $params['iv'] ?? '';
$type = $params['type'] ?? '1';     // 默认是递名帖,1是递名帖，2是AI族群
$auth = $params['auth'] ?? '';

if ($encryptData == '' || $iv == '' || $auth == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '参数缺少');
    exit;
}

$userData = '';

//从临时存储中获取用户资料
$storage = new TempStorage();
$userInfo = $storage->get($auth);

if($userInfo){
    $userInfo = json_decode($userInfo,true);
}else{
    Util::printResult($GLOBALS['ERROR_LOGIN'], "账号不存在");
    exit();
}

$sessionKey = $userInfo['session_key'];

$miniProgram = new MiniProgram($type);
$result = $miniProgram->decryptData($encryptData, $iv, $userData, $sessionKey);

if($result){
    $res = json_decode($userData,true);

    if(isset($res['errcode']) && $res['errcode'] == 40003)
        Util::printResult($GLOBALS['ERROR_WECHAT_USERINFO'],$res['errmsg']);
    else{
        $data['username'] = $res['nickName'];
        $data['gender'] = $res['gender'] == 2 ? 0 : $res['gender'];       //微信的性别定义是1是男，2是女，本项目的性别定义是1是男，0是女
        $data['province'] = $res['province'];
        $data['city'] = $res['city'];
        $data['country'] = $res['country'];
        $data['photo'] = $res['avatarUrl'];
        $data['openid'] = $res['openId'];
        $data['unionid'] = $res['unionId'];
        //这里放到redis中，提供一个凭证给用户，用户提供凭证由服务器进行操作
        $auth = md5($data['openid']);
        $tempStorage = new TempStorage();
        if(!$tempStorage->setTemp($auth,json_encode($data),600)){    //设置10分钟的有效期
            $logger->error("redis插入数据异常");
            Util::printResult($GLOBALS['ERROR_INSERT'],"插入数据错误");
            exit();
        }

        $printData['photo'] = $data['photo'];
        $printData['auth'] = $auth;
        $printData['username'] = $data['username'];
        Util::printResult($GLOBALS['ERROR_SUCCESS'],$printData);
    }
}else{
    Util::printResult($GLOBALS['ERROR_WECHAT_USERINFO'],"获取用户信息出错");
}
