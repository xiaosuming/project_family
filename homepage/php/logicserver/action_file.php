<?php
    $action_file_array['account_action'] = "logicserver/account_action.php";
    $action_file_array['person_action'] = "logicserver/person_action.php";
    $action_file_array['family_action'] = "logicserver/family_action.php";
    $action_file_array['event_action'] = "logicserver/event_action.php";
    $action_file_array['zone_action'] = "logicserver/zone_action.php";
    $action_file_array['familynews_action'] = "logicserver/familynews_action.php";
    $action_file_array['like_action'] = "logicserver/like_action.php";
    $action_file_array['feedback_action'] = "logicserver/feedback_action.php";
    $action_file_array['message_action'] = "logicserver/message_action.php";
    $action_file_array['file_action'] = "logicserver/file_action.php";
    $action_file_array['grave_action'] = "logicserver/grave_action.php";
    $action_file_array['test_action'] = "logicserver/test_action.php";
    $action_file_array['activity_action'] = "logicserver/activity_action.php";
    $action_file_array['legend_action'] = "logicserver/legend_action.php";
    $action_file_array['heirloom_action'] = "logicserver/heirloom_action.php";
    $action_file_array['collection_action'] = "logicserver/collection_action.php";
    $action_file_array['wechat_action'] = "logicserver/wechat_action.php";
    $action_file_array['invitation_action'] = "logicserver/invitation_action.php";
    $action_file_array['area_action'] = "logicserver/area_action.php";
    $action_file_array['album_action'] = "logicserver/album_action.php";
    $action_file_array['familyCelebrities_action'] = "logicserver/familyCelebrities_action.php";
    $action_file_array['infocard_action'] = "logicserver/infocard_action.php";
    $action_file_array['question_action'] = "logicserver/question_action.php";
    $action_file_array['task_action'] = "logicserver/task_action.php";
    $action_file_array['lookForFamilyTask_action'] = "logicserver/lookForFamilyTask_action.php";
    $action_file_array['family_storage_action'] = "logicserver/family_storage_action.php";
    $action_file_array['information_action'] = "logicserver/information_action.php";
    $action_file_array['greetingCard_action'] = "logicserver/greetingCard_action.php";
    $action_file_array['ai_action'] = "logicserver/ai_action.php";
    $action_file_array['qrcode_action'] = "logicserver/qrcode_action.php";

    //add
    $action_file_array['familycover_action'] = "logicserver/familycover_action.php";