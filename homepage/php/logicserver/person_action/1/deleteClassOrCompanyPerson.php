<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-14
 * Time: 下午2:27
 */

use DB\CDBEvent;
use DB\CDBFamily;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use Util\Check;
use Util\Util;
use Model\Person;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
$userId = $GLOBALS['userId'];

try {

    if ($personId != "") {
        $personDB = new CDBPerson();
        //检查操作的权限
        if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $person = $personDB->getPersonById($personId);        //在删除之前先获取

        if ($person->userId > 0) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "请先将用户和该人物解除绑定");
            exit;
        }

        $familyDB = new CDBFamily();
        //检查人物是否是家族的创始人
        if ($familyDB->isOriginatorForFamily($person->familyId, $person->userId)) {
            //无论如何创始人都是无法直接删除的
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $operation = $personDB->deleteClassOrCompanyPerson($personId);
        $data['operation'] = $operation;

        //前台需要判断是否有修改
        if ($operation) {
            //删出人物相关的大事件
            $CDBEvent = new CDBEvent();

            $CDBEvent->deleteEventByPersonId($personId);

            $familyNewsDB = new CDBFamilyNews();

            $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['DELETEPERSON'], $userId, $personId, $person->name, $userId);

            if ($GLOBALS['TEST_PERSON_TASK']) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 3;
                $source = [];
                $CDBPushPerson->setPushListForDeletePerson($modelId, $action, $source);
            }

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
