<?php
/**
 * 向师徒关系中添加人物
 * Created by PhpStorm.
 * User: xll
 * Date: 19-2-14
 * Time: 上午11:04
 */

use DB\CDBFamily;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use Util\Check;
use Util\PhotoGenerator;
use Util\Util;
use Model\Person;

$corePersonId = Check::checkInteger($params['corePersonId'] ?? ''); //中心人物
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''), 1, 30);  //名字               //必须
$gender = Check::checkInteger(trim(isset($params['gender']) ? $params['gender'] : ''));//必须 性别
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : '')); //头像
$relation = Check::checkInteger($params['relation'] ?? ''); //关系 1,男师傅 2，女师傅 3,师兄弟 4,师姐妹 6，男徒弟 7女徒弟
$userId = $GLOBALS['userId'];

try {

    if ($name != "" && $gender != "" && $corePersonId != "") {

        //检查人物照片是否定义，如果没有则随机产生一个照片
        if ($photo == "") {
            $photo = PhotoGenerator::generatePersonPhotoFromResource();
        } else {

            //TODO: 兼容老版本,之后可以删除
            if (strpos($photo, "img1") > 0) {
                $photo = str_replace("img1", "image", $photo);
            }
        }
        //检查姓名与关系
        switch ($relation) {
            case 1:
                if ($gender != 1) {
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
                    exit;
                }
                break;
            case 2:
                if ($gender != 0) {
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
                    exit;
                }
                break;
            case 3:
                if ($gender != 1) {
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
                    exit;
                }
                break;
            case 4:
                if ($gender != 0) {
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
                    exit;
                }
                break;
            case 6:
                if ($gender != 1) {
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
                    exit;
                }
                break;
            case 7:
                if ($gender != 0) {
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
                    exit;
                }
                break;
            default:
                Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '人物关系不正确');
                exit;
        }

        $personDB = new CDBPerson();
        $personDetail = $personDB->getPersonById($corePersonId);
        if ($personDetail == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $familyId = $personDetail->familyId;        //获取PersonDetail对象中的familyId

        $userId = $GLOBALS['userId'];
        //检查操作的权限
        $familyDB = new CDBFamily();
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        // 检查家族类型是否是家庭,4是师徒
        $familyDetail = $familyDB->getFamilyById($familyId);
        if ($familyDetail->groupType != 4) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
            exit;
        }

        $person = new Person();
        $person->id = Util::getNextId();
        $person->corePersonId = $corePersonId;
        $person->name = $name;
        $person->gender = $gender;
        $person->photo = $photo;
        $person->familyId = $familyId;
        $person->relation = $relation;

        $personId = $personDB->addPersonForMentor($person);
        $person = $personDB->getPersonById($personId);
        $data['addedPersonId'] = $personId;
        if ($personId > 0) {

            $familyNewsDB = new CDBFamilyNews();

            $familyNewsDB->addFamilyNews($familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['ADDRELATEPERSON'], $userId, $personId, $name, $userId);

            if ($GLOBALS['TEST_PERSON_TASK']) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 1;
                $source = $person;
                $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
            }
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "添加错误");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

