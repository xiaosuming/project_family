<?php
    /**
     * 为人物添加工作经历
     * @author jiangpegnfei
     * @date : 2017/04/13
     */

    //


    use DB\CDBPushPerson;
    use Util\Util;
    use DB\CDBPerson;
    use Util\Check;

    $personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));		    //人物id
    $company = Check::check(trim(isset($params['company']) ? $params['company'] : ''),1,100);             //公司
    $occupation = Check::check(trim(isset($params['occupation']) ? $params['occupation'] : ''),1,100);	//职位
    $startTime = Check::check(trim(isset($params['startTime']) ? $params['startTime'] : ''));//开始时间
    $endTime = Check::check(trim(isset($params['endTime']) ? $params['endTime'] : ''));      //结束时间
    $userId = $GLOBALS['userId'];

    try{

        if($personId!=""&&$company!=""&&$occupation!=""&&$startTime != ''&&$endTime != ''){


            //结束时间要大于开始时间
            if(Util::dateCompare($startTime,$endTime) >= 0){
                Util::printResult( $GLOBALS['ERROR_PARAM_WRONG'], "参数错误，开始时间应该小于结束时间");
                exit;
            }

            $personDB = new CDBPerson();
            //检查操作的权限
            if(!$personDB->verifyUserIdAndPersonId($userId,$personId)){
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $person = $personDB->getPersonById($personId);

            $id = $personDB->addJobForPerson($personId , $person->userId , $company , $occupation,$startTime,$endTime,$userId);

            if($id > 0){
                if ($GLOBALS['TEST_PERSON_TASK']) {
                    $CDBPushPerson = new CDBPushPerson();
                    $modelId = $personId;
                    $action = 2;
                    $source = $person;
                   $CDBPushPerson->setPushPersonJobs($modelId, $action, $source);
                }
                $data['addedId'] = $id;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错");
            }

        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
