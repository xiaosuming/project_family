<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-5-13
 * Time: 下午2:15
 */

use DB\CDBPerson;
use Util\Check;
use Util\SysLogger;
use Util\Util;

try {

    $colorId = Check::checkInteger($params['colorId'] ?? '');
    $color = Check::check($params['color'] ?? '');
    $userId = $GLOBALS['userId'];

    $personDB = new CDBPerson();

    if (!$personDB->checkUserCreatedUserRemarkPersonColor($colorId,$userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],'权限错误');
        exit;
    }

    $updateRow = $personDB->updateUserRemarkPersonColor($colorId,$color,$userId);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
