<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-2
 * Time: 下午2:02
 */

use DB\CDBPerson;
use Util\Check;
use Util\Util;

$personId = Check::checkInteger(trim($params['personId'] ?? ''));
$samePersonId = Check::checkInteger(trim($params['samePersonId'] ?? ''));

if ($personId == '' || $samePersonId == ''){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'],'参数缺失');
    exit;
}

try{
    $personDB = new CDBPerson();
    $userId = $GLOBALS['userId'];
    //检查操作的权限
    if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
    $personDb = new CDBPerson();
    $updateRow = $personDb->setAdoptionPersonByPersonId($personId,$samePersonId);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);

}catch (PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

