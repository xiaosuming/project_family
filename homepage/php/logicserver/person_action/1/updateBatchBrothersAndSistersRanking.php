<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-8-7
 * Time: 下午12:26
 */

use DB\CDBPerson;
use Util\Check;
use Util\Util;

$personIds = Check::check($params['personIds'] ?? ''); // 人物id集合 逗号分隔的字符串
$rankings = Check::check($params['rankings'] ?? '');  //排行集合  逗号分隔的字符串

try {

    if ($personIds == '' || $rankings == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $personDB = new CDBPerson();
    $userId = $GLOBALS['userId'];

    $personIdArr = explode(',', $personIds);
    $rankingArr = explode(',', $rankings);

    if (count($personIdArr) != count($rankingArr)) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "参数出错,请仔细检查参数");
        exit;
    }

    foreach ($personIdArr as $personId) {
        //检查操作的权限
        if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    $updateRow = $personDB->batchUpdateBrothersAndSistersRanking($rankingArr, $personIdArr);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}