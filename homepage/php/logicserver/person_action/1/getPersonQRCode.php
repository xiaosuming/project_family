<?php
    /**
     * 获取人物二维码
     * @author jiangpegnfei
     * @date : 2017/08/02
     * TODO 二维码路径不对
     */

    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    
    $personId = Check::check(trim(isset($params['personId']) ? $params['personId'] : ''));				//人物id
    $userId = $GLOBALS['userId'];

    try{

        if($personId!=""){
            $personDB = new CDBPerson();
            $person = $personDB->getPersonById($personId);

            if($person == null){
                Util::printResult( $GLOBALS['ERROR_SQL_QUERY'], "人物不存在");
                exit;
            }

            $familyDB = new CDBFamily();
            //检查操作的权限
            if(!$familyDB->isUserForFamily($person->familyId,$userId)){
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $result = $personDB->getPersonQRCode($personId);

            $qrPath = "";
            $retryTime = 0;
            while($qrPath === "" && $retryTime <= 3){
                if($result != null){
                    if($result['qrcode'] == "" || $result['qrcode'] <= 0){
                        //二维码为空
                        $shareCode = $result['share_url'];
                        $shareUrl = $GLOBALS['DOMAIN_NAME']."php/index.php?action=person_action&sub_action=qrcode_redirect&shareCode=$shareCode&userId=$userId";
                        $qrPath = Util::generateQRCode($shareUrl);
                        if(is_numeric($qrPath) && $qrPath <= 0){
                            //记录错误日志
                            $logger->error("生成二维码出错",array($qrPath));
                            $qrPath = "";
                            $retryTime++;   //重试次数+1
                            continue;       //重试
                        }
                        $personDB->setPersonQRCode($personId,$qrPath);
                        $data['qrcode'] = $qrPath;
                        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                        exit;
                    }else{
                        $data['qrcode'] = $result['qrcode'];
                        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
                        exit;
                    }
                }else{
                    Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                    exit;
                }
            }
            
            Util::printResult($GLOBALS['ERROR_EXCEPTION'], "二维码生成出现异常，请重试");
            
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }