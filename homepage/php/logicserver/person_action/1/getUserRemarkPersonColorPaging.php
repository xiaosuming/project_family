<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-5-13
 * Time: 下午1:05
 */

use DB\CDBPerson;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

try {
    $maxId = Check::checkInteger($params['maxId'] ?? 0);
    $sinceId = Check::checkInteger($params['sinceId'] ?? 0);
    $count = Check::checkInteger($params['count'] ?? 10);
    $familyId = Check::checkInteger($params['familyId'] ?? '');
    $userId = $GLOBALS['userId'];

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $personDB = new CDBPerson();
    $total = $personDB->countUserRemarkPersonColorRecord($userId,$familyId);

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $list = $personDB->getUserPersonRemarkColorListPage($familyId,$userId,$maxId,$sinceId,$count);

    $len = count($list);

    $nextSinceId = $list[0]['id'];

    $nextMaxId = $list[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $list, $nextMaxId, $nextSinceId, $lastPage);
    $paginator->printPage();

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
