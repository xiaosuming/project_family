<?php 
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Model\Person;
    use Util\Check;
    use Util\PostGenerate;
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

    try{
        
        if($familyId != ""){

            $familyDB = new CDBFamily();
            $userId = $GLOBALS['userId'];
            //检查这个用户是不是家族的创始人，创始人是不允许离开家族的
            if($familyDB->isOriginatorForFamily($familyId,$userId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "创始人无法离开家族");
                exit;
            }

            $personDB = new CDBPerson();
            $deleteId = $personDB->leaveFamily($familyId,$userId);
            $data['leave'] = $deleteId;
            if($deleteId > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], null);
            }
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }