<?php
    /**
     * 获取人物工作
     * @author jiangpegnfei
     * @date : 2017/04/12
     */

    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    
    $personId = Check::check(trim(isset($params['personId']) ? $params['personId'] : ''));				//人物id
    $userId = $GLOBALS['userId'];

    try{

        if($personId!=""){
            
            $personDB = new CDBPerson();
            $person = $personDB->getPersonById($personId);

            if($person == null){
                Util::printResult( $GLOBALS['ERROR_SQL_QUERY'], "人物不存在");
                exit;
            }

            $familyDB = new CDBFamily();
            //检查操作的权限
            if(!$familyDB->isUserForFamily($person->familyId,$userId)){
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $jobs = $personDB->getPersonJob($personId);
            
            $data['jobs'] = $jobs;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }