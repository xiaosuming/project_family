<?php
    /**
     * 以合并家族中用户的身份，获取家族中的所有人物
     */
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));				//必须

    $userId = $GLOBALS['userId'];

    try{
        if($familyId == ""){
            Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
        $familyDB = new CDBFamily();

        //检查操作的权限
        if(!$familyDB->verifyUserReadPermissionByMerge($userId,$familyId)){
            Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        
        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersonsByMerge($familyId);
        foreach($persons as &$person) {
            if (!empty($person['refFamilyId'])) {
                $person['refFamilyId'] = json_decode($person['refFamilyId'],true);
            }
        }
        $data['persons'] = $persons;
        if(count($persons) > 0){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult( $GLOBALS['ERROR_SQL_QUERY'], "sql查询时发生错误");
        }
        
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }