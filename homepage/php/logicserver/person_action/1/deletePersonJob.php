<?php

    use DB\CDBPushPerson;
    use Util\Util;
    use DB\CDBPerson;
    use Util\Check;

    $jobId = Check::checkInteger(trim(isset($params['jobId']) ? $params['jobId'] : ''));

    try {
        if($jobId == "") {
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
        $personDB = new CDBPerson();
        $userId = $GLOBALS['userId'];

        $flag = $personDB->checkUserIdAndJobId($userId, $jobId);
        if($flag == true) {

            $personId=$personDB->getPersonIdByJobId($jobId);
            $person=$personDB->getPersonById($personId);
            $rows = $personDB->deletePersonJob($jobId);
            if($rows>0) {

                if ($GLOBALS['TEST_PERSON_TASK']){
                    $CDBPushPerson = new CDBPushPerson();
                    $modelId = $personId;
                    $action = 3;
                    $source = $person;
                    $CDBPushPerson->setPushPersonJobs($modelId, $action, $source);
                }

                $data['rows'] = $rows;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            } else {
                Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除职业信息失败");
            }
        } else {//无权限
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    } catch(PDOException $e) {
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }