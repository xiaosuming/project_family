<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-2-14
 * Time: 下午3:09
 */

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger($params['familyId'] ?? ''); //班级id

try {

    $familyDB = new CDBFamily();
    // 检查家族类型是否是家庭,4是师徒
    $familyDetail = $familyDB->getFamilyById($familyId);
    if ($familyDetail->groupType != 4) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
        exit;
    }

    $personDB = new CDBPerson();

    $personList = $personDB->getAllMentorPersonList($familyId);

    $data['personList'] = $personList;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
