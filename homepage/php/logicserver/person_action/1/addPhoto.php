<?php

use DB\CDBPushPerson;
use Util\Util;
    use Util\ImageUpload;
    use DB\CDBPerson;
    use DB\CDBFamilyNews;
    use Model\Person;
    use Util\Check;
    use Util\PostGenerate;

    $personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
    $photo = isset($params["photo"]) ? $params["photo"] : '';
    $data = array();

    try{

        if($personId != "" && $photo != ""){
            $personDB = new CDBPerson();
            $userId = $GLOBALS['userId'];
            //检查操作的权限
            if(!$personDB->verifyUserIdAndPersonId($userId,$personId)){
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $photoId = $personDB->addPhoto($personId,$photo);

            if($photoId > 0){
                //记录操作的日志，展示在家族新鲜事中
                $familyId = $personDB->getFamilyIdByPersonId($personId);
                $person = $personDB->getPersonById($personId);

                $familyNewsDB = new CDBFamilyNews();
                $familyNewsDB->addFamilyNews($familyId,$GLOBALS['PERSON_MODULE'],$GLOBALS['ADDPHOTO'],$userId,$personId,$person->name,$userId,$photo);
                if ($GLOBALS['TEST_PERSON_TASK']) {

                    $CDBPushPerson = new CDBPushPerson();
                    $modelId = $personId;
                    $action = 2;
                    $source = $person;
                    $CDBPushPerson->setPushPersonPhoto($modelId, $action, $source);
                }

                $data['update'] = $photoId;
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], null);
            }
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
