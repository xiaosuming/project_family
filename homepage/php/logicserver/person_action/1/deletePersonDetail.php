<?php

use DB\CDBPushPerson;
use Util\Util;
use DB\CDBPerson;
use Util\Check;

$personDetailId = Check::checkInteger(trim(isset($params['personDetailId']) ? $params['personDetailId'] : ''));

try {
    if ($personDetailId == "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
    $personDB = new CDBPerson();
    $userId = $GLOBALS['userId'];

    $flag = $personDB->checkUserIdAndPersonDetailId($userId, $personDetailId);

    if ($flag == true) {
        $personId = $personDB->getPersonIdByPersonDetailId($personDetailId);
        $person = $personDB->getPersonById($personId);
        $rows = $personDB->deletePersonInfo($personDetailId);
        if ($rows > 0) {
            if ($GLOBALS['TEST_PERSON_TASK']) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 3;
                $source = $person;
                $type = $personDB->getTypeByPersonDetailId($personDetailId);
                if ($type) {
                    if ($type == 1) {
                        $CDBPushPerson->setPushPersonSkills($modelId, $action, $source);
                    }
                    if ($type == 2) {
                        $CDBPushPerson->setPushPersonFavorites($modelId, $action, $source);
                    }
                }
            }
            $data['rows'] = $rows;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除标签失败");
        }
    } else {//无权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
