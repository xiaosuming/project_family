<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-2
 * Time: 下午5:23
 */

use DB\CDBPerson;
use Util\Check;
use Util\SysLogger;
use Util\Util;

$shareCode = Check::check($params['shareCode'] ?? '');

try {

    if ($shareCode == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }

    $personDB = new CDBPerson();
    $person = $personDB->getPersonByShareCode($shareCode);
    $data['person'] = $person;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
