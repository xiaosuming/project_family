<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-10-11
 * Time: 下午5:21
 */
ini_set('memory_limit', '1024M');

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'] ?? 0; // 未登录可以获取公开家族的
$familyId = Check::checkInteger($params['familyId'] ?? '');
$datetime = $params['datetime'] ?? '';

try {
    if ($datetime == '' || $familyId == '') {
        Util::printResult($GLOBALS["ERROR_PARAM_MISSING"], '缺少参数');
        exit;
    }

    $familyDB = new CDBFamily();

    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($family->openType == 0) {
        //检查查询的权限
        if (!$familyDB->isUserForFamily($familyId, $userId) && !$familyDB->isAdminForFamily($familyId, $userId) && !$familyDB->isOriginatorForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    $personDB = new CDBPerson();
    $personList = $personDB->getFamilyChangePersonListByTime($familyId,$datetime);
    $data['personList'] = $personList;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}