<?php
/**
 * 获取所有的家庭中的人物
 * @Author: jiangpengfei
 * @Date:   2019-02-22
 */

use Util\Util;
use Util\Check;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\SysConst;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();

    // 检查家庭的类型
    if (!$familyDB->checkGroupType($familyId, SysConst::$GROUP_TYPE_MINIFAMILY)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '群组类型不正确');
        exit;
    }

    // 检查用户的权限
    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '用户权限不正确');
        exit;
    }

    $personDB = new CDBPerson();

    // 家庭和家族的获取人物接口可以通用
    $persons = $personDB->getFamilyPersons($familyId);

    $data['persons'] = $persons;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}