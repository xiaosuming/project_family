<?php
/**
 * 根据封存的版本号分页获取人物信息
 * @Author: jiangpengfei
 * @Date:   2019-02-21
 */

use DB\CDBPerson;
use Util\Util;
use Util\Check;
use DB\CDBFamily;
use Util\Paginator;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$version = Check::checkInteger($params['version'] ?? '');
$count = Check::checkInteger($params['count'] ?? 10);
$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();

    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $personDB = new CDBPerson();
    $persons = $personDB->getPersonsPagingBySnapshotVersion($familyId, $version, $count, $maxId, $sinceId);

    $total = $personDB->getPersonsCountBySnapshotVersion($familyId, $version);

    $data['persons'] = $persons;

    $len = count($persons);

    if ($len == 0) {
        $paginator = new Paginator($total, $data, 0, 0, true);
    } else {
        $maxId = $persons[$len - 1]['id'];
        $sinceId = $persons[0]['id'];

        $lastPage = false;
        if ($len < $count) {
            $lastPage = true;
        }

        $paginator = new Paginator($total, $data, $maxId, $sinceId, $lastPage);
    }

    $paginator->printPage();

} catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}