<?php
    /**
     * 可以根据多个人物的id,依次返回id对应的姓名
     */
    use Util\Util;
    use DB\CDBPerson;
    use Util\Check;
        
    $personsId = Check::check(trim(isset($params['personsId']) ? $params['personsId'] : ''));
    try{
        $idArray = explode(",",$personsId);
        if($personsId!=""){
            $personDB = new CDBPerson();
            $personsName = $personDB->getPersonsNameByPersonsId($idArray);
            $data['names'] = $personsName;
            if($personsName != null)
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            else
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'],"id不存在");
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }