<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-8-14
 * Time: 下午4:07
 */

use DB\CDBPerson;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$infoCardId = Check::checkInteger($params['infoCardId'] ?? '');

try {

    if ($familyId == '' || $infoCardId == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }

    $personDB = new CDBPerson();
    $userId = $GLOBALS['userId'];
    //检查操作的权限
    if (!$personDB->verifyUserIdAndFamilyId($userId, $familyId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
    $personId = $personDB->getPersonId($userId, $familyId);
    $updateRow = $personDB->bindUserInfoCardInFamily($infoCardId, $personId);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}