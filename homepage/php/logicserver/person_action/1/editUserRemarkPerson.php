<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-5-13
 * Time: 下午12:54
 */

use DB\CDBPerson;
use Util\Check;
use Util\SysLogger;
use Util\Util;

try {
    $remarkId = Check::checkInteger($params['remarkId'] ?? '');
    $remarkType = Check::checkInteger($params['remarkType'] ?? '');
    $userId = $GLOBALS['userId'];

    $personDB = new CDBPerson();

    if (!$personDB->checkUserCreatedUserRemarkPerson($remarkId,$userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],'权限错误');
        exit;
    }


    $updateRow = $personDB->updateUserPersonRemarkType($remarkId, $remarkType,$userId);
    $data['updateRow'] = $updateRow;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
