<?php
/**
 * 为人物添加标签-------技能,爱好
 * @author jiangpegnfei
 * @date : 2017/04/12
 */

use DB\CDBPushPerson;
use Util\Util;
use DB\CDBPerson;
use Util\Check;

$personId = Check::check(trim(isset($params['personId']) ? $params['personId'] : ''));                //人物id
$info = Check::check(trim(isset($params['info']) ? $params['info'] : ''), 1, 10);                    //必须
$type = Check::checkInteger(trim(isset($params['type']) ? $params['type'] : ''));
$userId = $GLOBALS['userId'];

try {

    if ($personId != "" && $info != "" && $type != "") {
        $personDB = new CDBPerson();
        //检查操作的权限
        if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $person = $personDB->getPersonById($personId);

        //检查标签是否重复
        if ($personDB->checkTagDuplicate($personId, $info, $type)) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "标签重复，请换一个标签重试");
            exit;
        }

        $id = $personDB->addForPerson($personId, $person->userId, $info, $type, $userId);

        if ($id > 0) {
            if ($GLOBALS['TEST_PERSON_TASK']) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 2;
                $source = $person;
                if ($type == 1) {
                    $source->skill = $info;
                    $CDBPushPerson->setPushPersonSkills($modelId,$action,$source);
                }
                if ($type == 2){
                    $source->favorite = $info;
                    $CDBPushPerson->setPushPersonFavorites($modelId,$action,$source);
                }
            }
            $data['addedId'] = $id;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错，请检查是否重复或稍后再试");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
