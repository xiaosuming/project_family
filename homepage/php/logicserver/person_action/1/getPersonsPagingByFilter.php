<?php
/**
 * 根据过滤条件获取人物数组
 * @author:jiangpengfei
 * @date: 2017-12-07
 */
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    use Util\Pager;

    $pageIndex = Check::checkInteger(trim($params['pageIndex'] ?? ''));
    $pageSize = Check::checkInteger(trim($params['pageSize'] ?? ''));

    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));				//必须
    $isUser = Check::checkInteger(trim($params['isUser'] ?? '0'));           //是否绑定了用户
    $hasPhone = Check::checkInteger(trim($params['hasPhone'] ?? '0'));       //是否有手机号
    $noDirectRelatives = Check::checkInteger(trim($params['noDirectRelatives'] ?? '0')); //不要有直系亲属

    try{
        if($familyId == ""){
            Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }

        $familyDB = new CDBFamily();
        //检查操作的权限
        $userId = $GLOBALS['userId'];
        if(!$familyDB->isUserForFamily($familyId,$userId)){
            Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
        
        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersonsPagingByFilter($pageIndex,$pageSize,$familyId,$isUser == 1,$hasPhone == 1);
        
        $unsetCount = 0;

        //检查是否需要去除直系亲属
        if($noDirectRelatives == 1){
            $me = $personDB->getPersonByFamilyIdAndUserId($familyId,$userId);

            if($me != null){

                $relativeIds = array();

                //父亲
                foreach($me->father as $personId){
                    $relativeIds[$personId] = $personId;
                }

                //母亲
                foreach($me->mother as $personId){
                    $relativeIds[$personId] = $personId;
                }

                //配偶
                foreach($me->spouse as $personId){
                    $relativeIds[$personId] = $personId;
                }

                //兄弟
                foreach($me->brother as $personId){
                    $relativeIds[$personId] = $personId;
                }

                //姐妹
                foreach($me->sister as $personId){
                    $relativeIds[$personId] = $personId;
                }

                //儿子
                foreach($me->son as $personId){
                    $relativeIds[$personId] = $personId;
                }

                //女儿
                foreach($me->daughter as $personId){
                    $relativeIds[$personId] = $personId;
                }

                $len = count($persons);

                for($i = 0; $i < $len; $i++){
                    
                    if(array_key_exists($persons[$i]['id'],$relativeIds)){
                        
                        unset($persons[$i]);
                        $unsetCount++;
                    }
                }
            }
        }
        
        $count = $personDB->getFamilyPersonsCountByFilter($familyId,$isUser == 1,$hasPhone == 1);
 
        $paging = new Pager($count - $unsetCount,array_values($persons),$pageIndex,$pageSize);
        $paging->printPage();

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }