<?php
    /**
     * 获取用户绑定的人物列表
     * @author jiangpegnfei
     * @date : 2017/11/20
     */

    use Util\Util;
    use DB\CDBPerson;
    use Util\Check;
    
    $userId = $GLOBALS['userId'];

    try{
        $personDB = new CDBPerson();
        $personList = $personDB->getUserBindPersonList($userId);

        $data['personList'] = $personList;

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }