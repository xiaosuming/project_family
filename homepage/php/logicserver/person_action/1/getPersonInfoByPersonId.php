<?php

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));

try {
    //检查用户权限

    if ($personId != "") {
        $personDB = new CDBPerson();
        $person = $personDB->getPersonById($personId);
        $data['person'] = $person;
        if ($person != null) {
            //检查操作的权限

            $familyDB = new CDBFamily();

            $family = $familyDB->getFamilyById($person->familyId);

            if ($family == null) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
                exit;
            }

            if ($family->openType == 0) {

                if (!$familyDB->isUserForFamily($person->familyId, $userId)) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                    exit;
                }
            }
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], $data);
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
