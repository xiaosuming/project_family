<?php
/**
 * 根据族群id和即时通讯用户获取对应的人物资料
 * @author: jiangpengfei
 * @date:   2019-03-25
 */

use Util\Util;
use Util\Check;
use DB\CDBPerson;
use DB\CDBAccount;

$imUser = Check::check($params['imUser'] ?? '');
$familyId = Check::checkInteger($params['familyId'] ?? '');

if ($imUser == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '参数缺失');
    exit;
}

try {
    $accountDB = new CDBAccount();
    $userId = $accountDB->getUserIdByIMUsername($imUser);

    if ($userId == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '查询失败，用户不存在');
        exit;
    }

    $personDB = new CDBPerson();
    $person = $personDB->getPersonByFamilyIdAndUserId($familyId, $userId);
    
    if ($person == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '查询失败，人物不存在');
        exit;
    }

    $data['person'] = $person;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
