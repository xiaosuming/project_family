<?php

use DB\CDBArea;
use DB\CDBPushPerson;
use Model\Person;
use Util\Util;
use DB\CDBPerson;
use DB\CDBFamilyNews;
use Util\Check;
use Util\PostGenerate;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''), 1, 30);
$country = Check::checkInteger(trim(isset($params['country']) ? $params['country'] : '0'));  //国家 默认为0 中国
$province = Check::checkInteger(trim(isset($params['province']) ? $params['province'] : '0')); //省 没有默认为 0
$city = Check::checkInteger(trim(isset($params['city']) ? $params['city'] : '0')); //市 没有默认为 0
$area = Check::checkInteger(trim(isset($params['area']) ? $params['area'] : '0')); //区 没有默认为 0
$town = Check::checkInteger(trim(isset($params['town']) ? $params['town'] : '0')); //乡 没有默认为0
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));
$birthday = Check::check(trim(isset($params['birthday']) ? $params['birthday'] : null));
$zi = Check::check(trim(isset($params['zi']) ? $params['zi'] : ''), 0, 10);
$zpname = Check::check(trim(isset($params['zpname']) ? $params['zpname'] : ''), 0, 10);
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : ''), 0, 255);
$isDead = Check::checkInteger(trim(isset($params['isDead']) ? $params['isDead'] : 0)); //默认为0 0表示未去世
$deadTime = trim(isset($params['deadTime']) ? $params['deadTime'] : null);
$ranking = $params['ranking'] ?? 0;

$phone = trim(isset($params['phone']) ? $params['phone'] : '');
if ($phone != '') {
    Check::checkPhoneNumber($phone);
}


try {

    if ($personId != "" && $name != "") {
        $personDB = new CDBPerson();
        $userId = $GLOBALS['userId'];
        if ($birthday!=null && $deadTime !=''){
            if ($birthday >= $deadTime) {
                Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "出生时间不能大于去世时间");
                exit;
            }
        }

        //检查操作的权限
        if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
        $CDBArea = new CDBArea();
        $result = $CDBArea->getAreaNameById($country) ?? '0';
        $countryName = $result['areaName'];
        $result = $CDBArea->getAreaNameById($province) ?? '0';
        $provinceName = $result['areaName'];
        $result = $CDBArea->getAreaNameById($city) ?? '0';
        $cityName = $result['areaName'];
        $result = $CDBArea->getAreaNameById($area) ?? '0';
        $areaName = $result['areaName'];
        $result = $CDBArea->getAreaNameById($town) ?? '0';
        $townName = $result['areaName'];
        
        $id = $personDB->editPerson($personId, $zi, $remark, $name, $isDead, 
        $deadTime, $country, $countryName, $province, $provinceName, $city, 
        $cityName, $area, $areaName, $town, $townName, $address, $birthday, 
        $phone, $zpname, $ranking);

        $data['editPerson'] = $id;
        //前台需要判断是否有修改
        if ($id > 0) {
            $person = $personDB->getPersonById($personId);

            $familyNewsDB = new CDBFamilyNews();

            $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['EDITPERSON'], $userId, $personId, $person->name, $userId);

            if ($GLOBALS['TEST_PERSON_TASK']) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 2;
                $source = $person;
                $CDBPushPerson->setEditPushPersonTask($modelId, $action, $source);
            }

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "无变化");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}