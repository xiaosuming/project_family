<?php
/**
 * 获取家族树的一个子树,也就是查询某个家族的某个人物的直系亲属和自己
 * author:jiangpengfei
 * date:2020-04-17
 */
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    
    $personID = Check::check(trim(isset($params['personId']) ? $params['personId'] : ''));				//必须

    try{
        if($personID!=""){
            $personDB = new CDBPerson();
            $person = $personDB->getPersonById($personID);
            if ($person == null) {
                Util::printResult( $GLOBALS['ERROR_SQL_QUERY'], "人物不存在");
                exit;
            }

            $familyId = $person->familyId;

            $familyDB = new CDBFamily();
            //检查操作的权限
            $userId = $GLOBALS['userId'];
            if(!$familyDB->isUserForFamily($familyId,$userId)&&!$familyDB->isAdminForFamily($familyId,$userId)&&!$familyDB->isOriginatorForFamily($familyId,$userId)){
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }
            
            $persons = $personDB->getFamilyPersonsSubTreeByPerson($familyId, $person);
            $data['persons'] = $persons;
            if(count($persons) > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult( $GLOBALS['ERROR_SQL_INSERT'], "sql查询时发生错误");
            }
            
        }else{
            Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }