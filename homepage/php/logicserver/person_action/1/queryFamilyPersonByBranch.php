<?php
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    
    $branchId = Check::checkInteger(trim(isset($params['branchId']) ? $params['branchId'] : ''));				//必须
    $userFamilyId = Check::checkInteger(trim($params['userFamilyId'] ?? ''));

    try{
        if($branchId!=""){
            $familyDB = new CDBFamily();
            //检查操作的权限
            $userId = $GLOBALS['userId'];
            if(!$familyDB->verifyUserIdAndBranchIdByFamilyId($userId,$branchId,$userFamilyId)){
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }
            
            $personDB = new CDBPerson();
            $persons = $personDB->getFamilyPersonsByBranchId($branchId);

            //将家族中特殊的人物点进行处理
            $personMap = array();       //人物字典
            $specialPersons = array();
            foreach($persons as &$personItem){  //这里取引用
                $personMap[$personItem['id']] = &$personItem;      //创建id和人物
                if($personItem['refPersonId'] != null && $personItem['refFamilyId'] != null){
                    //特殊人物
                    array_push($specialPersons,$personItem);
                }
            }

            //将特殊人物的属性改变
            foreach($specialPersons as $person){
                
                if(count($person['son']) == 0 && count($person['daughter']) == 0){
                    //子女都为空的是主分支上的
                    $refPerson = $personMap[$person['refPersonId']];

                    $personMap[$person['id']]['son'] = $refPerson['son'];
                    $personMap[$person['id']]['daughter'] = $refPerson['daughter'];

                    //将这些子女的父母更新为新的
                    foreach($refPerson['son'] as $sonId){
                        if($person['gender'] == $GLOBALS['GENDER_MALE']){
                            //更新父亲
                            $personMap[$sonId]['father'] = [$person['id']];
                            $personMap[$sonId]['mother'] = $person['spouse'];
                        }else{
                            //更新母亲
                            $personMap[$sonId]['mother'] = [$person['id']];
                            $personMap[$sonId]['father'] = $person['spouse'];
                        }
                        
                    }

                    //将这些子女的父母更新为新的
                    foreach($refPerson['daughter'] as $daughterId){
                        if($person['gender'] == $GLOBALS['GENDER_MALE']){
                            //更新父亲
                            $personMap[$daughterId]['father'] = [$person['id']];
                            $personMap[$daughterId]['mother'] = $person['spouse'];
                        }else{
                            //更新母亲
                            $personMap[$daughterId]['mother'] = [$person['id']];
                            $personMap[$daughterId]['father'] = $person['spouse'];
                        }
                        
                    }

                    $personMap[$refPerson['id']] = null;
                }
            }

            //最后循环一次，删除null的oerson
            foreach($persons as &$person){
                if($person == null){
                    $person = array_pop($persons);
                }
            }

            $data['persons'] = $persons;
            if(count($persons) > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult( $GLOBALS['ERROR_SQL_INSERT'], "sql查询时发生错误");
            }
            
        }else{
            Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }