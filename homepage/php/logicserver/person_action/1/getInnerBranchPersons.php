<?php
/**
 * 获取内建分支下的所有人物
 * @Author: jiangpengfei
 * @Date:   2019-02-14
 */

use DB\CDBPerson;
use Util\Check;
use Util\Util;
use DB\CDBFamily;

$branchId = Check::checkInteger($params['branchId'] ?? '');
$userId = $GLOBALS['userId'];

try {

    // 判断用户对分支是否有查看的权限
    $familyDB = new CDBFamily();
    $branch = $familyDB->getInnerBranch($branchId);

    if ($branch == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '分支不存在');
        exit;
    }

    if (!$familyDB->isUserForFamily($branch->familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $personDB = new CDBPerson();
    $persons = $personDB->getInnerBranchPersons($branchId);

    $data['persons'] = $persons;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}