<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-8-7
 * Time: 下午12:26
 */

use DB\CDBPerson;
use Util\Check;
use Util\Util;

$personId = Check::checkInteger($params['personId'] ?? ''); // 人物id
$ranking = Check::checkInteger($params['ranking'] ?? '');  //排行

try {
    if ($personId == '' || $ranking == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '缺少参数');
        exit;
    }
    $personDB = new CDBPerson();
    $userId = $GLOBALS['userId'];
    //检查操作的权限
    if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
    $updateRow = $personDB->updateBrothersAndSistersRanking($ranking, $personId);
    $data['updateRow'] = $updateRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}