<?php
/**
 * 分页获取家庭中的人物
 * @Author: jiangpengfei
 * @Date:   2019-02-22
 */

use Util\Util;
use Util\Check;
use DB\CDBFamily;
use DB\CDBPerson;
use Util\SysConst;
use Util\Paginator;

$familyId = Check::checkInteger($params['familyId'] ?? '');
$maxId = Check::checkInteger($params['maxId'] ?? 0);
$sinceId = Check::checkInteger($params['sinceId'] ?? 0);
$count = Check::checkInteger($params['count'] ?? 10);
$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();

    // 检查家庭类型
    if (!$familyDB->checkGroupType($familyId, SysConst::$GROUP_TYPE_MINIFAMILY)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '家族类型不正确');
        exit;
    }

    // 检查用户权限
    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限不足');
        exit;
    }

    $personDB = new CDBPerson();
    $persons = $personDB->getMiniFamilyPersonsPaging($familyId, $maxId, $sinceId, $count);
    $total = $personDB->getFamilyPersonsCount($familyId, NULL, NULL);

    $len = count($persons);

    $lastPage = false;
    if ($len == 0) {
        $lastPage = true;
        $maxId = 0;
        $sinceId = 0;
    } else {
        if ($len < $count) {
            $lastPage = true;
        } else {
            $lastPage = false;
        }

        $maxId = $persons[$len - 1]['id'];
        $sinceId = $persons[0]['id'];
    }

    $paginator = new Paginator($total, $persons, $maxId, $sinceId, $lastPage);
    $paginator->printPage();

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
