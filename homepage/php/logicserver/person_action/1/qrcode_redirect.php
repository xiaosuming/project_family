<?php
    /**
     * 人物二维码跳转
     * @author jiangpegnfei
     * @date : 2018/02/09
     */

    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBAccount;
    use Util\Check;
    
    $shareCode = Check::check(trim(isset($params['shareCode']) ? $params['shareCode'] : ''));				//人物id
    $userId = Check::checkInteger(trim(isset($params['userId']) ? $params['userId'] : ''));

    try{

        if($shareCode!=""&&$userId!=""){
            $personDB = new CDBPerson();
            $personId = $personDB->getPersonIdByShareCode($shareCode);
            if(!$personId){
                Util::printResult( $GLOBALS['ERROR_SQL_QUERY'], "人物不存在");
                exit;
            }
            $person = $personDB->getPersonById($personId);

            $accountDB = new CDBAccount();
            $user = $accountDB->getUserInfoById($userId);
            

            $personName = $person->name;
            $familyName = $person->familyName;
            $familyPhoto = explode('?',$person->familyPhoto)[0];
            $inviteUserName = $user['nickname'];

            $url = $GLOBALS['DOMAIN_NAME']."sharepage.html?familyName=".urlencode($familyName)."&familyPhoto=".$familyPhoto."&personName=".urlencode($personName)."&inviteUserName=".urlencode($inviteUserName)."&shareCode=$shareCode";

            header("location:$url");

        }else{
            Util::printResult( $GLOBALS['ERROR_SQL_QUERY'], "参数缺少");
            exit;
        }
           
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }