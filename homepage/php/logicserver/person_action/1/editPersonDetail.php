<?php
/**
 * 编辑人物的详情，比editPerson接口多了bloodType,maritalType,qq三个属性
 * author:jiangpengfei
 * date:2017-05-16
 *
 */

use DB\CDBArea;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use Model\Person;
use Util\Check;
use Util\Util;
use ThirdParty\MiniProgram;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''),1,30);
$birthday = Check::check(trim(isset($params['birthday']) ? $params['birthday'] : null));
$maritalStatus = Check::checkInteger(isset($params['maritalStatus']) ? $params['maritalStatus'] : 0);        //不传则是未婚
$bloodType = Check::checkInteger(isset($params['bloodType']) ? $params['bloodType'] : 0);                    //不传则是0
$qq = Check::check(trim(isset($params['qq']) ? $params['qq'] : ''));
$country = Check::checkInteger(trim(isset($params['country']) ? $params['country'] : '0')); //国家
$province = Check::checkInteger(trim(isset($params['province']) ? $params['province'] : '0')); //省
$city = Check::checkInteger(trim(isset($params['city']) ? $params['city'] : '0'));
$area = Check::checkInteger(trim(isset($params['area']) ? $params['area'] : '0'));
$town = Check::checkInteger(trim(isset($params['town']) ? $params['town'] : '0'));
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));
$phone = trim(isset($params['phone']) ? $params['phone'] : '');
$zi = Check::check(trim(isset($params['zi']) ? $params['zi'] : ''),0,10);
$zpname = Check::check(trim(isset($params['zpname']) ? $params['zpname'] : ''), 0, 10);
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : ''),0,255);
$isDead = Check::checkInteger(trim(isset($params['isDead']) ? $params['isDead'] : 0)); //默认为0 0表示未去世
$deadTime = trim(isset($params['deadTime']) ? $params['deadTime'] : null);
$ranking = $params['ranking'] ?? 0;
$sideText = Check::check($params['sideText'] ?? '');
$profileText = Check::check($params['profileText'] ?? '');

if ($phone != '') {
    Check::checkPhoneNumber($phone);
}

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$name, $qq, $address, $zi, $zpname, $remark, $sideText, $profileText]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {

    if ($personId != "" && $name != "") {
        $personDB = new CDBPerson();
        $userId = $GLOBALS['userId'];
        if ($birthday!=null  && $deadTime !=''){
            if ($birthday >= $deadTime) {
                Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "出生时间不能大于去世时间");
                exit;
            }
        }

        //检查操作的权限
        if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $CDBArea = new CDBArea();
        $result = $CDBArea->getAreaNameById($country);
        $countryName = $result['areaName'] ?? '0';
        $result = $CDBArea->getAreaNameById($province);
        $provinceName = $result['areaName'] ?? '0';
        $result = $CDBArea->getAreaNameById($city);
        $cityName = $result['areaName'] ?? '0';
        $result = $CDBArea->getAreaNameById($area);
        $areaName = $result['areaName'] ?? '0';
        $result = $CDBArea->getAreaNameById($town);
        $townName = $result['areaName'] ?? '0';

        $person=new Person();
        $person->country = $country;
        $person->province=$province;
        $person->city=$city;
        $person->area=$area;
        $person->town=$town;
        $person->address=$address;
        $person->name=$name;
        $person->birthday=$birthday;
        $person->phone=$phone;
        $person->bloodType=$bloodType;
        $person->maritalStatus=$maritalStatus;
        $person->qq=$qq;
        $person->countryName=$countryName;
        $person->provinceName=$provinceName;
        $person->cityName=$cityName;
        $person->areaName=$areaName;
        $person->townName=$townName;
        $person->zi=$zi;
        $person->zpname=$zpname;
        $person->remark=$remark;
        $person->isDead = $isDead;
        $person->deadTime = $deadTime;
        $person->ranking = $ranking;
        $person->sideText = $sideText;
        $person->profileText = $profileText;


        $count = $personDB->editPersonDetail($person, $personId);
        $data['editPerson'] = $count;
        //前台需要判断是否有修改
        if ($count > 0) {
            $person = $personDB->getPersonById($personId);

            $familyNewsDB = new CDBFamilyNews();
            $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['EDITPERSON'], $userId, $personId, $person->name, $userId);

            if ($GLOBALS['TEST_PERSON_TASK']) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 2;
                $source = $person;
                $CDBPushPerson->setEditPushPersonTask($modelId, $action, $source);
            }

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "无变化");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
