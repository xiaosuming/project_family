<?php
/**
 * 删除人物
 * author:jiangpengfei
 * date:2017-07-10
 */

use DB\CDBEvent;
use DB\CDBFamily;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use Util\Check;
use Util\Util;
use Model\Person;
use Util\HttpContext;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));

/**
 * 是否有父母
 * @param $person 人物对象
 * @return 父母的数量
 */
function hasParent(Person $person)
{
    return count($person->father) + count($person->mother);
}

/**
 * 是否有孩子
 * @param $person 人物对象
 * @return 孩子的数量
 */
function hasChild(Person $person)
{
    return count($person->son) + count($person->daughter);
}

function hasSpouse(Person $person)
{
    return count($person->spouse);
}

try {

    if ($personId != "") {
        $personDB = new CDBPerson();

        $userId = $GLOBALS['userId'];
        //检查操作的权限
        if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $person = $personDB->getPersonById($personId);        //在删除之前先获取

        if ($person->userId > 0) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "请先将用户和该人物解除绑定");
            exit;
        }

        $familyDB = new CDBFamily();

        $familyInfo = $familyDB->getFamilyById($person->familyId);

        if ($familyInfo == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit; 
        }

        //检查人物是否是家族的创始人
        if ($familyDB->isOriginatorForFamily($person->familyId, $person->userId)) {
            //无论如何创始人都是无法直接删除的
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $familyDB->pdo->beginTransaction();

        //检查人物是否可以被删除
        //能删的情况
        //1. 非核心人物
        //2. 上下代只有一边有人

        if ($familyInfo->groupType == 1) {

            if (!($person->type == 2 ||
                (hasParent($person) * hasChild($person) == 0 && hasSpouse($person) == 0))
            ) {
                $familyDB->pdo->rollback();
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物存在其他关联，不允许直接删除");
                exit;
            }
        } else if ($familyInfo->groupType == 2) {
            if (!((hasParent($person) * hasChild($person) == 0)))
            {
                $familyDB->pdo->rollback();
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物存在其他关联，不允许直接删除");
                exit;
            }
        }

        $samePersonId = $person->samePersonId;
        //删除之前先更新same_personId
        $updateRow = $personDB->updateSamePersonId($samePersonId);

        // 删除人物
        $operation = $personDB->deletePerson($personId, false);

        // 更新家族辈分
        $levelInfo = $familyDB->getFamilyLevelInfo($person->familyId);
        $level = array();
        foreach ($levelInfo as $i) {
            array_push($level, intval($i['familyLevel']));
        }
        $maxLevel = max($level);
        $minLevel = min($level);
        $familyDB->updateFamilyLevel($person->familyId, $minLevel, $maxLevel);

        $data['operation'] = $operation;

        //前台需要判断是否有修改
        if ($operation) {
            //删出人物相关的大事件
            $CDBEvent = new CDBEvent();

            $CDBEvent->deleteEventByPersonId($personId);

            $familyNewsDB = new CDBFamilyNews();

            $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['DELETEPERSON'], $userId, $personId, $person->name, $userId);

            //删除人物时清除人物的过继关系
            $adoptionList = $personDB->getAdoptionPersonList($personId);
            foreach ($adoptionList as $adoption){
                $delRow = $personDB->deleteAdoptionPersonByPersonId($adoption['src_personId'], $adoption['dest_personId']);
                if ($delRow>0){
                    if (!$personDB->checkSrcPersonIdIsExists($adoption['src_personId'])){
                        $personDB->restoreIsAdoption($adoption['src_personId']);
                    }
                    if (!$personDB->checkDestPersonIdIsExists($adoption['dest_personId'])){
                        $personDB->restoreIsAdoption($adoption['dest_personId']);
                    }
                }
            }

            if (!HttpContext::getInstance()->getEnv('private_db_host')) {
                // 删除人物时绑定关系也要删除
                $familyDB->deletePersonBindRecord($personId, $userId);
            }
            
            
            $familyDB->pdo->commit();

            if (!HttpContext::getInstance()->getEnv('private_db_host')) {

                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 3;
                $source = [];
                $CDBPushPerson->setPushListForDeletePerson($modelId, $action, $source);
            }


            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            $familyDB->pdo->rollback();
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $familyDB->pdo->rollback();
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}