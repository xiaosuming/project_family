<?php
/**
 * 取消人物与用户之间的绑定（踢人）
 * author:jiangpengfei
 * date:2017-10-09
 */
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    use Util\PostGenerate;

    $personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));

    try{

        if($personId === ""){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }

        $personDB = new CDBPerson();
        $userId = $GLOBALS['userId'];
        //检查操作的权限
        if(!$personDB->verifyUserIdAndPersonId($userId,$personId)){
            Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        $person = $personDB->getPersonById($personId); 		//先获取人物对象

        $familyDB = new CDBFamily();
        //检查人物是否是家族的创始人
        if($familyDB->isOriginatorForFamily($person->familyId,$person->userId)){
            //无论如何创始人都是无法取消绑定的
            Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        //不是管理员没有权限
        if(!$familyDB->isAdminForFamily($person->familyId,$userId)){
            Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        //是管理员，但不是创始人，操作对象也是管理员也不能操作
        if(!$familyDB->isOriginatorForFamily($person->familyId,$userId) && $familyDB->isAdminForFamily($person->familyId,$person->$userId)){
            Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
        // 解除人物绑定  则该人物绑定的用户需要同步取消关注该圈子

        $operation = $personDB->cancelUserBindAndCircle($personId);
        $data['operation'] = $operation;
        //前台需要判断是否有修改
        if($operation){
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else{
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "取消绑定失败");
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
