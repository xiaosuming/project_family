<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-5-13
 * Time: 下午2:04
 */

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\SysLogger;
use Util\Util;

try {

    $personId = Check::checkInteger($params['personId'] ?? '');
    $color = Check::check($params['color'] ?? '');
    $userId = $GLOBALS['userId'];

    $personDB = new CDBPerson();

    $familyId = $personDB->getFamilyIdByPersonId($personId);

    $familyDB = new CDBFamily();
    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $colorId = $personDB->addUserRemarkPersonColor($userId, $familyId, $personId, $color);
    $data['colorId'] = $colorId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
