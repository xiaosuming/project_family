<?php
/**
 * 获取用户可以@的用户列表
 * author: jiangpengfei
 * date: 2018-03-23
 */

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];

try{
    $familyDB = new CDBFamily();
    $familys = $familyDB->getJoinFamilyList($userId);

    $familyIds = array();
    array_walk($familys,function($family) use (&$familyIds){
        array_push($familyIds,$family['id']);
    });

    if (count($familyIds) === 0) {
        $data['users'] = array();
    } else {
        $personDB = new CDBPerson();
        $users = $personDB->getFamilyUsers($familyIds);
        
        $filterUsers = array();

        foreach($users as $user) {
            if ($user['id'] !== $userId) {
                array_push($filterUsers,$user);
            }
        }

        $data['users'] = $filterUsers;
    }

    Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}