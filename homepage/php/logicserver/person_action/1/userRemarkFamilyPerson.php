<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-5-13
 * Time: 上午10:26
 */

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\SysLogger;
use Util\Util;

try {
    $personId = Check::checkInteger($params['personId'] ?? '');
    $remarkType = Check::checkInteger($params['remarkType'] ?? ''); // 1待办
    $userId = $GLOBALS['userId'];

    $personDB = new CDBPerson();

    $familyId = $personDB->getFamilyIdByPersonId($personId);

    if ($familyId == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '人物id不存在');
        exit;
    }

    $familyDB = new CDBFamily();
    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误,不是该家族的用户');
        exit;
    }

    $remarkId = $personDB->addUserPersonIdRemark($familyId,$personId, $userId, $remarkType);
    $data['remarkId'] = $remarkId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
