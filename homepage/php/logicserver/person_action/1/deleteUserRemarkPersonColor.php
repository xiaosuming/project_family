<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-5-13
 * Time: 下午2:34
 */
use DB\CDBPerson;
use Util\Check;
use Util\SysLogger;
use Util\Util;

try {

    $colorId = Check::checkInteger($params['colorId'] ?? '');
    $userId = $GLOBALS['userId'];

    $personDB = new CDBPerson();

    if (!$personDB->checkUserCreatedUserRemarkPersonColor($colorId,$userId)){
        Util::printResult($GLOBALS['ERROR_PERMISSION'],'权限错误');
        exit;
    }

    $delRow = $personDB->deleteUserRemarkPersonColor($colorId);
    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
