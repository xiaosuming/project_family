<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/12/19
 * Time: 13:33
 */
	use Util\Check;
	use DB\CDBPerson;
	use Util\Util;
	$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));				//人物id
    try{
    	if($personId == '') {
			Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
			exit;
		}else{
			$userId = $GLOBALS['userId'];
			$personDB = new CDBPerson();
			$data['flag'] = $personDB->verifyUserIdAndPersonId($userId,$personId);
			Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
			exit;
		}
	}
	catch (PDOException $e){
		//异常处理
		$logger->error(Util::exceptionFormat($e));
		Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
	}