<?php
/**
 * 对于家族这种，是采用先录入人物信息，再绑定用户的方式
 * 而对于同学，兴趣小组这种，采取的是直接加入
 * 加入的时候，会带着这个用户的名片信息填入
 * @author: jiangpengfei
 * @date: 2018-11-08
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use DB\CDBAccount;
use Util\Check;
use Util\TempStorage;

$name = Check::check($params['name'] ?? '', 1, 30);
$inviteCode = Check::check($params['inviteCode'] ?? '');

$userId = $GLOBALS['userId'];

try {
    $tempStorage = new TempStorage();
    $groupId = $tempStorage->get($inviteCode);

    // 检查当前用户是否已经加入该族群
    $familyDB = new CDBFamily();

    if ($familyDB->isUserForFamily($groupId, $userId)) {
        // 已经加入家族了
        $data['exist'] = true;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        exit();
    }

    $accountDB = new CDBAccount();
    $userInfo = $accountDB->getUserInfoById($userId);

    $personDB = new CDBPerson();
    $recordId = $personDB->joinGroup($name, $groupId,2, $userInfo['photo'], $userId);

    $data['recordId'] = $recordId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}


