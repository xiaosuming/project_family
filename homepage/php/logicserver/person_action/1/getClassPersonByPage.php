<?php
/**
 * 分页获取班级中的人物
 * Created by PhpStorm.
 * User: xll
 * Date: 19-2-14
 * Time: 上午10:16
 */

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Paginator;
use Util\Util;

$familyId = Check::checkInteger($params['familyId'] ?? ''); //班级id
$maxId = Check::checkInteger($params['maxId'] ?? 0); // 最大id
$sinceId = Check::checkInteger($params['sinceId'] ?? 0); //
$count = Check::checkInteger($params['count'] ?? 10);

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $familyDB = new CDBFamily();
    // 检查家族类型是否是家庭,3是师生
    $familyDetail = $familyDB->getFamilyById($familyId);
    if ($familyDetail->groupType != 3) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
        exit;
    }

    $personDB = new CDBPerson();

    $total = $personDB->getCountPersonFromClass($familyId);

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $personList = $personDB->getClassPersonPaging($familyId, $maxId, $sinceId, $count);

    $len = count($personList);

    $nextSinceId = $personList[0]['id'];

    $nextMaxId = $personList[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $personList, $nextMaxId, $nextSinceId, $lastPage);
    $paginator->printPage();

} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
