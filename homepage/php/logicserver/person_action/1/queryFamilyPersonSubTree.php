<?php
/**
 * 获取家族树的一个子树,也就是查询某个用户在某个家族对应人物的直系亲属和自己
 * author:jiangpengfei
 * date:2016-12-10
 */
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    //queryFamilyPerson($familyId)
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));				//必须
    $requestUserId = Check::check(trim(isset($params['userId']) ? $params['userId'] : ''));				//非必须

    //如果默认为空，则取当前用户的子树
    if($requestUserId == ""){
        $requestUserId = $GLOBALS['userId'];
    }

    try{
        if($familyId!=""&&$requestUserId!=""){
            $familyDB = new CDBFamily();
            //检查操作的权限
            $userId = $GLOBALS['userId'];
            if(!$familyDB->isUserForFamily($familyId,$userId)&&!$familyDB->isAdminForFamily($familyId,$userId)&&!$familyDB->isOriginatorForFamily($familyId,$userId)){
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }
            
            $personDB = new CDBPerson();
            $persons = $personDB->getFamilyPersonsSubTree($familyId, $requestUserId);
            $data['persons'] = $persons;
            if(count($persons) > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult( $GLOBALS['ERROR_SQL_INSERT'], "sql查询时发生错误");
            }
            
        }else{
            Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }