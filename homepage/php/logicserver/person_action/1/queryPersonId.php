<?php
    /**
     * 根据用户id和家族id查询人物id
     */
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    //getPersonId($userId, $familyId)
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));		//必须

    try{
        if($familyId!=""){
            $familyDB = new CDBFamily();
            $userId = $GLOBALS['userId'];
            //检查用户对当前家族操作的权限
            if(!$familyDB->isUserForFamily($familyId,$userId)){
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            $personDB = new CDBPerson();
            $id = $personDB->getPersonId($userId, $familyId);
            $data['personId'] = $id;
            if($id > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], null);
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }