<?php
/**
 * 通过分享获取家族人物
 * @author: jiangpengfei
 * @date: 2018-02-02
 */
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use DB\CDBFamilyShare;

$shareCode = Check::check($params['shareCode'] ?? '');
$ip = Util::getRequestIp();

if($shareCode == ""){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

try{
    $familyShareDB = new CDBFamilyShare();
    $familyShare = $familyShareDB->getFamilyShareModelByShareCode($shareCode);
    $shareTime = strtotime($familyShare->createTime);
    $currentTime = time();

    //检查这个分享是否有效
    if($currentTime - $shareTime > 60 * 60 * 3){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "分享已过期，无权查看");
        exit;
    }

    //检查人数是否超过限制
    if($familyShareDB->checkShareIdAndIpExist($familyShare->id,$ip)){
        //存在了，给这条访问+1
        $familyShareDB->updateFamilyShareLogViewTimes($familyShare->id,$ip);
    }else{
        //不存在，检查是否超限
        $total = $familyShareDB->getFamilyShareLogTotal($familyShare->id);
        if($total >= $familyShare->viewsLimit){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "本次分享人数已达上限");
            exit;
        }
    }

    //获取shareCode对应的家族人物资料
    $levelLimit = $familyShare->levelLimit;
    $familyId = $familyShare->familyId;

    $personDB = new CDBPerson();
    $persons = $personDB->getFamilyPersons($familyId,$levelLimit);

    $data['persons'] = $persons;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;

}catch(PDOException $e){
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}