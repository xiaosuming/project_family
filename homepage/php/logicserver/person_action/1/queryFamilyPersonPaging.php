<?php
    use Util\Util;
    use Util\Check;
    use Util\Pager;
    use DB\CDBPerson;
    use DB\CDBFamily;
    //queryFamilyPerson($familyId)
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));				//必须
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : 1));			//必须
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] :10));				//必须
    $name = Check::check(trim(isset($params['name'])?$params['name']:''));						
    $level = Check::check(trim(isset($params['level'])?$params['level']:""));
    $orderby = Check::check(trim(isset($params['orderby'])?$params['orderby']:""));
    
    try{
        if($familyId!=""){
            $userId = $GLOBALS['userId'];
            
            $familyDB = new CDBFamily();

            $family = $familyDB->getFamilyById($familyId);

            if ($family == null) {
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            if ($family->openType == 0) {
                // 私密族群
                //检查查询的权限
                if(!$familyDB->isUserForFamily($familyId,$userId)&&!$familyDB->isAdminForFamily($familyId,$userId)&&!$familyDB->isOriginatorForFamily($familyId,$userId)){
                    Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                    exit;
                }
            }
            
            $personDB = new CDBPerson();
            $count = $personDB->getFamilyPersonsCount($familyId, $name, $level);
            if($pageSize == -1)
                $pageSize = $count;
                
            $personPaging = $personDB->getFamilyPersonsPaging($familyId, $pageIndex, $pageSize, $name, $level, $orderby);

            if($count >= 0){
                $paging = new Pager($count,$personPaging,$pageIndex,$pageSize);
                $paging->printPage();
            }else{
                Util::printResult( $GLOBALS['ERROR_SQL_INSERT'], "sql查询时发生错误");
            }
            
        }else{
            Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }