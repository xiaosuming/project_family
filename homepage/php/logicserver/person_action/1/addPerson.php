<?php
    /**
     * 这个接口当前不在使用
     */
    /*已重新实现图片上传功能
    *@author kellen
    *date :   2017/2/22
    */

    use DB\CDBPushPerson;
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\ImageUpload;
    use Util\Check;
    //addPerson($userId,$familyId,$name,$gender,$address,$birthday,$photo){
    
    $name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));				//必须
    $gender = Check::check(trim(isset($params['gender']) ? $params['gender'] : ''));			//必须
    $address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));
    $birthday = Check::check(trim(isset($params['birthday']) ? $params['birthday'] : ''));
    $familyId = Check::check(trim(isset($params['familyId']) ? $params['familyId'] : ''));
    $file = isset($_FILES['file']) ? $_FILES['file'] : '';
    $userId = $GLOBALS['userId'];

    try{

        if($name!=""&&$gender!=""){
            $personDB = new CDBPerson();
            $familyDB = new CDBFamily();
            //检查操作的权限
            if(!$familyDB->isUserForFamily($familyId, $userId)){
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }
            if($file != null)
            {
                $imageUpload = new ImageUpload($file , "/images/upload/");
                $updateResult = $imageUpload->upload();
                $data['result'] = $updateResult;
                if($updateResult == 1)
                {
                    $id = $personDB->addPerson("",$familyId , $name , $gender , $address , $birthday , $imageUpload->getPath());
                }else{
                    Util::printResult($GLOBALS['ERROR_SQL_UPDATE'] , "图片上传失败");
                    exit;
                }
            }else{
                $id = $personDB->addPerson("", $familyId , $name , $gender , $address , $birthday , "");
            }
            $data['addedPersonId'] = $id;
            if($id > 0){

                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
            }
            
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }