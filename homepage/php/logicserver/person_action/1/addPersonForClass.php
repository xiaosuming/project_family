<?php
/**
 * 向班级中添加人物
 * Created by PhpStorm.
 * User: xll
 * Date: 19-2-13
 * Time: 下午2:42
 */

use DB\CDBFamily;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use Model\Person;
use Util\Check;
use Util\PhotoGenerator;
use Util\Util;

$familyId = Check::checkInteger($params['familyId'] ?? ''); //班级id
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''), 1, 30);  //名字               //必须
$gender = Check::checkInteger(trim(isset($params['gender']) ? $params['gender'] : ''));//必须 性别
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : '')); //头像
$type = Check::checkInteger(trim($params['type'] ?? ''));  //类型关系 1.老师 2.学生
$userId = $GLOBALS['userId'];

try {

    if ($name != "" && $gender != "") {

        //检查人物照片是否定义，如果没有则随机产生一个照片
        if ($photo == "") {
            $photo = PhotoGenerator::generatePersonPhotoFromResource();
        } else {

            //TODO: 兼容老版本,之后可以删除
            if (strpos($photo, "img1") > 0) {
                $photo = str_replace("img1", "image", $photo);
            }
        }

        //检查操作的权限
        $familyDB = new CDBFamily();

        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        // 检查家族类型是否是家庭,3是师生
        $familyDetail = $familyDB->getFamilyById($familyId);
        if ($familyDetail->groupType != 3) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
            exit;
        }

        $person = new Person();
        $person->id = Util::getNextId();
        $person->name = $name;
        $person->gender = $gender;
        $person->photo = $photo;
        $person->familyId = $familyId;
        $person->type = $type;

        $personDB = new CDBPerson();
        $personId = $personDB->addTeacherAndStudent($person, $userId);
        $person = $personDB->getPersonById($personId);

        $data['addedPersonId'] = $personId;
        if ($personId > 0) {

            $familyNewsDB = new CDBFamilyNews();

            $familyNewsDB->addFamilyNews($familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['ADDRELATEPERSON'], $userId, $personId, $name, $userId);

            if ($GLOBALS['TEST_PERSON_TASK']) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 1;
                $source = $person;
                $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
            }
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "添加出错");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

