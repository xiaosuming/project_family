<?php
    /**
     * 根据姓名在家族中查找人物
     * @author jiangpegnfei
     * @date : 2017/12/06
     */

    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    
    $personName = Check::check(trim(isset($params['personName']) ? $params['personName'] : ''));				//人物姓名
    $userId = $GLOBALS['userId'];

    try{

        if($personName == ""){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }

        $familyDB = new CDBFamily();
        $familyList = $familyDB->getJoinFamilyList($userId);    //获取加入的家族
        $familyListTmp = $familyDB->getJoinFamilyForTmp($userId);       //获取临时加入的家族

        $familyIds = array();
        foreach($familyList as $family){
            array_push($familyIds,$family['id']);
        }

        foreach($familyListTmp as $family){
            array_push($familyIds,$family['id']);
        }
            
        $personDB = new CDBPerson();
        $persons = $personDB->getPersonByNameAndFamilyIds($personName,$familyIds);
        
        $data['persons'] = $persons;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }