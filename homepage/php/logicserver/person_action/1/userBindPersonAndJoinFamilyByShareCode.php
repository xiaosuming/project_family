<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-2
 * Time: 下午6:38
 */

use DB\CDBFamily;
use DB\CDBInformation;
use DB\CDBPerson;
use DB\CDBQuestion;
use Util\Check;
use Util\Util;

$shareCode = Check::check($params['shareCode'] ?? '');
$fromUserId = Check::checkInteger($params['fromUserId'] ?? '');
$userId = $GLOBALS['userId'];

if ($shareCode == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}
try {

    $personDB = new CDBPerson();
    $person = $personDB->getPersonByShareCode($shareCode);

    if (is_null($person)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "此人物不存在");
        exit;
    }

    if ($person->userId != 0) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '该人物已经绑定用户');
        exit;
    }


    $familyDB = new CDBFamily();

    if (!$familyDB->isUserForFamily($person->familyId, $fromUserId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }

    $family = $familyDB->getFamilyById($person->familyId);
    $socialCircleId = $family->socialCircleId;

    if ($personDB->bindUserToPersonByShareCode($userId, $socialCircleId, $person, $fromUserId)) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], '绑定人物并加入家族成功');
        exit;
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
