<?php
/**
 * author: jiangpengfei
 * Date: 2018/02/06
 */
	use Util\Check;
	use DB\CDBPerson;
	use Util\Util;
	$shareCode = Check::check(trim(isset($params['shareCode']) ? $params['shareCode'] : ''));				//人物分享码
    try{
    	if($shareCode == '') {
			Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
			exit;
		}else{
			$personDB = new CDBPerson();
			$data['used'] = $personDB->checkShareCodeIsUsed($shareCode);
			Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);
			exit;
		}
	}
	catch (PDOException $e){
		//异常处理
		$logger->error(Util::exceptionFormat($e));
		Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
	}