<?php
/**
 * Created by PhpStorm.
 * User: liuzhenhao
 * Date: 20-07-13
 * Time: 下午12:26
 */

use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
use Util\Util;


$personId = Check::checkInteger($params['personId'] ?? ''); // 人物id

try {
    $personDB = new CDBPerson();
    $familyDB = new CDBFamily();
    $userId = $GLOBALS['userId'];
//    检查操作的权限
//    if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
//        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
//        exit;
//    }
    //检查操作的权限
    $person = $personDB->getPersonSimpleInfoById($personId);
    if (!$familyDB->getUserPermission($userId, $person->familyId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }

    $data = $personDB->getChildrenRanking($personId);

    $result['ranking'] = $data;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
