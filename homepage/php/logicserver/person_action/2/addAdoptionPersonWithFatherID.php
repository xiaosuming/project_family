<?php

/**
 * User: jiangpengfei
 * Date: 2020-04-24
 * Time: 17:11
 */

use DB\CDBPerson;
use Util\Check;
use Util\Util;
use DB\CDBArea;
use DB\CDBFamilyNews;
use DB\CDBPushPerson;
use DB\DocumentDB;
use DB\CDBFamily;
use Model\Document;
use Model\Person;
use Util\PhotoGenerator;
use Util\HttpContext;

$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''), 1, 30);                //必须
$gender = trim(isset($params['gender']) ? $params['gender'] : '');            //必须
$country = Check::checkInteger(trim(isset($params['country']) ? $params['country'] : 0)); //国家 中国 默认为 0
$province = Check::checkInteger(trim(isset($params['province']) ? $params['province'] : 0)); //省 没有默认为 0
$city = Check::checkInteger(trim(isset($params['city']) ? $params['city'] : 0)); //市 没有默认为 0
$area = Check::checkInteger(trim(isset($params['area']) ? $params['area'] : 0)); //区 没有默认为 0
$town = Check::checkInteger(trim(isset($params['town']) ? $params['town'] : 0)); //乡 没有默认为 0
$address = trim(isset($params['address']) ? $params['address'] : '');
$birthday = trim(isset($params['birthday']) ? $params['birthday'] : null);
$relation = trim(isset($params['relation']) ? $params['relation'] : '');
$corePersonId = trim(isset($params['corePersonId']) ? $params['corePersonId'] : '');    //必须
$photo = trim(isset($params['photo']) ? $params['photo'] : '');
$zi = Check::check(trim(isset($params['zi']) ? $params['zi'] : ''), 0, 10);
$zpname = Check::check(trim(isset($params['zpname']) ? $params['zpname'] : ''), 0, 10);
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : ''), 0, 255);
$isDead = Check::checkInteger(trim(isset($params['isDead']) ? $params['isDead'] : 0)); //默认为0 0表示未去世
$deadTime = trim(isset($params['deadTime']) ? $params['deadTime'] : null);
$ranking = Check::checkInteger($params['ranking'] ?? 0);
$profileText = Check::check($params['profileText'] ?? '');          // 详细信息的输入
$sideText = Check::check($params['sideText'] ?? '');                      // 侧边栏文本
$destFatherID = Check::checkInteger($params['destFatherID'] ?? 0);   // 过继到的父亲ID
$adoptionMode = Check::checkInteger($params['adoptionMode'] ?? 0);   // 过继模式，0是出继，1是入继

if ($name == "" || $gender == "" || $corePersonId == "" || $destFatherID == 0) {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

//检查人物照片是否定义，如果没有则随机产生一个照片
if ($photo == "") {
    $photo = PhotoGenerator::generatePersonPhotoFromResource();
} else {
    //TODO: 兼容老版本,之后可以删除
    if (strpos($photo,"img1") > 0) {
        $photo = str_replace("img1","image",$photo);
    }
}
//检查性别与关系
switch ($relation) {
    case "6":
        if ($gender != "1") {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
            exit;
        }
        break;
    case "7":
        if ($gender != "0") {
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
            exit;
        }
        break;
    default:
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '人物关系不正确');
        exit;
}
$personDB = new CDBPerson();
$personDB->init();
try {

    $personDB->pdo->beginTransaction();
    $personDetail = $personDB->getPersonById($corePersonId);
    if ($personDetail == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }

    $familyId = $personDetail->familyId;        //获取PersonDetail对象中的familyId

    $destFather = $personDB->getPersonById($destFatherID);
    if ($destFather == null) {
        Util::printResult($GLOBALS['ERROR_ERMISSION'], "操作权限错误");
        exit;
    }

    $destFamilyID = $destFather->familyId;

    // 不在同一个家族
    if ($familyId != $destFamilyID) {
        Util::printResult($GLOBALS['ERROR_ERMISSION'], "目标父亲不在同一个家族");
        exit;
    }

    $userId = $GLOBALS['userId'];
    //检查操作的权限
    $familyDB = new CDBFamily();
    if (!$personDB->verifyUserIdAndPersonId($userId, $corePersonId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "当前人物操作权限错误");
        exit;
    }
    if (!$personDB->verifyUserIdAndPersonId($userId, $destFatherID)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "目标父亲无操作权限");
        exit;
    }
    if ($birthday!=null && $deadTime !=''){
        if ($birthday >= $deadTime) {
            Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "出生时间不能大于去世时间");
            exit;
        }
    }

    // 检查家族类型是否是家庭,1是家族
    $familyDetail = $familyDB->getFamilyById($familyId);

    if ($familyDetail->groupType != 1) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
        exit;
    }

    $CDBArea = new CDBArea();
    $result = $CDBArea->getAreaNameById($country);
    $countryName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($province);
    $provinceName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($city);
    $cityName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($area);
    $areaName = $result['areaName'] ?? '0';
    $result = $CDBArea->getAreaNameById($town);
    $townName = $result['areaName'] ?? '0';

    $person = new Person();
    $person->id = Util::getNextId();
    $person->country = $country;
    $person->countryName = $countryName;
    $person->province = $province;
    $person->provinceName = $provinceName;
    $person->city = $city;
    $person->cityName = $cityName;
    $person->area = $area;
    $person->areaName = $areaName;
    $person->town = $town;
    $person->townName = $townName;
    $person->address = $address;
    $person->name = $name;
    $person->birthday = $birthday;
    $person->gender = $gender;
    $person->photo = $photo;
    $person->familyId = $familyId;
    $person->relation = $relation;
    $person->corePersonId = $corePersonId;
    $person->branchId = $personDetail->branchId;
    $person->zi = $zi;
    $person->remark = $remark;
    $person->isDead = $isDead;
    $person->deadTime = $deadTime;
    $person->zpname = $zpname;
    $person->ranking = $ranking;
    $person->profileText = $profileText;
    $person->sideText = $sideText;

    $person1Id = $personDB->addRelatePerson($person, false);
    
    $person2 = $person;
    $person2->id = Util::getNextId();
    $person2->corePersonId = $destFatherID;
    if ($person2->birthday == "NULL") {
        $person2->birthday = null;
    }
    if ($person2->deadTime == "NULL") {
        $person2->deadTime = null;
    }
    $person2Id = $personDB->addRelatePerson($person2, false);

    $srcPersonID = 0;
    $destPersonID = 0;
    if ($adoptionMode == 0) {
        $srcPersonID = $person1Id;
        $destPersonID = $person2Id;
    } else {
        $srcPersonID = $person2Id;
        $destPersonID = $person1Id;
    }
 
    $insertId = $personDB->addAdoptionPersonByPersonId($srcPersonID, $destPersonID, false);
    $data['srcPersonID'] = $person1Id;
    $data['destPersonID'] = $person2Id;
    $data['inserId'] = $insertId;
    $personDB->pdo->commit();
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    $personDB->pdo->rollback();
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

