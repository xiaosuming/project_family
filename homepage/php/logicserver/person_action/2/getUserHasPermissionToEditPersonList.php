<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-4
 * Time: 下午3:17
 */

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

$maxId = Check::checkInteger($params['maxId'] ?? 0); // 最大id
$sinceId = Check::checkInteger($params['sinceId'] ?? 0); //
$count = Check::checkInteger($params['count'] ?? 10);
$familyId = Check::checkInteger($params['familyId'] ?? '');
$userId = $GLOBALS['userId'];

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();

    if (!$familyDB->isUserForFamily($familyId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '权限错误');
        exit;
    }
    if ($familyDB->isAdminForFamily($familyId, $userId) || $familyDB->isOriginatorForFamily($familyId, $userId)) {

        $total = $personDB->getCountPersonsByFamilyId($familyId);
        $personList = $personDB->getPersonListByFamilyId($familyId, $maxId, $sinceId, $count);

    } else {
        $total = $personDB->getCountUserCreatePersonsByFamilyIdAndUserId($familyId, $userId);
        $personList = $personDB->getUserCreatePersonListByFamilyIdAndUserId($familyId, $userId, $maxId, $sinceId, $count);
    }

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $len = count($personList);

    $nextSinceId = $personList[0]['id'];

    $nextMaxId = $personList[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $personList, $nextMaxId, $nextSinceId, $lastPage);

    $paginator->printPage();

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
