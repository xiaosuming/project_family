<?php

/**
 * User: 刘振豪
 * Date: 2020-09-04
 * Time: 17:11
 */

use DB\CDBPerson;
use Util\Check;
use Util\Util;
use DB\CDBArea;
use DB\CDBFamilyNews;
use DB\CDBPushPerson;
use DB\DocumentDB;
use DB\CDBFamily;
use Model\Document;
use Model\Person;
use Util\PhotoGenerator;
use Util\HttpContext;

$relation = trim(isset($params['relation']) ? $params['relation'] : '');
$corePersonId = trim(isset($params['corePersonId']) ? $params['corePersonId'] : '');    //必须
$destFatherID = Check::checkInteger($params['destFatherID'] ?? 0);   // 过继到的父亲ID
$adoptionMode = Check::checkInteger($params['adoptionMode'] ?? 0);   // 过继模式，0是出继，1是入继
$userId = $GLOBALS['userId'];

if ($corePersonId == "" || $destFatherID == 0) {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

//检查人物照片是否定义，如果没有则随机产生一个照片
//if ($photo == "") {
//    $photo = PhotoGenerator::generatePersonPhotoFromResource();
//} else {
//    //TODO: 兼容老版本,之后可以删除
//    if (strpos($photo,"img1") > 0) {
//        $photo = str_replace("img1","image",$photo);
//    }
//}
//检查性别与关系
//switch ($relation) {
//    case "6":
//        if ($gender != "1") {
//            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
//            exit;
//        }
//        break;
//    case "7":
//        if ($gender != "0") {
//            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '性别不对');
//            exit;
//        }
//        break;
//    default:
//        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '人物关系不正确');
//        exit;
//}

try {
    $personDB = new CDBPerson();
    $personDB->init();

    $personDB->pdo->beginTransaction();

    $personDetail = $personDB->getPersonObjectById($corePersonId);
    $destFather = $personDB->getPersonObjectById($destFatherID);

    if ($personDetail == null || $personDetail->type == 2 || $personDetail->isNotMain == 1) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }

    if ($personDetail->isAdoption != 0){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "已存在过继关系");
        exit;
    }

    $familyId = $personDetail->familyId;        //获取PersonDetail对象中的familyId


    if ($destFather == null || $destFather->type == 2 || $destFather->isNotMain == 1) {
        Util::printResult($GLOBALS['ERROR_ERMISSION'], "操作权限错误");
        exit;
    }

    if ($personDetail->level - $destFather->level != 1) {
        Util::printResult($GLOBALS['ERROR_ERMISSION'], "嗣父世代错误");
        exit;
    }

    $destFamilyID = $destFather->familyId;

    // 不在同一个家族
    if ($familyId != $destFamilyID) {
        Util::printResult($GLOBALS['ERROR_ERMISSION'], "目标父亲不在同一个家族");
        exit;
    }

    //检查操作的权限
    $familyDB = new CDBFamily();
    $permission = $familyDB->getUserPermission($userId,$familyId);
    if (Util::checkPermission($permission,[$personDetail->familyIndex,$destFather->familyIndex]) < 3){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
//    if (!$personDB->verifyUserIdAndPersonId($userId, $corePersonId)) {
//        Util::printResult($GLOBALS['ERROR_PERMISSION'], "当前人物操作权限错误");
//        exit;
//    }
//    if (!$personDB->verifyUserIdAndPersonId($userId, $destFatherID)) {
//        Util::printResult($GLOBALS['ERROR_PERMISSION'], "目标父亲无操作权限");
//        exit;
//    }

    // 检查家族类型是否是家庭,1是家族
    $familyDetail = $familyDB->getFamilyById($familyId);

    if ($familyDetail->groupType != 1) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
        exit;
    }

    $rankings = $personDB->getChildrenRanking($destFatherID);
    $ranking = $rankings ? end($rankings):0;

    $person = $personDetail;
    $person->id = Util::getNextId();
    $person->relation = $relation;
    $person->corePersonId = $destFatherID;
    $person->ranking = $ranking;
//    var_dump($person);
    // 根据添加关系处理被添加人属性
    switch($relation){
        case $GLOBALS['RELATION_SON'] :
        {
            $person->level = $destFather->level+1;
            $person->gender = 1;
            $person->type = 1;
            $person->familyIndex = 	$personDB->getLastPersonFamilyIndex($familyId,$destFather->level,$destFather->familyIndex) + 1 ;
            if ($destFather->gender == 0 ){
                $person->isNotMain = 1;
            }else{
                $person->isNotMain = $destFather->isNotMain == 1? 1:0;
            }
            break;
        }
        case $GLOBALS['RELATION_DAUGHTER'] :
        {
            $person->level = $destFather->level+1;
            $person->gender = 0;
            $person->type =1;
            if ($destFather->gender == 0 ){
                $person->isNotMain = 1;
            }else{
                $person->isNotMain = $destFather->isNotMain == 1? 1:0;
            }
            $person->familyIndex = 	$personDB->getLastPersonFamilyIndex($familyId,$destFather->level,$destFather->familyIndex) + 1 ;
            break;
        }
        default:
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '人物关系不正确');
            exit;
    }

    $person1Id = $personDB->addRelatePerson2($person, false);
    if ($person1Id <= 0){
        $personDB->pdo->rollback();
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错,数据库异常");
    }
    $person1 = $personDB->getPersonObjectById($person1Id);
//    var_dump($person1Id);
//    $srcPersonID = 0;
//    $destPersonID = 0;
    if ($adoptionMode == 1) {
        $srcPersonID = $person1Id;
        $destPersonID = $corePersonId;
        $data['srcPersonID'] = $person1Id;
        $data['destPersonID'] = $corePersonId;

        $insertId = $personDB->addAdoptionPersonByPersonId($person1Id, $corePersonId, false);
        $personDB->pdo->commit();
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } elseif ($adoptionMode == 0) {
        $srcPersonID = $corePersonId;
        $destPersonID = $person1Id;
        $data['srcPersonID'] = $corePersonId;
        $data['destPersonID'] = $person1Id;

        if ($personDetail->daughter || $personDetail->son){
            # 开始把假人子女的分支移动到入继方
            $move = $personDB->movePersonV3ForAdoption($corePersonId,$person1Id,false);
            if (!$move){
                $personDB->pdo->rollback();
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "过继失败");
                exit;
            }

        }

        $insertId = $personDB->addAdoptionPersonByPersonId($corePersonId, $person1Id, false);
        $personDB->pdo->commit();
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    }else{
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '出入继关系错误');
        exit;
    }





//    $insertId = $personDB->addAdoptionPersonByPersonId($srcPersonID, $destPersonID, false);
//    $data['srcPersonID'] = $person1Id;
//    $data['destPersonID'] = $person2Id;
//    $data['inserId'] = $insertId;
//    $personDB->pdo->commit();
//    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);


//    if ($person2->birthday == "NULL") {
//        $person2->birthday = null;
//    }
//    if ($person2->deadTime == "NULL") {
//        $person2->deadTime = null;
//    }

} catch (PDOException $e) {
    $personDB->pdo->rollback();
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

