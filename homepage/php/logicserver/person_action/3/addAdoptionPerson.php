<?php
/**
 * Created by PhpStorm.
 * User: liuzhenhao
 * Date: 2020-09-04
 * Time: 下午2:02
 */

use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
use Util\Util;

$srcPersonId = Check::checkInteger(trim($params['srcPersonId'] ?? '')); //过继人
$destPersonId = Check::checkInteger(trim($params['destPersonId'] ?? '')); //接收人
$familyId = Check::checkInteger(trim($params['familyId'] ?? '')); //家族id

if ($srcPersonId == '' || $destPersonId == '' || $familyId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '参数缺失');
    exit;
}

try {

    $personDB = new CDBPerson();
    $familyDB = new CDBFamily();

    $userId = $GLOBALS['userId'];
    $srcPerson = $personDB->getPersonObjectById($srcPersonId);
    $destPerson = $personDB->getPersonObjectById($destPersonId);

    //检查操作的权限
    $permission = $familyDB->getUserPermission($userId,$familyId);
    if (Util::checkPermission($permission,[$srcPerson->familyIndex,$destPerson->familyIndex]) < 3){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
//    if (!$personDB->verifyUserIdAndPersonId($userId, $srcPersonId)) {
//        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
//        exit;
//    }

    if ($personDB->checkAdoptionExists($srcPersonId, $destPersonId)) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '过继关系已经存在了');
        exit;
    }

    if($personDB->checkDestPersonIdIsExists($srcPersonId) || $personDB->checkSrcPersonIdIsExists($destPersonId)){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '过继权限错误');
        exit;
    }


    $insertId = $personDB->addAdoptionPersonByPersonId($srcPersonId, $destPersonId);
    $data['inserId'] = $insertId;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

