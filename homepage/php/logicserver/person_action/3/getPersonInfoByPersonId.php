<?php

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
$needOtherInfo = Check::checkInteger(trim(isset($params['needOtherInfo']) ? $params['needOtherInfo'] : 0));

try {
    //检查用户权限


    if ($personId != "") {
        $personDB = new CDBPerson();
        $person = $personDB->getPersonByIdV3($personId);
        $data['person'] = $person;
//        print_r($person);
        if ($person != null) {
            //检查操作的权限

            $familyDB = new CDBFamily();

            $family = $familyDB->getFamilyById($person['familyId']);

            if ($family == null) {
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
                exit;
            }

            if ($family->openType == 0) {

                if (!$familyDB->getUserPermission($userId, $person['familyId'])) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                    exit;
                }
            }

            $pdfTitle = $personDB->getPersonPdfTitle($personId);
            $data['person']['title'] = $pdfTitle ? $pdfTitle['title']:"";
            $data['person']['branchName'] = $pdfTitle ? $pdfTitle['branchName']:"";
            $data['person']['zan'] = $pdfTitle ? $pdfTitle['zan']:"";
            $data['person']['zanType'] = $pdfTitle ? $pdfTitle['zanType']:"";
            $data['person']['originName'] = $pdfTitle ? $pdfTitle['originName']:"";

            if ($needOtherInfo){
                $otherInfo = $personDB->getPersonOtherInfo($personId);
                if ($otherInfo){
                    $data['person']['occupation'] = $otherInfo['occupation'];
                    $data['person']['education'] =  $otherInfo['education'];
                    $data['person']['politicalStatus'] = $otherInfo['politicalStatus'];
                    $data['person']['birthProvince'] = $otherInfo['birthProvince'];
                    $data['person']['birthProvinceName'] = $otherInfo['birthProvinceName'];
                    $data['person']['birthCity'] = $otherInfo['birthCity'];
                    $data['person']['birthCityName'] = $otherInfo['birthCityName'];
                    $data['person']['birthArea'] = $otherInfo['birthArea'];
                    $data['person']['birthAreaName'] = $otherInfo['birthAreaName'];
                    $data['person']['burialProvince'] = $otherInfo['burialProvince'];
                    $data['person']['burialProvinceName'] = $otherInfo['burialProvinceName'];
                    $data['person']['burialCity'] = $otherInfo['burialCity'];
                    $data['person']['burialCityName'] = $otherInfo['burialCityName'];
                    $data['person']['burialArea'] = $otherInfo['burialArea'];
                    $data['person']['burialAreaName'] = $otherInfo['burialAreaName'];
                }else{
                    $data['person']['occupation'] = 0;
                    $data['person']['education'] = 0;
                    $data['person']['politicalStatus'] = 0;
                    $data['person']['birthProvince'] = 0;
                    $data['person']['birthProvinceName'] = '';
                    $data['person']['birthCity'] = 0;
                    $data['person']['birthCityName'] = '';
                    $data['person']['birthArea'] = 0;
                    $data['person']['birthAreaName'] = '';
                    $data['person']['burialProvince'] = 0;
                    $data['person']['burialProvinceName'] = '';
                    $data['person']['burialCity'] = 0;
                    $data['person']['burialCityName'] = '';
                    $data['person']['burialArea'] = 0;
                    $data['person']['burialAreaName'] = '';
                }
            }

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], $data);
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
