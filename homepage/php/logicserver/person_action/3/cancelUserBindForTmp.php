<?php
/**
 * 取消人物与用户之间的绑定(用户继续留在家族中)
 * author:liuzhenhao
 * date:2020-08-21
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use DB\CDBMessage;
use Util\Check;
use Util\PostGenerate;
use Model\Message;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
$userId = $GLOBALS['userId'];

try {

    if ($personId === "") {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $personDB = new CDBPerson();
    $familyDB = new CDBFamily();

    $person = $personDB->getPersonObjectById($personId);//先获取人物对象
    if (!$person){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物不存在");
        exit;
    }

    $family = $familyDB->getFamilyById($person->familyId);
    if (!$family){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "家族不存在");
        exit;
    }

    if (!$person->userId){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物未绑定");
        exit;
    }

    $messageCode = Util::generateRandomCode(36);

    if ($person->userId === $userId){

        $beCanceledUserPermission = $familyDB->getUserPermission($person->userId,$person->familyId,true);
        $beCanceledUserPermissionNum = Util::checkPermission($beCanceledUserPermission,[$person->familyIndex]);
        $operation = $personDB->cancelUserBindV3($personId, $person->familyId, $person->userId,true,$beCanceledUserPermissionNum == 2 ? 100:null);

        if ($operation) {
            //向所有的家族管理员都发送这条消息
            if ($beCanceledUserPermissionNum >= 4){
                1;
            }elseif ($beCanceledUserPermissionNum < 4){
                $adminsId = $familyDB->getFamilyChiefAdminId($person->familyId);

                $messageDB = new CDBMessage();
                foreach ($adminsId as $adminId) {
                    $message = new Message();
                    $message->fromUser = $userId;
                    $message->toUser = $adminId['userId'];
                    $message->content = '用户解除绑定';
                    $message->module = $GLOBALS['FAMILY_MODULE'];
                    $message->action = $GLOBALS['APPLY_UNBIND_USER'];
                    $message->recordId = $person->familyId;
                    $message->messageCode = $messageCode;

                    $extData = [
                        "personId" => $personId,
                        "personName" => $person->name,
                        "level" => $person->level,
                        "familyName" => $family->name
                    ];

                    $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
                    $messageId = $messageDB->addMessage($message);
                }
            }else{
                $chiefAdminsId = $familyDB->getFamilyChiefAdminId($person->familyId);
                $branchAdminsId = $familyDB->getFamilyBranchAdminId($person->familyId, $person->familyIndex, $person->level);
                $adminsId = array_merge($chiefAdminsId, $branchAdminsId);

                $messageDB = new CDBMessage();
                foreach ($adminsId as $adminId) {
                    $message = new Message();
                    $message->fromUser = $userId;
                    $message->toUser = $adminId['userId'];
                    $message->content = '用户解除绑定';
                    $message->module = $GLOBALS['FAMILY_MODULE'];
                    $message->action = $GLOBALS['REPLY_UNBIND_TO_ADMIN'];
                    $message->recordId = $person->familyId;
                    $message->messageCode = $messageCode;

                    $extData = [
                        "personId" => $personId,
                        "Name" => $person->name,
                        "level" => $person->level,
                        "familyName" => $family->name
                    ];

                    $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
                    $messageId = $messageDB->addMessage($message);
                }
            }


            $data['accept'] = $operation;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else {
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "取消绑定失败");
        }
    }else{

        $beCanceledUserPermission = $familyDB->getUserPermission($person->userId,$person->familyId,true);
        $beCanceledUserPermissionNum = Util::checkPermission($beCanceledUserPermission,[$person->familyIndex]);
        //检查操作的权限
        $userPermission = $familyDB->getUserPermission($userId,$person->familyId,true);
        $userPermissionNum = Util::checkPermission($userPermission,[$person->familyIndex]);
        if ($userPermissionNum < 3 || $userPermissionNum <= $beCanceledUserPermissionNum){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
        $operation = $personDB->cancelUserBindV3($personId, $person->familyId, $person->userId,true,
            $beCanceledUserPermissionNum == 2 ? 100:null);

        if ($operation){
            # 发送消息通知绑定的用户
            $messageDB = new CDBMessage();
            $message = new Message();
            $message->fromUser = $userId;
            $message->toUser = $person->userId;
            $message->content = '管理员解绑用户';
            $message->module = $GLOBALS['FAMILY_MODULE'];
            $message->action = $GLOBALS['REPLY_UNBIND_TO_USER'];
            $message->recordId = $person->familyId;
            $message->messageCode = $messageCode;

            $extData = [
                "personId" => $personId,
                "Name" => $person->name,
                "level" => $person->level,
                "familyName"=>$family->name
            ];

            $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);  // 调用JSON_UNESCAPED_UNICODE防止中文被编码
            $messageId = $messageDB->addMessage($message);
            $data['accept'] = $operation;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }else {
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "取消绑定失败");
        }

    }

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
