<?php

/**
 * 编辑人物的详情
 * author:liuzhenhao
 * date:2020-11-25
 *
 */

use DB\CDBArea;
use DB\CDBPerson;
use DB\CDBFamily;

//use DB\CDBPushPerson;
use Model\Person;
use Util\Check;
use Util\Util;
use DB\CDBFamilyNews;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''), 'personId');
$occupation = Check::checkInteger(trim(isset($params['occupation']) ? $params['occupation'] :null), 'occupation', true, true);
$education = Check::checkInteger(trim(isset($params['education']) ? $params['education'] : null), 'education', true, true);
$politicalStatus = Check::checkInteger(trim(isset($params['politicalStatus']) ? $params['politicalStatus'] : null), 'politicalStatus', true, true);
$birthProvince = Check::checkInteger(trim(isset($params['birthProvince']) ? trim($params['birthProvince']) : null), 'birthProvince', true, true); //省
$birthCity = Check::checkInteger(trim(isset($params['birthCity']) ? trim($params['birthCity']) : null), 'birthCity', true, true);
$birthArea = Check::checkInteger(trim(isset($params['birthArea']) ? trim($params['birthArea']) : null), 'birthArea', true, true);
$burialProvince = Check::checkInteger(trim(isset($params['burialProvince']) ? trim($params['burialProvince']) : null), 'burialProvince', true, true); //省
$burialCity = Check::checkInteger(trim(isset($params['burialCity']) ? trim($params['burialCity']) : null), 'burialCity', true, true);
$burialArea = Check::checkInteger(trim(isset($params['burialArea']) ? trim($params['burialArea']) : null), 'burialArea', true, true);
$trashField = Check::checkInteger(trim(isset($params['trashField']) ? trim($params['trashField']) : null), 'trashField', true, true);

try {

    if ($personId !== "") {
        $personDB = new CDBPerson();
        $familyDB = new CDBFamily();
        $userId = $GLOBALS['userId'];
        $person = $personDB->getPersonSimpleInfoById($personId);

        if (!$person) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物不存在");
            exit;
        }

        //检查操作的权限
        $permission = $familyDB->getUserPermission($userId, $person->familyId, true);
        if (Util::checkPermission($permission, [$person->familyIndex]) < 3 && $userId != $person->userId) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }


        $CDBArea = new CDBArea();
        $birthProvinceName = $birthProvince ? $CDBArea->getAreaNameById($birthProvince)['areaName'] : null;
        $birthCityName = $birthCity ? $CDBArea->getAreaNameById($birthCity)['areaName'] : null;
        $birthAreaName = $birthArea ? $CDBArea->getAreaNameById($birthArea)['areaName'] : null;
        $burialProvinceName = $burialProvince ? $CDBArea->getAreaNameById($burialProvince)['areaName'] : null;
        $burialCityName = $burialCity ? $CDBArea->getAreaNameById($burialCity)['areaName'] : null;
        $burialAreaName = $burialArea ? $CDBArea->getAreaNameById($burialArea)['areaName'] : null;


        $otherInfoData = [
            'occupation' => $occupation,
            'education' => $education,
            'political_status' => $politicalStatus,
            'birth_province' => $birthProvince,
            'birth_province_name' => $birthProvinceName,
            'birth_city' => $birthCity,
            'birth_city_name' => $birthCityName,
            'birth_area' => $birthArea,
            'birth_area_name' => $birthAreaName,
            'burial_province' => $burialProvince,
            'burial_province_name' => $burialProvinceName,
            'burial_city' => $burialCity,
            'burial_city_name' => $burialCityName,
            'burial_area' => $burialArea,
            'burial_area_name' => $burialAreaName,
            'trash_field' => $trashField,
        ];

        foreach ($otherInfoData as $k => $v) {
            if ($v === null) {
                unset($otherInfoData[$k]);
            }
        }

        if (!$otherInfoData) {
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "未修改");
            exit;
        }

        $personOtherInfo = $personDB->getPersonOtherInfo($personId);
        $count = $personOtherInfo ? $personDB->createOrUpdatePersonOtherInfo($personId, $userId, $otherInfoData, true) : $personDB->createOrUpdatePersonOtherInfo($personId, $userId, $otherInfoData, false);

        $data['editPerson'] = $count;

        //前台需要判断是否有修改

        if ($count >= 0) {

            $familyNewsDB = new CDBFamilyNews();
            $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['EDITPERSON'], $userId, $personId, $person->name, $userId);


            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "无变化");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
