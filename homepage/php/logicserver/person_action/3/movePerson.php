<?php
/**
 * 移动家族内的分支
 * @author jiangpengfei
 * @date 2018-08-07
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Model\Person;
use Util\Check;

$personId = Check::checkInteger($params['personId'] ?? '');
$toPersonId = Check::checkInteger($params['toPersonId'] ?? '');
$userId = $GLOBALS['userId'];
try {

    if ($personId == $toPersonId) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "不能是同一个人物");
        exit;
    }

    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();

    $person = $personDB->getPersonObjectById($personId);
    $toPerson = $personDB->getPersonObjectById($toPersonId);

    if ($toPerson->type == 2) { // 不是本家的不允许迁移
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "不允许迁移到配偶下");
        exit;
    }

    // 检查人物是否存在，并且是否是同一个家族
    if ($person == null || $toPerson == null || $person->familyId != $toPerson->familyId) {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "权限不足");
        exit;
    }

    if ($person->isAdoption == 1 || $toPerson->isAdoption == 1 || $person->bicolor > 1 || $toPerson->bicolor >1) { // 不能迁移到假人下
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "该人物存在特殊关联,不能直接迁移");
        exit;
    }

    // 检查用户对当前家族的权限
    if (Util::checkPermission($familyDB->getUserPermission($userId,$person->familyId),[$person->familyIndex,$toPerson->familyIndex]) < 3)
    {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "权限不足");
        exit;
    }

    if (intval($toPersonId) === intval($person->father[0])) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "迁移未变更");
        exit;
    }

    // 权限检查结束，开始移动
    if ($personDB->movePersonV33($personId, $toPersonId) > 0) {
        // 成功
        $data['move'] = true;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        // 失败
        Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "移动失败，请检查移动的人物是否属于同一分支");
    }

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}