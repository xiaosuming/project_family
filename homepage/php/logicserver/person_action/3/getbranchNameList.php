<?php
ini_set('memory_limit', '512M');
/**
 *  获取当前人物下的关联人物和关联家族信息
 * @author yuanxin
 * @version 3
 * @date 2020/09/15
 *
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

// 输入验证
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));                //必须
//$personId = Check::checkInteger(trim($params['personId'] ?? 0));



$userId = $GLOBALS['userId'];

try {
    if ($familyId != "") {
        $familyDB = new CDBFamily();
        $family = $familyDB->getFamilyById($familyId);
        $userPermissionData = $familyDB->getUserPermission($userId,$familyId);

        if ($family->openType == 0){
            if($userPermissionData  == null){
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '权限错误');
                exit;
            }
        }
   
        $personDB = new CDBPerson();


        $data = $personDB->getBranchNamesList($familyId);
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

        
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
