<?php
ini_set('memory_limit', '512M');
/**
 *  @author yuanxin
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

//queryFamilyPerson($familyId)
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));                //必须
// $personId = Check::checkInteger(trim($params['personId'] ?? 0));
// $isBranch = Check::checkInteger(trim($params['isBranch'] ?? 0));

try{
$personDB = new CDBPerson();
$familyDB = new CDBFamily();
$result  = $personDB->getFamilyPersons($familyId);
$refFamilys = array();
$refPersons = array();
$refStartPersonId = array();
// 拿出所有reffamily和refPersons
foreach($result as $person){
    if($person['refFamilyId']!=''  && $person['refFamilyId'] != null){
        $temp  =  preg_replace("/\[|]/", "" ,$person['refFamilyId']);
        $temp2  =  preg_replace("/\[|]/", "" ,$person['refPersonId']);
        array_push($refFamilys,$temp);
        array_push($refPersons,$person['id']);
        array_push($refStartPersonId,$temp2);
        // array_push($refFamilys,$person['refFamilyId']);
    }
}
$familys= $familyDB->getFamilyByIds($refFamilys);
// var_dump($familyName);
$familyName = array();
foreach($familys as $family){
    array_push($familyName,$family['name']);
}



$data['refFamilys'] = $refFamilys;
$data['refPersons'] = $refPersons;
$data['refStartPersonId'] = $refStartPersonId;
$data['label'] = $familyName;
// $data =  $personDB->getMultiFamilyPersons($familyId,$refFamilys,$refStartPersonId,$refPersons);


Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}


?>