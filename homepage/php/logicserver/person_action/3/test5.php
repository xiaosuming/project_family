<?php
/**
 * 
 * 更新大量数据的脚本
 * @author yuanxin
 * 
 */
use DB\CDBPerson;
use DB\CDBManager;
use Util\Util;
use Model\Person;
use Util\RelationParser;
// use PDO;
require('../../../db/db_person_class.php');
require('../../../config/db_config.izuqun.php');
require('../../../db/db_family_class.php');
require('../../../db/db_mysql_class.php');
require('../../../util/HttpContext.php');
require('../../../util/Util.class.php');
require('../../../util/RelationParser.class.php');
require('../../../model/Person.php');
//$servername = "localhost";
//$username = "debian-sys-maint";
//$dbname = "family";
//$password = "4GV7wJSbQ6PCIt69";
$servername = "47.111.172.152";
$username = "family";
//$username = 'root';
$dbname = "family";
//$password = '666';
$password = "Flare1111";
$pdo = null;
try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	// $pdo = new PDO($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd']);
	echo "连接成功";
}
catch(PDOException $e) {
	echo $e->getMessage();
	exit;
}
 $familyId = 1124;
// $personId = 3394;
$GLOBALS['index'] = 0;
// $familyId = 709; 
// $personId = 6673497142557934593; 
$TABLE = "gener_family";
// $personDB = new CDBPerson();
//  $persons = $personDB->getFamilyPersons($familyId, null, $personId);
$sql = "select id from $TABLE where is_delete = 0";
$result = $conn->query($sql);
var_dump($result);
$familys = $result->fetchAll(PDO::FETCH_ASSOC );
$familysId = array_map(function ($v){
    return $v['id'];
},$familys);
var_dump($familysId);
foreach ([$familysId] as $fid){
    $GLOBALS['index'] = 0;
    print_r($fid);
    $sql = "SELECT id,same_personId,ranking, familyId,type,is_delete,name,father,mother,son,daughter,spouse,sister,brother,level,family_index FROM gener_person
                     WHERE familyId = $fid  AND is_delete = 0  order by level asc , 
                     type asc";
    $result = $conn->query($sql);
    $result =$result->fetchAll();
    for ($i = 0; $i < count($result); $i++) {
        $person = new Person($result[$i]);
        //转换为person对象
        $parser = new RelationParser($person);
        $result[$i]['father'] = $parser->getFathers();
        $result[$i]['mother'] = $parser->getMothers();
        $result[$i]['son'] = $parser->getSons();
        $result[$i]['daughter'] = $parser->getDaughters();
        $result[$i]['spouse'] = $parser->getSpouses();
        $result[$i]['brother'] = $parser->getBrothers();
        $result[$i]['sister'] = $parser->getSisters();
    }
    $persons = $result;
    $stack = array();
    foreach($persons as $key => $person) {
        if($person['type'] == 1 ) {
            $currentPerson = $person;
            array_push($stack,$currentPerson);
            $id = $person['id'];
            $index = $GLOBALS['index'];
            $sql  = "update gener_person set family_index = $index  where id = $id;";
            $GLOBALS['index']+=1;
            $s= $conn->exec($sql);
            var_dump($GLOBALS['index']);
            unset($persons[$key]);
            break;
        }
    }
// 有配偶的情况下
    while($currentPerson['spouse'] != null ) {
        $spouse = array_shift($currentPerson['spouse']);
        $stack = getSpouses($persons,$spouse,$stack,$conn);
    }
// 有孩子的情况下
    if($currentPerson['son'] != null || ($currentPerson['daughter'] != null)) {
        $children  =  array_merge($currentPerson['son'],$currentPerson['daughter']);
        $children = getChildrenRank($persons,$children);
        while($children != null) {
            $child = array_shift($children);
            $stack = getChildren($persons,$child,$stack,$conn);
        }
    }
}


/**
    * 获取所有配偶，存入结果
    */
function getSpouses($persons,$spouse,$stack,$conn) {
	foreach($persons as $key  => $person) {
		if($person['id'] == $spouse) {
			array_push($stack,$person);
			$id = $person['id'];
			$index = $GLOBALS['index'];
			$sql  = "update gener_person set family_index = $index  where id = $id;";
			$GLOBALS['index']+=1;
			$s= $conn->exec($sql);
			var_dump($s);
			var_dump($GLOBALS['index']);
			unset($persons[$key]);
			break;
		}
	}
	return $stack;
}
/**
* 获取所有孩子
*/
function getChildren($persons,$child,$stack,$conn) {
	foreach($persons as $key  => $person) {
		if($person['id'] == $child) {
			array_push($stack,$person);
			$id = $person['id'];
			$index = $GLOBALS['index'];
			$sql  = "update gener_person set family_index = $index  where id = $id;";
			$GLOBALS['index']+=1;
			$s= $conn->exec($sql);
			var_dump($s);
			var_dump($GLOBALS['index']);
			unset($persons[$key]);
			$spouses= $person['spouse'];
			if($spouses != null) {
				while($spouses !=null) {
					$spouse = array_shift($spouses);
					$stack = getSpouses($persons,$spouse,$stack,$conn);
				}
			}
			if($person['son'] != null || ($person['daughter'] != null)) {
				$children  =  array_merge($person['son'],$person['daughter']);
				$children = getChildrenRank($persons,$children);
				while($children != null) {
					$child = array_shift($children);
					$stack = getChildren($persons,$child,$stack,$conn);
				}
			}
			break;
		}
	}
	return $stack;
}
/**
 *   获取孩子排行
 *  
 */
function getChildrenRank($persons,$children) {
	if(count($children) > 1) {
		$ranking = array();
		$newChildren = array();
		$n = 0;
		foreach($persons as $key => $person) {
			if(in_array($person['id'],$children)) {
				$ranking[$key] = $person['ranking'];
				$newChildren[$n++] = $person['id'];
				if($n == count($children)) {
					break;
				}
			}
		}
		array_multisort($ranking, SORT_ASC, $newChildren);
		return  $newChildren;
	} else {
		return $children;
	}
}
?>
