<?php
/**
 * 编辑人物的详情
 * author:liuzhenhao
 * date:2020-07-27
 *
 */

use DB\CDBArea;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBFamily;
use DB\CDBPushPerson;
use Model\Person;
use Util\Check;
use Util\Util;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''),'personId');
$name = Check::check(isset($params['name']) ? trim($params['name']) : null,1,30);
$birthday = Check::check(isset($params['birthday']) ? trim($params['birthday']) : null);
$maritalStatus = Check::checkInteger(isset($params['maritalStatus']) ? trim($params['maritalStatus']) : '','maritalStatus',true,true);        //不传则是未婚
$bloodType = Check::checkInteger(isset($params['bloodType']) ? trim($params['bloodType']) : '','bloodType',true,true);                    //不传则是0
$qq = Check::check(isset($params['qq']) ? trim($params['qq']) : null);
$country = Check::checkInteger(trim(isset($params['country']) ? trim($params['country']) : ''),'country',true,true); //国家
$province = Check::checkInteger(trim(isset($params['province']) ? trim($params['province']) : ''),'province',true,true); //省
$city = Check::checkInteger(trim(isset($params['city']) ? trim($params['city']) : ''),'city',true,true);
$area = Check::checkInteger(trim(isset($params['area']) ? trim($params['area']) : ''),'area',true,true);
$town = Check::checkInteger(trim(isset($params['town']) ? trim($params['town']) : ''),'town',true,true);
$address = Check::check(isset($params['address']) ? trim($params['address']) : null);
$phone = isset($params['phone']) ? trim($params['phone']) : null;
$zi = Check::check(isset($params['zi']) ? trim($params['zi']) : null,0,10);
$zpname = Check::check(isset($params['zpname']) ? trim($params['zpname']) : null, 0, 10);
$remark = Check::check(isset($params['remark']) ? trim($params['remark']) : null,0,255);
$isDead = Check::checkInteger(trim(isset($params['isDead']) ? $params['isDead'] : ''),'isDead',true,true); //默认为0 0表示未去世
$deadTime = isset($params['deadTime']) ? trim($params['deadTime']) : null;
//$ranking = $params['ranking'] ?? 0;
$sideText = Check::check(isset($params['sideText']) ? trim($params['sideText']):null);
$profileText = Check::check(isset($params['profileText']) ? $params['profileText']:null);
$burialPlace = Check::check(isset($params['burialPlace']) ? trim($params['burialPlace']):null);
$birthPlace = Check::check(isset($params['birthPlace']) ? trim($params['birthPlace']):null);
$realMother = Check::check(isset($params['realMother']) ? trim($params['realMother']):null);
//$isNotMain = Check::checkInteger(trim(isset($params['isNotMain']) ? trim($params['isNotMain']) : ''),'isNotMain',true,true);

if ($phone != '') {
    Check::checkPhoneNumber($phone);
}

try {

    if ($personId !== "" && $name !== "") {
        $personDB = new CDBPerson();
        $familyDB = new CDBFamily();
        $userId = $GLOBALS['userId'];
        $person = $personDB->getPersonSimpleInfoById($personId);

        if (!$person){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物不存在");
            exit;
        }

        //检查操作的权限
        $permission = $familyDB->getUserPermission($userId,$person->familyId,true);
        if (Util::checkPermission($permission, [$person->familyIndex]) < 3 && $userId != $person->userId) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }


        if ($birthday!=null  && $deadTime !=''){
            if ($birthday >= $deadTime) {
                Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "出生时间不能大于去世时间");
                exit;
            }
        }

        $CDBArea = new CDBArea();
        $countryName = $country ? $CDBArea->getAreaNameById($country)['areaName']:null;
        $provinceName = $province ? $CDBArea->getAreaNameById($province)['areaName']:null;
        $cityName = $city ? $CDBArea->getAreaNameById($city)['areaName']:null;
        $areaName = $area ? $CDBArea->getAreaNameById($area)['areaName']:null;
        $townName = $town ? $CDBArea->getAreaNameById($town)['areaName']:null;

        $postData = [
            'name'=>$name,
            'birthday'=>$birthday,
            'marital_status'=>$maritalStatus,
            'blood_type'=>$bloodType,
            'qq'=>$qq,
            'country'=>$country,
            'province'=>$province,
            'city'=>$city,
            'area'=>$area,
            'town'=>$town,
            'address'=>$address,
            'phone'=>$phone,
            'zi'=>$zi,
            'zpname'=>$zpname,
            'remark'=>$remark,
            'is_dead'=>$isDead,
            'dead_time'=>$deadTime,
            'sideText'=>$sideText,
            'burialPlace'=>$burialPlace,
            'birthPlace'=>$birthPlace,
            'country_name'=>$countryName,
            'province_name'=>$provinceName,
            'city_name'=>$cityName,
            'area_name'=>$areaName,
            'town_name'=>$townName,
            'realMother'=>$realMother,
            'profileText'=>$profileText
//            'isNotMain'=>$isNotMain
        ];

        $updateData = [];
        foreach ($postData as $k =>$v){
            if (in_array($k,['birthday', 'dead_time'])){
                if ($v === ''){
                    $updateData[$k] = null;
                    continue;
                }elseif($v === null){
                    continue;
                }
                $updateData[$k] = $v;
                continue;
            }
            if ($v !== null){
                $updateData[$k] = $v;
            }
        }

        if ($updateData['is_dead'] == 1){
            $updateData['userId'] = 0;
        }


        if (!$updateData){
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "未修改");
            exit;
        }

        $count = $personDB->editPersonDetailV3($updateData, $personId, $userId);

        $data['editPerson'] = $count;

        //前台需要判断是否有修改

        if ($count >= 0) {
            $person = $personDB->getPersonById($personId);

            $familyNewsDB = new CDBFamilyNews();
            $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['EDITPERSON'], $userId, $personId, $person->name, $userId);

            if ($GLOBALS['TEST_PERSON_TASK']) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 2;
                $source = $person;
                $CDBPushPerson->setEditPushPersonTask($modelId, $action, $source);
            }

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "无变化");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
