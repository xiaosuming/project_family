<?php
    /**
    * 添加相关人物
    * 依据版本三的逻辑，移除了个人信息等不必要的参数。
    *  @author  yuanxin
    *  @version 3
    *  @date 2020/08/24
    * 
    *
    */
    use DB\CDBPerson;
    use Model\Person;
    use DB\CDBFamily;
    use Util\Util;
    use Util\Check;



    $familyId = Check::check(trim(isset($params['familyId'])?$params['familyId']:0));
    $corePersonId = trim(isset($params['corePersonId']) ? $params['corePersonId'] : '');
    try{
        if( ''== $corePersonId){
		    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
		    exit;
	    }
        $familyDB = new CDBFamily();
        $familyDetail = $familyDB->getFamilyById($familyId);
        
		// if ($familyDetail->groupType != 1) {
		// 	Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
		// 	exit;
        // }

        $userId = $GLOBALS['userId'];
        $personDB =  new  CDBPerson();
        $person = $personDB->getPersonById2($corePersonId);
        if(is_null($person)){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "查无此人");
		    exit;
        }
        if($person->daughter == null  && $person->son == null  && $person->spouse == null){
            $gender = $person->gender == 1?0:1;
            $father =$person->father;
            $mother = $person->mother;
            $brother = $person->brother;
            $sister = $person->sister;
            /*********************************修改姐妹************************** */
            if(array() != $sister){
                foreach($sister as $person){
                    $personSister= new Person();
                    $personSister = $personDB->getPersonById2($person);
                    if($gender == 0){
                        foreach($personSister->brother as $key =>  $data){
                            if($data == $corePersonId){
                                unset($personSister->brother[$key]);
                            break;
                            }
                        }
                        array_push($personSister->sister,$corePersonId);
                    }else{
                        foreach($personSister->sister as $key =>  $sister){
                            if($sister == $corePersonId){
                                unset($personSister->sister[$key]);
                            break;
                            }
                        }
                        array_push($personSister->brother,$corePersonId);
                    }
                    $personDB->modifyPersonRelation($personSister);
                }
            }
          /*********************************修改兄弟************************** */
          if(array() != $brother){
            foreach($brother as $person){
                $personBrother= new Person();
                $personBrother = $personDB->getPersonById2($person);
                if($gender == 0){
                    foreach($personBrother->brother as $key => $data){
                        if($data == $corePersonId){
                        unset($personBrother->brother[$key]);
                        }
                    }
                    array_push($personBrother->sister,$corePersonId);

                }else{
                    foreach($personBrother->sister as $key =>  $sister){
                        if($sister == $corePersonId){
                            unset($personBrother->sister[$key]);
                        break;
                        }
                    }
                    array_push($personBrother->brother,$corePersonId);
                }

                $personDB->modifyPersonRelation($personBrother);
              }
            }
            /*********************************修改父亲************************** */
            if(array() != $father){
                foreach($father as $person){
                    $personFather= new Person();
                    $personFather = $personDB->getPersonById2($person);
                    if($gender == 0){
                        foreach($personFather->son as $key =>  $son){
                            if($son == $corePersonId){
                                unset($personFather->son[$key]);
                            break;
                            }
                        }
                        array_push($personFather->daughter,$corePersonId);
                    }else{
                        foreach($personFather->daughter as $key =>  $daughter){
                            if($daughter == $corePersonId){
                                unset($personFather->daughter[$key]);
                            break;
                            }
                        }
                        array_push($personFather->son,$corePersonId);
                    }
                    $personDB->modifyPersonRelation($personFather);
                }
            }
            /*********************************修改母亲************************** */
            if(array() != $mother){
                foreach($mother as $person){
                    $personMother= new Person();
                    $personMother = $personDB->getPersonById2($person);
                    if($gender == 0){
                        foreach($personMother->son as $key =>  $son){
                            if($son == $corePersonId){
                                unset($personMother->son[$key]);
                            break;
                            }
                        }
                        array_push($personMother->daughter,$corePersonId);
                    }else{
                        foreach($personMother->daughter as $key =>  $daughter){
                            if($daughter == $corePersonId){
                                unset($personMother->daughter[$key]);
                            break;
                            }
                        }
                        array_push($personMother->son,$corePersonId);
                    }
                    $personDB->modifyPersonRelation($personMother);
                }
            }
            $updateRow = $personDB->changePersonGender($corePersonId,$familyId,$gender);
			Util::printResult($GLOBALS['ERROR_SUCCESS'], "修改成功");
        }else{
            Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "该人物必须没有配偶，没有子女");
			exit;
        }
    } catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}



 ?>