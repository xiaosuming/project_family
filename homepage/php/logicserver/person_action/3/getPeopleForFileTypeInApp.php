<?php

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''), 'familyId'); //必须
$showMain = Check::checkInteger(trim(isset($params['showMain']) ? $params['showMain'] : 1), 'showMain');
//$level = Check::checkInteger(trim(isset($params['level']) ? $params['level'] : null), 'level',true, true); //必须
//$showDaughter = Check::checkInteger(trim(isset($params['showDaughter']) ? $params['showDaughter'] : 1), 'showDaughter');
//$showSpouse = Check::checkInteger(trim(isset($params['showSpouse']) ? $params['showSpouse'] : 1), 'showSpouse');
//$lastFamilyIndex = Check::checkInteger(trim(isset($params['lastFamilyIndex']) ? $params['lastFamilyIndex'] : -1));
//$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
//$page = Check::checkInteger(trim(isset($params['page']) ? $params['page'] : 1));
//$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 6));


//if ($pageSize > 99) {
//    $pageSize = 99;
//}
//
//$filterParams = array_filter([
//    'isNotMain' => $showDaughter == 1 ? null : 0,
//    'type' => $showSpouse == 1 ? null: 1,
//    'name' => $name,
//    'level' => $level,
//], function ($v) {
//    if ($v === '' || $v === null) {
//        return false;
//    }
//    return true;
//});

try {
    $userId = $GLOBALS['userId'];

    $familyDB = new CDBFamily();

    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($family->openType == 0) {
        //检查查询的权限
        if (!$familyDB->getUserPermission($userId, $familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    $personDB = new CDBPerson();

    $data = $personDB->getPeopleForFileTypeInApp($familyId, $showMain);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

