<?php
use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''),'familyId'); //必须
$level = Check::checkInteger(trim(isset($params['level']) ? $params['level'] : ''),'level'); //必须
$gender = Check::check(trim(isset($params['gender']) ? $params['gender'] : ''));
$isAdoption = Check::check(trim(isset($params['isAdoption']) ? $params['isAdoption'] : ''));
$isDead = Check::check(trim(isset($params['isDead']) ? $params['isDead'] : ''));
$maritalStatus = Check::check(trim(isset($params['maritalStatus']) ? $params['maritalStatus'] : ''));
$isBind = Check::check(trim(isset($params['isBind']) ? $params['isBind'] : ''));
$permission = Check::check(trim(isset($params['permission']) ? $params['permission'] : ''));
$isNotMain = Check::check(trim(isset($params['isNotMain']) ? $params['isNotMain'] : ''));
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
$bicolor = Check::check(trim(isset($params['bicolor']) ? $params['bicolor'] : ''));
$page = Check::checkInteger(trim(isset($params['page']) ? $params['page'] : 1));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 20));


if ($pageSize > 99){
    $pageSize = 99;
}

$filter_params = array_filter([
    'gender'=>$gender,
    'is_adoption'=>$isAdoption,
    'is_dead'=>$isDead,
    'marital_status'=>$maritalStatus,
    'permissionType'=>$permission,
    'type'=>$type,
    'isNotMain'=>$isNotMain == -1 ? null:$isNotMain,
    'name'=>$name,
    'isBind'=>$isBind,
    'bicolor'=>$bicolor
],function ($v){
    if ($v === '' || $v === null){
        return false;
    }
    return true;
});

try {
    $userId = $GLOBALS['userId'];

    $familyDB = new CDBFamily();

    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($family->openType == 0) {
        //检查查询的权限
        if (!$familyDB->getUserPermission($userId, $familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    $personDB = new CDBPerson();

    $data = $personDB->getFamilyPersonListByLevelV3($familyId, $level,$page,$pageSize ,$filter_params);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

