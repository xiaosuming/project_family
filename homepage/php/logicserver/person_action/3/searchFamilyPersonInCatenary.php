<?php
use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''),'familyId'); //必须
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
$page = Check::checkInteger(trim(isset($params['page']) ? $params['page'] : 1));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 20));


if ($pageSize > 99){
    $pageSize = 99;
}

$filter_params = array_filter([
    'name'=>$name,
],function ($v){
    if ($v === '' || $v === null){
        return false;
    }
    return true;
});

try {
    $userId = $GLOBALS['userId'];

    $familyDB = new CDBFamily();

    $family = $familyDB->getFamilyById($familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($family->openType == 0) {
        //检查查询的权限
        if (!$familyDB->getUserPermission($userId, $familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    $personDB = new CDBPerson();

    $data = $personDB->searchPeopleByNameInCatenary($familyId, $filter_params, $page, $pageSize);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

