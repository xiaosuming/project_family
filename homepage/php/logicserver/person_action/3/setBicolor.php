<?php
/**
 * 设置祧子
 * @author: yuanxin
 * @date 2020/08/26
 * 
 *
 */
use DB\CDBPerson;
use Model\Person;
use DB\CDBFamily;
use Util\Util;
use Util\Check;

$familyId = Check::check(trim(isset($params['familyId'])?$params['familyId']:0));
// 承祧   
$personId  = Check::checkInteger(trim($params['personId'] ?? 0));
// 假人选中的父亲们
$fatherList = trim($params['fatherList'] ?? '');
//生物学父亲id
$biologicalFatherId = Check::checkInteger(trim($params['biologicalFatherId'] ?? 0));
$fathers = json_decode($fatherList,true);   


try{
    if( 0  ==  $personId  ||  '' == $fatherList || 0 == $biologicalFatherId ||  count($fathers) == 0  ){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
    $familyDB = new CDBFamily();
    $familyDetail = $familyDB->getFamilyById($familyId);
    $userId = $GLOBALS['userId'];
    // 权限操作

    if ($familyDetail->openType == 0) {
        //检查查询的权限
        if (!$familyDB->getUserPermission($userId,$familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    if(in_array($biologicalFatherId,$fathers)){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "人物不可即是生父，又是被兼祧者");
        exit;
    }


    $personDB = new CDBPerson();
    $person =  $personDB->getPersonById2($personId);
    if($person->bicolor > 0){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "人物已承祧，请取消承祧状态重新关联");
            exit;
    }


    $personDB->addBicolorRelate($personId,$fathers ,$familyId,$biologicalFatherId);

     
   
   Util::printResult($GLOBALS['ERROR_SUCCESS'], "添加成功");

} catch (PDOException $e) {
    //异常处理
//    print_r($e);

    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}



 ?>
