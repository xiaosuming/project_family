<?php
ini_set('memory_limit', '512M');
/**
 *  极简模式的吊线图导出
 * @author yuanxin
 * @version 3
 * @date 2020/8/10
 * 
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

// 输入验证
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));                //必须
$personId = Check::checkInteger(trim($params['personId'] ?? 0));
$showType = Check::checkInteger(trim($params['showType'] ?? 1)); //显示方向  1左起 -1右起
$filter  =  Check::checkInteger(trim($params['filter'] ?? 0)); //是否过滤配偶 0显示 1不显示
$userFamilyId = null;
$userId = $GLOBALS['userId'];

try {
    if ($familyId != "") {
        $familyDB = new CDBFamily();
        $family = $familyDB->getFamilyById($familyId);
        if ($family == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "没有这个家族");
            exit;
        }
       
        $personDB = new CDBPerson();
        // 获取所有的家族成员
        $filter =1 ;
        
        $persons = $personDB->getFamilyPersons3($familyId, null, $personId, $filter);

        if($persons != null){
            $personsCount = count($persons);
        $brotherX = 306;
        // 配偶之间偏移量
        $spouseX = 153;
        $fatherY = 80;
        // 当前层最大坐标
        $biggestX = 0;
        $rootY = $persons[0]['level'];
        $persons[0]['y']  = 0;
        $persons[0]['x']  = 0;
        $personsStack = array();
        $personsStack[0] = array();
        array_push($personsStack[0],0);


        for($i = 1 ; $i<$personsCount   ;$i++){
                // 向下添加表示有孩子,向右添加表示没有孩子了有兄弟或者表兄弟,分支录完,向上添加表示这一分支录完;
                $preLevel = $persons[$i - 1]['level'];
                $level = $persons[$i]['level'];
                $stackLevel = $level -$rootY;
                $persons[$i ]['y'] = ($level  - $rootY)*  $fatherY; 
                if($persons[$i]['level'] > $preLevel){
                        $personsStack[$stackLevel]   =  array();
                        $persons[$i]['x']  = $persons[$i - 1 ]['x'];
                        array_push($personsStack[$stackLevel],$i);
                }elseif($level == $preLevel){
                        array_push($personsStack[$stackLevel],$i);
                        $persons[$i]['x'] = $biggestX + $brotherX;
                        $biggestX += $brotherX;  
                }else{
                        $persons[$i]['x'] = $biggestX + $brotherX;
                        $biggestX += $brotherX;  
                        while($level < $preLevel){
                            $preStackLevel = $preLevel - $rootY;
                            $nextStackLevel = $preStackLevel  - 1 ;
                            if(count($personsStack[$preStackLevel])%2 > 0 ){
                                $persons[$personsStack[$nextStackLevel][count($personsStack[$nextStackLevel]) - 1]]['x'] =  
                                ($persons[$personsStack[$preStackLevel][floor(count($personsStack[$preStackLevel])/2)]]['x']);
                            }else{
                                $persons[$personsStack[$nextStackLevel][count($personsStack[$nextStackLevel]) - 1]]['x'] =  
                                ($persons[$personsStack[$preStackLevel][0]]['x']  + $persons[$personsStack[$preStackLevel][count($personsStack[$preStackLevel]) - 1 ]]['x']) / 2;
                            }              
                            $preLevel -- ;
                        }
                        array_push($personsStack[$stackLevel],$i);
                }
            }
            for($j = count($personsStack)-2 ; $j>=0  ; $j--){
                if(count($personsStack[$j+1])%2 > 0 ){
                    $persons[$personsStack[$j][count($personsStack[$j]) - 1]]['x'] =  
                    ($persons[$personsStack[$j+1][floor(count($personsStack[$j+1])/2)]]['x']);
                }else{
                    $persons[$personsStack[$j][count($personsStack[$j]) - 1]]['x'] =  
                    ($persons[$personsStack[$j+1][0]]['x']  + $persons[$personsStack[$j+1][count($personsStack[$j+1]) - 1 ]]['x']) / 2;
                } 
            }
         $data['persons'] =  $persons;
        }     

        if (count($persons) > 0) {
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "sql查询时发生错误");
        }
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
