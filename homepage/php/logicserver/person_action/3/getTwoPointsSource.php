<?php
/**
 * 两点溯源
 * @author: yuanxin
 * @date 2020/08/24
 * 
 *
 */

use DB\CDBPerson;
use Model\Person;
use DB\CDBFamily;
use Util\Util;
use Util\Check;

$familyId = Check::check(trim(isset($params['familyId'])?$params['familyId']:0));
//其实可以直接传familyIndex 减少两次查表
$personIdA = Check::checkInteger(trim($params['personIdA'] ?? 0));
$personIdB = Check::checkInteger(trim($params['personIdB'] ?? 0));

try{
    if( 0  ==  $personIdA ||  0 == $personIdB){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $familyDB = new CDBFamily();
    $familyDetail = $familyDB->getFamilyById($familyId);
    $userId = $GLOBALS['userId'];

    // 权限操作
    // if ($familyDetail->groupType != 1) {
    //     Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
    //     exit;
    // }
    $personDB =  new  CDBPerson();
    $personA = $personDB->getPersonById2($personIdA);
    $personB = $personDB->getPersonById2($personIdB);
    if("" == $personA || "" == $personB){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "查不到这个人");
        exit;
    }
    /*******************************调整AB大小，Aindex一定小于B************* */
    if($personA->familyIndex > $personB->familyIndex){
        $temp = new Person();
        $temp = $personA;
        $personA = $personB;
        $personB = $temp;
    }
    $sourcePersonId =  $personDB->getTwoPointsSource($personA->familyIndex,$personB->familyIndex,$familyId);
    if($personA->familyIndex == $sourcePersonId[0]['familyIndex']){
        // 判断A和B的关系
        $personALastIndex = $personDB->getLastPersonFamilyIndex($familyId,$personA->level,$personA->familyIndex);
        // 不包含B则寻找A父亲
        if($personALastIndex    <   $personB->familyIndex){
            if(array() ==  $personA->father) {
                $data['personId'] = $personA->mother[0];
            }else{
                $data['personId'] = $personA->father[0];
            }
        }else{
            $data['personId'] = $personA->id;
        }
    }elseif($personB->familyIndex == $sourcePersonId[0]['familyIndex']){
        //等于B不需要判断，b一定小于Alevel
        // $personBLastIndex = $personDB->getLastPersonFamilyIndex($familyId,$personB->level,$personB->familyIndex);
            if(array() ==  $personB->father) {
                $data['personId'] = $personB->mother[0];
            }else{
                $data['personId'] = $personB->father[0];
            }
    }else{
        
        $person = $personDB->getPersonByFamilyIndex($sourcePersonId[0]['familyIndex'],$familyId);
        if(array() == $person->father){
            $data['personId'] =  $person->mother[0];
        }else{
            $data['personId'] =  $person->father[0];
        }
    }
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}



 ?>