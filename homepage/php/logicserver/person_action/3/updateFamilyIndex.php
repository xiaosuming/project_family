<?php
    ini_set('memory_limit', '512M');
    /**
     *  更新一个家族的familyIndex
     * 
     *  先先序遍历
     *  然后更新数据库
     * @author yuanxin
     * @version 3
     */
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;

    //queryFamilyPerson($familyId)
    // 输入验证
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));				//必须
    $personId = Check::checkInteger(trim($params['personId'] ?? 0));
    $isBranch = Check::checkInteger(trim($params['isBranch'] ?? 0));

    $userFamilyId = null;
    if($isBranch == 1){
        $userFamilyId = Check::checkInteger(trim($params['userFamilyId'] ?? ''));
    }
    $userId = $GLOBALS['userId'];
    try{
        if($familyId!=""){
            $familyDB = new CDBFamily();

            // $family = $familyDB->getFamilyById($familyId);

            // if ($family == null) {
            //     Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            //     exit;
            // }

            // if ($family->openType == 0) {
            //     // 私有族群检查权限
                
            //     if($isBranch == 0){
            //         //检查操作的权限
            //         if(!$familyDB->isUserForFamily($familyId,$userId)&&!$familyDB->isAdminForFamily($familyId,$userId)) {
            //             Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            //             exit;
            //         }
            //     }else if($isBranch == 1){
            //         //检查通过分支请求的权限
            //         if(!$familyDB->verifyUserIdAndFamilyIdByBranch($userId,$familyId,$userFamilyId)){
            //             Util::printResult( $GLOBALS['ERROR_PERMISSION'], "分支权限错误");
            //             exit;
            //         }
            //     }else{
            //         Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "branch参数错误");
            //         exit;
            //     }
            // }



            $personDB = new CDBPerson();
            // 获取所有的家族成员
            $persons = $personDB->getFamilyPersons($familyId, null, $personId);
           
            
            if($persons != null ){
                $stack = array();
                foreach($persons as $key => $person){
                    if($person['type'] == 1 ){
                        $currentPerson = $person;
                        array_push($stack,$currentPerson);
                        unset($persons[$key]);
                       break;
                    }
                }    
                    // 有配偶的情况下
                   while($currentPerson['spouse'] != null ){
                       $spouse = array_shift($currentPerson['spouse']);
                       $stack = getSpouses($persons,$spouse,$stack);
                   }
                    // 有孩子的情况下
                    if($currentPerson['son'] != null || ($currentPerson['daughter'] != null))
                    {
                        
                        $children  =  array_merge($currentPerson['son'],$currentPerson['daughter']);
                        $children = getChildrenRank($persons,$children);
                        while($children != null){
                            $child = array_shift($children);
                            $stack = getChildren($persons,$child,$stack);
                        }
                    }
            }
            
            // 更新FamilyIndex
            foreach($stack as $key => $person){
                $result = $personDB->updateFamilyIndex($person['familyId'], $person['id'],$key);
            }

            // 添加最大的end
            // $result =  $personDB->insertDefaultLastIndex($familyId);

            
            // 将数据根据 level 降序排列，根据 type 升序排列 , ranking 升序
            // array_multisort($level, SORT_ASC, $type, SORT_ASC,$ranking, SORT_ASC, $persons);
            $data['persons'] = $result;
            

            if(count($persons) > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult( $GLOBALS['ERROR_SQL_QUERY'], "sql查询时发生错误");
            }
            
        }else{
            Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
//        print_r($e);
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }

   /**
    * 获取所有配偶，存入结果
    */
    function getSpouses($persons,$spouse,$stack){
            foreach($persons as $key  => $person){
             
                if($person['id'] == $spouse){
                      array_push($stack,$person);
                      unset($persons[$key]);
                break;
                }
            }
        return $stack;     
    }

    /**
    * 获取所有孩子
    */
    function getChildren($persons,$child,$stack){

            foreach($persons as $key  => $person){
                if($person['id'] == $child){
                      array_push($stack,$person);
                      unset($persons[$key]);
                      $spouses= $person['spouse'];
                      if($spouses != null)
                      {
                        while($spouses !=null){
                            $spouse = array_shift($spouses);
                            $stack = getSpouses($persons,$spouse,$stack);
                        }
                      }
                      if($person['son'] != null || ($person['daughter'] != null))
                      {
                          $children  =  array_merge($person['son'],$person['daughter']);
                         $children = getChildrenRank($persons,$children);
                          while($children != null){
                            $child = array_shift($children);
                            $stack = getChildren($persons,$child,$stack);
                         }
                      }
                break;
                }
            }

        return $stack;     
      
    }


    /**
     *   获取孩子排行
     *  
     */
    function getChildrenRank($persons,$children){
        if(count($children) > 1){
            $ranking = array();
            $newChildren = array();
            $n = 0;
        
            foreach($persons as $key => $person){
                if(in_array($person['id'],$children)){
                    $ranking[$key] = $person['ranking'];
                    $newChildren[$n++] = $person['id'];
                    if($n == count($children)){
                          break;
                    }
                }
           }
            array_multisort($ranking, SORT_ASC, $newChildren);
            return  $newChildren;
        }
        else {
            return $children;
        }
   

    }
