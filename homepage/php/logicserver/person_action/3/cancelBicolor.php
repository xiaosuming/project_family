<?php
/**
 * 取消祧子关系
 * @author: yuanxin
 * @date 2020/08/26
 * 
 *
 */
use DB\CDBPerson;
use Model\Person;
use DB\CDBFamily;
use Util\Util;
use Util\Check;

$familyId = Check::check(trim(isset($params['familyId'])?$params['familyId']:0));
// 承祧   
$personId  = Check::checkInteger(trim($params['personId'] ?? 0));




try{
    if( 0  ==  $personId  ){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
    $familyDB = new CDBFamily();
    $familyDetail = $familyDB->getFamilyById($familyId);
    $userId = $GLOBALS['userId'];
    // 权限操作
    if ($familyDetail->openType == 0) {
        //检查查询的权限
        if (!$familyDB->getUserPermission($userId,$familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }


    $personDB = new CDBPerson();
    $person =  $personDB->getPersonById2($personId);
    if($person->bicolor == 0){
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "人物没有承祧关系");
            exit;
    }

    $result = $personDB->cancelBicolor($personId);

    if($result){
        Util::printResult($GLOBALS['ERROR_SUCCESS'], "取消成功");
    }else{
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "取消失败");
    }

     
   

} catch (PDOException $e) {
    //异常处理
//    print_r($e);

    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}



 ?>
