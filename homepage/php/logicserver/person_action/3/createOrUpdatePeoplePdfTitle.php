<?php

/**
 * 用户申请加入家族
 * @author liuzhenhao
 * @date 2020-11-20
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use DB\CDBFamilyNews;


//$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''), 'familyId');
$title = Check::check(trim(isset($params['title']) ? $params['title'] : null), 0, 100);
$zan = Check::check(trim(isset($params['zan']) ? $params['zan'] : null), 0, 2000);
$branchName = Check::check(trim(isset($params['branchName']) ? trim($params['branchName']) : null), 0, 100);
$zanType = Check::check(trim(isset($params['zanType']) ? $params['zanType'] : null), 0, 100);
$originName = Check::check(trim(isset($params['originName']) ? $params['originName'] : null), 0, 5);
$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''), 'personId');
$userId = $GLOBALS['userId'];


try {
    $familyDB = new CDBFamily();
    $personDB = new CDBPerson();


    $person = $personDB->getPersonV3($personId);
    if (!$person) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '人物不存在');
        exit;
    }

    $familyId = $person['familyId'];
    if (Util::checkPermission($familyDB->getUserPermission($userId, $familyId)) < 4) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }

    $data = [
        'title' => $title,
        'zan' => $zan,
        'branch_name' => $branchName,
        'zan_type' => $zanType,
        'origin_name' => $originName
    ];
    foreach ($data as $k => $v) {
        if ($v === null) {
            unset($data[$k]);
        };
    }

    $originTitle = $personDB->getPersonPdfTitle($personId);
    if ($originTitle) {
        $result = $personDB->UpdateOrCreatePdfTitle($personId, $familyId, $userId, $data, true);
    } else {
        $result = $personDB->UpdateOrCreatePdfTitle($personId, $familyId, $userId, $data, false);
    }

    if ($result[0] === -1){
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "$result[1]");
        exit;
    }

    $familyNewsDB = new CDBFamilyNews();
    $familyNewsDB->addFamilyNews($familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['EDITPERSON'], $userId, $personId, $person['name'], $userId);


    Util::printResult($GLOBALS['ERROR_SUCCESS'], ["result" => $result[1]]);

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
