<?php
/**
 * 用户申请加入家族
 * @author liuzhenhao
 * @date 2020-07-14
 */


use Util\Check;
use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;

$userId = $GLOBALS['userId'];

$familyId = Check::checkInteger(trim($params['familyId'] ?? ''));
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
$fatherName =  Check::check(trim(isset($params['fatherName']) ? $params['fatherName'] : ''));

if (!$name && (!$name && $fatherName)){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数仅能只传入name或者同时传入name和fatherName");
    exit;
}

try{
    $personDB = new CDBPerson();
    $familyDB = new CDBFamily();

    //检查用户权限
    if(Util::checkPermission($familyDB->getUserPermission($userId,$familyId),[],false) < 1){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "该用户无权限");
        exit;
    }

    $data = $personDB->checkPersonOrFather($name, $fatherName, $familyId);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data ? ['status'=>1]:['status'=>0]);
}catch(PDOException $e){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}