<?php
ini_set('memory_limit', '512M');
/**
 * 直通图的初代实现
 *  @author yuanxin
 *  @version 3
 *  @date 2020/8/11
 */
use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));
//必须
$personId = Check::checkInteger(trim($params['personId'] ?? 0));
// $userFamilyId = null;
// if ($isBranch == 1) {
//     $userFamilyId = Check::checkInteger(trim($params['userFamilyId'] ?? ''));
// }
$userId = $GLOBALS['userId'];
try {
	if ($familyId != "") {
		$familyDB = new CDBFamily();
		$personDB = new CDBPerson();
		// 兄弟之间偏移量
		// $brotherX = 306;
		$brotherX = 150;
		// 父子之间偏移量
		$fatherY = 80;
		$persons = $personDB->getFamilyPersonsByFamilyId($familyId,1);
		// $personId = 6697059310603600896;
		$personId  =  $personId >0 ? $personId  : $persons[0]['id'];
		// $corePerson = $personDB->getPersonById2($personId);
		$firstChild = getFirstChild($persons,$personId);
		$corePerson = $personDB->getPersonById2($firstChild);
		// var_dump($persons);
		// exit;
		$currentPersonFamilyIndex = $corePerson->familyIndex;
		$currentLevel =  $corePerson->level;
		// $currentLevel  = $persons[0]['level'];
		$rootLevel = $persons[0]['level'];
		
		$result = array();
		//层级最近小孩子指针
		$minChild = 0;
		// 层级最大孩子指针
		$maxChild = count($persons);
		$personsCount = $maxChild;
		// // 下一个兄弟指针，没有兄弟指向末尾
		// $nextBrother = $personsCount;
		$fix  =  array();
		// $brother = array();
		for ($j = $rootLevel ; $j <= $persons[$maxChild - 1]['level'] ; $j++) {
			$fix[$j - $rootLevel]  =  0;
        }
        
		if($maxChild > 0) {
			for ($i = 0 ; $i < $personsCount ; $i++) {
				//  没有偏移
				if($persons[$i]['level'] < $currentLevel){
					// 左上角
					if($persons[$i]['family_index'] >= $minChild && ($persons[$i]['family_index']<=$currentPersonFamilyIndex)){
                        $minChild = $persons[$i]['family_index'];
                       $fix[$persons[$i]['level'] - $rootLevel] ++;
					   array_push($result,$persons[$i]);
					//    右上角
					}elseif($persons[$i]['family_index'] >= $currentPersonFamilyIndex && ($persons[$i]['family_index'] < $maxChild)){
						$maxChild = $persons[$i]['family_index'];
                        array_push($result,$persons[$i]);
                    }elseif($persons[$i]['family_index'] > $maxChild){
						$brotherKey = count($result) - 1 ;
						$brother = array_merge($result[$brotherKey]['brother'],$result[$brotherKey]['sister']);
						if($brother != null){
							if(in_array($persons[$i]['id'],$brother)){
								array_push($result,$persons[$i]);
							}
						}
					
					}
                //  辈分等于当前选中人物
                 }elseif($persons[$i]['level'] == $currentLevel){
                    if($persons[$i]['family_index'] >= $minChild && $persons[$i]['family_index']<= $currentPersonFamilyIndex){
						$fix[$persons[$i]['level'] - $rootLevel] ++;
                        array_push($result,$persons[$i]);
					}elseif($persons[$i]['family_index'] >= $currentPersonFamilyIndex && ($persons[$i]['family_index'] < $maxChild)){
                        array_push($result,$persons[$i]);
                    }
				}
			}
		}
		$currentLevel  = $result[0]['level'];
		$x = 0;
		for ($j = 0 ; $j < count($result) ; $j++) {
			if($result[$j]['level'] > $currentLevel) {
				$x = 0;
				$currentLevel = $result[$j]['level'];
			}
			$result[$j]['x'] = $brotherX*($x - ($fix[$currentLevel - $rootLevel] > 0?$fix[$currentLevel - $rootLevel] - 1 : 0));
			$result[$j]['y'] = $fatherY*($currentLevel - $rootLevel);
			$x++;
	
		}

		$data['persons'] = $result;
		if (count($persons) > 0) {
			Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
		} else {
			Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "sql查询时发生错误");
		}
	} else {
		Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
		exit;
	}
}
catch (PDOException $e) {
	//异常处理
	$logger->error(Util::exceptionFormat($e));
	Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
/**
 *   获取孩子排行
 *  
 */
function getChildrenRank($persons,$children) {
	if(count($children) > 1) {
		$ranking = array();
		$newChildren = array();
		$n = 0;
		foreach($persons as $key => $person) {
			if(in_array($person['id'],$children)) {
				$ranking[$key] = $person['ranking'];
				$newChildren[$n++] = $person['id'];
				if($n == count($children)) {
					break;
				}
			}
		}
		array_multisort($ranking, SORT_ASC, $newChildren);
		return  $newChildren;
	} else {
		return $children;
	}
}
/**
 *   递归获取第一个孩子
 *  
 */
function getFirstChild($persons,$personId) {
	foreach($persons as $person) {
		if($person['id'] == $personId) {
			if(count($person['son']) + count($person['daughter']) > 0) {
				$children  =  array_merge($person['son'],$person['daughter']);
				$children = getChildrenRank($persons,$children);
				$firstChild =  $children[0];
				return getFirstChild($persons,$firstChild);
			} else {
				return $personId;
			}
		}
	}
}