<?php

/**
 * 批量添加相关人物
 * 依据单个添加人物进行简单的封装
 * @author  liuzhenhao
 * @version 3
 *
 *
 */

use DB\CDBArea;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use DB\DocumentDB;
use DB\CDBFamily;
use Model\Document;
use Model\Person;
use Util\Check;
use Util\PhotoGenerator;
use Util\Util;
use Util\HttpContext;
use ThirdParty\MiniProgram;
use Util\RedLock\RedLock;
use Config\RedisInstanceConfig;
use Util\SysConst;

// 批量添加的数组
$peopleData = Check::checkArray(json_decode(trim(isset($params['peopleData']) ? $params['peopleData'] : ''), true));
// 家族id
$familyId = Check::check(trim(isset($params['familyId']) ? $params['familyId'] : 0));
$type = Check::checkInteger($params['type'] ?? 0);

if (!$peopleData || !$familyId){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

if (count($peopleData) > 20){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数过多， 请减少参数");
    exit;
}

$checkData = [];

foreach ($peopleData as $k => $v) {
    //被添加人姓名
    $checkData[$k]['name'] = Check::check(trim(isset($v['name']) ? $v['name'] : ''), 1, 30);
    //被添加人与发起人关系
    $checkData[$k]['relation'] = trim(isset($v['relation']) ? $v['relation'] : '');
    //添加发起人结点
    $checkData[$k]['corePersonId'] = trim(isset($v['corePersonId']) ? $v['corePersonId'] : '');
    // 被添加人排名
    $checkData[$k]['ranking'] = Check::checkInteger(trim($v['ranking'] ?? 0));

    $checkData[$k]['photo'] = Check::check(trim(isset($v['photo']) ? $v['photo'] : ''));
    // 性别
    $checkData[$k]['gender'] = trim(isset($v['gender']) ? $v['gender'] : '');            //必须

    $checkData[$k]['address'] = trim(isset($v['address']) ? $v['address'] : '');
    $checkData[$k]['birthday'] = trim(isset($v['birthday']) ? $v['birthday'] : null);

    $checkData[$k]['zi'] = Check::check(trim(isset($v['zi']) ? $v['zi'] : ''), 0, 10);
    $checkData[$k]['zpname'] = Check::check(trim(isset($v['zpname']) ? $v['zpname'] : ''), 0, 10);
    $checkData[$k]['remark'] = Check::check(trim(isset($v['remark']) ? $v['remark'] : ''), 0, 255);
    $checkData[$k]['isDead'] = Check::checkInteger(trim(isset($v['isDead']) ? $v['isDead'] : 0)); //默认为0 0表示未去世
    $checkData[$k]['deadTime'] = trim(isset($v['deadTime']) ? $v['deadTime'] : null);

    $checkData[$k]['profileText'] = Check::check($v['profileText'] ?? '');          // 详细信息的输入
    $checkData[$k]['sideText'] = Check::check($v['sideText'] ?? '');                      // 侧边栏文本
    $checkData[$k]['branchName'] = Check::check(trim(isset($v['branchName']) ? $v['branchName'] : null), 0, 100);
}

// 这里要对家族进行加锁，这个锁防止出现脏数据
$redlockConfig = RedisInstanceConfig::getRedlockConfig();
$redLock = new RedLock($redlockConfig);
// 对操作加锁
$lock = $redLock->lock(SysConst::$FAMILY_LOCK . $familyId);
if (is_bool($lock) && !$lock) {
    // 没有拿到锁，报错
    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '当前正在添加人物请稍后再试');
    exit;
}
$data = [];
try {
    $personDB = new CDBPerson();
    $personDB->init();
    $personDB->pdo->beginTransaction();
    foreach ($checkData as $k => $v) {
        //被添加人姓名
        $name = $checkData[$k]['name'];
        //被添加人与发起人关系
        $relation = $checkData[$k]['relation'];
        //添加发起人结点
        $corePersonId = $checkData[$k]['corePersonId'];
        // 被添加人排名
        $ranking = $checkData[$k]['ranking'];

        $photo = $checkData[$k]['photo'];
        // 性别
        $gender = trim(isset($v['gender']) ? $v['gender'] : '');            //必须

        $address = $checkData[$k]['address'];
        $birthday = $checkData[$k]['birthday'];

        $zi = $checkData[$k]['zi'];
        $zpname = $checkData[$k]['zpname'];
        $remark = $checkData[$k]['remark'];
        $isDead = $checkData[$k]['isDead']; //默认为0 0表示未去世
        $deadTime = $checkData[$k]['deadTime'];

        $profileText = $checkData[$k]['profileText'];          // 详细信息的输入
        $sideText = $checkData[$k]['sideText'];                      // 侧边栏文本
        $branchName = $checkData[$k]['branchName'];

        if ($type != 0) {
            $wxApp = new MiniProgram($type);
            $checkMsg = $wxApp->msgSecCheck([$address, $zi, $zpname, $remark, $profileText, $sideText]);
            if (!$checkMsg) {
                $personDB->pdo->rollback();
                $redLock->unlock($lock);
                Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
                exit();
            }
        }

        if ($name != "" && $corePersonId != "") {
            $familyDB = new CDBFamily();
            $userId = $GLOBALS['userId'];
            $personDB = new CDBPerson();
            $corePerson = $personDB->getPersonById2($corePersonId);


            //检查操作的权限
            $permission = $familyDB->getUserPermission($userId, $familyId);
            if (Util::checkPermission($permission, [$corePerson->familyIndex]) < 3) {
                $personDB->pdo->rollback();
                $redLock->unlock($lock);
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            // 检查家族类型是否是家庭,1是家族

            $familyDetail = $familyDB->getFamilyById($familyId);
            if ($familyDetail->groupType != 1) {
                $personDB->pdo->rollback();
                $redLock->unlock($lock);
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
                exit;
            }

            if ($corePerson->isAdoption == 2) {
                $personDB->pdo->rollback();
                $redLock->unlock($lock);
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "出继人物下不能添加人物");
                exit;
            }

            if ($corePerson->bicolor > 1) {
                $personDB->pdo->rollback();
                $redLock->unlock($lock);
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "祧子下不能添加人物");
                exit;
            }


            $CDBArea = new CDBArea();
//        $result = $CDBArea->getAreaNameById($country);
            $countryName = '0';
//        $result = $CDBArea->getAreaNameById($province);
            $provinceName = '0';
//        $result = $CDBArea->getAreaNameById($city);
            $cityName = '0';
//        $result = $CDBArea->getAreaNameById($area);
            $areaName = '0';
//        $result = $CDBArea->getAreaNameById($town);
            $townName = '0';


            $rankings = $personDB->getChildrenRanking($corePersonId);
            $tempRanking = $ranking;
            $tempId = 0;


            if (in_array($ranking, $rankings)) {
                $tempRanking = $rankings[count($rankings) - 1] + 1;
            } else {
                $rankings = $personDB->getChildrenRanking($corePersonId, false, 0, true);
                if ($ranking >= $rankings[count($rankings) - 1]['ranking']) {
                    $tempRanking = $rankings[count($rankings) - 1]['ranking'] + 1;
//                $tempId = $rankings[count($rankings) - 1]['id'] + 1;
                } else {
                    for ($i = 0; $i < count($rankings); $i++) {
                        if ($rankings[$i]['ranking'] < $tempRanking) {
                            $tempId = $rankings[$i]['id'];
                        } else {
                            break;
                        }
                    }
                }
            }


            $person = new Person();

            $person->id = Util::getNextId();

            $person->name = $name;
            $person->relation = $relation;
            $person->corePersonId = $corePersonId;

            $person->ranking = $tempRanking;
            $person->photo = $photo;


            $person->countryName = $countryName;
            $person->provinceName = $provinceName;
            $person->cityName = $cityName;
            $person->areaName = $areaName;
            $person->townName = $townName;

            $person->gender = $gender;
            $person->country = 0;
            $person->province = 0;
            $person->city = 0;
            $person->area = 0;
            $person->town = 0;
            $person->address = $address;
            $person->birthday = $birthday;
            $person->zi = $zi;
            $person->zpname = $zpname;
            $person->remark = $remark;
            $person->isDead = $isDead;
            $person->deadTime = $deadTime;
            $person->profileText = $profileText;
            $person->sideText = $sideText;


            $person->familyId = $corePerson->familyId;

            // 根据添加关系处理被添加人属性
            switch ($relation) {
                case $GLOBALS['RELATION_FATHER'] :
                {
                    if ($corePerson->father != null) {
                        $personDB->pdo->rollback();
                        $redLock->unlock($lock);
                        Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物已有父亲，不允许添加");
                        exit;
                    }
                    $person->level = $corePerson->level - 1;
                    $person->gender = 1;
                    $person->type = 1;
                    $person->familyIndex = 0;
                    break;
                }
                case $GLOBALS['RELATION_SPOUSE'] :
                {
                    $person->level = $corePerson->level;
                    $person->gender = ($corePerson->gender == 1 ? 0 : 1);
                    $person->type = 2;
                    $person->isNotMain = 1;
                    $person->familyIndex = $corePerson->familyIndex + count($corePerson->spouse) + 1;
                    break;
                }
                case $GLOBALS['RELATION_SON'] :
                {
                    $person->level = $corePerson->level + 1;
                    $person->gender = 1;
                    $person->type = 1;

                    if ($tempId > 0) {
                        $tempPerson = $personDB->getPersonById2($tempId);
                        $person->familyIndex = $personDB->getLastPersonFamilyIndex($familyId, $corePerson->level + 1, $tempPerson->familyIndex) + 1;
                    } else {
                        $person->familyIndex = $personDB->getLastPersonFamilyIndex($familyId, $corePerson->level, $corePerson->familyIndex) + 1;
                    }
                    if ($corePerson->gender == 0) {
                        $person->isNotMain = 1;
                    } else {
                        $person->isNotMain = $corePerson->isNotMain == 1 ? 1 : 0;
                    }
                    break;
                }
                case $GLOBALS['RELATION_DAUGHTER'] :
                {
                    $person->level = $corePerson->level + 1;
                    $person->gender = 0;
                    $person->type = 1;
                    if ($corePerson->gender == 0) {
                        $person->isNotMain = 1;
                    } else {
                        $person->isNotMain = $corePerson->isNotMain == 1 ? 1 : 0;
                    }

                    if ($tempId > 0) {
                        $tempPerson = $personDB->getPersonById2($tempId);
                        $person->familyIndex = $personDB->getLastPersonFamilyIndex($familyId, $corePerson->level + 1, $tempPerson->familyIndex) + 1;
                    } else {
                        $person->familyIndex = $personDB->getLastPersonFamilyIndex($familyId, $corePerson->level, $corePerson->familyIndex) + 1;
                    }
                    break;
                }
                default:
                    $personDB->pdo->rollback();
                    $redLock->unlock($lock);
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '人物关系不正确');
                    exit;
            }


            $person->id = $personDB->addRelatePerson2($person, false);

            $personId = $person->id;
            $data[$k]['personId'] = $person->id;
            $data[$k]['familyIndex'] = $person->familyIndex;
            $data[$k]['gender'] = $person->gender;
            $data[$k]['type'] = $person->type;
            $data[$k]['level'] = $person->level;
            $data[$k]['ranking'] = $person->ranking;
            $data[$k]['name'] = $person->name;
            $data[$k]['isNotMain'] = $person->isNotMain;

            if ($personId > 0) {
                $familyNewsDB = new CDBFamilyNews();
                $familyNewsDB->addFamilyNews($familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['ADDRELATEPERSON'], $userId, $personId, $name, $userId);
                if (!HttpContext::getInstance()->getEnv('private_db_host')) {
                    $CDBPushPerson = new CDBPushPerson();
                    $modelId = $personId;
                    $action = 1;
                    $source = $person;
                    $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
                }

                if ($branchName !== null) {
                    $createBranchName = $personDB->UpdateOrCreatePdfTitle($personId, $familyId, $userId, ['branch_name' => $branchName], true);
//                    if ($createBranchName[0] === -1){
//                        $personDB->pdo->rollback();
//                        $redLock->unlock($lock);
//                        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "$createBranchName[1]");
//                        exit;
//                    }
                }
            }else if ($personId === -1) {
                $personDB->pdo->rollback();
                $redLock->unlock($lock);
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错,数据库异常");
            } else if ($personId === -2) {
                $personDB->pdo->rollback();
                $redLock->unlock($lock);
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错，该人物不属于这个家族");
            } else {
                $personDB->pdo->rollback();
                $redLock->unlock($lock);
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "未知错误");
            }
        }
    }
    $personDB->pdo->commit();
    // 添加人物之后，释放锁
    $redLock->unlock($lock);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    exit;

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $personDB->pdo->rollback();
    $redLock->unlock($lock);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

