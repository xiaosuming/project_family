<?php


/**
 * 获取兄弟姐妹关系
 * @author liuzhenhao
 * @date 2020-07-23
 */


use Util\Check;
use Util\Util;
use DB\CDBPerson;

$userId = $GLOBALS['userId'];

$personId = Check::checkInteger(trim($params['personId'] ?? ''), 'personId');


try {
    $personDB = new CDBPerson();

    $data = $personDB->getSiblingRelationshipInfo($personId);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}