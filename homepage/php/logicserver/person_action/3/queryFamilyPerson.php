<?php
    ini_set('memory_limit', '512M');
    /**
     * 获取家族中的所有人物
     */
    use Util\Util;
    use DB\CDBPerson;
    use DB\CDBFamily;
    use Util\Check;
    //queryFamilyPerson($familyId)
    
    $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));				//必须
    $personId = isset($params['personId']) && intval($params['personId']) > 0 ? intval($params['personId']) : 0;
    $isBranch = Check::checkInteger(trim($params['isBranch'] ?? 0));
    $needOtherInfo = Check::checkInteger(trim(isset($params['needOtherInfo']) ? $params['needOtherInfo'] : 0));



    $userFamilyId = null;
    if($isBranch == 1){
        $userFamilyId = Check::checkInteger(trim($params['userFamilyId'] ?? ''));
    }
    $userId = $GLOBALS['userId'];

    try{
        if($familyId!=""){
            $familyDB = new CDBFamily();
            $personDB = new CDBPerson();

            $family = $familyDB->getFamilyById($familyId);
            $person = $personDB->getPersonObjectById($personId);

            if ($family == null) {
                Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                exit;
            }

            if ($family->openType == 0) {
                // 私有族群检查权限

                $permission = $familyDB->getUserPermission($userId, $familyId);
                if (Util::checkPermission($permission,[$person->familyIndex]) < 0){
                    Util::printResult( $GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                    exit;
                }
            }

            $persons = $personDB->getFamilyPersons($familyId, null, $personId, $needPermission, $needOtherInfo);

            foreach($persons as &$person) {
                if ($person['refFamilyId'] == ""){
                    $person['refFamilyId'] = null;
                }
                if (!empty($person['refFamilyId'])) {
                    $person['refFamilyId'] = json_decode($person['refFamilyId'],true);
                }
            }
            $data['persons'] = $persons;
            if(count($persons) > 0){
                Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
            }else{
                Util::printResult( $GLOBALS['ERROR_SQL_QUERY'], "sql查询时发生错误");
            }
            
        }else{
            Util::printResult( $GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
            exit;
        }	
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
