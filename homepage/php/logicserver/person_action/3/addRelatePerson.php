<?php
/**
 * 添加相关人物
 * 依据版本三的逻辑，移除了个人信息等不必要的参数。
 *  @author  yuanxin
 *  @version 3
 * 
 *
 */
use DB\CDBArea;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use DB\DocumentDB;
use DB\CDBFamily;
use Model\Document;
use Model\Person;
use Util\Check;
use Util\PhotoGenerator;
use Util\Util;
use Util\HttpContext;
use ThirdParty\MiniProgram;
use Util\RedLock\RedLock;
use Config\RedisInstanceConfig;
use Util\SysConst;

// 家族id
$familyId = Check::check(trim(isset($params['familyId'])?$params['familyId']:0));
//被添加人姓名
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''), 1, 30);
//被添加人与发起人关系
$relation = trim(isset($params['relation']) ? $params['relation'] : '');
//添加发起人结点
$corePersonId = trim(isset($params['corePersonId']) ? $params['corePersonId'] : '');
// 被添加人排名
$ranking = Check::checkInteger(trim($params['ranking'] ?? 0));
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));
//上一个人的familyIndex  ,父亲的坐标一定为起始节点0,配偶为当前人物+配偶个数+1,孩子为当前节点深度遍历最后一个结点
// $familyIndex = Check::checkInteger(trim(isset($params['familyIndex'])?$params['familyIndex']:''));
//上一个人的personId 配合familyIndex用于验证数据库是否被更改过
// $lastPersonId = trim(isset($params['lastPersonId']) ? $params['lastPersonId'] : '');


// $name = Check::check(trim(isset($params['name']) ? $params['name'] : ''), 1, 30);                //必须
$gender = trim(isset($params['gender']) ? $params['gender'] : '');            //必须
$country = Check::checkInteger(trim(isset($params['country']) ? $params['country'] : 0)); //国家 中国 默认为 0
$province = Check::checkInteger(trim(isset($params['province']) ? $params['province'] : 0)); //省 没有默认为 0
$city = Check::checkInteger(trim(isset($params['city']) ? $params['city'] : 0)); //市 没有默认为 0
$area = Check::checkInteger(trim(isset($params['area']) ? $params['area'] : 0)); //区 没有默认为 0
$town = Check::checkInteger(trim(isset($params['town']) ? $params['town'] : 0)); //乡 没有默认为 0
$address = trim(isset($params['address']) ? $params['address'] : '');
$birthday = trim(isset($params['birthday']) ? $params['birthday'] : null);
// $relation = trim(isset($params['relation']) ? $params['relation'] : '');
// $corePersonId = trim(isset($params['corePersonId']) ? $params['corePersonId'] : '');    //必须
// $photo = trim(isset($params['photo']) ? $params['photo'] : '');
$zi = Check::check(trim(isset($params['zi']) ? $params['zi'] : ''), 0, 10);
$zpname = Check::check(trim(isset($params['zpname']) ? $params['zpname'] : ''), 0, 10);
$remark = Check::check(trim(isset($params['remark']) ? $params['remark'] : ''), 0, 255);
$isDead = Check::checkInteger(trim(isset($params['isDead']) ? $params['isDead'] : 0)); //默认为0 0表示未去世
$deadTime = trim(isset($params['deadTime']) ? $params['deadTime'] : null);
// $ranking = Check::checkInteger($params['ranking'] ?? 0);
$profileText = Check::check($params['profileText'] ?? '');          // 详细信息的输入
$sideText = Check::check($params['sideText'] ?? '');                      // 侧边栏文本
$branchName = Check::check(trim(isset($params['branchName']) ? $params['branchName'] : null), 0, 100);

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$address, $zi, $zpname, $remark, $profileText, $sideText]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
	if ($name != ""  && $corePersonId != "") {
        $familyDB = new CDBFamily();
        $userId = $GLOBALS['userId'];
        $personDB = new CDBPerson();
        $corePerson = $personDB->getPersonById2($corePersonId);



		//检查操作的权限
        $permission = $familyDB->getUserPermission($userId, $familyId);
        if (Util::checkPermission($permission,[$corePerson->familyIndex]) < 3){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

		// 检查家族类型是否是家庭,1是家族

		$familyDetail = $familyDB->getFamilyById($familyId);
		if ($familyDetail->groupType != 1) {
			Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
			exit;
		}

		if ($corePerson->isAdoption == 2){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "出继人物下不能添加人物");
            exit;
        }

        if ($corePerson->bicolor > 1){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "祧子下不能添加人物");
            exit;
		}
		

		$CDBArea = new CDBArea();
        $result = $CDBArea->getAreaNameById($country);
        $countryName = $result['areaName'] ?? '0';
        $result = $CDBArea->getAreaNameById($province);
        $provinceName = $result['areaName'] ?? '0';
        $result = $CDBArea->getAreaNameById($city);
        $cityName = $result['areaName'] ?? '0';
        $result = $CDBArea->getAreaNameById($area);
        $areaName = $result['areaName'] ?? '0';
        $result = $CDBArea->getAreaNameById($town);
        $townName = $result['areaName'] ?? '0';


        // 这里要对家族进行加锁，这个锁防止出现脏数据
        $redlockConfig = RedisInstanceConfig::getRedlockConfig();
        $redLock = new RedLock($redlockConfig);
        // 对操作加锁
        $lock = $redLock->lock(SysConst::$FAMILY_LOCK.$familyId);
        if (is_bool($lock) && !$lock) {
            // 没有拿到锁，报错
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '当前正在添加人物请稍后再试');
            exit;
        }



        $rankings = $personDB->getChildrenRanking($corePersonId);
        $tempRanking = $ranking;
        $tempId = 0;



        if (in_array($ranking,$rankings)){
            $tempRanking = $rankings[count($rankings) - 1] + 1;
        }else{
            $rankings = $personDB->getChildrenRanking($corePersonId,false,0,true);
            if ($ranking >= $rankings[count($rankings) - 1]['ranking']){
                $tempRanking = $rankings[count($rankings) - 1]['ranking'] + 1;
//                $tempId = $rankings[count($rankings) - 1]['id'] + 1;
            }else{
                for ($i = 0 ; $i < count($rankings);$i++){
                    if ($rankings[$i]['ranking'] < $tempRanking){
                        $tempId = $rankings[$i]['id'];
                    }else{
                        break;
                    }
                }
            }
        }


		$person = new Person();

		$person->id = Util::getNextId();

        $person->name = $name;
		$person->relation = $relation;
		$person->corePersonId = $corePersonId;

		$person->ranking = $tempRanking;
		$person->photo = $photo;


		$person->countryName = $countryName;
        $person->provinceName = $provinceName;
        $person->cityName = $cityName;
        $person->areaName = $areaName;
        $person->townName = $townName;
		
		$person->gender=$gender;
		$person->country=$country;
		$person->province=$province;
		$person->city=$city;
		$person->area=$area;
		$person->town=$town;
		$person->address=$address;
		$person->birthday=$birthday;
		$person->zi=$zi;
		$person->zpname=$zpname;
		$person->remark=$remark;
		$person->isDead=$isDead;
		$person->deadTime=$deadTime;
		$person->profileText=$profileText;
		$person->sideText=$sideText;

		
		$person->familyId = $corePerson->familyId;

		// 根据添加关系处理被添加人属性		
		switch($relation){
				 case $GLOBALS['RELATION_FATHER'] :
					{
						if($corePerson->father!=null){
							Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物已有父亲，不允许添加");
							exit;
						}
						$person->level = $corePerson->level - 1;
						$person->gender = 1;
						$person->type = 1;
						$person->familyIndex = 0; 					
					break;
					}
				case $GLOBALS['RELATION_SPOUSE'] :
					{
						$person->level = $corePerson->level;
						$person->gender = ($corePerson->gender==1?0:1);
						$person->type = 2;
                        $person->isNotMain=1;
						$person->familyIndex =$corePerson->familyIndex+count($corePerson->spouse)+1;
					break;
					}
				case $GLOBALS['RELATION_SON'] :
					{
						$person->level = $corePerson->level+1;
						$person->gender = 1;
						$person->type = 1;

                        if ($tempId > 0){
                            $tempPerson = $personDB->getPersonById2($tempId);
                            $person->familyIndex  = $personDB->getLastPersonFamilyIndex($familyId,$corePerson->level + 1,$tempPerson->familyIndex) + 1;
                        }else{
                            $person->familyIndex = 	$personDB->getLastPersonFamilyIndex($familyId,$corePerson->level,$corePerson->familyIndex) + 1 ;
                        }
						if ($corePerson->gender == 0 ){
                            $person->isNotMain = 1;
                        }else{
                            $person->isNotMain = $corePerson->isNotMain == 1? 1:0;
                        }
					break;
					}
				case $GLOBALS['RELATION_DAUGHTER'] :
					{
						$person->level = $corePerson->level+1;
						$person->gender = 0;
						$person->type =1;
                        if ($corePerson->gender == 0){
                            $person->isNotMain = 1;
                        }else{
                            $person->isNotMain = $corePerson->isNotMain == 1? 1:0;
                        }

                        if ($tempId > 0){
                            $tempPerson = $personDB->getPersonById2($tempId);
                            $person->familyIndex  = $personDB->getLastPersonFamilyIndex($familyId,$corePerson->level + 1,$tempPerson->familyIndex) + 1;
                        }else{
                            $person->familyIndex = 	$personDB->getLastPersonFamilyIndex($familyId,$corePerson->level,$corePerson->familyIndex) + 1 ;
                        }
					break;
					}	
				 default:
                    Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], '人物关系不正确');
                exit;
		}



		$person->id = $personDB->addRelatePerson2($person,true);

        // 添加人物之后，释放锁
        $redLock->unlock($lock);

		$personId = $person->id;
		$data['personId'] = $person->id;
		$data['familyIndex'] = $person->familyIndex;
		$data['gender'] = $person->gender;
		$data['type'] = $person->type;
		$data['level'] = $person->level;
		$data['ranking'] = $person->ranking;
		$data['name'] = $person->name;
		$data['isNotMain'] = $person->isNotMain;
		
		if ($personId > 0) {
			$familyNewsDB = new CDBFamilyNews();
			$familyNewsDB->addFamilyNews($familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['ADDRELATEPERSON'], $userId, $personId, $name, $userId);
			if (!HttpContext::getInstance()->getEnv('private_db_host')) {
				$CDBPushPerson = new CDBPushPerson();
				$modelId = $personId;
				$action = 1;
				$source = $person;
				$CDBPushPerson->setPushPersonTask($modelId, $action, $source);
			}

			if ($branchName !== null){
                $createBranchName = $personDB->UpdateOrCreatePdfTitle($personId, $familyId, $userId, ['branch_name' => $branchName], true);

//                if ($createBranchName[0] === -1){
//                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "$createBranchName[1]");
//                    exit;
//                }
            }

			Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
		} else if ($personId === -1) {
			Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错,数据库异常");
		} else if ($personId === -2) {
			Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错，该人物不属于这个家族");
		} else {
			Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "未知错误");
		}
	} else {
		Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
		exit;
	}
}
catch (PDOException $e) {
	//异常处理
//    print_r($e);
	$logger->error(Util::exceptionFormat($e));
	Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
