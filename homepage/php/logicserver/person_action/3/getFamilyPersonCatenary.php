<?php
ini_set('memory_limit', '512M');

/**
 *  获取带配偶的吊线图,左右方向
 * @author yuanxin
 * @version 3
 *
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

// 输入验证
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));                //必须
$personId = Check::checkInteger(trim($params['personId'] ?? 0));
$showType = Check::checkInteger(trim($params['showType'] ?? 1)); //显示方向  1左起 -1右起
$filter = Check::checkInteger(trim($params['filter'] ?? 0)); //是否过滤配偶 0显示 1不显示
$showMain = Check::checkInteger(trim($params['isNotMain'] ?? 1)); //是否过滤外亲
$showLocal = Check::checkInteger(trim($params['showLocal'] ?? 0)); //局部展示 0 全部展示 1 局部展示

$userFamilyId = null;

$userId = $GLOBALS['userId'];

try {
    if ($familyId != "") {
        $familyDB = new CDBFamily();

        $family = $familyDB->getFamilyById($familyId);
        $userPermissionData = $familyDB->getUserPermission($userId, $familyId);
        $showMain = intval($showMain);

        if ($family->openType == 0) {
            if ($userPermissionData == null) {
                Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '权限错误');
                exit;
            }
        }

        $personDB = new CDBPerson();
        
        //TODO 更新数据库gener_family的membercount字段 ，使用membercount
    //    $persons = $personDB->getFamilyPersons3($familyId, null, 0, $filter,$showMain);
        // $data['showType'] =  "local";
        // $persons = $personDB->getTwoThousandsPeople($familyId,$personId,$filter,$showMain);
        // 
        // if(count($persons)>3000)
    if($family->memberCount >3000){
        $data['showType'] =  "local";
            // $persons = $personDB->getTwoThousandsPeople($familyId,$personId,$filter,$showMain);
            // if($personId == 0)
            // {
            //     $persons = $personDB->getTwoThousandsPeople($familyId,0,$filter);
            // }else{
            //     $persons = $personDB->getTwoThousandsPeople($familyId,$personId,$filter,$showMain);
            // }
             $persons = $personDB->getFamilyPersonsLocal2($familyId,$userId,$personId,$filter, $showMain);
        }else{
            $data['showType'] =  "all";
            $persons = $personDB->getFamilyPersons3($familyId, null, 0, $filter,$showMain);
        }




        if ($persons != null) {
            $rootY = $persons[0]['level'];
            $rootX = $persons[0]['familyIndex'];
            // 兄弟之间偏移量
            $brotherX = 306;
            // 父子之间偏移量
            $fatherY = 80;
            // 配偶之间偏移量
            $spouseX = 153;
            if ($filter == 1) {
                $brotherX = 150;
            }
            $lastLevel = $rootY;
            $lastPersonX = 0;
            // 深度
            $deep = 0;
            // 不显示配偶
            if ($filter == 1) {
                foreach ($persons as $key => $person) {
                    $persons[$key]['y'] = ($person['level'] - $rootY) * $fatherY;
                    //    下一个添加的大于上一个辈分,向下添加
                    if ($person['level'] > $lastLevel) {
                        //    向下偏移量
                        $persons[$key]['x'] = $lastPersonX;
                    } else {
                        $persons[$key]['x'] = $lastPersonX + $brotherX;
                    }
                    $lastLevel = $person['level'];
                    $lastPersonX = $persons[$key]['x'];
                    $persons[$key]['x'] *= $showType;
                }
            } // 显示配偶
            else {
                $spouseArray = array();
                $spouseStartX = 0;
                foreach ($persons as $key => $person) {
                    $persons[$key]['y'] = ($person['level'] - $rootY) * $fatherY;
                    if ($person['type'] == 2) {
                        $persons[$key]['x'] = $spouseStartX + $spouseX;
                        $spouseStartX += $spouseX;
                        $persons[$key]['x'] *= $showType;
                      
                    } // 本家添加
                    else {
                        //    下一个添加的大于上一个辈分,向下添加
                        if ($person['level'] > $lastLevel) {
                            //    向下偏移量
                            $persons[$key]['x'] = $lastPersonX;
                        }
                        //    // 同级添加,向上逻辑相同
                        // 向上添加
                        else {
                            $persons[$key]['x'] = $lastPersonX + $brotherX;
                            if (count($spouseArray) > 0) {
                                // while($persons[$key]['level']<=array_key_last($spouseArray)){ //php7.3以前不支持这个方法
                                end($spouseArray);
                                key($spouseArray);
                                while ((count($spouseArray) > 0) && ($persons[$key]['level'] <= key($spouseArray))) {
                                    $x = array_pop($spouseArray) + 2;
                                    end($spouseArray);
                                    key($spouseArray);
                                    if ($persons[$key]['x'] <= $x) {
                                        $persons[$key]['x'] = $x + $spouseX;
                                    }
                                }
                            }
                        }
                        $lastLevel = $person['level'];
                        $lastPersonX = $persons[$key]['x'];
                        $spouseStartX = $lastPersonX;
                        $persons[$key]['x'] *= $showType;
                        // 当配偶大于1时，给当前辈分添加最大x值
                        if (count($person['spouse']) > 1) {
                            $spouseArray[$person['level']] = $lastPersonX + count($person['spouse']) * $spouseX;
                        }
                    }
                }
            }
            $data['persons'] = $persons;
        }

        $personsCount = count($persons);
        // if($personsCount > 3000)
        // {
        //     $data['showType'] =  "local";
        // }else{
        //     $data['showType'] =  "all";
        // }
        if ($personsCount > 0) {
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "sql查询时发生错误");
        }
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
