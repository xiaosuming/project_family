<?php
/**
 * Created by PhpStorm.
 * User: liuzhenhao
 * Date: 2020-09-09
 * Time: 下午2:02
 */

use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
use Util\Util;

$srcPersonId = Check::checkInteger(trim($params['srcPersonId'] ?? '')); //过继人
$destPersonId = Check::checkInteger(trim($params['destPersonId'] ?? '')); //接收人
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));

if ($srcPersonId == '' || $destPersonId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '参数缺失');
    exit;
}

try {

    $personDB = new CDBPerson();
    $familyDB = new CDBFamily();
    $userId = $GLOBALS['userId'];
    //检查操作的权限

    $srcPerson = $personDB->getPersonObjectById($srcPersonId);
    $destPerson = $personDB->getPersonObjectById($destPersonId);
    $permission = $familyDB->getUserPermission($userId,$familyId);
    if (Util::checkPermission($permission,[$srcPerson->familyIndex,$destPerson->familyIndex]) < 3){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }


    $personDB = new CDBPerson();
    $delRow = $personDB->deleteAdoptionPersonByPersonId($srcPersonId, $destPersonId);
    if ($delRow>0){
        if (!$personDB->checkSrcPersonIdIsExists($srcPersonId)){
            $personDB->restoreIsAdoption($srcPersonId);
        }
        if (!$personDB->checkDestPersonIdIsExists($destPersonId)){
            $personDB->restoreIsAdoption($destPersonId);
        }
    }
    $data['delRow'] = $delRow;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

