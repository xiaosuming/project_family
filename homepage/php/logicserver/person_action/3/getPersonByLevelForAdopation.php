<?php


use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;

$familyId = Check::checkInteger(trim($params['familyId'] ?? ''),'familyId');
$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : 20));
$page = Check::checkInteger(trim(isset($params['page']) ? $params['page'] : 1));
$name = Check::check(trim(isset($params['name']) ? $params['name'] : ''));
$showNaturalParent = Check::checkInteger(trim(isset($params['showNaturalParent']) ? $params['showNaturalParent'] : 0));
$showAdoption = Check::checkInteger(trim(isset($params['showAdoption']) ? $params['showAdoption'] : 1));
$userId = $GLOBALS['userId'];

if ($pageSize > 99) {
    $pageSize = 99;
}

try {
    $userId = $GLOBALS['userId'];

    $personDB = new CDBPerson();

    $person = $personDB->getPersonObjectById($personId);

    if (!$person){
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物不存在");
        exit;
    }

    $familyDB = new CDBFamily();

    $family = $familyDB->getFamilyById($familyId);

    if ($family->openType == 0) {
        //检查查询的权限
        if (!$familyDB->getUserPermission($userId,$familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    $data = $personDB->getPersonByLevelForAdopation($familyId, $person , $page, $pageSize,$name,$showNaturalParent,$showAdoption);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
