<?php
ini_set('memory_limit', '512M');

/**
 *  家族人员合并
 * 合并逻辑 :  A家族下的一个节点C,和B家族的祖先结点D合并,生成一个新家族F
 * C节点及以下的所有信息被覆盖,新生成的家族F下的节点为D
 * @author yuanxin
 * @version 3
 *
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

// 输入验证
$familyIdA = Check::checkInteger(trim(isset($params['familyIdA']) ? $params['familyIdA'] : ''));
$familyIdB = Check::checkInteger(trim(isset($params['familyIdB']) ? $params['familyIdB'] : ''));
// A家族中被合并的人物
$personId = Check::checkInteger(trim($params['personId'] ?? 0));
$level = Check::checkInteger(trim($params['level'] ?? 0));
// $familyIndex = Check::checkInteger(trim(isset($params['familyIndex']) ? $params['familyIndex'] : ''));
$showType = Check::checkInteger(trim($params['showType'] ?? 1)); //显示方向  1左起 -1右起
$familyPhoto = Check::check(trim(isset($params['familyPhoto']) ? $params['familyPhoto'] : ''));             //家族照片
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));         
$hxChatRoom = Check::check(trim(isset($params['hxChatRoom']) ? $params['hxChatRoom'] : ''));           
$desc = Check::check(trim(isset($params['description']) ? $params['description'] : ''), 0, 50);             //家族描述
$familyName = Check::check(trim(isset($params['familyName']) ? $params['familyName'] : ''), 1, 14);                //家族姓名
$surname = trim(isset($params['surname']) ? $params['surname'] : '');                          //姓氏
$groupType = Check::checkInteger($params['groupType'] ?? 1);        // 群体类型，默认是1，1是家族，2是家庭，3是师生，4是师徒 5是公司同事
$data = array();
//是否过滤配偶 0显示 1不显示
$filter = Check::checkInteger(trim($params['filter'] ?? 0));


try {



    $familyDB = new CDBFamily();
    $userId = $GLOBALS['userId'];

    // 管理员头像
    $hxChatRoom;

    $personDB = new CDBPerson();
    // 设置一个最大范围
 
    if(count($lastPerson->father)!=1){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "选中人物父亲数量异常");
        exit;
    }

    $endIndex = 999999999;
    $lastPerson = $personDB->getPersonById2($personId);
    $familyIndex = $lastPerson->familyIndex;
    // 先处理A家族
    $lastPersonFamilyIndex = $personDB->getLastPersonFamilyIndex($familyIdA, $level, $familyIndex);
    $familyPeopleAHead = $personDB->getFamilyPersonsByFamilyIndex($familyIdA, 0, $familyIndex - 1, $filter);
    // // 获取B家族的所有人
    $familyPeopleB = $personDB->getFamilyPersons3($familyIdB, null, 0, $filter);
    // 获取A家族的后半段
    $familyPeopleATail = $personDB->getFamilyPersonsByFamilyIndex($familyIdA, $lastPersonFamilyIndex + 1, $endIndex, $filter);
    // A家族人物关系处理
    if ($familyPeopleAHead != null) {
        // 迁移节点的孩子
        foreach ($familyPeopleAHead as $key => $person) {
            if(count($lastPerson->father)>0){
                if($person['id']  == $lastPerson->father[0]){
                    if($lastPerson->gender == 0){
                         foreach($familyPeopleAHead[$key]['daughter'] as $key2 => $daughter){
                             if($daughter  == $lastPerson->id){
                                 unset($familyPeopleAHead[$key]['daughter'][$key2]);
                                 array_push($familyPeopleAHead[$key]['daughter'],$familyPeopleB[0]['id']);
                             }
                         }
                    }else{
                        foreach($familyPeopleAHead[$key]['son'] as $key2 => $son){
                            if($son  == $lastPerson->id){
                                unset($familyPeopleAHead[$key]['son'][$key2]);
                                array_push($familyPeopleAHead[$key]['son'],$familyPeopleB[0]['id']);
                            }
                        }
                    }
                }
            }
            if(count($lastPerson->mother)>0){
                if($person['id']  == $lastPerson->mother[0]){
                    if($lastPerson->gender == 0){
                         foreach($familyPeopleAHead[$key]['daughter'] as $key2 => $daughter){
                             if($daughter  == $lastPerson->id){
                                 unset($familyPeopleAHead[$key]['daughter'][$key2]);
                                 array_push($familyPeopleAHead[$key]['daughter'],$familyPeopleB[0]['id']);
                             }
                         }
                    }else{
                        foreach($familyPeopleAHead[$key]['son'] as $key2 => $son){
                            if($son  == $lastPerson->id){
                                unset($familyPeopleAHead[$key]['son'][$key2]);
                                array_push($familyPeopleAHead[$key]['son'],$familyPeopleB[0]['id']);
                            }
                        }
                    }
                }
            }
            if(count($lastPerson->sister)>0){
                  if($person['id']  == $lastPerson->sister[0]){
                     if($lastPerson->gender == 0){
                      foreach($familyPeopleAHead[$key]['sister'] as $key2 => $sister){
                         if($sister  == $lastPerson->id){
                             unset($familyPeopleAHead[$key]['sister'][$key2]);
                             array_push($familyPeopleAHead[$key]['sister'],$familyPeopleB[0]['id']);
                         }
                     }
                      }else{
                    foreach($familyPeopleAHead[$key]['brother'] as $key2 => $brother){
                        if($brother  == $lastPerson->id){
                            unset($familyPeopleAHead[$key]['brother'][$key2]);
                            array_push($familyPeopleAHead[$key]['brother'],$familyPeopleB[0]['id']);
                        }
                    }
                  }
                
                }
              }
              if(count($lastPerson->brother)>0){
                if($person['id']  == $lastPerson->brother[0]){
                   if($lastPerson->gender == 0){
                    foreach($familyPeopleAHead[$key]['sister'] as $key2 => $sister){
                       if($sister  == $lastPerson->id){
                           unset($familyPeopleAHead[$key]['sister'][$key2]);
                           array_push($familyPeopleAHead[$key]['sister'],$familyPeopleB[0]['id']);
                       }
                   }
                    }else{
                  foreach($familyPeopleAHead[$key]['brother'] as $key2 => $brother){
                      if($brother  == $lastPerson->id){
                          unset($familyPeopleAHead[$key]['brother'][$key2]);
                          array_push($familyPeopleAHead[$key]['brother'],$familyPeopleB[0]['id']);
                      }
                  }
                }
              
              }
            }
        }

        foreach ($familyPeopleAHead as $key => $person) {
            if(count($lastPerson->brother)>0){
            if($person['id']  == $lastPerson->brother[0]){
                if($lastPerson->gender == 0){
                     foreach($familyPeopleAHead[$key]['sister'] as $key2 => $sister){
                         if($sister  == $lastPerson->id){
                             unset($familyPeopleAHead[$key]['sister'][$key2]);
                             array_push($familyPeopleAHead[$key]['sister'],$familyPeopleB[0]['id']);
                         }
                     }
                }else{
                    foreach($familyPeopleAHead[$key]['brother'] as $key2 => $brother){
                        if($brother  == $lastPerson->id){
                            unset($familyPeopleAHead[$key]['brother'][$key2]);
                            array_push($familyPeopleAHead[$key]['brother'],$familyPeopleB[0]['id']);
                        }
                    }
                }
            }
        }
        if(count($lastPerson->sister)>0){
            if($person['id']  == $lastPerson->sister[0]){
                if($lastPerson->gender == 0){
                     foreach($familyPeopleAHead[$key]['sister'] as $key2 => $sister){
                         if($sister  == $lastPerson->id){
                             unset($familyPeopleAHead[$key]['sister'][$key2]);
                             array_push($familyPeopleAHead[$key]['sister'],$familyPeopleB[0]['id']);
                         }
                     }
                }else{
                    foreach($familyPeopleAHead[$key]['brother'] as $key2 => $brother){
                        if($brother  == $lastPerson->id){
                            unset($familyPeopleAHead[$key]['brother'][$key2]);
                            array_push($familyPeopleAHead[$key]['brother'],$familyPeopleB[0]['id']);
                        }
                    }
                }
            }
        }
        }
        
        // unset($familyPeopleAHead[count($familyPeopleAHead) - 1]['son']);
        // if(!isset($familyPeopleAHead[count($familyPeopleAHead) - 1]['son'])){
        //     $familyPeopleAHead[count($familyPeopleAHead) - 1]['son'] = array();
        // }
        // array_push($familyPeopleAHead[count($familyPeopleAHead) - 1]['son'], $familyPeopleB[0]['id']);
        // 处理父亲
        $familyPeopleB[0]['father'] = array();
        array_push($familyPeopleB[0]['father'], $lastPerson->father[0]);
        // 处理排行
        $familyPeopleB[0]['ranking'] =  $familyPeopleAHead[count($familyPeopleAHead) - 1]['ranking'];
    }
   // 处理辈分
   $levelDifference = $familyPeopleB[0]['level'] -  $lastPerson->level;
   foreach($familyPeopleB as $key => $person ){
       $familyPeopleB[$key]['level']  =  $person['level'] - $levelDifference;
   }

    // 合并处理好的数据
    $persons = array_merge($familyPeopleAHead, $familyPeopleB, $familyPeopleATail);
  // 创建家族
  list($familyId, $personIdNew, $socialCircleId) = $familyDB->createFamilyAndCircleV3($userId, $familyName, $surname,$desc, $familyPhoto, $photo, $hxChatRoom, $groupType,false);
    // 处理新家族的人物id和index,familyId
    $oldPersonId = array();
    $familyIndex = 0;
    foreach($persons as $key =>  $person){
        $newPersonId =  Util::getNextId();
        $oldPersonId[$person['id']] = $newPersonId;
        $persons[$key]['id'] = $newPersonId;
        $persons[$key]['familyIndex'] = $familyIndex++;
        $persons[$key]['familyId'] = $familyId;
    }
    // var_dump($persons);exit;
    // $mergePersonIdB = $oldPersonId[$familyPeopleB[0]['id']];
    // $oldPersonId = array_flip($oldPersonId);
    $oldPersonId[$personId] = $oldPersonId[$familyPeopleB[0]['id']];

    // 替换所有人物id
    foreach($persons as $key =>  $person){
         if(array() != $person['father']){
             foreach($person['father'] as $key2 =>  $father){
                $persons[$key]['father'][$key2] = $oldPersonId[$father];
             }
         }
         if(array() != $person['spouse']){
            foreach($person['spouse'] as $key2 => $spouse){
                $persons[$key]['spouse'][$key2] = $oldPersonId[$spouse];
            }
        }
        if(array() != $person['mother']){
            foreach($person['mother'] as $key2 => $mother){
                $persons[$key]['mother'][$key2] = $oldPersonId[$mother];
            }
        }
        if(array() != $person['brother']){
            foreach($person['brother'] as $key2 => $brother){
                $persons[$key]['brother'][$key2] = $oldPersonId[$brother];
            }
        }
        if(array() != $person['sister']){
            foreach($person['sister'] as $key2 => $sister){
                $persons[$key]['sister'][$key2] = $oldPersonId[$sister];
            }
        }
        if(array() != $person['son']){
            foreach($person['son'] as $key2 => $son){
                $persons[$key]['son'][$key2] = $oldPersonId[$son];
            }
        }
        if(array() != $person['daughter']){
            foreach($person['daughter'] as $key2 => $daughter){
                $persons[$key]['daughter'][$key2] = $oldPersonId[$daughter];
            }
        }
    }

// exit;

  
    // 添加人物
    $result =  $personDB->batchPersonAdd($persons,$familyId,$userId);
    var_dump($result);
    exit;




    if ($persons != null) {
        $rootY = $persons[0]['level'];
        $rootX = $persons[0]['familyIndex'];
        // 兄弟之间偏移量
        $brotherX = 306;
        // 配偶之间偏移量
        $spouseX = 153;
        // 父子之间偏移量
        $fatherY = 80;
        $lastLevel = $rootY;
        $lastPersonX = 0;
        // 深度
        $deep = 0;
        // 不显示配偶
        if ($filter == 1) {
            foreach ($persons as $key => $person) {
                $persons[$key]['y'] = ($person['level'] - $rootY) * $fatherY;
                //    下一个添加的大于上一个辈分,向下添加
                if ($person['level'] > $lastLevel) {
                    //    向下偏移量
                    $persons[$key]['x'] = $lastPersonX;
                } else {
                    $persons[$key]['x'] = $lastPersonX + $brotherX;
                }
                $lastLevel = $person['level'];
                $lastPersonX = $persons[$key]['x'];
                $persons[$key]['x'] *= $showType;
            }
        } // 显示配偶
        else {
            $spouseArray = array();
            $spouseStartX = 0;
            foreach ($persons as $key => $person) {
                $persons[$key]['y'] = ($person['level'] - $rootY) * $fatherY;
                if ($person['type'] == 2) {
                    $persons[$key]['x'] = $spouseStartX + $spouseX;
                    $spouseStartX += $spouseX;
                    $persons[$key]['x'] *= $showType;
                } // 本家添加
                else {
                    //    下一个添加的大于上一个辈分,向下添加
                    if ($person['level'] > $lastLevel) {
                        //    向下偏移量
                        $persons[$key]['x'] = $lastPersonX;
                    }
                    //    // 同级添加,向上逻辑相同
                    // 向上添加
                    else {
                        $persons[$key]['x'] = $lastPersonX + $brotherX;
                        if (count($spouseArray) > 0) {
                            // while($persons[$key]['level']<=array_key_last($spouseArray)){ //php7.3以前不支持这个方法
                            end($spouseArray);
                            key($spouseArray);
                            while ((count($spouseArray) > 0) && ($persons[$key]['level'] <= key($spouseArray))) {
                                $x = array_pop($spouseArray) + 2;
                                end($spouseArray);
                                key($spouseArray);

                                if ($persons[$key]['x'] <= $x) {
                                    $persons[$key]['x'] = $x + $spouseX;
                                }
                            }
                        }
                    }
                    $lastLevel = $person['level'];
                    $lastPersonX = $persons[$key]['x'];
                    $spouseStartX = $lastPersonX;
                    $persons[$key]['x'] *= $showType;
                    // 当配偶大于1时，给当前辈分添加最大x值
                    if (count($person['spouse']) > 1) {
                        $spouseArray[$person['level']] = $lastPersonX + count($person['spouse']) * $spouseX;
                    }
                }
            }
        }
        $data['persons'] = $persons;
    }


    // $data['result'] = $result;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
    // print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
?>