<?php
/**
 * 删除人物 改动
 * @author:yuanxin
 * @version 3
 */

use DB\CDBEvent;
use DB\CDBFamily;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use Util\Check;
use Util\Util;
use Model\Person;
use Util\HttpContext;
use Util\RedLock\RedLock;
use Config\RedisInstanceConfig;
use Util\SysConst;


$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''));

/**
 * 是否有父母
 * @param $person
 * @return
 */
function hasParent(Person $person): int
{
    return count($person->father) + count($person->mother);
}

/**
 * 是否有孩子
 * @param $person
 * @return
 */
function hasChild(Person $person): int
{
    return count($person->son) + count($person->daughter);
}

function hasSpouse(Person $person): int
{
    return count($person->spouse);
}

try {

    if ($personId != "") {
        $personDB = new CDBPerson();
        $familyDB = new CDBFamily();
        $person = $personDB->getPersonObjectById($personId);

        $userId = $GLOBALS['userId'];

        //检查操作的权限
        $permission = $familyDB->getUserPermission($userId, $person->familyId);
        if (Util::checkPermission($permission,[$person->familyIndex]) < 3){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
//        if (!$personDB->verifyUserIdAndPersonId($userId, $personId)) {
//            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
//            exit;
//        }

              //在删除之前先获取

        if ($person->userId > 0) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "请先将用户和该人物解除绑定");
            exit;
        }

        $familyInfo = $familyDB->getFamilyById($person->familyId);

        if ($familyInfo == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit; 
        }

        //检查人物是否是家族的创始人
        if ($familyDB->isOriginatorForFamily($person->familyId, $person->userId)) {
            //无论如何创始人都是无法直接删除的
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        // 这里要对家族进行加锁，这个锁防止出现脏数据
        $redlockConfig = RedisInstanceConfig::getRedlockConfig();
        $redLock = new RedLock($redlockConfig);
        // 对操作加锁
        $lock = $redLock->lock(SysConst::$FAMILY_LOCK.$familyId);
        if (is_bool($lock) && !$lock) {
            // 没有拿到锁，报错
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '当前正在删除人物请稍后再试');
            exit;
        }

        $familyDB->pdo->beginTransaction();

        //检查人物是否可以被删除
        //能删的情况
        //1. 非核心人物
        //2. 上下代只有一边有人


        //if  familyIndex equals min familyIndex（<= 0 ) , node  can not be deleted
        if ($person->familyIndex <= 0 ){
            $familyDB->pdo->rollback();
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "不能删除顶点结点");
            exit;
        }

        if($person->type == 1){
            if (count($person->daughter) > 0 || count($person->son) > 0){
                $familyDB->pdo->rollback();
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物存在子女，请使用分支删除");
                exit;
            }
        }






        if ($familyInfo->groupType == 1) {

            if (!($person->type == 2 ||
                (hasParent($person) * hasChild($person) == 0 && hasSpouse($person) == 0))
            ) {
                $familyDB->pdo->rollback();
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物存在其他关联，不允许直接删除");
                exit;
            }
        } else if ($familyInfo->groupType == 2) {
            if (!((hasParent($person) * hasChild($person) == 0)))
            {
                $familyDB->pdo->rollback();
                Util::printResult($GLOBALS['ERROR_PERMISSION'], "该人物存在其他关联，不允许直接删除2");
                exit;
            }
        }

        if (!HttpContext::getInstance()->getEnv('private_db_host')) {
            // 删除人物时绑定关系也要删除
            $familyDB->deletePersonBindRecord($personId, $userId);
        }

        $samePersonId = $person->samePersonId;
        //删除之前先更新same_personId
        if($samePersonId  != 0){
                $updateRow = $personDB->updateSamePersonId($samePersonId);
        }

        // 删除人物
        $operation = $personDB->deletePerson3($personId, false);

        // 更新家族辈分
        $levelInfo = $familyDB->getFamilyLevelInfo($person->familyId);
        $level = array();
        foreach ($levelInfo as $i) {
            array_push($level, intval($i['familyLevel']));
        }
        $maxLevel = max($level);
        $minLevel = min($level);
        $familyDB->updateFamilyLevel($person->familyId, $minLevel, $maxLevel);

        $data['operation'] = $operation;

        //前台需要判断是否有修改
        if ($operation) {
            //删除人物相关的大事件
            $CDBEvent = new CDBEvent();

            $CDBEvent->deleteEventByPersonId($personId);

            $familyNewsDB = new CDBFamilyNews();

            $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['DELETEPERSON'], $userId, $personId, $person->name, $userId);

            //删除人物时清除人物的过继关系
            $adoptionList = $personDB->getAdoptionPersonList($personId);
            foreach ($adoptionList as $adoption){
                $delRow = $personDB->deleteAdoptionPersonByPersonId($adoption['src_personId'], $adoption['dest_personId']);
                if ($delRow>0){
                    if (!$personDB->checkSrcPersonIdIsExists($adoption['src_personId'])){
                        $personDB->restoreIsAdoption($adoption['src_personId']);
                    }
                    if (!$personDB->checkDestPersonIdIsExists($adoption['dest_personId'])){
                        $personDB->restoreIsAdoption($adoption['dest_personId']);
                    }
                }
            }

       
            
            
            $familyDB->pdo->commit();

            // 添加人物之后，释放锁
            $redLock->unlock($lock);

            if (!HttpContext::getInstance()->getEnv('private_db_host')) {

                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 3;
                $source = [];
                $CDBPushPerson->setPushListForDeletePerson($modelId, $action, $source);
            }


            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            $familyDB->pdo->rollback();
            Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $familyDB->pdo->rollback();
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
