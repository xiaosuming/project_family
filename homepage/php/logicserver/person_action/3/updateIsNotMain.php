<?php
/**
 * 转换外戚
 * author:liuzhenhao
 * date:2020-07-27
 *
 */

use DB\CDBArea;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBFamily;
use DB\CDBPushPerson;
use Model\Person;
use Util\Check;
use Util\Util;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''),'personId');
$isNotMain = Check::checkInteger(trim(isset($params['isNotMain']) ? trim($params['isNotMain']) : ''),'isNotMain',true,true);

try {

    if ($personId !== "") {
        $personDB = new CDBPerson();
        $familyDB = new CDBFamily();
        $userId = $GLOBALS['userId'];

        $person = $personDB->getPersonObjectById($personId);
        if (!$person){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物不存在");
            exit;
        }

        if (($person->type == 0 && $person->gender == 1) || ($person->type == 1 && $person->gender == 1)){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "修改错误");
            exit;
        }

        //检查操作的权限
        if (Util::checkPermission($familyDB->getUserPermission($userId,$person->familyId,true),[$person->familyIndex])<3){
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }


        $count = $personDB->updateIsNotMainByFamilyIndex($person,$isNotMain,$userId);
        $data['update'] = $count;

        //前台需要判断是否有修改

        if ($count > 0) {
            $person = $personDB->getPersonById($personId);

            $familyNewsDB = new CDBFamilyNews();
            $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['EDITPERSON'], $userId, $personId, $person->name, $userId);

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], "无变化");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
