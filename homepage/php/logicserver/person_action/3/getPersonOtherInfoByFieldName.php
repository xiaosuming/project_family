<?php
/**
 * 按字段名获取人物其他信息
 * author:liuzhenhao
 * date:2021-06-17
 *
 */


use DB\CDBPerson;
use DB\CDBFamily;
use Model\Person;
use Util\Check;
use Util\Util;

$personId = Check::checkInteger(trim(isset($params['personId']) ? $params['personId'] : ''), 'personId');
$fields = Check::checkArray(json_decode(trim($params['fields'] ?? ''), true), 'fields', true);

try {
    if (!$fields) {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $personDB = new CDBPerson();
    $familyDB = new CDBFamily();
    $userId = $GLOBALS['userId'];
    $person = $personDB->getPersonSimpleInfoById($personId);

    if (!$person){
        Util::printResult($GLOBALS['ERROR_SUCCESS'], []);
        exit;
    }

    $family = $familyDB->getFamilyById($person->familyId);

    if ($family == null) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($family->openType == 0) {
        //检查查询的权限
        if (!$familyDB->getUserPermission($userId, $person->familyId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    function camelize($uncamelized_words, $separator = '_')
    {
        $uncamelized_words = $separator . str_replace($separator, " ", strtolower($uncamelized_words));
        return ltrim(str_replace(" ", "", ucwords($uncamelized_words)), $separator);
    }

    $fieldsOtherInfoMap = [];
    $fieldsPdfInfoMap = [];

    $fieldsOtherInfo = ['trash_field'];
    $fieldsPdfInfo = ['title', 'zan', 'zan_type', 'branch_name', 'zan_type', 'origin_name'];
    foreach ($fieldsOtherInfo as $k => $v) {
        $mapValue = camelize($v);
        if (in_array($mapValue, $fields)) {
            $fieldsOtherInfoMap[$v] = $mapValue;
        }
    }
    foreach ($fieldsPdfInfo as $k => $v) {
        $mapValue = camelize($v);
        if (in_array($mapValue, $fields)) {
            $fieldsPdfInfoMap[$v] = $mapValue;
        }
    }

    $otherInfo = $fieldsOtherInfoMap ? $personDB->getPersonOtherInfoByFieldsName($personId, $fieldsOtherInfoMap):[];
    $pdfInfo = $fieldsPdfInfoMap ? $personDB->getPersonPdfTitleByfieldsName($personId, $fieldsPdfInfoMap): [];
    if ($otherInfo === null){
        $otherInfo = [];
        foreach ($fieldsOtherInfoMap as $k=>$v){
            $otherInfo[$v] = null;
        };
    }

    if ($pdfInfo === null){
        $pdfInfo = [];
        foreach ($fieldsPdfInfoMap as $k=>$v){
            $pdfInfo[$v] = null;
        };
    }

    Util::printResult($GLOBALS['ERROR_SUCCESS'], array_merge($otherInfo, $pdfInfo));

} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}



