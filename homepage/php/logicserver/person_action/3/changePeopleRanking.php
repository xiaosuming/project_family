<?php

/**
 * 用户申请加入家族
 * @author liuzhenhao
 * @date 2020-07-23
 */


use Util\Check;
use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use DB\CDBFamilyNews;

$userId = $GLOBALS['userId'];

$personId = Check::checkInteger(trim($params['personId'] ?? ''),'personId');
$rankingList = Check::checkArray(json_decode(trim($params['rankingList'] ?? ''),true),'rankingList',false);
$singleRanking = Check::checkInteger(trim(isset($params['singleRanking']) ? $params['singleRanking'] : 0));

$personDB = new CDBPerson();
$familyDB = new CDBFamily();
$person = $personDB->getPersonSimpleInfoById($personId);

if (!$person){
    Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物不存在");
    exit;
}

//检查操作的权限
$permission = $familyDB->getUserPermission($userId,$person->familyId,true);
if (Util::checkPermission($permission, [$person->familyIndex]) < 3 && $userId != $person->userId) {
    Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
    exit;
}


$ranking =[];
$people = [];
if ($rankingList){
    foreach ($rankingList as $p){
        Check::checkInteger($p['id'] ?? '','id');
        Check::checkInteger($p['ranking'] ?? '','ranking');
        $ranking[] = $p['ranking'];
        $people[] = $p['id'];
    }

    if (!$rankingList){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "rankingList不能为空");
        exit;
    }

    if (sizeof($rankingList) !== sizeof(array_unique($ranking))){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "ranking值有重复");
        exit;
    }
}else{
    if ($singleRanking >= 0 ) {
        $personDB = new CDBPerson();
        $rankingList = $personDB->getChildrenRanking($personId,true,0,true);
        foreach ($rankingList as $k=>$v){
            if ($v['id'] == $personId){
                $people[] = $personId;
                $rankingList[$k]['ranking'] = $singleRanking;
                $rankingList[$k]['id'] = $personId;
                continue;
            }
            $ranking[] = $v['ranking'];
            $people[] = $v['id'];
        }
        if (in_array($singleRanking, $ranking)){
            Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "排行有重复");
            exit;
        }
    }else{
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数错误");
        exit;
    }
}

try {
    $personDB = new CDBPerson();
    $data = $personDB->changePeopleRanking($rankingList,$people,$personId);
//    var_dump($data);

    if ($data === -1){
        Util::printResult($GLOBALS['ERROR_PERSON_EXISTS'], "兄弟姐妹关系错误");
        exit;
    }elseif ($data === -2){
        Util::printResult($GLOBALS['ERROR_PERSON_EXISTS'], "修改失败");
        exit;
    }
//    elseif ($data === 0) {
//        Util::printResult($GLOBALS['ERROR_PERSON_EXISTS'], "修改失败2");
//        exit;
//    }
    $familyNewsDB = new CDBFamilyNews();
    $familyNewsDB->addFamilyNews($person->familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['EDITPERSON'], $userId, $personId, $person->name, $userId);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], '修改成功');
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
