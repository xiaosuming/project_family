<?php
ini_set('memory_limit', '512M');
/**
 * 
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
//queryFamilyPerson($familyId)

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));                //必须
$personId = Check::checkInteger(trim($params['personId'] ?? 0));
$isBranch = Check::checkInteger(trim($params['isBranch'] ?? 0));

$userFamilyId = null;
if ($isBranch == 1) {
    $userFamilyId = Check::checkInteger(trim($params['userFamilyId'] ?? ''));
}
$userId = $GLOBALS['userId'];

try {
    if ($familyId != "") {
        $familyDB = new CDBFamily();

        $family = $familyDB->getFamilyById($familyId);

        if ($family == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        if ($family->openType == 0) {
            // 私有族群检查权限

            if ($isBranch == 0) {
                //检查操作的权限
                if (!$familyDB->isUserForFamily($familyId, $userId) && !$familyDB->isAdminForFamily($familyId, $userId)) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                    exit;
                }
            } else if ($isBranch == 1) {
                //检查通过分支请求的权限
                if (!$familyDB->verifyUserIdAndFamilyIdByBranch($userId, $familyId, $userFamilyId)) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "分支权限错误");
                    exit;
                }
            } else {
                Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "branch参数错误");
                exit;
            }
        }

        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersonsDescByLevel($familyId, null, $personId);
        foreach ($persons as  &$person) {
            if (!empty($person['refFamilyId'])) {
                $person['refFamilyId'] = json_decode($person['refFamilyId'], true);
            }
        }
        $result = array();
        $rank = getRank($persons, $personId);
        $level  =  getLevel($persons, $personId);
        $fatherId = getFatherId($persons, $personId);
        $result =  getBroSis($persons, $personId, $rank, $level, $result);
        $result =  getBroSisAndFatherId($persons, $personId, $rank, $level, $result);

        foreach ($result as &$person) {
            echo ($person['name']);
            echo ("       x :" . $person['x']);
            echo ("y :" . $person['y']);
            echo "<br />";
        }

        $data['persons'] = $result;

        if (count($persons) > 0) {
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "sql查询时发生错误");
        }
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

/**
 * 获取用户的父亲id 没有返回0
 * @param $persons 查询结果集
 * @param $personId 查询的id
 * 
 *  
 */
function getFatherId($persons, $personId)
{
    $fatherId = 0;
    foreach ($persons as &$person) {
        if ($person['id'] == $personId) {
            $fatherId  = $person['father'][0];
            break;
        }
    }
    return $fatherId;
}
/**
 * 获取用户的排行 没有返回0
 * @param $persons 查询结果集
 * @param $personId 查询的id
 * 
 *  
 */
function getRank($persons, $personId)
{
    $rank = 0;
    foreach ($persons as &$person) {
        if ($person['id'] == $personId) {
            $rank  = $person['ranking'];
            break;
        }
    }
    return $rank;
}

/**
 * 获取用户的辈分值 没有返回0
 * @param $persons 查询结果集
 * @param $personId 查询的id
 * 
 *  
 */
function getLevel($persons, $personId)
{
    foreach ($persons as &$person) {
        if ($person['id'] == $personId) {
            $level  = $person['level'];
            break;
        }
    }
    return $level;
}

/**
 * 获取自己的兄弟姐妹和父亲 ，并递归获取父亲的信息 ,
 * @param  persons 数据源
 * @param currentId  当前的人物id
 * @param result 输出的结果
 * @param rank 当前人物的排行
 * @param level 查询人物的辈分
 * 
 * 
 */

function getBroSisAndFatherId($persons, $currentId, $rank, $level, $result)
{
    foreach ($persons as $key => $person) {
        if ($person['id'] == $currentId) {
            $person['x'] = 0;
            $person['y'] = $level  - $person['level'];
            array_push($result, $person);
            unset($persons[$key]);
            if (!empty($person['father'][0])) {
                $fatherId = $person['father'][0];
                $rank  =  getRank($persons, $fatherId);
                $result = getBroSis($persons, $fatherId, $rank, $level, $result);
                $result = getBroSisAndFatherId($persons, $fatherId, $rank, $level, $result);
            }
        }
    }
    return $result;
}
/**
 * 获取自己的兄弟姐妹信息
 * @param  persons 数据源
 * @param currentId  当前的人物id
 * @param result 输出的结果
 * @param rank 当前人物的小家排行
 * @param level 查询人物的辈分
 * 
 * 
 */
function getBroSis($persons, $currentId, $rank, $level, $result)
{
    $x = 1;
    foreach ($persons as $key => $person) {
        if ((in_array($currentId, $person['brother']) || in_array($currentId, $person['sister']))  and $currentId != $person['id']) {
            //   这里会有个小问题 无法处理 有些兄弟设置了排行  有些没有设置
            $person['x'] = $person['ranking'] == 0 ? $x++ :  $person['ranking'] - $rank;
            $person['y'] = $level -  $person['level'];


            array_push($result, $person);
            unset($persons[$key]);
        }
    }
    return $result;
}
