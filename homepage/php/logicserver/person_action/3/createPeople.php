<?php
/**
 * 添加相关人物
 * @author: yuanxin
 *
 */

use DB\CDBArea;
use DB\CDBFamilyNews;
use DB\CDBPerson;
use DB\CDBPushPerson;
use DB\DocumentDB;
use DB\CDBFamily;
use Model\Document;
use Model\Person;
use Util\Check;
use Util\PhotoGenerator;
use Util\Util;
use Util\HttpContext;


$fatherId =  6690186471095141377;

$person = new Person();
$person->id = Util::getNextId();
$person->name ='测试数据'.mt_rand(0,10000000);
$person->zpname = 'zpname';
$person->hxUsername = 'hxUsername'; //环信用户名
$person->type = 1;   //人物类型，1是本家族人物，2是本家族人物的配偶
$person->level;
$person->address = 'address';
$person->birthday = '';
$person->gender=mt_rand(0,1);
$person->photo = 'photo';
$person->phone = 'phone';
$person->bloodType  = 1;      //血型
$person->maritalStatus = 1;
$person->qq = 'qq';
$person->shareUrl='shareUrl';
$person->familyId= 799;
$person->familyName = '大家族测试';
$person->groupType = 1;      // 群组类型
$person->familyPhoto ='familyPhoto';
$person->userId=2;
// $person->father = $fatherId;
// $person->brother;
$person->corePersonId = $fatherId;
$person->branchId = 0;
$person->createBy = 2;
$person->createTime = '2016-11-12 16:47:50';
$person->updateBy = 2;
$person->updateTime ='2016-11-12 16:47:50' ;
$person->isDead = 0;
$person->isAdoption = 0;
$person->samePersonId = 0;       // 标记了过继关系
$person->ranking =  0;            // 兄弟姐妹间的排行
$person->isDelete = 0;
$person->familyIndex= 0;   //人物家族内顺序，即坐标


try {

    if ($name != "" && $gender != "" && $corePersonId != "") {

        // $CDBArea = new CDBArea();
        // $result = $CDBArea->getAreaNameById($country);
        // $countryName = $result['areaName'] ?? '0';
        // $result = $CDBArea->getAreaNameById($province);
        // $provinceName = $result['areaName'] ?? '0';
        // $result = $CDBArea->getAreaNameById($city);
        // $cityName = $result['areaName'] ?? '0';
        // $result = $CDBArea->getAreaNameById($area);
        // $areaName = $result['areaName'] ?? '0';
        // $result = $CDBArea->getAreaNameById($town);
        // $townName = $result['areaName'] ?? '0';

        $personDB = new CDBPerson();     
        $personId = $personDB->addRelatePerson2($person);

        // $person = $personDB->getPersonById2($personId);
        $data['addedPersonId'] = $personId;
        if ($personId > 0) {

            $familyNewsDB = new CDBFamilyNews();
            $familyNewsDB->addFamilyNews($familyId, $GLOBALS['PERSON_MODULE'], $GLOBALS['ADDRELATEPERSON'], $userId, $personId, $name, $userId);
            if (!HttpContext::getInstance()->getEnv('private_db_host')) {
                $CDBPushPerson = new CDBPushPerson();
                $modelId = $personId;
                $action = 1;
                $source = $person;
                $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
            }
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else if ($personId === -1) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错,数据库异常");
        } else if ($personId === -2) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "插入出错，该人物不属于这个家族");
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "未知错误");
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
