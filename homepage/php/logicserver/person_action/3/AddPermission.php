<?php
/**
 * 设置管理员权限
 * @author: yuanixn
 */

use Util\Check;
use Util\Util;
use DB\CDBFamily;

$administratorId = Check::checkInteger($params['administratorId'] ?? '');   // 被设置的管理员的用户id
$permissionType = Check::checkInteger($params['permissionType'] ?? '');  // 被设置的权限等级
$familyId = Check::checkInteger($params['familyId'] ?? '');   // 家族id
$startIndex = Check::checkInteger($params['startIndex'] ?? '');   //  起始Index
$level = Check::checkInteger($params['level'] ?? ''); //起始人物辈份


$userId = $GLOBALS['userId'];

try {
    $familyDB = new CDBFamily();
    // 检查当前用户的该家族权限
    $userPermissionData = $familyDB->getUserPermission($userId,$familyId);

    if($userPermissionData  ==null){
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '家族内没有绑定当前用户');
        exit;
    }

    // 当被设置的等级大于当前用户的等级，或者是总管理员以下的角色（没有赋予权限的等级）
    if (($userPermissionData[0]['permissionType'] <= $permissionType) || $userPermissionData[0]['permissionType']<3) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '权限不足');
        exit;
    }
    if (!$familyDB->isUserForFamily($familyId, $administratorId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '分支管理员必须是家族内成员');
        exit;
    }
 
    $administratorPermissionData = $familyDB->getUserPermission($administratorId,$familyId);
    // 检查用户是否已经是当前分支的管理员
    if ($administratorPermissionData != null) {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '当前用户已经是管理员');
        exit;
    }

    // 去数据库拿到分支内最大下标
    $endIndex = $familyDB->getBranchEndIndex($startIndex,$familyId,$level);
    $permissionId = $familyDB->addPermission($familyId,$userId,$startIndex,$endIndex[0]['familyIndex'],$permissionType,$administratorId);
    
    $data['permissionId'] = $permissionId;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}