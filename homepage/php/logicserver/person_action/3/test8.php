<?php

use DB\CDBFamily;
use DB\CDBPerson;
use Util\Check;
use Util\Util;
try {
    $userId = $GLOBALS['userId'];

//    $personDB = new CDBPerson();
//
//    $person = $personDB->getPersonObjectById($personId);

//    if (!$person){
//        Util::printResult($GLOBALS['ERROR_PERMISSION'], "人物不存在");
//        exit;
//    }
//
//    $familyDB = new CDBFamily();
//
//    $family = $familyDB->getFamilyById($familyId);

//    if ($family->openType == 0) {
//        //检查查询的权限
//        if (!$familyDB->getUserPermission($userId,$familyId)) {
//            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
//            exit;
//        }
//    }

    $data = $personDB->getPersonByLevelForAdopation($familyId, $person , $name,$showNaturalParent,$showAdoption);

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
} catch (PDOException $e) {
    //异常处理
//    print_r($e);
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
