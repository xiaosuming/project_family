<?php
/**
 * 获取允许设置祧子的人。即亲兄弟，姐妹
 * @author: yuanxin
 * @date 2020/08/27
 * 
 *
 */

use DB\CDBPerson;
use Model\Person;
use DB\CDBFamily;
use Util\Util;
use Util\Check;

$familyId = Check::check(trim(isset($params['familyId'])?$params['familyId']:0));
// 承祧   
$personId = Check::checkInteger(trim($params['personId'] ?? 0));


try{
    if( 0  ==  $personId){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    $familyDB = new CDBFamily();
    $familyDetail = $familyDB->getFamilyById($familyId);
    $userId = $GLOBALS['userId'];

    // 权限操作
    // if ($familyDetail->groupType != 1) {
    //     Util::printResult($GLOBALS['ERROR_PERMISSION'], "族群类型不正确，操作权限错误");
    //     exit;
    // }
    $personDB =  new  CDBPerson();
    $person= $personDB->getPersonById2($personId);
    if("" == $personA){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数错误，查不到这个人");
        exit;
    }
    $father = $personDB->getPersonById2($personId);

   
   
   Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}



 ?>