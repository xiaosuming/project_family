<?php
ini_set('memory_limit', '512M');
/**
 *  @author yuanxin
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;

// $refPersons = Check::check(trim(isset($params['refPersons']) ? $params['refPersons'] : ''));                //必须
// $refFamilys = Check::check(trim(isset($params['refFamilys']) ? $params['refFamilys'] : ''));                //必须
// $familyId = Check::check(trim(isset($params['familyId']) ? $params['familyId'] : ''));                //必须
// $refFamilys = Check::check(trim(isset($params['refFamilys']) ? $params['refFamilys'] : ''));                //必须
// $refStartPersonId = Check::check(trim(isset($params['refStartPersonId']) ? $params['refStartPersonId'] : ''));                //必须
// $refPersons = Check::check(trim(isset($params['refPersons']) ? $params['refPersons'] : ''));                //必须
// $familyId = 823;

$options = json_decode($params['options'], true);
// var_dump($options);
$refFamilys = $options['refFamilys'] ?? '';
$refPersons = $options['refPersons'] ?? '';
$refStartPersonId = $options['refStartPersonId'] ?? '';

$personDB = new CDBPerson();
// $familyId =  823;
// $refFamilys = ["816"];
// // $result  = $personDB->getFamilyPersons($familyId);
// $refStartPersonId = ["6694551053905429505"];
// $refPersons = ["6694551128085890049"];
// $persons = $personDB->getMultiFamilyPersons($familyId,$refFamilys,$refStartPersonId,$refPersons);
// foreach($persons as $person){
//     var_dump("id:          ".$person['id']);
//     var_dump("name:          ");
//     var_dump($person['name']);
//     var_dump("son:          ");
//     var_dump($person['son']);
//     var_dump("daughter:          ");
//     var_dump($person['daughter']);
//     var_dump("father:          ");
//     var_dump($person['father']);
//     var_dump("mother:          ");
//     var_dump($person['mother']);

// }
// $refFamilys = array();
// $refPersons = array();
// // 拿出所有reffamily和refPersons
// foreach($result as $person){
//     if($person['refFamilyId']!=''  && $person['refFamilyId'] != null){
//         $temp  =  preg_replace("/\[|]/", "" ,$person['refFamilyId']);
//         array_push($refPersons,$person['id']);
//         array_push($refFamilys,$temp);
//     }
// }


$endIndex = 999999999;
$filter = 0;
$familyId =710;
$corePersonLevel =  0;
$corePersonFamilyIndex = 1;
$personDB = new CDBPerson();
$persons = $personDB->getLastPersonFamilyIndex($familyId,$corePersonLevel,$corePersonFamilyIndex);
var_dump($persons);
exit;


$persons  =  $personDB->getFamilyPersonsByFamilyIndex($familyId,0,$endIndex,$filter=0);
for($i=0 ;$i<count($refPersons);$i ++ ){
    $persons =  $personDB->getMultiFamilyPersons($persons,$refPersons[$i],$refFamilys[$i],$refStartPersonId[$i],0);
}
$newperson = $personDB->modeChange($persons);
var_dump($newperson);

/**
 * 删除子女
 */

function deletePersons($persons,$peopleStack,$spousesStack){
     while($peopleStack!= null){
        $nextPersonId = array_shift($peopleStack);
        foreach($persons as $key => $person){
            if($person['id'] == $nextPersonId){
                $peopleStack= array_merge($peopleStack,$person['son'],$person['daughter']);
                array_merge($spousesStack,$person['spouse']);
                unset($persons[$key]);
            break;
            }
        }
     }
     $data['persons'] = $persons;
     $data['peopleStack'] = $peopleStack;
     $data['spousesStack'] = $spousesStack;
 
     return $data;
}

/**
 * 删除配偶
 */
function deleteSpouses($persons,$peopleStack,$spousesStack){
    while($spousesStack!= null){
       $nextPersonId = array_shift($spousesStack);
       foreach($persons as $key => $person){
           if($person['id'] == $nextPersonId){
               unset($persons[$key]);
           break;
           }
       }
    }
    $data['persons'] = $persons;
    $data['peopleStack'] = $peopleStack;
    $data['spousesStack'] = $spousesStack;

    return $data;
}
?>