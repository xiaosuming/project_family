<?php
ini_set('memory_limit', '512M');
/**
 * 获取家族中的所有本家子女
 * 测试版
 * @author yuanxin　
 */

use Util\Util;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
//use Util\RedLock\RedLock;
//use Config\RedisInstanceConfig;
//use Util\SysConst;
////queryFamilyPerson($familyId)
//
//
//$redlockConfig = RedisInstanceConfig::getRedlockConfig();
//$logger->error(json_encode($redlockConfig));
////if ($GLOBALS['userId']==2401){
////    Util::printResult($GLOBALS['ERROR_SUCCESS'], $redlockConfig);
////}
//$redLock = new RedLock($redlockConfig);
//// 对操作加锁
//$lock = $redLock->lock(SysConst::$FAMILY_LOCK.'0');
//if (is_bool($lock) && !$lock) {
//    // 没有拿到锁，报错
//    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '当前正在添加人物请稍后再试');
//    exit;
//}
//exit;

$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));                //必须
$personId = isset($params['personId']) && intval($params['personId']) > 0 ? intval($params['personId']) : 0;
$isBranch = Check::checkInteger(trim($params['isBranch'] ?? 0));
$userFamilyId = null;
if ($isBranch == 1) {
    $userFamilyId = Check::checkInteger(trim($params['userFamilyId'] ?? ''));
}
$userId = $GLOBALS['userId'];

try {
    if ($familyId != "") {
        $familyDB = new CDBFamily();

        $family = $familyDB->getFamilyById($familyId);

        if ($family == null) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }

        if ($family->openType == 0) {
            // 私有族群检查权限
            if ($isBranch == 0) {
                //检查操作的权限
                if (!$familyDB->isUserForFamily($familyId, $userId) && !$familyDB->isAdminForFamily($familyId, $userId)) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
                    exit;
                }
            } else if ($isBranch == 1) {
                //检查通过分支请求的权限
                if (!$familyDB->verifyUserIdAndFamilyIdByBranch($userId, $familyId, $userFamilyId)) {
                    Util::printResult($GLOBALS['ERROR_PERMISSION'], "分支权限错误");
                    exit;
                }
            } else {
                Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "branch参数错误");
                exit;
            }
        }

        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersons($familyId, null, $personId);


        foreach ($persons as &$person) {
            if (!empty($person['refFamilyId'])) {
                $person['refFamilyId'] = json_decode($person['refFamilyId'], true);
            }
        }
        $result = array();
        $tree = makeTree($persons, $persons[0]['id'], $result);


        // $data['persons'] = $persons;
        $data['persons'] = $tree;


        if (count($persons) > 0) {
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "sql查询时发生错误");
        }
    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

// 将结果转换成需要的树
function makeTree($source, $parentid, $result)
{
    foreach ($source as $key => $item) {
        if ((in_array($parentid, $item['father']) || in_array($parentid, $item['mother'])) and $item['type'] == 1 and $item['gender'] == 0) {
            array_push($result, array_shift($source));
            // $temp =  json_encode($temp);
            $result = getResult($source, $item['id'], $result);
            json_encode($result);
        } else {
            array_shift($source);
        }
    }

    return $result;
}

function getResult($source, $parentid, $result)
{
    foreach ($source as $key => $item) {
        if (in_array($parentid, $item['mother']) || in_array($parentid, $item['father'])) {
            array_push($result, $item);
            unset($source[$key]);
            $result = getResult($source, $item['id'], $result);
        }
    }
    return $result;
}
