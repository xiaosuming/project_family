<?php
$version = $params['version'] ?? "1";
$whiteList['queryFamilyPersonsByShare'] = 'allow';
$whiteList['checkPersonShareCode'] = 'allow';
$whiteList['qrcode_redirect'] = 'allow';
$whiteList['getPersonByShareCode'] = 'allow';
$whiteList['getFamilyChangePersonsByTimeNoLogin'] = 'allow';

require_once("filter/filter.php");
$person_action_file_array['addPerson'] = "logicserver/person_action/{$version}/addPerson.php";
$person_action_file_array['addForPerson'] = "logicserver/person_action/{$version}/addForPerson.php";
$person_action_file_array['addJobForPerson'] = "logicserver/person_action/{$version}/addJobForPerson.php";
$person_action_file_array['addPhoto'] = "logicserver/person_action/{$version}/addPhoto.php";
$person_action_file_array['queryPersonId'] = "logicserver/person_action/{$version}/queryPersonId.php";
$person_action_file_array['editPerson'] = "logicserver/person_action/{$version}/editPerson.php";
$person_action_file_array['editPersonDetail'] = "logicserver/person_action/{$version}/editPersonDetail.php";
$person_action_file_array['deletePerson'] = "logicserver/person_action/{$version}/deletePerson.php";
$person_action_file_array['addRelatePerson'] = "logicserver/person_action/{$version}/addRelatePerson.php";
$person_action_file_array['queryFamilyPerson'] = "logicserver/person_action/{$version}/queryFamilyPerson.php";
$person_action_file_array['queryFamilyPersonSubTree'] = "logicserver/person_action/{$version}/queryFamilyPersonSubTree.php";
$person_action_file_array['queryFamilyPersonSubTreeByPersonID'] = "logicserver/person_action/{$version}/queryFamilyPersonSubTreeByPersonID.php";
$person_action_file_array['queryFamilyPersonPaging'] = "logicserver/person_action/{$version}/queryFamilyPersonPaging.php";
$person_action_file_array['getPersonInfoByPersonId'] = "logicserver/person_action/{$version}/getPersonInfoByPersonId.php";
$person_action_file_array['getFamilyPersonList'] = "logicserver/person_action/{$version}/getFamilyPersonList.php";
$person_action_file_array['getPersonsNameByPersonsId'] = "logicserver/person_action/{$version}/getPersonsNameByPersonsId.php";
$person_action_file_array['getPersonTag'] = "logicserver/person_action/{$version}/getPersonTag.php";
$person_action_file_array['getPersonJob'] = "logicserver/person_action/{$version}/getPersonJob.php";
$person_action_file_array['leaveFamily'] = "logicserver/person_action/{$version}/leaveFamily.php";
$person_action_file_array['getPersonQRCode'] = "logicserver/person_action/{$version}/getPersonQRCode.php";
$person_action_file_array['cancelUserBind'] = "logicserver/person_action/{$version}/cancelUserBind.php";
$person_action_file_array['queryFamilyPersonByBranch'] = "logicserver/person_action/{$version}/queryFamilyPersonByBranch.php";
$person_action_file_array['deletePersonDetail'] = "logicserver/person_action/{$version}/deletePersonDetail.php";
$person_action_file_array['deletePersonJob'] = "logicserver/person_action/{$version}/deletePersonJob.php";
$person_action_file_array['getUserBindPersonList'] = "logicserver/person_action/{$version}/getUserBindPersonList.php";
$person_action_file_array['getPersonsByName'] = "logicserver/person_action/{$version}/getPersonsByName.php";
$person_action_file_array['getPersonsPagingByFilter'] = "logicserver/person_action/{$version}/getPersonsPagingByFilter.php";
$person_action_file_array['checkAuthority'] = "logicserver/person_action/{$version}/checkAuthority.php";

$person_action_file_array['queryFamilyPersonsByShare'] = "logicserver/person_action/{$version}/queryFamilyPersonsByShare.php";
$person_action_file_array['checkPersonShareCode'] = "logicserver/person_action/{$version}/checkPersonShareCode.php";

$person_action_file_array['qrcode_redirect'] = "logicserver/person_action/{$version}/qrcode_redirect.php";
$person_action_file_array['getAtUsers'] = "logicserver/person_action/{$version}/getAtUsers.php";
$person_action_file_array['queryFamilyPersonByMerge'] = "logicserver/person_action/{$version}/queryFamilyPersonByMerge.php";
$person_action_file_array['addAdoptionPerson'] = "logicserver/person_action/{$version}/addAdoptionPerson.php";
$person_action_file_array['movePerson'] = "logicserver/person_action/{$version}/movePerson.php";
$person_action_file_array['updateBrothersAndSistersRanking'] = "logicserver/person_action/{$version}/updateBrothersAndSistersRanking.php";
$person_action_file_array['updateBatchBrothersAndSistersRanking'] = "logicserver/person_action/{$version}/updateBatchBrothersAndSistersRanking.php";
$person_action_file_array['updateUserInfoCardIdInFamily'] = "logicserver/person_action/{$version}/updateUserInfoCardIdInFamily.php";
$person_action_file_array['getInfoCardByPersonId'] = "logicserver/person_action/{$version}/getInfoCardByPersonId.php";
$person_action_file_array['getAdoptionPersonList'] = "logicserver/person_action/{$version}/getAdoptionPersonList.php";
$person_action_file_array['deleteAdoptionPerson'] = "logicserver/person_action/{$version}/deleteAdoptionPerson.php";
$person_action_file_array['getFamilyChangePersonsByTime'] = "logicserver/person_action/{$version}/getFamilyChangePersonsByTime.php";
$person_action_file_array['joinGroup'] = "logicserver/person_action/{$version}/joinGroup.php";
$person_action_file_array['cancelUserBindForTmp'] = "logicserver/person_action/{$version}/cancelUserBindForTmp.php";
$person_action_file_array['getInnerBranchPersons'] = "logicserver/person_action/{$version}/getInnerBranchPersons.php";
$person_action_file_array['getPersonsPagingBySnapshotVersion'] = "logicserver/person_action/{$version}/getPersonsPagingBySnapshotVersion.php";


$person_action_file_array['addPersonForClass'] = "logicserver/person_action/{$version}/addPersonForClass.php";
$person_action_file_array['getClassPersonByPage'] = "logicserver/person_action/{$version}/getClassPersonByPage.php";
$person_action_file_array['getAllClassPerson'] = "logicserver/person_action/{$version}/getAllClassPerson.php";
$person_action_file_array['addPersonForMentor'] = "logicserver/person_action/{$version}/addPersonForMentor.php";
$person_action_file_array['getMentorPersonByPage'] = "logicserver/person_action/{$version}/getMentorPersonByPage.php";
$person_action_file_array['getAllMentorPerson'] = "logicserver/person_action/{$version}/getAllMentorPerson.php";
$person_action_file_array['addPersonForCompany'] = "logicserver/person_action/{$version}/addPersonForCompany.php";
$person_action_file_array['getCompanyPersonByPage'] = "logicserver/person_action/{$version}/getCompanyPersonByPage.php";
$person_action_file_array['getAllCompanyPerson'] = "logicserver/person_action/{$version}/getAllCompanyPerson.php";
$person_action_file_array['addPersonForMiniFamily'] = "logicserver/person_action/{$version}/addPersonForMiniFamily.php";
$person_action_file_array['getMiniFamilyPersons'] = "logicserver/person_action/{$version}/getMiniFamilyPersons.php";
$person_action_file_array['getMiniFamilyPersonsPaging'] = "logicserver/person_action/{$version}/getMiniFamilyPersonsPaging.php";
$person_action_file_array['deleteClassOrCompanyPerson'] = "logicserver/person_action/{$version}/deleteClassOrCompanyPerson.php";
$person_action_file_array['getPersonByFamilyIdAndIM'] = "logicserver/person_action/{$version}/getPersonByFamilyIdAndIM.php";
$person_action_file_array['getPersonByShareCode'] = "logicserver/person_action/{$version}/getPersonByShareCode.php";
$person_action_file_array['userBindPersonAndJoinFamilyByShareCode'] = "logicserver/person_action/{$version}/userBindPersonAndJoinFamilyByShareCode.php";
$person_action_file_array['getUserHasPermissionToEditPersonList'] = "logicserver/person_action/{$version}/getUserHasPermissionToEditPersonList.php";

$person_action_file_array['getFamilyChangePersonsByTimeNoLogin'] = "logicserver/person_action/{$version}/getFamilyChangePersonsByTime.php";


$person_action_file_array['userRemarkFamilyPerson'] = "logicserver/person_action/{$version}/userRemarkFamilyPerson.php";
$person_action_file_array['getUserRemarkPersonPaging'] = "logicserver/person_action/{$version}/getUserRemarkPersonPaging.php";
$person_action_file_array['editUserRemarkPerson'] = "logicserver/person_action/{$version}/editUserRemarkPerson.php";
$person_action_file_array['deleteUserRemarkPerson'] = "logicserver/person_action/{$version}/deleteUserRemarkPerson.php";

$person_action_file_array['addUserRemarkPersonColor'] = "logicserver/person_action/{$version}/addUserRemarkPersonColor.php";
$person_action_file_array['getUserRemarkPersonColorPaging'] = "logicserver/person_action/{$version}/getUserRemarkPersonColorPaging.php";
$person_action_file_array['editUserRemarkPersonColor'] = "logicserver/person_action/{$version}/editUserRemarkPersonColor.php";
$person_action_file_array['deleteUserRemarkPersonColor'] = "logicserver/person_action/{$version}/deleteUserRemarkPersonColor.php";
$person_action_file_array['addAdoptionPersonWithFatherID'] = "logicserver/person_action/{$version}/addAdoptionPersonWithFatherID.php";

$person_action_file_array['getChildrenRanking'] = "logicserver/person_action/{$version}/getChildrenRanking.php";
$person_action_file_array['setBicolor'] = "logicserver/person_action/{$version}/setBicolor.php";
$person_action_file_array['cancelBicolor'] = "logicserver/person_action/{$version}/cancelBicolor.php";

$person_action_file_array['test'] = "logicserver/person_action/{$version}/test.php";
$person_action_file_array['test2'] = "logicserver/person_action/{$version}/test2.php";
$person_action_file_array['test333'] = "logicserver/person_action/{$version}/test333.php";
$person_action_file_array['getFamilyPersonCatenaryMid'] = "logicserver/person_action/{$version}/getFamilyPersonCatenaryMid.php";
$person_action_file_array['test5'] = "logicserver/person_action/{$version}/test5.php";
$person_action_file_array['test6'] = "logicserver/person_action/{$version}/test6.php";
$person_action_file_array['test7'] = "logicserver/person_action/{$version}/test7.php";
$person_action_file_array['changePersonGender'] = "logicserver/person_action/{$version}/changePersonGender.php";
$person_action_file_array['getTwoPointsSource'] = "logicserver/person_action/{$version}/getTwoPointsSource.php";
$person_action_file_array['test11'] = "logicserver/person_action/{$version}/test11.php";

$person_action_file_array['familyPersonMerge'] = "logicserver/person_action/{$version}/familyPersonMerge.php";
$person_action_file_array['getFamilyPersonCatenary'] = "logicserver/person_action/{$version}/getFamilyPersonCatenary.php";
$person_action_file_array['updateFamilyIndex'] = "logicserver/person_action/{$version}/updateFamilyIndex.php";
$person_action_file_array['getFamilyPersonCatenaryNew'] = "logicserver/person_action/{$version}/getFamilyPersonCatenaryNew.php";

$person_action_file_array['checkPersonOrFather'] = "logicserver/person_action/{$version}/checkPersonOrFather.php";
$person_action_file_array['PermissionTest'] = "logicserver/person_action/{$version}/PermissionTest.php";
$person_action_file_array['AddPermission'] = "logicserver/person_action/{$version}/AddPermission.php";
$person_action_file_array['DeletePermission'] = "logicserver/person_action/{$version}/DeletePermission.php";

$person_action_file_array['getFamilyPersonListByLevel'] = "logicserver/person_action/{$version}/getFamilyPersonListByLevel.php";
$person_action_file_array['getPersonPositionByList'] = "logicserver/person_action/{$version}/getPersonPositionByList.php";
$person_action_file_array['changePeopleRanking'] = "logicserver/person_action/{$version}/changePeopleRanking.php";
$person_action_file_array['getSiblingRelationshipPeople'] = "logicserver/person_action/{$version}/getSiblingRelationshipPeople.php";
$person_action_file_array['getPersonByLevelForAdopation'] = "logicserver/person_action/{$version}/getPersonByLevelForAdopation.php";
$person_action_file_array['updatePersonInfo'] = "logicserver/person_action/{$version}/updatePersonInfo.php";
$person_action_file_array['getRefInfo'] = "logicserver/person_action/{$version}/getRefInfo.php";
$person_action_file_array['updateIsNotMain'] = "logicserver/person_action/{$version}/updateIsNotMain.php";
$person_action_file_array['searchFamilyPersonInCatenary'] = "logicserver/person_action/{$version}/searchFamilyPersonInCatenary.php";
$person_action_file_array['createOrUpdatePeoplePdfTitle'] = "logicserver/person_action/{$version}/createOrUpdatePeoplePdfTitle.php";
$person_action_file_array['createOrUpdatePersonOtherInfo'] = "logicserver/person_action/{$version}/createOrUpdatePersonOtherInfo.php";
$person_action_file_array['getPeopleByLevelForApp'] = "logicserver/person_action/{$version}/getPeopleByLevelForApp.php";
$person_action_file_array['test8'] = "logicserver/person_action/{$version}/test8.php";

$person_action_file_array['getPeopleForFileTypeInApp'] = "logicserver/person_action/{$version}/getPeopleForFileTypeInApp.php";
$person_action_file_array['getPeoplePositionByLevelForApp'] = "logicserver/person_action/{$version}/getPeoplePositionByLevelForApp.php";

$person_action_file_array['batchAddRelatePerson'] = "logicserver/person_action/{$version}/batchAddRelatePerson.php";
$person_action_file_array['getPersonOtherInfoByFieldName'] = "logicserver/person_action/{$version}/getPersonOtherInfoByFieldName.php";
