<?php
/**
 * 获取活动详情
 */

use DB\CDBActivity;
use DB\CDBInformation;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];        //操作用户id
$activityId = Check::checkInteger(trim($params['id'] ?? ''));        //活动id

//检查是否存在参数缺少的情况
if ($activityId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {

    $activityDB = new CDBActivity();

    $activity = $activityDB->getActivity($activityId);

    if ($activity == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "活动不存在");
        exit;
    }

    $socialCircleId = $activity->socialCircleId;

    $informationDB = new CDBInformation();
    //检查用户对活动的查看权限
    if (!$informationDB->checkUserFollowExist($userId, $socialCircleId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $activity->content = htmlspecialchars_decode($activity->content);
    $activityId = $activity->id;
    $salt = 'shareActivity';
    $shareCode = md5($activityId . $socialCircleId. $userId . $salt);
    $data['activity'] = $activity;
    $data['shareCode'] = $shareCode;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
