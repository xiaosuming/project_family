<?php
/**
 * 分页获取用户可参加的活动
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-19
 * Time: 下午3:01
 */

use DB\CDBActivity;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

$maxId = Check::checkInteger($params['maxId'] ?? 0); // 最大id
$sinceId = Check::checkInteger($params['sinceId'] ?? 0); //
$count = Check::checkInteger($params['count'] ?? 10);
$socialCircleId = Check::checkInteger($params['socialCircleId'] ?? ''); //圈子id
$userId = $GLOBALS['userId'];

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $activityDB = new CDBActivity();

    $total = $activityDB->getSocialCircleActivedActivityCount($socialCircleId, $userId);

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $activiedActivityList = $activityDB->getSocialCircleActivedActivityPaging($userId, $maxId, $sinceId, $count,$socialCircleId);

    foreach ($activiedActivityList as $k => $activity) {
        $participatorCount = $activityDB->getActivityParticipatorCount($activity['id']);
        $activiedActivityList[$k]['participatorCount'] = $participatorCount;
    }


    $len = count($activiedActivityList);

    $nextSinceId = $activiedActivityList[0]['id'];

    $nextMaxId = $activiedActivityList[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $activiedActivityList, $nextMaxId, $nextSinceId, $lastPage);

    $paginator->printPage();

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
