<?php
/**
 * 更新活动,这个更新策略只能由创建记录的人更新
 */

use DB\CDBActivity;
use DB\CDBZone;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];                                                    //操作用户id

$activityId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));                //要编辑的活动id
$title = Check::check(trim(isset($params['title']) ? $params['title'] : ''), 1, 30);               //活动标题
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''), 1, 500);            //活动内容
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));                //活动配图
$startTime = Check::checkDateTime(trim(isset($params['startTime']) ? $params['startTime'] : ''));    //开始时间
$endTime = Check::checkDateTime(trim(isset($params['endTime']) ? $params['endTime'] : ''));            //结束时间
$deadline = Check::checkDateTime(trim(isset($params['deadline']) ? $params['deadline'] : ''));        //报名截止时间
$updateBy = $userId;                                                                        //更新记录的用户id
$coorX = Check::check(trim(isset($params['coorX']) ? $params['coorX'] : ''));                      //横坐标
$coorY = Check::check(trim(isset($params['coorY']) ? $params['coorY'] : ''));                      //纵坐标
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));
$title = Util::filterContentReplace($title);
$content = Util::filterContentReplace($content);
$address = Util::filterContentReplace($address);


//检查参数是否缺少
if ($activityId == "" || $content == "" || $startTime == "" || $endTime == "" || $deadline == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}
/*检查活动的时间设置是否有问题*/
if (Util::dateCompare($deadline, $startTime) >= 0) {
    Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "报名截止时间不应该迟于活动开始时间");
    exit;
}

if (Util::dateCompare($startTime, $endTime) >= 0) {
    Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "活动开始时间不应该迟于活动结束时间");
    exit;
}

try {

    $activityDB = new CDBActivity();
    //检查当前用户是否有对活动的操作权限(谁建立谁就有权限更新所有属性)
    $activity = $activityDB->getActivity($activityId);
    if ($activity == null) {
        //否则检查操作权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($activity->createBy != $userId) {
        //否则检查操作权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    $rows = $activityDB->updateActivity($activityId, $title, $content, $photo, $startTime, $endTime, $deadline, $coorX, $coorY, $address, $updateBy);
    $data['updateActivityCount'] = $rows;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
