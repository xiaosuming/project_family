<?php
/**
 * 分页获取报名结束的活动
 */

use DB\CDBActivity;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

$maxId = Check::checkInteger($params['maxId'] ?? 0); // 最大id
$sinceId = Check::checkInteger($params['sinceId'] ?? 0); //
$count = Check::checkInteger($params['count'] ?? 10);
$socialCircleId = Check::checkInteger($params['socialCircleId'] ?? ''); //圈子id
$userId = $GLOBALS['userId'];

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $activityDB = new CDBActivity();

    $total = $activityDB->getSocialCircleOverdueActivityCount($socialCircleId, $userId);

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $overdueActivityList = $activityDB->getSocialCircleOverdueActivityPaging($userId, $maxId, $sinceId, $count,$socialCircleId);

    foreach ($overdueActivityList as $k => $activity) {
        $participatorCount = $activityDB->getActivityParticipatorCount($activity['id']);
        $overdueActivityList[$k]['participatorCount'] = $participatorCount;
    }

    $len = count($overdueActivityList);

    $nextSinceId = $overdueActivityList[0]['id'];

    $nextMaxId = $overdueActivityList[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $overdueActivityList, $nextMaxId, $nextSinceId, $lastPage);

    $paginator->printPage();

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
