<?php
/**
 * 获取分享的活动不需要登录
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-28
 * Time: 上午10:04
 */

use DB\CDBActivity;
use Util\Check;
use Util\Util;

$activityId = Check::checkInteger(trim($params['id'] ?? ''));        //活动id
$shareCode = Check::check($params['shareCode'] ?? '');
$fromUserId = Check::checkInteger($params['fromUserId'] ?? '');

//检查是否存在参数缺少的情况
if ($activityId == '' || $shareCode == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {

    $activityDB = new CDBActivity();

    $activity = $activityDB->getActivity($activityId);

    if ($activity == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "活动不存在");
        exit;
    }

    $socialCircleId = $activity->socialCircleId;

    $salt = 'shareActivity';

    $verifyShareCode = md5($activityId . $socialCircleId. $fromUserId . $salt);

    if ($verifyShareCode != $shareCode) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '无法查看活动详情');
        exit;
    }

    $activity->content = htmlspecialchars_decode($activity->content);
    $data['activity'] = $activity;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
