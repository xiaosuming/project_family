<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-29
 * Time: 下午3:28
 */

use DB\CDBActivity;
use DB\CDBFamily;
use DB\CDBInformation;
use Util\Check;
use Util\Util;

$shareCode = Check::check($params['shareCode'] ?? '');
$fromUserId = Check::checkInteger($params['fromUserId'] ?? '');
$activityId = Check::checkInteger(trim($params['id'] ?? ''));        //活动id
$userId = $GLOBALS['userId'];

if ($activityId == '' || $shareCode == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {

    $activityDB = new CDBActivity();

    $activity = $activityDB->getActivity($activityId);

    if ($activity == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "活动不存在");
        exit;
    }

    $socialCircleId = $activity->socialCircleId;

    $salt = 'shareActivity';

    $verifyShareCode = md5($activityId . $socialCircleId . $fromUserId . $salt);

    if ($verifyShareCode != $shareCode) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '无法查看活动详情');
        exit;
    }

    $informationDB = new CDBInformation();
    //检查用户对活动的查看权限
    if ($informationDB->checkUserFollowExist($userId, $socialCircleId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "用户已经关注了圈子");
        exit;
    }

    $familyDB = new CDBFamily();
    $familyId = $familyDB->getFamilyIdBySocialCircleId($socialCircleId);
    if (!$familyId) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '家族信息不存在');
        exit;
    }

    if ($familyDB->acceptTemp2UserJoin($familyId, $socialCircleId, $userId, $fromUserId)) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], '加入家族成功');
        exit;
    }


} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
