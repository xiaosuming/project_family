<?php
/**
 * 添加家族活动
 * @author jiangpengfei
 * date :   2017/04/18
 */

use DB\CDBAccount;
use DB\CDBActivity;
use DB\CDBInformation;
//use DB\DocumentDB;
//use Model\Document;
use Util\Check;
use Util\PostGenerate;
use Util\Util;

$title = Check::check(trim(isset($params['title']) ? $params['title'] : ''), 1, 30);                //活动标题
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''), 1, 500);        //活动内容
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));                    //活动配图
$startTime = Check::checkDateTime(trim(isset($params['startTime']) ? $params['startTime'] : ''));       //开始时间
$endTime = Check::checkDateTime(trim(isset($params['endTime']) ? $params['endTime'] : ''));             //结束时间
$deadline = Check::checkDateTime(trim(isset($params['deadline']) ? $params['deadline'] : ''));          //截止时间
$coorX = Check::check(trim(isset($params['coorX']) ? $params['coorX'] : ''));                      //横坐标
$coorY = Check::check(trim(isset($params['coorY']) ? $params['coorY'] : ''));                      //纵坐标
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));
$userId = $GLOBALS['userId'];
$nowTime = date("Y-m-d H:i:s", time()); //现在时间
$socialCircleId = Check::checkInteger($params['socialCircleId'] ?? ''); // 圈子id
$title = Util::filterContentReplace($title);
$content = Util::filterContentReplace($content);
$address = Util::filterContentReplace($address);

try {

    $informationDB = new CDBInformation();

    //检查用户对活动的查看权限
    if (!$informationDB->checkUserFollowExist($userId, $socialCircleId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
        exit;
    }

    $circleDetail = $informationDB->getInfoCategoryDetail($socialCircleId);

    if (is_null($circleDetail)) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '圈子不存在');
        exit;
    }

    $activityDB = new CDBActivity();

    if ($circleDetail['circleType'] == 5) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '用户不可以向名片圈子发布活动');
        exit;
    }

    if ($circleDetail['circleType'] == 2 && !$activityDB->isSystemUser($userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '该圈子只有系统用户可以发布活动');
        exit;
    }

    //如果是系统用户创建活动，活动type是2
    if ($activityDB->isSystemUser($userId)) {
        $activityType = 2;
    } else {
        //普通用户创建，活动type是1
        $activityType = 1;
    }

    if ($title == '' || $content == '' || $photo == '' || $startTime == '' || $endTime == '' || $deadline == '') {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }

    /*检查活动的时间设置是否有问题*/

    /*活动截止时间跟现在时间比 活动截止时间要大于现在时间*/
    if (Util::dateCompare($deadline, $nowTime) <= 0) {
        Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "活动截止时间不应该迟于现在的时间");
        exit;
    }
    /*活动开始时间跟活动截至时间比较*/
    if (Util::dateCompare($deadline, $startTime) >= 0) {
        Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "活动开始时间不应该迟于活动截止时间");
        exit;
    }
    /*活动结束时间跟活动开始时间的比较*/

    if (Util::dateCompare($startTime, $endTime) >= 0) {
        Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "活动开始时间不应该迟于活动结束时间");
        exit;
    }

    $activityId = $activityDB->addActivity($socialCircleId, $title, $content, $photo, $activityType, $startTime, $endTime, $deadline, $userId, $coorX, $coorY, $address);

    $data['addedActivityId'] = $activityId;

    if ($activityId > 0) {

        $accountDB = new CDBAccount();
        $user = $accountDB->getUserInfo($userId);

        $photos = array();
        array_push($photos, $photo);
        $pathJsonStr = json_encode($photos);
        $content = PostGenerate::generateAddActivityPost($activityId, $title);

        // 普通用户
        if ($user['user_type'] == 1) {
            $type = 2;        //推文类型 1是用户发表，2是活动,3是系统推送,4是发布问题
            $postId = $informationDB->addPost($type, $coorX, $coorY, $address, $userId, $socialCircleId, $pathJsonStr, $content);
            $data['addedPostId'] = $postId;

            // 系统推送用户
        } else if ($user['user_type'] == 2) {
            $type = 2;
            $postId = $informationDB->addSystemPost($type, $coorX, $coorY, $address, $userId, $socialCircleId, $pathJsonStr, $content);
            $data['addedPostId'] = $postId;
        }

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '创建活动失败');
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
