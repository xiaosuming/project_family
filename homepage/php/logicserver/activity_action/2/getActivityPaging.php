<?php
/**
 * 分页获取活动
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-19
 * Time: 下午3:01
 */

use DB\CDBActivity;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

$maxId = Check::checkInteger($params['maxId'] ?? 0); // 最大id
$sinceId = Check::checkInteger($params['sinceId'] ?? 0); //
$count = Check::checkInteger($params['count'] ?? 10);
$socialCircleId = Check::checkInteger($params['socialCircleId'] ?? ''); //圈子id
$userId = $GLOBALS['userId'];

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $activityDB = new CDBActivity();

    $total = $activityDB->getSocialCircleActivityCount($socialCircleId, $userId);

    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $activityList = $activityDB->getSocialCircleActivityPaging($userId, $maxId, $sinceId, $count, $socialCircleId);

    foreach ($activityList as $k => $activity) {
        $participatorCount = $activityDB->getActivityParticipatorCount($activity['id']);
        $activityList[$k]['participatorCount'] = $participatorCount;
    }

    $len = count($activityList);

    $nextSinceId = $activityList[0]['id'];

    $nextMaxId = $activityList[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $activityList, $nextMaxId, $nextSinceId, $lastPage);

    $paginator->printPage();

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
