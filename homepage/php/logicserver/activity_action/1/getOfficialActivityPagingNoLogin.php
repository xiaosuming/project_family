<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-22
 * Time: 下午12:55
 */

use DB\CDBActivity;
use DB\CDBInformation;
use Util\Check;
use Util\Paginator;
use Util\SysLogger;
use Util\Util;

$maxId = Check::checkInteger($params['maxId'] ?? 0); // 最大id
$sinceId = Check::checkInteger($params['sinceId'] ?? 0); //
$count = Check::checkInteger($params['count'] ?? 10);
$userId = $GLOBALS['userId'];

try {

    if ($count <= 0 || $count >= 50) {
        Util::printResult($GLOBALS['ERROR_PARAM_WRONG'], "count参数范围错误");
        exit;
    }

    $activityDB = new CDBActivity();

    $total = $activityDB->getCountOfficialSocialCircleActivityPagingNoLogin();


    $isFinish = false;
    $nextMaxId = $maxId;
    $lastPage = false;
    $isSinceIdEdit = false;

    $activityList = $activityDB->getOfficialSocialCircleActivityPagingNoLogin( $maxId, $sinceId, $count);

    foreach ($activityList as $k => $activity) {
        $participatorCount = $activityDB->getActivityParticipatorCount($activity['id']);
        $activityList[$k]['participatorCount'] = $participatorCount;
    }

    $len = count($activityList);

    $nextSinceId = $activityList[0]['id'];

    $nextMaxId = $activityList[$len - 1]['id'];

    if ($len == 0 || $len >= $total) {
        $nextSinceId = 0;
        $nextMaxId = 0;
        $lastPage = true;
    }

    $paginator = new Paginator($total, $activityList, $nextMaxId, $nextSinceId, $lastPage);

    $paginator->printPage();

} catch (PDOException $e) {
    SysLogger::getInstance()->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], '出现异常');
}
