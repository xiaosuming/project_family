<?php
/**
 * 获取当前时间
 * 在活动的状态判定时，不能依赖用户机器的时间，需要从服务器获取当前时间
 *
 */

 use Util\Util;

 $currentTime = Util::getCurrentTime();
 $data['currentTime'] = $currentTime;
 Util::printResult($GLOBALS['ERROR_SUCCESS'],$data);