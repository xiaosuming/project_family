<?php
/**
 * 分页获取可报名的活动
 */

use Util\Util;
use Util\Pager;
use DB\CDBActivity;
use DB\CDBPerson;
use Util\Check;
use DB\CDBFamily;

$userId = $GLOBALS['userId'];                                                                    //操作用户id
$familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));            //家族id
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1'));        //页码
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '10'));        //每页数量


if ($familyId == "") {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {
    $familyDB = new CDBFamily();

    //如果指定获取某个家族的活动，需要检查用户操作权限
    if ($familyId > 0) {
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit;
        }
    }

    $activityDB = new CDBActivity();
    $total = $activityDB->getActivedActivityCount($familyId, $userId);
    $activities = $activityDB->getActivedActivityPaging($pageIndex, $pageSize, $familyId, $userId);

    $pager = new Pager($total, $activities, $pageIndex, $pageSize);
    $pager->printPage();        //以分页的格式输出
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
