<?php
/**
 * 添加家族活动
 * @author jiangpengfei
 * date :   2017/04/18
 */

use DB\CDBActivity;
use DB\CDBFamily;
use DB\CDBZone;
use DB\DocumentDB;
use Model\Document;
use Model\Post;
use Util\Check;
use Util\PostGenerate;
use Util\Util;
use ThirdParty\MiniProgram;

$title = Check::check(trim(isset($params['title']) ? $params['title'] : ''), 1, 30);                //活动标题
$content = Check::check(trim(isset($params['content']) ? $params['content'] : ''), 1, 500);        //活动内容
$photo = Check::check(trim(isset($params['photo']) ? $params['photo'] : ''));                    //活动配图
$startTime = Check::checkDateTime(trim(isset($params['startTime']) ? $params['startTime'] : ''));       //开始时间
$endTime = Check::checkDateTime(trim(isset($params['endTime']) ? $params['endTime'] : ''));             //结束时间
$deadline = Check::checkDateTime(trim(isset($params['deadline']) ? $params['deadline'] : ''));          //截止时间
$coorX = Check::check(trim(isset($params['coorX']) ? $params['coorX'] : ''));                      //横坐标
$coorY = Check::check(trim(isset($params['coorY']) ? $params['coorY'] : ''));                      //纵坐标
$address = Check::check(trim(isset($params['address']) ? $params['address'] : ''));
$userId = $GLOBALS['userId'];
$nowTime = date("Y-m-d H:i:s", time()); //现在时间
$title = Util::filterContentReplace($title);
$content = Util::filterContentReplace($content);
$address = Util::filterContentReplace($address);

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$title, $content, $address]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    $activityDB = new CDBActivity();
    //如果是系统用户创建活动，活动type是2并且familyId固定是0
    if ($activityDB->isSystemUser($userId)) {
        $type = 2;
        $familyId = 0;

    } else {
        //普通用户创建，活动type是1
        $type = 1;
        $familyId = Check::checkInteger(trim(isset($params['familyId']) ? $params['familyId'] : ''));//家族id

        $familyDB = new CDBFamily();
        //检查操作的权限
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
            exit;
        }
    }

    if ($title != "" && $content != "" && $photo != "" && $startTime != "" && $endTime != "" && $deadline != "") {
        /*检查活动的时间设置是否有问题*/

        /*活动截止时间跟现在时间比 活动截止时间要大于现在时间*/
        if (Util::dateCompare($deadline, $nowTime) <= 0) {
            Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "活动截止时间不应该迟于现在的时间");
            exit;
        }
        /*活动开始时间跟活动截至时间比较*/
        if (Util::dateCompare($deadline, $startTime) >= 0) {
            Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "活动开始时间不应该迟于活动截止时间");
            exit;
        }
        /*活动结束时间跟活动开始时间的比较*/

        if (Util::dateCompare($startTime, $endTime) >= 0) {
            Util::printResult($GLOBALS['ERROR_INPUT_FORMAT'], "活动开始时间不应该迟于活动结束时间");
            exit;
        }

        $activityId = $activityDB->createActivity($familyId, $title, $content, $photo, $type, $startTime, $endTime, $deadline, $userId, $coorX, $coorY, $address);

        $data['addedActivityId'] = $activityId;
        if ($activityId > 0) {
            $zoneDB = new CDBZone();
            $zone = $zoneDB->getZoneByUserId($userId);
            $zoneId = 0;
            if ($zone == null) {
                $zoneId = $zoneDB->addZone($userId, "");
            } else {
                $zoneId = $zone->id;
            }

            //构建数组
            $photos = array();
            array_push($photos, $photo);

            //动作更新到推文
            $post = new Post();
            $post->zoneId = $zoneId;
            $post->userId = $userId;
            $post->content = PostGenerate::generateAddActivityPost($activityId, $title);
            $post->address = "";
            $post->photo = json_encode($photos);
            $post->type = 2;
            $zoneDB->addPost($post, $familyId); //参数2代表系统自动生成,向指定家族推送

            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
        }

    } else {
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
        exit;
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
