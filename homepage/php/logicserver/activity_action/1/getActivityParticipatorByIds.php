<?php
    /**
     * 获取活动所有的参与者
     */	
    use Util\Util;
    use DB\CDBActivity;
    use Util\Check;

    $activityIds = Check::check(trim(isset($params['activityIds']) ? $params['activityIds'] : ''));		//活动id数组


    try{

        $activityIds = explode(",",$activityIds);

        $activityDB = new CDBActivity();
        
        $participators = $activityDB->getActivityParticipatorByIds($activityIds);

        $data['participators'] = $participators;

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }