<?php
/**
 * Created by PhpStorm.
 * User: php
 * Date: 2017/10/11 0011
 * Time: 14:26
 */

use DB\CDBActivity;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
use Util\Pager;
use Util\Util;

$userId = $GLOBALS['userId'];
//获得活动ID
$activityId = Check::checkInteger(trim(isset($params['activityId']) ? $params['activityId'] : ''));
$pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1'));        //页码
$pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '10'));        //每页数量


try {
    $activityDB = new CDBActivity();

    $activity = $activityDB->getActivity($activityId);

    if ($activity == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "活动不存在");
        exit;
    }

    $familyId = $activity->familyId;

    //检查用户对活动的查看权限
    $familyDB = new CDBFamily();

    if ($familyId != 0) {
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit;
        }
    }

    $total = $activityDB->getActivityParticipatorCount($activityId);
    $participatorPage = $activityDB->getActivityParticipatorPage($activityId, $pageIndex, $pageSize);
    $pager = new Pager($total, $participatorPage, $pageIndex, $pageSize);
    $pager->printPage();

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
