<?php
/**
 * 检查参加状态
 * author:jiangpengfei
 * date:2017-06-15
 */
    use Util\Util;
    use DB\CDBActivity;
    use Util\Check;
    $userId = $GLOBALS['userId'];		//操作用户id
    $activityId = Check::checkInteger(trim(isset($params['activityId']) ? $params['activityId'] : ''));		//活动id
    
    //检查是否存在参数缺少的情况
    if($activityId == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    try{
        $activityDB = new CDBActivity();
        $status = $activityDB->checkActivityParticipateStatus($activityId,$userId);     //0是未参加，1是已参加
        $data['status'] = $status;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);	

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }