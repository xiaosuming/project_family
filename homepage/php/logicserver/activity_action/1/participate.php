<?php
/**
 * 参加活动
 * author:jiangpengfei
 * date:2017-06-15
 */

    use Util\Util;
    use DB\CDBActivity;
    use Util\Check;
    $userId = $GLOBALS['userId'];		//操作用户id
    $activityId = Check::checkInteger(trim(isset($params['activityId']) ? $params['activityId'] : ''));		//活动id

    //检查是否存在参数缺少的情况
    if($activityId == ""){
        Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
        exit;
    }

    try{
        $activityDB = new CDBActivity();
        $participateId = $activityDB->participate($activityId,$userId);
        if ($participateId == false){
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'],'截止时间已过');
        }else{
            $data['participateId'] = $participateId;
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
        }

    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
