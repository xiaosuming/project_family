<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-22
 * Time: 下午1:20
 */

use DB\CDBActivity;
use DB\CDBInformation;
use Util\Check;
use Util\Util;

$activityId = Check::checkInteger(trim($params['id'] ?? ''));        //活动id

//检查是否存在参数缺少的情况
if ($activityId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {

    $activityDB = new CDBActivity();

    $activity = $activityDB->getActivity($activityId);

    if ($activity->circleType != 1 && $activity->circleType != 2) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '不是官方创建的活动,无权限');
        exit;
    }

    $activity->content = htmlspecialchars_decode($activity->content);

    $data['activity'] = $activity;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
