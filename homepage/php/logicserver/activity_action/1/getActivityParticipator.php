<?php
/**
 * 获取活动所有的参与者
 */

use DB\CDBActivity;
use DB\CDBPerson;
use DB\CDBFamily;
use Util\Check;
use Util\Util;

//操作用户id
$userId = $GLOBALS['userId'];
$activityId = Check::checkInteger(trim(isset($params['activityId']) ? $params['activityId'] : ''));        //活动id


try {

    $activityDB = new CDBActivity();

    $activity = $activityDB->getActivity($activityId);

    if ($activity == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "活动不存在");
        exit;
    }

    $familyId = $activity->familyId;

    //检查用户对活动的查看权限
    $familyDB = new CDBFamily();

    if ($familyId != 0) {
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit;
        }
    }

    $participator = $activityDB->getActivityParticipator($activityId);

    $participatorCount = $activityDB->getActivityParticipatorCount($activityId);

    $data['participator'] = $participator;
    $data['participatorCount'] = $participatorCount;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
