<?php
/**
 * 获取活动详情
 * 接口弃用
 */

use DB\CDBFamily;
use Util\Util;
use DB\CDBActivity;
use Util\Check;

$userId = $GLOBALS['userId'];        //操作用户id
$activityId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));        //活动id

//检查是否存在参数缺少的情况
if (empty($activityId)) {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {

    $activityDB = new CDBActivity();

    $activity = $activityDB->getActivity($activityId);

    if ($activity == null) {
        Util::printResult($GLOBALS['ERROR_SQL_QUERY'], "活动不存在");
        exit;
    }

    $familyId = $activity->familyId;

    //检查用户对活动的查看权限
    $familyDB = new CDBFamily();

    if ($familyId != 0) {
        if (!$familyDB->isUserForFamily($familyId, $userId)) {
            Util::printResult($GLOBALS['ERROR_PERMISSION'], "权限错误");
            exit;
        }
    }

    $activity->content = htmlspecialchars_decode($activity->content);
    $data['activity'] = $activity;
    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
