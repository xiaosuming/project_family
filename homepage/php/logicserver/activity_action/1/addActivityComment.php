<?php
/**
 * Created by PhpStorm.
 * User: php
 * Date: 2017/10/16 0016
 * Time: 10:11
 */

use DB\CDBActivity;
use Util\Check;
use Util\Util;
use ThirdParty\MiniProgram;

$userId = $GLOBALS['userId'];
$activityId = check::checkInteger(trim(isset($params['activityId']) ? $params['activityId'] : '')); //活动Id
$replyTo = check::checkInteger(trim(isset($params['replyTo']) ? $params['replyTo'] : '-1')); //回复的评论ID
$replyToUserId = check::checkInteger(trim(isset($params['replyToUserId']) ? $params['replyToUserId'] : '-1')); //回复的用户ID
$comment = check::check(trim(isset($params['comment']) ? $params['comment'] : ''), 1, 255); //评论内容
$comment = Util::filterContentReplace($comment);

if ($activityId == '' || $replyToUserId == '' || $replyTo == '' || $comment == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "缺少参数");
    exit;
}

$type = Check::checkInteger($params['type'] ?? 0);
if ($type != 0){
    $wxApp = new MiniProgram($type);
    $checkMsg = $wxApp->msgSecCheck([$comment]);
    if (!$checkMsg){
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "文字内容包含有非法内容，请重新输入");
        exit();
    }
}

try {
    $activityComment = new CDBActivity();

    if ($replyTo == '-1') {
        $path = '0';
    } else {
        $result = $activityComment->selectPath($replyTo);
        if ($result['replyTo'] == '-1') {
            $path = $replyTo . ',';

        } else {
            $path = $result['path'] . $result['id'] . ',';
        }
    }


    $commentId = $activityComment->addActivityComment($activityId, $userId, $replyTo, $replyToUserId, $comment, $path);

    if ($commentId > 0) {
        $data['commentId'] = $commentId;
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], null);
    }

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}

