<?php
/**
 * 删除活动 谁创建的谁可以删除
 */

use Util\Util;
use DB\CDBActivity;
use Util\Check;

$userId = $GLOBALS['userId'];                                                    //操作用户id
$activityId = Check::checkInteger(trim(isset($params['id']) ? $params['id'] : ''));    //要删除的活动id

//先检查参数缺少
if (empty($activityId)) {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], "参数缺失");
    exit;
}

try {
    $activityDB = new CDBActivity();
    //检查当前用户是否有对活动的操作权限(谁建立谁就有权限删除)
    $activity = $activityDB->getActivity($activityId);

    if ($activity == null) {
        //否则检查操作权限
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    if ($activity->createBy != $userId) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限异常");
        exit;
    }

    $rows = $activityDB->deleteActivity($activityId);
    $data['deleteActivityCount'] = $rows;
    if ($rows > 0) {
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);
    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], "删除失败");
    }
} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
