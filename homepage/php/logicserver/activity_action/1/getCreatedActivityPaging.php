<?php
    /**
     * 分页获取创建的活动
     */
    use Util\Util;
    use Util\Pager;
    use DB\CDBActivity;
    use Util\Check;

    $userId = $GLOBALS['userId'];													                //操作用户id
    $pageIndex = Check::checkInteger(trim(isset($params['pageIndex']) ? $params['pageIndex'] : '1'));		//页码
    $pageSize = Check::checkInteger(trim(isset($params['pageSize']) ? $params['pageSize'] : '10'));		//每页数量

    try{
        $activityDB = new CDBActivity();

        $total = $activityDB->getUserCreatedActivityCount($userId);
        $activities = $activityDB->getUserCreatedActivityPaging($pageIndex, $pageSize, $userId);

        $pager = new Pager($total,$activities,$pageIndex,$pageSize);
        $pager->printPage();		//以分页的格式输出
    }catch(PDOException $e){
        //异常处理
        $logger->error(Util::exceptionFormat($e));
        Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
    }
