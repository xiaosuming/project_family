<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/16 0016
 * Time: 14:46
 */

use DB\CDBActivity;
use Util\Check;
use Util\Util;

$userId = $GLOBALS['userId'];
$commentId = check::checkInteger(trim(isset($params['commentId']) ? $params['commentId'] : ''));
if ($commentId == '') {
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'], '参数缺失');
    exit;
}

try {

    $CDBActivity = new CDBActivity();

    if (!$CDBActivity->verifyCommentIdAndUserId($commentId, $userId)) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], "操作权限错误");
        exit;
    }
    $row = $CDBActivity->deleteComment($commentId);

    $data['deleteComments'] = $row;
    if ($row > 0) {
        $CDBActivity->deleteReplyComment($commentId);
        Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

    } else {
        Util::printResult($GLOBALS['ERROR_SQL_DELETE'], null);
    }
} catch (PDOException $e) {
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}