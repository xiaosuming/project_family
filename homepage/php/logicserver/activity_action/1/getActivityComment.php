<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/16 0016
 * Time: 13:34
 */

use DB\CDBActivity;
use Util\Check;
use Util\Util;

$activityId=$activityId = check::checkInteger(trim(isset($params['activityId']) ? $params['activityId'] : '')); //活动Id

if($activityId == ''){
    Util::printResult($GLOBALS['ERROR_PARAM_MISSING'],'缺少参数');
    exit;
}
try{

    $CDBActivity=new CDBActivity();
    $activityComments=$CDBActivity->getActivityComment($activityId);
    Util::printResult($GLOBALS['ERROR_SUCCESS'],$activityComments);

}catch (PDOException $exception){
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}