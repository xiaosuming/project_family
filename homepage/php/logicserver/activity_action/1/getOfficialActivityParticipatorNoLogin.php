<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-4-22
 * Time: 下午1:32
 */

use DB\CDBActivity;
use DB\CDBInformation;
use Util\Check;
use Util\Util;

//操作用户id
$activityId = Check::checkInteger(trim(isset($params['activityId']) ? $params['activityId'] : ''));        //活动id

try {

    $activityDB = new CDBActivity();

    $activity = $activityDB->getActivity($activityId);

    if ($activity->circleType != 1 && $activity->circleType != 2) {
        Util::printResult($GLOBALS['ERROR_PERMISSION'], '不是官方创建的活动,无权限');
        exit;
    }

    $socialCircleId = $activity->socialCircleId;

    $participator = $activityDB->getActivityParticipator($activityId);

    $participatorCount = $activityDB->getActivityParticipatorCount($activityId);

    $data['participator'] = $participator;
    $data['participatorCount'] = $participatorCount;

    Util::printResult($GLOBALS['ERROR_SUCCESS'], $data);

} catch (PDOException $e) {
    //异常处理
    $logger->error(Util::exceptionFormat($e));
    Util::printResult($GLOBALS['ERROR_EXCEPTION'], "出现异常");
}
