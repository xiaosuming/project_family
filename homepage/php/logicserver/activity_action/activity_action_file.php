<?php

$whiteList['getCurrentTime'] = 'allow';
$whiteList['getActivityWithoutLogin'] = 'allow';
$whiteList['getActivityPagingNoLogin'] = 'allow';
$whiteList['getOfficialActivityNoLogin'] = 'allow';
$whiteList['getOfficialActivityParticipatorNoLogin'] = 'allow';
$whiteList['getOfficialActivityPagingNoLogin'] = 'allow';

$version = $params['version'] ?? "1";
require_once("filter/filter.php");
$activity_action_file_array['addActivity'] = "logicserver/activity_action/{$version}/addActivity.php";
$activity_action_file_array['updateActivity'] = "logicserver/activity_action/{$version}/updateActivity.php";
$activity_action_file_array['deleteActivity'] = "logicserver/activity_action/{$version}/deleteActivity.php";
$activity_action_file_array['getActivity'] = "logicserver/activity_action/{$version}/getActivity.php";
$activity_action_file_array['getActivityPaging'] = "logicserver/activity_action/{$version}/getActivityPaging.php";
$activity_action_file_array['getActivedActivityPaging'] = "logicserver/activity_action/{$version}/getActivedActivityPaging.php";
$activity_action_file_array['getOverdueActivityPaging'] = "logicserver/activity_action/{$version}/getOverdueActivityPaging.php";
$activity_action_file_array['getCreatedActivityPaging'] = "logicserver/activity_action/{$version}/getCreatedActivityPaging.php";
$activity_action_file_array['getParticipateActivityPaging'] = "logicserver/activity_action/{$version}/getParticipateActivityPaging.php";
$activity_action_file_array['participate'] = "logicserver/activity_action/{$version}/participate.php";
$activity_action_file_array['cancelParticipate'] = "logicserver/activity_action/{$version}/cancelParticipate.php";
$activity_action_file_array['checkParticipateStatus'] = "logicserver/activity_action/{$version}/checkParticipateStatus.php";
$activity_action_file_array['getActivityParticipator'] = "logicserver/activity_action/{$version}/getActivityParticipator.php";
$activity_action_file_array['getActivityParticipatorByIds'] = "logicserver/activity_action/{$version}/getActivityParticipatorByIds.php";
$activity_action_file_array['getCurrentTime'] = "logicserver/activity_action/{$version}/getCurrentTime.php";
$activity_action_file_array['getActivityParticipatorPaging'] = "logicserver/activity_action/{$version}/getActivityParticipatorPaging.php";
$activity_action_file_array['addActivityComment'] = "logicserver/activity_action/{$version}/addActivityComment.php";
$activity_action_file_array['getActivityComment'] = "logicserver/activity_action/{$version}/getActivityComment.php";
$activity_action_file_array['deleteActivityComment'] = "logicserver/activity_action/{$version}/deleteActivityComment.php";
$activity_action_file_array['getActivityWithoutLogin'] = "logicserver/activity_action/{$version}/getActivityWithoutLogin.php";
$activity_action_file_array['userJoinFamilyByActivityShareCode'] = "logicserver/activity_action/{$version}/userJoinFamilyByActivityShareCode.php";
$activity_action_file_array['getOfficialActivityPagingNoLogin'] = "logicserver/activity_action/{$version}/getOfficialActivityPagingNoLogin.php";
$activity_action_file_array['getOfficialActivityNoLogin'] = "logicserver/activity_action/{$version}/getOfficialActivityNoLogin.php";
$activity_action_file_array['getOfficialActivityParticipatorNoLogin'] = "logicserver/activity_action/{$version}/getOfficialActivityParticipatorNoLogin.php";
