<?php
      /*********************以下是全局变量的定义****************************/

    /**
     * 错误码定义
     * 0代表没有错误
     * -10000开头是SQL错误
     * -20000开头是操作错误
     * -30000开头的是异常错误
     */
    $GLOBALS['ERROR_SUCCESS'] = "0";
    $GLOBALS['ERROR_SQL_INIT'] = "-10000001";                   //SQL数据库初始化错误
    $GLOBALS['ERROR_SQL_INSERT'] = "-10000002";                 //插入出错，没有任何记录插入
    $GLOBALS['ERROR_SQL_DELETE'] = "-10000003";                 //删除出错，没有任何记录被删除
    $GLOBALS['ERROR_SQL_UPDATE'] = "-10000004";                 //更新出错，没有任何记录被更新
    $GLOBALS['ERROR_SQL_QUERY'] = "-10000005";		            //查询出错，记录不存在


    $GLOBALS['ERROR_LOGIN'] = "-20000001";						//登录错误
    $GLOBALS['ERROR_PARAM_MISSING'] = "-20000002";				//参数缺失
    $GLOBALS['ERROR_PERMISSION'] = "-20000003";					//权限错误
    $GLOBALS['ERROR_REQUEST'] = "-20000004";					//请求错误，接口不存在
    $GLOBALS['ERROR_FAMILY_LOGIN'] = "-20000005";				//家族登录出错
    $GLOBALS['ERROR_REGISTER_DUPLICATEEMAIL'] = "-20000006"; 	//注册邮箱重复出错
    $GLOBALS['ERROR_REGISTER'] = "-20000007"; 					//注册出错
    $GLOBALS['ERROR_REGISTER_DUPLICATEUSERNAME'] = "-20000008";	//注册用户名重复出错
    $GLOBALS['ERROR_REGISTER_DUPLICATEPHONE'] = "-20000009";	//注册手机重复出错
    $GLOBALS['ERROR_REGISTER_TYPE'] = "-20000010";				//注册方式不存在出错
    $GLOBALS['ERROR_LOGIN_TYPE'] = "-20000011";					//登录方式不存在出错
    $GLOBALS['ERROR_ZONE_EXIST'] = "-20000012";					//空间已经存在，不允许重复创建
    $GLOBALS['ERROR_INPUT_FORMAT'] = "-20000013";				//输入格式错误
    $GLOBALS['ERROR_COLLECTION_EXIST'] = "-20000014";           //收藏已经存在，不允许重复创建
    $GLOBALS['ERROR_REGISTER_DUPLICATEWECHAT'] = "-20000015";   //微信账号已绑定
    $GLOBALS['ERROR_INSERT_REDIS'] = "-20000016";               //REDIS插入失败
    $GLOBALS['ERROR_ACCOUNT_NOT_EXIST'] = "-20000017";               //账号不存在
    $GLOBALS['ERROR_PARAM_WRONG'] = "-20000018";				//参数不正确
    $GLOBALS['ERROR_PHONE_USETIMES_LIMITED'] = "-20000019";     //电话在24小时中使用了10次
    $GLOBALS['ERROR_FILE_FORMAT'] = "-20000020";                //文件格式错误
    $GLOBALS['ERROR_EXISTS_TASKID'] =  "-20000021";             //悬赏任务不存在
    $GLOBALS['ERROR_DIR_EXISTS'] =  "-20000022";             //目录存在
    $GLOBALS['ERROR_HAVE_UPLOAD'] =  "-20000023";             //重复上传
    $GLOBALS['ERROR_JSON_FORMAT'] =  "-20000024";             //JSON格式错误
    $GLOBALS['ERROR_JSON_VALUE'] =  "-20000025";             //JSON字段错误
    $GLOBALS['ERROR_FILE_SIZE'] =  "-20000026";             //家族存储 超过最大容量值
    $GLOBALS['ERROR_PERSON_EXISTS'] =  "-20000027";             //人物不存在家族中
    $GLOBALS['ERROR_SUBTOKEN'] = "-20000028";                      //子令牌无效
    $GLOBALS['ERROR_OVER_SPEED'] = "-20000029";                      //接口请求超速，需要验证码
    $GLOBALS['ERROR_OVER_LIMIT'] = "-20000030";                     //接口请求超过限制
    $GLOBALS['ERROR_CAPTCHA'] = "-20000031";                     //接口请求超过限制
    $GLOBALS['ERROR_KEYWORD_FILE_NOT_EXISTS'] = '-20000032';     //关键词文件不存在
    $GLOBALS['ERROR_KEYWORD_EMPTY'] = '-20000033';               //关键词未空
    $GLOBALS['ERROR_ENCODING_WRONG'] = '-20000034';              //encoding错误
    $GLOBALS['ERROR_CONTENT_EMPTY'] = '-20000035';               //文章内容是空
    $GLOBALS['ERROR_FILTER_KEYWORD_WRONG'] = '-20000036';        //关键词替换失败

    $GLOBALS['ERROR_EXCEPTION'] = "-30000001";					//出现异常
    $GLOBALS['ERROR_FILE_UPLOAD'] = "-30000002";				//文件上传失败
    $GLOBALS['ERROR_VCODE'] = "-30000003";                      //验证错误
    $GLOBALS['ERROR_WECHAT_ACCESS_TOKEN'] = "-30000004";        //获取微信access_token错误
    $GLOBALS['ERROR_WECHAT_USERINFO'] = "-30000005";            //获取用户信息错误
    $GLOBALS['ERROR_DATA_EXPIRE'] = "-30000006";            //数据有效期错误

    $GLOBALS['RELATION_FATHER'] = "1";			//父亲关系
    $GLOBALS['RELATION_MOTHER'] = "2";			//母亲关系
    $GLOBALS['RELATION_BROTHER'] = "3";			//兄弟关系
    $GLOBALS['RELATION_SISTER'] = "4";			//姐妹关系
    $GLOBALS['RELATION_SPOUSE'] = "5";			//配偶关系
    $GLOBALS['RELATION_SON'] = "6";				//儿子关系
    $GLOBALS['RELATION_DAUGHTER'] = "7";			//女儿关系

    $GLOBALS['GENDER_FEMALE'] = "0";		//女性
    $GLOBALS['GENDER_MALE'] = "1";		//男性
    $GLOBALS['GENDER_UNKNOWN'] = "2";     //未知


    $GLOBALS['REGISTER_TYPE_EMAIL'] = "1";	//通过邮箱注册
    $GLOBALS['REGISTER_TYPE_PHONE'] = "2";	//通过手机注册

    $GLOBALS['LOGIN_TYPE_EMAIL'] = "1";		//通过邮件登录
    $GLOBALS['LOGIN_TYPE_PHONE'] = "2";		//登录手机号登录
    $GLOBALS['LOGIN_TYPE_USERNAME'] = "3";	//通过用户名登录

    $GLOBALS['LOGIN_DEVICE_BROWSER'] = "1";		//浏览器方式登录
    $GLOBALS['LOGIN_DEVICE_APP'] = "2";			//app方式登录
    $GLOBALS['LOGIN_DEVICE_WECHAT'] = "3";		//微信方式登录

    $GLOBALS['LOGIN_FAILED'] = "-1";				//登录失败的值
    $GLOBALS['LOGIN_SUCCESS'] = "1";				//登录成功的值

    $GLOBALS['LOCK_LOGIN_TIMES'] = "5";			//登录失败锁定的次数

    $GLOBALS['SHA256_SALT'] = "adhisugdd";		//加密的salt

    $GLOBALS['WRITE_MODE_ALONE'] = "0";			//单独撰写
    $GLOBALS['WRITE_MODE_UNION'] = "1";			//联合撰写

    $GLOBALS['EVENT_UPDATE_TYPE_INIT'] = "0";			//初始化编辑
    $GLOBALS['EVENT_UPDATE_TYPE_ADD'] = "1";			//增加内容的编辑
    $GLOBALS['EVENT_UPDATE_TYPE_EDIT'] = "2";			//修改内容的编辑
    $GLOBALS['EVENT_UPDATE_TYPE_DELETE'] = "3";		//缩减内容的编辑

    $GLOBALS['SYSTEM_POST'] = "2";                //系统生成的日志
    $GLOBALS['USER_POST'] = "1";                  //用户发表的日志

/***************************************模块的id定义********************************************/
    $GLOBALS['LIKE_MODULE'] = "1";		//点赞
    $GLOBALS['SYSTEM_MODULE'] = "2";		//系统
    $GLOBALS['ORDER_MODULE'] = "3";		//订单
    $GLOBALS['EVENT_MODULE'] = "4";		//大事件
    $GLOBALS['POST_MODULE'] = "5";		//推文
    $GLOBALS['GRAVE_MODULE'] = "6";		//祖坟
    $GLOBALS['COMMENT_MODULE'] = "7";		//评论
    $GLOBALS['PERSON_MODULE'] = "8";		//人物模块
    $GLOBALS['FAMILY_MODULE'] = "9";		//家族模块
    $GLOBALS['USER_MODULE'] = "10";		//用户模块
    $GLOBALS['ACTIVITY_MODULE'] = "11";	//活动模块
    $GLOBALS['QR_MODULE'] = "12";         //二维码模块
    $GLOBALS['ALBUM_MODULE'] = "13";      //相册模块
    $GLOBALS['QUESTION_TASK_MODULE'] = "14";    //提问悬赏模块
    $GLOBALS['FAMILY_CELEBRITY_MODULE'] = "15"; //家族名人模块
    $GLOBALS['FAMILY_HEIRLOOM_MODULE'] = "16"; //家族收藏模块
    $GLOBALS['FEEDBACK_MODULE'] = "17"; //反馈模块
    $GLOBALS['LOOK_FOR_FAMILY_MODULE'] = "18"; //家族寻亲模块
    $GLOBALS['INFO_CARD_MODULE'] = "19"; //名片模块
    $GLOBALS['INFORMATION_MODULE'] = "20"; //资讯模块
    $GLOBALS['MAX_MODULE'] = "20";
    $GLOBALS['INFO_POST_MODULE'] = "21"; //资讯推文模块
    $GLOBALS['AI_IMAGE_MODULE'] = "22"; //资讯推文模块

/**************************************动作的定义*********************************************/

    $GLOBALS['ADDPHOTO'] = "1";			//添加照片
    $GLOBALS['ADDRELATEPERSON'] = "2";	//添加相关人物
    $GLOBALS['DELETEPERSON'] = "3";		//删除人物
    $GLOBALS['EDITPERSON'] = "4";			//编辑人物

    $GLOBALS['ADDADMIN'] = "5";			//添加管理员
    $GLOBALS['RESCINDADMIN'] = "6";		//移除管理员权限
    $GLOBALS['TRANSFERORIGINATOR'] = "7";	//移交创始人身份

    $GLOBALS['ADDEVENT'] = "8";			//添加大事件
    $GLOBALS['DELETEEVENT'] = "9";		//删除大事件
    $GLOBALS['UPDATEEVENT'] = "10";		//更新大事件

    $GLOBALS['INVITETOJOIN'] = "11";      //邀请加入家族
    $GLOBALS['APPLYJOIN'] = "12";         //申请加入家族

    $GLOBALS['MERGE_FAMILY_BY_MARRIAGE'] = '13';         //通过联姻 关联人物
    $GLOBALS['MERGE_FAMILY_BY_PERSON'] = '14';           //通过同一个人  关联人物
    $GLOBALS['MERGE_BRANCH'] = '15';                    //合并分支

    $GLOBALS['MESSAGE_INVITE_REPLY_ACCEPT'] = '16';                    //邀请消息的回复同意
    $GLOBALS['MESSAGE_REPLY_REFUSE'] = '17';                    //消息的回复拒绝

    $GLOBALS['USER_PERSON_BIND'] = '18';                    //用户绑定人物
    $GLOBALS['LOOK_FOR_FAMILY_CONTACT'] = '19';                    //用户寻亲请求联系
    $GLOBALS['MESSAGE_APPLY_REPLY_ACCEPT'] = '20';                    //申请加入家族消息的回复同意
    $GLOBALS['MESSAGE_MERGE_BRANCH_REPLY_ACCEPT'] = '21';                    //分支合并消息的回复同意
    $GLOBALS['MESSAGE_MERGE_FAMILY_REPLY_ACCEPT'] = '22';                    //家族合并消息的回复同意
    $GLOBALS['AT_USER'] = '23';                             //@用户
    $GLOBALS['EXCHANGE_INFOCARD'] = '24';                   // 交换名片
    $GLOBALS['ADD_QUESTION'] = '25';                        // 添加问题
    $GLOBALS['SET_BEST_ANSWER'] = '26';                     // 设置为最佳答案

    $GLOBALS['INVITE_TO_BIND'] = '27';                   #邀请绑定
    $GLOBALS['ADMIN_UNBIND_USER'] = '28';              #管理员解绑用户
    $GLOBALS['APPLY_UNBIND_USER'] = '29';              #申请解绑用户
    $GLOBALS['REPLY_BIND_USER'] = '30';                #绑定人物的回复消息
    $GLOBALS['LEAVE_FAMILY'] = '31';                #离开家族
    $GLOBALS['REPLY_INVITE_BIND'] = '33';                #回复邀请绑定
    $GLOBALS['REPLY_UNBIND_TO_ADMIN'] = '34';                #回复，通知管理员自己主动解绑
    $GLOBALS['REPLY_UNBIND_TO_USER'] = '35';                #回复，管理员解绑通知用户
    $GLOBALS['REPLY_MERGE_FAMILY_BY_MARRIAGE'] = '36';                #回复，姻亲关联的回复
    $GLOBALS['REPLY_MERGE_FAMILY_BY_PERSON'] = '37';                #回复，族谱关联一个人的回复
    $GLOBALS['REPLY_MERGE_FAMILYS_TO_NEW_FAMILY'] = '38';                #回复，合并成一个新的家族的回复

    $GLOBALS['MERGE_FAMILYS_TO_NEW_FAMILY'] = '32';                    //合并两个家族生成新家族
    /**************************************验证码操作的定义****************************************/

    $GLOBALS['AUTH_REGISTER_PHONE'] = "1";                   //注册时手机验证操作
    $GLOBALS['AUTH_REGISTER_EMAIL'] = "2";                   //注册时邮箱验证操作
    $GLOBALS['AUTH_FORGET_PASSWORD_PHONE'] = "3";                   //找回密码时手机验证操作
    $GLOBALS['AUTH_FORGET_PASSWORD_EMAIL'] = "4";                   //找回密码时邮箱验证操作

/*************************************反馈的问题类型*************************************************/
    $GLOBALS['FEEDBACK_IMPROVE'] = "1";              //改善建议
    $GLOBALS['FEEDBACK_SLOW'] = "2";                 //卡顿
    $GLOBALS['FEEDBACK_CRASH'] = "3";                //闪退
    $GLOBALS['FEEDBACK_OTHER'] = "9";                //其他建议

/*************************************反馈状态*************************************************/
    $GLOBALS['FEEDBACK_HANDLED'] = "1";              //已处理
    $GLOBALS['FEEDBACK_UNTREATED'] = "2";                 //未处理
    $GLOBALS['FEEDBACK_HANDLING'] = "3";                //处理中

/*************************************定义二维码生成的合法地址*****************************************/
    $GLOBALS['VALID_URL'] = [
                                "m.izuqun.com",
                                "www.izuqun.com",
                                "app.izuqun.com",
                                "www.xinhuotech.com:9089",
                                "www.xinhuotech.com:9090",
                                "www.xinhuotech.com:9092",
                                "image.izuqun.com"];

/************************************一些限定值 ******************************************************/
    $GLOBALS['MAX_FAMILY_COUNT'] = 5;
    $GLOBALS['MAX_FAMILY_STORAGE'] = 1048576*1024;
    $GLOBALS['MAX_REPORT'] = 5;


/***********************************lua脚本的sha码 **************************************************/
    $GLOBALS['RATE_LIMIT_SHA'] = 'fbe9e95b2645f306b0179c9d38cccdc4d3c649aa';

/***********************************一些加密函数中用到的秘钥********************************************/

    $GLOBALS['INFOCARD_CODE_IV'] = '1323126578645690';   # 用户的接收码生成中用到的iv变量
    $GLOBALS['INFOCARD_SHARE_CODE_KEY'] = 'dnvuhtgntosfhtoa123474hmxdsioplm';
    $GLOBALS['INFOCARD_SHARE_CODE_IV'] = '1587498226884223';
