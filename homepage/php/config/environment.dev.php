<?php
    $GLOBALS['IMGSERVER'] = "http://www.xinhuotech.com:9090/php/index.php";
    $GLOBALS['ROUTER_TYPE'] = "/#";
    $GLOBALS['DOMAIN_NAME'] = "http://www.xinhuotech.com:9092/";
    $GLOBALS['PRIVATE_DB_PROXY'] = "http://test.xinhuotech.com:8797";
    
    /* 定义了根目录位置 */
    $GLOBALS['ROOT_DIR'] = '/var/www/family/php';

    /*************************************一些默认的设置********************************************/
    $GLOBALS['TEST_MODE'] = false;       //测试模式,true的话需要邀请码才能注册,false的话不需要邀请码
    $GLOBALS['TEST_PERSON_TASK'] = true;  //测试模式  true的话可以设置任务队列
    $GLOBALS['TEST_FAMILY_TASK'] = true;  //测试模式  true的话可以设置任务队列
    $GLOBALS['TEST_EVENT_TASK'] = true;  //测试模式  true的话可以设置任务队列
    $GLOBALS['TEST_GRAVE_TASK'] = true;  //测试模式  true的话可以设置任务队列
    $GLOBALS['TEST_INFO_CARD_TASK'] = true; //测试模式 true的话可以设置任务队列
    $GLOBALS['DEBUG_MODE'] = true;          //调试模式

    $GLOBALS['VALID_URL'] = [
        "m.izuqun.com",
        "www.izuqun.com",
        "app.izuqun.com",
        "wiki.izuqun.com",
        "www.xinhuotech.com:9089",
        "www.xinhuotech.com:9090",
        "www.xinhuotech.com:7070",
        "www.xinhuotech.com:9092",
        "localhost:4200",
        "localhost:8080",
        "image.izuqun.com"];