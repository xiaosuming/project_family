<?php
/**
 * redis 实例配置
 * Author: 江鹏飞
 * Date: 2019-01-08
 */

namespace Config;

class RedisInstanceConfig {

    /**
     * 获取存储用的redis实例的配置
     */
    public static function getStorageConfig() {
        return [
            'HOST' => '47.111.172.152',
            'PORT' => '6379',
            'AUTH' => 'flare1111'
        ];
    }

    /**
     * 获取临时存储数据库的配置
     */
    public static function getTempStorageConfig() {
        return [
            'HOST' => '47.111.172.152',
            'PORT' => '6382',
            'AUTH' => 'flare1111'
        ];
    }

    /**
     * 获取验证数据库的配置
     */
    public static function getAuthConfig() {
        return [
            'HOST' => '47.111.172.152',
            'PORT' => '6380',
            'AUTH' => 'flare1111'
        ];
    }

    /**
     * 获取缓存数据库的配置
     */
    public static function getCacheConfig() {
        return [
            'HOST' => '47.111.172.152',
            'PORT' => '6381',
            'AUTH' => 'flare1111'
        ];
    }

    /**
     * 获取redlock的配置
     */
    public static function getRedlockConfig() {
        return [
            [
                '47.111.172.152','6383',0,'flare1111'
            ],
            [
                '47.111.172.152','6384',0,'flare1111'
            ]
        ];
    }
}
