<?php
    $GLOBALS['IMGSERVER'] = "http://www.xinhuotech.com:9090/php/index.php";
    $GLOBALS['ROUTER_TYPE'] = "/#";
    $GLOBALS['DOMAIN_NAME'] = "http://www.xinhuotech.com:9092/";
    $GLOBALS['PRIVATE_DB_PROXY'] = "http://api.izuqun.com:8797";

    /* 定义了根目录位置 */
    $GLOBALS['ROOT_DIR'] = '/home/bonus18/gitfile/family/homepage/php';

    /*************************************一些默认的设置********************************************/
    $GLOBALS['TEST_MODE'] = false;       //测试模式,true的话需要邀请码才能注册,false的话不需要邀请码
    $GLOBALS['TEST_PERSON_TASK'] = true;  //测试模式  true的话可以设置任务队列
    $GLOBALS['TEST_FAMILY_TASK'] = true;  //测试模式  true的话可以设置任务队列
    $GLOBALS['TEST_EVENT_TASK'] = true;  //测试模式  true的话可以设置任务队列
    $GLOBALS['TEST_GRAVE_TASK'] = true;  //测试模式  true的话可以设置任务队列
    $GLOBALS['TEST_INFO_CARD_TASK'] = true; //测试模式 true的话可以设置任务队列
    $GLOBALS['DEBUG_MODE'] = true;          //调试模式

    $GLOBALS['VALID_URL'] = [
        "m.izuqun.com",
        "www.izuqun.com",
        "www.xinhuotech.com:9089",
        "www.xinhuotech.com:9090",
        "localhost:4200",
        "localhost:8080",
        "image.izuqun.com"];



    $GLOBALS['db_host'] = "localhost";
    $GLOBALS["db_name"] = "family";
    $GLOBALS['db_user'] = "root";
    $GLOBALS['db_pwd'] = "666";

    // $GLOBALS['db_host'] = "localhost";
    // $GLOBALS["db_name"] = "family";y
    // $GLOBALS['db_user'] = "debian-sys-maint";
    // $GLOBALS['db_pwd'] = "4GV7wJSbQ6PCIt69";

//    $GLOBALS['redis_host'] = "116.62.25.128";
    $GLOBALS['redis_host'] = 'localhost';//redis
    $GLOBALS['redis_port'] = 6378;
    $GLOBALS['redis_cache'] = "cache";      //缓存数据库
    $GLOBALS['redis_pass'] = "flare1111";   //redis密码

    $GLOBALS['redis_post_delete'] = "zset_post_delete";  //删除推文任务队列
    $GLOBALS['redis_post'] = "zset_post";                //推送推文任务队列
    $GLOBALS['redis_mail'] = "mail";                //发送邮件任务队列
    $GLOBALS['redis_message'] = "message";          //发送短信验证码任务队列
    $GLOBALS['redis_huanxin'] = 'huanxin';          //环信注册队列,1是用户注册，弃用
    $GLOBALS['redis_instant_message'] = 'im_queue';         //即时聊天注册队列
    $GLOBALS['redis_question'] = 'question';        //推送问题任务队列
    $GLOBALS['redis_question_delete'] = 'question_delete'; //删除问题任务队列
    $GLOBALS['redis_info'] = 'zset_info';           // 资讯推送任务
    $GLOBALS['redis_info_delete'] = 'zset_info_delete'; // 资讯删除任务
    $GLOBALS['redis_info_post'] = 'zset_info_post'; //动态推送任务
    $GLOBALS['redis_info_post_delete'] = 'zset_info_post_delete'; //动态推送删除任务


    $GLOBALS['redis_search_put'] = 'redis_search_put'; //搜索索引批量增加任务
    $GLOBALS['redis_search_delete'] = 'redis_search_delete'; //搜索索引批量删除任务

    $GLOBALS['redis_search_db'] = "documentId";
    $GLOBALS['redis_ocr_task'] = "ocr_task";    //ocr任务队列
    $GLOBALS['redis_pdf_task'] = "pdf_task";    //pdf任务队列，弃用
    $GLOBALS['redis_family_pdf_task'] = 'family_pdf_task';


    // rabbitmq的配置
    $GLOBALS['rabbitmq_ip'] = '47.111.172.152';
    $GLOBALS['rabbitmq_port'] = 35972;
    $GLOBALS['rabbitmq_user'] = 'xinhuo_test';
    $GLOBALS['rabbitmq_pass'] = 'xinhuotest111';
