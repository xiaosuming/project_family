<?php

namespace Config;

class IPMap {
    private $ipmap;
    private static $instance;

    private function __construct() {
        $this->ipmap['116.62.25.128'] = "img1.izuqun.com";
        $this->ipmap['xinhuo-image-storage-1256125487.file.myqcloud.com'] = "image.izuqun.com";
        $this->ipmap['xinhuo-image-storage-1256125487.cossh.myqcloud.com'] = "image.izuqun.com";
        $this->ipmap['xinhuo-image-storage-1256125487.cos.ap-shanghai.myqcloud.com'] = "image.izuqun.com";
        $this->ipmap['xinhuo-storage-1256125487.cos.ap-shanghai.myqcloud.com'] = "storage.izuqun.com";
        $this->ipmap['xinhuo-storage-1256125487.cossh.myqcloud.com'] = "storage.izuqun.com";
        
    }

    public static function getInstance() {
        
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }
        
        return self::$instance;
    }

    /**
     * 将第三方的域名或ip转为公司自己的域名
     * @param $ipOrDomain ip或domain
     * @return string 公司域名
     */
    public function getCompanyDomain($ipOrDomain) {

        if (isset($this->ipmap[$ipOrDomain])) {
            return $this->ipmap[$ipOrDomain];
        } else {
            return null;
        }
        
    }
}

