<?php
/**
 * 腾讯云通讯的集成封装
 * author: jiangpengfei
 * date: 2018-02-27
 */
namespace ThirdParty;

use ThirdParty\TimRestAPI;
use ThirdParty\TimUtil;
use Util\TempStorage;
use Util\SysLogger;

class TencentIM implements IInstantMessage
{
    private $_timApi = null;
    private $_tempStorage = null;
    private $_REDIS_KEY = "tim_user_sig";
    private $_identifier = null;
    private $_appid = null;
    private $_adminUserSig = null;

    public function __construct()
    {
        $this->_identifier = $GLOBALS['tencent_im_identifier'];
        $this->_appid = $GLOBALS['tencent_im_appid'];
        $this->_timApi = new TimRestAPI();
        $this->_timApi->init($this->_appid, $this->_identifier);
    }

    /**
     * 初始化函数，当且仅当需要使用系统管理员账号时才需要调用
     * 
     * @return none
     */
    private function _init()
    {
        if ($this->_adminUserSig === null) {
            $this->_adminUserSig = $this->getUserSig($this->_identifier);

            if ($this->_adminUserSig && $this->_adminUserSig != -1) {
                $this->_timApi->set_user_sig($this->_adminUserSig);      //为timApi设置用户签名
            } else {
                $this->_adminUserSig = $this->signature($this->_identifier);
                $this->saveUserSig($this->_identifier, $this->_adminUserSig);
                $this->_timApi->set_user_sig($this->_adminUserSig);
            }
        }
    }

    /**
     * 根据用户名生成签名
     * 
     * @param string $userIdentifier 用户名
     * 
     * @return mix -1生成失败,string签名字符串
     */
    public function signature($userIdentifier)
    {
        $private_pem_path = $GLOBALS['ROOT_DIR']."/third_party/tencent/signature/private_key";
            
        if (!file_exists($private_pem_path)) {
            SysLogger::getInstance()->error("私钥文件不存在");
            return -1;
        }

        if (TimUtil::is_64bit()) {
            if (PATH_SEPARATOR==':') {
                $signature = $GLOBALS['ROOT_DIR']."/third_party/tencent/signature/linux-signature64";
            } else {
                $signature = "signature\\windows-signature64.exe";
            }
        } else {
            if (PATH_SEPARATOR==':') {
                $signature = "signature/linux-signature32";
            } else {
                $signature = "signature\\windows-signature32.exe";
            }
        }

        $ret = $this->_timApi->generate_user_sig($userIdentifier, '36000', $private_pem_path, $signature);
        if ($ret == null || strstr($ret[0], "failed")) {
            SysLogger::getInstance()->error("获取usrsig失败");
            return -1;
        } else {
            //获取usersig成功,$ret[0]就是sig值，保存起来，有效期是36000
            return $ret[0];
        }
    }

    /**
     * 向redis中保存user sig
     * @return bool 保存成功或失败
     */
    public function saveUserSig($userKey,$userSig)
    {
        if ($this->_tempStorage === null) {
            $this->_tempStorage = new TempStorage();
        }

        return $this->_tempStorage->setTemp($userKey, $userSig, 35950);
    }

    /**
     * 从redis中取出user sig
     * @return mix token或者false(false代表不存在)
     */
    public function getUserSig($userKey)
    {
        if ($this->_tempStorage === null) {
            $this->_tempStorage = new TempStorage();
        }

        return $this->_tempStorage->get($userKey);
    }

    /**
     * 创建用户
     * @param $username 用户名
     * @param $password 无用
     * @return bool true代表成功，false代表失败
     */
    public function createUser($username, $password, $face_url = null)
    {
        $this->_init();
        $identifier = $username;
        $nick = 'u'.$username;
        if ($face_url === null) {
            $face_url = "https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmH_-AIYTLAAAHLtDFCbg683.jpg";
        }
        $result = $this->_timApi->account_import($identifier, $nick, $face_url);

        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("创建用户失败", $result);
            return false;
        }
    }

    /**
     * 创建聊天室
     * @param name 聊天室名称
     * @param description 聊天室描述
     * @param maxusers 聊天室成员最大数
     * @param owner 聊天室的管理员
     * @param members 聊天室的成员
     * @return mix 创建成功为聊天室Id，创建失败为false
     */
    public function createChatRoom(string $name, string $description, int $maxusers, string $owner, array $members)
    {
        $this->_init();
        $group_type = 'ChatRoom';
        $group_name = $name;
        $owner_id = $owner;
        $info_set['introduction'] = $description;
        $info_set['max_member_num'] = $maxusers;

        $mem_list = array();

        foreach ($members as $member) {
            $account['Member_Account'] = $member;
            $mem_list[] = $account;
        }



        $result = $this->_timApi->group_create_group2($group_type, $group_name, $owner_id, $info_set, $mem_list);
        if ($result['ActionStatus'] === 'OK') {
            return $result['GroupId'];
        } else {
            SysLogger::getInstance()->error("创建聊天室失败", $result);
            return false;
        }
    }

    /**
     * 删除聊天室
     * @param $chatroomId 聊天室id
     * @return bool 成功返回true，失败返回false
     */
    public function deleteChatRoom($chatroomId)
    {
        $this->_init();
        $result = $this->_timApi->group_destroy_group($chatroomId);

        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("删除聊天室失败", $result);
            return false;
        }
    }

    /**
     * 添加聊天室成员
     * @param $chatroom 聊天室
     * @param $username 用户名
     * @return bool false失败,true成功
     */
    public function addUserToChatRoom($chatroom, $username)
    {
        $this->_init();
        $result = $this->_timApi->group_add_group_member($chatroom, $username, 0);

        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("添加用户到聊天室失败", $result);
            return false;
        }
    }

    /**
     * 发送消息给单个用户
     * @param $sendUsers 发送的用户数组
     * @param $msgJson   消息内容,字符串类型
     * @param $ext       消息的扩展字段,是一个map
     * @return bool true代表成功，false代表失败
     */
    public function sendTextMessageToUser($sendUsers, $msgJson, $ext = null)
    {
        $this->_init();
        $account_list = $sendUsers;
        $content['msg'] = $msgJson;

        if ($ext != null) {
            $content['ext'] = $ext;
        }

        $result = $this->_timApi->openim_batch_sendmsg($account_list, json_encode($content));

        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("发送消息失败", $result);
            return false;
        }
    }

    public function sendCustomMessageToUser($sendUsers, $msg, $data)
    {
        $this->_init();
        $account_list = $sendUsers;
 
        $result = $this->_timApi->openim_batch_sendmsg_custom($account_list, $msg, $data);

        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("发送消息失败", $result);
            return false;
        }
    }

    /**
     * 创建群组
     * @param groupName 群组名称
     * @param description 聊天室描述
     * @param maxusers 聊天室成员最大数
     * @param owner 聊天室的管理员
     * @param members 聊天室的成员
     * @param isPublic 是否公开
     * @param needPermit 加入群是否需要群主或者群管理员审批，默认是false
     * @param allowInvites 是否允许群成员邀请别人加入此群。 true：允许群成员邀请人加入此群，false：只有群主或者管理员才可以往群里加人。
     * @return mix 创建成功为聊天室Id，创建失败为false
     */
    public function createChatGroup(
        string $groupName,
        string $description,
        int $maxusers,
        string $owner,
        array $members,
        bool $isPublic,
        bool $needPermit,
        bool $allowInvites,
        $customInfo = null
    ) {
        $this->_init();
        if (!$isPublic) {
            $group_type = 'Private';
        } else {
            $group_type = 'Public';
        }
        
        $group_name = $groupName;
        $owner_id = $owner;
        $info_set['introduction'] = $description;
        $info_set['max_member_num'] = $maxusers;
        foreach ($members as $member) {
            $account['Member_Account'] = $member;
            $mem_list[] = $account;
        }

        if ($customInfo == null) {
            $result = $this->_timApi->group_create_group2($group_type, $group_name, $owner_id, $info_set, $mem_list);
        } else {
            $result = $this->_timApi->group_create_group3($group_type, $group_name, $owner_id, $info_set, $mem_list, $customInfo);
        }

        
        if ($result['ActionStatus'] === 'OK') {
            return $result['GroupId'];
        } else {

            SysLogger::getInstance()->error("创建群聊失败", $result);
            return false;
        }
    }

    /**
     * 加入黑名单
     * @param $ownerUser
     * @param $blockUsers
     * @return bool 成功true,失败false
     */
    public function blockUser($ownerUser, $blockUsers)
    {
        $this->_init();
        $result =  $this->_timApi->black_list_add($ownerUser, $blockUsers);

        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("拉黑名单失败", $result);
            return false;
        }
    }

    /**
     * 从黑名单中删除用户
     * @param $ownerUser
     * @param $blockUser
     * @return bool 成功true失败false
     */
    public function deleteBlockUser($ownerUser, $blockUser)
    {
        $this->_init();
        $result =  $this->_timApi->black_list_delete($ownerUser, $blockUser);
        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("删除黑名单失败", $result);
            return false;
        }
    }
/**
    * 修改用户的昵称
    * @param $userIdentity 用户的标识
    * @param $nickname   用户昵称
    */
    public function updateNickName($userIdentity, $nickname) {
        $this->_init();
        $result = $this->_timApi->profile_portrait_set($userIdentity, $nickname);

        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("更新用户昵称失败", $result);
            return false;
        }
    }

    /**
     * 管理员指定账号向另外的账号发送消息
     * @param $user1 账号1
     * @param $user2 账号2
     * @param $sync  是否同步到终端
     * @param $content 消息内容 
     * @return bool 执行成功还是失败
     */
    public function sendMsgFromOneToAnother($user1, $user2, $sync, $content) {
        $this->_init();
        
        $msgBody = [
            "MsgType" => "TIMTextElem",
            "MsgContent" => [
                "Text" => $content
            ]
        ];

        // $account_id, $receiver, $msg_content
        $result = $this->_timApi->openim_send_msg2_sync($user1, $user2, $sync, [$msgBody]);

        if ($result['ActionStatus'] === 'OK') {
            return true;
        } else {
            SysLogger::getInstance()->error("名片私信发送单聊消息失败", $result);
            return false;
        }
    }
}
