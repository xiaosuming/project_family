<?php
/**
 * 环信的集成
 * @author jiangpengfei
 * @date   2017-09-06
 */
namespace ThirdParty;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;
use Util\TempStorage;
use Util\Http;
use Util\Util;

class Huanxin{
    const AUTH_ERROR_CODE = '400';                  //一般情况下都是用户名或者密码错误造成的验证错误
    const TOKEN_ERROR_CODE = '401';                 //一般情况下都是因为token错误
    const RESOURCE_NOT_FOUND_ERROR_CODE = "404";    //资源不存在
    const REQUEST_TOO_LARGE_ERROR_CODE = "413";     //请求过大
    const OVERSPEED_ERROR_CODE = "429";             //请求过快
    const REQUEST_ERROR_CODE = "500";               //请求出错
    const HUANXIN_TOKEN_KEY = 'huanxin_token';      //环信token的临时存储key

    private $client_id = null;
    private $client_secret = null;
    private $token = null;
    private $token_expiration = 5183980;    //环信的过期时间是5284000,这里默认的要少20秒，是为了防止失败重新获取带来更多的性能损失
    private $tempStorage = null;
    private $http = null;
    private $server = null;
    private $logger = null;
    private $time_start = null;

    public function __construct($client_id,$client_secret){
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->http = new Http();
        $this->server = $GLOBALS['hx_server'] . "/" . $GLOBALS['hx_orgname'] . "/" . $GLOBALS['hx_appname'];
        $this->logger = new Logger("logger");         //用来记录环信登录，注册用户等操作日志
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $path = __DIR__ . "/log/$year/$month/$day/";
        if(!file_exists($path)){
            $dirs = explode('/', $path);
            $dir='';
            foreach ($dirs as $part) {
                $dir.=$part.'/';
                if (!is_dir($dir) && strlen($dir)>0)
                    mkdir($dir, 0777);
            }
    
        }
        $this->logger->pushHandler(new StreamHandler($path."/huanxin_error.log", Logger::ERROR));
        $this->logger->pushHandler(new StreamHandler($path."/huanxin_info.log", Logger::INFO));

        // $raven_client = new \Raven_Client('http://06782336cd9d452694ad6178ea2c1bfd:e8fb2c80a83648008fd10f22d1d520ab@www.xinhuotech.com:8080/2');
        // $this->logger = new Logger("logger");         //用来记录全局的异常警告日志
        // $handler = new \Monolog\Handler\RavenHandler($raven_client);
        // $handler->setFormatter(new \Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));

        // $this->logger->pushHandler($handler);

        $this->time_start = Util::getMicroTime();
    }

    /**
     * 向redis中保存token
     * @return bool 保存成功或失败
     */
    private function saveToken(){
        if($this->tempStorage === null){
            $this->tempStorage = new TempStorage();
        }

        if($this->token === null)
            return false;

        return $this->tempStorage->setTemp(Huanxin::HUANXIN_TOKEN_KEY,$this->token,$this->token_expiration);
    }

    /**
     * 从redis中取出token
     * @return mix token或者false(false代表不存在)
     */
    private function getToken(){
        if($this->tempStorage === null){
            $this->tempStorage = new TempStorage();
        }

        return $this->tempStorage->get(Huanxin::HUANXIN_TOKEN_KEY);
    }

    /**
     * 登录环信的服务器
     * @return bool 登录成功或失败
     */
    private function login(){
        $url = $this->server . "/token";

        $this->http->setUrl($url);
        $this->http->setTimeout(3000);
        $postData['grant_type'] = 'client_credentials';
        $postData['client_id'] = $this->client_id;
        $postData['client_secret'] = $this->client_secret;
        $this->http->setParams(json_encode($postData));

        $header[] = 'Content-Type:application/json';
 
        $response = $this->http->postWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("login error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            $data = json_decode($response['data'],true);
            //记录本次登录日志
            $this->logger->info("login success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            $this->token = $data['access_token'];
            $this->token_expiration = $data['expires_in'] - 20;
            //存储到redis中
            $this->saveToken();
            return true;
        }
        
    }

    /**
     * 创建用户
     * @param $username 用户名
     * @param $password 密码
     * @param $face_url 头像地址
     * @return bool 成功true，失败false
     */
    public function createUser($username,$password,$face_url = null){
        if($this->token === null){
            $this->token = $this->getToken();   //先从redis中取
            if(!$this->token){
                //登录
                if(!$this->login()){
                    return false;
                }
            }
        }

        $url = $this->server . "/users";
        $this->http->setUrl($url);
        $this->http->setTimeout(3000);
        $postData['username'] = $username;
        $postData['password'] = $password;
        $this->http->setParams(json_encode($postData));

        $header[] = 'Content-Type:application/json';
        $header[] = 'Authorization:Bearer '.$this->token;
        $response = $this->http->postWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("create user error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            //记录注册用户的日志
            $this->logger->info("create user success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return true;
        }
    }

    /**
     * 创建聊天室
     * @param name 聊天室名称
     * @param description 聊天室描述
     * @param maxusers 聊天室成员最大数
     * @param owner 聊天室的管理员
     * @param members 聊天室的成员 
     * @return mix 创建成功为聊天室Id，创建失败为false
     */
    public function createChatRoom(string $name,string $description,int $maxusers,string $owner,array $members){
        if($this->token === null){
            $this->token = $this->getToken();   //先从redis中取
            if(!$this->token){
                //登录
                if(!$this->login()){
                    return false;
                }
            }
        }


        $url = $this->server . "/chatrooms";
        $this->http->setUrl($url);
        $this->http->setTimeout(3000);
        $postData['name'] = $name;
        $postData['description'] = $description;
        $postData['maxusers'] = $maxusers;
        $postData['owner'] = $owner;
        if(count($members) > 0)
            $postData['members'] = $members;
        $this->http->setParams(json_encode($postData));

        $header[] = 'Content-Type:application/json';
        $header[] = 'Authorization:Bearer '.$this->token;
        $response = $this->http->postWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("create chatroom error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            //记录注册聊天室的日志
            $this->logger->info("create chatroom success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            $result = json_decode($response['data'],true);
            return $result['data']['id'];
        }
    }

    /**
     * 删除聊天室
     * 
     */
    public function deleteChatRoom($chatroomId){
        if($this->token === null){
            $this->token = $this->getToken();   //先从redis中取
            if(!$this->token){
                //登录
                if(!$this->login()){
                    return false;
                }
            }
        }

        $url = $this->server . "/chatrooms/$chatroomId";
        $this->http->setUrl($url);
        $this->http->setTimeout(3000);

        $header[] = 'Content-Type:application/json';
        $header[] = 'Authorization:Bearer '.$this->token;
        $response = $this->http->deleteWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("create chatroom error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            //记录删除聊天室的日志
            $this->logger->info("delete chatroom success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return true;
        }
    }

    /**
     * 添加聊天室成员
     * @param $chatroom 聊天室
     * @param $username 用户名
     * @return bool false失败,true成功
     */
    public function addUserToChatRoom($chatroom,$username){
        if($this->token === null){
            $this->token = $this->getToken();   //先从redis中取
            if(!$this->token){
                //登录
                if(!$this->login()){
                    return false;
                }
            }
        }

        $url = $this->server . "/chatrooms/$chatroom/users/$username";
        $this->http->setUrl($url);
        $this->http->setTimeout(3000);

        $header[] = 'Content-Type:application/json';
        $header[] = 'Authorization:Bearer '.$this->token;
        $response = $this->http->postWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("add user to chatroom error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            //记录添加用户到聊天室的日志
            $this->logger->info("add user to chatroom success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return true;
        }
    }

    /**
     * 发送消息给单个用户
     * @param $sendUsers 发送的用户数组
     * @param $msgJson   消息内容,为json对象
     * @param $ext       消息的扩展字段,是一个map
     * @return bool true代表成功，false代表失败
     */
    public function sendTextMessageToUser($sendUsers,$msgJson,$ext = null){
        if($this->token === null){
            $this->token = $this->getToken();   //先从redis中取
            if(!$this->token){
                //登录
                if(!$this->login()){
                    return false;
                }
            }
        }

        $adminUsername = $GLOBALS['hx_admin'];

        $request = array();
        $request['target_type'] = 'users';
        $request['target'] = $sendUsers;

        $msg = array();
        $msg['type'] = "txt";
        $msg['msg'] = $msgJson;
        $request['msg'] = $msg;

        $request['from'] = $adminUsername;

        if($ext != null){
            $request['ext'] = $ext;
        }

        $url = $this->server . "/messages";
        $this->http->setParams(json_encode($request));
        $this->http->setUrl($url);
        $this->http->setTimeout(3000);

        $header[] = 'Content-Type:application/json';
        $header[] = 'Authorization:Bearer '.$this->token;
        $response = $this->http->postWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("send text message to user error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            //记录发送消息成功的日志
            $this->logger->info("send test message to user success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return true;
        }
    }

    /**
     * 创建群组
     * @param groupName 群组名称
     * @param description 聊天室描述
     * @param maxusers 聊天室成员最大数
     * @param owner 聊天室的管理员
     * @param members 聊天室的成员 
     * @param isPublic 是否公开
     * @param needPermit 加入群是否需要群主或者群管理员审批，默认是false
     * @param allowInvites 是否允许群成员邀请别人加入此群。 true：允许群成员邀请人加入此群，false：只有群主或者管理员才可以往群里加人。
     * @return mix 创建成功为聊天室Id，创建失败为false
     */
    public function createChatGroup(string $groupName,string $description,int $maxusers,
    string $owner,array $members,bool $isPublic,bool $needPermit,bool $allowInvites,$customInfo = null
                                    ){
        if($this->token === null){
            $this->token = $this->getToken();   //先从redis中取
            if(!$this->token){
                //登录
                if(!$this->login()){
                    return false;
                }
            }
        }


        $url = $this->server . "/chatgroups";
        $this->http->setUrl($url);
        $this->http->setTimeout(3000);
        $postData['groupname'] = $groupName;
        $postData['desc'] = $description;
        $postData['maxusers'] = $maxusers;
        $postData['owner'] = $owner;
        if(count($members) > 0)
            $postData['members'] = $members;
        
        $postData['public'] = $isPublic;
        $postData['members_only'] = $needPermit;
        $postData['allowinvites'] = $allowInvites;


        $this->http->setParams(json_encode($postData));

        $header[] = 'Content-Type:application/json';
        $header[] = 'Authorization:Bearer '.$this->token;
        $response = $this->http->postWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("create chatgroup error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            //记录注册聊天室的日志
            $this->logger->info("create chatgroup success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            $result = json_decode($response['data'],true);
            return $result['data']['groupid'];
        }
    }

    /**
     * 加入黑名单
     */
    public function blockUser($ownerUser,$blockUsers){
        if($this->token === null){
            $this->token = $this->getToken();   //先从redis中取
            if(!$this->token){
                //登录
                if(!$this->login()){
                    return false;
                }
            }
        }


        $url = $this->server . '/users/' . $ownerUser . '/blocks/users';
        $this->http->setUrl($url);
        $this->http->setTimeout(3000);
        $postData['usernames'] = $blockUsers;

        $this->http->setParams(json_encode($postData));

        $header[] = 'Content-Type:application/json';
        $header[] = 'Authorization:Bearer '.$this->token;
        $response = $this->http->postWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("block user error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            //记录添加黑名单日志
            $this->logger->info("block user success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return true;
        }
    }

    /**
     * 从黑名单中删除用户
     */
    public function deleteBlockUser($ownerUser,$blockUser){
        if($this->token === null){
            $this->token = $this->getToken();   //先从redis中取
            if(!$this->token){
                //登录
                if(!$this->login()){
                    return false;
                }
            }
        }

        $url = $this->server . "/users/$ownerUser/blocks/users/$blockUser";
        $this->http->setUrl($url);
        $this->http->setTimeout(3000);

        $header[] = 'Content-Type:application/json';
        $header[] = 'Authorization:Bearer '.$this->token;
        $response = $this->http->deleteWithHeader($header);

        #从response中获取请求的状态码和结果
        $status = $response['status'];
        $time_end = Util::getMicroTime();
        $use_time = $time_end - $this->time_start;
        $this->time_start = $time_end;
        if($status !== 200){
            //出错,记录到log中
            $this->logger->error("delete block user error:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return false;
        }else{
            //记录删除黑名单的日志
            $this->logger->info("delete block user success:",array('url' => $url,'status' => $status,'use_time[s]' => $use_time,'result' => $response['data']));
            return true;
        }
    }

}