<?php
/**
 * 即时通信的接口抽象
 * author: jiangpengfei 
 * date: 2017-02-27
 */
namespace ThirdParty;

interface IInstantMessage{
    /**
     * 创建用户
     */
    public function createUser($username,$password,$face_url = null);

    /**
     * 创建聊天室
     * @param name 聊天室名称
     * @param description 聊天室描述
     * @param maxusers 聊天室成员最大数
     * @param owner 聊天室的管理员
     * @param members 聊天室的成员 
     * @return mix 创建成功为聊天室Id，创建失败为false
     */
    public function createChatRoom(string $name,string $description,int $maxusers,string $owner,array $members);

    /**
     * 删除聊天室
     * @param $chatroomId 聊天室id
     */
    public function deleteChatRoom($chatroomId);

    /**
     * 添加聊天室成员
     * @param $chatroom 聊天室
     * @param $username 用户名
     * @return bool false失败,true成功
     */
    public function addUserToChatRoom($chatroom,$username);

    /**
     * 发送消息给单个用户
     * @param $sendUsers 发送的用户数组
     * @param $msgJson   消息内容,为json对象
     * @param $ext       消息的扩展字段,是一个map
     * @return bool true代表成功，false代表失败
     */
    public function sendTextMessageToUser($sendUsers,$msgJson,$ext = null);

    /**
     * 创建群组
     * @param groupName 群组名称
     * @param description 聊天室描述
     * @param maxusers 聊天室成员最大数
     * @param owner 聊天室的管理员
     * @param members 聊天室的成员 
     * @param isPublic 是否公开
     * @param needPermit 加入群是否需要群主或者群管理员审批，默认是false
     * @param allowInvites 是否允许群成员邀请别人加入此群。 true：允许群成员邀请人加入此群，false：只有群主或者管理员才可以往群里加人。
     * @return mix 创建成功为聊天室Id，创建失败为false
     */
    public function createChatGroup(string $groupName,string $description,
    int $maxusers,string $owner,array $members,bool $isPublic,bool $needPermit,bool $allowInvites,$customInfo = null
);

    /**
     * 加入黑名单
     */
    public function blockUser($ownerUser,$blockUsers);

    /**
     * 从黑名单中删除用户
     */
    public function deleteBlockUser($ownerUser,$blockUser);

    /**
     * 修改用户的昵称
     * @param $userIdentity 用户的标识
     * @param $nickname   用户昵称
     */
    public function updateNickName($userIdentity, $nickname);
    
    /**
     * 管理员指定账号向另外的账号发送消息
     * @param $user1 账号1
     * @param $user2 账号2
     * @param $sync  是否同步到终端
     * @param $content 消息内容
     * @return bool 执行成功还是失败
     */
    public function sendMsgFromOneToAnother($user1, $user2, $sync, $content);
}