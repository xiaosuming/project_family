<?php
/**
 * 支付宝支付的异步通知类型
 * @author: jiangpengfei
 * @date:   2019-03-22
 */

namespace ThirdParty\Alipay;

class AlipayNotifyTradeStatus {
    public static $TRADE_SUCCESS = 'TRADE_SUCCESS';     // 支付宝默认开启
    public static $TRADE_FINISHED = 'TRADE_FINISHED';   // 不开启
    public static $TRADE_CLOSED = 'TRADE_CLOSED';       // 不开启
    public static $WAIT_BUYER_PAY = 'WAIT_BUYER_PAY';   // 不开启
}