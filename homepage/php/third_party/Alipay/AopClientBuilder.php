<?php
/**
 * AopClient的创建
 * @author: jiangpengfei
 * @date:   2019-03-20
 */

namespace ThirdParty\Alipay;

use ThirdParty\Alipay\Aop\AopClient;

class AopClientBuilder {
    public static function instance() {
        $alipayConf = \Yaconf::get('alipay');
        
        $aop = new AopClient();
        $aop->gatewayUrl = $alipayConf['gatewayUrl'];
        $aop->appId = $alipayConf['app_id'];
        $aop->rsaPrivateKeyFilePath = $alipayConf['merchant_private_key'];
        $aop->format = "json";
        $aop->charset = $alipayConf['charset'];
        $aop->signType = $alipayConf['sign_type'];
        $aop->alipayrsaPublicKey = $alipayConf['alipay_public_key'];

        return $aop;
    }
}