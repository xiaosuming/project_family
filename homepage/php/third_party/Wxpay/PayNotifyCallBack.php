<?php
/**
*
**/

namespace ThirdParty\Wxpay;

use ThirdParty\Wxpay\Lib\WxPayApi;
use ThirdParty\Wxpay\Lib\WxPayNotify;
use ThirdParty\Wxpay\Lib\WxPayOrderQuery;
use ThirdParty\Wxpay\Lib\WxPayException;
use Util\SysLogger;
use Util\Util;
use DB\CDBOrder;
use Util\SysConst;

class PayNotifyCallBack extends WxPayNotify
{
	private $logger;
	private $localLogger;
	public function __construct()
	{
		$this->logger = SysLogger::getInstance();
		$this->localLogger = SysLogger::getInstance(true, 'wxpay_notify');
	}

	//查询订单
	public function Queryorder($transaction_id)
	{
		$input = new WxPayOrderQuery();
		$input->SetTransaction_id($transaction_id);

		$config = new WxPayConfig();
		try {
			$result = WxPayApi::orderQuery($config, $input);
		} catch (WxPayException $e) {
			SysLogger::getInstance()->error('微信支付查询订单异常', [Util::exceptionFormat($e)]);
			return false;
		}
		if(array_key_exists("return_code", $result)
			&& array_key_exists("result_code", $result)
			&& $result["return_code"] == "SUCCESS"
			&& $result["result_code"] == "SUCCESS")
		{
			return true;
		}
		return false;
	}

	/**
	*
	* 回包前的回调方法
	* 业务可以继承该方法，打印日志方便定位
	* @param string $xmlData 返回的xml参数
	*
	**/
	public function LogAfterProcess($xmlData)
	{
		$data['info'] =  "call back， return xml:" . $xmlData;
		$this->localLogger->info('order_query', $data);
		return;
	}
	
	//重写回调处理函数
	/**
	 * @param WxPayNotifyResults $data 回调解释出的参数
	 * @param WxPayConfigInterface $config
	 * @param string $msg 如果回调处理失败，可以将错误信息输出到该方法
	 * @return true回调出来完成不需要继续回调，false回调处理未完成需要继续回调
	 */
	public function NotifyProcess($objData, $config, &$msg)
	{
		$data = $objData->GetValues();
		//1、进行参数校验
		if(!array_key_exists("return_code", $data) 
			||(array_key_exists("return_code", $data) && $data['return_code'] != "SUCCESS")) {
			$msg = "异常";
			return false;
		}
		if(!array_key_exists("transaction_id", $data)){
			$msg = "输入参数不正确";
			return false;
		}

		//2、进行签名验证
		try {
			$checkResult = $objData->CheckSign($config);
			if($checkResult == false){
				//签名错误
				$this->logger->error('order_query', ['error'=>"微信支付签名错误"]);
				return false;
			}
		} catch(Exception $e) {
			$this->logger->error('order_query', Util::exceptionFormat($e));
			return false;
		}

		//3、处理业务逻辑
		$this->localLogger->info('order_query', $data);
		$notfiyOutput = array();

		$totalAmount = $data['cash_fee'];		// 支付的总金额
		$sellerId = $data['mch_id'];			// 商家id
		$appId = $data['appid'];				// appId
		$orderId = $data['out_trade_no'];		// 订单id
		$tradeStatus = $data['return_code'];	// 返回吗
		$payway = SysConst::$ORDER_PAYWAY_WEIXIN;	// 微信支付方式

		// 从数据库查询订单状态
        $orderDB = new CDBOrder();
        $order = $orderDB->getOrder($orderId);

        if ($order == null) {
			$this->logger->error('订单不存在', $orderId);
            return false;
        }

        // 检查订单金额
        if ($order->realPrice != $totalAmount) {
			$this->logger->error('订单金额错误', $data);
            return false;
        }

		$wxConfig = \Yaconf::get('wxpay');

        // 检查订单的sellerId
        if ($sellerId != $wxConfig['seller_id']) {
            $this->logger->error('订单sellerId错误', $data);
            return false;
        }

        // 检查订单的app_id
        if ($appId != $wxConfig['app_id']) {
            $this->logger->error('订单appid错误', $data);
            return false;
        }


        if ($order->status != SysConst::$ORDER_STATUS_UNPAY) {
            // 如果订单不是创建状态，说明已经查询过了
            return false;
        }

        if ($tradeStatus == 'SUCCESS') {
            $orderDB->chatOrderCallbackOnSuccess($orderId, SysConst::$ORDER_PAYWAY_WEIXIN);
			return true;
        } else {
            // 订单失败了
            $this->logger->error('微信支付:订单交易失败:', ['orderId' => $orderId]);
			return false;
        }

		return true;
	}
}

