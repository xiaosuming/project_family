<?php
namespace ThirdParty;

use Util\TempStorage;

class MiniProgram
{
    private $appId;
    private $appSecret;
    private $tempStorage;

    /**
     * @param $type 小程序的类型，1是递名帖，2是AI族群
     */
    public function __construct($type = 1)
    {
        if ($type == 1) {
            $this->appId = $GLOBALS['mini_appid'];
            $this->appSecret = $GLOBALS['mini_secret'];
        } else if ($type == 2) {
            $this->appId = $GLOBALS['ai_mini_appid'];
            $this->appSecret = $GLOBALS['ai_mini_secret'];
        }
        $this->tempStorage = new TempStorage();
    }

    public function getSignPackage($url = "")
    {
        $jsapiTicket = $this->getJsApiTicket();

        if ($url == "") {
      // 注意 URL 一定要动态获取，不能 hardcode.
            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
            $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }

        $timestamp = time();
        $nonceStr = $this->createNonceStr();

    // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

        $signature = sha1($string);

        $signPackage = array(
            "appId" => $this->appId,
            "nonceStr" => $nonceStr,
            "timestamp" => $timestamp,
            "url" => $url,
            "signature" => $signature,
            "rawString" => $string
        );
        return $signPackage;
    }

    private function createNonceStr($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    private function getJsApiTicket()
    {
    // jsapi_ticket 应该全局存储与更新，
        $ticket = $this->tempStorage->get("jsapi_ticket");
        if (!$ticket) {
            $accessToken = $this->getAccessToken();
      // 如果是企业号用以下 URL 获取 ticket
      // $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
            $res = json_decode($this->httpGet($url));
            $ticket = $res->ticket;
            if ($ticket) {
                $this->tempStorage->setTemp("jsapi_ticket", $ticket, 7100);   //留10s
            }
        }

        return $ticket;
    }

    private function getAccessToken()
    {
    // access_token 应该全局存储与更新，以下代码以写入到文件中做示例
        $accessToken = $this->tempStorage->get("mini_access_token_" . $this->type);
        if (!$accessToken) {
      // 如果是企业号用以下URL获取access_token
      // $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
            $res = json_decode($this->httpGet($url));
            $accessToken = $res->access_token;
            if ($accessToken) {
                $this->tempStorage->setTemp("mini_access_token", $accessToken, 7100);   //留100s
            }
        }
        return $accessToken;
    }

    public function getQrCode()
    {
        $accessToken = $this->getAccessToken();
        $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' . $accessToken;

        echo $url;
    // $this->httpGet($url);
    }

    public function getUserInfo($code)
    {
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid=' .
            $this->appId
            . '&secret=' .
            $this->appSecret
            . '&js_code=' .
            $code
            . '&grant_type=authorization_code';

        return $this->httpGet($url);
    }

    private function httpGet($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    // 为保证第三方服务器与微信服务器之间数据传输的安全性，所有微信接口采用https方式调用，必须使用下面2行代码打开ssl安全校验。
    // 如果在部署过程中代码在此处验证失败，请到 http://curl.haxx.se/ca/cacert.pem 下载新的证书判别文件。
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_URL, $url);

        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }


    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData($encryptedData, $iv, &$data, $sessionKey)
    {
        if (strlen($sessionKey) != 24) {
            return false;
        }
        $aesKey = base64_decode($sessionKey);


        if (strlen($iv) != 24) {
            return false;
        }
        $aesIV = base64_decode($iv);

        $aesCipher = base64_decode($encryptedData);

        $result = openssl_decrypt($aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);

        $dataObj = json_decode($result);
        if ($dataObj == null) {
            return false;
        }
        if ($dataObj->watermark->appid != $this->appId) {
            return false;
        }
        $data = $result;
        return true;
    }

    /**
     * 检验图片.
     * @param $accessToken string access_token
     * @param $img ??? 可能是图片吧，亲
     *
     * @return array 成功0，失败返回对应的错误码
     */
    public function imgSecCheck($img){
        $accessToken = $this->getAccessToken();
        $media = file_get_contents($img['tmp_name']);
        if (empty($media)) {
            return [];
        }
        $farce = 0;//强制更新token
        $retry = 0;
        $num = 0;
        $url = 'https://api.weixin.qq.com/wxa/img_sec_check?access_token=';
        $boundary="----".md5(date);
        $formdata = "--" . $boundary ."\r\n";
        $formdata .= "Content-Disposition: form-data; name=\"upload\"; filename=\"upload.gif\"\r\n";
        $formdata .= "Content-type: application/octet-stream\r\n\r\n";
        $formdata .= $media."\r\n";
        $formdata .= "--" . $boundary . "\r\n"."--\r\n\r\n";
        do {
            $token = $accessToken;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url . $token);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $formdata);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                "Content-Type: multipart/form-data; boundary=".$boundary,
                "Content-Length: " . strlen($formdata)
            ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);
            curl_close($curl);
            $resultObj = json_decode($result,false);
//            var_dump($url . $token);
//            var_dump($resultObj);
            $num++;
            //token过期
            if ($resultObj->errcode == 40001) {
                $farce = 1;
                $retry = 1;
            }else if ($resultObj->errcode == -1) {//服务器返回忙
                $retry = 1;
            } else if($resultObj->errcode == 0){
                $farce = 0;
                $retry = 0;
            }
        } while ($retry && $num < 5);
        if ($resultObj->errcode != 0) {
            return $resultObj;
        }
        return $resultObj;
    }

    /**
     * 检验文字.
     * @param $accessToken string access_token
     * @param $content array or string 检查的字符数组
     *
     * @return bool 成功0，失败返回对应的错误码
     */
    public function msgSecCheck($content){
        $accessToken = $this->getAccessToken();
        $content = json_encode($content,JSON_UNESCAPED_UNICODE);
        if (empty($content)) {
            return true;
        }
        $farce = 0;//强制更新token
        $retry = 0;
        $num = 0;
        $url = 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token=';
        $formdata = json_encode(['content'=>$content],JSON_UNESCAPED_UNICODE);
        $boundary="----".md5(date);
//        $formdata = "--" . $boundary ."\r\n";
//        $formdata .= "Content-Disposition: form-data; name=\"upload\"; filename=\"upload.gif\"\r\n";
//        $formdata .= "Content-type: application/octet-stream\r\n\r\n";
//        $formdata .= $media."\r\n";
//        $formdata .= "--" . $boundary . "\r\n"."--\r\n\r\n";
        do {
            $token = $accessToken;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url . $token);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $formdata);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                "Content-Type: multipart/form-data; boundary=".$boundary,
                "Content-Length: " . strlen($formdata)
            ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);
            curl_close($curl);
            $resultObj = json_decode($result,false);
//            var_dump($url . $token);
//            var_dump($resultObj);
            $num++;
            //token过期
            if ($resultObj->errcode == 40001) {
                $farce = 1;
                $retry = 1;
            }else if ($resultObj->errcode == -1) {//服务器返回忙
                $retry = 1;
            } else if($resultObj->errcode == 0){
                $farce = 0;
                $retry = 0;
            }
        } while ($retry && $num < 5);
        if ($resultObj->errcode != 0) {
            return false;
        }
        return true;
    }
//    {
//        $url = "https://api.weixin.qq.com/wxa/img_sec_check?access_token=$accessToken";
//        $postData = array(
//            'media'=>$img
//        );
//
//        $postData = http_build_query($postData); //做一层过滤
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL,            $url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
//        curl_setopt($ch, CURLOPT_POST,           1 );
//        curl_setopt($ch, CURLOPT_POSTFIELDS,     $postData );
//        curl_setopt ( $ch, CURLOPT_URL, $url );
//        curl_setopt ( $ch, CURLOPT_POST, 1 );
//        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
//        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 5 );
////        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
//        curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, false );
//        curl_setopt ( $ch, CURLOPT_SAFE_UPLOAD, false);
//        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $postData );
//        $result=curl_exec ($ch);
//        curl_close($ch);
//        return json_decode($result,true);
//    }

}

