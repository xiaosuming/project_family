<?php
    header('Access-Control-Allow-Origin: *');
    
    date_default_timezone_set("Asia/Shanghai");
    require_once("logicserver/action_file.php");
    require_once('vendor/autoload.php');
    
    //require_once('function/SystemMonitor.class.php');

    use Util\Util;
    use Util\SystemMonitor;
    use Util\SysLogger;    
    use Model\Limiter;
    use ThirdParty\Alipay\Aop\AopClient;
    use Util\SysConst;
    use DB\CDBOrder;
    use ThirdParty\Alipay\AlipayNotifyTradeStatus;

    $alipayParams = $_POST;
    $logger = SysLogger::getInstance(true, 'alipay_notify');
    $errorLogger = SysLogger::getInstance();

    if (!isset($alipayParams['trade_status']) && $alipayParams['trade_status'] != AlipayNotifyTradeStatus::$TRADE_SUCCESS) {
        $logger->error("不支持的异步通知", $alipayParams);
        exit;    
    }

    $alipayConfig = \Yaconf::get('alipay');
    $aopClient = new AopClient();
    $aopClient->alipayrsaPublicKey = $alipayConfig['alipay_public_key'];

    // 验签，保证接收到的信息没有问题
    $result = $aopClient->rsaCheckV1($alipayParams, '', $alipayConfig['sign_type']);

    if (!$result) {
        // 验签失败
        echo "success";
        exit;
    }

    // 记录到日志
    $logger->info('alipay_notify', $_POST);


    /* 实际验证过程建议商户添加以下校验。
    1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
    2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
    3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
    4、验证app_id是否为该商户本身。
    */

    // 商户订单号
    $orderId = htmlspecialchars($alipayParams['out_trade_no']);

    // 支付宝交易号
    $tradeNo = htmlspecialchars($alipayParams['trade_no']);

    $payway = SysConst::$ORDER_PAYWAY_ALIPAY;
    
    // appId
    $appId = htmlspecialchars($alipayParams['app_id']);

    // 订单的操作方
    $sellerId = htmlspecialchars($alipayParams['seller_id']);

    // 总价格
    $totalAmount = htmlspecialchars($alipayParams['total_amount']);

    // 交易状态
    $tradeStatus = htmlspecialchars($alipayParams['trade_status']);

    $orderDB = new CDBOrder();
    $order = $orderDB->getOrder($orderId);

    if ($order == null) {
        // 订单不存在
        $errorLogger->error('订单不存在', $alipayParams);
        echo "success";
        exit;
    }

    // 检查订单的金额
    // 注意这里有小数相乘，因此需要使用bcmath的高精度的计算
    if (intval($order->realPrice) != bcmul($totalAmount, 100)) {
        // 金额不正确 
        $errorLogger->error('订单金额不正确', $alipayParams);
        echo "error";
        exit;
    }

    // 检查订单的sellerId
    if ($sellerId != $alipayConfig['seller_id']) {
        // 商户id不正确
        $errorLogger->error('商户id不正确', $alipayParams);
        echo "error";
        exit;
    }

    // 检查订单的app_id
    if ($appId != $alipayConfig['app_id']) {
        // app id 不正确
        $errorLogger->error('app id 不正确', $alipayParams);
        echo "error";
        exit;
    }

    if ($order->status != SysConst::$ORDER_STATUS_UNPAY) {
        // 订单不是未支付状态
        $errorLogger->error('订单不是未支付状态', $alipayParams);
        echo "success";
        exit;
    }

    if ($tradeStatus == AlipayNotifyTradeStatus::$TRADE_SUCCESS) {
        // 交易成功
        $orderDB->chatOrderCallbackOnSuccess($orderId, $payway);

    } else {
        $errorLogger->error('订单交易失败', $alipayParams);
        echo "success";
        exit;
    }