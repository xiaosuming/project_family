<?php
require_once('config/environment.php');
$code = isset($_GET['code']) ? $_GET['code'] : "";
$state = isset($_GET['state']) ? $_GET['state'] : "";

if($state === "login"){
    header("location:https://m.izuqun.com".$GLOBALS['ROUTER_TYPE']."/login-wechat/".$code);
}else if($state === "register"){
    header("location:https://m.izuqun.com".$GLOBALS['ROUTER_TYPE']."/register-wechat/".$code);
} else if ($state === "login_dev") {    // 本地的测试模式
    header("location:http://localhost:4200".$GLOBALS['ROUTER_TYPE']."/login-wechat/".$code);
} else if ($state === "register_dev") { // 本地的测试模式
    header("location:https://localhost:4200".$GLOBALS['ROUTER_TYPE']."/register-wechat/".$code);
}else{
    //这里是复杂的多参数模式
    $strs = explode("|",$state);
    if(count($strs) > 0){
        if($strs[0] === "pt-in-family" && count($strs) == 2){
            //通过微信登录并申请加入家族
            header("location:https://m.izuqun.com".$GLOBALS['ROUTER_TYPE']."/login-wechat/".$code."/".$strs[1]);
        }
    }
}