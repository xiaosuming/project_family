<?php

/**
 * 这是一个用来批量生成tencentIM腾讯云通讯用户的脚本
 */


require_once('vendor/autoload.php');

    use DB\CDBManager;
    use ThirdParty\TencentIM;

    if (!isset($GLOBALS['pdo'])) {
        $pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
        if ($pdo)
            $GLOBALS['pdo'] = $pdo;
    } else {
        $pdo = $GLOBALS['pdo'];
    }

try{
    $pdo->beginTransaction();
    $i = 3;
    $TencentIM = new TencentIM();
    while($i< 10000){
        $username = 'deafultuser_'.$i;
        $nickname = 'u'.$username;
        if($TencentIM->createUser($username, '')){
            $sql = "INSERT INTO tencent_IM_user (userId, name, nickname, create_time, update_time)
            VALUES ( 0, '$username', '$nickname', now(), now())";
            $pdo->insert($sql);
            echo '成功注册'.$username.PHP_EOL;
        }else{
            $pdo->rollback();
            echo $username.'失败'.PHP_EOL;
            break;
        }

        $i++;
    }

    $pdo->commit();

}catch (PDOException $e) {
    $pdo->rollback();
    //异常处理
    var_dump($e);
}