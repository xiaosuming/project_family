<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-21
 * Time: 下午1:34
 */

use DB\CDBInformationPost;
use DB\CDBManager;

require_once '../vendor/autoload.php';
//type是3的推文 推给系统中的每一个用户
// 1 获取所有的用户 2 获取用户关注的圈子  3 把推文推到每个圈子中 4 获取所有圈子中的用户去重 5 发送到时间轴上
function pushOldPostToNewTimeLine()
{
    $mysql = new CDBManager('116.62.25.128', 'family', 'Flare1111', 'family');
    //获取type是1,2的用户列表
    $sql = "SELECT distinct userId FROM gener_zonePost where (type = 1 or type =2) and isDelete=0";
    $queryUsers = $mysql->query($sql);
    $queryUsers = array_column($queryUsers, 'userId');

    $mysql->beginTransaction();

    foreach ($queryUsers as $userId) {

        //获取用户发的推文
        $sql = "select * from gener_zonePost where userId = '$userId' and isDelete=0";
        $posts = $mysql->query($sql);

        // var_dump($posts);

        //获取用户关注的圈子
        $sql = "select categoryId from gener_information_follow where userId = '$userId' and isDelete = 0";
        $socialCircleIds = $mysql->query($sql);

        foreach ($posts as $post) {

            $type = $post['type'];
            $coorX = $post['coor_x'];
            $coorY = $post['coor_y'];
            $address = $post['address'];
            $photo = $post['photo'];
            $content = addslashes($post['content']);
            $createTime = $post['create_time'];

            $pushUserIds = [];

            $postId = 0;
            foreach ($socialCircleIds as $socialCircleId) {

                if (!array_key_exists('categoryId', $socialCircleId)) {
                    continue;
                }
                $categoryId = $socialCircleId['categoryId'];

                $sql = "select circleType from gener_information_category where id='$categoryId' and is_delete=0";
                $circleType = $mysql->uniqueResult($sql);
                if ($circleType['circleType'] != 3) {
                    continue;
                }

                $sql = "select userId from gener_information_follow where categoryId = '$categoryId' and isDelete = 0";
                $followUserIds = $mysql->query($sql);

                if (count($followUserIds) > 0) {
                    $followUserIds = array_column($followUserIds, 'userId');

                    $pushUserIds = array_merge($followUserIds, $pushUserIds);
                }


                $sql = "INSERT INTO gener_information (type,isArticle,coorX,coorY,address,userId,categoryId,photo,content,create_time,create_by,update_time,update_by)
            VALUES( '$type',0,'$coorX','$coorY','$address','$userId','$categoryId','$photo','$content','$createTime','$userId','$createTime','$userId')";
                $postId = $mysql->insert($sql);

                if ($postId > 0) {

                    $sql = "UPDATE gener_user_profile SET info_post_num = info_post_num + 1 WHERE userId = '$userId' ";
                    $mysql->update($sql);
                    //添加推文的同时要更新圈子的推文数量
                    $sql = "UPDATE gener_information_category SET num = num + 1 WHERE id = '$categoryId' ";
                    $mysql->update($sql);

                }


            }

            // 一条推文推给哪些人
            $pushUserIds = array_unique($pushUserIds);
//            var_dump($pushUserIds);
//            exit;
            if ($postId != 0) {
                $redisPost = new CDBInformationPost();
                //设置推文的推送任务
                $taskPushResult = $redisPost->setPostTaskWithOutMq($pushUserIds, $postId, strtotime($createTime), $userId);
            }
            echo '新增推文动态' . $postId . PHP_EOL;

        }

    }

    $mysql->commit();

}

pushOldPostToNewTimeLine();
