<?php
/**
 * 创建姓氏圈子脚本
 * @author: jiangpengfei
 * @date:   2019-03-21
 */


require_once('../vendor/autoload.php');

use DB\CDBManager;
use Util\Util;
use DB\CDBInformation;

$systemUserId = 420;
$pid = 4667;

$avatar = 'https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIMCABqZfAACALIbo__s039.jpg';
$GLOBALS['db_host'] = "116.62.25.128";

function createCircle($lastName){
    global $systemUserId;
    global $pid;
    global $avatar;

    $informationDB = new CDBInformation();
    $informationDB->addInformationCategory($systemUserId, $avatar, $lastName, $pid, 1, 1);
}

$file = fopen('lastname.txt', "r");
while(!feof($file)) {
    $str = trim(fgets($file));

    if ($str != '') {
        $lastName = $str.'氏圈子';
        createCircle($lastName);
    }
}

