<?php
/**
 * 在redis中更新用户follow
 */
require_once('../vendor/autoload.php');

use DB\UserFollowDB;
use DB\CDBPerson;
use DB\CDBManager;

$mysql = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
// $mysql = new CDBManager('116.62.25.128', $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);

$sql = "SELECT id,userId FROM gener_person where userId > 0";

$result = $mysql->query($sql);

$personDB = new CDBPerson();

foreach($result as &$item) {
    $personId = $item['id'];
    $userId = $item['userId'];

    $followIds = $personDB->getPersonsIdByPathLen($personId, 3);     //路径3以内

    if (count($followIds) > 0) {
        $followIds = $personDB->getUserIdsFromPersonIds($followIds, $userId);

        if (count($followIds) > 0) {
            $userFollowDB = new UserFollowDB();
            $userFollowDB->appendUserFollow($userId, $followIds);
        }
    }

}