<?php
/**
 * 为alipay的SDK文件夹Aop自动生成namespace
 * 使用方法：php autogen.php App\\ThirdParty\\Alipay\\Aop ./App/ThirdParty/Alipay/Aop
 * @author: jiangpengfei12@gmail.com
 * @date: 2018-10-30
 */

 // 遍历文件夹
function walkDir($dir, $prefix) {
    $handler = opendir($dir);

    while( ($filename = readdir($handler)) !== false) {
        if ($filename != '.' && $filename != '..') {
            // 找到文件，判断是不是目录
            if (is_dir($dir.DIRECTORY_SEPARATOR.$filename)) {
                // 是目录
                walkDir($dir.DIRECTORY_SEPARATOR.$filename, $prefix."\\".$filename);
            } else {
                // 不是目录
                
                $suffix = substr($filename, strlen($filename)-4, 4);

                if ($suffix == '.php') {
                    $namespace = $prefix;
                    autogen($dir.DIRECTORY_SEPARATOR.$filename, $namespace);
                }

            }
        }
    }
}

function autogen($file, $namespace) {

    $content = file_get_contents($file);
    $content = str_replace(" App\\", " ", $content);
    file_put_contents($file, $content);
}

if ($argc != 3) {
    echo '参数数量不正确';
    exit;
}

// 命名空间的共同前缀，如App\ThirdParty\Ailpay\Aop\Request\*，
// 那么App\\ThirdParty\\Alipay\\Aop就是共同前缀

$prefix = $argv[1];
$directory = $argv[2];      // Alipay的Aop位置

if (!file_exists($directory)) {
    echo "指定文件夹不存在";
}

walkDir($directory, $prefix);