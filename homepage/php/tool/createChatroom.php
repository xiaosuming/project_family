<?php
/**
 * 这是一个用来为数据库中用户创建环信账号的脚本
 */

require_once('../vendor/autoload.php');

use DB\CDBManager;
use ThirdParty\InstantMessage;
use Util\Util;
use DB\CDBAccount;

function createHuanxinChatRoom(){
    $mysql = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
    $sql = "select id,name,originator,hx_chatroom from gener_family";
    $result = $mysql->query($sql);
   
    $im = new InstantMessage();
    foreach($result as $family){

        if (substr($family['hx_chatroom'],0,1) == '@') {
            echo "跳过";
            continue;
        }

        if ($family['id'] == 346) {
            continue;
        }

        $chatroomName = $family['name'];
        
        //查找创始人对应的环信用户
        $accountDB = new CDBAccount();
        $userInfo = $accountDB->getHuanxinInfoById($family['originator']);
        if($userInfo == null){
            echo ("管理员账号不存在");
            continue;
        }

        $hxUsername = $userInfo['hxUsername'];

        $chatroomId = $im->createChatRoom($chatroomName,$chatroomName."的聊天室",5000,$hxUsername,array());
        //为家族创建聊天室
        if($chatroomId){
            echo "创建聊天室成功\n";
        }else{
            exit("创建聊天室失败".$chatroomName);
        }

        //更新聊天室信息
        $familyId = $family['id'];
        //存回数据库
        $sql = "update gener_family set hx_chatroom = '$chatroomId' WHERE id = '$familyId' ";
        $updateRow = $mysql->update($sql);

        if($updateRow > 0){
            echo "更新id为{$familyId}的家族的聊天室\n";
        }else{
            echo "更新出错\n";
            exit();
        }
    }
}

createHuanxinChatRoom();