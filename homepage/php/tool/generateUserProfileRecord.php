<?php
/**
 * 这是一个用来生成用户profile记录的脚本
 */

require_once("../config/db_config.php");
require_once("../db/db_mysql_class.php");

use DB\CDBManager;

function generate(){
    $mysql = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
    $sql = "SELECT id FROM gener_user";

    $userIds = $mysql->query($sql);
    foreach($userIds as $value){
        $userId = $value['id'];
        $c_sql = "SELECT count(id) FROM gener_collection WHERE userId = '$userId' AND is_delete = '0'";
        $c_result = $mysql->uniqueResult($c_sql);
        $collectionNum = $c_result['count(id)'];

        $f_sql = "SELECT count(id) FROM gener_person WHERE userId = '$userId'";
        $f_result = $mysql->uniqueResult($f_sql);
        $familyNum = $f_result['count(id)'];

        $p_sql = "SELECT count(id) FROM gener_zonePost WHERE userId = '$userId'";
        $p_result = $mysql->uniqueResult($p_sql);
        $postNum = $p_result['count(id)'];


        $sql = "INSERT INTO gener_user_profile(userId,post_num,collection_num,family_num,create_time,create_by,update_time,update_by) 
                VALUES('$userId','$postNum','$collectionNum','$familyNum',now(),'$userId',now(),'$userId')";
        
        echo $mysql->insert($sql);
    }
}

generate();