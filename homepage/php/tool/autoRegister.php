<?php
require_once('../vendor/autoload.php');

use DB\CDBAccount;
use DB\HuanxinDB;
use Model\User;
use Util\Util;

$phones = array(
    // "13902200226","18270755928","13755816608","13979790888","18870711739","13803585179","13611668903","13602406911","13817068386","15970128572",
    // "15970916582","13217972959"
    "18202197063","13155790526","18296863566"
);

$accountDB = new CDBAccount();

foreach($phones as $phone){

    $user = new User();
    $user->username = "user".$phone;
    $user->nickname = "nickname";
    $user->phone = $phone;
    $user->password = hash("sha256","123456".$GLOBALS['SHA256_SALT'],false);

    $hxUsername = md5("izuqun_".$user->username . "_" . Util::generateRandomCode(10));
    $hxPassword = Util::generateRandomCode(20);
    //将用户名和密码推到任务队列中
    $hxdb = new HuanxinDB();
    if(!$hxdb->pushUserTask($hxUsername,$hxPassword)){
        Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "注册失败");
        exit();
    }

    $user->hxUsername = $hxUsername;
    $user->hxPassword = $hxPassword;
    $accountDB->userRegister($user);
}