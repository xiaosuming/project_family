<?php
/**
 * 删除某个时间点之前的pdf
 */

require_once('../vendor/autoload.php');

use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;
use DB\CDBManager;

function clearBefore($time) {

    $mysql = new CDBManager('47.111.172.152', $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
    $sql = "SELECT preview_pdf, print_pdf FROM gener_pdf_task WHERE create_time < '$time'";

    $items = $mysql->query($sql);

    foreach ($items as $item) {
        $files = [];
        if ($item['preview_pdf'] != "") {
            $prefix = str_split($item['preview_pdf'], 1)[0];
            if ($prefix == "h") {
                $url = $item['preview_pdf'];
                $res = explode("/", $url);
                $files[] = $res[count($res)-1];
            } else if ($prefix == "[") {
                $urls = json_decode($item['preview_pdf']);
                foreach($urls as $url) {
                    $res = explode("/", $url);
                    $files[] = $res[count($res)-1];
                }
            } else {
                continue;
            }
        }

        if ($item['print_pdf'] != "") {
            if ($item['print_pdf'] != "") {
                $prefix = str_split($item['print_pdf'], 1)[0];
                if ($prefix == "h") {
                    $url = $item['print_pdf'];
                    $res = explode("/", $url);
                    $files[] = $res[count($res)-1];
                } else if ($prefix == "[") {
                    $urls = json_decode($item['print_pdf']);
                    foreach($urls as $url) {
                        $res = explode("/", $url);
                        $files[] = $res[count($res)-1];
                    }
                } else {
                    continue;
                }
            }
        }

        foreach($files as $filename) {
            echo "删除".$filename.PHP_EOL;
            $client = new StorageFacadeClient();
            $client->setBucket(StorageBucket::$STORAGE);
            $res = $client->delete(StorageBucket::$STORAGE, $filename);
            var_dump($res);
            exit;
        } 
    }
}

clearBefore("2018-12-31 23:59:59");