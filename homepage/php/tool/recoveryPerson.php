<?php
/**
 * 这是一个用来生成用户profile记录的脚本
 */

require_once("../config/db_config.php");
require_once("../db/db_mysql_class.php");

use DB\CDBManager;

function gstr($str){
    if($str == ""){
        return "";
    }
    $strArr = explode(",",$str);
    $result = "";
    foreach($strArr as $s){
        $result .= $s."|";
    }
    return $result;
}

class Person{
    public $personId;
    public $branchId;
    public $familyId;
    public $userId;
    public $type;
    public $name;
    // public $countryName;
    // public $provinceName;
    // public $cityName;
    // public $areaName;
    // public $townName;
    public $address;
    public $birthday;
    public $gender;
    // public $zi;
    public $phone;
    public $photo;
    public $shareUrl;
    
    // public $refFamilyId;
    public $father;
    public $mother;
    public $son;
    public $daughter;
    public $spouse;
    public $sister;
    public $brother;
    public $level;
    // public $remark;
    public $createBy;
    public $createTime;
    public $updateBy;
    public $updateTime;

    public function __construct($person_json){
        $this->personId = $person_json['id'];
        $this->branchId = 0;
        $this->familyId = $person_json['familyId'];
        $this->userId = $person_json['userId'];
        $this->type = 1;
        $this->name = $person_json['name'];
        $this->address = $person_json['address'];
        $this->birthday = $person_json['birthday'] ?? NULL;
        $this->gender = $person_json['gender'];
        $this->phone = $person_json['phone'] ?? '';
        $this->photo = $person_json['photo'];
        $this->shareUrl = $person_json['shareUrl'];
        $this->father = gstr($person_json['fatherList']);
        $this->mother = gstr($person_json['motherList']);
        $this->son = gstr($person_json['sonList']);
        $this->daughter = gstr($person_json['daughterList']);
        $this->spouse = gstr($person_json['spouseList']);
        $this->sister = gstr($person_json['sisterList']);
        $this->brother = gstr($person_json['brotherList']);
        $this->level = $person_json['level'];
        $this->createBy = $person_json['updateBy'];
        $this->createTime = $person_json['updateTime'];
        $this->updateBy = $person_json['updateBy'];
        $this->updateTime = $person_json['updateTime'];
    }
}

function insertOrUpdate($person){
    
    $mysql = new CDBManager("116.62.25.128", $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
    
    $sql = "SELECT * FROM gener_person WHERE id = '$person->personId' ";
    $result = $mysql->query($sql);
    
    if(count($result) == 1){
        $person->type = $result[0]['type'];
    }

    try{
        $mysql->beginTransaction();
        if(count($result) == 1){
            $mysql->commit();
            return;
            // //查出了结果，删除
            // $sql = "DELETE FROM gener_person WHERE id = '$person->personId' ";
            // $mysql->deletesql($sql);
        }

        $birthdayStr = $person->birthday == NULL ? "NULL"  : "'$person->birthday'";

        $sql = "INSERT INTO gener_person (
            id,branchId,familyId,userId,type,is_delete,name,birthday,zi,gender,photo,remark,share_url,is_dead,dead_time,ref_familyId,ref_personId,father,mother,son,daughter,spouse,sister,brother,level,confirm,blood_type,marital_status,phone,qq,qrcode,country,country_name,province,province_name,city,city_name,area,area_name,town,town_name,address,create_by,create_time,update_by,update_time
            )VALUES(
                '$person->personId','$person->branchId','$person->familyId','$person->userId','$person->type',0,'$person->name',$birthdayStr,'','$person->gender','$person->photo','','$person->shareUrl',0,NULL,NULL,NULL,'$person->father','$person->mother','$person->son','$person->daughter','$person->spouse','$person->sister','$person->brother','$person->level',0,NULL,NULL,'$person->phone',NULL,'',-1,0,-1,0,-1,0,-1,0,-1,0,'$person->address','$person->createBy','$person->createTime','$person->updateBy','$person->updateTime'
            )";
        
        $mysql->insert($sql);
        $mysql->commit();

    }catch(PDOException $e){
        $mysql->rollback();
        echo $sql."\n";
        exit($e->getMessage());       //出现异常直接退出
    }
}

$data = file_get_contents("/home/jiang/data-new-2.txt");
$strArr = explode("\n",$data);

array_pop($strArr);         ///去除最后一个空行

$count = 0;

foreach($strArr as &$str){
    $data = json_decode($str,true);
    $persons = $data['data']['persons'];
    echo "人数:".count($persons);
    foreach($persons as &$person){
        $newPerson = new Person($person);
        insertOrUpdate($newPerson);
    }
}