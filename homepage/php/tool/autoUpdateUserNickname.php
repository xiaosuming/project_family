<?php
/**
 * 更新所有的用户昵称到腾讯云通信上
 */

require_once('../vendor/autoload.php');

use DB\CDBManager;
use ThirdParty\InstantMessage;
use Util\Util;
use DB\CDBAccount;

function updateNickname(){
    $mysql = new CDBManager('116.62.25.128', $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
    $sql = "select id,nickname,hx_username from gener_user";
    $result = $mysql->query($sql);
    
    $im = new InstantMessage();
    $i = 0;
    foreach($result as $user){

        echo "第 $i 条 :";
        $i++;

        $nickname = $user['nickname'];
        $hxUser = $user['hx_username'];

        if ($hxUser == '') {
            continue;
        }
 
        $result = $im->updateNickName($hxUser, $nickname);
        //为家族创建聊天室
        if($result){
            echo "更改成功\n";
        }else{
            var_dump($user);
            echo "更改失败".$hxUser.PHP_EOL;
        }

    }
}

updateNickname();