<?php
/**
 * 将旧的名片数据更新到搜索中
 */
require_once('../vendor/autoload.php');

use DB\CDBInfoCard;
use DB\CDBManager;
use DB\CDBPushInfoCard;
use Util\Util;
use Model\InfoCard;

// $mysql = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
$mysql = new CDBManager('47.111.172.152', $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);

$pushInfoCardDB = new CDBPushInfoCard();
$infoCardDB = new CDBInfoCard();


$sql = $sql = "SELECT id,remark,
userId,
familyId,
personId,
name,
name_en as nameEn,
country,
country_name as countryName,
province,
province_name as provinceName,
city,
city_name as cityName,
area,
area_name as areaName,
town,
town_name as townName,
address,
photo,
background_img as backgroundImg,
background_color as backgroundColor,
background_type as backgroundType,
company,
company_en as companyEn,
gender,
birthday,
blood_type as bloodType,
marital_status as maritalStatus,
phone,
qq,
email,
company_profile as companyProfile,
company_url as companyUrl,
bank_account as bankAccount,
longitude,latitude,
skills,favorites,jobs,identity,type,permission,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 

FROM gener_info_card WHERE is_delete = 0";

// 用户自己的名片
$result = $mysql->query($sql);

$pushInfoCardDB = new CDBPushInfoCard();
$infoCardDB = new CDBInfoCard();

foreach($result as &$item) {
    $cardId = $item['id'];
    $userId = $item['userId'];

    $sql = "SELECT create_by FROM gener_receive_info_card where cardId = '$cardId' AND is_delete = 0";

    // 搜索用户接收的名片
    $accessUsers = $mysql->query($sql);

    if (count($accessUsers) > 0) {
        $accessUsers = array_column($accessUsers, 'create_by');
        foreach($accessUsers as $key=>$user) {
            $accessUsers[$key] = intval($user);
        }
    } else {
        $accessUsers = [];
    }

    array_push($accessUsers, intval($userId));

    $infoCard = new InfoCard($item);

    $modelId =  $userId.'_'.$cardId;
    $action = 1;    // 添加操作
    $jobStr = Util::getJobStrFormJobs($infoCard->jobs);
    $source = [
        'id' => intval($cardId),
        'remark'=>$infoCard->remark,
        'userId'=>intval($userId),
        'familyId'=>0,
        'personId'=>0,
        'name'=>$infoCard->name,
        'nameEn'=>$infoCard->nameEn,
        'address'=>$infoCard->address,
        'photo'=>$infoCard->photo,
        'backgroundImg'=>$infoCard->backgroundImg,
        'backgroundColor'=>$infoCard->backgroundColor,
        'backgroundType'=>$infoCard->backgroundType,
        'company'=>$infoCard->company,
        'companyEn'=>$infoCard->companyEn,
        'phone'=>Util::arrayToString($infoCard->phone),
        'jobs'=>$jobStr,
        'type'=>0,
        'email'=>$infoCard->email,
        'companyProfile'=>$infoCard->companyProfile,
        'access' => $accessUsers
    ];


    $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);

}
