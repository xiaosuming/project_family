<?php
/**
 * 更新名片中的position
 */
require_once('../vendor/autoload.php');

use DB\CDBInfoCard;
use DB\CDBManager;
use DB\CDBPushInfoCard;
use Util\Util;
use Model\InfoCard;

// $mysql = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
$mysql = new CDBManager('47.111.172.152', $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);

$sql = "SELECT id,jobs, position FROM gener_info_card WHERE is_delete = '0' ";

$result = $mysql->query($sql);

foreach($result as $infocard) {
    if ($infocard['position'] != '') {
        continue;
    }

    $jobs = $infocard['jobs'];

    if ($jobs != '') {
        $jobs = json_decode($jobs);
    }

    if (count($jobs) > 0) {
        $job = $jobs[count($jobs) - 1];
        $position = $job->job;

        $cardId = $infocard['id'];
        $update_sql = "UPDATE gener_info_card set position = '$position' WHERE id = '$cardId' ";
        $mysql->update($update_sql);
    }
}
