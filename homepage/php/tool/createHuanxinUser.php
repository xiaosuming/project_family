<?php
/**
 * 这是一个用来为数据库中用户创建环信账号的脚本
 */

require_once("../config/db_config.php");
require_once("../config/third_party.php");
require_once("../db/db_mysql_class.php");
require_once("../vendor/monolog/monolog/src/Monolog/Handler/HandlerInterface.php");
require_once("../vendor/monolog/monolog/src/Monolog/Handler/AbstractHandler.php");
require_once("../vendor/monolog/monolog/src/Monolog/Handler/AbstractProcessingHandler.php");
require_once("../vendor/monolog/monolog/src/Monolog/Logger.php");
require_once("../vendor/monolog/monolog/src/Monolog/Handler/StreamHandler.php");
require_once("../util/Http.class.php");
require_once("../util/Util.class.php");
require_once("../util/TempStorage.class.php");
require_once("../third_party/huanxin/Huanxin.class.php");
require_once('../vendor/autoload.php');

use DB\CDBManager;
use ThirdParty\Huanxin;
use Util\Util;

function createHuanxinUser(){
    $mysql = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
    $sql = "select id,username,hx_username,hx_password from gener_user where username != '' order by id desc";
    $result = $mysql->query($sql);
    echo count($result);
    $huanxin = new Huanxin($GLOBALS['hx_client_id'],$GLOBALS['hx_client_secret']);
    foreach($result as $user){
        if($user['hx_username'] == "" || $user['hx_password'] == ""){
            $hxUsername = md5("izuqun_".$user['username'] . "_" . Util::generateRandomCode(10));
            $hxPassword = Util::generateRandomCode(20);
            //为没有环信账号的用户创建环信账号
            if($huanxin->createUser($hxUsername,$hxPassword)){
                echo "创建环信账号成功\n";
            }else{
                exit("创建环信账号失败".$hxUsername);
            }

            $userId = $user['id'];
            //存回数据库
            $sql = "update gener_user set hx_username = '$hxUsername',hx_password = '$hxPassword' WHERE id = '$userId' ";
            $updateRow = $mysql->update($sql);

            if($updateRow > 0){
                echo "更新id为{$userId}的用户的环信账号，账号用户名为$hxUsername,密码是$hxPassword\n";
            }else{
                echo "更新出错，环信账号用户名为$hxUsername,密码是$hxPassword\n";
                exit();
            }
        }else{
            $userId = $user['id'];
            echo "id为{$userId}的用户已存在账号\n";
        }
    }
}

createHuanxinUser();