<?php
/**
 * 更新名片群体交换的人数
 * @Author: jiangpengfei
 * @Date:   2019-03-05
 */

// require_once("../config/db_config.php");
require_once("../config/db_config.izuqun.php");
require_once("../db/db_mysql_class.php");

use DB\CDBManager;

function generate(){

    $mysql = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);

    $sql = "SELECT id FROM gener_infocard_exchange WHERE isDelete = '0' ";

    $exchanges = $mysql->query($sql);
    foreach($exchanges as $exchange){
        $exchangeId = $exchange['id'];

        $sql = "SELECT count(id) FROM gener_infocard_exchange_record WHERE exchangeId = '$exchangeId' AND isDelete = '0' ";
        
        $result = $mysql->uniqueResult($sql);

        $total = $result['count(id)'];

        $sql = "UPDATE gener_infocard_exchange SET total = '$total' WHERE id = '$exchangeId' AND isDelete = '0' ";
        $mysql->update($sql);

        echo "更新交换 $exchangeId 的参与人数为 $total \n";
    }
}

generate();