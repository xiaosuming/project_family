<?php
/**
 * 自动将用户关注族群圈子
 * @Author: jiangpengfei
 * @Date:   2019-03-12
 */

require_once('../vendor/autoload.php');

use DB\CDBManager;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;


function createFamilyCircle(){

    //116.62.25.128
    $mysql = new CDBManager('116.62.25.128', 'family', 'Flare1111', 'family');
    $sql = "select id,socialCircleId from gener_family where is_delete = '0' AND socialCircleId > 0 order by id desc";
    $result = $mysql->query($sql);

    foreach($result as $family){

        try {
            $mysql->beginTransaction();
            $familyId = $family['id'];
            $socialCircleId = $family['socialCircleId'];

            // 获取家族的所有用户
            $sql = "SELECT userId FROM gener_person WHERE familyId = '$familyId' AND userId > 0 AND is_delete = 0 ";

            $users = $mysql->query($sql);
            foreach($users as $userId) {
                $userId = $userId['userId'];
                //用户关注圈子
                $sql = "INSERT INTO gener_information_follow (userId,categoryId,createTime,createBy,updateTime,updateBy)
                VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
                $followId = $mysql->insert($sql);
                if ($followId < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                    $mysql->rollback();
                    exit;
                }

                $sql = "UPDATE gener_information_category SET follow = follow+1 WHERE id = '$socialCircleId'";
                $updateRow = $mysql->update($sql);
                if ($updateRow <= 0) {
                    echo '更新关注数量失败';
                    $mysql->rollback();
                    exit;
                }
            }
            $mysql->commit();
        } catch(PDOException $e) {
            var_dump($e);
            $mysql->rollback();
        }
    }
}

createFamilyCircle();