<?php
/**
 * 自动创建没有圈子的族群的圈子
 * @Author: jiangpengfei
 * @Date:   2019-03-12
 */

require_once('../vendor/autoload.php');

use DB\CDBManager;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;


function createFamilyCircle(){
    $mysql = new CDBManager('116.62.25.128', 'family', 'Flare1111', 'family');
    $sql = "select id,photo,originator,name from gener_family where is_delete = '0' AND socialCircleId = '0' order by id desc";
    $result = $mysql->query($sql);

    foreach($result as $family){

        try {
            $mysql->beginTransaction();
            $userId = $family['originator'];
            $familyPhoto = $family['photo'];
            $familyName = $family['name'];
            $familyId = $family['id'];

            echo "更新家族".$familyId.PHP_EOL;

            //创建圈子
            $sql = "INSERT INTO gener_information_category (userId,avatar,name,circleType,create_by,create_time,
            update_by,update_time) VALUES ('$userId','$familyPhoto','$familyName',3,'$userId',now(),'$userId',now())";

            $socialCircleId = $mysql->insert($sql);

            if ($socialCircleId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '创建圈子失败');
                $mysql->rollback();
                exit;
            }

            // 更新族群的圈子
            $sql = "UPDATE gener_family SET socialCircleId = '$socialCircleId' WHERE id = '$familyId' ";
            $mysql->update($sql);

            //创始人关注圈子
            $sql = "INSERT INTO gener_information_follow (userId,categoryId,createTime,createBy,updateTime,updateBy)
            VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
            $followId = $mysql->insert($sql);
            if ($followId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                $mysql->rollback();
                exit;
            }

            $sql = "UPDATE gener_information_category SET follow=1 WHERE id = '$socialCircleId'";
            $updateRow = $mysql->update($sql);
            if ($updateRow <= 0) {
                echo '更新关注数量失败';
                $mysql->rollback();
                exit;
            }
            
            $mysql->commit();
        } catch(PDOException $e) {
            var_dump($e);
            $mysql->rollback();
        }
    }
}

createFamilyCircle();