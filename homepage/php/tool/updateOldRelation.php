<?php
/**
 * 更新旧的relation表示形式
 */

/**
 * 更新名片中的position
 */
require_once('../vendor/autoload.php');
use DB\CDBManager;
use Util\Util;

function relationSplit($str){
    if($str != ""&&$str != NULL)
        return explode("|", substr($str,0,strlen($str)-1));		//返回一个id数组
    else
        return array();											//返回一个空数组
}

function updateRelationStr($str) {
    $ids = relationSplit($str);
    $res = '';
    foreach($ids as $id) {
        $res .= '@'.$id.'|';
    }

    return $res;
}

$mysql = new CDBManager('47.111.172.152', $GLOBALS['db_user'], $GLOBALS['db_pwd'], 'family');

$sql = "SELECT * FROM gener_person";

$result = $mysql->query($sql);

foreach($result as $person) {
    $fatherStr = updateRelationStr($person['father']);
    $motherStr = updateRelationStr($person['mother']);
    $spouseStr = updateRelationStr($person['spouse']);
    $sisterStr = updateRelationStr($person['sister']);
    $brotherStr = updateRelationStr($person['brother']);
    $sonStr = updateRelationStr($person['son']);
    $daughterStr = updateRelationStr($person['daughter']);

    $personId = $person['id'];

    $sql = "UPDATE gener_person SET father = '$fatherStr', mother = '$motherStr', spouse = '$spouseStr', brother = '$brotherStr', sister = '$sisterStr', son = '$sonStr', daughter = '$daughterStr' where id = '$personId' ";

    $mysql->update($sql);
}

echo "更新完毕";
