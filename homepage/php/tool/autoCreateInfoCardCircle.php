<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 19-3-18
 * Time: 下午12:02
 */

use DB\CDBManager;
use Util\Util;

require_once '../vendor/autoload.php';

function createInfoCardCircle()
{
    $mysql = new CDBManager('116.62.25.128', 'family', 'Flare1111', 'family');
    $sql = "SELECT id,userId,name,photo  FROM gener_info_card WHERE is_delete = 0 AND socialCircleId =0 AND type = 0  ORDER BY id DESC";
    $infoCard = $mysql->query($sql);

    foreach ($infoCard as $item) {

        try {
            $mysql->beginTransaction();
            $userId = $item['userId'];
            $name = $item['name'];
            $photo = $item['photo'];
            $infoCardId = $item['id'];

            echo '更新名片' . $infoCardId . PHP_EOL;

            //创建圈子
            $sql = "INSERT INTO gener_information_category (userId,avatar,name,circleType,create_by,create_time,
            update_by,update_time) VALUES ('$userId','$photo','$name',5,'$userId',now(),'$userId',now())";

            $socialCircleId = $mysql->insert($sql);

            if ($socialCircleId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '创建圈子失败');
                $mysql->rollback();
                exit;
            }

            // 更新名片的圈子
            $sql = "UPDATE gener_info_card SET socialCircleId = '$socialCircleId' WHERE id = '$infoCardId' ";
            $mysql->update($sql);

            //创始人关注圈子
            $sql = "INSERT INTO gener_information_follow (userId,categoryId,createTime,createBy,updateTime,updateBy)
            VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
            $followId = $mysql->insert($sql);
            if ($followId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                $mysql->rollback();
                exit;
            }

            $sql = "UPDATE gener_information_category SET follow=1 WHERE id = '$socialCircleId'";
            $updateRow = $mysql->update($sql);
            if ($updateRow <= 0) {
                echo '更新关注数量失败';
                $mysql->rollback();
                exit;
            }

            $mysql->commit();
        } catch (PDOException $e) {
            var_dump($e);
            $mysql->rollback();
        }

    }
}

createInfoCardCircle();
