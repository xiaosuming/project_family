#!/bin/bash

docker run \
-p 6380:6379 \
-v $PWD/data:/data \
-v $PWD/conf/redis.conf:/etc/redis/redis.conf \
--privileged=true \
--name redis-family-auth \
-d redis redis-server /etc/redis/redis.conf
