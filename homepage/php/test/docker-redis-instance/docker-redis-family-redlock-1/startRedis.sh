#!/bin/bash

docker run \
-p 6383:6379 \
-v $PWD/data:/data \
-v $PWD/conf/redis.conf:/etc/redis/redis.conf \
--privileged=true \
--name redis-family-redlock-1 \
-d redis redis-server /etc/redis/redis.conf
