#!/bin/bash

docker run \
-p 6384:6379 \
-v $PWD/data:/data \
-v $PWD/conf/redis.conf:/etc/redis/redis.conf \
--privileged=true \
--name redis-family-redlock-2 \
-d redis redis-server /etc/redis/redis.conf
