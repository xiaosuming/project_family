<?php
require_once('../../vendor/autoload.php');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;
use ThirdParty\InstantMessage;

$client = new Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@www.xinhuotech.com:8080/2');
$logger = new Logger("logger");         //用来记录全局的异常警告日志
$handler = new Monolog\Handler\RavenHandler($client);
$handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));

$logger->pushHandler($handler);


function testCreateUser(){
    $imClient = new InstantMessage();
    var_dump($imClient->createUser('test_user_1','121212'));
}


function testCreateChatroom(){
    $imClient = new InstantMessage();
    $name = '测试聊天室';
    $description = '聊天室的描述';
    $maxusers = 500;
    $owner = 'izuqun_admin';
    $members = array('test_user');
    var_dump($imClient->createChatRoom($name, $description, $maxusers, $owner, $members));
}

function testDeleteChatroom(){
    $imClient = new InstantMessage();
    var_dump($imClient->deleteChatRoom('@TGS#3OMAC2CF2'));
}

function testAddUserToChatroom(){
    $imClient = new InstantMessage();
    var_dump($imClient->addUserToChatRoom('@TGS#3IC5B2CF4','izuqun_admin'));
    
}

function testSendTextMessageToUser(){
    $imClient = new InstantMessage();
    $sendUsers = array('test_user');
    $msgJson = "消息内容和";
    $ext = "扩展内容";
    var_dump($imClient->sendTextMessageToUser($sendUsers,$msgJson,$ext));
}

function testCreateChatGroup(){
    $imClient = new InstantMessage();
    $groupName = "测试聊天群";
    $description = "测试聊天群描述";
    $maxusers = 500;
    $owner = "izuqun_admin";
    $members = array('test_user');
    $info1['test1'] = "test1";
    $info1['test2'] = "test2";

    var_dump($imClient->createChatGroup($groupName,$description,$maxusers,$owner,$members,true,true,false,$info1));
}

function testBlockUser(){
    $imClient = new InstantMessage();
    var_dump($imClient->blockUser('izuqun_admin',array('test_user')));
}

function testDeleteBlockUser(){
    $imClient = new InstantMessage();
    var_dump($imClient->deleteBlockUser('izuqun_admin','test_user'));
}

testCreateUser();

testCreateChatroom();

testDeleteChatroom();
testAddUserToChatroom();

testSendTextMessageToUser();

testCreateChatGroup();

testBlockUser();
testDeleteBlockUser();