<?php 
    require_once('../config/db_config.php');

    $redis = new \Swoole\Redis;
    $redis->connect($GLOBALS['redis_host'], $GLOBALS['redis_port'],function ($redis, $result) {
        if ($result === false) {
            echo "connect to redis server failed.\n";
            return;
        }

        $redis->auth($GLOBALS['redis_pass'],function($redis,$result){
            $redis->set('test_key','hello world', function ($redis, $result) {
                if ($result === false) {
                    echo "error happend";
                    return;
                }
                $redis->get('test_key', function ($redis, $result) {
                    echo $result;
                });
            });
        });
    });