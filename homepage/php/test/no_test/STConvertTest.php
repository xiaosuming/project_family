<?php

require_once "../../vendor/autoload.php";

use Util\STConvert\STConvert;

$stConvert = new STConvert(STConvert::CONVERT_TYPE_S2T);

echo $stConvert->convert("赵");

$stConvert = new STConvert(STConvert::CONVERT_TYPE_T2S);

echo $stConvert->convert("趙");