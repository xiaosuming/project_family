#!/bin/bash
#自动化测试的脚本

# 执行某个测试类： ./run_db_unit_test.sh class_name.php
# 执行单个的测试用例: ./run_db_unit_test.sh class_name.php --filter test_case_name

if grep family_test ../config/db_config.php > /dev/null
then
    echo "database config file exist 'family_test',step 1(replace) skipped"
else
    # 将数据库改为测试数据库
    sed -i '/db_name/s/family/family_test/g' ../config/db_config.php
    sed -i '/redis_host/s/47.111.172.152/127.0.0.1/g' ../config/db_config.php
    sed -i '/HOST/s/47.111.172.152/127.0.0.1/g' ../config/RedisInstanceConfig.php
fi

# 自动启动所有需要的redis实例
bash startAllRedisInstance.sh

./../vendor/bin/phpunit  ${2} ${3} --configuration phpunit.xml db_test/${1}

# 数据库改回来

sed -i '/db_name/s/family_test/family/g' ../config/db_config.php
sed -i '/redis_host/s/127.0.0.1/47.111.172.152/g' ../config/db_config.php
sed -i '/HOST/s/127.0.0.1/47.111.172.152/g' ../config/RedisInstanceConfig.php

# 自动移除所有的redis实例
bash stopAllRedisInstance.sh
