<?php


use DB\CDBActivity;
use Model\Activity;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

class DBActivityTest extends TestCase
{

    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $activityDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->activityDB = new CDBActivity();
        return $this->createMySQLXMLDataSet('db_test/xml/activity_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBActivity::verifyUserIdAndActivityId()
     */
    public function testVerifyUserIdAndActivityId()
    {
        $this->assertFalse($this->activityDB->verifyUserIdAndActivityId(1, 11));
        $this->assertTrue($this->activityDB->verifyUserIdAndActivityId(1, 1));
    }

    /**
     * @covers DB\CDBActivity::createActivity()
     */
    public function testCreateActivity()
    {
        $familyId = 1;
        $title = 'title';
        $content = 'content';
        $photo = 'http://image1.jjjj.com';
        $startTime = '2018-05-04 09:09:09';
        $endTime = '2018-05-06 09:09:09';
        $deadline = '2018-05-05 10:00:00';
        $userId = 1;
        $coorX = 1;
        $coorY = 1;
        $address = 'address';
        $this->assertEquals(4, $this->activityDB->createActivity($familyId, $title, $content, $photo, 1, $startTime, $endTime, $deadline, $userId, $coorX, $coorY, $address));
    }

    /**
     * @covers DB\CDBActivity::updateActivity()
     */
    public function testUpdateActivity()
    {
        $activityId = 1;
        $title = 'title';
        $content = 'content';
        $photo = 'http://image1.jjjj.com';
        $startTime = '2018-05-04 09:09:09';
        $endTime = '2018-05-06 09:09:09';
        $deadline = '2018-05-05 10:00:00';
        $userId = 1;
        $coorX = 1;
        $coorY = 1;
        $address = 'address';
        $this->assertEquals(1, $this->activityDB->updateActivity($activityId, $title, $content, $photo, $startTime, $endTime, $deadline, $coorX, $coorY, $address, $userId));
    }

    /**
     * @covers DB\CDBActivity::deleteActivity()
     */
    public function testDeleteActivity()
    {
        $this->assertEquals(1, $this->activityDB->deleteActivity(1));
    }

    /**
     * @covers DB\CDBActivity::getActivity()
     */
    public function testGetActivity()
    {
        $this->assertNull($this->activityDB->getActivity(111));
        $activity = new Activity(array());
        $activity->id = '3';
        $activity->familyId = '1';
        $activity->familyName = 'jiazu';
        $activity->title = 'title2';
        $activity->content = 'content2';
        $activity->photo = 'photo2';
        $activity->startTime = "2019-09-09 09:09:09";
        $activity->endTime = "2021-09-09 10:09:09";
        $activity->deadline = '2020-09-09 09:19:09';
        $activity->type = '1';
        $activity->coorX = '1';
        $activity->coorY = '1';
        $activity->address = 'address';
        $activity->createByName = 'jiang';
        $activity->createTime = '2017-09-09 09:09:09';
        $activity->createBy = '1';
        $activity->updateTime = '2017-09-09 09:09:09';
        $activity->updateBy = '1';
        $activity->nickname = 'jiang111';
        $activity->socialCircleId = '0';
        $activity->userPhoto = 'http://test.photo';
        $this->assertEquals($activity, $this->activityDB->getActivity(3));
    }

    /**
     * @covers DB\CDBActivity::getActivityCount()
     */
    public function testGetActivityCount()
    {
        $this->assertEquals(3, $this->activityDB->getActivityCount(1, 1));
        $this->assertEquals(3, $this->activityDB->getActivityCount(-1, 1));
    }

    /**
     * @covers DB\CDBActivity::getActivityPaging()
     */
    public function testGetActivityPaging()
    {
        $array = [[
            'id' => "3",
            'familyId' => "1",
            'title' => 'title2',
            'photo' => 'photo2',
            'startTime' => '2019-09-09 09:09:09',
            'endTime' => '2021-09-09 10:09:09',
            'deadline' => '2020-09-09 09:19:09',
            'coorX' => "1",
            'coorY' => "1",
            'type' => '1',
            'address' => "address",
            'createBy' => "1",
            'createTime' => "2017-09-09 09:09:09",
            'updateBy' => "1",
            'updateTime' => "2017-09-09 09:09:09"
        ]];
        $this->assertEquals($array, $this->activityDB->getActivityPaging(1, 1, 1, 1));
        $this->assertEquals($array, $this->activityDB->getActivityPaging(1, 1, -1, 1));
    }


    /**
     * @covers DB\CDBActivity::getActivedActivityCount()
     */
    public function testGetActivedActivityCount()
    {
        $this->assertEquals(2, $this->activityDB->getActivedActivityCount(1, 1));
        $this->assertEquals(2, $this->activityDB->getActivedActivityCount(-1, 1));
    }

    /**
     * @covers DB\CDBActivity::getActivedActivityPaging()
     */
    public function testGetActivedActivityPaging()
    {
        $this->assertEquals(1, count($this->activityDB->getActivedActivityPaging(1, 1, 1, 1)));
        $this->assertEquals(1, count($this->activityDB->getActivedActivityPaging(1, 1, -1, 1)));
    }

    /**
     * @covers DB\CDBActivity::getOverdueActivityCount()
     */
    public function testGetOverdueActivityCount()
    {
        $this->assertEquals(1, $this->activityDB->getOverdueActivityCount(1, 1));
        $this->assertEquals(1, $this->activityDB->getOverdueActivityCount(-1, 1));
    }

    /**
     * @covers DB\CDBActivity::getOverdueActivityPaging()
     */
    public function testGetOverdueActivityPaging()
    {
        $array = [[
            'id' => "1",
            'familyId' => "1",
            'title' => "title",
            'photo' => "photo",
            'startTime' => "2017-09-09 09:09:09",
            'endTime' => "2017-09-09 10:09:09",
            'deadline' => "2017-09-09 09:19:09",
            'coorX' => "1",
            'coorY' => "1",
            'type' => '1',
            'address' => "address",
            'createBy' => "2",
            'createTime' => "2017-09-09 09:09:09",
            'updateBy' => "2",
            'updateTime' => "2017-09-09 09:09:09"
        ]];
        $this->assertEquals($array, $this->activityDB->getOverdueActivityPaging(1, 1, 1, 1));
        $this->assertEquals($array, $this->activityDB->getOverdueActivityPaging(1, 1, -1, 1));
    }

    /**
     * @covers DB\CDBActivity::getUserCreatedActivityPaging()
     */
    public function testGetUserCreatedActivityPaging()
    {
        $array = [[
            'id' => "3",
            'familyId' => "1",
            'title' => 'title2',
            'photo' => 'photo2',
            'startTime' => '2019-09-09 09:09:09',
            'endTime' => '2021-09-09 10:09:09',
            'deadline' => '2020-09-09 09:19:09',
            'coorX' => "1",
            'coorY' => "1",
            'type' => '1',
            'address' => "address",
            'createBy' => "1",
            'createTime' => "2017-09-09 09:09:09",
            'updateBy' => "1",
            'updateTime' => "2017-09-09 09:09:09",
            'socialCircleId' => '0'
        ]];
        $this->assertEquals($array, $this->activityDB->getUserCreatedActivityPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBActivity::getUserCreatedActivityCount()
     */
    public function testGetUserCreatedActivityCount()
    {
        $this->assertEquals(2, $this->activityDB->getUserCreatedActivityCount(1));
    }

    /**
     * @covers DB\CDBActivity::getUserParticipateActivityPaging()
     */
    public function testGetUserParticipateActivityPaging()
    {
        $array = [[
            'id' => "1",
            'familyId' => "1",
            'title' => 'title',
            'photo' => 'photo',
            'startTime' => '2017-09-09 09:09:09',
            'endTime' => '2017-09-09 10:09:09',
            'deadline' => '2017-09-09 09:19:09',
            'coorX' => "1",
            'coorY' => "1",
            'address' => "address",
            'createBy' => "2",
            'createTime' => "2017-09-09 09:09:09",
            'updateBy' => "2",
            'updateTime' => "2017-09-09 09:09:09"
        ]];
        $this->assertEquals($array, $this->activityDB->getUserParticipateActivityPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBActivity::getUserParticipateActivityCount()
     */
    public function testGetUserParticipateActivityCount()
    {
        $this->assertEquals(1, $this->activityDB->getUserParticipateActivityCount(1));
    }

    /**
     * @covers DB\CDBActivity::participate()
     */
    public function testParticipate()
    {
        $this->assertFalse(false, $this->activityDB->participate(1, 1));
        $this->assertEquals('活动不存在', $this->activityDB->participate(11, 1));
        $this->assertEquals(3, $this->activityDB->participate(2, 1));
        $this->assertEquals(2, $this->activityDB->participate(3, 1));
    }

    /**
     * @covers DB\CDBActivity::cancelParticipate()
     */
    public function testCancelParticipate()
    {
        $this->assertEquals(1, $this->activityDB->cancelParticipate(1, 1));
    }

    /**
     * @covers DB\CDBActivity::checkActivityParticipateStatus()
     */
    public function testCheckActivityParticipateStatus()
    {
        $this->assertEquals(1, $this->activityDB->checkActivityParticipateStatus(1, 1));
        $this->assertEquals(0, $this->activityDB->checkActivityParticipateStatus(11, 1));
        $this->assertEquals(0, $this->activityDB->checkActivityParticipateStatus(3, 1));
    }

    /**
     * @covers DB\CDBActivity::getActivityParticipator()
     */
    public function testGetActivityParticipator()
    {
        $array = [[
            'id' => "1",
            'username' => "jiang",
            'nickname' => "jiang111",
            'photo' => "http://test.photo"
        ]];
        $this->assertEquals($array, $this->activityDB->getActivityParticipator(1));
    }

    /**
     * @covers DB\CDBActivity::getActivityParticipatorCount()
     */
    public function testGetActivityParticipatorCount()
    {
        $this->assertEquals(1, $this->activityDB->getActivityParticipatorCount(1));
    }

    /**
     * @covers DB\CDBActivity::getActivityParticipatorPage()
     */
    public function testGetActivityParticipatorPage()
    {
        $array = [[
            'id' => "1",
            'username' => "jiang",
            'nickname' => "jiang111",
            'photo' => "http://test.photo"
        ]];
        $this->assertEquals($array, $this->activityDB->getActivityParticipatorPage(1, 1, 1));
    }

    /**
     * @covers DB\CDBActivity::getActivityParticipatorByIds()
     */
    public function testGetActivityParticipatorByIds()
    {
        $array = [[
            'id' => "1",
            'activityId' => '1',
            'username' => "jiang",
            'nickname' => "jiang111",
            'photo' => "http://test.photo"
        ]];
        $this->assertNull($this->activityDB->getActivityParticipatorByIds('ididid'));
        $this->assertEquals($array, $this->activityDB->getActivityParticipatorByIds([1]));
        $this->assertEquals(1,count($this->activityDB->getActivityParticipatorByIds(array(1,2))));
    }

    /**
     * @covers DB\CDBActivity::addActivityComment()
     */
    public function testAddActivityComment()
    {
        $activityId = 1;
        $userId = 1;
        $replyTo = 2;
        $replyToUserId = 1;
        $comment = 'comment1';
        $path = 0;
        $this->assertEquals(2, $this->activityDB->addActivityComment($activityId, $userId, $replyTo, $replyToUserId, $comment, $path));
    }

    /**
     * @covers DB\CDBActivity::updatePath()
     */
    public function testUpdatePath()
    {
        $this->assertEquals(1, $this->activityDB->updatePath(1));
    }

    /**
     * @covers DB\CDBActivity::getActivityComment()
     */
    public function testGetActivityComment()
    {
        $array = [[
            'replytoUsername' => "jiang111",
            'replytoUserPhoto' => "http://test.photo",
            'photo' => "http://test.photo",
            'nickname' => "jiang111",
            'id' => "1",
            'activityId' => "1",
            'userId' => "1",
            'replyto' => "1",
            'replytoUserId' => "1",
            'comment' => "comment",
            'createTime' => "2017-09-09 09:09:09",
            'createBy' => "1",
            'updateTime' => "2017-09-09 09:09:09",
            'updateBy' => "1"
        ]];
        $this->assertEquals($array, $this->activityDB->getActivityComment(1));
    }

    /**
     * @covers DB\CDBActivity::selectPath()
     */
    public function testSelectPath()
    {
        $array = [
            'id' => "1",
            'path' => "0",
            'replyTo' => "1"
        ];

        $this->assertEquals($array, $this->activityDB->selectPath(1));
        $this->assertFalse($this->activityDB->selectPath(11));
    }

    /**
     * @covers DB\CDBActivity::deleteReplyComment()
     */
    public function testDeleteReplyComment()
    {
        $this->assertEquals(0, $this->activityDB->deleteReplyComment(1));
    }

    /**
     * @covers DB\CDBActivity::deleteComment()
     */
    public function testDeleteComment()
    {
        $this->assertEquals(1, $this->activityDB->deleteComment(1));
    }

    /**
     * @covers DB\CDBActivity::verifyCommentIdAndUserId()
     */
    public function testVerifyCommentIdAndUserId()
    {
        $this->assertTrue($this->activityDB->verifyCommentIdAndUserId(1, 1));
        $this->assertTrue($this->activityDB->verifyCommentIdAndUserId(1, 2));
        $this->assertFalse($this->activityDB->verifyCommentIdAndUserId(11, 1));
        $this->assertFalse($this->activityDB->verifyCommentIdAndUserId(1, 3));

    }

}