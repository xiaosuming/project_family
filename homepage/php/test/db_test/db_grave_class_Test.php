<?php

use DB\CDBGrave;
use Model\Grave;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-20
 * Time: 下午4:37
 */
class DBGraveTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;
    private $conn = null;
    private $graveDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    protected function getDataSet()
    {
        $this->graveDB = new CDBGrave();
        return $this->createMySQLXMLDataSet('db_test/xml/grave_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBGrave::verifyUserIdAndGraveId()
     */
    public function testVerifyUserIdAndGraveId()
    {
        $this->assertTrue($this->graveDB->verifyUserIdAndGraveId(1, 1));
        $this->assertTrue($this->graveDB->verifyUserIdAndGraveId(2, 2));
        $this->assertFalse($this->graveDB->verifyUserIdAndGraveId(3, 2));
    }

    /**
     * @covers DB\CDBGrave::checkPersonGraveExist()
     */
    public function testCheckPersonGraveExist()
    {
        $this->assertTrue($this->graveDB->checkPersonGraveExist(1));
        $this->assertFalse($this->graveDB->checkPersonGraveExist(111));
    }

    /**
     * @covers DB\CDBGrave::addGrave()
     */
    public function testAddGrave()
    {
        $personId = 1;
        $personName = 'name';
        $familyId = 1;
        $coorX = 12;
        $coorY = 12;
        $address = 'address';
        $photo = 'oooo';
        $description = 'ssss';
        $userId = 1;
        $this->assertEquals(3, $this->graveDB->addGrave($personId, $personName, $familyId, $coorX, $coorY, $address, $photo, $description, $userId));
    }

    /**
     * @covers DB\CDBGrave::updateGrave()
     */
    public function testUpdateGrave()
    {
        $graveId = 1;
        $coorX = 14;
        $coorY = 14;
        $address = 'address';
        $photo = 'photo11';
        $description = 'sdsdsd';
        $userId = 1;
        $this->assertEquals(1, $this->graveDB->updateGrave($graveId, $coorX, $coorY, $address, $photo, $description, $userId));
    }

    /**
     * @covers DB\CDBGrave::getGrave()
     */
    public function testGetGrave()
    {
        $grave = new Grave(array());
        $grave->id = 1;
        $grave->personId = 1;
        $grave->personName = 'name';
        $grave->familyId = 1;
        $grave->coorX = 11;
        $grave->coorY = 11;
        $grave->address = 'address';
        $grave->photo = 'photo';
        $grave->description = 'dessss';
        $grave->createTime = '2018-06-20 12:00:00';
        $grave->createBy = 1;
        $grave->updateTime = '2018-06-20 12:00:00';
        $grave->updateBy = 1;
        $this->assertEquals($grave, $this->graveDB->getGrave(1));
        $this->assertNull($this->graveDB->getGrave(111));
    }


    /**
     * @covers DB\CDBGrave::deleteGrave()
     */
    public function testDeleteGrave()
    {
        $this->assertEquals(1, $this->graveDB->deleteGrave(1));
    }

    /**
     * @covers DB\CDBGrave::getGravesInfoPaging()
     */
    public function testGetGravesInfoPaging()
    {
        $array = [[
            'id' => "2",
            'personId' => "1",
            'personName' => "name",
            'familyId' => "1",
            'coorX' => "11",
            'coorY' => "11",
            'address' => "address",
            'photo' => "photo",
            'description' => "dessss",
            'createTime' => "2018-06-20 12:00:00",
            'createBy' => "2",
            'updateTime' => "2018-06-20 12:00:00",
            'updateBy' => "2"
        ]];
        $this->assertEquals($array, $this->graveDB->getGravesInfoPaging(1, 1, 1));
    }

    /**
     * @covers  DB\CDBGrave::getGravesInfoCount()
     */
    public function testGetGravesInfoCount()
    {
        $this->assertEquals(2, $this->graveDB->getGravesInfoCount(1));
    }

    /**
     * @covers DB\CDBGrave::getGraveInfoByPersonId()
     */
    public function testGetGraveInfoByPersonId()
    {
        $array = [[
            'id' => "1",
            'personId' => "1",
            'personName' => "name",
            'familyId' => "1",
            'coorX' => "11",
            'coorY' => "11",
            'address' => "address",
            'photo' => "photo",
            'description' => "dessss",
            'createTime' => "2018-06-20 12:00:00",
            'createBy' => "1",
            'updateTime' => "2018-06-20 12:00:00",
            'updateBy' => "1"
        ],
            [
                'id' => "2",
                'personId' => "1",
                'personName' => "name",
                'familyId' => "1",
                'coorX' => "11",
                'coorY' => "11",
                'address' => "address",
                'photo' => "photo",
                'description' => "dessss",
                'createTime' => "2018-06-20 12:00:00",
                'createBy' => "2",
                'updateTime' => "2018-06-20 12:00:00",
                'updateBy' => "2"
            ]
        ];
        $this->assertEquals($array, $this->graveDB->getGraveInfoByPersonId(1));
    }

    /**
     * @covers DB\CDBGrave::getUnCreateGravePersonListByFamilyId
     */
    public function testGetUnCreateGravePersonListByFamilyId()
    {
        $array = array(array(
            'id' => "2",
            'personName' => "1",
            'familyName' => "haha"));

        $this->assertEquals($array,$this->graveDB->getUnCreateGravePersonListByFamilyId(1));
    }
}