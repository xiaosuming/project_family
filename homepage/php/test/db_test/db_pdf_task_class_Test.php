<?php

use DB\CDBPdfTask;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-7-4
 * Time: 下午2:11
 */
class DBPdfTaskTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;

    private $conn = null;

    private $pdfDB = null;


    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->pdfDB = new CDBPdfTask();
        return $this->createMySQLXMLDataSet(__DIR__ . '/xml/pdf_task_dataset.xml');     //默认的数据集
    }


    /**
     * @covers \DB\CDBPdfTask::addPdfTask
     */
    public function testAddPdfTask()
    {
        $userId = 1;
        $familyId = 1;
        $familyTreeName = 'family';
        $direction = 'direction';
        $this->assertEquals(2, $this->pdfDB->addPdfTask($userId, $familyId, $familyTreeName, $direction));
    }

    /**
     * @covers \DB\CDBPdfTask::updatePdf
     */
    public function testUpdatePdf()
    {
        $pdfId = 1;
        $previewPdf = 'preview_url';
        $printPdf = 'print_url';
        $this->assertEquals(1, $this->pdfDB->updatePdf($pdfId, $previewPdf, $printPdf));
    }

    /**
     * @covers \DB\CDBPdfTask::getPdf
     */
    public function testGetPdf()
    {
        $array = [
            'id' => 1,
            'familyId' => 22,
            'preview_pdf' => 1,
            'print_pdf' => 1,
            'finish' => 1,
            'create_by' => 1,
            'create_time' => '2018-07-03 11:11:11',
            'update_by' => 1,
            'update_time' => '2018-07-03 11:11:11'
        ];
        $this->assertEquals($array, $this->pdfDB->getPdf(1));
    }

    /**
     * @covers \DB\CDBPdfTask::addFamilyPdfTask
     */
    public function testAddFamilyPdfTask()
    {
        $this->assertEquals(2, $this->pdfDB->addFamilyPdfTask(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1));
    }

    /**
     * @covers \DB\CDBPdfTask::getFamilyPdfTaskDetail
     */
    public function testGetFamilyPdfTaskDetail()
    {
        $array = [
            'id' => 1,
            'familyId' => 22,
            'preview_pdf' => "1",
            'print_pdf' => "1",
            'familyTreeName' => "name",
            'direction' => "1",
            'eventIds' => "[]",
            'photoIdOnes' => "[]",
            'photoIds' => "[]",
            'graveIds' => "[]",
            'finish' => "1",
            'isHD' => '0',
            'startPersonId' => '0',
            'create_time' => "2018-07-03 11:11:11",
            'create_by' => "1",
            'update_time' => "2018-07-03 11:11:11",
            'update_by' => "1"
        ];
        $this->assertEquals($array, $this->pdfDB->getFamilyPdfTaskDetail(1));
    }
}
