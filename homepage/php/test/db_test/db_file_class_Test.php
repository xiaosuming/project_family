<?php

use DB\CDBFile;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-20
 * Time: 上午10:50
 */
class DBFileTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;
    private $conn = null;
    private $fileDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    protected function getDataSet()
    {
        $this->fileDB = new CDBFile();
        return $this->createMySQLXMLDataSet('db_test/xml/file_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBFile::addFile()
     */
    public function testAddFile()
    {
        $filePath = 'file';
        $module = 1;
        $userId = 1;
        $this->assertEquals(2, $this->fileDB->addFile($filePath, $module, $userId));
    }

    /**
     * @covers DB\CDBFile::getFilesPaging()
     */
    public function testGetFilesPaging()
    {
        $array = [[
            'filePath' => "file_path",
            'module' => "1",
            'createBy' => "1",
            'createTime' => "2018-06-20 12:00:00"
        ]];
        $this->assertEquals($array,$this->fileDB->getFilesPaging(1, 1, 1, 1));
        $this->assertEquals($array,$this->fileDB->getFilesPaging(1, 1, -1, 1));
    }

    /**
     * @covers DB\CDBFile::getFilesCount()
     */
    public function testGetFilesCount(){
        $this->assertEquals(1,$this->fileDB->getFilesCount(1,1));
        $this->assertEquals(1,$this->fileDB->getFilesCount(-1,1));
    }
}