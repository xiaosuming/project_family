<?php

use DB\CDBLike;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-20
 * Time: 下午6:31
 */
class DBLikeTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $likeDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {

            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->likeDB = new CDBLike();
        return $this->createMySQLXMLDataSet('db_test/xml/like_dataset.xml');     //默认的数据集
    }


    /**
     * @covers DB\CDBLike::checkLike()
     */
    public function testCheckLike()
    {
        $this->assertEquals(1,$this->likeDB->checkLike(1,1,1));
        $this->assertEquals(-1,$this->likeDB->checkLike(1,11,11));
    }

    /**
     * @covers DB\CDBLike::like()
     */
    public function testLike()
    {
        $this->assertEquals(-1,$this->likeDB->like(1,1,1));
    }
}
