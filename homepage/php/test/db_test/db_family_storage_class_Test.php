<?php

use DB\CDBFamilyStorage;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-15
 * Time: 下午1:04
 */
class DBFamilyStorageTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $familyShareDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    public function getDataSet()
    {
        $this->familyShareDB = new CDBFamilyStorage();
        return $this->createMySQLXMLDataSet('db_test/xml/family_storage_dataset.xml');     //默认的数据集
    }

    /**
     * 测试创建目录
     * @covers DB\CDBFamilyStorage::createDir()
     */
    public function testCreateDir()
    {
        $familyId = 1;
        $dirName = 'dirName';
        $parent = 1;
        $parent_path = '0,1';
        $size = 100;
        $file_type = 2;
        $md5 = '12233';
        $userId = 1;
        $this->assertEquals(5, $this->familyShareDB->createDir($familyId, $dirName, $parent, $parent_path, $size, $file_type, $md5, $userId));
    }

    /**
     * 检查当前目录名字在该文件夹下是否存在
     * @covers DB\CDBFamilyStorage::checkDirNameExistsInFamily()
     */
    public function testCheckDirNameExistsInFamily()
    {
        $this->assertEquals(1, $this->familyShareDB->checkDirNameExistsInFamily(1, 'name', 0));
        $this->assertEquals(0,$this->familyShareDB->checkDirNameExistsInFamily(1, 'name', 1));
    }

    /**
     * 获取当前文件夹下的MD5值 防止文件的重复上传
     * @covers DB\CDBFamilyStorage::checkFileExistsInDir()
     */
    public function testCheckFileExistsInDir()
    {
        $this->assertEquals(1, $this->familyShareDB->checkFileExistsInDir(1, 1, '121212'));
    }

    /**
     * @covers DB\CDBFamilyStorage::getContentsInDirByMd5()
     */
    public function testGetContentsInDirByMd5()
    {
        $array = [[
            'source_ip' => "123",
            'group_name' => "group_name",
            'remote_filename' => "remote_filename",
            'file_url' => "file_url",
            'size' => "1000",
            'file_type' => "1"
        ]];
        $this->assertEquals($array, $this->familyShareDB->getContentsInDirByMd5(1, 1, '121212'));
    }

    /**
     * @covers DB\CDBFamilyStorage::uploadFileToDir()
     */
    public function testUploadFileToDir()
    {
        $familyId = 1;
        $fileName = 'filename1';
        $parentId = 1;
        $parent_path = '0,1';
        $source_ip = '123333';
        $group_name = 'groupname1';
        $remote_filename = 'remotefilename1';
        $file_url = 'fileurl1';
        $size = '1000';
        $file_type = 2;
        $md5 = '1111111';
        $userId = 1;
        $this->assertEquals(5, $this->familyShareDB->uploadFileToDir($familyId, $fileName, $parentId, $parent_path, $source_ip, $group_name, $remote_filename, $file_url, $size, $file_type, $md5, $userId));
    }

    /**
     * @covers DB\CDBFamilyStorage::getDirNameById()
     */
    public function testGetDirNameById()
    {
        $array = [
            'name' => 'name',
            'parent_path' => 0
        ];
        $this->assertEquals($array, $this->familyShareDB->getDirNameById(1));
        $this->assertFalse($this->familyShareDB->getDirNameById(1111));
    }


    /**
     * @covers DB\CDBFamilyStorage::getFamilyIdById()
     */
    public function testGetFamilyIdById()
    {
        $this->assertEquals(1, $this->familyShareDB->getFamilyIdById(1));
        $this->assertFalse($this->familyShareDB->getFamilyIdById(111));
    }

    /**
     * @covers DB\CDBFamilyStorage::getFamilyIdByIdInRecycle()
     */
    public function testGetFamilyIdByIdInRecycle()
    {
        $this->assertFalse($this->familyShareDB->getFamilyIdByIdInRecycle(2));
        $this->assertEquals(1,$this->familyShareDB->getFamilyIdByIdInRecycle(3));
    }

    /**
     * @covers DB\CDBFamilyStorage::checkDirExists()
     */
    public function testCheckDirExists()
    {
        $this->assertEquals(1, $this->familyShareDB->checkDirExists(1));
    }

    /**
     * @covers DB\CDBFamilyStorage::checkFileExists()
     */
    public function testCheckFileExists()
    {
        $this->assertEquals(1, $this->familyShareDB->checkFileExists(2));
        $this->assertNull(null, $this->familyShareDB->checkFileExists(22));
    }

    /**
     * @covers DB\CDBFamilyStorage::checkFileExistsInRecycle()
     */
    public function testCheckFileExistsInRecycle()
    {
        $this->assertFalse(false, $this->familyShareDB->checkFileExistsInRecycle(1));
        $this->assertFalse(false, $this->familyShareDB->checkFileExistsInRecycle(2));
    }

    /**
     * @covers DB\CDBFamilyStorage::changeDirStatus()
     */
    public function testChangeDirStatus()
    {
        $this->assertEquals(1, $this->familyShareDB->changeDirStatus(1));
        $this->assertEquals(0, $this->familyShareDB->changeDirStatus(2));
    }

    /**
     * @covers DB\CDBFamilyStorage::changeChildrenDirStatus()
     */
    public function testChangeChildrenDirStatus()
    {
        $this->assertEquals(1, $this->familyShareDB->changeChildrenDirStatus(1));
    }

    /**
     * @covers DB\CDBFamilyStorage::changeFileStatus()
     */
    public function testChangeFileStatus()
    {
        $this->assertEquals(1, $this->familyShareDB->changeFileStatus(2));
    }

    /**
     * @covers DB\CDBFamilyStorage::getAllFiles()
     */
    public function testGetAllFiles()
    {
        $array = [[
            'id' => "2",
            'familyId' => "1",
            'name' => "name",
            'parent' => "1",
            'parent_path' => "0,1",
            'file_url' => "file_url",
            'size' => "1000",
            'file_type' => "1",
            'md5' => "121212",
            'create_by' => "1",
            'create_time' => "2018-06-19 12:00:00",
            'update_by' => "1",
            'update_time' => "2018-06-19 12:00:00"
        ]];
        $this->assertEquals($array, $this->familyShareDB->getAllFiles(1));
    }

    /**
     * @covers DB\CDBFamilyStorage::getFilesByFileId()
     */
    public function testGetFilesByFileId()
    {
        $array = ['id' => "2",
            'familyId' => "1",
            'name' => "name",
            'parent' => "1",
            'parent_path' => "0,1",
            'file_url' => "file_url",
            'size' => "1000",
            'file_type' => "1",
            'md5' => "121212",
            'create_by' => "1",
            'create_time' => "2018-06-19 12:00:00",
            'update_by' => "1",
            'update_time' => "2018-06-19 12:00:00"
        ];

        $this->assertEquals($array, $this->familyShareDB->getFilesByFileId(2));
        $this->assertFalse($this->familyShareDB->getFilesByFileId(222));
    }

    /**
     * @covers DB\CDBFamilyStorage::renameFileName()
     */
    public function testRenameFileName()
    {
        $this->assertEquals(1, $this->familyShareDB->renameFileName('name1', 1));
    }

    /**
     * @covers DB\CDBFamilyStorage::getFilesByRecycle()
     */
    public function testGetFilesByRecycle()
    {
        $result = $this->familyShareDB->getFilesByRecycle(1);
        $this->assertEquals(4, $result[0]['id']);
    }

    /**
     * @covers DB\CDBFamilyStorage::recoverFileFromRecycle()
     */
    public function testRecoverFileFromRecycle()
    {
        $this->assertEquals(0, $this->familyShareDB->recoverFileFromRecycle(1));
    }

    /**
     * @covers DB\CDBFamilyStorage::recoverChildrenFileFromRecycle()
     */
    public function testRecoverChildrenFileFromRecycle()
    {
        $this->assertEquals(1, $this->familyShareDB->recoverChildrenFileFromRecycle(1));
    }

    /**
     * @covers DB\CDBFamilyStorage::deleteFilesFromRecycle()
     */
    public function testDeleteFilesFromRecycle()
    {
        $this->assertEquals(2, $this->familyShareDB->deleteFilesFromRecycle());
    }

    /**
     * @covers DB\CDBFamilyStorage::getParentAndNameInRecycle()
     */
    public function testGetParentAndNameInRecycle()
    {
        $this->assertFalse(false, $this->familyShareDB->getParentAndNameInRecycle(1));
        $result = $this->familyShareDB->getParentAndNameInRecycle(3);
        $this->assertEquals('name', $result['name']);
    }

    /**
     * @covers DB\CDBFamilyStorage::getParentIdAndName()
     */
    public function testGetParentIdAndName()
    {
        $array = [
            'name' => 'name',
            'parent' => 0,
            'is_dir' => 1
        ];
        $this->assertEquals($array, $this->familyShareDB->getParentIdAndName(1));
        $this->assertFalse($this->familyShareDB->getParentIdAndName(111));
    }

    /**
     * @covers DB\CDBFamilyStorage::getFileNameByParentPath()
     */
    public function testGetFileNameByParentPath()
    {
        $array = [[
            'name' => 'name'
        ]];
        $this->assertEquals($array, $this->familyShareDB->getFileNameByParentPath(1, 1));
    }

    /**
     * @covers DB\CDBFamilyStorage::rename()
     */
    public function testRename()
    {
        $this->assertEquals(1, $this->familyShareDB->rename('name1', 1));
    }

    /**
     * @covers DB\CDBFamilyStorage::getDirNameByParentPath()
     */
    public function testGetDirNameByParentPath()
    {
        $array = [[
            'name' => 'name'
        ]];
        $this->assertEquals($array, $this->familyShareDB->getDirNameByParentPath(0, 1));
    }

    /**
     * @covers DB\CDBFamilyStorage::getContentByDirId()
     */
    public function testGetContentByDirId()
    {
        $array = [[
            'id' => "2",
            'familyId' => "1",
            'name' => "name",
            'parent' => "1",
            'parent_path' => "0,1",
            'group_name' => "group_name",
            'remote_filename' => "remote_filename",
            'file_url' => "file_url",
            'size' => "1000",
            'file_type' => "1",
            'md5' => "121212",
            'is_dir' => "0",
            'create_by' => "1",
            'create_time' => "2018-06-19 12:00:00",
            'update_by' => "1",
            'update_time' => "2018-06-19 12:00:00"
        ]];
        $this->assertEquals($array, $this->familyShareDB->getContentByDirId(1));
    }

    /**
     * @covers DB\CDBFamilyStorage::getRootDir()
     */
    public function testGetRootDir()
    {
        $array = [[
            'id' => "1",
            'familyId' => "1",
            'name' => "name",
            'parent' => "0",
            'parent_path' => "0",
            'size' => "1000",
            'file_type' => "1",
            'file_url' => "file_url",
            'is_dir' => "1",
            'create_by' => "1",
            'create_time' => "2018-06-19 12:00:00",
            'update_by' => "1",
            'update_time' => "2018-06-19 12:00:00"
        ]];
        $this->assertEquals($array, $this->familyShareDB->getRootDir(1));
    }

    /**
     * @covers DB\CDBFamilyStorage::isCreatorForStorage()
     */
    public function testIsCreatorForStorage()
    {
        $this->assertTrue($this->familyShareDB->isCreatorForStorage(1, 1, 1));
        $this->assertFalse($this->familyShareDB->isCreatorForStorage(11, 1, 1));
        $this->assertFalse($this->familyShareDB->isCreatorForStorage(1, 1, 9));
    }

    /**
     * @covers DB\CDBFamilyStorage::getContentFromStorageLimit()
     */
    public function testGetContentFromStorageLimit()
    {
        $array = [
            'id' => "1",
            'familyId' => "1",
            'max_size' => "1000",
            'used_size' => "500",
            'recycle_size' => "200",
            'create_time' => "2018-06-19",
            'update_time' => "2018-06-19"
        ];
        $this->assertEquals($array, $this->familyShareDB->getContentFromStorageLimit(1));
        $this->assertFalse($this->familyShareDB->getContentFromStorageLimit(2));
    }

    /**
     * @covers DB\CDBFamilyStorage::updateUsedSize()
     */
    public function testUpdateUsedSize()
    {
        $this->assertEquals(1, $this->familyShareDB->updateUsedSize(1, 600));
    }

    /**
     * @covers DB\CDBFamilyStorage::updateUsedSizeForDel()
     */
    public function testUpdateUsedSizeForDel()
    {
        $this->assertEquals(1, $this->familyShareDB->updateUsedSizeForDel(1, 600, 300));
    }

    /**
     * @covers DB\CDBFamilyStorage::getSumSizeByDirId()
     */
    public function testGetSumSizeByDirId()
    {
        $this->assertEquals(2000, $this->familyShareDB->getSumSizeByDirId(1, 1));
        $this->assertFalse($this->familyShareDB->getSumSizeByDirId(4, 2));
    }

    /**
     * @covers DB\CDBFamilyStorage::getFileSizeByFileId()
     */
    public function testGetFileSizeByFileId()
    {
        $this->assertEquals(1000, $this->familyShareDB->getFileSizeByFileId(2, 1));
        $this->assertFalse($this->familyShareDB->getFileSizeByFileId(3, 1));
    }

    /**
     * @covers DB\CDBFamilyStorage::getFileSizeByFileIdInRecycle()
     */
    public function testGetFileSizeByFileIdInRecycle()
    {
        $this->assertFalse(false, $this->familyShareDB->getFileSizeByFileIdInRecycle(2, 1));
        $this->assertEquals(1000, $this->familyShareDB->getFileSizeByFileIdInRecycle(3, 1));
    }

    /**
     * @covers DB\CDBFamilyStorage::getSumSizeByDirIdInRecycle()
     */
    public function testGetSumSizeByDirIdInRecycle()
    {
        $this->assertFalse($this->familyShareDB->getSumSizeByDirIdInRecycle(1, 1));
        $this->assertEquals(1000,$this->familyShareDB->getSumSizeByDirIdInRecycle(4, 1));
    }

    /**
     * @covers DB\CDBFamilyStorage::cleanRecycleSizeByFamilyId()
     */
    public function testCleanRecycleSizeByFamilyId()
    {
        $this->assertEquals(1, $this->familyShareDB->cleanRecycleSizeByFamilyId(1));
    }

    /**
     * @covers DB\CDBFamilyStorage::checkFileUrlExists()
     */
    public function testCheckFileUrlExists()
    {
        $this->assertEquals(2, $this->familyShareDB->checkFileUrlExists('file_url', 1));
    }

}