<?php

use DB\CDBCollection;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-11
 * Time: 上午11:15
 */
class DBCollectionTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $collectionDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }


    public function getDataSet()
    {
        $this->collectionDB = new CDBCollection();
        return $this->createMySQLXMLDataSet('db_test/xml/collection_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBCollection::addCollection()
     */
    public function testAddCollection()
    {
        $moduleId = 1;
        $recordId = 2;
        $recordTitle = 'title';
        $recordContent = 'content';
        $extraInfo1 = 'info1';
        $extraInfo2 = 'info2';
        $userId = 1;
        $this->assertEquals(2, $this->collectionDB->addCollection($moduleId, $recordId, $recordTitle, $recordContent, $extraInfo1, $extraInfo2, $userId));
    }

    /**
     * @covers DB\CDBCollection::checkCollectionExists()
     */
    public function testCheckCollectionExists()
    {
        $this->assertTrue(true, $this->collectionDB->checkCollectionExists(1, 1, 1));
    }

    /**
     * @covers DB\CDBCollection::getCollectionsTotal()
     */
    public function testGetCollectionsTotal()
    {
        $this->assertEquals(1, $this->collectionDB->getCollectionsTotal(1));
    }

    /**
     * @covers DB\CDBCollection::getCollectionsPaging()
     */
    public function testGetCollectionsPaging()
    {
        $array = [[
            'id' => "1",
            'userId' => "1",
            'moduleId' => "1",
            'recordId' => "1",
            'recordTitle' => "title",
            'recordContent' => "content",
            'extraInfo1' => "info1",
            'extraInfo2' => "info2",
            'createTime' => "2018-06-09 11:26:00",
            'createBy' => "1",
            'updateTime' => "2018-06-09 11:26:00",
            'updateBy' => "1"

        ]];

        $this->assertEquals($array,$this->collectionDB->getCollectionsPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBCollection::deleteCollection()
     */
    public function testDeleteCollection(){
        $this->assertEquals(1,$this->collectionDB->deleteCollection(1));
    }

}