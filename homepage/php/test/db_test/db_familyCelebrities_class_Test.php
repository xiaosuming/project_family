<?php

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use DB\CDBFamilyCelebrities;
use Model\Celebrity;
use Util\Util;

class DBFamilyCelebritiesTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $familyCelebritiesDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    public function getDataSet()
    {
        $this->familyCelebritiesDB = new CDBFamilyCelebrities();
        return $this->createMySQLXMLDataSet('db_test/xml/celebrities_dataset.xml');     //默认的数据集
    }

    /**
     * 测试 添加家族传说人物
     *
     * @covers DB\CDBFamilyCelebrities::addCelebrity
     */
    public function testAddCelebrity()
    {
        $originalRows = $this->getConnection()->getRowCount("gener_family_celebrities");
        $this->familyCelebritiesDB->addCelebrity(2, 1, "asd", "asd", "sdf", 1991, 2991, "hahaha", "hahaha", 3);
        $this->assertEquals($originalRows + 1, $this->getConnection()->getRowCount("gener_family_celebrities"));
    }

    /**
     * 测试 获取家族传说人物
     *
     * @covers DB\CDBFamilyCelebrities::getCelebrities
     */
    public function testGetCelebrities()
    {
        //家族不存在
        $this->assertEquals(array(), $this->familyCelebritiesDB->getCelebrities(-1));
        //家族存在
        $res = array(
            array(
                "id" => "3",
                "familyId" => "2",
                "personId" => "2",
                "personName" => "xiajiao",
                "styleName" => "z",
                "pseudonym" => "g",
                "yearOfBirth" => "1012",
                "yearOfPass" => "2101",
                "profile" => "IamGod",
                "photo" => "asdfasdf",
                "createTime" => "2011-11-11 11:11:11",
                "createBy" => "2",
                "updateTime" => "2011-11-11 11:11:11",
                "updateBy" => "2"
            )
        );
        $this->assertEquals($res, $this->familyCelebritiesDB->getCelebrities(2));
    }

    /**
     * 根据ID获取家族传说人物
     *
     * @covers DB\CDBFamilyCelebrities::getCelebrityById
     */
    public function testGetCelebrityById()
    {
        //传说人物不存在
        $this->assertNull($this->familyCelebritiesDB->getCelebrityById(-1));
        //传说人物存在
        $celebrity = new Celebrity();
        $celebrity->id = "2";
        $celebrity->familyId = "1";
        $celebrity->personId = "2";
        $celebrity->personName = "asd";
        $celebrity->styleName = "d";
        $celebrity->pseudonym = "g";
        $celebrity->yearOfBirth = "789";
        $celebrity->yearOfPass = "856";
        $celebrity->profile = "rthsfdhgs";
        $celebrity->photo = "sdfsdfg";
        $celebrity->createTime = "2011-11-11 11:11:11";
        $celebrity->createBy = "2";
        $celebrity->updateTime = "2011-11-11 11:11:11";
        $celebrity->updateBy = "2";
        $this->assertEquals($celebrity, $this->familyCelebritiesDB->getCelebrityById(2));
    }

    /**
     * 测试 检查用户是否记录的创建者
     *
     * @covers DB\CDBFamilyCelebrities::isCreaterOfCelebrity
     */
    public function testIsCreaterOfCelebrity()
    {
        //用户是创建者
        $this->assertTrue($this->familyCelebritiesDB->isCreaterOfCelebrity(4, 3));
        //用户不是创建者
        $this->assertFalse($this->familyCelebritiesDB->isCreaterOfCelebrity(4, 2));
    }

    /**
     * 测试 删除家族传说人物
     *
     * @covers DB\CDBFamilyCelebrities::deleteCelebrity
     */
    public function testDeleteCelebrity()
    {
        $originalRows = $this->getConnection()->getRowCount("gener_family_celebrities");
        //删除失败
        $this->assertEquals(0, $this->familyCelebritiesDB->deleteCelebrity(0));
        $this->assertEquals($originalRows, $this->getConnection()->getRowCount("gener_family_celebrities"));

        //删除成功
        $this->assertGreaterThan(0, $this->familyCelebritiesDB->deleteCelebrity(2));
        $this->assertEquals($originalRows, $this->getConnection()->getRowCount("gener_family_celebrities"));

    }

    /**
     * 测试 修改家族传说人物资料
     *
     * @covers DB\CDBFamilyCelebrities::setCelebrity
     */
    public function testSetCelebrity()
    {
        //修改失败
        $this->assertEquals(0, $this->familyCelebritiesDB->setCelebrity(0, "zi", "hao", "9999", "jiangjie", "9"));

        $celebrity = $this->familyCelebritiesDB->getCelebrityById(1);
        $celebrity->styleName = "zi";
        $celebrity->pseudonym = "hao";
        $celebrity->yearOfPass = "9999";
        $celebrity->profile = "jianjie";
        $celebrity->updateBy = "9";
        $celebrity->updateTime = "2011-11-11 11:11:11";

        //修改成功
        $this->assertEquals(1, $this->familyCelebritiesDB->setCelebrity($celebrity->id, "zi", "hao", "9999", "jianjie", "9"));
        $neoCelebrity = $this->familyCelebritiesDB->getCelebrityById(1);
        $neoCelebrity->updateTime = "2011-11-11 11:11:11";
        $this->assertEquals($celebrity, $neoCelebrity);
    }

    /**
     * 测试 更新家族传说人物资料
     *
     * @covers DB\CDBFamilyCelebrities::updateCelebrity
     */
    public function testUpdateCelebrity()
    {
        //更新失败
        $this->assertEquals(0, $this->familyCelebritiesDB->updateCelebrity(0, "hahaha", "1000", "qwdvbgrthn", "9"));

        $celebrity = $this->familyCelebritiesDB->getCelebrityById(1);
        $celebrity->personName = "hahaha";
        $celebrity->yearOfBirth = "1000";
        $celebrity->photo = "qwdvbgrthn";
        $celebrity->updateBy = "9";
        $celebrity->updateTime = "2011-11-11 11:11:11";

        //更新成功
        $this->assertEquals(1, $this->familyCelebritiesDB->updateCelebrity($celebrity->id, "hahaha", "1000", "qwdvbgrthn", "9"));
        $neoCelebrity = $this->familyCelebritiesDB->getCelebrityById(1);
        $neoCelebrity->updateTime = "2011-11-11 11:11:11";
        $this->assertEquals($celebrity, $neoCelebrity);
    }

    /**
     * 测试 检查某个人物是否已经被标记为名人
     *
     * @covers DB\CDBFamilyCelebrities::checkCelebrityExist
     */
    public function testCheckCelebrityExist()
    {
        //已经被标记
        $this->assertTrue($this->familyCelebritiesDB->checkCelebrityExist(2));
        //未被标记
        $this->assertFalse($this->familyCelebritiesDB->checkCelebrityExist(0));
    }

    /**
     * @covers DB\CDBFamilyCelebrities::getCelebrityIdsByCreator
     */
    public function testGetCelebrityIdsByCreator()
    {
        $array = [
            ['id'=>1],
            ['id'=>2],
            ['id'=>3]
        ];
        $this->assertEquals($array,$this->familyCelebritiesDB->getCelebrityIdsByCreator(2));
    }
}
