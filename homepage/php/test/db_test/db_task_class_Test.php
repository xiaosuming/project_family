<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/13 0013
 * Time: 15:34
 */

use DB\CDBTask;
use Model\Task;
use Model\TaskTender;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;


class DBTaskTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $taskDB = null;


    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->taskDB = new CDBTask();
        return $this->createMySQLXMLDataSet(__DIR__ . '/xml/task_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBTask::addTask
     */
    public function testAddTask()
    {
        $this->assertGreaterThan(0, $this->taskDB->addTask(1, 'this is title', 'this is content', 'photo,photo1,photo2', 1));
    }

    /**
     * @covers DB\CDBTask::getUsers
     */
    public function testGetUsers()
    {
        $array = [
            ['userId' => 2],
            ['userId' => 3]
        ];
        $this->assertEquals($array, $this->taskDB->getUsers(1));
    }

    /**
     * @covers \DB\CDBTask::getSameFamilyUsers
     */
    public function testGetSameFamilyUsers()
    {
        $array = [
            ['userId' => 2],
            ['userId' => 3]
        ];
        $this->assertEquals($array, $this->taskDB->getSameFamilyUsers(1, 1));
    }

    /**
     * @covers DB\CDBTask::verifyTaskIdAndUserId()
     */
    public function testVerifyTaskIdAndUserId()
    {
        $this->assertTrue(true, $this->taskDB->verifyTaskIdAndUserId(1, 1));
        $this->assertFalse($this->taskDB->verifyTaskIdAndUserId(11, 11));
    }

    /**
     * @covers DB\CDBTask::getTaskPoint()
     */
    public function testGetTaskPoint()
    {
        $this->assertEquals(1, $this->taskDB->getTaskPoint(1));
    }

    /**
     * @covers DB\CDBTask::updateTask()
     */
    public function testUpdateTask()
    {
        $this->assertGreaterThan(0, $this->taskDB->updateTask(1, 'update title', 'update content', 'photo', 2, '2017-05-07 10:00:01', 1));
    }

    /**
     * @covers DB\CDBTask::deleteTask()
     */
    public function testDeleteTask()
    {
        $this->assertEquals(1, $this->taskDB->deleteTask(1, 1));
        $this->assertFalse($this->taskDB->deleteTask(111, 1));
    }

    /**
     * @covers DB\CDBTask::addUserTaskTender()
     */
    public function testAddUserTaskTender()
    {
        $this->assertGreaterThan(0, $this->taskDB->addUserTaskTender(1, 1, 'this is tender content'));

    }

    /**
     * @covers DB\CDBTask::existsUserIdANDTaskIdInTender()
     */
    public function testExistsUserIdANDTaskIdInTender()
    {
        $this->assertTrue(true, $this->taskDB->existsUserIdANDTaskIdInTender(1, 1));
    }

    /**
     * @covers DB\CDBTask::updateTenderAccept()
     */
    public function testUpdateTenderAccept()
    {
        $this->assertEquals(1, $this->taskDB->updateTenderAccept(1, 1, 1));
    }

    /**
     * @covers  DB\CDBTask::getTaskDetail()
     */
    public function testGetTaskDetail()
    {
        $taskTender = [
            'userId' => 1,
            'title' => 'this is title',
            'content' => 'this is tender content',
            'photo' => 'photo,photo1,photo2',
            'point' => '1',
            'create_time' => '2017-05-06 10:00:01',
            'create_by' => 1,
            'update_time' => '2017-05-06 10:00:01',
            'update_by' => 1
        ];

        $this->assertGreaterThan($taskTender, $this->taskDB->getTaskDetail(1));
        $this->assertEquals(array('nickname'=>null),$this->taskDB->getTaskDetail(111));
    }

    /**
     * @covers DB\CDBTask::getUserTenderPaging()
     */
    public function testGetUserTenderPaging()
    {
        $taskTender = [
            0 => [
                'id' => '1',
                'userId' => 1,
                'taskId' => 1,
                'content' => 'this is tender content',
                'is_accept' => 0,
                'createTime' => '2017-05-06 10:00:01',
                'createBy' => 1,
                'updateTime' => '2017-05-06 10:00:01',
                'updateBy' => 1,
                'photo' => 'http://test.photo',
                'nickname' => 'jiang111'
            ]
        ];

        $this->assertEquals($taskTender, $this->taskDB->getUserTenderPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBTask::getUserTenderTotal()
     */
    public function testGetUserTenderTotal()
    {
        $this->assertEquals(1, $this->taskDB->getUserTenderTotal(1));
    }

    /**
     * @covers DB\CDBTask::existsTaskId()
     */
    public function testExistsTaskId()
    {
        $this->assertTrue(true, $this->taskDB->existsTaskId(1));
    }

    /**
     * @covers \DB\CDBTask::getTasksDetail
     */
    public function testGetTasksDetail()
    {
        $array = [[
            'id' => "1",
            'userId' => "1",
            'title' => "this is title ",
            'content' => "this is content",
            'photo' => "photo,photo1,photo2",
            'point' => "1",
            'state' => "1",
            'tenderCount' => "1",
            'createTime' => "2017-05-06 10:00:01",
            'createBy' => "1",
            'updateTime' => "2017-05-06 10:00:01",
            'updateBy' => "1"
        ]];
        $this->assertNull($this->taskDB->getTasksDetail((1)));
        $this->assertEquals($array, $this->taskDB->getTasksDetail("(1)"));
    }

}

