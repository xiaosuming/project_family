<?php

use DB\CDBFamilyNews;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-20
 * Time: 上午10:50
 */
class DBFamilyNewsTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;
    private $conn = null;
    private $familyNewsDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    protected function getDataSet()
    {
        $this->familyNewsDB = new CDBFamilyNews();
        return $this->createMySQLXMLDataSet('db_test/xml/family_news_dataset.xml');     //默认的数据集
    }


    /**
     * @covers DB\CDBFamilyNews::verifyUserIdAndFamilyNewsIdManagePermission()
     */
    public function testVerifyUserIdAndFamilyNewsIdManagePermission()
    {
        $this->assertTrue(true, $this->familyNewsDB->verifyUserIdAndFamilyNewsIdManagePermission(1, 1));
    }

    /**
     * @covers DB\CDBFamilyNews::verifyUserIdAndFamilyNewsIdReadPermission()
     */
    public function testVerifyUserIdAndFamilyNewsIdReadPermission()
    {
        $this->assertTrue(true, $this->familyNewsDB->verifyUserIdAndFamilyNewsIdReadPermission(1, 1));
    }

    /**
     * @covers  DB\CDBFamilyNews::addFamilyNews()
     */
    public function testAddFamilyNews()
    {
        $familyId = 1;
        $module = 1;
        $action = 1;
        $operatorId = 2;
        $thingId = 2;
        $thingName = 'name111';
        $userId = 1;
        $extraInfo1 = "ddd";
        $extraInfo2 = "zzzzz";
        $this->assertEquals(2, $this->familyNewsDB->addFamilyNews($familyId, $module, $action, $operatorId, $thingId, $thingName, $userId, $extraInfo1, $extraInfo2));
    }

    /**
     * @covers DB\CDBFamilyNews::addMultiFamilyNews()
     */
    public function testAddMultiFamilyNews()
    {
        $insertValue = "(1,1,1,3,3,'name22','aa','aaa',1,'2018-06-20 12:00:00',1,'2018-06-20 12:00:00'),
        (1,1,1,3,3,'name33','bb','bbb',1,'2018-06-20 12:00:00',1,'2018-06-20 12:00:00'),
        (1,1,1,3,3,'name44','cc','ccc',1,'2018-06-20 12:00:00',1,'2018-06-20 12:00:00')";
        $this->assertEquals(2, $this->familyNewsDB->addMultiFamilyNews($insertValue));
    }

    /**
     * @covers DB\CDBFamilyNews::deleteFamilyNews()
     */
    public function testDeleteFamilyNews()
    {
        $this->assertEquals(1, $this->familyNewsDB->deleteFamilyNews(1));
    }

    /**
     * @covers DB\CDBFamilyNews::queryFamilyNewsCount()
     */
    public function testQueryFamilyNewsCount()
    {
        $this->assertEquals(1, $this->familyNewsDB->queryFamilyNewsCount(1));
    }

    /**
     * @covers DB\CDBFamilyNews::queryFamilyNewsPaging()
     */
    public function testQueryFamilyNewsPaging()
    {
        $array = [[
            'id' => "1",
            'familyId' => "1",
            'familyName' => "haha",
            'username' => "jiang",
            'nickname' => "jiang111",
            'photo' => "http://test.photo",
            'module' => "1",
            'action' => "1",
            'operatorId' => "1",
            'name' => "1",
            'thingId' => "1",
            'thingName' => "thingName",
            'extraInfo1' => "extra_info1",
            'extraInfo2' => "extra_info2",
            'createBy' => "1",
            'createTime' => "2018-06-20 12:00:00",
            'updateBy' => "1",
            'updateTime' => "2018-06-20 12:00:00"

        ]];
        $this->assertEquals($array, $this->familyNewsDB->queryFamilyNewsPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBFamilyNews::getFamilyNews()
     */
    public function testGetFamilyNews()
    {
        $familyNews = new \Model\FamilyNews(array());
        $familyNews->id = 1;
        $familyNews->familyId = 1;
        $familyNews->module = 1;
        $familyNews->action = 1;
        $familyNews->operatorId = 1;
        $familyNews->thingId = 1;
        $familyNews->thingName = 'thingName';
        $familyNews->extraInfo1 = 'extra_info1';
        $familyNews->extraInfo2 = 'extra_info2';
        $familyNews->createBy = 1;
        $familyNews->createTime = '2018-06-20 12:00:00';
        $familyNews->updateBy = 1;
        $familyNews->updateTime = '2018-06-20 12:00:00';

        $this->assertEquals($familyNews, $this->familyNewsDB->getFamilyNews(1));
    }

    /**
     * @covers DB\CDBFamilyNews::getMyFamilyNewsCount()
     */
    public function testGetMyFamilyNewsCount()
    {
        $this->assertEquals(1, $this->familyNewsDB->getMyFamilyNewsCount(1));
    }

    /**
     * @covers DB\CDBFamilyNews::getMyFamilyNewsPaging()
     */
    public function testGetMyFamilyNewsPaging()
    {
        $array = [[
            'id' => "1",
            'familyId' => "1",
            'familyName' => "haha",
            'module' => "1",
            'action' => "1",
            'operatorId' => "1",
            'username' => "jiang",
            'nickname' => "jiang111",
            'photo' => "http://test.photo",
            'thingId' => "1",
            'thingName' => "thingName",
            'extraInfo1' => "extra_info1",
            'extraInfo2' => "extra_info2",
            'createBy' => "1",
            'createTime' => "2018-06-20 12:00:00",
            'updateBy' => "1",
            'updateTime' => "2018-06-20 12:00:00"

        ]];
        $this->assertEquals($array, $this->familyNewsDB->getMyFamilyNewsPaging(1, 1, 1));
    }
}