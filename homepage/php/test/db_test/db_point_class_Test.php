<?php

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use DB\CDBPoint;
use Model\Point;
use Util\Util;
use Model\PointRecord;

class DBPointTest extends TestCase {
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $pointDB = null;

    final public function getConnection(){
        if($this->conn === null){
            if(self::$pdo === null){
                self::$pdo = new PDO( $GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD'] );
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo,$GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet(){
        $this->pointDB =  new CDBPoint();
        return $this->createMySQLXMLDataSet('db_test/xml/point_dataset.xml');     //默认的数据集
    }

    /**
     * 测试初始化用户积分
     * @covers DB\CDBPoint::initUserPoint
     */
    public function testInitUserPoint(){
        $this->assertGreaterThan(0,$this->pointDB->initUserPoint(1));
    }

    /**
     * 测试获取用户积分信息
     * @covers DB\CDBPoint::getUserPointInfo
     */
    public function testGetUserPointInfo(){
        $point = new Point();
        $point->id = 1;
        $point->userId = '1';
        $point->totalPoint = '0';
        $point->usedPoint = '0';
        $point->remainPoint = '0';
        $point->createBy = '1';
        $point->createTime = "2017-09-09 09:09:09";
        $point->updateBy = '1';
        $point->updateTime = "2017-09-09 09:09:09";

        $this->assertEquals($point,$this->pointDB->getUserPointInfo(1));
        $this->assertNull($this->pointDB->getUserPointInfo(111));
    }

    /**
     * 测试为用户更新积分
     * @covers DB\CDBPoint::updatePointForUser
     */
    public function testUpdatePointForUser(){
        $pointRecord = new PointRecord();
        $pointRecord->userId = 1; // 用户id
        $pointRecord->amount = 3; // 积分数量
        $pointRecord->type = 1;   // 类型，1是增加，2是减少
        $pointRecord->module = 3; // 模块id
        $pointRecord->action = 1; // 动作id
        $pointRecord->createBy = 1;
        $pointRecord->updateBy = 1;

        $this->assertGreaterThan(0,$this->pointDB->updatePointForUser($pointRecord, true));    //为用户1增加3个积分
    
        $point = $this->pointDB->getUserPointInfo(1);

        $this->assertEquals(3,$point->totalPoint);
        $this->assertEquals(0,$point->usedPoint);
        $this->assertEquals(3,$point->remainPoint);
    
        $pointRecord = new PointRecord();
        $pointRecord->userId = 1; // 用户id
        $pointRecord->amount = 3; // 积分数量
        $pointRecord->type = 2;   // 类型，1是增加，2是减少
        $pointRecord->module = 3; // 模块id
        $pointRecord->action = 1; // 动作id
        $pointRecord->createBy = 1;
        $pointRecord->updateBy = 1;
        $this->assertGreaterThan(0,$this->pointDB->updatePointForUser($pointRecord, true));    //为用户1减少3个积分

        $point = $this->pointDB->getUserPointInfo(1);

        $this->assertEquals(3,$point->totalPoint);
        $this->assertEquals(3,$point->usedPoint);
        $this->assertEquals(0,$point->remainPoint);

        //测试事件记录数是否正常
        $this->assertEquals(2,$this->getConnection()->getRowCount('gener_point_record'));
    }

    /**
     * 测试分页获取用户积分更新事件
     * @covers DB\CDBPoint::getUserPointUpdateEventsPaging
     */
    public function testGetUserPointUpdateEventsPaging(){

        $pointRecord = new PointRecord();
        $pointRecord->userId = 1; // 用户id
        $pointRecord->amount = 3; // 积分数量
        $pointRecord->type = 1;   // 类型，1是增加，2是减少
        $pointRecord->module = 3; // 模块id
        $pointRecord->action = 1; // 动作id
        $pointRecord->createBy = 1;
        $pointRecord->updateBy = 1;
        $this->pointDB->updatePointForUser($pointRecord);

        $events = $this->pointDB->getUserPointUpdateEventsPaging(1,10,1);
        $this->assertEquals(1,count($events));

        $events = $this->pointDB->getUserPointUpdateEventsPaging(2,10,1);
        $this->assertEquals(0,count($events));
   
        $events = $this->pointDB->getUserPointUpdateEventsPaging(1,10,2);
        $this->assertEquals(0,count($events));
    }

    /**
     * 测试分页获取用户积分更新事件
     * @covers DB\CDBPoint::getUserPointUpdateEventsCount
     */
    public function testGetUserPointUpdateEventsCount(){
        $pointRecord = new PointRecord();
        $pointRecord->userId = 1; // 用户id
        $pointRecord->amount = 3; // 积分数量
        $pointRecord->type = 1;   // 类型，1是增加，2是减少
        $pointRecord->module = 3; // 模块id
        $pointRecord->action = 1; // 动作id
        $pointRecord->createBy = 1;
        $pointRecord->updateBy = 1;
        $this->pointDB->updatePointForUser($pointRecord);
        
        $this->assertEquals(1,$this->pointDB->getUserPointUpdateEventsCount(1));

        $this->assertEquals(0,$this->pointDB->getUserPointUpdateEventsCount(2));
    }


}