<?php
/**
 * 大事件测试
 *
 */


use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use DB\CDBEvent;
use Util\Util;
use Model\Event;
use Model\EventHistory;

class DBEventTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $eventDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    public function getDataSet()
    {
        $this->eventDB = new CDBEvent();
        return $this->createMySQLXMLDataSet('db_test/xml/event_dataset.xml');     //默认的数据集
    }

    /**
     * 测试 检查用户id和事件id的权限是否匹配,这里检查的是查看的权限
     * @covers DB\CDBEvent::verifyUserIdAndEventId
     */
    public function testVerifyUserIdAndEventId()
    {
        /* 用户是大事件绑定的 */
        $this->assertTrue($this->eventDB->verifyUserIdAndEventId(1, 1));//验证成功

        /* 用户不是大事件绑定的，并且不在同一个家族 */
        $this->assertFalse($this->eventDB->verifyUserIdAndEventId(1, 2));//验证失败

        /* 用户不是大事件绑定的，但是在同一个家族 */
        $this->assertTrue($this->eventDB->verifyUserIdAndEventId(3, 2));
    }

    /**
     * 测试 添加大事件
     * @covers DB\CDBEvent::addEvent
     */
    public function testAddEvent()
    {
        $personId = 6;
        $userId = 2;
        $familyId = 33;
        $iconograph = "asd";
        $type = 1;
        $openTime = "2017-11-03";
        $expTime = "2017-11-03";
        $expName = "添加测试";
        $expContent = "asdgfasf";
        $writeMode = 0;
        $writer = 2;

        $event = new Event();
        $event->personId = $personId;
        $event->userId = $userId;
        $event->familyId = $familyId;
        $event->iconograph = $iconograph;
        $event->type = $type;
        $event->openTime = $openTime;
        $event->expTime = $expTime;
        $event->expName = $expName;
        $event->expContent = $expContent;
        $event->writeMode = $writeMode;
        $event->createBy = $writer;
        $event->updateBy = $writer;

        $this->assertGreaterThan(0, $this->eventDB->addEvent($event));
    }

    /**
     * 测试 添加编辑历史
     * @covers DB\CDBEvent::addEventEditHistory
     */
    public function testAddEventEditHistory()
    {
        $eventId = 15;
        $expTime = "2017-11-03";
        $expName = "添加测试 改";
        $expContent = "safasdf asd";
        $eventEditType = 1;
        $writer = 1;
        $this->assertGreaterThan(0, $this->eventDB->addEventEditHistory($eventId, $expTime, $expName, $expContent, $eventEditType, $writer));
    }

    /**
     * 测试 查看编辑大事件权限
     * @covers DB\CDBEvent::checkUpdateEventPermission
     */
    public function testCheckUpdateEventPermission()
    {
        // result == null
        $this->assertFalse($this->eventDB->checkUpdateEventPermission(1, 111));
        /* 是大事件的作者 */
        $this->assertTrue($this->eventDB->checkUpdateEventPermission(1, 1));//有权限

        /* 不是大事件的作者，大事件公开编辑，但是不在一个家族 */
        $this->assertFalse($this->eventDB->checkUpdateEventPermission(1, 2));//无权限

        /* 是大事件的作者，大事件公开编辑，且在一个家族 */
        $this->assertTrue($this->eventDB->checkUpdateEventPermission(3, 2));//有权限

        /* 是大事件的作者，大事件不公开编辑，且在一个家族 */
        $this->assertFalse($this->eventDB->checkUpdateEventPermission(3, 4));//有权限
    }

    /**
     * 测试 编辑事件
     * @covers DB\CDBEvent::updateEvent
     */
    public function testUpdateEvent()
    {
        //$this->assertEquals(-1, $this->eventDB->updateEvent($id,$expTime,$expName,$expContent,$write_mode,$updateTime,$updateBy));        
        $id = 15;
        $expTime = "2017-11-11";
        $expName = "kaikaikai";
        $expContent = "aslkdfklsf";
        $write_mode = 0;
        $updateTime = "2017-11-03";
        $updateBy = 2;
        $this->assertEquals(1, $this->eventDB->updateEvent($id, $expTime, $expName, $expContent, $write_mode, $updateTime, $updateBy));
    }

    /**
     * 测试 删除大事件
     * @covers DB\CDBEvent::deleteEvent
     */
    public function testDeleteEvent()
    {
        $this->assertEquals(0, $this->eventDB->deleteEvent(0));//删除0行

        $this->assertEquals(1, $this->eventDB->deleteEvent(15));//删除1行
    }

    /**
     * 测试 查询某一大事件
     * @covers DB\CDBEvent::getEvent
     */
    public function testGetEvent()
    {
        $this->assertNull($this->eventDB->getEvent(0));//事件不存在

        $event = new Event(array());
        $event->id = '5';
        $event->sourceId = "0";
        $event->personId = '4';
        $event->personName = 'jiangpengfei spouse';
        $event->userId = '3';
        $event->familyId = '44';
        $event->expTime = "2017-09-12";
        $event->type = '2';
        $event->iconograph = "123456";
        $event->expName = "test_event_5";
        $event->abstract = "test_event_5";
        $event->expContent = "test_event_5";
        $event->writeMode = '0';
        $event->participateNum = '1';
        $event->resources = 'resources';
        $event->version = '1';
        $event->openTime = "2017-09-27";
        $event->createTime = "2017-09-11 00:00:00";
        $event->createBy = '3';
        $event->updateTime = "2017-09-11 00:00:00";
        $event->updateBy = '3';

        $this->assertEquals($event, $this->eventDB->getEvent(5));//事件存在
    }

    /**
     * 测试 获取一个大事件所有的参与撰写人
     * @covers DB\CDBEvent::getEventWriters
     */
    public function testGetEventWriters()
    {
        $this->assertEquals(0, count($this->eventDB->getEventWriters(0)));//事件不存在

        $writers = array(
            0 => array(
                "writer" => "1",
                "nickname" => "jiang111"
            ),
            1 => array(
                "writer" => "2",
                "nickname" => "jiang222"
            ),
            2 => array(
                "writer" => "3",
                "nickname" => "jiang333"
            )
        );
        $this->assertEquals($writers, $this->eventDB->getEventWriters(9));
    }

    /**
     * 测试 根据指定条件查询所有的秘密事件的总数
     * @covers DB\CDBEvent::getSecretEventsTotal
     */
    public function testGetSecretEventsTotal()
    {
        //查询秘密事件总数
        $personId = -1;
        $userId = -1;
        $familyId = -1;
        $startTime = -1;
        $endTime = -1;
        $currentTime = -1;
        $this->assertEquals(7, $this->eventDB->getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime));

        //查询某个人物的秘密事件总数
        $personId = 1;
        $this->assertEquals(1, $this->eventDB->getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime));

        //人物不存在
        $personId = -2;
        $this->assertEquals(0, $this->eventDB->getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime));

        //查询某个用户的秘密事件总数
        $personId = -1;
        $userId = 1;
        $this->assertEquals(1, $this->eventDB->getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime));

        //用户不存在
        $userId = -2;
        $this->assertEquals(0, $this->eventDB->getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime));

        //查询某个家族的秘密事件总数
        $userId = -1;
        $familyId = 33;
        $this->assertEquals(2, $this->eventDB->getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime));

        //家族不存在
        $familyId = -2;
        $this->assertEquals(0, $this->eventDB->getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime));

        //查询秘密事件总数
        $personId = 4;
        $userId = 3;
        $familyId = 33;
        $startTime = '2017-10-11';
        $endTime = '2017-10-11';
        $currentTime = '2017-12-11';
        $this->assertEquals(1, $this->eventDB->getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime));
        $this->assertEquals(0, $this->eventDB->getSecretEventsTotal(100, 100, 100, -1, -1, -1));
    }

    /**
     * 测试 根据指定条件查询所有的秘密事件
     * @covers DB\CDBEvent::getSecretEventsPaging
     */
    public function testGetSecretEventsPaging()
    {
        //获取所有秘密事件
        $personId = -1;
        $userId = -1;
        $familyId = -1;
        $startTime = -1;
        $endTime = -1;
        $currentTime = -1;
        $pageIndex = 1;
        $pageSize = 10;

        $allSecretEvent = $this->eventDB->getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize);
        $this->assertGreaterThan(0, count($allSecretEvent));

        //查询某个人物的所有秘密事件
        $personId = 3;
        $allSecretEvent = $this->eventDB->getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize);
        $this->assertGreaterThan(0, count($allSecretEvent));

        //personId不存在
        $personId = -2;
        $allSecretEvent = $this->eventDB->getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize);
        $this->assertEquals(0, count($allSecretEvent));

        //查询某个用户的所有秘密事件
        $personId = -1;
        $userId = 2;
        $allSecretEvent = $this->eventDB->getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize);
        $this->assertGreaterThan(0, count($allSecretEvent));

        //用户不存在
        $userId = -2;
        $allSecretEvent = $this->eventDB->getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize);
        $this->assertEquals(0, count($allSecretEvent));

        //查询某个家族的所有秘密事件
        $userId = -1;
        $familyId = 55;
        $allSecretEvent = $this->eventDB->getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize);
        $this->assertGreaterThan(0, count($allSecretEvent));

        //家族不存在
        $familyId = -2;
        $allSecretEvent = $this->eventDB->getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize);
        $this->assertEquals(0, count($allSecretEvent));
        //获取所有秘密事件
        $pageIndex = 1;
        $pageSize = 10;
        $personId = 4;
        $userId = 3;
        $familyId = 33;
        $startTime = '2017-10-11';
        $endTime = '2017-10-11';
        $currentTime = '2017-12-11';
        $allSecretEvent = $this->eventDB->getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize);
        $this->assertEquals(2, $allSecretEvent[0]['id']);
    }

    /**
     * 测试 根据指定条件查询所有的大事件的总数
     * @covers DB\CDBEvent::getEventsTotal
     */
    public function testGetEventsTotal()
    {//数据待定
        //查询大事件总数
        $personId = -1;
        $userId = -1;
        $familyId = -1;
        $startTime = -1;
        $endTime = -1;
        $this->assertEquals(7, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));

        //查询某个人物的大事件总数
        $personId = 1;
        $this->assertEquals(4, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));

        //人物不存在
        $personId = -2;
        $this->assertEquals(0, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));

        //查询某个用户的大事件总数
        $personId = -1;
        $userId = 2;
        $this->assertEquals(1, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));

        //用户不存在
        $userId = -2;
        $this->assertEquals(0, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));

        //查询某个家族的大事件总数
        $userId = -1;
        $familyId = 55;
        $this->assertEquals(2, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));

        //家族不存在
        $familyId = -2;
        $this->assertEquals(0, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));

        $personId = 1;
        $userId = 1;
        $familyId = 1;
        $startTime = '2017-10-11';
        $endTime = '-1';
        $this->assertEquals(0, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));
        $startTime = '-1';
        $endTime = '2017-10-11';
        $this->assertEquals(0, $this->eventDB->getEventsTotal($personId, $userId, $familyId, $startTime, $endTime));
    }

    /**
     * 测试 根据指定条件查询所有的大事件
     * @covers DB\CDBEvent::getEventsPaging
     */
    public function testGetEventsPaging()
    {
        $personId = -1;
        $userId = -1;
        $familyId = -1;
        $startTime = -1;
        $endTime = -1;
        $pageIndex = 1;
        $pageSize = 10;
        $isUserSelf = true;

        //获取所有大事件
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertGreaterThan(0, count($allEvent));

        //查询某个人物的所有事件
        $personId = 8;
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertEquals(0, count($allEvent));

        //personId不存在
        $personId = -2;
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertEquals(0, count($allEvent));

        //查询某个用户的所有事件
        $personId = -1;
        $userId = 3;
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertGreaterThan(0, count($allEvent));

        //用户不存在
        $userId = -2;
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertEquals(0, count($allEvent));

        //查询某个家族的所有事件
        $userId = -1;
        $familyId = 22;
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertGreaterThan(0, count($allEvent));

        //家族不存在
        $familyId = -2;
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertEquals(0, count($allEvent));

        $personId = 1;
        $userId = 1;
        $familyId = 22;
        $startTime = '2017-10-11';
        $endTime = -1;
        $pageIndex = 1;
        $pageSize = 10;
        $isUserSelf = false;
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertEquals(1, $allEvent[0]['id']);
        $personId = 1;
        $userId = 1;
        $familyId = 22;
        $startTime = -1;
        $endTime = '2017-10-11';
        $pageIndex = 1;
        $pageSize = 10;
        $isUserSelf = false;
        $allEvent = $this->eventDB->getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf);
        $this->assertEquals(1, $allEvent[0]['id']);
    }

    /**
     * 测试 获取某一个事件的所有编辑历史
     * @covers DB\CDBEvent::getEventEditHistory
     */
    public function testGetEventEditHistory()
    {
        //事件不存在
        $this->assertEquals(array(), $this->eventDB->getEventEditHistory(-1));

        //事件存在
        $allEditHistory = $this->eventDB->getEventEditHistory(9);
        $this->assertGreaterThan(0, count($allEditHistory));
    }

    /**
     * 测试 获取事件的历史版本
     * @covers DB\CDBEvent::getEventHistory
     */
    public function testGetEventHistory()
    {
        $this->assertNull($this->eventDB->getEventHistory(-1));
        //数据待定
        $eventHistory = array(
            "id" => '1',
            "eventId" => '9',
            "expTime" => "2017-10-29",
            "expName" => "test_event_9",
            "expContent" => "test_event_9",
            "editType" => '1',
            "writer" => '1',
            "createBy" => '1',
            "createTime" => "2017-09-11 00:00:00",
            "updateBy" => '1',
            "updateTime" => "2017-09-11 00:00:00"
        );

        $this->assertEquals(new EventHistory($eventHistory), $this->eventDB->getEventHistory(1));
    }

    /**
     * 测试 获取用户编辑的事件总数
     * @covers DB\CDBEvent::getMyEditEventsCount
     */
    public function testGetMyEditEventsCount()
    {
        $personId = -1;
        $searchUserId = -1;
        $familyId = -1;
        $myUserId = -1;

        //用户不存在的情况
        $this->assertEquals(0, $this->eventDB->getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId));

        //用户存在的情况
        $myUserId = 2;
        $this->assertGreaterThan(0, $this->eventDB->getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId));

        //人物不存在
        $personId = -2;
        $this->assertEquals(0, $this->eventDB->getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId));

        //人物存在
        $personId = 7;
        $this->assertEquals(0, $this->eventDB->getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId));

        //搜索用户不存在
        $personId = -1;
        $searchUserId = -2;
        $this->assertEquals(0, $this->eventDB->getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId));

        //搜索用户存在
        $searchUserId = 1;
        $this->assertGreaterThan(0, $this->eventDB->getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId));

        //家族不存在
        $searchUserId = -1;
        $familyId = -2;
        $this->assertEquals(0, $this->eventDB->getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId));

        //家族存在
        $familyId = 44;
        $this->assertGreaterThan(0, $this->eventDB->getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId));
    }

    /**
     * 测试 获取用户编辑的事件分页
     * @covers DB\CDBEvent::getMyEditEventsPaging
     */
    public function testGetMyEditEventsPaging()
    {
        $pageIndex = 1;
        $pageSize = 10;
        $personId = -1;
        $searchUserId = -1;
        $familyId = -1;
        $myUserId = -1;

        //用户不存在的情况
        $allEditEvent = count($this->eventDB->getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId));
        $this->assertEquals(0, $allEditEvent);

        //用户存在的情况
        $myUserId = 2;
        $allEditEvent = count($this->eventDB->getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId));
        $this->assertGreaterThan(0, $allEditEvent);

        //人物不存在
        $personId = -2;
        $allEditEvent = count($this->eventDB->getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId));
        $this->assertEquals(0, $allEditEvent);

        //人物存在
        $personId = 7;
        $allEditEvent = count($this->eventDB->getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId));
        $this->assertEquals(0, $allEditEvent);

        //搜索用户不存在
        $personId = -1;
        $searchUserId = -2;
        $allEditEvent = count($this->eventDB->getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId));
        $this->assertEquals(0, $allEditEvent);

        //搜索用户存在
        $searchUserId = 1;
        $allEditEvent = count($this->eventDB->getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId));
        $this->assertGreaterThan(0, $allEditEvent);

        //家族不存在
        $searchUserId = -1;
        $familyId = -2;
        $allEditEvent = count($this->eventDB->getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId));
        $this->assertEquals(0, $allEditEvent);

        //家族存在
        $familyId = 44;
        $allEditEvent = count($this->eventDB->getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId));
        $this->assertGreaterThan(0, $allEditEvent);
    }

    /**
     * 测试 删除人物相关的大事件
     * @covers DB\CDBEvent::deleteEventByPersonId
     */
    public function testDeleteEventByPersonId()
    {
        $this->assertEquals(0, $this->eventDB->deleteEventByPersonId(-1));//人物不存在
        $this->assertGreaterThan(0, $this->eventDB->deleteEventByPersonId(3));//人物存在
    }

    /**
     * 测试 添加评论
     * @covers DB\CDBEvent::addEventComment
     */
    public function testAddEventComment()
    {
        $eventId = 5;
        $userId = 3;
        $comment = "asdfsa";
        $replyTo = -1;
        $replyToUserId = 3;
        $this->assertGreaterThan(0, $this->eventDB->addEventComment($eventId, $userId, $comment, $replyTo, $replyToUserId));
    }

    /**
     * 测试获取特定大事件的所有评论
     * @covers DB\CDBEvent::getEventCommentsTotal
     */
    public function testGetEventCommentsTotal()
    {
        $this->assertEquals(array(), $this->eventDB->getEventCommentsTotal(-1));//大事件无评论

        $eventCommentData = array(//数据待定

            array(
                "id" => '2',
                "comment" => "testing2",
                "photo" => 'http://test.photo',
                "userId" => '1',
                "replyto" => '1',
                "replytoUserId" => '2',
                "replytoUserName" => "jiang222",
                "createBy" => '1',
                "createByName" => "jiang111",
                "createTime" => "2017-09-25 00:00:00"
            ),
            array(
                "id" => '1',
                "userId" => '2',
                "photo" => 'http://test.photo',
                "comment" => "testing",
                "replyto" => '-1',
                "replytoUserId" => '2',
                "replytoUserName" => "jiang222",
                "createBy" => '2',
                "createByName" => "jiang222",
                "createTime" => "2017-09-25 00:00:00"
            )
        );

        $this->assertEquals($eventCommentData, $this->eventDB->getEventCommentsTotal(11));//大事件存在
    }

    /**
     * 测试 验证用户是否对评论有操作权限
     * @covers DB\CDBEvent::verifyCommentIdAndUserId
     */
    public function testVerifyCommentIdAndUserId()
    {
        $this->assertTrue($this->eventDB->verifyCommentIdAndUserId(2, 1));//验证成功

        $this->assertFalse($this->eventDB->verifyCommentIdAndUserId(2, 2));//验证失败
    }

    /**
     * @covers DB\CDBEvent::checkEventCanLinkToUser
     */
    public function testCheckEventCanLinkToUser()
    {
        $this->assertFalse($this->eventDB->checkEventCanLinkToUser(1));
        $this->assertTrue($this->eventDB->checkEventCanLinkToUser(3));
    }

    /**
     * @covers DB\CDBEvent::checkEventCanLinkToPerson
     */
    public function testCheckEventCanLinkToPerson()
    {
        $this->assertFalse($this->eventDB->checkEventCanLinkToPerson(1));
        $this->assertTrue($this->eventDB->checkEventCanLinkToPerson(15));
    }

    /**
     * 测试 删除评论
     * @covers DB\CDBEvent::deleteComment
     */
    public function testDeleteComment()
    {
        $this->assertEquals(0, $this->eventDB->deleteComment(0));//删除0行

        $this->assertEquals(1, $this->eventDB->deleteComment(2));//删除1行
    }

    /**
     * 测试大事件能否复制给人物
     * @covers DB\CDBEvent::checkEventCanCopyToPerson
     */
    public function testCheckEventCanCopyToPerson()
    {
        $this->assertFalse($this->eventDB->checkEventCanCopyToPerson(4, 1));

        $this->assertFalse($this->eventDB->checkEventCanCopyToPerson(6, 4));

        $this->assertTrue($this->eventDB->checkEventCanCopyToPerson(6, 2));
    }

    /**
     * 测试关联用户大事件到人物
     * @covers DB\CDBEvent::linkUserEventToPerson
     */
    public function testLinkUserEventToPerson()
    {
        $this->assertEquals(1, $this->eventDB->linkUserEventToPerson(15, 2));

        //大事件的数量不会增长
        $this->assertEquals(15, $this->getConnection()->getRowCount("gener_significantEvent"));
    }

    /**
     * 测试关联所有的人物大事件到用户
     * @covers DB\CDBEvent::linkAllPersonEventsToUser
     */
    public function testLinkAllPersonEventsToUser()
    {
        $this->assertEquals(1, $this->eventDB->linkAllPersonEventsToUser(5, 2));

        //大事件的数量不会增长
        $this->assertEquals(15, $this->getConnection()->getRowCount("gener_significantEvent"));

        //关联秘密事件，用户的大事件不增加，仍然是1
        $this->assertEquals(1, $this->eventDB->getEventsTotal(-1, 2, -1, -1, -1));

        $this->assertEquals(6, $this->eventDB->linkAllPersonEventsToUser(4, 2));

        //关联公开事件，用户的大事件增加，现在是3
        $this->assertEquals(3, $this->eventDB->getEventsTotal(-1, 2, -1, -1, -1));
    }

    /**
     * 测试在家族之间复制大事件
     * @covers DB\CDBEvent::copyEventToAnotherPerson
     */
    public function testCopyEventToAnotherPerson()
    {
        $this->assertGreaterThan(1, $this->eventDB->copyEventToAnotherPerson(5, 2));

        //大事件的数量会增长1
        $this->assertEquals(16, $this->getConnection()->getRowCount("gener_significantEvent"));

        //拷贝秘密事件，用户的大事件不应该增加，现在是0
        $this->assertEquals(0, $this->eventDB->getEventsTotal(2, -1, -1, -1, -1));

        $this->assertGreaterThan(1, $this->eventDB->copyEventToAnotherPerson(4, 2));

        //拷贝普通事件，用户的大事件不应该增加，现在是1
        $this->assertEquals(0, $this->eventDB->getEventsTotal(2, -1, -1, -1, -1));
    }

    /**
     * @covers DB\CDBEvent::getEventAllCopy
     */
    public function testGetEventAllCopy()
    {
        $array = [
            ['id' => 7],
            ['id' => 1]
        ];
        $this->assertEquals($array, $this->eventDB->getEventAllCopy(1, 0));
        $this->assertEquals($array, $this->eventDB->getEventAllCopy(7, 1));
    }

    /**
     * @covers DB\CDBEvent::getPersonPagingWithEvent
     */
    public function testGetPersonPagingWithEvent()
    {
        $this->assertGreaterThan(0, count($this->eventDB->getPersonPagingWithEvent(44, 0, 0, 10)));
    }

    /**
     * @covers DB\CDBEvent::getPersonWithEventTotal
     */
    public function testGetPersonWithEventTotal() {
        $this->assertEquals(2, $this->eventDB->getPersonWithEventTotal(44));
    }
}