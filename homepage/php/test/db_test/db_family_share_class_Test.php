<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-12
 * Time: 下午6:01
 */

use Model\FamilyShare;
use Model\FamilyShareLog;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;
use DB\CDBFamilyShare;

class DBFamilyShareTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $familyShareDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    public function getDataSet()
    {
        $this->familyShareDB = new CDBFamilyShare();
        return $this->createMySQLXMLDataSet('db_test/xml/family_share_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBFamilyShare::checkShareIdAndIpExist()
     */
    public function testCheckShareIdAndIpExist()
    {
        $this->assertTrue(true, $this->familyShareDB->checkShareIdAndIpExist(1, 123));
        $this->assertTrue(true, $this->familyShareDB->checkShareIdAndIpExist(1, 1234));
    }

    /**
     * @covers DB\CDBFamilyShare::createFamilyShare()
     */
    public function testCreateFamilyShare()
    {
        $familyShare = new FamilyShare();
        $familyShare->familyId = 1;
        $familyShare->shareCode = '11';
        $familyShare->timeLimit = 1;
        $familyShare->levelLimit = 1;
        $familyShare->viewsLimit = 1;
        $familyShare->createBy = 1;
        $this->assertEquals(2, $this->familyShareDB->createFamilyShare($familyShare));
    }

    /**
     * @covers DB\CDBFamilyShare::deleteFamilyShare()
     */
    public function testDeleteFamilyShare()
    {
        $this->assertEquals(1, $this->familyShareDB->deleteFamilyShare(1));
    }

    /**
     * @covers DB\CDBFamilyShare::getFamilyShareModel()
     */
    public function testGetFamilyShareModel()
    {
        $familyShare = new FamilyShare();
        $familyShare->id = 1;
        $familyShare->familyId = 1;
        $familyShare->familyName = null;
        $familyShare->familyPhoto = null;
        $familyShare->shareCode = 11;
        $familyShare->timeLimit = 1;
        $familyShare->levelLimit = 1;
        $familyShare->viewsLimit = 1;
        $familyShare->createTime = "2018-06-14 12:00:00";
        $familyShare->createBy = 1;
        $familyShare->updateTime = "2018-06-14 12:00:00";
        $familyShare->updateBy = 1;
        $familyShare->samePersonId = 0;
        $this->assertEquals($familyShare, $this->familyShareDB->getFamilyShareModel(1));
        $this->assertNull($this->familyShareDB->getFamilyShareModel(111));
    }

    /**
     * @covers DB\CDBFamilyShare::getFamilyShareModelByShareCode()
     */
    public function testGetFamilyShareModelByShareCode()
    {
        $familyShare = new FamilyShare();
        $familyShare->id = 1;
        $familyShare->familyId = 1;
        $familyShare->familyName = null;
        $familyShare->familyPhoto = null;
        $familyShare->shareCode = 11;
        $familyShare->timeLimit = 1;
        $familyShare->levelLimit = 1;
        $familyShare->viewsLimit = 1;
        $familyShare->createTime = "2018-06-14 12:00:00";
        $familyShare->createBy = 1;
        $familyShare->updateTime = "2018-06-14 12:00:00";
        $familyShare->updateBy = 1;
        $familyShare->samePersonId = 0;
        $this->assertEquals($familyShare, $this->familyShareDB->getFamilyShareModelByShareCode(11));
        $this->assertNull($this->familyShareDB->getFamilyShareModelByShareCode(11111));
    }

    /**
     * @covers DB\CDBFamilyShare::getFamilyShareDetail()
     */
    public function testGetFamilyShareDetail()
    {
        $familyShare = new FamilyShare();
        $familyShare->id = 1;
        $familyShare->familyId = 1;
        $familyShare->familyName = null;
        $familyShare->familyPhoto = null;
        $familyShare->shareCode = 11;
        $familyShare->timeLimit = 1;
        $familyShare->levelLimit = 1;
        $familyShare->viewsLimit = 1;
        $familyShare->createTime = "2018-06-14 12:00:00";
        $familyShare->createBy = 1;
        $familyShare->updateTime = "2018-06-14 12:00:00";
        $familyShare->updateBy = 1;
        $familyShare->samePersonId = 0;
        $this->assertEquals($familyShare, $this->familyShareDB->getFamilyShareDetail(1));
        $this->assertNull($this->familyShareDB->getFamilyShareDetail(1111));
    }

    /**
     * @covers DB\CDBFamilyShare::addFamilyShareLog()
     */
    public function testAddFamilyShareLog()
    {
        $familyShareLog = new FamilyShareLog();
        $familyShareLog->shareId = 1;
        $familyShareLog->ip = 123;
        $this->assertEquals(2, $this->familyShareDB->addFamilyShareLog($familyShareLog));
    }

    /**
     * @covers DB\CDBFamilyShare::getUserFamilySharePaging()
     */
    public function testGetUserFamilySharePaging()
    {
        $array = [[
            'id' => "1",
            'familyId' => "1",
            'familyName' => NULL,
            'familyPhoto' => NULL,
            'shareCode' => "11",
            'timeLimit' => "1",
            'levelLimit' => "1",
            'viewsLimit' => "1",
            'createBy' => "1",
            'createTime' => "2018-06-14 12:00:00",
            'updateBy' => "1",
            'updateTime' => "2018-06-14 12:00:00"

        ]];
        $this->assertEquals($array, $this->familyShareDB->getUserFamilySharePaging(1, 1, 1, 1));
        $this->assertEquals($array, $this->familyShareDB->getUserFamilySharePaging(1, -1, 1, 1));
    }

    /**
     * @covers DB\CDBFamilyShare::getUserFamilyShareTotal()
     */
    public function testGetUserFamilyShareTotal()
    {
        $this->assertEquals(1, $this->familyShareDB->getUserFamilyShareTotal(1, 1));
        $this->assertEquals(1, $this->familyShareDB->getUserFamilyShareTotal(1, -1));
    }

    /**
     * @covers DB\CDBFamilyShare::getFamilyShareLogPaging()
     */
    public function testGetFamilyShareLogPaging()
    {
        $array = [[
            'id' => "1",
            'shareId' => "1",
            'ip' => "123",
            'times' => "1",
            'createTime' => "2018-06-14 12:00:00",
            'updateTime' => "2018-06-14 12:00:00"
        ]];
        $this->assertEquals($array,$this->familyShareDB->getFamilyShareLogPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBFamilyShare::getFamilyShareLogTotal()
     */
    public function testGetFamilyShareLogTotal(){
        $this->assertEquals(1,$this->familyShareDB->getFamilyShareLogTotal(1));
    }

    /**
     * @covers DB\CDBFamilyShare::updateFamilyShareLogViewTimes()
     */
    public function testUpdateFamilyShareLogViewTimes(){
        $this->assertEquals(1,$this->familyShareDB->updateFamilyShareLogViewTimes(1,123));
    }

}