<?php

use DB\CDBZone;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-21
 * Time: 下午5:05
 */
class DBZoneTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $zoneDB = null;


    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->zoneDB = new CDBZone();
        return $this->createMySQLXMLDataSet(__DIR__ . '/xml/zone_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBZone::verifyZoneIdAndUserId()
     */
    public function testVerifyZoneIdAndUserId()
    {
        $this->assertTrue($this->zoneDB->verifyZoneIdAndUserId(1, 1));
    }

    /**
     * @covers DB\CDBZone::verifyPostIdAndUserId()
     */
    public function testVerifyPostIdAndUserId()
    {
        $this->assertTrue($this->zoneDB->verifyPostIdAndUserId(1, 1));
    }

    /**
     * @covers DB\CDBZone::verifyCommentIdAndUserId()
     */
    public function testVerifyCommentIdAndUserId()
    {
        $this->assertTrue($this->zoneDB->verifyCommentIdAndUserId(1, 1));
        $this->assertFalse($this->zoneDB->verifyCommentIdAndUserId(111, 1));
        $this->assertFalse($this->zoneDB->verifyCommentIdAndUserId(1, 2));
    }

    /**
     * @covers DB\CDBZone::addZone()
     */
    public function testAddZone()
    {
        $this->assertGreaterThan(1, $this->zoneDB->addZone(1, 'this is content'));
        $this->assertEquals(3, $this->zoneDB->addZone(1, 'this is content'));
    }

    /**
     * @covers DB\CDBZone::updateZone()
     */
    public function testUpdateZone()
    {
        $id = 1;
        $content = 'edit content';
        $canShare = 1;
        $updateBy = 1;
        $this->assertEquals(1, $this->zoneDB->updateZone($id, $content, $canShare, $updateBy));
    }

    /**
     * @covers DB\CDBZone::deleteZone()
     */
    public function testDeleteZone()
    {
        $this->assertEquals(1, $this->zoneDB->deleteZone(1));
    }

    /**
     * @covers DB\CDBZone::getZoneByUserId()
     */
    public function testGetZoneByUserId()
    {
        $zone = new \Model\Zone(array());
        $zone->id = 1;
        $zone->userId = 1;
        $zone->content = 'content';
        $zone->canShare = 1;
        $zone->createTime = '2018-06-22 12:00:00';
        $zone->createBy = 1;
        $zone->updateTime = '2018-06-22 12:00:00';
        $zone->updateBy = 1;
        $this->assertEquals($zone, $this->zoneDB->getZoneByUserId(1));
        $this->assertNull($this->zoneDB->getZoneByUserId(111));
    }

    /**
     * @covers DB\CDBZone::getZoneByZoneId()
     */
    public function testGetZoneByZoneId()
    {
        $zone = new \Model\Zone(array());
        $zone->id = 1;
        $zone->userId = 1;
        $zone->content = 'content';
        $zone->canShare = 1;
        $zone->createTime = '2018-06-22 12:00:00';
        $zone->createBy = 1;
        $zone->updateTime = '2018-06-22 12:00:00';
        $zone->updateBy = 1;
        $this->assertEquals($zone, $this->zoneDB->getZoneByZoneId(1));
        $this->assertNull($this->zoneDB->getZoneByZoneId(111));
    }

    /**
     * @covers  DB\CDBZone::isZoneExist()
     */
    public function testIsZoneExist()
    {
        $this->assertTrue(true, $this->zoneDB->isZoneExist(1));
    }

    /**
     * @covers DB\CDBZone::getUsers()
     */
    public function testGetUsers()
    {
        $array = [[
            'userId' => 1
        ]];
        $this->assertEquals($array, $this->zoneDB->getUsers(11));
    }

    /**
     * @covers DB\CDBZone::getSameFamilyUsers()
     */
    public function testGetSameFamilyUsers()
    {
        $array = [[
            'userId' => 11
        ]];
        $this->assertEquals($array, $this->zoneDB->getSameFamilyUsers(1, 1));
    }

    /**
     * @covers DB\CDBZone::addSystemPost()
     */
    public function testAddSystemPost()
    {
        $post = new \Model\Post();
        $post->zoneId = 1;
        $post->userId = 1;
        $post->content = 'post content';
        $post->coorX = 11;
        $post->coorY = 11;
        $post->address = 'address';
        $post->photo = 'photo';
        $post->type = 1;
        $post->userId = 1;
        $this->assertEquals(2, $this->zoneDB->addSystemPost($post));
    }

    /**
     * @covers DB\CDBZone::addPost()
     */
    public function testAddPost()
    {
        $post = new \Model\Post();
        $post->zoneId = 1;
        $post->userId = 1;
        $post->content = 'post content';
        $post->coorX = 11;
        $post->coorY = 11;
        $post->address = 'address';
        $post->photo = 'photo';
        $post->type = 1;
        $post->userId = 1;
        $this->assertEquals(2, $this->zoneDB->addPost($post));
    }


    /**
     * @covers DB\CDBZone::updatePost()
     */
    public function testUpdatePost()
    {
        $postId = 1;
        $content = 'update content';
        $coorX = 20;
        $coorY = 22;
        $address = 'add';
        $update_time = '2018-06-22 17:00:00';
        $update_by = 1;
        $this->assertEquals(1, $this->zoneDB->updatePost($postId, $content, $coorX, $coorY, $address, $update_time, $update_by));
    }

    /**
     * @covers DB\CDBZone::deletePost()
     */
    public function testDeletePost()
    {
        $this->assertEquals(0,$this->zoneDB->deletePost(111));
        $this->assertEquals(1, $this->zoneDB->deletePost(1));
    }

    /**
     * @covers DB\CDBZone::getPost()
     */
    public function testGetPost()
    {
        $post = new \Model\Post();
        $post->id = 1;
        $post->zoneId = 1;
        $post->userId = 1;
        $post->author = 'jiang111';
        $post->authorPhoto = 'http://test.photo';
        $post->familyId = 1;
        $post->content = 'content';
        $post->coorX = 1;
        $post->coorY = 1;
        $post->address = 'address';
        $post->photo = null;
        $post->collection = 1;
        $post->likeStatus = 1;
        $post->type = 1;
        $post->commentCount = 10;
        $post->likeCount = 1;
        $post->createTime = '2018-06-22 12:00:00';
        $post->createBy = 1;
        $post->updateTime = '2018-06-22 12:00:00';
        $post->updateBy = 1;
        $this->assertEquals($post, $this->zoneDB->getPost(1));
        $this->assertNull($this->zoneDB->getPost(111));
    }

    /**
     * @covers  DB\CDBZone::getZonePostsPaging()
     */
    public function testGetZonePostsPaging()
    {
        $array = [[
            'id' => "1",
            'zoneId' => "1",
            'userId' => "1",
            'author' => "jiang111",
            'authorPhoto' => "http://test.photo",
            'content' => "content",
            'coorX' => "1",
            'coorY' => "1",
            'address' => "address",
            'photo' => "photo",
            'collection' => "1",
            'likeStatus' => "1",
            'commentCount' => "10",
            'likeCount' => "1",
            'type' => "1",
            'createTime' => "2018-06-22 12:00:00",
            'createBy' => "1",
            'updateTime' => "2018-06-22 12:00:00",
            'updateBy' => "1"

        ]];
        $this->assertEquals($array, $this->zoneDB->getZonePostsPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBZone::getZonePostsTotalCount()
     */
    public function testGetZonePostsTotalCount()
    {
        $this->assertEquals(1, $this->zoneDB->getZonePostsTotalCount(1));
    }

    /**
     * @covers DB\CDBZone::addPostComment()
     */
    public function testAddPostComment()
    {
        $postId = 1;
        $userId = 1;
        $replyTo = 1;
        $replyToUserId = 1;
        $comment = 'coooooooooo';
        $this->assertEquals(2, $this->zoneDB->addPostComment($postId, $userId, $replyTo, $replyToUserId, $comment, 5));
    }

    /**
     * @covers DB\CDBZone::updatePostComment()
     */
    public function testUpdatePostComment()
    {
        $commentId = 1;
        $comment = 'update comment';
        $updateTime = '2018-06-22 15:00:00';
        $updateBy = 1;
        $this->assertEquals(1, $this->zoneDB->updatePostComment($commentId, $comment, $updateTime, $updateBy));
    }

    /**
     * @covers DB\CDBZone::updatePostLikeCount()
     */
    public function testUpdatePostLikeCount()
    {
        $this->assertEquals(1, $this->zoneDB->updatePostLikeCount(1, 10));
    }

    /**
     * @covers DB\CDBZone::updatePostCommentCount()
     */
    public function testUpdatePostCommentCount()
    {
        $this->assertEquals(1, $this->zoneDB->updatePostCommentCount(1, 1));
    }

    /**
     * @covers DB\CDBZone::deletePostComment()
     */
    public function testDeletePostComment()
    {
        $this->assertEquals(1, $this->zoneDB->deletePostComment(1));
    }

    /**
     * @covers DB\CDBZone::getAllReplyComment()
     */
    public function testGetAllReplyComment()
    {
        $array = [[
            'postId' => "1",
            'userId' => "1",
            'replyto' => "1",
            'comment' => "comment",
            'create_time' => "2018-06-22 12:00:00",
            'create_by' => "1",
            'update_time' => "2018-06-22 12:00:00",
            'update_by' => "1"
        ]];
        $this->assertEquals($array, $this->zoneDB->getAllReplyComment(1));
    }

    /**
     * @covers DB\CDBZone::getPostCommentsTotal()
     */
    public function testGetPostCommentsTotal()
    {
        $array = [[
            'id' => "1",
            'userId' => "1",
            'replyto' => "1",
            'replytoUserId' => "1",
            'replytoUsername' => "jiang111",
            'comment' => "comment",
            'createTime' => "2018-06-22 12:00:00",
            'createByName' => "jiang111",
            'photo' => "http://test.photo",
            'createBy' => "1"
        ]];
        $this->assertEquals($array, $this->zoneDB->getPostCommentsTotal(1, 5));
    }

    /**
     * @covers DB\CDBZone::getRelatePostsPagingTotal_v1()
     */
    public function testGetRelatePostsPagingTotal_v1()
    {
        $this->assertEquals(1, $this->zoneDB->getRelatePostsPagingTotal_v1(1));
    }

    /**
     * @covers DB\CDBZone::getRelatePostsPagingTotal()
     */
    public function testGetRelatePostsPagingTotal()
    {
        $this->assertEquals(1, $this->zoneDB->getRelatePostsPagingTotal(1));
    }

    /**
     * @covers DB\CDBZone::getRelatePostsPaging()
     */
    public function testGetRelatePostsPaging()
    {
        $this->assertEquals(array(), $this->zoneDB->getRelatePostsPaging(1, 1, 1));
    }

//    /**
//     * @covers DB\CDBZone::getPostCountByTimestamp()
//     */
//    public function testGetPostCountByTimestamp(){
//
//    }

    /**
     * @covers DB\CDBZone::setPostOverReport()
     */
    public function testSetPostOverReport()
    {
        $this->assertEquals(1, $this->zoneDB->setPostOverReport(1));
    }

    /**
     * @covers DB\CDBZone::checkUserInBlackList()
     */
    public function testCheckUserInBlackList()
    {
        $this->assertTrue(true, $this->zoneDB->checkUserInBlackList(1, 2));
    }

    /**
     * @covers DB\CDBZone::addUserToBlackList()
     */
    public function testAddUserToBlackList()
    {
        $this->assertEquals(2, $this->zoneDB->addUserToBlackList(1, 3));
    }

    /**
     * @covers DB\CDBZone::getBlockByBlockId()
     */
    public function testGetBlockByBlockId()
    {
        $array = [
            'id' => "1",
            'userId' => "1",
            'block_userId' => "2",
            'create_time' => "2018-06-22 12:00:00",
            'create_by' => "1",
            'update_time' => "2018-06-22 12:00:00",
            'update_by' => "1"
        ];
        $this->assertEquals($array, $this->zoneDB->getBlockByBlockId(1));
    }

    /**
     * @covers DB\CDBZone::deleteUserFromBlackList()
     */
    public function testDeleteUserFromBlackList()
    {
        $this->assertEquals(1, $this->zoneDB->deleteUserFromBlackList(1));
    }

    /**
     * @covers DB\CDBZone::getUserBlackList()
     */
    public function testGetUserBlackList()
    {
        $array = [[
            'id' => "1",
            'userId' => "1",
            'nickname' => NULL,
            'photo' => NULL,
            'block_userId' => "2",
            'create_time' => "2018-06-22 12:00:00",
            'create_by' => "1",
            'update_time' => "2018-06-22 12:00:00",
            'update_by' => "1"

        ]];
        $this->assertEquals($array, $this->zoneDB->getUserBlackList(1));
    }

    /**
     * @covers DB\CDBZone::getFamilyPostsPaging
     */
    public function testGetFamilyPostsPaging() {
        $this->assertTrue(is_array($this->zoneDB->getFamilyPostsPaging(1, 1, 10, 1)));
    }

    /**
     * @covers DB\CDBZone::pushPostToFamily
     */
    public function testPushPostToFamily() {
        $this->assertTrue($this->zoneDB->pushPostToFamily(1, [1]));
    }

}

