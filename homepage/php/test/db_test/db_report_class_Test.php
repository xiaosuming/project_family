<?php

use DB\CDBReport;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-21
 * Time: 下午3:51
 */
class DBOReportTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $reportDB = null;


    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->reportDB = new CDBReport();
        return $this->createMySQLXMLDataSet(__DIR__ . '/xml/report_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBReport::addReport()
     */
    public function testAddReport()
    {
        $moduleId = 1;
        $recordId = 11;
        $reportType = 2;
        $userId = 1;
        $this->assertEquals(2, $this->reportDB->addReport($moduleId, $recordId, $reportType, $userId));
    }

    /**
     * @covers DB\CDBReport::checkUserReportExist()
     */
    public function testCheckUserReportExist()
    {
        $this->assertEquals(1, $this->reportDB->checkUserReportExist(1, 1, 1, 1));
        $this->assertFalse($this->reportDB->checkUserReportExist(11,11, 11, 11));
    }

    /**
     * @covers DB\CDBReport::getReportsTotal()
     */
    public function testGetReportsTotal()
    {
        $this->assertEquals(1, $this->reportDB->getReportsTotal(1, 1, 1));
    }

    /**
     * @covers  DB\CDBReport::getReportsPaging()
     */
    public function testGetReportsPaging()
    {
        $array = [[
            'id' => "1",
            'userId' => "1",
            'moduleId' => "1",
            'recordId' => "1",
            'createTime' => "2018-06-21 12:00:00",
            'createBy' => "1",
            'updateTime' => "2018-06-21 12:00:00",
            'updateBy' => "1"
        ]];
        $this->assertEquals($array,$this->reportDB->getReportsPaging(1, 1));
        $this->assertEquals($array,$this->reportDB->getReportsPaging(1, 1,1,1,1));
    }


}