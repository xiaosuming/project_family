<?php

use DB\CDBLookForFamily;
use Model\LookForFamilyTask;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-21
 * Time: 上午10:20
 */
class DBLookForFamilyTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;

    private $conn = null;

    private $lookForFamilyDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {

            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->lookForFamilyDB = new CDBLookForFamily();
        return $this->createMySQLXMLDataSet('db_test/xml/lookfor_family_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBLookForFamily::verifyTaskIdAndUserId()
     */
    public function testVerifyTaskIdAndUserId()
    {
        $this->assertTrue(true, $this->lookForFamilyDB->verifyTaskIdAndUserId(1, 1));
        $this->assertFalse( $this->lookForFamilyDB->verifyTaskIdAndUserId(11, 11));
    }

    /**
     * @covers DB\CDBLookForFamily::verifyTaskResultIdAndUserId()
     */
    public function testVerifyTaskResultIdAndUserId()
    {
        $this->assertTrue(true, $this->lookForFamilyDB->verifyTaskResultIdAndUserId(1, 1));
    }

    /**
     * @covers DB\CDBLookForFamily::getAllTask()
     */
    public function testGetAllTask()
    {
        $array = [[
            'id' => 1,
            'taskContent' => 'task_content',
            'excludeFamilyIds' => '1,2,2',
            'priority' => 1,
            'threshold' => 1,
            'email' => '123@qq.com',
            'cycle' => 1,
            'isFinish' => 1,
            'createBy' => 1,
            'createTime' => '2018-06-20 12:00:00',
            'updateBy' => 1,
            'updateTime' => '2018-06-20 12:00:00'
        ]];
        $this->assertEquals($array, $this->lookForFamilyDB->getAllTask(1));
        $this->assertEquals($array, $this->lookForFamilyDB->getAllTask(1, 1));
    }

    /**
     * @covers DB\CDBLookForFamily::addTask()
     */
    public function testAddTask()
    {
        $lookForFamily = new LookForFamilyTask();
        $lookForFamily->taskContent = 'content1';
        $lookForFamily->excludeFamilyIds = '1';
        $lookForFamily->priority = 2;
        $lookForFamily->threshold = 2;
        $lookForFamily->cycle = 1;
        $lookForFamily->email = 'email@email.com';
        $lookForFamily->createBy = 1;
        $lookForFamily->updateBy = 1;
        $this->assertEquals(2, $this->lookForFamilyDB->addTask($lookForFamily));
    }

    /**
     * @covers DB\CDBLookForFamily::updateTaskContent()
     */
    public function testUpdateTaskContent()
    {
        $this->assertEquals(0, $this->lookForFamilyDB->updateTaskContent(1, 'content22', 1));
    }

    /**
     * @covers DB\CDBLookForFamily::updateTaskSetting()
     */
    public function testUpdateTaskSetting()
    {
        $this->assertEquals(0, $this->lookForFamilyDB->updateTaskSetting(1, 2, 2, 2, 'email@a.com', 1));
    }

    /**
     * @covers DB\CDBLookForFamily::updateTaskStatus()
     */
    public function testUpdateTaskStatus()
    {
        $this->assertEquals(1, $this->lookForFamilyDB->updateTaskStatus(1, 2, 1));
    }

    /**
     * @covers DB\CDBLookForFamily::recommitTask()
     */
    public function testRecommitTask()
    {
        $this->assertEquals(1, $this->lookForFamilyDB->recommitTask(1, array(1, 2, 3), 1));
    }

    /**
     * @covers DB\CDBLookForFamily::getTaskResultPaging()
     */
    public function testGetTaskResultPaging()
    {
        $array = [[
            'id' => "1",
            'taskId' => "1",
            'isFind' => "1",
            'findContents' => "find_contents",
            'createTime' => "2018-06-20 12:00:00"
        ]];
        $this->assertEquals($array, $this->lookForFamilyDB->getTaskResultPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBLookForFamily::getTaskResultTotal()
     */
    public function testGetTaskResultTotal()
    {
        $this->assertEquals(1, $this->lookForFamilyDB->getTaskResultTotal(1));
    }

    /**
     * @covers DB\CDBLookForFamily::getTaskResult()
     */
    public function testGetTaskResult()
    {
        $array = [
            'id' => "1",
            'taskId' => "1",
            'isFind' => "1",
            'findContents' => "find_contents",
            'createTime' => "2018-06-20 12:00:00"
        ];
        $this->assertEquals($array, $this->lookForFamilyDB->getTaskResult(1));
    }
}