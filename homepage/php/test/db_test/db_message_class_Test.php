<?php

use DB\CDBMessage;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-21
 * Time: 上午11:48
 */
class DBMessageTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;

    private $conn = null;

    private $messageDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {

            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->messageDB = new CDBMessage();
        return $this->createMySQLXMLDataSet('db_test/xml/message_dataset.xml');     //默认的数据集
    }

//    /**
//     * @covers DB\CDBMessage::addMessage()
//     */
//    public function testAddMessage()
//    {
//        $message = new \Model\Message();
//        $message->messageCode = 'msgcode';
//        $message->fromUser = 1;
//        $message->toUser = 1;
//        $message->content = 'content';
//        $message->module = 1;
//        $message->action = 1;
//        $message->recordId = 1;
//        $message->extraRecordId = 1;
//        $this->assertEquals(2,$this->messageDB->addMessage($message));
//    }

    /**
     * @covers DB\CDBMessage::getMessage()
     */
    public function testGetMessage()
    {
        $message = new \Model\Message();
        $message->id = 1;
        $message->messageCode = 'message_code';
        $message->fromUser = 1;
        $message->fromUserName = 'jiang111';
        $message->toUser = 1;
        $message->content = 'content';
        $message->module = 0;
        $message->action = 1;
        $message->recordId = 1;
        $message->extraRecordId = 1;
        $message->createBy = 1;
        $message->createTime = '2017-11-11 00:00:00';
        $message->updateBy = 1;
        $message->updateTime = '2017-11-11 00:00:00';
        $message->isDelete = 0;
        $message->isRead = 0;
        $message->isAccept = 1;
        $this->assertEquals($message, $this->messageDB->getMessage(1));
        $this->assertNull($this->messageDB->getMessage(111));
    }

    /**
     * @covers DB\CDBMessage::deleteMessage()
     */
    public function testDeleteMessage()
    {
        $this->assertEquals(1, $this->messageDB->deleteMessage(1, 1));
    }

    /**
     * @covers DB\CDBMessage::getMessagesPaging()
     */
    public function testGetMessagesPaging()
    {
        $array = [[
            'id' => "1",
            'fromUser' => "1",
            'content' => "content",
            'module' => "0",
            'action' => "1",
            'recordId' => "1",
            'extraRecordId' => "1",
            'isRead' => "0",
            'isAccept' => "1",
            'createBy' => "1",
            'fromUserName' => "jiang111",
            'fromUserPhoto' => "http://test.photo",
            'createTime' => "2017-11-11 00:00:00",
            'ext' => ''
        ]];
        $this->assertEquals($array, $this->messageDB->getMessagesPaging(1, 1, 1, 0, 0));
    }

    /**
     * @covers DB\CDBMessage::getMessagesCount()
     */
    public function testGetMessagesCount()
    {
        $this->assertEquals(1, $this->messageDB->getMessagesCount(1, 0, 0));
    }

    /**
     * @covers DB\CDBMessage::updateMessageStatus()
     */
    public function testUpdateMessageStatus()
    {
        $this->assertEquals(1, $this->messageDB->updateMessageStatus(1, true, 1));
    }

    /**
     * @covers DB\CDBMessage::acceptInvite()
     */
    public function testAcceptInvite()
    {
        $this->assertFalse($this->messageDB->acceptInvite(1));
    }

    /**
     * @covers  DB\CDBMessage::refuseInvite()
     */
    public function testRefuseInvite()
    {
        $this->assertTrue($this->messageDB->refuseInvite(1));
        $this->assertFalse( $this->messageDB->refuseInvite(111));
    }

    /**
     * @covers  DB\CDBMessage::acceptApplyJoin()
     */
    public function testAcceptApplyJoin()
    {
        $this->assertFalse($this->messageDB->acceptApplyJoin(1, 1));
        $this->assertFalse($this->messageDB->acceptApplyJoin(111, 1));
    }

    /**
     * @covers DB\CDBMessage::refuseApplyJoin()
     */
    public function testRefuseApplyJoin()
    {
        $this->assertFalse($this->messageDB->refuseApplyJoin(1, 1));
        $this->assertFalse($this->messageDB->refuseApplyJoin(111, 1));
    }

//    /**
//     * @covers DB\CDBMessage::sendMergeFamilyMessage()
//     */
//    public function testSendMergeFamilyMessage(){
//
//    }

    /**
     * @covers DB\CDBMessage::acceptMergeFamily()
     */
    public function testAcceptMergeFamily()
    {
        $message = new \Model\Message();
        $message->recordId = 1;
        $message->action = 1;
        $message->id = 1;
        $this->assertEquals(-4, $this->messageDB->acceptMergeFamily($message, 1));
    }

//    /**
//     * @covers DB\CDBMessage::sendMergeBranchMessage()
//     */
//    public function testSendMergeBranchMessage()
//    {
//
//    }

    /**
     * @covers DB\CDBMessage::acceptMergeBranch()
     */
    public function testAcceptMergeBranch()
    {
        $message = new \Model\Message();
        $message->recordId = 1;
        $message->id = 1;
        $this->assertEquals(-3, $this->messageDB->acceptMergeBranch($message, 1, 'branchName', 1));
    }
}