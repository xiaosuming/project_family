<?php

use DB\CDBUserInvitationCode;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-21
 * Time: 下午4:51
 */
class DBUserInvitationCode extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $userInvitationCodeDB = null;


    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->userInvitationCodeDB = new CDBUserInvitationCode();
        return $this->createMySQLXMLDataSet(__DIR__ . '/xml/user_invitation_code_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBUserInvitationCode::addUserInvitationCode()
     */
    public function testAddUserInvitationCode()
    {
        $this->assertEquals(8, strlen($this->userInvitationCodeDB->addUserInvitationCode(1)));
    }


    /**
     * @covers DB\CDBUserInvitationCode::checkInvitationCode()
     */
    public function testCheckInvitationCode()
    {
        $this->assertTrue($this->userInvitationCodeDB->checkInvitationCode('code'));
        $this->assertFalse($this->userInvitationCodeDB->checkInvitationCode('ccccode'));
    }

    /**
     * @covers DB\CDBUserInvitationCode::checkUserHasCode()
     */
    public function testCheckUserHasCode()
    {
        $this->assertFalse(false, $this->userInvitationCodeDB->checkUserHasCode(2));
        $array = [
            'id' => 1,
            'code' => 'code',
            'create_time' => '2017-05-06 10:00:01',
            'create_by' => 1
        ];
        $this->assertEquals($array, $this->userInvitationCodeDB->checkUserHasCode(1));
    }

    /**
     * @covers DB\CDBUserInvitationCode::bindUserIdToInvitationCode()
     */
    public function testBindUserIdToInvitationCode()
    {
        $this->assertGreaterThan(0, $this->userInvitationCodeDB->bindUserIdToInvitationCode(1, 'code'));
        $this->assertEquals(-1, $this->userInvitationCodeDB->bindUserIdToInvitationCode(1, '111code'));
    }

}