<?php

use DB\CDBFamilyLegend;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-20
 * Time: 上午10:23
 */
class DBFamilyLegendTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;

    private $conn = null;

    private $familyLegendDB = null;

    protected function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    protected function getDataSet()
    {
        $this->familyLegendDB = new CDBFamilyLegend();
        return $this->createMySQLXMLDataSet('db_test/xml/family_legend_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBFamilyLegend::verifyUserIdAndLegendId()
     */
    public function testVerifyUserIdAndLegendId()
    {
        $this->assertTrue($this->familyLegendDB->verifyUserIdAndLegendId(1,1));
        $this->assertFalse($this->familyLegendDB->verifyUserIdAndLegendId(1,111));
    }

    /**
     * @covers DB\CDBFamilyLegend::addLegend()
     */
    public function testAddLegend()
    {
        $familyId = 1;
        $title = 'title1';
        $content = 'content1';
        $userId = 1;
        $this->assertEquals(2,$this->familyLegendDB->addLegend($familyId, $title, $content, $userId));
    }

    /**
     * @covers DB\CDBFamilyLegend::updateLegend()
     */
    public function testUpdateLegend()
    {
        $legendId =1;
        $title = 'tttt';
        $content = 'cccc';
        $userId = 1;
        $this->assertEquals(1,$this->familyLegendDB->updateLegend($legendId, $title, $content, $userId));
    }

    /**
     * @covers DB\CDBFamilyLegend::deleteLegend()
     */
    public function testDeleteLegend()
    {
        $this->assertEquals(1,$this->familyLegendDB->deleteLegend(1));
    }

    /**
     * @covers DB\CDBFamilyLegend::getLegend()
     */
    public function testGetLegend()
    {
        $familyLegend = new \Model\FamilyLegend(array());
        $familyLegend->id = 1;
        $familyLegend->title = 'title';
        $familyLegend->content = 'content';
        $familyLegend->familyId = 1;
        $familyLegend->createTime = '2018-06-19';
        $familyLegend->createBy = 1;
        $familyLegend->updateTime = '2018-06-19';
        $familyLegend->updateBy = 1;
        $this->assertEquals($familyLegend,$this->familyLegendDB->getLegend(1));
        $this->assertNull($this->familyLegendDB->getLegend(111));
    }

    /**
     * @covers DB\CDBFamilyLegend::getLegendsTotal()
     */
    public function testGetLegendsTotal()
    {
        $this->assertEquals(1,$this->familyLegendDB->getLegendsTotal(1));
    }

    /**
     * @covers DB\CDBFamilyLegend::getLegendsPaging()
     */
    public function testGetLegendsPaging()
    {
        $array = [[
            'id'=>1,
            'title'=>'title',
            'content'=>'content',
            'familyId'=>1,
            'createTime'=>'2018-06-19',
            'createBy'=>1,
            'updateTime'=>'2018-06-19',
            'updateBy'=>1
        ]];
        $this->assertEquals($array,$this->familyLegendDB->getLegendsPaging(1,1,1));
    }

}