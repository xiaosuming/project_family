<?php

use DB\CDBArea;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-5-10
 * Time: 上午10:28
 */

class DBAreaTest extends Testcase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $areaDB = null;

    final public function getConnection()
    {
        if($this->conn === null){
            if(self::$pdo === null){
                self::$pdo = new PDO( $GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD'] );
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo,$GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }


    public function getDataSet()
    {
        $this->areaDB = new CDBArea();
        return $this->createMySQLXMLDataSet('db_test/xml/area_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBArea::getFirstArea()
     */
    public function testGetFirstArea(){
        $array = [[
            'areaId' =>'1',
            'areaName'=>'china'
        ]];
        $result = $this->areaDB->getFirstArea();
        $this->assertEquals($array,$result);
    }

    /**
     * @covers DB\CDBArea::getOtherArea()
     */
    public function testGetOtherArea(){
        $array = [[
            'areaId'=>'2',
            'areaName'=>'taiwan'
        ]];
        $result = $this->areaDB->getOtherArea(1);
        $this->assertEquals($array,$result);
    }

    /**
     * @covers DB\CDBArea::getAreaNameById()
     */
    public function testGetAreaNameById(){
        $array = [
            'areaName'=>'china'
        ];
        $array1= [
            'areaName'=>'taiwan'
        ];
        $this->assertEquals($array,$this->areaDB->getAreaNameById(1));
        $this->assertEquals($array1,$this->areaDB->getAreaNameById(2));
        $this->assertFalse($this->areaDB->getAreaNameById(200000));
    }

}