<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/14
 * Time: 16:06
 */

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use Model\Album;
use Model\Photo;
use DB\CDBAlbum;

class CDBAlbumTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $albumDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    public function getDataSet()
    {
        $this->albumDB = new CDBAlbum();
        return $this->createMySQLXMLDataSet('db_test/xml/album_test.xml');     //默认的数据集
    }

    /** 测试是否能创建相册
     * @covers DB\CDBAlbum::createAlbum
     */
    public function testCreateAlbum()
    {
        $albumName = "xll";
        $description = "hello world";
        $familyId = 1;
        $updateBy = 1;
        $createBy = 1;
        $this->albumDB->createAlbum($albumName, $description, $familyId, $createBy, $updateBy);
        $this->assertEquals(4, $this->getConnection()->getRowCount('gener_family_album'));

    }

    /**
     * @covers DB\CDBAlbum::editAlbum()
     */
    public function testEditAlbum()
    {
        $albumId = 1;
        $album_name = 'name1';
        $description = 'description';
        $userId = 1;
        $this->assertEquals(1, $this->albumDB->editAlbum($albumId, $album_name, $description, $userId));
    }

    /** 测试是否能得到相册
     * @covers DB\CDBAlbum::getAlbumById
     */
    public function testGetAlbumById()
    {
        $result = array();
        $result['id'] = 1;
        $result['albumName'] = "ICDI";
        $result['description'] = "hhhh";
        $result['familyId'] = 2;
        $result['albumSize'] = 100;
        $result['createTime'] = "2017-11-08 12:44:02";
        $result['updateTime'] = "2017-11-08 12:44:02";
        $result['is_delete'] = 0;
        $result['updateBy'] = 1;
        $result['createBy'] = 1;
        $album = new Album($result);
        //有数据
        $this->assertEquals($album, $this->albumDB->getAlbumById(1));
        //无数据
        $this->assertEquals(null, $this->albumDB->getAlbumById(4));
    }

    /** 测试是否能得到照片
     * @covers DB\CDBAlbum::getPhotoById
     */
    public function testGetPhotoById()
    {
        $result = array();
        $result['id'] = 1;
        $result['albumId'] = 1;
        $result['photo'] = "pppppppp";
        $result['description'] = "hhhh";
        $result['country'] = '';
        $result['province'] = '';
        $result['city'] = '';
        $result['area'] = '';
        $result['town'] = '';
        $result['size'] = 50;
        $result['address'] = "shanghai";
        $result['createTime'] = "2017-11-08 12:44:02";
        $result['is_delete'] = 0;
        $result['createBy'] = 1;
        $photo = new Photo($result);
        //有数据
        $this->assertEquals($photo, $this->albumDB->getPhotoById(1));
        //无数据
        $this->assertEquals(null, $this->albumDB->getPhotoById(4));
    }

    /**
     * @covers DB\CDBAlbum::getBatchPhotoHaveDelete
     */
    public function testGetBatchPhotoHaveDelete()
    {
        $array = [[
            'id' => "2",
            'albumId' => "1",
            'photo' => "pppppppp",
            'description' => "hhhh",
            'countryName' => NULL,
            'provinceName' => NULL,
            'cityName' => NULL,
            'areaName' => NULL,
            'townName' => NULL,
            'size' => "100",
            'address' => "shanghai",
            'createTime' => "2017-11-08 12:44:02",
            'createBy' => "1"
        ]];
        $this->assertEquals($array, $this->albumDB->getBatchPhotoHaveDelete(2));
    }

    /** 测试删除相册
     * @covers DB\CDBAlbum::deleteAlbum
     */
    public function testDeleteAlbum()
    {
        //有记录
        $this->assertTrue($this->albumDB->deleteAlbum(1));
        //有记录但是已经删除
        $this->assertTrue($this->albumDB->deleteAlbum(2));
        //无记录
        $this->assertTrue($this->albumDB->deleteAlbum(4));
    }

    /** 测试改变相册大小
     * @covers DB\CDBAlbum::changeAlbumSize
     */
    public function testChangeAlbumSize()
    {
        //增加且相册未删除
        $this->assertTrue($this->albumDB->changeAlbumSize(100, 1, 1, 1));
        //增加且相册删除
        $this->assertFalse($this->albumDB->changeAlbumSize(100, 2, 1, 1));
        //减少且相册未删除
        $this->assertTrue($this->albumDB->changeAlbumSize(100, 1, 1, 0));
        //减少且相册删除
        $this->assertFalse($this->albumDB->changeAlbumSize(100, 2, 1, 0));
    }

    /**
     * @covers DB\CDBAlbum::createAlbumPerson()
     */
    public function testCreateAlbumPerson()
    {
        $this->assertTrue(true, $this->albumDB->createAlbumPerson(1, 1, 1));
    }

    /**
     * @covers DB\CDBAlbum::clearPhotoPerson()
     */
    public function testClearPhotoPerson()
    {
        $this->assertEquals(1, $this->albumDB->clearPhotoPerson(1));
    }

    /**
     * @covers DB\CDBAlbum::updatePhotoPerson()
     */
    public function testUpdatePhotoPerson()
    {
        $this->assertTrue(true, $this->albumDB->updatePhotoPerson(1, 1, 1));
    }

    /**测试将照片加到相册
     * @covers DB\CDBAlbum::addPhotoToAlbum
     *
     */
    public function testAddPhotoToAlbum()
    {
        //向存在的相册中添加
        $this->assertEquals(4, $this->albumDB->addPhotoToAlbum(1, 'photo', 'description', 1, 'china', 1, 'shanghai', 1, 'shanghai', 1, 'pudong', 1, 'sanlin', 100, 'address', 1));
        $this->assertEquals(4, $this->getConnection()->getRowCount('gener_family_photo'));
        //向不存在的相册中添加
        $this->assertEquals(-1, $this->albumDB->addPhotoToAlbum(2, 'photo', 'description', 1, 'china', 1, 'shanghai', 1, 'shanghai', 1, 'pudong', 1, 'sanlin', 100, 'address', 1));
    }

    /**
     * @covers DB\CDBAlbum::editPhoto()
     */
    public function testEditPhoto()
    {
        $photoId = 1;
        $description = 'description';
        $country = 1;
        $countryName = 'china';
        $province = 1;
        $provinceName = 'shanghai';
        $city = 1;
        $cityName = 'shanghai';
        $area = 1;
        $areaName = 'pujiang';
        $town = 1;
        $townName = 'sanlin';
        $address = 'address';
        $this->assertEquals(1, $this->albumDB->editPhoto($photoId, $description, $country, $countryName, $province, $provinceName,
            $city, $cityName, $area, $areaName, $town, $townName, $address));
    }

    /**测试从相册删除照片
     * @covers DB\CDBAlbum::deletePhotoFromAlbum
     */
    public function testDeletePhotoFromAlbum()
    {
        //该照片未删除 album_size >= size
        $this->assertTrue($this->albumDB->deletePhotoFromAlbum(1, 1));
        //该照片未删除 album_size <= size
        $this->assertFalse($this->albumDB->deletePhotoFromAlbum(3, 1));
        //该照片已经删除
        $this->assertTrue($this->albumDB->deletePhotoFromAlbum(2, 1));
    }

    /**
     * @covers DB\CDBAlbum::deleteBatchPhotoFromAlbum
     */
    public function testDeleteBatchPhotoFromAlbum()
    {
        $this->assertEquals(1, $this->albumDB->deleteBatchPhotoFromAlbum(1, 1));
    }

    /** 测试得到相册数量
     * @covers DB\CDBAlbum::getAlbumCount
     *
     */
    public function testGetAlbumCount()
    {
        //查看的应该包括未删除的
        $this->assertEquals(2, $this->albumDB->getAlbumCount(2, 1));
        $this->assertEquals(0, $this->albumDB->getAlbumCount(-1, 1));
    }

    /**
     * @covers DB\CDBAlbum::getAlbum()
     */
    public function testGetAlbum()
    {
        $album = new Album(array());
        $album->id = 1;
        $album->familyId = 2;
        $album->albumName = 'ICDI';
        $album->description = 'hhhh';
        $album->photo = 'pppppppp';
        $album->albumSize = '100';
        $album->createTime = '2017-11-08 12:44:02';
        $album->createBy = 1;
        $album->updateTime = '2017-11-08 12:44:02';
        $album->updateBy = 1;
        $album->photoCount = '';
        $this->assertEquals($album, $this->albumDB->getAlbum(1));
        $this->assertNull($this->albumDB->getAlbum(111));
    }

    /**
     * @covers DB\CDBAlbum::getAlbumPaging()
     */
    public function testGetAlbumPaging()
    {
        $album = new Album(array());
        $album->id = 3;
        $album->familyId = 2;
        $album->albumName = 'ICDI';
        $album->description = 'hhhh';
        $album->photo = 'pppppppp';
        $album->albumSize = '10';
        $album->createTime = '2017-11-08 12:44:02';
        $album->createBy = 1;
        $album->updateTime = '2017-11-08 12:44:02';
        $album->updateBy = 1;
        $album->photoCount = 1;
        $this->assertEquals(array($album), $this->albumDB->getAlbumPaging(1, 1, 2, 1));
        $this->assertEquals(array(), $this->albumDB->getAlbumPaging(1, 1, -1, 1));
    }

    /**测试得到照片数量
     * @covers DB\CDBAlbum::getPhotoCount
     */
    public function testGetPhotoCount()
    {
        //查看包括未删除的
        $this->assertEquals(1, $this->albumDB->getPhotoCount(1));
    }

    /**
     * @covers DB\CDBAlbum::getPhotoPaging()
     */
    public function testGetPhotoPaging()
    {
        $array = [[
            'id' => "1",
            'albumId' => "1",
            'photo' => "pppppppp",
            'description' => "hhhh",
            'countryName' => NULL,
            'provinceName' => NULL,
            'areaName' => NULL,
            'townName' => NULL,
            'size' => "50",
            'address' => "shanghai",
            'createTime' => "2017-11-08 12:44:02",
            'createBy' => "1"
        ]];

        $this->assertEquals($array, $this->albumDB->getPhotoPaging(1, 1, 1));
    }

    /**
     * @covers DB\CDBAlbum::getPersonPhotoCount()
     */
    public function testGetPersonPhotoCount()
    {
        $this->assertEquals(1, $this->albumDB->getPersonPhotoCount(2, 1, 'xll'));
        $this->assertEquals(1, $this->albumDB->getPersonPhotoCount(2, 1, ''));
        $this->assertEquals(1, $this->albumDB->getPersonPhotoCount(2, -1, 'xll'));
    }

    /**
     * @covers DB\CDBAlbum::getPersonPhotoPaging()
     */
    public function testGetPersonPhotoPaging()
    {
        $array = [[
            'id' => "1",
            'albumId' => "1",
            'photo' => "pppppppp",
            'description' => "hhhh",
            'countryName' => NULL,
            'provinceName' => NULL,
            'areaName' => NULL,
            'townName' => NULL,
            'size' => "50",
            'address' => "shanghai",
            'createTime' => "2017-11-08 12:44:02",
            'createBy' => "1"

        ]];

        $this->assertEquals($array, $this->albumDB->getPersonPhotoPaging(2, 1, 1, 1, 'xll'));
        $this->assertEquals($array, $this->albumDB->getPersonPhotoPaging(2, 1, 1, 1, ''));
        $this->assertEquals($array, $this->albumDB->getPersonPhotoPaging(2, -1, 1, 1, 'xll'));
    }

    /**
     * @covers DB\CDBAlbum::getAllPhotosDetail()
     */
    public function testGetAllPhotosDetail()
    {
        $array = [[
            'id' => "1",
            'albumId' => "1",
            'photo' => "pppppppp",
            'description' => "hhhh",
            'countryName' => NULL,
            'provinceName' => NULL,
            'areaName' => NULL,
            'townName' => NULL,
            'size' => "50",
            'address' => "shanghai",
            'createTime' => "2017-11-08 12:44:02",
            'createBy' => "1"

        ]];

        $this->assertEquals($array, $this->albumDB->getAllPhotosDetail(2, 1, 'xll'));
        $this->assertEquals($array, $this->albumDB->getAllPhotosDetail(2, 1, ''));
        $this->assertEquals($array, $this->albumDB->getAllPhotosDetail(2, -1, 'xll'));
    }

    /**
     * @covers  DB\CDBAlbum::getPersonInPhoto()
     */
    public function testGetPersonInPhoto()
    {
        $array = [[
            'name' => 'xll'
        ]];
        $this->assertEquals($array, $this->albumDB->getPersonInPhoto(1));
    }

}
