<?php
/**
 * 家族单元测试
 */
require_once("../vendor/autoload.php");

use DB\CDBFamily;
use Model\Family;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;
use Monolog\Logger;
use DB\CDBPerson;
use DB\CDBManager;
use Util\Util;
use Model\MiniFamilyUpdateApply;
use Model\MiniFamilyBindRecommend;
use Model\Person;

class DBFamilyTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $familyDB = null;
    
    public $pdoS = null;
    public $TABLE = "gener_family";
    public $TABLE_PERSON = "gener_person";

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    public function getDataSet()
    {
        $this->familyDB = new CDBFamily();
        return $this->createMySQLXMLDataSet('db_test/xml/family_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBFamily::verifyUserIdAndFamilyIdByBranch()
     */
    public function testVerifyUserIdAndFamilyIdByBranch()
    {
        $this->assertTrue($this->familyDB->verifyUserIdAndFamilyIdByBranch(1, 22, 22));
        $this->assertFalse($this->familyDB->verifyUserIdAndFamilyIdByBranch(2, 22, 22));
        $this->assertTrue($this->familyDB->verifyUserIdAndFamilyIdByBranch(1, 22, 2));
        $this->assertFalse($this->familyDB->verifyUserIdAndFamilyIdByBranch(1, 23, 2));
    }

    /**
     * @covers DB\CDBFamily::verifyUserIdAndBranchIdByFamilyId()
     */
    public function testVerifyUserIdAndBranchIdByFamilyId()
    {
        $this->assertTrue($this->familyDB->verifyUserIdAndBranchIdByFamilyId(1, 1, 22));
        $this->assertFalse($this->familyDB->verifyUserIdAndBranchIdByFamilyId(16, 1, 22));
        $this->assertFalse($this->familyDB->verifyUserIdAndBranchIdByFamilyId(1, 3, 22));
    }

    /**
     * @covers DB\CDBFamily::verifyUserReadPermissionByMerge()
     */
    public function testVerifyUserReadPermissionByMerge()
    {
        $this->assertFalse($this->familyDB->verifyUserReadPermissionByMerge(1, 22));
        $this->assertTrue($this->familyDB->verifyUserReadPermissionByMerge(1, 2));
    }

    /**
     * @covers DB\CDBFamily::initFamilyStorage()
     */
    public function testInitFamilyStorage()
    {
        $this->assertEquals(2, $this->familyDB->initFamilyStorage(2));
    }

    /**
     * @covers DB\CDBFamily::createFamily
     * @covers DB\CDBFamily::createFamilyInitCallback
     * @covers DB\CDBFamily::initFamilyStorage
     */
    public function testCreateFamily()
    {
        $userId = 1;
        $familyName = 'xll-family';
        $adminName = 'xll';
        $surname = 'x';
        $adminGender = '1';
        $address = 'address';
        $birthday = '1993-10-12';
        $desc = 'description';
        $photo = 'photo';
        $familyPhoto = 'familyPhoto';
        $hxChatRoom = '';
        $groupType = 1;
        $GLOBALS['userId'] = 1;
        
        list($familyId, $personId) = $this->familyDB->createFamily($userId, $familyName, $adminName, $surname, $adminGender, $address, $birthday, $desc, $photo, $familyPhoto, $hxChatRoom, $groupType);
        $this->assertEquals(26, $familyId);
        $this->assertGreaterThan(0, $personId);
    }


    /**
     * @covers DB\CDBFamily::addFamilyUser()
     */
    public function testAddFamilyUser()
    {
        //var_dump($this->familyDB->addFamilyUser(22,1));
        $this->assertEquals(3, $this->familyDB->addFamilyUser(22, 1));
    }

    /**
     * @covers DB\CDBFamily::deleteFamilyUser()
     */
    public function testDeleteFamilyUser()
    {
        $this->assertEquals(1, $this->familyDB->deleteFamilyUser(22, 1));
    }


    /**
     * @covers DB\CDBFamily::deleteFamily()
     */
    public function testDeleteFamily()
    {
        $this->assertEquals(1, $this->familyDB->deleteFamily(22, 1));
    }

    /**
     * @covers DB\CDBFamily::getFamilyById()
     */
    public function testGetFamilyById()
    {
        $family = new Family(array());
        $family->id = '22';
        $family->branchId = '';
        $family->name = 'haha';
        $family->surname = 'haha';
        $family->description = 'hahahahaha';
        $family->photo = 'c';
        $family->qrcode = '1';
        $family->minLevel = '-5';
        $family->maxLevel = '5';
        $family->permission = '0';
        $family->hxChatRoom = '1';
        $family->originator = '2';
        $family->memberCount = '6';
        $family->desc = 'hahahahaha';
        $family->openType = '0';
        $family->groupType = '1';
        $family->identity = '1';
        $family->createTime = '2017-11-11 00:00:00';
        $family->createBy = '1';
        $family->updateTime = '2017-11-11 00:00:00';
        $family->updateBy = '1';
        $family->socialCircleId = '1';
        $family->type = '1';

        $this->assertEquals($family, $this->familyDB->getFamilyById(22));
        $this->assertNull($this->familyDB->getFamilyById(222));
    }

    /**
     * @covers DB\CDBFamily::getFamilyByIds()
     */
    public function testGetFamilyByIds()
    {
        $array = [[
            'id' => "22",
            'name' => "haha",
            'surname' => "haha",
            'minLevel' => "-5",
            'maxLevel' => "5",
            'hxChatRoom' => "1",
            'qrcode' => "1",
            'permission' => "0",
            'originator' => "2",
            'memberCount' => "6",
            'description' => "hahahahaha",
            'photo' => "c",
            'groupType' => '1',
            'createBy' => "1",
            'createTime' => "2017-11-11 00:00:00",
            'updateBy' => "1",
            'updateTime' => "2017-11-11 00:00:00",
            'socialCircleId' => '1'
        ]];
        $this->assertEquals($array, $this->familyDB->getFamilyByIds([22]));
    }

    /**
     * @covers DB\CDBFamily::updateFamilyLevel()
     */
    public function testUpdateFamilyLevel()
    {
        $this->assertEquals(1, $this->familyDB->updateFamilyLevel(22, 1, 1));
    }

    /**
     * @covers DB\CDBFamily::updateFamilyDescription()
     */
    public function testUpdateFamilyDescription()
    {
        $this->assertEquals(1, $this->familyDB->updateFamilyDescription(22, 'dedede'));
    }

    /**
     * @covers DB\CDBFamily::updateFamilyPhoto()
     */
    public function testUpdateFamilyPhoto()
    {
        $this->assertEquals(1, $this->familyDB->updateFamilyPhoto(22, 'ppp'));
    }


    /**
     * @covers DB\CDBFamily::updateFamilyName()
     */
    public function testUpdateFamilyName()
    {
        $this->assertEquals(1, $this->familyDB->updateFamilyName(22, 'faname'));
    }

    /**
     * @covers DB\CDBFamily::getUserCreateFamilyCount()
     */
    public function testGetUserCreateFamilyCount()
    {
        $this->assertEquals(4, $this->familyDB->getUserCreateFamilyCount(1));
    }

    /**
     * @covers DB\CDBFamily::getMyManageFamily()
     */
    public function testGetMyManageFamily()
    {
        $array = [[
            'id' => "22",
            'name' => "haha",
            'surname' => "haha",
            'maxLevel' => "5",
            'minLevel' => "-5",
            'hxChatRoom' => "1",
            'permission' => "0",
            'originator' => "2",
            'originatorName' => 'xu111',
            'memberCount' => "6",
            'description' => "hahahahaha",
            'photo' => "c",
            'groupType' => '1',
            'createBy' => "1",
            'createTime' => "2017-11-11 00:00:00",
            'updateBy' => "1",
            'updateTime' => "2017-11-11 00:00:00"
        ]];
        $this->assertEquals($array, $this->familyDB->getMyManageFamily(1));
    }

    /**
     * @covers DB\CDBFamily::getJoinFamily()
     */
    public function testGetJoinFamily()
    {
        $count = count($this->familyDB->getJoinFamily(1));
        $this->assertEquals(6, $count);
    }

    /**
     * @covers  DB\CDBFamily::getJoinFamilyForTmp()
     */
    public function testGetJoinFamilyForTmp()
    {
        $array = [[
            'id' => "22",
            'name' => "haha",
            'surname' => "haha",
            'maxLevel' => "5",
            'minLevel' => "-5",
            'hxChatRoom' => "1",
            'permission' => "0",
            'originator' => "2",
            'originatorName' => 'xu111',
            'memberCount' => "6",
            'description' => "hahahahaha",
            'photo' => "c",
            'groupType' => '1',
            'createBy' => "1",
            'createTime' => "2017-11-11 00:00:00",
            'updateBy' => "1",
            'updateTime' => "2017-11-11 00:00:00",
            'identity' => '1'
        ]];
        $this->assertEquals($array, $this->familyDB->getJoinFamilyForTmp(1));
    }

    /**
     * @covers DB\CDBFamily::getJoinFamilyList()
     */
    public function testGetJoinFamilyList()
    {
        $count = count($this->familyDB->getJoinFamilyList(1));
        $this->assertEquals(6, $count);
    }

    /**
     * @covers DB\CDBFamily::getApplyUserList()
     */
    public function testGetApplyUserList()
    {
        $array = [[
            'userId' => "1",
            'username' => "jiang",
            'nickname' => "jiang111",
            'photo' => "http://test.photo",
            'createTime' => "2017-09-09 09:09:09",
            'updateTime' => "2017-09-09 09:09:09"
        ]];
        $this->assertEquals($array, $this->familyDB->getApplyUserList(22));
    }

    /**
     * @covers DB\CDBFamily::isUserForApply()
     */
    public function testIsUserForApply()
    {
        $this->assertTrue($this->familyDB->isUserForApply(1, 22));
        $this->assertFalse($this->familyDB->isUserForApply(100, 22));
    }

    /**
     * @covers DB\CDBFamily::isOriginatorForFamily()
     */
    public function testIsOriginatorForFamily()
    {
        $this->assertFalse($this->familyDB->isOriginatorForFamily(22, 1));
        $this->assertTrue($this->familyDB->isOriginatorForFamily(22, 2));
    }

    /**
     * @covers DB\CDBFamily::isAdminForFamily()
     */
    public function testIsAdminForFamily()
    {
        $this->assertTrue($this->familyDB->isAdminForFamily(22, 1));
        $this->assertFalse($this->familyDB->isAdminForFamily(22, 2));

    }

    /**
     * @covers DB\CDBFamily::isUserForFamily()
     */
    public function testIsUserForFamily()
    {
        $this->assertTrue($this->familyDB->isUserForFamily(22, 1));
        $this->assertTrue($this->familyDB->isUserForFamily(2, 1));
        $this->assertFalse($this->familyDB->isUserForFamily(3, 1));
    }

    /**
     * @covers DB\CDBFamily::addAdmin()
     */
    public function testAddAdmin()
    {
        $this->assertTrue($this->familyDB->addAdmin(22, 1));
        $GLOBALS['userId'] = 2;
        $this->assertTrue($this->familyDB->addAdmin(22, 2));
        $this->assertFalse($this->familyDB->addAdmin(22, 22));
    }

    /**
     * @covers DB\CDBFamily::rescindAdmin()
     */
    public function testRescindAdmin()
    {
        $this->assertTrue($this->familyDB->rescindAdmin(22, 1));
        $this->assertFalse($this->familyDB->rescindAdmin(22, 11));
    }

    /**
     * @covers DB\CDBFamily::transferOriginator()
     */
    public function testTransferOriginator()
    {
        $this->assertTrue($this->familyDB->transferOriginator(22, 1));
        $this->assertFalse($this->familyDB->transferOriginator(22, 1));
    }

    /**
     * @covers DB\CDBFamily::getFamilyOriginatorId()
     */
    public function testGetFamilyOriginatorId()
    {
        $this->assertEquals(2, $this->familyDB->getFamilyOriginatorId(22));
        $this->assertNull($this->familyDB->getFamilyOriginatorId(100));
    }

    /**
     * @covers DB\CDBFamily::getFamilyAdminId()
     */
    public function testGetFamilyAdminId()
    {
        $this->assertEquals(array(array('adminId' => 1)), $this->familyDB->getFamilyAdminId(22));
    }

    /**
     * @covers DB\CDBFamily::getFamilyQRCode()
     */
    public function testGetFamilyQRCode()
    {
        $this->assertEquals(array('qrcode' => '1'), $this->familyDB->getFamilyQRCode(22));
    }

    /**
     * @covers  DB\CDBFamily::updateFamilyQRCode()
     */
    public function testUpdateFamilyQRCode()
    {
        $this->assertEquals(1, $this->familyDB->updateFamilyQRCode(22, '2'));
    }

    /**
     * @covers DB\CDBFamily::getFamilyIdentity()
     */
    public function testGetFamilyIdentity()
    {
        $this->assertEquals(1, $this->familyDB->getFamilyIdentity(22));
    }

    /**
     * @covers DB\CDBFamily::getFamilyIdByIdentity()
     */
    public function testGetFamilyIdByIdentity()
    {
        $this->assertEquals(22, $this->familyDB->getFamilyIdByIdentity(1));
        $this->assertNull($this->familyDB->getFamilyIdByIdentity(111111));
    }

    /**
     * @covers DB\CDBFamily::getFamilyInfoByIdentity()
     */
    public function testGetFamilyInfoByIdentity()
    {
        $array = [
            'id' => "22",
            'name' => "haha",
            'surname' => "haha",
            'photo' => "c",
            'description' => "hahahahaha"
        ];

        $this->assertEquals($array, $this->familyDB->getFamilyInfoByIdentity(1));
    }


    /**
     * @covers DB\CDBFamily::acceptApply()
     */
    public function testAcceptApply()
    {
        $this->assertEquals(3, $this->familyDB->acceptApply(22, 2));
    }

    /**
     * @covers DB\CDBFamily::getChatRoomByFamilyId()
     */
    public function testGetChatRoomByFamilyId()
    {
        $this->assertEquals(1, $this->familyDB->getChatRoomByFamilyId(22));
        $this->assertNull($this->familyDB->getChatRoomByFamilyId(222));
    }

    /**
     * @covers DB\CDBFamily::mergeFamily
     * @covers DB\CDBFamily::mergeTwoPerson
     */
    public function testMergeFamily()
    {
        $GLOBALS['MERGE_FAMILY_BY_MARRIAGE'] = '13';
        $GLOBALS['MERGE_FAMILY_BY_PERSON'] = '14';
        $GLOBALS['userId'] = '1';
        $this->assertEquals(4, $this->familyDB->mergeFamily(1, 2, 13));
        $this->assertEquals(2, $this->familyDB->mergeFamily(1, 2, 14));
    }

    /**
     * @covers DB\CDBFamily::addBranch()
     */
    public function testAddBranch()
    {
        $branch = new \Model\Branch();
        $branch->branchName = 'b1';
        $branch->branchPersonId = 1;
        $branch->type = 1;
        $branch->createBy = 1;
        $branch->updateBy = 1;
        $this->assertEquals(2, $this->familyDB->addBranch($branch));
    }

    /**
     * @covers DB\CDBFamily::addFamilyBranch()
     */
    public function testAddFamilyBranch()
    {
        $familyId = 22;
        $branchId = 1;
        $userId = 1;
        $this->assertEquals(3, $this->familyDB->addFamilyBranch($familyId, $branchId, $userId));
    }


    /**
     * @covers DB\CDBFamily::getBranchesByFamilyId()
     */
    public function testGetBranchesByFamilyId()
    {
        $this->assertEquals(1, count($this->familyDB->getBranchesByFamilyId(22)));
    }

    /**
     * @covers DB\CDBFamily::mergeBranch()
     * @covers DB\CDBFamily::updatePersonBranchId
     * @covers DB\CDBFamily::updatePersonBranchIdByPersonId
     * @covers DB\CDBFamily::getFamilyIdsByBranchId
     * @covers DB\CDBFamily::getBranchIdsByFamilyId
     */
    public function testMergeBranch()
    {
        $this->assertEquals(1, $this->familyDB->mergeBranch(1, 2, 'all', 22));
        $this->assertEquals(1, $this->familyDB->mergeBranch(3, 2, 'all', 22));
    }

    /**
     * @covers DB\CDBFamily::getApplyingUserList()
     */
    public function testGetApplyingUserList()
    {
        $this->assertEquals(array(), $this->familyDB->getApplyingUserList(22, 1));
    }


    /**
     * @covers DB\CDBFamily::getFamilyGenderInfo()
     */
    public function testGetFamilyGenderInfo()
    {
        $array = [
            'female' => 3,
            'male' => 2
        ];
        $this->assertEquals($array, $this->familyDB->getFamilyGenderInfo(22));
    }

    /**
     * 测试 获取家族年龄分布
     *
     * @cover DB\CDBFamily::getFamilyAgeInfo
     */
    public function testGetFamilyAgeInfo()
    {

        //familyId不存在
        $statistic = array(
            array(
                "name" => "0~10",
                "sum" => 0
            ),
            array(
                "name" => "11~20",
                "sum" => 0
            ),
            array(
                "name" => "21~30",
                "sum" => 0
            ),
            array(
                "name" => "31~40",
                "sum" => 0
            ),
            array(
                "name" => "41~50",
                "sum" => 0
            ),
            array(
                "name" => "51~60",
                "sum" => 0
            ),
            array(
                "name" => "60以上",
                "sum" => 0
            ),
            array(
                "name" => "未知",
                "sum" => 0
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyAgeInfo(-1));

        //familyId存在
        $statistic = array(
            array(
                "name" => "0~10",
                "sum" => 5
            ),
            array(
                "name" => "11~20",
                "sum" => 0
            ),
            array(
                "name" => "21~30",
                "sum" => 0
            ),
            array(
                "name" => "31~40",
                "sum" => 0
            ),
            array(
                "name" => "41~50",
                "sum" => 0
            ),
            array(
                "name" => "51~60",
                "sum" => 0
            ),
            array(
                "name" => "60以上",
                "sum" => 0
            ),
            array(
                "name" => "未知",
                "sum" => 0
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyAgeInfo(22));
    }

    /**
     * 测试 获取家族成员的地区分布
     *
     * @cover DB\CDBFamily::getFamilyAreaInfo
     */
    public function testGetFamilyAreaInfo()
    {
        $country = -1;
        $province = -1;
        $city = -1;
        $area = -1;

        //familyId不存在
        $this->assertEquals(array(), $this->familyDB->getFamilyAreaInfo(-1, $country, $province, $city, $area));

        //familyId存在，且：
        //人员在各个国家的分布
        $statistic = array(
            array(
                "country" => 1,
                "sum" => 5
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));

        //国家不存在
        $country = -2;
        $this->assertEquals(array(), $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));

        //人员在某个国家的省份分布
        $country = 1;//dnf
        $statistic = array(
            array(
                "province" => 11,
                "sum" => 5
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));

        //省份不存在
        $province = -2;
        $this->assertEquals(array(), $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));

        //人员在某个国家的某个省的城市分布
        $province = 11;
        $statistic = array(
            array(
                "city" => 222,
                "sum" => 5
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));

        //城市不存在
        $city = -2;
        $this->assertEquals(array(), $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));

        //人员在某个国家的某个省的某个城市的区分布
        $city = 222;
        $statistic = array(
            array(
                "area" => 3333,
                "sum" => 5
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));

        //区不存在
        $area = -2;
        $this->assertEquals(array(), $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));

        //人员在某个国家的某个省的某个城市的某个区的镇分布
        $area = 3333;
        $statistic = array(
            array(
                "town" => 44444,
                "sum" => 5
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyAreaInfo(22, $country, $province, $city, $area));
    }

    /**
     * 测试 获取家族成员的血型分布
     *
     * @cover DB\CDBFamily::getFamilyBloodTypeInfo
     */
    public function testGetFamilyBloodTypeInfo()
    {
        //familyId不存在
        $this->assertEquals(array(
            array(
                "name" => "未知",
                "sum" => 0
            )
        ), $this->familyDB->getFamilyBloodTypeInfo(-1));

        //familyId存在
        $statistic = array(
            array(
                "name" => "B",
                "sum" => 5
            ),
            array(
                "name" => "未知",
                "sum" => 0
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyBloodTypeInfo(22));

    }

    /**
     * 测试 获取家族成员的婚姻状况
     *
     * @cover DB\CDBFamily::getFamilyMaritalInfo
     */
    public function testGetFamilyMaritalInfo()
    {
        //familyId不存在
        $this->assertEquals(array(), $this->familyDB->getFamilyMaritalInfo(-1));

        //familyId存在
        $statistic = array(
            array(
                "name" => "未婚",
                "sum" => 5
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyMaritalInfo(22));
    }

    /**
     * 测试 获取家族成员的辈分分布
     *
     * @cover DB\CDBFamily::getFamilyLevelInfo
     */
    public function testGetFamilyLevelInfo()
    {
        //familyId不存在
        $this->assertEquals(array(), $this->familyDB->getFamilyLevelInfo(-1));

        //familyId存在
        $statistic = array(
            array(
                "familyLevel" => 2,
                "sum" => 5
            )
        );
        $this->assertEquals($statistic, $this->familyDB->getFamilyLevelInfo(22));
    }

    /**
     * 测试 获取家族成员的爱好分布
     *
     * @cover DB\CDBFamily::getFamilyHobbyInfo
     */
    public function testGetFamilyHobbyInfo()
    {
        //familyId不存在
        $this->assertEquals(array(), $this->familyDB->getFamilyHobbyInfo(-1));
        //familyId存在

        $statistic = array(
            array(
                "info" => "aaa",
                "sum" => 2
            ),
            array(
                "info" => "sss",
                "sum" => 1
            )
        );

        $this->assertEquals($statistic, $this->familyDB->getFamilyHobbyInfo(22));
    }
    
    /**
     * @covers DB\CDBFamily::getChatGroupsByFamilyId
     */
    public function testGetChatGroupsByFamilyId()
    {
        $array = [[
            'id' => "1",
            'familyId' => "22",
            'familyName' => "haha",
            'chatGroupId' => "1",
            'chatGroupName' => "11",
            'description' => "description",
            'maxusers' => "100",
            'isPublic' => "1",
            'needPermit' => "1",
            'allowinvites' => "1",
            'owner' => "xll",
            'createBy' => "1",
            'createTime' => "2017-11-11 00:00:00",
            'updateBy' => "1",
            'updateTime' => "2017-11-11 00:00:00"
        ]];
        $this->assertEquals($array, $this->familyDB->getChatGroupsByFamilyId(22));
    }

    /**
     * @covers DB\CDBFamily::getChatGroupByHxId
     */
    public function testGetChatGroupByHxId()
    {
        $array = [
            'id' => "1",
            'familyId' => "22",
            'familyName' => "haha",
            'chatGroupId' => "1",
            'chatGroupName' => "11",
            'description' => "description",
            'maxusers' => "100",
            'isPublic' => "1",
            'needPermit' => "1",
            'allowinvites' => "1",
            'owner' => "xll",
            'createBy' => "1",
            'createTime' => "2017-11-11 00:00:00",
            'updateBy' => "1",
            'updateTime' => "2017-11-11 00:00:00"
        ];
        $this->assertEquals($array, $this->familyDB->getChatGroupByHxId(1));
    }

    /**
     * @covers DB\CDBFamily::getChatGroupBySystemId
     */
    public function testGetChatGroupBySystemId()
    {
        $array = [
            'id' => "1",
            'familyId' => "22",
            'familyName' => "haha",
            'chatGroupId' => "1",
            'chatGroupName' => "11",
            'description' => "description",
            'maxusers' => "100",
            'isPublic' => "1",
            'needPermit' => "1",
            'allowinvites' => "1",
            'owner' => "xll",
            'createBy' => "1",
            'createTime' => "2017-11-11 00:00:00",
            'updateBy' => "1",
            'updateTime' => "2017-11-11 00:00:00"
        ];
        $this->assertEquals($array, $this->familyDB->getChatGroupBySystemId(1));
    }

    /**
     * @covers DB\CDBFamily::deleteChatGroupByHxId
     */
    public function testDeleteChatGroupByHxId()
    {
        $this->assertEquals(1, $this->familyDB->deleteChatGroupByHxId(1));
    }

    /**
     * @covers DB\CDBFamily::deleteChatGroupBySystemId
     */
    public function testDeleteChatGroupBySystemId()
    {
        $this->assertEquals(1, $this->familyDB->deleteChatGroupBySystemId(1));
    }

    /**
     * @covers DB\CDBFamily::getFamilyMinLevelWithUserId
     */
    public function testGetFamilyMinLevelWithUserId()
    {
        $this->assertEquals(2, $this->familyDB->getFamilyMinLevelWithUserId(22));
        $this->assertNull($this->familyDB->getFamilyMinLevelWithUserId(222));
    }

    /**
     * 测试 添加家族合并记录
     *
     * @cover DB\CDBFamily::addFamilyMergeRecord
     */
    public function testAddFamilyMergeRecord()
    {
        $this->assertGreaterThan(0, $this->familyDB->addFamilyMergeRecord(22, 23));
        $this->assertEquals(3, $this->getConnection()->getRowCount('gener_family_merge'), '记录总数不对');
        $this->assertEquals(1, $this->familyDB->addFamilyMergeRecord(2, 22));
    }

    /**
     * 测试获取公开的族群
     *
     * @covers DB\CDBFamily::getOpenFamilys
     */
    public function testGetOpenFamilys()
    {
        $this->assertEquals(1, count($this->familyDB->getOpenFamilys()));
    }

    /**
     * @covers DB\CDBFamily::getExportFamilyPdfPaging
     */
    public function testGetExportFamilyPdfPaging()
    {
        $array = [[
            'id' => "1",
            'familyId' => "22",
            'preview_pdf' => "1",
            'print_pdf' => "1",
            'create_time' => "2018-07-03 11:11:11",
            'create_by' => "1",
            'update_time' => "2018-07-03 11:11:11",
            'update_by' => "1"
        ]];
        $this->assertEquals($array, $this->familyDB->getExportFamilyPdfPaging(22, 1, 1));
    }

    /**
     * @covers DB\CDBFamily::autoUpdateFamilyLevel
     */
    public function testAutoUpdateFamilyLevel()
    {
        $this->assertTrue($this->familyDB->autoUpdateFamilyLevel(22));
        $this->assertFalse($this->familyDB->autoUpdateFamilyLevel(222));
    }

    /**
     * @covers DB\CDBFamily::checkEventIdIsExists
     */
    public function testCheckEventIdIsExists()
    {
        $eventIdsArr = [1, 3];
        $this->assertEquals(2, count($this->familyDB->checkEventIdIsExists($eventIdsArr, 22)));
    }

    /**
     * @covers DB\CDBFamily::checkPhotoIdIsExists
     */
    public function testCheckPhotoIdIsExists()
    {
        $photoIdsArr = [1, 2, 3];
        $array = [
            ['id' => 1],
            ['id' => 3]
        ];
        $this->assertEquals($array, $this->familyDB->checkPhotoIdIsExists($photoIdsArr, 22));
    }

    /**
     * @covers DB\CDBFamily::checkGraveIdIsExists
     */
    public function testCheckGraveIdIsExists()
    {
        $graveIdsArr = [1, 2];
        $array = [
            ['id' => 1],
            ['id' => 2]
        ];
        $this->assertEquals($array, $this->familyDB->checkGraveIdIsExists($graveIdsArr, 22));
    }

    /**
     * @covers DB\CDBFamily::getPersonInnerBranch
     */
    public function testGetPersonInnerBranch()
    {
        $this->assertEquals(1, count($this->familyDB->getPersonInnerBranch(1)));

        $this->assertEquals(0, count($this->familyDB->getPersonInnerBranch(2)));
    }

    /**
     * @covers DB\CDBFamily::getAdminInnerBranch
     */
    public function testGetAdminInnerBranch()
    {
        $this->assertEquals(1, count($this->familyDB->getAdminInnerBranch(1)));
        $this->assertEquals(0, count($this->familyDB->getAdminInnerBranch(2)));
    }

    /**
     * @covers DB\CDBFamily::isAdminForPersonInBranch
     */
    public function testIsAdminForPersonInBranch()
    {
        $this->assertTrue($this->familyDB->isAdminForPersonInBranch(1, 1));
        $this->assertFalse($this->familyDB->isAdminForPersonInBranch(2, 1));
    }

    /**
     * @covers DB\CDBFamily::addInnerBranchAdmin
     */
    public function testAddInnerBranchAdmin()
    {
        $this->assertGreaterThan(0, $this->familyDB->addInnerBranchAdmin(1, 1, 1));
    }

    /**
     * @covers DB\CDBFamily::isAdminForBranch
     */
    public function testIsAdminForBranch()
    {
        $this->assertTrue($this->familyDB->isAdminForBranch(1, 1));
        $this->assertFalse($this->familyDB->isAdminForBranch(2,1));
    }

    /**
     * @covers DB\CDBFamily::rescindInnerBranchAdmin
     */
    public function testRescindInnerBranchAdmin()
    {
        $this->assertEquals(1, $this->familyDB->rescindInnerBranchAdmin(1, 1, 1));
        $this->assertEquals(0, $this->familyDB->rescindInnerBranchAdmin(2, 1, 1));
    }

    /**
     * @covers DB\CDBFamily::addFamilySnapshot
     */
    public function testAddFamilySnapshot() {
        $this->assertGreaterThan(0, $this->familyDB->addFamilySnapshot(22, 'ccc', 2));
    }

    /**
     * @covers DB\CDBFamily::checkGroupType
     */
    public function testCheckGroupType() 
    {
        $this->assertTrue($this->familyDB->checkGroupType(22, 1));
        $this->assertFalse($this->familyDB->checkGroupType(99, 1));
        $this->assertFalse($this->familyDB->checkGroupType(22, 2));
    }

    /**
     * @covers DB\CDBFamily::applyBindMiniFamilyToFamily
     */
    public function testApplyBindMiniFamilyToFamily() 
    {
        $this->assertGreaterThan(0, $this->familyDB->applyBindMiniFamilyToFamily(1,1,1,1,'1',1));
    }

    /**
     * @covers DB\CDBFamily::getPersonDirectlyRelatedFamilyMembers
     */
    public function testGetPersonDirectlyRelatedFamilyMembers()
    {
        $this->assertGreaterThan(0, $this->familyDB->getPersonDirectlyRelatedFamilyMembers(22,1));
    }

    /**
     * @covers DB\CDBFamily::recommendPersonBindByPersonId
     */
    public function testRecommendPersonBindByPersonId() {
        list($pair, $allPersonMap) = $this->familyDB->recommendPersonBindByPersonId(25, 100, 108);

        $this->assertEquals(17, count($pair));
        $this->assertEquals(34, count($allPersonMap));
    }

    /**
     * @covers DB\CDBFamily::addMiniFamilyBindRecommend
     * @covers DB\CDBFamily::getMiniFamilyBindRecommend
     */
    public function testAddAndGetMiniFamilyBindRecommend() {
        $miniFamilyBindRecommend = new MiniFamilyBindRecommend();
        $miniFamilyBindRecommend->applyId = 1;
        $miniFamilyBindRecommend->miniFamilyId = 1;
        $miniFamilyBindRecommend->familyId = 2;
        $miniFamilyBindRecommend->familyPersonId = 1;
        $miniFamilyBindRecommend->miniFamilyPersonId = 2;
        $miniFamilyBindRecommend->recommendData = 'dasdasdas';
        $miniFamilyBindRecommend->recommendType = 1;
        $miniFamilyBindRecommend->createBy = 1;
        $miniFamilyBindRecommend->updateBy = 1;

        $id = $this->familyDB->addMiniFamilyBindRecommend($miniFamilyBindRecommend);

        $this->assertGreaterThan(0, $id);

        $this->assertGreaterThan(0, $this->familyDB->getMiniFamilyBindRecommend($id)->id);
    }

    /**
     * @covers DB\CDBFamily::addMiniFamilyBindRecord
     */
    public function testAddMiniFamilyBindRecord() {
        list($pair, $allPersonMap) = $this->familyDB->recommendPersonBindByPersonId(25, 100, 108);

        $GLOBALS['userId'] = 1;
        $GLOBALS['GENDER_FEMALE'] = 0;
        $GLOBALS['GENDER_MALE'] = 1;

        $recommendData['pair'] = $pair;
        $recommendData['persons'] = $allPersonMap;

        $this->assertTrue($this->familyDB->addMiniFamilyBindRecord(24, 25, 100, 108, $recommendData, 1, 1));
    }

    /**
     * @covers DB\CDBFamily::getBindRootRecordById
     */
    public function testGetBindRootRecordById() {
        $this->assertGreaterThan(0, $this->familyDB->getBindRootRecordById(1)['id']);
    }

    /**
     * @covers DB\CDBFamily::getAllFamilyBindRecordsPaging
     */
    public function testGetAllFamilyBindRecordsPaging() {
        $this->assertGreaterThan(0, count($this->familyDB->getAllFamilyBindRecordsPaging(24, 0, 0)));
    }

    /**
     * @covers DB\CDBFamily::getAllMiniFamilyBindRecords
     */
    public function testGetAllMiniFamilyBindRecords() {
        $this->assertGreaterThan(0, count($this->familyDB->getAllMiniFamilyBindRecords(25)));
    }

    /**
     * @covers DB\CDBFamily::getRecommendUpdateInfoFromMiniFamilyToFamily
     */
    public function testGetRecommendUpdateInfoFromMiniFamilyToFamily() {

        list($pair, $allPersonMap) = $this->familyDB->recommendPersonBindByPersonId(25, 100, 108);

        $GLOBALS['userId'] = 1;
        $GLOBALS['GENDER_FEMALE'] = 0; 
        $GLOBALS['GENDER_MALE'] = 1;

        $recommendData['pair'] = $pair;
        $recommendData['persons'] = $allPersonMap;

        $this->familyDB->addMiniFamilyBindRecord(24, 25, 100, 108, $recommendData, 1, 1);

        // 修改部分人物的资料
        $personDB = new CDBPerson();
        $personDB->editPerson(112, 'dada', 'dada', 'dada', '0', '2010-09-09', '0', 'shanghai', '0', 'minghang', '0', 'ddd',
        '0', 'dada', '0', 'dd', 'addddd', '2019-09-02', '15000895133','zpppp', '1');

        // 这里会出现false
        $recommendData = $this->familyDB->getRecommendUpdateInfoFromMiniFamilyToFamily(25, 24);

        $pair = $recommendData['pair'];
        $persons = $recommendData['persons'];
        $diffs = $recommendData['diff'];

        $this->assertEquals(17, count($pair));
        $this->assertEquals(34, count($persons));

        $diffNum = 0;
        foreach($diffs as $item) {
            if (count($item) > 0) {
                $diffNum++;
            }
        }

        $this->assertEquals(10, $diffNum);
    }

    /**
     * @covers DB\CDBFamily::addMiniFamilyUpdateApply
     */
    public function testAddMiniFamilyUpdateApply() {

        // 修改部分人物的资料
        $personDB = new CDBPerson();
        $personDB->editPerson(112, 'dada', 'dada', 'dada', '0', '2010-09-09', '0', 'shanghai', '0', 'minghang', '0', 'ddd',
        '0', 'dada', '0', 'dd', 'addddd', '2019-09-02', '15000895133','zpppp', '1');

        $recommendData = $this->familyDB->getRecommendUpdateInfoFromMiniFamilyToFamily(25, 24);

        $recommend = new MiniFamilyBindRecommend();
        $recommend->applyId = 0;
        $recommend->familyId = 24;
        $recommend->miniFamilyId = 25;
        $recommend->familyPersonId = 100;
        $recommend->miniFamilyPersonId = 108;
        $recommend->recommendData = json_encode($recommendData);
        $recommend->recommendType = 2;
        $recommend->createBy = 1;
        $recommend->updateBy = 1;

        $recommendId = $this->familyDB->addMiniFamilyBindRecommend($recommend);

        $apply = new MiniFamilyUpdateApply();
        
        $apply->recommendId = $recommendId;
        $apply->remark = 'dasdasd';
        $apply->familyId = 24;
        $apply->miniFamilyId = 25;
        $apply->isAccept = 0;
        $apply->createBy = 1;
        $apply->updateBy = 1;

        $this->assertGreaterThan(0, $this->familyDB->addMiniFamilyUpdateApply($apply));
    }

    /**
     * @covers DB\CDBFamily::updateInfoFromMiniFamilyToFamily
     */
    public function testUpdateInfoFromMiniFamilyToFamily() {
        list($pair, $allPersonMap) = $this->familyDB->recommendPersonBindByPersonId(25, 100, 108);

        $GLOBALS['userId'] = 1;
        $GLOBALS['GENDER_FEMALE'] = 0; 
        $GLOBALS['GENDER_MALE'] = 1;

        $recommendData['pair'] = $pair;
        $recommendData['persons'] = $allPersonMap;

        $this->familyDB->addMiniFamilyBindRecord(24, 25, 100, 108, $recommendData, 1, 1);

        // 修改部分人物的资料
        $personDB = new CDBPerson();
        $personDB->editPerson(112, 'dada', 'dada', 'dada', '0', '2010-09-09', '0', 'shanghai', '0', 'minghang', '0', 'ddd',
        '0', 'dada', '0', 'dd', 'addddd', '2019-09-02', '15000895133','zpppp', '1');

        $recommendData = $this->familyDB->getRecommendUpdateInfoFromMiniFamilyToFamily(25, 24);

        $recommend = new MiniFamilyBindRecommend();
        $recommend->applyId = 0;
        $recommend->familyId = 24;
        $recommend->miniFamilyId = 25;
        $recommend->familyPersonId = 100;
        $recommend->miniFamilyPersonId = 108;
        $recommend->recommendData = json_encode($recommendData);
        $recommend->recommendType = 2;
        $recommend->createBy = 1;
        $recommend->updateBy = 1;

        $recommendId = $this->familyDB->addMiniFamilyBindRecommend($recommend);

        $apply = new MiniFamilyUpdateApply();
        
        $apply->recommendId = $recommendId;
        $apply->remark = 'dasdasd';
        $apply->familyId = 24;
        $apply->miniFamilyId = 25;
        $apply->isAccept = 0;
        $apply->createBy = 1;
        $apply->updateBy = 1;

        $applyId = $this->familyDB->addMiniFamilyUpdateApply($apply);

        $miniFamilyBindRecommend = $this->familyDB->getMiniFamilyBindRecommend($recommendId);

        $this->assertTrue($this->familyDB->updateInfoFromMiniFamilyToFamily($applyId, $miniFamilyBindRecommend, 1));
    }

    /**
     * @covers DB\CDBFamily::getMiniFamilyUpdateApply
     */
    public function testGetMiniFamilyUpdateApply() {
        $this->assertGreaterThan(0, $this->familyDB->getMiniFamilyUpdateApply(1)->id);
    }

    /**
     * @covers DB\CDBFamily::refuseBindMiniFamilyApply
     */
    public function testRefuseBindMiniFamilyApply() {
        $this->assertGreaterThan(0, $this->familyDB->refuseBindMiniFamilyApply(1));
    }

    /**
     * @covers DB\CDBFamily::refuseUpdateMiniFamilyApply
     */
    public function testRefuseUpdateMiniFamilyApply() {
        $this->assertGreaterThan(0, $this->familyDB->refuseUpdateMiniFamilyApply(1));
    }

    /**
     * @covers DB\CDBFamily::deleteBranch
     */
    public function testDeleteBranch() {
        $personId = 203;
        $familyId = 26;
        $userId = 1;
        $this->assertGreaterThan(0, $this->familyDB->deleteBranch($personId, $familyId, $userId));
        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersons($familyId);

        $personMap = [];
        foreach($persons as $person) {
            $personMap[$person['id']] = true;
        }


        $this->assertTrue(isset($personMap['200']));
        $this->assertTrue(isset($personMap['201']));
        $this->assertTrue(isset($personMap['202']));
        $this->assertTrue(isset($personMap['204']));
        $this->assertTrue(isset($personMap['205']));
        $this->assertFalse(isset($personMap['203']));
        $this->assertFalse(isset($personMap['206']));


        $personId = 200;
        $this->assertGreaterThan(0, $this->familyDB->deleteBranch($personId, $familyId, $userId));
        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersons($familyId);

        $personMap = [];
        foreach($persons as $person) {
            $personMap[$person['id']] = true;
        }


        $this->assertFalse(isset($personMap['200']));
        $this->assertFalse(isset($personMap['201']));
        $this->assertFalse(isset($personMap['202']));
        $this->assertFalse(isset($personMap['204']));
        $this->assertFalse(isset($personMap['205']));
        $this->assertFalse(isset($personMap['203']));
        $this->assertFalse(isset($personMap['206']));

    }

    /**
     * @covers DB\CDBFamily::verifyPersonIdAndFamilyId
     */
    public function testVerifyPersonIdAndFamilyId() {
        $personId = 200;
        $familyId = 26;
        $this->assertGreaterThan(0, $this->familyDB->verifyPersonIdAndFamilyId($personId, $familyId));
    }

    /**
     * @covers DB\CDBFamily::reSetBranch
     */
    public function testReSetBranch() {
        $personId = 203;
        $familyId = 26;
        $userId = 1;
        $this->assertGreaterThan(0, $this->familyDB->deleteBranch($personId, $familyId, $userId));

        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersons($familyId);

        $personMap = [];
        foreach($persons as $person) {
            $personMap[$person['id']] = true;
        }
        $this->assertTrue(isset($personMap['200']));
        $this->assertTrue(isset($personMap['201']));
        $this->assertTrue(isset($personMap['202']));
        $this->assertTrue(isset($personMap['204']));
        $this->assertTrue(isset($personMap['205']));
        $this->assertFalse(isset($personMap['203']));
        $this->assertFalse(isset($personMap['206']));

        $logs = $this->familyDB->readBranchLogs($familyId);

        $checkres = $this->checkRelations($logs[0]['id'], true);
        $expect = [
            'self-father'  => true,
            'self-mother'  => true,
            'self-brother' => true,
            'self-sister'  => true,
            'father-self'  => true,
            'mother-self'  => true,
            'brother-self' => true,
            'sister-self'  => true,
        ];
        $this->assertEquals($expect, $checkres);


        $relation = '';// father/mother personId
        $this->assertGreaterThan(0, $this->familyDB->reSetBranch($logs[0]['id'], $userId, $relation));

        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersons($familyId);


        $personMap = [];
        foreach($persons as $person) {
            $personMap[$person['id']] = true;
        }
        //var_dump($personMap);die;

        $this->assertTrue(isset($personMap['200']));
        $this->assertTrue(isset($personMap['201']));
        $this->assertTrue(isset($personMap['202']));
        $this->assertTrue(isset($personMap['204']));
        $this->assertTrue(isset($personMap['205']));
        $this->assertTrue(isset($personMap['203']));
        $this->assertTrue(isset($personMap['206']));

        $checkres = $this->checkRelations($logs[0]['id'], false);
        $this->assertEquals($expect, $checkres);

    }

    /**
     * @covers DB\CDBFamily::readBranchLogs
     */
    public function testReadBranchLogs() {
        $familyId = 26;
        $this->assertGreaterThan(0, $this->familyDB->readBranchLogs($familyId));
        var_dump(json_encode($this->familyDB->readBranchLogs($familyId)));
    }

    /**
     * 检查关系树是否正确
     */
    public function checkRelations($logId, $is_deleted = true, $relations = ''){
        $this->init();
        $personDB = new CDBPerson();
        $recordTableName = 'gener_family_branch_log';
        $sql = "SELECT personId, relations FROM $recordTableName WHERE id = '$logId'";
        $res = $this->pdoS->uniqueResult($sql);
        $persons = json_decode($res['relations'], true);
        //组装
        $self = '@'.$res['personId'].'|';
        $father  = Util::arrayToRelation($persons['father']);
        $mother  = Util::arrayToRelation($persons['mother']);
        $brother = Util::arrayToRelation($persons['brother']);
        $sister  = Util::arrayToRelation($persons['sister']);
        $returnArray = [
            'self-father'  => true,
            'self-mother'  => true,
            'self-brother' => true,
            'self-sister'  => true,
            'father-self'  => true,
            'mother-self'  => true,
            'brother-self' => true,
            'sister-self'  => true,
        ];
        //self-people
        $sql = "SELECT father, mother, brother, sister FROM $this->TABLE_PERSON WHERE id = ".$res['personId'] ;
        $selfres = $this->pdoS->uniqueResult($sql);
        $returnArray['self-father'] = $this->checkStrToStr($selfres['father'], $father, $is_deleted);
        $returnArray['self-mother'] = $this->checkStrToStr($selfres['mother'], $mother, $is_deleted);
        if($brother!= $self){
            //不是只有自己
            $returnArray['self-brother'] = $this->checkStrToStr($selfres['brother'], $brother, $is_deleted);
        }
        if($sister!= $self){
            $returnArray['self-sister'] = $this->checkStrToStr($selfres['sister'], $sister, $is_deleted);
        }
        //people-self
        if($persons['gender'] == 1){//男
            $pkey = 'son';
            $bkey = 'brother';
        }else{
            $pkey = 'daughter';
            $bkey = 'sister';
        }
        //parents
        $parents = array_merge($persons['father'], $persons['mother']);
        if(!empty($parents)){
            foreach($parents as $key => $val){
                $sql = " SELECT $pkey FROM $this->TABLE_PERSON WHERE id = '$val' " ;
                $parentsRes = $this->pdoS->uniqueResult($sql);

                if(!$this->checkStrToStr($parentsRes[$pkey], $self, $is_deleted)){
                    $returnArray['father-self'] = false;
                    $returnArray['mother-self'] = false;
                    break;
                }
            }
        }
        //brothers
        $brothers = array_merge($persons['brother'], $persons['sister']);
        if(!empty($brothers)){
            foreach($brothers as $key => $val){
                if($val == $res['personId']){
                    //如果是自己，跳出判断（即为true）
                    continue;
                }
                $sql = " SELECT $bkey FROM $this->TABLE_PERSON WHERE id = '$val' " ;
                $brotherRes = $this->pdoS->uniqueResult($sql);
                if(!$this->checkStrToStr($brotherRes[$bkey], $self, $is_deleted)){
                    $returnArray['brother-self'] = false;
                    $returnArray['sister-self'] = false;
                    break;
                }
            }
        }
        return $returnArray;
    }

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdoS) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdoS = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdoS)
                    $GLOBALS['pdo'] = $this->pdoS;
            } else {
                $this->pdoS = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 判断是否存在 空 true;非空 对比是否在数组内 不在 false 在 true
     * @param lStr
     * @param str
     * @param is_deleted 是删除后 true 还是还原后 false
     * @return bool 在(或为空) true; 不在 false
     * 
     */
    private function checkStrToStr($lStr, $str, $is_deleted = true){
        if($is_deleted == true){
            if(empty($lStr)){
                return true;
            }else{
                if(empty($str)){
                    return false;
                }else{
                    return (strpos($lStr, $str) === false);
                }
            }
        }else{
            if(empty($lStr)){
                return empty($str);
            }else{
                if(empty($str)){
                    return false;
                }else{
                    return !(strpos($lStr, $str) === false);
                }
            }
        }
    }

    /**
     * @covers DB\CDBFamily::copyFamilyBrandByPerson
     */
    public function testCopyFamilyBrandByPerson() {
        $oldPersonId = 202;
        $newFamilyId = 26;
        $newParentId = 206;
        $optionUser = 1;
        $GLOBALS['userId'] = $optionUser;
        $isSetUserId = 0;
        $this->familyDB->copyFamilyBranchByPerson(26, $oldPersonId, $newFamilyId, $newParentId, $optionUser, $isSetUserId);
       
        $personDB = new CDBPerson();
        $person = $personDB->getPersonById(206);
        var_dump($person->son);
        $son = $personDB->getPersonById($person->son[0]);
        var_dump($son->spouse);
        var_dump($son->son);

        $this->assertTrue(isset($person->son));
        $this->assertTrue(isset($son->spouse));
        $this->assertTrue(isset($son->son));
    }


    /**
     * @covers DB\CDBFamily::updateFamilyStartLevel
     */
    public function testUpdateFamilyStartLevel() {
        $FamilyId = 26;
        $optionUser = 1;
        $startLevel = 10;
        $res = $this->familyDB->updateFamilyStartLevel($FamilyId, $optionUser, $startLevel);

        $this->assertTrue($res['error_code']);
        $this->assertEquals($startLevel, $res['error_msg']);
    }
}
