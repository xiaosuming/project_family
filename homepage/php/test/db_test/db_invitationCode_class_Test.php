<?php

use DB\CDBInvitationCode;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-20
 * Time: 下午6:02
 */
class DBInvitationCodeTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $invitationCodeDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {

            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->invitationCodeDB = new CDBInvitationCode();
        return $this->createMySQLXMLDataSet('db_test/xml/invitationCode_dataset.xml');     //默认的数据集
    }

    /**
     * @covers  DB\CDBInvitationCode::getNewInvitationCode()
     */
    public function testGetNewInvitationCode()
    {
        $count = $this->getConnection()->getRowCount('gener_invitation_code');
        $this->assertEquals(1, $count);
        $this->assertEquals(6, strlen($this->invitationCodeDB->getNewInvitationCode()));
        $this->assertEquals(2, $count + 1);
    }

    /**
     * @covers DB\CDBInvitationCode::checkInvitationCode()
     */
    public function testCheckInvitationCode()
    {
        $this->assertTrue(true, $this->invitationCodeDB->checkInvitationCode('opop'));
        $this->assertFalse($this->invitationCodeDB->checkInvitationCode('opopee'));

    }

    /**
     * @covers DB\CDBInvitationCode::bindUserIdToInvitationCode
     */
    public function testBindUserIdToInvitationCode(){
        $this->assertEquals(1,$this->invitationCodeDB->bindUserIdToInvitationCode(1,'opop'));
    }
    /**
     * @covers  DB\CDBInvitationCode::getInvitationCodeTotal()
     */
    public function testGetInvitationCodeTotal()
    {
        $this->assertEquals(1, $this->invitationCodeDB->getInvitationCodeTotal(1));
    }

    /**
     * @covers  DB\CDBInvitationCode::getInvitationCodePaging()
     */
    public function testGetInvitationCodePaging()
    {
        $array = [[
            'id' => "1",
            'invitation_code' => "opop",
            'userId' => "1",
            'create_time' => "2018-06-20 12:00:00",
            'update_time' => "2018-06-20 12:00:00"
        ]];
        $this->assertEquals($array,$this->invitationCodeDB->getInvitationCodePaging(1, 1, 1));
    }

    /**
     * @covers  DB\CDBInvitationCode::checkOpenidHasCode()
     */
    public function testCheckOpenidHasCode(){
        $this->assertEquals('opop',$this->invitationCodeDB->checkOpenidHasCode('openid'));
        $this->assertFalse($this->invitationCodeDB->checkOpenidHasCode('openidsss'));
    }

    /**
     * @covers DB\CDBInvitationCode::bindOpenidWithCode()
     */
    public function testBindOpenidWithCode(){
        $this->assertEquals(6,strlen($this->invitationCodeDB->bindOpenidWithCode('openid1')));
    }
}
