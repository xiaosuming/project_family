<?php

use Model\InfoCard;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use DB\CDBPerson;
use DB\UserFollowDB;
use Model\Person;
use Util\Util;

class DBPersonTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $personDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->personDB = new CDBPerson();
        return $this->createMySQLXMLDataSet('db_test/xml/person_dataset.xml');     //默认的数据集
    }

    /**
     * @covers \DB\CDBPerson::verifyUserIdAndFamilyId
     */
    public function testVerifyUserIdAndFamilyId()
    {
        $this->assertTrue($this->personDB->verifyUserIdAndFamilyId(1, 1));
        $this->assertFalse($this->personDB->verifyUserIdAndFamilyId(11, 11));
    }

    /**
     * 测试用户id和人物id之间的权限验证
     * @covers DB\CDBPerson::verifyUserIdAndPersonId
     */
    public function testVerifyUserIdAndPersonId()
    {
        $this->assertFalse($this->personDB->verifyUserIdAndPersonId(1, 8));
        $this->assertTrue($this->personDB->verifyUserIdAndPersonId(1, 1));
        $this->assertTrue($this->personDB->verifyUserIdAndPersonId(2, 2));
        $this->assertTrue($this->personDB->verifyUserIdAndPersonId(2, 3));
    }

    /**
     * @covers \DB\CDBPerson::getPersonId
     */
    public function testGetPersonId()
    {
        $this->assertEquals(1, $this->personDB->getPersonId(1, 1));
    }

    /**
     * @covers \DB\CDBPerson::getUserIdByPersonId
     */
    public function testGetUserIdByPersonId()
    {
        $this->assertEquals(1, $this->personDB->getUserIdByPersonId(1));
    }

    /**
     * @covers \DB\CDBPerson::addPersonWithNoTransaction
     */
    public function testAddPersonWithNoTransaction()
    {
        $userId = 1;
        $familyId = 1;
        $name = 'xll';
        $gender = 1;
        $address = 'address';
        $birthday = '1988-10-09';
        $photo = 'photo_url';
        $GLOBALS['userId'] = 1;
        $GLOBALS['GENDER_FEMALE'] = 0;
        $GLOBALS['GENDER_MALE'] = 1;
        $this->assertGreaterThan(0, $this->personDB->addPersonWithNoTransaction($userId, $familyId, $name, $gender, $address, $birthday, $photo));
        $this->assertGreaterThan(0, $this->personDB->addPersonWithNoTransaction($userId, $familyId, $name, 0, $address, NULL, $photo));
    }

    /**
     * @covers \DB\CDBPerson::addPerson
     */
    public function testAddPerson()
    {
        $userId = 1;
        $familyId = 1;
        $name = 'xll';
        $gender = 1;
        $address = 'address';
        $birthday = '1988-10-09';
        $photo = 'photo_url';
        $GLOBALS['userId'] = 1;
        $GLOBALS['GENDER_FEMALE'] = 0;
        $GLOBALS['GENDER_MALE'] = 1;
        $this->assertGreaterThan(0, $this->personDB->addPerson($userId, $familyId, $name, $gender, $address, $birthday, $photo));
        $this->assertGreaterThan(0, $this->personDB->addPerson($userId, $familyId, $name, 0, $address, $birthday, $photo));
    }

    /**
     * @covers \DB\CDBPerson::editPerson
     */
    public function testEditPerson()
    {
        $personId = 1;
        $zi = 'zidd';
        $remark = 'remark';
        $name = 'jackie';
        $isDead = 0;
        $deadTime = null;
        $country = 1;
        $countryName = 'china';
        $province = 1;
        $provinceName = 'shanghai';
        $city = 1;
        $cityName = 'shanghai';
        $area = 1;
        $areaName = 'pudong';
        $town = 1;
        $townName = 'sanlin';
        $address = 'dddd';
        $birthday = '1988-09-09';
        $phone = '12378788478';
        $zpname = 'zpname';
        $ranking = '1';
        $this->assertEquals(1, $this->personDB->editPerson($personId, $zi, $remark, $name, $isDead, $deadTime, $country, $countryName, $province, $provinceName, $city, $cityName, $area, $areaName, $town, $townName, $address, $birthday, $phone, $zpname, $ranking));
        $this->assertEquals(1, $this->personDB->editPerson($personId, $zi, $remark, $name, $isDead, '2130-01-01', $country, $countryName, $province, $provinceName, $city, $cityName, $area, $areaName, $town, $townName, $address, NULL, $phone, $zpname, $ranking));
    }

    /**
     * @covers \DB\CDBPerson::editPersonDetail
     */
    public function testEditPersonDetail()
    {
        $person = new Person();
        $person->name = 'jackie';
        $person->zi = 'j';
        $person->zpname = 'zpname';
        $person->remark = 'remark';
        $person->isDead = 0;
        $person->address = 'address';
        $person->phone = '12345678901';
        $person->bloodType = 1;
        $person->maritalStatus = 1;
        $person->country = 1;
        $person->countryName = 'china';
        $person->province = 1;
        $person->provinceName = 'shanghai';
        $person->city = 1;
        $person->cityName = 'shanghai';
        $person->area = 1;
        $person->areaName = 'pudong';
        $person->town = 1;
        $person->townName = 'sanlin';
        $person->qq = '12222';
        $person->birthday = NULL;
        $person->deadTime = NULL;
        $this->assertEquals(1, $this->personDB->editPersonDetail($person, 1));
        $person->birthday = '1989-01-11';
        $person->deadTime = '2190-01-11';
        $this->assertEquals(1, $this->personDB->editPersonDetail($person, 1));

    }

    /**
     * @covers \DB\CDBPerson::getPersonById
     */
    public function testGetPersonById()
    {
        $person = new Person();
        $person->id = '1';
        $person->name = 'jiang';
        $person->zpname = '';
        $person->hxUsername = 'hx_username';
        $person->type = '0';
        $person->level = '0';
        $person->address = 'pujinlu';
        $person->birthday = '1995-02-06';
        $person->gender = '1';
        $person->photo = 'http://test.photo';
        $person->phone = '15000895132';
        $person->bloodType = '1';
        $person->maritalStatus = '0';
        $person->qq = '786185931';
        $person->shareUrl = '123456789';
        $person->familyId = '1';
        $person->familyName = 'jiangFamily';
        $person->familyPhoto = 'http://test.photo';
        $person->refFamilyId = array();
        $person->refPersonId = array();
        $person->userId = '1';
        $person->father = array(2);
        $person->mother = array(3);
        $person->brother = array(1);
        $person->sister = array(7);
        $person->spouse = array(6);
        $person->son = array(4);
        $person->daughter = array(5);
        $person->country = '1';
        $person->province = '2';
        $person->city = '2';
        $person->area = '2';
        $person->town = '2';
        $person->relation = '';
        $person->corePersonId = '';
        $person->branchId = '1';
        $person->countryName = 'China';
        $person->provinceName = 'shanghai';
        $person->cityName = 'shanghai';
        $person->areaName = 'minhang';
        $person->townName = 'pujiangzhen';
        $person->createBy = '1';
        $person->createTime = '2017-09-09 09:09:09';
        $person->updateBy = '1';
        $person->updateTime = '2017-09-09 09:09:09';
        $person->zi = '';
        $person->remark = '';
        $person->isDead = '0';
        $person->deadTime = NULL;
        $person->samePersonId = '0';
        $person->ranking = '1';
        $person->sideText = '';
        $person->profileText = '';
        $person->groupType = '1';
        $person->deadTime = null;
        $this->assertEquals($person, $this->personDB->getPersonById(1));
    }

    /**
     * @covers \DB\CDBPerson::getPersonsNameByPersonsId
     */
    public function testGetPersonsNameByPersonsId()
    {
        $array = [[
            'id' => "1",
            'name' => "jiang"
        ]];
        $this->assertEquals($array, $this->personDB->getPersonsNameByPersonsId(array(1)));
    }


    /**
     * @covers \DB\CDBPerson::addRelatePerson
     * @covers \DB\CDBPerson::addPersonWithLevel
     * @covers \DB\CDBPerson::updateRecord
     * @covers \DB\CDBPerson::updateSpouseStatus
     *
     */
    public function testAddRelatePersons()
    {
        $person = new Person();
        $person->id = Util::getNextId();
        $person->name = 'jackie';
        $person->zi = 'j';
        $person->zpname = 'zpname';
        $person->remark = 'remark';
        $person->isDead = 0;
        $person->address = 'address';
        $person->phone = '12345678901';
        $person->bloodType = 1;
        $person->maritalStatus = 1;
        $person->country = 1;
        $person->countryName = 'china';
        $person->province = 1;
        $person->provinceName = 'shanghai';
        $person->city = 1;
        $person->cityName = 'shanghai';
        $person->area = 1;
        $person->areaName = 'pudong';
        $person->town = 1;
        $person->townName = 'sanlin';
        $person->qq = '12222';
        $person->branchId = 0;
        $person->familyId = 1;
        $person->photo = 'test_photo';
        $person->relation = 1;
        $person->corePersonId = 1;
        $person->ranking = 1;
        $person->sideText = '侧边栏信息';
        $person->profileText = '详细信息';
        $GLOBALS['userId'] = 1;
        $GLOBALS['GENDER_FEMALE'] = 0;
        $GLOBALS['GENDER_MALE'] = 1;
        $GLOBALS['RELATION_FATHER'] = "1";            //父亲关系
        $GLOBALS['RELATION_MOTHER'] = "2";            //母亲关系
        $GLOBALS['RELATION_BROTHER'] = "3";            //兄弟关系
        $GLOBALS['RELATION_SISTER'] = "4";            //姐妹关系
        $GLOBALS['RELATION_SPOUSE'] = "5";            //配偶关系
        $GLOBALS['RELATION_SON'] = "6";                //儿子关系
        $GLOBALS['RELATION_DAUGHTER'] = "7";            //女儿关系

        $person->birthday = null;
        $person->deadTime = null;
        $person->gender = 1;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person));

        $person->id = Util::getNextId();
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->relation = 6;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person));

        $person->id = Util::getNextId();
        $person->relation = 0;
        $person->corePersonId = 6;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertEquals(-2, $this->personDB->addRelatePerson($person));

        $person->id = Util::getNextId();
        $person->relation = 5;
        $person->corePersonId = 2;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person));

        $person->id = Util::getNextId();
        $person->relation = 2;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person));


        $person->id = Util::getNextId();
        $person->relation = 1;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person));

        $person->id = Util::getNextId();
        $person->relation = 3;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person));

        $person->id = Util::getNextId();
        $person->relation = 4;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person));

        $person->id = Util::getNextId();
        $person->relation = 7;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person));

        $person->id = Util::getNextId();
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->gender = 0;
        $person->relation = 6;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 5;
        $person->corePersonId = 1;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 2;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 1;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 3;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 4;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 7;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 5;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->corePersonId = 1;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 6;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->corePersonId = 5;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 1;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->corePersonId = 5;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 2;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->corePersonId = 5;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 3;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->corePersonId = 5;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 4;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->corePersonId = 5;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 6;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->corePersonId = 5;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

        $person->id = Util::getNextId();
        $person->relation = 7;
        $person->birthday = '1989-01-01';
        $person->deadTime = '2100-11-11';
        $person->corePersonId = 5;
        $this->assertGreaterThan(0, $this->personDB->addRelatePerson($person, false));

    }

    /**
     * @covers DB\CDBPerson::deletePerson
     */
    public function testDeletePerson()
    {
        $this->assertTrue($this->personDB->deletePerson(1));
        $this->assertTrue($this->personDB->deletePerson(2));
        $this->assertTrue($this->personDB->deletePerson(3));
        $this->assertTrue($this->personDB->deletePerson(4));
        $this->assertTrue($this->personDB->deletePerson(5));
    }

    /**
     * @covers DB\CDBPerson::getFamilyPersons
     */
    public function testGetFamilyPersons()
    {
        // $array = [
        //     [
        //         'id' => "2",
        //         'samePersonId' => "0",
        //         'name' => "jiangson",
        //         'zi' => "",
        //         'remark' => null,
        //         'type' => "1",
        //         'countryName' => "China",
        //         'provinceName' => "shanghai",
        //         'cityName' => "shanghai",
        //         'areaName' => "minhang",
        //         'townName' => "pujiangzhen",
        //         'address' => "pujinlu",
        //         'birthday' => "1995-02-06",
        //         'gender' => "1",
        //         'phone' => "15000895133",
        //         'photo' => "http://test.photo",
        //         'shareUrl' => "123456789121",
        //         'isDead' => "0",
        //         'deadTime' => NULL,
        //         'familyId' => "1",
        //         'refFamilyId' => NULL,
        //         'father' => [],
        //         'mother' => [],
        //         'son' => [1],
        //         'daughter' => [7],
        //         'spouse' => [3],
        //         'sister' => [],
        //         'brother' => [2],
        //         'level' => "-1",
        //         'userId' => "2",
        //         'ranking' => '2',
        //         'sideText' => '',
        //         'profileText' => '',
        //         'createBy' => "1",
        //         'createTime' => "2017-09-09 09:09:09",
        //         'updateBy' => "1",
        //         'updateTime' => "2017-09-09 09:09:09",
        //         'username' => "jiangson",
        //         'userPhoto' => "http://test.photo",
        //         'nickname' => "jiangson",
        //         'hxUsername' => "hx_username1",
        //         'isAdoption' => "0"
        //     ]
        //     , [
        //         'id' => "3",
        //         'samePersonId' => "0",
        //         'name' => "jiangdau",
        //         'zi' => "",
        //         'remark' => "",
        //         'type' => "2",
        //         'countryName' => "China",
        //         'provinceName' => "shanghai",
        //         'cityName' => "shanghai",
        //         'areaName' => "minhang",
        //         'townName' => "pujiangzhen",
        //         'address' => "pujinlu",
        //         'birthday' => "1995-02-06",
        //         'gender' => "0",
        //         'phone' => "15000895134",
        //         'photo' => "http://test.photo",
        //         'shareUrl' => "1234567891213213123123132",
        //         'isDead' => "0",
        //         'deadTime' => NULL,
        //         'familyId' => "1",
        //         'refFamilyId' => NULL,
        //         'father' => [],
        //         'mother' => [],
        //         'son' => [1],
        //         'daughter' => [7],
        //         'spouse' => [2],
        //         'sister' => [3],
        //         'brother' => [],
        //         'level' => "-1",
        //         'userId' => "0",
        //         'ranking' => '1',
        //         'sideText' => '',
        //         'profileText' => '',
        //         'createBy' => "2",
        //         'createTime' => "2017-09-09 09:09:09",
        //         'updateBy' => "2",
        //         'updateTime' => "2017-09-09 09:09:09",
        //         'username' => NULL,
        //         'userPhoto' => NULL,
        //         'nickname' => NULL,
        //         'hxUsername' => NULL,
        //         'isAdoption' => "0"
        //     ],
        //     [
        //         'id' => "1",
        //         'samePersonId' => "0",
        //         'name' => "jiang",
        //         'zi' => "",
        //         'remark' => "",
        //         'type' => "0",
        //         'countryName' => "China",
        //         'provinceName' => "shanghai",
        //         'cityName' => "shanghai",
        //         'areaName' => "minhang",
        //         'townName' => "pujiangzhen",
        //         'address' => "pujinlu",
        //         'birthday' => "1995-02-06",
        //         'gender' => "1",
        //         'phone' => "15000895132",
        //         'photo' => "http://test.photo",
        //         'shareUrl' => "123456789",
        //         'isDead' => "0",
        //         'deadTime' => NULL,
        //         'familyId' => "1",
        //         'refFamilyId' => NULL,
        //         'father' => [2],
        //         'mother' => [3],
        //         'son' => [4],
        //         'daughter' => [5],
        //         'spouse' => [6],
        //         'sister' => [7],
        //         'brother' => [1],
        //         'level' => "0",
        //         'userId' => "1",
        //         'ranking' => '1',
        //         'sideText' => '',
        //         'profileText' => '',
        //         'createBy' => "1",
        //         'createTime' => "2017-09-09 09:09:09",
        //         'updateBy' => "1",
        //         'updateTime' => "2017-09-09 09:09:09",
        //         'username' => "jiang",
        //         'userPhoto' => "http://test.photo",
        //         'nickname' => "jiang111",
        //         'hxUsername' => "hx_username",
        //         'isAdoption' => "0"
        //     ],
        //     [
        //         'id' => "6",
        //         'samePersonId' => "0",
        //         'name' => "spouse6",
        //         'zi' => "",
        //         'remark' => "",
        //         'type' => "2",
        //         'countryName' => "China",
        //         'provinceName' => "shanghai",
        //         'cityName' => "shanghai",
        //         'areaName' => "minhang",
        //         'townName' => "pujiangzhen",
        //         'address' => "pujinlu",
        //         'birthday' => "1995-02-06",
        //         'gender' => "0",
        //         'phone' => "15000892363",
        //         'photo' => "http://test.photo",
        //         'shareUrl' => "123456789as121",
        //         'isDead' => "0",
        //         'deadTime' => NULL,
        //         'familyId' => "1",
        //         'refFamilyId' => NULL,
        //         'father' => [],
        //         'mother' => [],
        //         'son' => [4],
        //         'daughter' => [5],
        //         'spouse' => [1],
        //         'sister' => [6],
        //         'brother' => [],
        //         'level' => "0",
        //         'userId' => "0",
        //         'ranking' => "1",
        //         'sideText' => '',
        //         'profileText' => '',
        //         'createBy' => "1",
        //         'createTime' => "2017-09-09 09:09:09",
        //         'updateBy' => "1",
        //         'updateTime' => "2017-09-09 09:09:09",
        //         'username' => null,
        //         'userPhoto' => null,
        //         'nickname' => null,
        //         'hxUsername' => null,
        //         'isAdoption' => "0"
        //     ],
        //     [
        //         'id' => "7",
        //         'samePersonId' => "0",
        //         'name' => "sister7",
        //         'zi' => "",
        //         'remark' => "",
        //         'type' => "1",
        //         'countryName' => "China",
        //         'provinceName' => "shanghai",
        //         'cityName' => "shanghai",
        //         'areaName' => "minhang",
        //         'townName' => "pujiangzhen",
        //         'address' => "pujinlu",
        //         'birthday' => "1995-02-06",
        //         'gender' => "0",
        //         'phone' => "15000892363",
        //         'photo' => "http://test.photo",
        //         'shareUrl' => "123456789as121",
        //         'isDead' => "0",
        //         'deadTime' => NULL,
        //         'familyId' => "1",
        //         'refFamilyId' => NULL,
        //         'father' => [2],
        //         'mother' => [3],
        //         'son' => [],
        //         'daughter' => [],
        //         'spouse' => [],
        //         'sister' => [7],
        //         'brother' => [1],
        //         'level' => "0",
        //         'userId' => "0",
        //         'ranking' => "1",
        //         'sideText' => '',
        //         'profileText' => '',
        //         'createBy' => "1",
        //         'createTime' => "2017-09-09 09:09:09",
        //         'updateBy' => "1",
        //         'updateTime' => "2017-09-09 09:09:09",
        //         'username' => null,
        //         'userPhoto' => null,
        //         'nickname' => null,
        //         'hxUsername' => null,
        //         'isAdoption' => "1"
        //     ],
        //     [
        //         'id' => "4",
        //         'samePersonId' => "0",
        //         'name' => "jiangW",
        //         'zi' => "",
        //         'remark' => "",
        //         'type' => "1",
        //         'countryName' => "China",
        //         'provinceName' => "shanghai",
        //         'cityName' => "shanghai",
        //         'areaName' => "minhang",
        //         'townName' => "pujiangzhen",
        //         'address' => "pujinlu",
        //         'birthday' => "1995-02-06",
        //         'gender' => "1",
        //         'phone' => "15000895136",
        //         'photo' => "http://test.photo",
        //         'shareUrl' => "1234562789",
        //         'isDead' => "0",
        //         'deadTime' => NULL,
        //         'familyId' => "1",
        //         'refFamilyId' => NULL,
        //         'father' => [1],
        //         'mother' => [6],
        //         'son' => [],
        //         'daughter' => [],
        //         'spouse' => [],
        //         'sister' => [5],
        //         'brother' => [4],
        //         'level' => "1",
        //         'userId' => "3",
        //         'ranking' => '1',
        //         'sideText' => '',
        //         'profileText' => '',
        //         'createBy' => "1",
        //         'createTime' => "2017-09-09 09:09:09",
        //         'updateBy' => "1",
        //         'updateTime' => "2017-09-09 09:09:09",
        //         'username' => "jiangspouse",
        //         'userPhoto' => "http://test.photo",
        //         'nickname' => "jiangspouse",
        //         'hxUsername' => "hx_username12",
        //         'isAdoption' => "0"
        //     ],
        //     [
        //         'id' => "5",
        //         'samePersonId' => "0",
        //         'name' => "xll",
        //         'zi' => "",
        //         'remark' => "",
        //         'type' => "1",
        //         'countryName' => "China",
        //         'provinceName' => "shanghai",
        //         'cityName' => "shanghai",
        //         'areaName' => "minhang",
        //         'townName' => "pujiangzhen",
        //         'address' => "pujinlu",
        //         'birthday' => "1995-02-06",
        //         'gender' => "0",
        //         'phone' => "15000895123",
        //         'photo' => "http://test.photo",
        //         'shareUrl' => "123456789as121",
        //         'isDead' => "0",
        //         'deadTime' => NULL,
        //         'familyId' => "1",
        //         'refFamilyId' => NULL,
        //         'father' => [1],
        //         'mother' => [6],
        //         'son' => [],
        //         'daughter' => [],
        //         'spouse' => [],
        //         'sister' => [5],
        //         'brother' => [4],
        //         'level' => "1",
        //         'userId' => "1",
        //         'ranking' => "1",
        //         'createBy' => "1",
        //         'sideText' => '',
        //         'profileText' => '',
        //         'createTime' => "2017-09-09 09:09:09",
        //         'updateBy' => "1",
        //         'updateTime' => "2017-09-09 09:09:09",
        //         'username' => "jiang",
        //         'userPhoto' => "http://test.photo",
        //         'nickname' => "jiang111",
        //         'hxUsername' => "hx_username",
        //         'isAdoption' => "0"
        //     ]

        // ];
        $this->assertGreaterThan(1, count($this->personDB->getFamilyPersons(1)));
        $this->assertEquals(array(), $this->personDB->getFamilyPersons(89, 1));
    }

    /**
     * @covers DB\CDBPerson::getFamilyPersonsByMerge
     */
    public function testGetFamilyPersonsByMerge()
    {
        $array = [
            [
                'id' => "2",
                'samePersonId' => "0",
                'name' => "jiangson",
                'zi' => "",
                'remark' => "",
                'type' => "1",
                'gender' => "1",
                'photo' => "http://test.photo",
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [],
                'mother' => [],
                'son' => [1],
                'daughter' => [7],
                'spouse' => [3],
                'sister' => [],
                'brother' => [2],
                'level' => "-1",
                'userId' => "2",
                'ranking' => '2',
                'isAdoption' => 0
            ]
            , [
                'id' => "3",
                'samePersonId' => "0",
                'name' => "jiangdau",
                'zi' => "",
                'remark' => "",
                'type' => "2",
                'gender' => "0",
                'photo' => "http://test.photo",
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [],
                'mother' => [],
                'son' => [1],
                'daughter' => [7],
                'spouse' => [2],
                'sister' => [3],
                'brother' => [],
                'level' => "-1",
                'userId' => "0",
                'ranking' => '1',
                'isAdoption' => 0
            ],
            [
                'id' => "1",
                'samePersonId' => "0",
                'name' => "jiang",
                'zi' => "",
                'remark' => "",
                'type' => "0",
                'gender' => "1",
                'photo' => "http://test.photo",
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [2],
                'mother' => [3],
                'son' => [4],
                'daughter' => [5],
                'spouse' => [6],
                'sister' => [7],
                'brother' => [1],
                'level' => "0",
                'userId' => "1",
                'ranking' => '1',
                'isAdoption' => 0
            ],
            [
                'id' => "6",
                'samePersonId' => "0",
                'name' => "spouse6",
                'zi' => "",
                'remark' => "",
                'type' => "2",
                'gender' => "0",
                'photo' => "http://test.photo",
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [],
                'mother' => [],
                'son' => [4],
                'daughter' => [5],
                'spouse' => [1],
                'sister' => [6],
                'brother' => [],
                'level' => "0",
                'userId' => "0",
                'ranking' => "1",
                'isAdoption' => 0
            ],
            [
                'id' => "7",
                'samePersonId' => "0",
                'name' => "sister7",
                'zi' => "",
                'remark' => "",
                'type' => "1",
                'gender' => "0",
                'photo' => "http://test.photo",
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [2],
                'mother' => [3],
                'son' => [],
                'daughter' => [],
                'spouse' => [],
                'sister' => [7],
                'brother' => [1],
                'level' => "0",
                'userId' => "0",
                'ranking' => "1",
                'isAdoption' => 1
            ],

            [
                'id' => "4",
                'samePersonId' => "0",
                'name' => "jiangW",
                'zi' => "",
                'remark' => "",
                'type' => "1",
                'gender' => "1",
                'photo' => "http://test.photo",
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [1],
                'mother' => [6],
                'son' => [],
                'daughter' => [],
                'spouse' => [],
                'sister' => [5],
                'brother' => [4],
                'level' => "1",
                'userId' => "3",
                'ranking' => '1',
                'isAdoption' => 0
            ],
            [
                'id' => "5",
                'samePersonId' => "0",
                'name' => "xll",
                'zi' => "",
                'remark' => "",
                'type' => "1",
                'gender' => "0",
                'photo' => "http://test.photo",
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [1],
                'mother' => [6],
                'son' => [],
                'daughter' => [],
                'spouse' => [],
                'sister' => [5],
                'brother' => [4],
                'level' => "1",
                'userId' => "1",
                'ranking' => '1',
                'isAdoption' => 0
            ]
        ];
        $this->assertEquals($array, $this->personDB->getFamilyPersonsByMerge(1));
        $this->assertEquals($array, $this->personDB->getFamilyPersonsByMerge(1, 1));
    }

    /**
     * @covers DB\CDBPerson::getFamilyPersonsByBranchId
     */
    public function testGetFamilyPersonsByBranchId()
    {
        $array = [[
            'id' => "1",
            'samePersonId' => "0",
            'name' => "jiang",
            'zi' => "",
            'remark' => "",
            'address' => "pujinlu",
            'type' => "0",
            'birthday' => "1995-02-06",
            'gender' => "1",
            'phone' => "15000895132",
            'photo' => "http://test.photo",
            'shareUrl' => "123456789",
            'isDead' => "0",
            'deadTime' => NULL,
            'familyId' => "1",
            'refFamilyId' => NULL,
            'refPersonId' => NULL,
            'father' => [2],
            'mother' => [3],
            'son' => [4],
            'daughter' => [5],
            'spouse' => [6],
            'sister' => [7],
            'brother' => [1],
            'level' => "0",
            'userId' => "1",
            'ranking' => '1',
            'createBy' => "1",
            'createTime' => "2017-09-09 09:09:09",
            'updateBy' => "1",
            'updateTime' => "2017-09-09 09:09:09",
            'hxUsername' => "hx_username",
            'isAdoption' => 0

        ]];
        $this->assertEquals($array, $this->personDB->getFamilyPersonsByBranchId(1));
    }

    /**
     * @covers DB\CDBPerson::getFamilyPersonList
     */
    public function testGetFamilyPersonList()
    {
        $array = [
            [
                'id' => "2",
                'samePersonId' => "0",
                'name' => "jiangson",
                'photo' => "http://test.photo",
                'userId' => "2",
                'isAdoption' => 0
            ], [
                'id' => "3",
                'samePersonId' => "0",
                'name' => "jiangdau",
                'photo' => "http://test.photo",
                'userId' => "0",
                'isAdoption' => 0
            ],
            [
                'id' => "1",
                'samePersonId' => "0",
                'name' => "jiang",
                'photo' => "http://test.photo",
                'userId' => "1",
                'isAdoption' => 0
            ],
            [
                'id' => "6",
                'samePersonId' => "0",
                'name' => "spouse6",
                'photo' => "http://test.photo",
                'userId' => "0",
                'isAdoption' => 0
            ],
            [
                'id' => "7",
                'samePersonId' => "0",
                'name' => "sister7",
                'photo' => "http://test.photo",
                'userId' => "0",
                'isAdoption' => 1
            ],

            [
                'id' => "4",
                'samePersonId' => "0",
                'name' => "jiangW",
                'photo' => "http://test.photo",
                'userId' => "3",
                'isAdoption' => 0
            ],
            [
                'id' => "5",
                'samePersonId' => "0",
                'name' => "xll",
                'photo' => "http://test.photo",
                'userId' => "1",
                'isAdoption' => 0
            ]

        ];
        $this->assertEquals($array, $this->personDB->getFamilyPersonList(1));
    }

    /**
     * @covers DB\CDBPerson::getFamilyPersonListWithPhone
     */
    public function testGetFamilyPersonListWithPhone()
    {
        $array = [
            [
                'id' => "2",
                'samePersonId' => "0",
                'name' => "jiangson",
                'birthday' => "1995-02-06",
                'gender' => "1",
                'blood_type' => "1",
                'marital_status' => "0",
                'phone' => "15000895133",
                'qq' => "786185932",
                'country' => "1",
                'country_name' => "China",
                'province' => "2",
                'province_name' => "shanghai",
                'city' => "2",
                'city_name' => "shanghai",
                'area' => "2",
                'area_name' => "minhang",
                'town' => "2",
                'town_name' => "pujiangzhen",
                'address' => "pujinlu",
                'ranking' => '2',
                'isAdoption' => 0
            ]
            , [
                'id' => "3",
                'samePersonId' => "0",
                'name' => "jiangdau",
                'birthday' => "1995-02-06",
                'gender' => "0",
                'blood_type' => "1",
                'marital_status' => "0",
                'phone' => "15000895134",
                'qq' => "786185933",
                'country' => "1",
                'country_name' => "China",
                'province' => "2",
                'province_name' => "shanghai",
                'city' => "2",
                'city_name' => "shanghai",
                'area' => "2",
                'area_name' => "minhang",
                'town' => "2",
                'town_name' => "pujiangzhen",
                'address' => "pujinlu",
                'ranking' => '1',
                'isAdoption' => 0
            ],
            [
                'id' => "1",
                'samePersonId' => "0",
                'name' => "jiang",
                'birthday' => "1995-02-06",
                'gender' => "1",
                'blood_type' => "1",
                'marital_status' => "0",
                'phone' => "15000895132",
                'qq' => "786185931",
                'country' => "1",
                'country_name' => "China",
                'province' => "2",
                'province_name' => "shanghai",
                'city' => "2",
                'city_name' => "shanghai",
                'area' => "2",
                'area_name' => "minhang",
                'town' => "2",
                'town_name' => "pujiangzhen",
                'address' => "pujinlu",
                'ranking' => '1',
                'isAdoption' => 0
            ],
            [
                'id' => "6",
                'samePersonId' => "0",
                'name' => "spouse6",
                'birthday' => "1995-02-06",
                'gender' => "0",
                'blood_type' => "1",
                'marital_status' => "1",
                'phone' => "15000892363",
                'qq' => "786185912",
                'country' => "1",
                'country_name' => "China",
                'province' => "2",
                'province_name' => "shanghai",
                'city' => "2",
                'city_name' => "shanghai",
                'area' => "2",
                'area_name' => "minhang",
                'town' => "2",
                'town_name' => "pujiangzhen",
                'address' => "pujinlu",
                'ranking' => "1",
                'isAdoption' => 0
            ],
            [
                'id' => "7",
                'samePersonId' => "0",
                'name' => "sister7",
                'birthday' => "1995-02-06",
                'gender' => "0",
                'blood_type' => "1",
                'marital_status' => "1",
                'phone' => "15000892363",
                'qq' => "786185912",
                'country' => "1",
                'country_name' => "China",
                'province' => "2",
                'province_name' => "shanghai",
                'city' => "2",
                'city_name' => "shanghai",
                'area' => "2",
                'area_name' => "minhang",
                'town' => "2",
                'town_name' => "pujiangzhen",
                'address' => "pujinlu",
                'ranking' => "1",
                'isAdoption' => 1
            ],

            [
                'id' => "4",
                'samePersonId' => "0",
                'name' => "jiangW",
                'birthday' => "1995-02-06",
                'gender' => "1",
                'blood_type' => "1",
                'marital_status' => "1",
                'phone' => "15000895136",
                'qq' => "786185937",
                'country' => "1",
                'country_name' => "China",
                'province' => "2",
                'province_name' => "shanghai",
                'city' => "2",
                'city_name' => "shanghai",
                'area' => "2",
                'area_name' => "minhang",
                'town' => "2",
                'town_name' => "pujiangzhen",
                'address' => "pujinlu",
                'ranking' => '1',
                'isAdoption' => 0
            ],
            [
                'id' => "5",
                'samePersonId' => "0",
                'name' => "xll",
                'birthday' => "1995-02-06",
                'gender' => "0",
                'blood_type' => "1",
                'marital_status' => "1",
                'phone' => "15000895123",
                'qq' => "786185912",
                'country' => "1",
                'country_name' => "China",
                'province' => "2",
                'province_name' => "shanghai",
                'city' => "2",
                'city_name' => "shanghai",
                'area' => "2",
                'area_name' => "minhang",
                'town' => "2",
                'town_name' => "pujiangzhen",
                'address' => "pujinlu",
                'ranking' => '1',
                'isAdoption' => 0
            ]
        ];
        $this->assertEquals($array, $this->personDB->getFamilyPersonListWithPhone(1));
    }

    /**
     * @covers DB\CDBPerson::verifyPersonExistsInFamily
     */
    public function testVerifyPersonExistsInFamily()
    {
        $str = "id = 1";
        $this->assertEquals(1, $this->personDB->verifyPersonExistsInFamily($str, 1));
        $this->assertEquals(0, $this->personDB->verifyPersonExistsInFamily("id = 10", 6));
    }

    /**
     * 测试根据家族id和用户id获取人物
     * @cover DB\CDBPerson::getPersonByFamilyIdAndUserId
     */
    public function testGetPersonByFamilyIdAndUserId()
    {
        $this->assertEquals(1, $this->personDB->getPersonByFamilyIdAndUserId(1, 1)->id);
        $this->assertNull($this->personDB->getPersonByFamilyIdAndUserId(7, 1));
    }

    /**
     * @covers DB\CDBPerson::getFamilyPersonsSubTree
     * @covers DB\CDBPerson::getPersonByFamilyIdAndUserId
     */
    public function testGetFamilyPersonsSubTree()
    {
        $array = [
            [
                'id' => "2",
                'samePersonId' => "0",
                'name' => "jiangson",
                'zi' => "",
                'remark' => "",
                'type' => "1",
                'address' => "pujinlu",
                'birthday' => "1995-02-06",
                'gender' => "1",
                'phone' => "15000895133",
                'photo' => "http://test.photo",
                'shareUrl' => "123456789121",
                'isDead' => "0",
                'deadTime' => NULL,
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [],
                'mother' => [],
                'son' => [1],
                'daughter' => [7],
                'spouse' => [3],
                'sister' => [],
                'brother' => [2],
                'level' => "-1",
                'userId' => "2",
                'ranking' => '2',
                'createBy' => "1",
                'createTime' => "2017-09-09 09:09:09",
                'updateBy' => "1",
                'updateTime' => "2017-09-09 09:09:09",
                'hxUsername' => 'hx_username1',
                'isAdoption' => 0
            ]
            , [
                'id' => "3",
                'samePersonId' => "0",
                'name' => "jiangdau",
                'zi' => "",
                'remark' => "",
                'type' => "2",
                'address' => "pujinlu",
                'birthday' => "1995-02-06",
                'gender' => "0",
                'phone' => "15000895134",
                'photo' => "http://test.photo",
                'shareUrl' => "1234567891213213123123132",
                'isDead' => "0",
                'deadTime' => NULL,
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [],
                'mother' => [],
                'son' => [1],
                'daughter' => [7],
                'spouse' => [2],
                'sister' => [3],
                'brother' => [],
                'level' => "-1",
                'userId' => "0",
                'ranking' => '1',
                'createBy' => "2",
                'createTime' => "2017-09-09 09:09:09",
                'updateBy' => "2",
                'updateTime' => "2017-09-09 09:09:09",
                'hxUsername' => null,
                'isAdoption' => 0
            ],
            [
                'id' => "1",
                'samePersonId' => "0",
                'name' => "jiang",
                'zi' => "",
                'remark' => "",
                'type' => "0",
                'address' => "pujinlu",
                'birthday' => "1995-02-06",
                'gender' => "1",
                'phone' => "15000895132",
                'photo' => "http://test.photo",
                'shareUrl' => "123456789",
                'isDead' => "0",
                'deadTime' => NULL,
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [2],
                'mother' => [3],
                'son' => [4],
                'daughter' => [5],
                'spouse' => [6],
                'sister' => [7],
                'brother' => [1],
                'level' => "0",
                'userId' => "1",
                'ranking' => '1',
                'createBy' => "1",
                'createTime' => "2017-09-09 09:09:09",
                'updateBy' => "1",
                'updateTime' => "2017-09-09 09:09:09",
                'hxUsername' => 'hx_username',
                'isAdoption' => 0
            ],
            [
                'id' => "6",
                'samePersonId' => "0",
                'name' => "spouse6",
                'zi' => "",
                'remark' => "",
                'type' => "2",
                'address' => "pujinlu",
                'birthday' => "1995-02-06",
                'gender' => "0",
                'phone' => "15000892363",
                'photo' => "http://test.photo",
                'shareUrl' => "123456789as121",
                'isDead' => "0",
                'deadTime' => NULL,
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [],
                'mother' => [],
                'son' => [4],
                'daughter' => [5],
                'spouse' => [1],
                'sister' => [6],
                'brother' => [],
                'level' => "0",
                'userId' => "0",
                'ranking' => "1",
                'createBy' => "1",
                'createTime' => "2017-09-09 09:09:09",
                'updateBy' => "1",
                'updateTime' => "2017-09-09 09:09:09",
                'hxUsername' => null,
                'isAdoption' => 0
            ],
            [
                'id' => "7",
                'samePersonId' => "0",
                'name' => "sister7",
                'zi' => "",
                'remark' => "",
                'type' => "1",
                'address' => "pujinlu",
                'birthday' => "1995-02-06",
                'gender' => "0",
                'phone' => "15000892363",
                'photo' => "http://test.photo",
                'shareUrl' => "123456789as121",
                'isDead' => "0",
                'deadTime' => NULL,
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [2],
                'mother' => [3],
                'son' => [],
                'daughter' => [],
                'spouse' => [],
                'sister' => [7],
                'brother' => [1],
                'level' => "0",
                'userId' => "0",
                'ranking' => "1",
                'createBy' => "1",
                'createTime' => "2017-09-09 09:09:09",
                'updateBy' => "1",
                'updateTime' => "2017-09-09 09:09:09",
                'hxUsername' => null,
                'isAdoption' => 1
            ],

            [
                'id' => "4",
                'samePersonId' => "0",
                'name' => "jiangW",
                'zi' => "",
                'remark' => "",
                'type' => "1",
                'address' => "pujinlu",
                'birthday' => "1995-02-06",
                'gender' => "1",
                'phone' => "15000895136",
                'photo' => "http://test.photo",
                'shareUrl' => "1234562789",
                'isDead' => "0",
                'deadTime' => NULL,
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [1],
                'mother' => [6],
                'son' => [],
                'daughter' => [],
                'spouse' => [],
                'sister' => [5],
                'brother' => [4],
                'level' => "1",
                'userId' => "3",
                'ranking' => '1',
                'createBy' => "1",
                'createTime' => "2017-09-09 09:09:09",
                'updateBy' => "1",
                'updateTime' => "2017-09-09 09:09:09",
                'hxUsername' => 'hx_username12',
                'isAdoption' => 0
            ],
            [
                'id' => "5",
                'samePersonId' => "0",
                'name' => "xll",
                'zi' => "",
                'remark' => "",
                'type' => "1",
                'address' => "pujinlu",
                'birthday' => "1995-02-06",
                'gender' => "0",
                'phone' => "15000895123",
                'photo' => "http://test.photo",
                'shareUrl' => "123456789as121",
                'isDead' => "0",
                'deadTime' => NULL,
                'familyId' => "1",
                'refFamilyId' => NULL,
                'father' => [1],
                'mother' => [6],
                'son' => [],
                'daughter' => [],
                'spouse' => [],
                'sister' => [5],
                'brother' => [4],
                'level' => "1",
                'userId' => "1",
                'ranking' => "1",
                'createBy' => "1",
                'createTime' => "2017-09-09 09:09:09",
                'updateBy' => "1",
                'updateTime' => "2017-09-09 09:09:09",
                'hxUsername' => 'hx_username',
                'isAdoption' => 0
            ]
        ];
        $this->assertEquals($array, $this->personDB->getFamilyPersonsSubTree(1, 1));
        $this->assertEquals(array(), $this->personDB->getFamilyPersonsSubTree(6, 1));
    }


    /**
     * 测试根据过滤条件获取家族人物
     * @cover DB\CDBPerson::getFamilyPersonsPagingByFilter
     */
    public function testGetFamilyPersonsPagingByFilter()
    {
        $this->assertEquals(2, count($this->personDB->getFamilyPersonsPagingByFilter(1, 2, 1, true, true)));
        $this->assertEquals(2, count($this->personDB->getFamilyPersonsPagingByFilter(1, 2, 1, false, false)));
        $this->assertEquals(2, count($this->personDB->getFamilyPersonsPagingByFilter(1, 2, 1, false, true)));
        $this->assertEquals(2, count($this->personDB->getFamilyPersonsPagingByFilter(1, 2, 1, true, false)));
    }

    /**
     * 测试根据过滤条件获取人物总数
     * @cover DB\CDBPerson::getFamilyPersonsCountByFilter
     */
    public function testGetFamilyPersonsCountByFilter()
    {
        $this->assertEquals(7, $this->personDB->getFamilyPersonsCountByFilter(1, true, true));
        $this->assertEquals(7, $this->personDB->getFamilyPersonsCountByFilter(1, false, true));
        $this->assertEquals(4, $this->personDB->getFamilyPersonsCountByFilter(1, true, false));
    }

    /**
     * @covers DB\CDBPerson::getFamilyPersonsCount
     */
    public function testGetFamilyPersonsCount()
    {
        $this->assertEquals(0, $this->personDB->getFamilyPersonsCount(1, 'xll', '-1'));
    }

    /**
     * @covers DB\CDBPerson::getFamilyPersonsPaging
     */
    public function testGetFamilyPersonsPaging()
    {
        $familyId = 1;
        $pageIndex = 1;
        $pageSize = 10;
        $name = 'xll';
        $level = '1';
        $result = $this->personDB->getFamilyPersonsPaging($familyId, $pageIndex, $pageSize, $name, $level);
        $this->assertEquals(5, $result['0']['id']);
    }

    /**
     * @covers DB\CDBPerson::addPhoto
     */
    public function testAddPhoto()
    {
        $this->assertEquals(1, $this->personDB->addPhoto(5, 'path'));
    }

    /**
     * @covers DB\CDBPerson::checkShareCodeIsUsed
     */
    public function testCheckShareCodeIsUsed()
    {

        $this->assertFalse($this->personDB->checkShareCodeIsUsed('1234567891213213123123132'));
        $this->assertTrue($this->personDB->checkShareCodeIsUsed('123456789121'));
    }

    /**
     * 测试解除用户绑定(踢人)
     * @covers DB\CDBPerson::cancelUserBind
     */
    public function testCancelUserBind()
    {
        /* 1.能删除管理员的绑定 */
        $this->assertTrue($this->personDB->cancelUserBind(1));
        $this->assertEquals(2, $this->getConnection()->getRowCount("gener_familyAdmin"));
        /* 2.不能删除管理员的绑定  */
        $this->assertTrue($this->personDB->cancelUserBind(2));
        $this->assertEquals(2, $this->getConnection()->getRowCount("gener_familyAdmin"));

    }

    /**
     * 测试解除用户绑定，但不踢出家族
     * @covers DB\CDBPerson::cancelUserBindForTmp
     */
    public function testCancelUserBindForTmp()
    {
        /* 1.能删除管理员的绑定 */
        $this->assertTrue($this->personDB->cancelUserBindForTmp(1,1,1));
        $this->assertEquals(2, $this->getConnection()->getRowCount("gener_familyAdmin"));
        /* 2.不能删除管理员的绑定  */
        $this->assertTrue($this->personDB->cancelUserBindForTmp(2, 1, 2));
        $this->assertEquals(2, $this->getConnection()->getRowCount("gener_familyAdmin"));

    }

    /**
     * 测试检查用户是否有权限删除某个标签
     *
     * @covers DB\CDBPerson::checkUserIdAndPersonDetailId
     */
    public function testCheckUserIdAndPersonDetailId()
    {
        //用户没有操作权限
        $this->assertFalse($this->personDB->checkUserIdAndPersonDetailId(4, 2));
        //用户不存在
        $this->assertFalse($this->personDB->checkUserIdAndPersonDetailId(-1, 2));
        //用户是直系亲属
        $this->assertTrue($this->personDB->checkUserIdAndPersonDetailId(2, 1));
        //用户有操作权限
        $this->assertTrue($this->personDB->checkUserIdAndPersonDetailId(1, 1));
        //用户是家族创始人
        $this->assertTrue($this->personDB->checkUserIdAndPersonDetailId(1, 2));
        //用户是管理员
        $this->assertTrue($this->personDB->checkUserIdAndPersonDetailId(3, 1));

        $this->assertFalse($this->personDB->checkUserIdAndPersonDetailId(1, 3));
    }

    /**
     * 测试删除指定标签
     *
     * @covers DB\CDBPerson::deletePersonInfo
     */
    public function testDeletePersonInfo()
    {
        //原始行数
        $originalRowsCount = $this->getConnection()->getRowCount("gener_personDetail");
        //删除不成功
        $this->assertEquals(0, $this->personDB->deletePersonInfo(-1));
        $this->assertEquals($originalRowsCount, $this->getConnection()->getRowCount("gener_personDetail"));
        //删除成功
        $this->assertEquals(1, $this->personDB->deletePersonInfo(1));
        $this->assertEquals($originalRowsCount, $this->getConnection()->getRowCount("gener_personDetail"));
    }

    /**
     * 测试验证用户是否有权限删除职业信息
     *
     * @cover DB\CDBPerson::checkUserIdAndJobId
     */
    public function testCheckUserIdAndJobId()
    {
        //用户不存在
        $this->assertFalse($this->personDB->checkUserIdAndJobId(-1, 1));
        //用户没有操作权限
        $this->assertFalse($this->personDB->checkUserIdAndJobId(4, 1));
        //用户有操作权限
        $this->assertTrue($this->personDB->checkUserIdAndJobId(2, 2));
        //用户是直系亲属
        $this->assertTrue($this->personDB->checkUserIdAndJobId(2, 1));
        //用户是家族创始人
        $this->assertTrue($this->personDB->checkUserIdAndJobId(1, 2));
        //用户是管理员
        $this->assertTrue($this->personDB->checkUserIdAndJobId(3, 1));
        $this->assertFalse($this->personDB->checkUserIdAndJobId(3, 5));
    }

    /**
     * 测试删除指定的职业信息
     *
     * @cover DB\CDBPerson::deletePersonJob
     */
    public function testDeletePersonJob()
    {
        //原始数据行数
        $originalRowsCount = $this->getConnection()->getRowCount("gener_job");
        //删除失败
        $this->assertEquals(0, $this->personDB->deletePersonJob(-1));
        $this->assertEquals($originalRowsCount, $this->getConnection()->getRowCount("gener_job"));
        //删除成功
        $this->assertEquals(1, $this->personDB->deletePersonJob(1));
        $this->assertEquals($originalRowsCount, $this->getConnection()->getRowCount("gener_job"));
    }

    /**
     * 测试获取用户绑定的人物列表
     * @cover DB\CDBPerson::getUserBindPersonList
     */
    public function testGetUserBindPersonList()
    {
        $this->assertEquals(2, count($this->personDB->getUserBindPersonList(1)));
    }

    /**
     * 测试根据姓名和家族id获取人物
     * @cover DB\CDBPerson::getPersonByNameAndFamilyIds
     */
    public function testGetPersonByNameAndFamilyIds()
    {
        $arr = array('121', '122', '123', '124', '125', '126', '127', '128', '129', '130', '131', '132', '133', '134', '135', '136', '137', '138');
        $this->assertGreaterThan(0, $this->personDB->getPersonByNameAndFamilyIds("李白", $arr));

    }


    /**
     * 测试获取@的用户列表
     * @cover DB\CDBPerson::getAtUsers
     */
    public function testGetAtUsers()
    {
        $this->assertEquals(4, count($this->personDB->getFamilyUsers(array(1))));
    }

    /**
     * 测试根据路径的长度获取人物id
     * @cover DB\CDBPerson::getPersonsIdByPathLen
     */
    public function testGetPersonsIdByPathLen()
    {
        $resArr = $this->personDB->getPersonsIdByPathLen(1, 3);
        $this->assertEquals(7, count($resArr));
    }

    /**
     * 测试绑定用户到人物
     * @cover DB\CDBPerson::bindUserToPerson
     */
    public function testBindUserToPerson()
    {
        $this->assertEquals(1, $this->personDB->bindUserToPerson(4, '1234567891213213123123132', 1, 1));
        $userFollowDB = new UserFollowDB();

        $arr = [1, 2, 3];
        $follows = $userFollowDB->getUserFollow(4);
        sort($follows);
        $this->assertEquals($arr, $follows);

        $arr = ['2','4','11'];
        $follows = $userFollowDB->getUserFollow(1);
        sort($follows);
        $this->assertEquals($arr, $follows);

        $follows = $userFollowDB->getUserFollow(2);
        sort($follows);
        $this->assertEquals([1, 3, 4, 11], $follows);

        $follows = $userFollowDB->getUserFollow(3);
        sort($follows);
        $this->assertEquals($arr, $follows);

        //清除测试的副作用
        $userFollowDB->removeUserFollow(1, [4]);
        $userFollowDB->removeUserFollow(2, [4]);
        $userFollowDB->removeUserFollow(3, [4]);
        $userFollowDB->removeUserFollow(4, [1, 2, 3]);
    }

    /**
     * @covers DB\CDBPerson::bindUserToPersonWithoutTransaction
     */
    public function testBindUserToPersonWithoutTransaction()
    {
        $this->assertEquals(1, $this->personDB->bindUserToPersonWithoutTransaction(2, '1234567891213213123123132', 1, 1, 1));
    }

    /**
     * @covers DB\CDBPerson::bindUserToPersonWithPersonId
     */
    public function testBindUserToPersonWithPersonId()
    {

        $person = new Person();
        $person->id = 3;
        $person->familyId = 1;
        $this->assertEquals(-1, $this->personDB->bindUserToPersonWithPersonId(2, $person));
        $this->assertEquals(1, $this->personDB->bindUserToPersonWithPersonId(11, $person));
    }

    /**
     * @covers DB\CDBPerson::getFamilyIdByPersonId
     */
    public function testGetFamilyIdByPersonId()
    {
        $this->assertEquals(1, $this->personDB->getFamilyIdByPersonId(1));
        $this->assertNull($this->personDB->getFamilyIdByPersonId(11));
    }

    /**
     * @covers DB\CDBPerson::addForPerson
     */
    public function testAddForPerson()
    {
        $personId = 1;
        $userId = 1;
        $info = 'person_info';
        $type = 1;
        $operateUserId = 2;
        $this->assertEquals(3, $this->personDB->addForPerson($personId, $userId, $info, $type, $operateUserId));
    }

    /**
     * @covers DB\CDBPerson::checkTagDuplicate
     */
    public function testCheckTagDuplicate()
    {
        $this->assertTrue($this->personDB->checkTagDuplicate(1, 'test1', 0));
        $this->assertFalse($this->personDB->checkTagDuplicate(11, 'test11', 0));
    }

    /**
     * @covers DB\CDBPerson::addJobForPerson
     */
    public function testAddJobForPerson()
    {
        $personId = 1;
        $userId = 1;
        $company = 'gongsi';
        $occupation = 'php';
        $startTime = '2017-07-07';
        $endTime = '2018-09-09';
        $operateUserId = 1;
        $this->assertEquals(3, $this->personDB->addJobForPerson($personId, $userId, $company, $occupation, $startTime, $endTime, $operateUserId));
    }

    /**
     * @covers DB\CDBPerson::getPersonTag
     */
    public function testGetPersonTag()
    {
        $array = [[
            'id' => 1,
            'personId' => 1,
            'userId' => 1,
            'info' => 'test1',
            'type' => 0
        ]];
        $this->assertEquals($array, $this->personDB->getPersonTag(1));
    }

    /**
     * @covers DB\CDBPerson::getPersonJob
     */
    public function testGetPersonJob()
    {
        $array = [[
            'id' => 1,
            'personId' => 1,
            'userId' => 1,
            'company' => '123',
            'occupation' => '456',
            'startTime' => '2017-10-17',
            'endTime' => '2018-10-17'
        ]];
        $this->assertEquals($array, $this->personDB->getPersonJob(1));
    }

    /**
     * @covers DB\CDBPerson::getPhotosByPersonId
     */
    public function testGetPhotosByPersonId()
    {
        $array = [
            'photo' => 'http://test.photo'
        ];
        $this->assertEquals($array, $this->personDB->getPhotosByPersonId(5));
    }

    /**
     * @covers DB\CDBPerson::getTypeByPersonDetailId
     */
    public function testGetTypeByPersonDetailId()
    {
        $result = $this->personDB->getTypeByPersonDetailId(1);
        $this->assertEquals(0, $result['type']);
        $this->assertFalse($this->personDB->getTypeByPersonDetailId(11));
    }

    /**
     * @covers DB\CDBPerson::leaveFamily
     */
    public function testLeaveFamily()
    {

        $this->assertEquals(1, $this->personDB->leaveFamily(1, 3));
    }

    /**
     * @covers DB\CDBPerson::getPersonQRCode
     */
    public function testGetPersonQRCode()
    {
        $array = [
            'qrcode' => 'dasasdasdasdasdwasda',
            'share_url' => '1234562789'
        ];
        $this->assertEquals($array, $this->personDB->getPersonQRCode(4));
    }

    /**
     * @covers DB\CDBPerson::getChatRoomByPersonId
     */
    public function testGetChatRoomByPersonId()
    {
        $this->assertNull($this->personDB->getChatRoomByPersonId(11));
        $this->assertEquals('huanxin_chatroom', $this->personDB->getChatRoomByPersonId(1));
    }

    /**
     * @covers DB\CDBPerson::setPersonQRCode
     */
    public function testSetPersonQRCode()
    {
        $this->assertEquals(1, $this->personDB->setPersonQRCode(1, 'qrcode'));
    }

    /**
     * @covers DB\CDBPerson::getFamilyIdByActivityId
     */
    public function testGetFamilyIdByActivityId()
    {
        $this->assertEquals(1, $this->personDB->getFamilyIdByActivityId(1));
        $this->assertFalse($this->personDB->getFamilyIdByActivityId(11));
    }

    /**
     * @covers DB\CDBPerson::updateFamilyPermission
     */
    public function testUpdateFamilyPermission()
    {
        $this->assertEquals(1, $this->personDB->updateFamilyPermission(1, 2, 1));
    }

    /**
     * @covers DB\CDBPerson::getPersonIdByPersonDetailId
     */
    public function testGetPersonIdByPersonDetailId()
    {
        $this->assertEquals(1, $this->personDB->getPersonIdByPersonDetailId(1));
        $this->assertFalse($this->personDB->getPersonIdByPersonDetailId(11));
    }

    /**
     * @covers DB\CDBPerson::getPersonIdByJobId
     */
    public function testGetPersonIdByJobId()
    {
        $this->assertEquals(1, $this->personDB->getPersonIdByJobId(1));
        $this->assertFalse($this->personDB->getPersonIdByJobId(11));
    }

    /**
     * @covers DB\CDBPerson::getUserIdsFromPersonIds
     */
    public function testGetUserIdsFromPersonIds()
    {
        $idArr = [2];
        $this->assertEquals($idArr, $this->personDB->getUserIdsFromPersonIds([1, 2], 1));
    }

    /**
     * @covers DB\CDBPerson::setAdoptionPersonByPersonId
     */
    public function testSetAdoptionPersonByPersonId()
    {
        $this->assertEquals(1, $this->personDB->setAdoptionPersonByPersonId(1, 2));
    }

    /**
     * @covers DB\CDBPerson::updateSamePersonId
     */
    public function testUpdateSamePersonId()
    {
        $this->assertEquals(1, $this->personDB->setAdoptionPersonByPersonId(1, 2));
        $this->assertEquals(1, $this->personDB->updateSamePersonId(2));
    }

    /**
     * 测试移动人物
     * @covers DB\CDBPerson::movePerson
     */
    public function testMovePerson()
    {

        // 移动不存在的人物
        $this->assertFalse($this->personDB->movePerson(10, 7));

        // 移动同一分支的人物
        $this->assertFalse($this->personDB->movePerson(1, 4));

        // 移动同一分支的配偶
        $this->assertFalse($this->personDB->movePerson(1, 6));

        // 移动男性
        $this->assertTrue($this->personDB->movePerson(1, 7));

        $person7 = $this->personDB->getPersonById(7);
        $this->assertEquals([2], $person7->father);
        $this->assertEquals([3], $person7->mother);
        $this->assertEquals([], $person7->spouse);
        $this->assertEquals([], $person7->daughter);
        $this->assertEquals([1], $person7->son);
        $this->assertEquals([], $person7->brother);
        $this->assertEquals([7], $person7->sister);

        $person1 = $this->personDB->getPersonById(1);

        $this->assertEquals([], $person1->father);
        $this->assertEquals([7], $person1->mother);
        $this->assertEquals([], $person1->sister);
        $this->assertEquals([1], $person1->brother);
        $this->assertEquals([6], $person1->spouse);
        $this->assertEquals([4], $person1->son);
        $this->assertEquals([5], $person1->daughter);

        $person2 = $this->personDB->getPersonById(2);
        $this->assertEquals([], $person2->father);
        $this->assertEquals([], $person2->mother);
        $this->assertEquals([], $person2->sister);
        $this->assertEquals([2], $person2->brother);
        $this->assertEquals([3], $person2->spouse);
        $this->assertEquals([], $person2->son);
        $this->assertEquals([7], $person2->daughter);

        $person3 = $this->personDB->getPersonById(3);
        $this->assertEquals([], $person3->father);
        $this->assertEquals([], $person3->mother);
        $this->assertEquals([3], $person3->sister);
        $this->assertEquals([], $person3->brother);
        $this->assertEquals([2], $person3->spouse);
        $this->assertEquals([], $person3->son);
        $this->assertEquals([7], $person3->daughter);

        // assert level
        $this->assertEquals($person7->level + 1, $person1->level);

        $person6 = $this->personDB->getPersonById(6);
        $this->assertEquals($person7->level + 1, $person6->level);

        $person4 = $this->personDB->getPersonById(4);
        $this->assertEquals($person7->level + 2, $person4->level);

        $person5 = $this->personDB->getPersonById(5);
        $this->assertEquals($person7->level + 2, $person5->level);

        // 移动到有女儿和配偶的人物下
        $this->assertTrue($this->personDB->movePerson(5, 2));

        // 移动女性
        $this->assertTrue($this->personDB->movePerson(5, 7));
    }

    /*
     * @covers DB\CDBPerson::updateBrothersAndSistersRanking
     */
    public function testUpdateBrothersAndSistersRanking()
    {
        $this->assertEquals(1, $this->personDB->updateBrothersAndSistersRanking(1, 2));
    }

    /**
     * @covers DB\CDBPerson::batchUpdateBrothersAndSistersRanking
     */
    public function testBatchUpdateBrothersAndSistersRanking()
    {
        $personIdArr = [1, 2, 3];
        $rankingArr = [4, 5, 6];
        $this->assertEquals(3, $this->personDB->batchUpdateBrothersAndSistersRanking($personIdArr, $rankingArr));
    }

    /**
     * @covers DB\CDBPerson::bindUserInfoCardInFamily
     */
    public function testBindUserInfoCardInFamily()
    {
        $this->assertEquals(1, $this->personDB->bindUserInfoCardInFamily(1, 1));

    }

    /**
     * @covers DB\CDBPerson::getInfoCardByPersonId
     */
    public function testGetInfoCardByPersonId()
    {
        $this->assertEquals(1, $this->personDB->bindUserInfoCardInFamily(1, 1));
        $infoCard = new InfoCard();

        $infoCard->id = '1';
        $infoCard->remark = 'my info card';
        $infoCard->userId = '1';
        $infoCard->familyId = '0';
        $infoCard->personId = '0';
        $infoCard->name = 'jiangpengfei';
        $infoCard->nameEn = 'jack';
        $infoCard->country = '1';
        $infoCard->countryName = 'china';
        $infoCard->province = '1';
        $infoCard->provinceName = 'anhui';
        $infoCard->city = '1';
        $infoCard->cityName = 'chaohu';
        $infoCard->area = '1';
        $infoCard->areaName = 'juchaoqu';
        $infoCard->town = '1';
        $infoCard->townName = 'asda';
        $infoCard->address = '123123';
        $infoCard->photo = '1';
        $infoCard->backgroundImg = '1';
        $infoCard->backgroundColor = '1';
        $infoCard->backgroundType = '0';
        $infoCard->company = '1';
        $infoCard->companyEn = '1';
        $infoCard->gender = '1';
        $infoCard->birthday = '2017-09-09';
        $infoCard->bloodType = '1';
        $infoCard->maritalStatus = '1';
        $infoCard->phone = 112312312312;
        $infoCard->qq = '78768127381';
        $infoCard->email = '123@qq.com';
        $infoCard->companyProfile = '1';
        $infoCard->companyUrl = '123.com';
        $infoCard->bankAccount = '1234567890';
        $infoCard->longitude = 10;
        $infoCard->latitude = 10;
        $infoCard->skills = array("eat", "sleep", 'fishing', 'haha');
        $infoCard->favorites = array('1', '2', '3', '4');
        $infoCard->jobs = array(array("startTime" => "2017-09-09", "endTime" => "2017-10-10", "job" => "develop"), array("startTime" => " 2017-09-09", "endTime" => "2017-10-10", "job" => "develop"));
        $infoCard->identity = 'f1e6e8812aa0aa76acd7704761c4041d';
        $infoCard->permission = 0;
        $infoCard->position = 'work';
        $infoCard->ocrCardImg = 'ocrcard_img';
        $infoCard->taxNumber = '111111111';
        $infoCard->createTime = '2017-09-09 09:09:09';
        $infoCard->createBy = '1';
        $infoCard->updateTime = '2017-09-09 09:09:09';
        $infoCard->updateBy = '1';
        $infoCard->type = 0;
        $infoCard->isDefault = 0;
        $infoCard->imageOrder = array();
        $infoCard->viewNum = null;
        $infoCard->shareNum = null;

        $this->assertEquals($infoCard, $this->personDB->getInfoCardByPersonId(1));
    }

    /**
     * @covers DB\CDBPerson::addAdoptionPersonByPersonId
     */
    public function testAddAdoptionPersonByPersonId()
    {
        $this->assertEquals(4, $this->personDB->addAdoptionPersonByPersonId(1, 7));
    }

    /**
     * @covers DB\CDBPerson::deleteAdoptionPersonByPersonId
     */
    public function testDeleteAdoptionPersonByPersonId()
    {
        $this->assertEquals(1, $this->personDB->deleteAdoptionPersonByPersonId(1, 2));
    }

    /**
     * @covers DB\CDBPerson::checkDestPersonIdIsExists
     */
    public function testCheckDestPersonIdIsExists()
    {
        $this->assertTrue($this->personDB->checkDestPersonIdIsExists(2));
        $this->assertFalse($this->personDB->checkDestPersonIdIsExists(1));
    }

    /**
     * @covers DB\CDBPerson::checkAdoptionExists
     */
    public function testCheckAdoptionExists()
    {
        $this->assertTrue($this->personDB->checkAdoptionExists(1, 2));
        $this->assertFalse($this->personDB->checkAdoptionExists(1, 7));
    }

    /**
     * @covers DB\CDBPerson::getAdoptionPersonList
     */
    public function testGetAdoptionPersonList()
    {
        $array = [[
            'src_personId' => "1",
            'dest_personId' => "2",
            'is_delete' => "0",
            'create_time' => "2017-09-09 09:09:09",
            'create_by' => "1",
            'update_time' => "2017-09-09 09:09:09",
            'update_by' => "1"
        ]];
        $this->assertEquals($array, $this->personDB->getAdoptionPersonList(1));
    }

    /**
     * @covers DB\CDBPerson::changePersonType
     */
    public function testChangePersonType()
    {
        $this->assertEquals(1, $this->personDB->changePersonType(1, 1));
    }

    /**
     * @covers DB\CDBPerson::restoreIsAdoption
     */
    public function testRestoreIsAdoption()
    {
        $this->assertEquals(1, $this->personDB->restoreIsAdoption(7));
    }

    /**
     * @covers DB\CDBPerson::checkSrcPersonIdIsExists
     */
    public function testCheckSrcPersonIdIsExists()
    {
        $this->assertTrue($this->personDB->checkSrcPersonIdIsExists(1));
        $this->assertFalse($this->personDB->checkSrcPersonIdIsExists(2));
    }

    /**
     * @covers DB\CDBPerson::addPersonToInnerBranch
     */
    public function testAddPersonToInnerBranch()
    {
        $this->assertGreaterThan(0, $this->personDB->addPersonToInnerBranch(1, 1, 2));
    }

    /**
     * @covers DB\CDBPerson::getInnerBranchPersons
     */
    public function testGetInnerBranchPersons()
    {
        $this->assertGreaterThan(0, count($this->personDB->getInnerBranchPersons(1)));
    }

    /**
     * @covers DB\CDBPerson::getPersonBranchList
     */
    public function testGetPersonBranchList()
    {
        $this->assertGreaterThan(0, count($this->personDB->getPersonBranchList(2)));
    }

    /**
     * @covers DB\CDBPerson::getPersonsPagingBySnapshotVersion
     */
    public function testGetPersonsPagingBySnapshotVersion() 
    {
        $this->assertEquals(1, count($this->personDB->getPersonsPagingBySnapshotVersion(1, 1, 10, 0, 0)));
        $this->assertEquals(0, count($this->personDB->getPersonsPagingBySnapshotVersion(1, 1, 10, 1, 0)));
        $this->assertEquals(0, count($this->personDB->getPersonsPagingBySnapshotVersion(1, 3, 10, 0, 0)));
    }

    /**
     * @covers DB\CDBPerson::getPersonsCountBySnapshotVersion
     */
    public function testGetPersonsCountBySnapshotVersion() {
        $this->assertEquals(1, $this->personDB->getPersonsCountBySnapshotVersion(1, 1));
        $this->assertEquals(0, $this->personDB->getPersonsCountBySnapshotVersion(1, 3));
    }

    /**
     * @covers DB\CDBPerson::getMiniFamilyPersonsPaging
     */
    public function testGetMiniFamilyPersonsPaging() 
    {
        $this->assertEquals(1, count($this->personDB->getMiniFamilyPersonsPaging(2, 0, 0, 10)));
        $this->assertEquals(0, count($this->personDB->getMiniFamilyPersonsPaging(2, 1, 0, 10)));
        $this->assertEquals(0, count($this->personDB->getMiniFamilyPersonsPaging(87, 0, 0, 10)));
    }

    /**
     * @covers DB\CDBPerson::recordPersonOptionLog
     */

    public function testRecordPersonOptionLog() 
    {
        $personId = 53;
        $familyId = 2;
        $function_name = __FUNCTION__;
        $person = $this->personDB->getPersonById($personId);
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '测试方法';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = NULL;
        $res = $this->personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
        $this->assertGreaterThan(0, $res);

    }
}
