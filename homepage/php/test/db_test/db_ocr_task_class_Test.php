<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/13 0013
 * Time: 15:34
 */

use DB\CDBOCRTask;
use Model\Task;
use Model\TaskTender;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;


class DBOcrTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $ocrDB = null;


    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->ocrDB = new CDBOCRTask();
        return $this->createMySQLXMLDataSet(__DIR__ . '/xml/ocr_task_dataset.xml');     //默认的数据集
    }

    /**
     * 测试添加ocr任务
     * @covers DB\CDBOCRTask::addOCRTask
     */
    public function testAddOCRTask()
    {
        $this->assertGreaterThan(0, $this->ocrDB->addOCRTask(1, 'this is photo'));
    }

    /**
     * @covers DB\CDBOCRTask::updateOCRResult()
     */
    public function testUpdateOCRResult()
    {
        $this->assertEquals(1, $this->ocrDB->updateOCRResult(1, 'result1232'));
    }

    /**
     * 测试获取OCR结果
     * @covers DB\CDBOCRTask::getOCRResult
     */
    public function testGetOCRResult()
    {
        $result = $this->ocrDB->getOCRResult(1);
        $this->assertEquals(1, $result['id']);
    }
}

