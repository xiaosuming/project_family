<?php

use DB\CDBFamilyHeirloom;
use Model\FamilyHeirloom;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-19
 * Time: 下午4:26
 */
class DBHeirloom extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $familyHeirloomDB = null;

    /**
     * Returns the test database connection.
     *
     * @return \PHPUnit\DbUnit\Database\Connection
     */
    final function getConnection()
    {
        // TODO: Implement getConnection() method.
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    /**
     * Returns the test dataset.
     *
     * @return \PHPUnit\DbUnit\DataSet\IDataSet
     */
    protected function getDataSet()
    {
        // TODO: Implement getDataSet() method.
        $this->familyHeirloomDB = new CDBFamilyHeirloom();
        return $this->createMySQLXMLDataSet('db_test/xml/family_heirloom_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBFamilyHeirloom::verifyUserIdAndHeirloomId()
     */
    public function testVerifyUserIdAndHeirloomId()
    {
        $this->assertTrue(true, $this->familyHeirloomDB->verifyUserIdAndHeirloomId(1, 1));
        $this->assertFalse( $this->familyHeirloomDB->verifyUserIdAndHeirloomId(1, 111));
    }

    /**
     * @covers DB\CDBFamilyHeirloom::addHeirloom()
     */
    public function testAddHeirloom()
    {

        $familyId = 1;
        $title = 'title';
        $content = 'content';
        $photo = 'photo';
        $userId = 1;
        $count = $this->getConnection()->getRowCount('gener_family_heirloom');
        $this->assertEquals(1, $count);
        $this->assertEquals(2, $this->familyHeirloomDB->addHeirloom($familyId, $title, $content, $photo, $userId));
        $this->assertEquals(2, $count + 1);
    }

    /**
     * @covers DB\CDBFamilyHeirloom::updateHeirloom()
     */
    public function testUpdateHeirloom()
    {
        $heirloomId = 1;
        $title = 'title1';
        $content = 'content1';
        $photo = 'photo1';
        $userId = 1;
        $this->assertEquals(1, $this->familyHeirloomDB->updateHeirloom($heirloomId, $title, $content, $photo, $userId));
    }

    /**
     * @covers DB\CDBFamilyHeirloom::deleteHeirloom()
     */
    public function testDeleteHeirloom()
    {
        $this->assertEquals(1, $this->familyHeirloomDB->deleteHeirloom(1));
    }

    /**
     * @covers DB\CDBFamilyHeirloom::getHeirloom()
     */
    public function testGetHeirloom()
    {
        $heirloom = new FamilyHeirloom(array());
        $heirloom->id = 1;
        $heirloom->familyId = 1;
        $heirloom->title = 'title';
        $heirloom->content = 'content';
        $heirloom->photo = 'photo';
        $heirloom->createTime = '2018-06-19';
        $heirloom->createBy = 1;
        $heirloom->updateTime = '2018-06-19';
        $heirloom->updateBy = 1;
        $this->assertEquals($heirloom, $this->familyHeirloomDB->getHeirloom(1));
        $this->assertNull($this->familyHeirloomDB->getHeirloom(111));
    }

    /**
     * @covers DB\CDBFamilyHeirloom::getHeirloomsTotal()
     */
    public function testGetHeirloomsTotal()
    {
        $this->assertEquals(1, $this->familyHeirloomDB->getHeirloomsTotal(1));
    }

    /**
     * @covers DB\CDBFamilyHeirloom::getHeirloomsPaging()
     */
    public function testGetHeirloomsPaging()
    {
        $array = [[
            'id' => 1,
            'familyId' => 1,
            'title' => 'title',
            'content' => 'content',
            'photo' => 'photo',
            'createTime' => '2018-06-19',
            'createBy' => 1,
            'updateTime' => '2018-06-19',
            'updateBy' => 1
        ]];

        $this->assertEquals($array, $this->familyHeirloomDB->getHeirloomsPaging(1, 1, 1));
    }

}