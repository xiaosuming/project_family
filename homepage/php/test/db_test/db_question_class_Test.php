<?php

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use DB\CDBQuestion;
use Model\Question;
use Model\Answer;
use Util\Util;

class DBQuestionTest extends TestCase {
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $questionDB = null;

    final public function getConnection()
    {
        if($this->conn === null){
            if(self::$pdo === null){
                self::$pdo = new PDO( $GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD'] );
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo,$GLOBALS['DB_DBNAME']);
        }
        return $this->conn;
    }

    public function getDataSet(){
        $this->questionDB =  new CDBQuestion();
        return $this->createMySQLXMLDataSet('db_test/xml/question_dataset.xml');     //默认的数据集
    }

    /**
     * 测试 添加问题
     *
     * @covers DB\CDBQuestion::addQuestion
     */
    public function testAddQuestion(){
        $originalRows = $this->getConnection()->getRowCount("gener_user_question");
        $this->assertGreaterThan(0, $this->questionDB->addQuestion(1, 1, "title", "content", "photo"));
        $this->assertEquals($originalRows+1, $this->getConnection()->getRowCount("gener_user_question"));
    }

    /**
     * 测试 根据问题ID获取问题详情
     *
     * @covers DB\CDBQuestion::getQuestionByQuestionId
     */
    public function testGetQuestionByQuestionId() {
        //id not exist
        $this->assertNull($this->questionDB->getQuestionByQuestionId(-1));
        //id exist
        $question = new Question();
        $question->id = "1";
        $question->userId = "2";
        $question->nickname = 'jiang111';
        $question->userPhoto = 'http://test.photo';
        $question->title = "hahaha";
        $question->content = "hahaha";
        $question->photo = "asdfasfasfas";
        $question->addition = "1";
        $question->createTime = "2017-12-14 10:08:46";
        $question->createBy = "2";
        $question->updateTime = "2017-12-14 10:08:46";
        $question->updateBy = "2";
        $this->assertEquals($question, $this->questionDB->getQuestionByQuestionId(1));
    }

    /**
     * 测试 获取指定问题集的问题详情
     *
     * @covers DB\CDBQuestion::getQuestionsDetail
     */
    public function testGetQuestionsDetail() {
        //all id exist
        $resultSet = array(
            0 => array(
                "id" => "1",
                "userId" => "2",
                "title" => "hahaha",
                "content" => "hahaha",
                "photo" => "asdfasfasfas",
                "addition" => "1",
                "answerCount" => "2",
                "createTime" => "2017-12-14 10:08:46",
                "createBy" => "2",
                "updateTime" => "2017-12-14 10:08:46",
                "updateBy" => "2"
            ),
            1 => array(
                "id" => "2",
                "userId" => "3",
                "title" => "test2",
                "content" => "testing2",
                "photo" => "asdgagasgfsafsafsa",
                "addition" => "0",
                "answerCount" => null,
                "createTime" => "2017-12-14 10:08:46",
                "createBy" => "3",
                "updateTime" => "2017-12-14 10:08:46",
                "updateBy" => "3"
            )
        );
        $this->assertEquals($resultSet, $this->questionDB->getQuestionsDetail("(1,2)"));
        //one exist, not another
        array_pop($resultSet);
        $this->assertEquals($resultSet, $this->questionDB->getQuestionsDetail("(1,-1)"));
        //none id exist
        array_pop($resultSet);
        $this->assertEquals($resultSet, $this->questionDB->getQuestionsDetail("(-1,-1)"));
        //other error
        $this->assertNull($this->questionDB->getQuestionsDetail("2"));
    }

    /**
     * 测试 删除问题
     *
     * @covers DB\CDBQuestion::deleteQuestion
     */
    public function testDeleteQuestion() {
        //id not exist
        //get question rows
        $questionRows = $this->getConnection()->getRowCount("gener_user_question");
        //get answer rows
        $answerRows = $this->getConnection()->getRowCount("gener_question_answer");
        //get addition rows
        $additionRows = $this->getConnection()->getRowCount("gener_question_addition");
        //get comment rows
        $commentRows = $this->getConnection()->getRowCount("gener_answer_comment");
        //get like rows
        $likeRows = $this->getConnection()->getRowCount("gener_like");
        //delete
        $this->assertEquals(0, $this->questionDB->deleteQuestion(-1, 2));
        $this->assertEquals($questionRows, $this->getConnection()->getRowCount("gener_user_question"));
        $this->assertEquals($answerRows, $this->getConnection()->getRowCount("gener_question_answer"));
        $this->assertEquals($additionRows, $this->getConnection()->getRowCount("gener_question_addition"));
        $this->assertEquals($commentRows, $this->getConnection()->getRowCount("gener_answer_comment"));
        $this->assertEquals($likeRows, $this->getConnection()->getRowCount("gener_like"));
        //id exist
        //delete
        $this->assertEquals(9, $this->questionDB->deleteQuestion(1, 2));
        $this->assertEquals(2, $this->getConnection()->getRowCount("gener_user_question"));
        $this->assertEquals(2, $this->getConnection()->getRowCount("gener_question_answer"));
        $this->assertEquals(1, $this->getConnection()->getRowCount("gener_question_addition"));
        $this->assertEquals(3, $this->getConnection()->getRowCount("gener_answer_comment"));
        $this->assertEquals(2, $this->getConnection()->getRowCount("gener_like"));
    }

    /**
     * 测试 检查用户操作问题的权限
     *
     * @covers DB\CDBQuestion::checkUserIdAndQuestionId
     */
    public function testCheckUserIdAndQuestionId() {
        //userId exist, questionId exist
        $this->assertTrue($this->questionDB->checkUserIdAndQuestionId(3, 2));//has permission
        $this->assertFalse($this->questionDB->checkUserIdAndQuestionId(3, 1));//has no permission
        //userId not exist, questionId exist
        $this->assertFalse($this->questionDB->checkUserIdAndQuestionId(-1, 1));
        //userId exist, questionId not exist
        $this->assertFalse($this->questionDB->checkUserIdAndQuestionId(3, -1));
        //userId not exist, questionId not exist
        $this->assertFalse($this->questionDB->checkUserIdAndQuestionId(-1, -1));
    }

    /**
     * 测试 添加追问
     *
     * @covers DB\CDBQuestion::addAdditionQuestion
     * @covers DB\CDBQuestion::getQuestionsDetail
     */
    public function testAddAdditionQuestion() {
        $originalRows = $this->getConnection()->getRowCount("gener_question_addition");
        $this->assertGreaterThan(0, $this->questionDB->addAdditionQuestion(3, 2, "content", "photo"));
        $this->assertEquals($originalRows+1, $this->getConnection()->getRowCount("gener_question_addition"));
        $question = $this->questionDB->getQuestionByQuestionId(2);
        $this->assertEquals(1, $question->addition);
    }

    /**
     * 测试 获取某个问题的所有追问
     *
     * @covers DB\CDBQuestion::getAdditionQuestions
     */
    public function testGetAdditionQuestions() {
        //id exist
        $resultSet = array(
            array(
                "id" => "1",
                "questionId" => "1",
                "content" => "asdfasdf",
                "photo" => "adfasfas",
                "createTime" => "2017-12-14 10:08:46",
                "createBy" => "2",
                "updateTime" => "2017-12-14 10:08:46",
                "updateBy" => "2"
            )
        );
        $this->assertEquals($resultSet, $this->questionDB->getAdditionQuestions(1));
        //id not exist
        $this->assertEquals(array(), $this->questionDB->getAdditionQuestions(-1));
        //id is string
        $this->assertEquals(array(), $this->questionDB->getAdditionQuestions("asd!@#$%^&*()_+-=-"));
    }

    /**
     * 测试 添加用户回答
     *
     * @covers DB\CDBQuestion::addAnswer
     */
    public function testAddAnswer() {
        $originalRows = $this->getConnection()->getRowCount("gener_question_answer");
        $this->assertGreaterThan(0, $this->questionDB->addAnswer(3, 1, "answer testing"));
        $this->assertEquals($originalRows+1, $this->getConnection()->getRowCount("gener_question_answer"));
    }

    /**
     * 测试 根据ID获取回答详情
     *
     * @covers DB\CDBQuestion::getAnswerById
     */
    public function testGetAnswerById() {
        //exsit
        $answer = new Answer();
        $answer->id = "1";
        $answer->userId = "2";
        $answer->questionId = "1";
        $answer->content = "65a1sdf611!@#!@#";
        $answer->comment_count = "3";
        $answer->like_count = "0";
        $answer->createTime = "2017-12-14 10:08:46";
        $answer->createBy = "2";
        $answer->updateTime = "2017-12-14 10:08:46";
        $answer->updateBy = "2";
        $this->assertEquals($answer, $this->questionDB->getAnswerById(1));
        //not exist
        $this->assertNull($this->questionDB->getAnswerById(-1));
        //id is string
        $this->assertNull($this->questionDB->getAnswerById("auisdhf!@$#!$"));
    }

    /**
     * 测试 分页获取某个问题的所有回答
     *
     * @covers DB\CDBQuestion::getAnswersPaging
     */
    public function testGetAnswersPaging() {
        //id exist
        $this->assertEquals(0, count($this->questionDB->getAnswersPaging(1, -1, -1)));//illegal pageIndex and pageSize
        $this->assertEquals(2, count($this->questionDB->getAnswersPaging(1, 1, 10)));//legal pageIndex and pageSize
        $this->assertEquals(0, count($this->questionDB->getAnswersPaging(1, 10, 10)));//over pageIndex
        $this->assertEquals(0, count($this->questionDB->getAnswersPaging(1, 0, 10)));//pageIndex equals to zero
        //id not exist
        $this->assertEquals(0, count($this->questionDB->getAnswersPaging(-1, 1, 1)));
    }

    /**
     * 测试 获取某个问题的回答数量
     *
     * @covers DB\CDBQuestion::getAnswersCount
     */
    public function testGetAnswersCount() {
        //id exist
        $this->assertEquals(2, $this->questionDB->getAnswersCount(1));
        //id not exist
        $this->assertEquals(0, $this->questionDB->getAnswersCount(-1));
    }

    /**
     * 测试 检查用户操作回答的权限
     *
     * @covers DB\CDBQuestion::checkUserIdAndAnswerId
     */
    public function testCheckUserIdAndAnswerId() {
        //userId exist, questionId exist
        $this->assertTrue($this->questionDB->checkUserIdAndAnswerId(2, 1));    //has permission
        $this->assertFalse($this->questionDB->checkUserIdAndAnswerId(2, 2));    //has no permission
        //userId not exist, questionId exist
        $this->assertFalse($this->questionDB->checkUserIdAndAnswerId(-1, 1));
        //userId exist, questionId not exist
        $this->assertFalse($this->questionDB->checkUserIdAndAnswerId(2, -1));
        //userId not exist, questionId not exist
        $this->assertFalse($this->questionDB->checkUserIdAndAnswerId(-1, -1));
    }

    /**
     * 测试 用户修改回答
     *
     * @covers DB\CDBQuestion::updateAnswerContent
     */
    public function testUpdateAnswerContent() {
        //answerId exist
        $this->assertEquals(1, $this->questionDB->updateAnswerContent(2, "", 3));//content is empty
        $this->assertEquals(1, $this->questionDB->updateAnswerContent(2, "asdfasfd", 3));//content is normal
        //answerId not exist
        $this->assertEquals(0, $this->questionDB->updateAnswerContent(-1, "", 3));
        //userId not exist
        $this->assertEquals(1, $this->questionDB->updateAnswerContent(2, "", -1));
    }

    /**
     * 测试 获取回答详情
     *
     * @covers DB\CDBQuestion::getAnswerDetail
     */
    public function testGetAnswerDetail(){
        //answerId not exist
        $this->assertNull($this->questionDB->getAnswerDetail(-1));
        //answerIdexist
        $answer = new Answer();
        $answer->id = "1";
        $answer->userId = "2";
        $answer->questionId = "1";
        $answer->content = "65a1sdf611!@#!@#";
        $answer->comment_count = "3";
        $answer->like_count = "0";
        $answer->createTime = "2017-12-14 10:08:46";
        $answer->updateTime = "2017-12-14 10:08:46";
        $this->assertEquals($answer, $this->questionDB->getAnswerDetail(1));
    }

    /**
     * 测试 添加评论
     *
     * @covers DB\CDBQuestion::addComment
     */
    public function testAddComment(){
        $originalRows = $this->getConnection()->getRowCount("gener_answer_comment");
        $this->assertGreaterThan(0, $this->questionDB->addComment(2, 4, -1, 4, "asdfasdf"));
        $this->assertEquals($originalRows+1, $this->getConnection()->getRowCount("gener_answer_comment"));
    }

    /**
     * 测试 获取某个回答的所有评论
     *
     * @covers DB\CDBQuestion::getComments
     */
    public function testGetComments() {
        //id not exist
        $comments = $this->questionDB->getComments(-1);
        $this->assertEquals(0, count($comments));
        //id exist
        $comments = $this->questionDB->getComments(1);
        $this->assertEquals(3, count($comments));
    }

    /**
     * 测试 检查用户操作评论的权限
     *
     * @covers DB\CDBQuestion::checkUserIdAndCommentId
     */
    public function testCheckUserIdAndCommentId() {
        //userId exist, commemtId exist
        $this->assertTrue($this->questionDB->checkUserIdAndCommentId(2, 3));    //has permission
        $this->assertFalse($this->questionDB->checkUserIdAndCommentId(2, 2));    //has no permission
        //userId not exist, commemtId exist
        $this->assertFalse($this->questionDB->checkUserIdAndCommentId(-1, 1));
        //userId exist, commemtId not exist
        $this->assertFalse($this->questionDB->checkUserIdAndCommentId(2, -1));
        //userId not exist, commemtId not exist
        $this->assertFalse($this->questionDB->checkUserIdAndCommentId(-1, -1));
    }

    /**
     * 测试 删除指定评论
     *
     * @covers DB\CDBQuestion::deleteComment
     */
    public function testDeleteComment() {
        //id exist
        //before
        //get comment count
        $answer = $this->questionDB->getAnswerById(1);
        $originalCommentCount = $answer->comment_count;
        //get rows
        $originalRows = $this->getConnection()->getRowCount("gener_answer_comment");
        //delete
        $this->assertEquals(1, $this->questionDB->deleteComment(1));
        //after
        $answer = $this->questionDB->getAnswerById(1);
        $this->assertEquals($originalRows, $this->getConnection()->getRowCount("gener_answer_comment"));
        $this->assertEquals($originalCommentCount-1, $answer->comment_count);

        //id not exist
        //before
        //get comment count
        $answer = $this->questionDB->getAnswerById(1);
        $originalCommentCount = $answer->comment_count;
        //get rows
        $originalRows = $this->getConnection()->getRowCount("gener_answer_comment");
        //delete
        $this->assertEquals(0, $this->questionDB->deleteComment(-1));
        //after
        $answer = $this->questionDB->getAnswerById(1);
        $this->assertEquals($originalRows, $this->getConnection()->getRowCount("gener_answer_comment"));
        $this->assertEquals($originalCommentCount, $answer->comment_count);
    }

    /**
     * 测试 更新回答点赞数
     *
     * @covers DB\CDBQuestion::updateAnswerLikeCount
     */
    public function testUpdateAnswerLikeCount() {
        //get answer
        $answer = $this->questionDB->getAnswerById(1);
        //id not exist
        $this->assertEquals(0, $this->questionDB->updateAnswerLikeCount(-1, 3));
        //likeCount is less than zero
        $this->assertEquals(0, $this->questionDB->updateAnswerLikeCount(1, -1));
        //likeCount is string with number
        $this->assertEquals(0, $this->questionDB->updateAnswerLikeCount(1, "128"));
        //likeCount is string with letter
        $this->assertEquals(0, $this->questionDB->updateAnswerLikeCount(1, "asd"));
        //id exist
        $this->assertEquals(1, $this->questionDB->updateAnswerLikeCount(1, 3));
    }
}
