<?php

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use DB\CDBInfoCard;
use Model\InfoCard;
use Util\Util;
use Model\InfoCardShareRecord;
use Model\InfoCardExchange;


class DBInfoCardTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $infoCardDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {

            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->infoCardDB = new CDBInfoCard();
        return $this->createMySQLXMLDataSet('db_test/xml/info_card_dataset.xml');     //默认的数据集
    }

    /**
     * 测试添加用户名片
     * @covers DB\CDBInfoCard::addInfoCard
     */
    public function testAddInfoCard()
    {

        $initCount = $this->getConnection()->getRowCount('gener_info_card');

        $infoCard = new InfoCard();

        $infoCard->remark = 'my info card';
        $infoCard->userId = 1;
        $infoCard->familyId = 1;
        $infoCard->personId = 1;
        $infoCard->socialCircleId = 3;
        $infoCard->name = 'jiangpengfei';
        $infoCard->country = '1';
        $infoCard->countryName = 'china';
        $infoCard->province = '1';
        $infoCard->provinceName = 'anhui';
        $infoCard->city = '1';
        $infoCard->cityName = 'chaohu';
        $infoCard->area = '1';
        $infoCard->areaName = 'juchaoqu';
        $infoCard->town = '1';
        $infoCard->townName = '131231';
        $infoCard->address = '123123';
        $infoCard->photo = '1';
        $infoCard->backgroundImg = '1';
        $infoCard->backgroundColor = '1';
        $infoCard->backgroundType = '0';
        $infoCard->company = '1';
        $infoCard->gender = '1';
        $infoCard->birthday = '2018-09-09';
        $infoCard->bloodType = '1';
        $infoCard->maritalStatus = '1';
        $infoCard->phone = '11231231231';
        $infoCard->qq = '781684123';
        $infoCard->skills = json_encode(array('1', '2', '3', '4'));
        $infoCard->favorites = json_encode(array('1', '2', '3', '4'));
        $infoCard->jobs = json_encode(array(array('startTime' => '1', 'endTime' => '2', 'job' => '3'), array('startTime' => '1', 'endTime' => '2', 'job' => '3')));
        $infoCard->type = '0';
        $infoCard->identity = 'f1e6e8812aa0aa76acd7704761c4041d';
        $infoCard->createBy = '1';
        $infoCard->updateBy = '1';
        $infoCard->isDefault = 0;

        $this->assertGreaterThan(1, $this->infoCardDB->addInfoCard($infoCard));

        $this->assertEquals($initCount + 1, $this->getConnection()->getRowCount('gener_info_card'));
    }

    /**
     * 测试通讯录添加名片
     * @covers DB\CDBInfoCard::addInfoCardByAddressList()
     */
    public function testAddInfoCardByAddressList()
    {
        $initCount = $this->getConnection()->getRowCount('gener_info_card');
        $infoCard = new InfoCard();
        $infoCard->remark = 'my addressBook card';
        $infoCard->userId = 2;
        $infoCard->photo = '1';
        $infoCard->name = 'xuliulei';
        $infoCard->phone = '13778787889';
        $infoCard->skills = json_encode(array());
        $infoCard->favorites = json_encode(array());
        $infoCard->jobs = json_encode(array());
        $infoCard->createBy = 2;
        $infoCard->updateBy = 2;
        $this->assertGreaterThan(1, $this->infoCardDB->addInfoCardByAddressList($infoCard));
        $this->assertEquals($initCount + 1, $this->getConnection()->getRowCount('gener_info_card'));
    }

    /**
     * 测试编辑用户名片
     * @covers DB\CDBInfoCard::editInfoCard
     */
    public function testEditInfoCard()
    {
        $infoCard = new InfoCard();

        $infoCard->id = 1;
        $infoCard->remark = 'my info card';
        $infoCard->userId = 1;
        $infoCard->name = 'jiangpengfei';
        $infoCard->country = '1';
        $infoCard->countryName = 'china';
        $infoCard->province = '1';
        $infoCard->provinceName = 'anhui';
        $infoCard->city = '1';
        $infoCard->cityName = 'chaohu';
        $infoCard->area = '1';
        $infoCard->areaName = 'juchaoqu';
        $infoCard->town = '1';
        $infoCard->townName = '131231';
        $infoCard->address = '123123';
        $infoCard->photo = '1';
        $infoCard->backgroundImg = '1';
        $infoCard->backgroundColor = '1';
        $infoCard->backgroundType = '0';
        $infoCard->company = '1';
        $infoCard->gender = '1';
        $infoCard->birthday = '2018-09-09';
        $infoCard->bloodType = '1';
        $infoCard->maritalStatus = '1';
        $infoCard->phone = '112312312312';
        $infoCard->qq = '781684123';
        $infoCard->permission = 0;
        $infoCard->skills = json_encode(array('1', '2', '3', '4'));
        $infoCard->favorites = json_encode(array('1', '2', '3', '4'));
        $infoCard->jobs = json_encode(array(array('startTime' => '1', 'endTime' => '2', 'job' => '3'), array('startTime' => '1', 'endTime' => '2', 'job' => '3')));
        $infoCard->updateBy = '1';

        $this->assertEquals(1, $this->infoCardDB->editInfoCard($infoCard));

        $infoCard->id = 200;
        $this->assertEquals(0, $this->infoCardDB->editInfoCard($infoCard));
    }

    /**
     * 测试删除名片
     * @covers DB\CDBInfoCard::deleteInfoCard
     */
    public function testDeleteInfoCard()
    {
        $initCount = $this->getConnection()->getRowCount('gener_info_card');
        $this->assertEquals(1, $this->infoCardDB->deleteInfoCard(1));
        $this->assertEquals(0, $this->infoCardDB->deleteInfoCard(4));
        $this->assertEquals($initCount, $this->getConnection()->getRowCount('gener_info_card'));
    }

    /**
     * 测试获取名片详情
     * @covers DB\CDBInfoCard::getInfoCard
     */
    public function testGetInfoCard()
    {
        $infoCard = new InfoCard();

        $infoCard->id = '1';
        $infoCard->remark = 'my info card';
        $infoCard->userId = '1';
        $infoCard->familyId = '0';
        $infoCard->personId = '0';
        $infoCard->socialCircleId =1;
        $infoCard->name = 'jiangpengfei';
        $infoCard->nameEn = 'jack';
        $infoCard->country = '1';
        $infoCard->countryName = 'china';
        $infoCard->province = '1';
        $infoCard->provinceName = 'anhui';
        $infoCard->city = '1';
        $infoCard->cityName = 'chaohu';
        $infoCard->area = '1';
        $infoCard->areaName = 'juchaoqu';
        $infoCard->town = '1';
        $infoCard->townName = 'asda';
        $infoCard->address = '123123';
        $infoCard->photo = '1';
        $infoCard->backgroundImg = '1';
        $infoCard->backgroundColor = '1';
        $infoCard->backgroundType = '0';
        $infoCard->company = '1';
        $infoCard->companyEn = '1';
        $infoCard->gender = '1';
        $infoCard->birthday = '2017-09-09';
        $infoCard->bloodType = '1';
        $infoCard->maritalStatus = '1';
        $infoCard->phone = 112312312312;
        $infoCard->qq = '78768127381';
        $infoCard->email = '123@qq.com';
        $infoCard->companyProfile = '1';
        $infoCard->companyUrl = '123.com';
        $infoCard->bankAccount = '1234567890';
        $infoCard->longitude = 10;
        $infoCard->latitude = 10;
        $infoCard->skills = array("eat", "sleep", 'fishing', 'haha');
        $infoCard->favorites = array('1', '2', '3', '4');
        $infoCard->jobs = array(array("startTime" => "2017-09-09", "endTime" => "2017-10-10", "job" => "develop"), array("startTime" => " 2017-09-09", "endTime" => "2017-10-10", "job" => "develop"));
        $infoCard->identity = 'f1e6e8812aa0aa76acd7704761c4041d';
        $infoCard->permission = 0;
        $infoCard->position = 'work';
        $infoCard->ocrCardImg = 'ocrcard_img';
        $infoCard->taxNumber = '111111111';
        $infoCard->createTime = '2017-09-09 09:09:09';
        $infoCard->createBy = '1';
        $infoCard->updateTime = '2017-09-09 09:09:09';
        $infoCard->updateBy = '1';
        $infoCard->type = 0;
        $infoCard->isDefault = 0;
        $infoCard->openScope = 0;

        $this->assertEquals($infoCard, $this->infoCardDB->getInfoCard(1));

        $this->assertEquals(null, $this->infoCardDB->getInfoCard(4));
    }

    /**
     * 测试获取所有的名片
     * @covers DB\CDBInfoCard::getAllInfoCards
     */
    public function testGetAllInfoCards()
    {
        $this->assertEquals(2, count($this->infoCardDB->getAllInfoCards(1, 0)));

        $this->assertEquals(0, count($this->infoCardDB->getAllInfoCards(1, 4)));
    }

    /**
     * @covers DB\CDBInfoCard::getAllTypeInfoCardsPageByImport()
     */
    public function testGetAllTypeInfoCardsPageByImport()
    {
        $this->assertEquals(0, count($this->infoCardDB->getAllTypeInfoCardsPageByImport(1, 1, 1)));
    }

    /**
     * 展示指定的名片
     * @covers DB\CDBInfoCard::showInfoCardById()
     */
    public function testShowInfoCardById()
    {
        $this->assertEquals(1, $this->infoCardDB->showInfoCardById(1));
        $this->assertEquals(0, $this->infoCardDB->showInfoCardById(11));
    }

    /**
     * 获取指定的名片的部分信息
     * @covers DB\CDBInfoCard::showInfoCardById()
     * @covers DB\CDBInfoCard::getSectionInfoFromCard()
     */
    public function testGetSectionInfoFromCard()
    {
        $array = [
            'userId' => '1',
            'name' => 'jiangpengfei',
            'nameEn' => 'jack',
            'company' => '1',
            'companyEn' => '1',
            'email' => '123@qq.com',
            'companyProfile' => '1',
            'companyUrl' => '123.com',
            'bankAccount' => '1234567890',
            'longitude' => '10',
            'latitude' => '10',
            'type' => '0',
            'photo' => '1',
            'backgroundImg' => '1',
            'backgroundColor' => '1',
            'backgroundType' => '0',
            'countryName' => 'china',
            'provinceName' => 'anhui',
            'cityName' => 'chaohu',
            'areaName' => 'juchaoqu',
            'townName' => 'asda',
            'address' => '123123',
            'gender' => '1',
            'phone' => '112312312312',
            'jobs' => '[{"startTime":"2017-09-09","endTime":"2017-10-10","job":"develop"},{"startTime":" 2017-09-09","endTime":"2017-10-10","job":"develop"}]',
            'position' => 'work',
            'taxNumber' => 111111111,
            'chatPermission' => '1',
            'chatCost' => '0',
            'socialCircleId' => '1',
            'openScope' => '0'

        ];
        $this->assertEquals(1, $this->infoCardDB->showInfoCardById(1));
        $this->assertEquals($array, $this->infoCardDB->getSectionInfoFromCard(1));
        $this->assertNull($this->infoCardDB->getSectionInfoFromCard(12));
    }

    /**
     * 根据identity获取名片信息
     * @covers DB\CDBInfoCard::getInfoCardByIdentity()
     */
    public function testGetInfoCardByIdentity()
    {
        $array = [
            'id' => '1',
            'userId' => '1',
            'remark' => 'my info card',
            'name' => 'jiangpengfei',
            'nameEn' => 'jack',
            'company' => '1',
            'companyEn' => '1',
            'email' => '123@qq.com',
            'companyProfile' => '1',
            'companyUrl' => '123.com',
            'bankAccount' => '1234567890',
            'longitude' => '10',
            'latitude' => '10',
            'type' => '0',
            'photo' => '1',
            'backgroundImg' => '1',
            'backgroundColor' => '1',
            'backgroundType' => '0',
            'countryName' => 'china',
            'provinceName' => 'anhui',
            'cityName' => 'chaohu',
            'areaName' => 'juchaoqu',
            'townName' => 'asda',
            'address' => '123123',
            'gender' => '1',
            'phone' => '112312312312',
            'birthday' => '2017-09-09',
            'bloodType' => '1',
            'qq' => '78768127381',
            'skills' => '["eat","sleep","fishing","haha"]',
            'favorites' => '["1","2","3","4"]',
            'jobs' => '[{"startTime":"2017-09-09","endTime":"2017-10-10","job":"develop"},{"startTime":" 2017-09-09","endTime":"2017-10-10","job":"develop"}]',
            'position' => 'work',
            'taxNumber' => 111111111,
            'isDefault'=>0,
            'chatPermission' => '1',
            'chatCost' => '0',
            'socialCircleId' => '1',
            'openScope' => '0'

        ];
        $this->assertEquals($array, $this->infoCardDB->getInfoCardByIdentity('f1e6e8812aa0aa76acd7704761c4041d'));
        $this->assertNull($this->infoCardDB->getInfoCardByIdentity('812aa0aa76acd7704761c4041d'));
    }

    /**
     * 接收名片
     * @covers DB\CDBInfoCard::receiveInfoCardById()
     */
    public function testReceiveInfoCardById()
    {
        $initCount = $this->getConnection()->getRowCount('gener_receive_info_card');
        $this->assertGreaterThan(1, $this->infoCardDB->receiveInfoCardById(1, 2));
        $this->assertEquals($initCount + 1, $this->getConnection()->getRowCount('gener_receive_info_card'));
    }

    /**
     * 判断名片是否已经接收
     * @covers DB\CDBInfoCard::existsCardIdOnReceive()
     */
    public function testExistsCardIdOnReceive()
    {
        $this->assertTrue($this->infoCardDB->existsCardIdOnReceive(1, 1));
        $this->assertFalse($this->infoCardDB->existsCardIdOnReceive(11, 1));
    }

    /**
     * 获取所有从通讯录得到的名片
     * @covers DB\CDBInfoCard::getAllInfoCardFromAddressList()
     */
    public function testGetAllInfoCardFromAddressList()
    {

        $this->assertEquals(0, count($this->infoCardDB->getAllInfoCardFromAddressList(1)));

    }

    /**
     * 通通讯录中获取手机号和名字
     * @covers DB\CDBInfoCard::addInfoCardByAddressList()
     * @covers DB\CDBInfoCard::getPhoneAndNameFromAddressBook()
     */
    public function testGetPhoneAndNameFromAddressBook()
    {
        $infoCard = new InfoCard();
        $infoCard->remark = 'my addressBook card';
        $infoCard->userId = 2;
        $infoCard->name = 'xuliulei';
        $infoCard->phone = '13778787889';
        $infoCard->skills = json_encode(array());
        $infoCard->favorites = json_encode(array());
        $infoCard->jobs = json_encode(array());
        $infoCard->createBy = 2;
        $infoCard->updateBy = 2;
        $this->assertGreaterThan(1, $this->infoCardDB->addInfoCardByAddressList($infoCard));

        $array = [
            ['name' => 'xuliulei',
                'phone' => '13778787889']
        ];
        $this->assertEquals($array, $this->infoCardDB->getPhoneAndNameFromAddressBook(2));
    }

    /**
     * 分页获取接收到的名片
     * @covers DB\CDBInfoCard::getReceiveInfoCardPaging()
     */
    public function testGetReceiveInfoCardPaging()
    {
        $array = [[
            'id' => '1',
            'userId' => '1',
            'familyId' => '0',
            'personId' => '0',
            'socialCircleId'=>'1',
            'permission' => '0',
            'identity' => 'f1e6e8812aa0aa76acd7704761c4041d',
            'remark' => 'my info card',
            'name' => 'jiangpengfei',
            'nameEn' => 'jack',
            'company' => '1',
            'companyEn' => '1',
            'email' => '123@qq.com',
            'companyProfile' => '1',
            'companyUrl' => '123.com',
            'bankAccount' => '1234567890',
            'longitude' => '10',
            'latitude' => '10',
            'type' => '0',
            'birthday' => '2017-09-09',
            'photo' => '1',
            'backgroundImg' => '1',
            'backgroundColor' => '1',
            'backgroundType' => '0',
            'countryName' => 'china',
            'provinceName' => 'anhui',
            'cityName' => 'chaohu',
            'areaName' => 'juchaoqu',
            'townName' => 'asda',
            'address' => '123123',
            'gender' => '1',
            'phone' => '112312312312',
            'tag' => '',
            'jobs' => '[{"startTime":"2017-09-09","endTime":"2017-10-10","job":"develop"},{"startTime":" 2017-09-09","endTime":"2017-10-10","job":"develop"}]',
            'position' => 'work',
            'taxNumber' => '111111111',
            'chatPermission' => '1',
            'chatCost' => '0',
            'openScope' => '0'
        ]];
        $this->assertEquals($array, $this->infoCardDB->getReceiveInfoCardPaging(1, 1, 1));
    }

    /**
     * 获取所有接收到的名片
     * @covers DB\CDBInfoCard::getAllReceiveInfoCard()
     */
    public function testGetAllReceiveInfoCard()
    {
        $array = [[
            'id' => '1',
            'userId' => '1',
            'familyId' => '0',
            'personId' => '0',
            'socialCircleId'=>'1',
            'permission' => '0',
            'identity' => 'f1e6e8812aa0aa76acd7704761c4041d',
            'remark' => 'my info card',
            'name' => 'jiangpengfei',
            'nameEn' => 'jack',
            'company' => '1',
            'companyEn' => '1',
            'email' => '123@qq.com',
            'companyProfile' => '1',
            'companyUrl' => '123.com',
            'bankAccount' => '1234567890',
            'longitude' => '10',
            'latitude' => '10',
            'type' => '0',
            'photo' => '1',
            'backgroundImg' => '1',
            'backgroundColor' => '1',
            'backgroundType' => '0',
            'countryName' => 'china',
            'provinceName' => 'anhui',
            'cityName' => 'chaohu',
            'areaName' => 'juchaoqu',
            'townName' => 'asda',
            'address' => '123123',
            'gender' => '1',
            'birthday' => '2017-09-09',
            'phone' => '112312312312',
            'jobs' => '[{"startTime":"2017-09-09","endTime":"2017-10-10","job":"develop"},{"startTime":" 2017-09-09","endTime":"2017-10-10","job":"develop"}]',
            'position' => 'work',
            'tag' => '',
            'taxNumber' => '111111111',
            'chatPermission' => '1',
            'chatCost' => '0',
            'isShow' => '0',
            'isDefault' => '0',
            'openScope' => '0'
        ]];
        $this->assertEquals($array, $this->infoCardDB->getAllReceiveInfoCard(1));
    }

    /**
     * 接收的名片的总数
     * @covers DB\CDBInfoCard::countReceiveInfoCard()
     */
    public function testCountReceiveInfoCard()
    {
        $this->assertEquals(1, $this->infoCardDB->countReceiveInfoCard(1));
    }

    /**
     * @covers DB\CDBInfoCard::getCreateByInfoCardId
     */
    public function testGetCreateByInfoCardId()
    {
        $array = [[
            'createBy' => 1
        ]];
        $this->assertEquals($array, $this->infoCardDB->getCreateByInfoCardId(1));
    }

    /**
     * @covers DB\CDBInfoCard::verifyUserIdInReceive
     */
    public function testVerifyUserIdInReceive()
    {
        $this->assertTrue($this->infoCardDB->verifyUserIdInReceive(1, 1));
        $this->assertFalse($this->infoCardDB->verifyUserIdInReceive(1, 2));
    }

    /**
     * 删除接收的名片
     * @covers DB\CDBInfoCard::deleteReceiveInfoCard()
     */
    public function testDeleteReceiveInfoCard()
    {
        $this->assertEquals(1, $this->infoCardDB->deleteReceiveInfoCard(1, 1));
    }

    /**
     * 指定展示的名片
     * @covers DB\CDBInfoCard::updateIsShowForMyInfoCard()
     */
    public function testUpdateIsShowForMyInfoCard()
    {
        $this->assertEquals(1, $this->infoCardDB->updateIsShowForMyInfoCard(1, 1));
    }

    /**
     * 检查人物id是否存在
     * @covers DB\CDBInfoCard::verifyPersonIdExists()
     */
    public function testVerifyPersonIdExists()
    {
        $this->assertFalse(false, $this->infoCardDB->verifyPersonIdExists(1, 1));
    }

    /**
     * 更新导入的名片
     * @covers DB\CDBInfoCard::updateImportInfoCard()
     */
    public function testUpdateImportInfoCard()
    {
        $infoCard = new InfoCard();

        $infoCard->remark = 'my info card';
        $infoCard->userId = 1;
        $infoCard->familyId = 1;
        $infoCard->personId = 1;
        $infoCard->socialCircleId = 1;
        $infoCard->name = 'jiangpengfei';
        $infoCard->country = '1';
        $infoCard->countryName = 'china';
        $infoCard->province = '1';
        $infoCard->provinceName = 'anhui';
        $infoCard->city = '1';
        $infoCard->cityName = 'chaohu';
        $infoCard->area = '1';
        $infoCard->areaName = 'juchaoqu';
        $infoCard->town = '1';
        $infoCard->townName = '131231';
        $infoCard->address = '123123';
        $infoCard->gender = '1';
        $infoCard->birthday = '2018-09-09';
        $infoCard->bloodType = '1';
        $infoCard->maritalStatus = '1';
        $infoCard->phone = '11231231231';
        $infoCard->qq = '781684123';
        $infoCard->updateBy = '1';
        $this->assertEquals(0, $this->infoCardDB->updateImportInfoCard($infoCard));
    }

    /**
     * 导入的名片的总数
     * @covers DB\CDBInfoCard::countImportInfoCards()
     */
    public function testCountImportInfoCards()
    {
        $this->assertEquals(0, $this->infoCardDB->countImportInfoCards(1, 1));
    }

    /**
     * 分页获取导入的名片
     * @covers DB\CDBInfoCard::addInfoCard()
     * @covers DB\CDBInfoCard::getImportInfoCardsPaging()
     * @covers DB\CDBInfoCard::getAllImportInfoCards()
     * @covers DB\CDBInfoCard::getImportFamilyList()
     */
    public function testGetImportInfoCardsPaging()
    {
        $infoCard = new InfoCard();

        $infoCard->remark = 'my info card';
        $infoCard->userId = 1;
        $infoCard->familyId = 1;
        $infoCard->personId = 1;
        $infoCard->socialCircleId = 1;
        $infoCard->name = 'jiangpengfei';
        $infoCard->nameEn = 'jack';
        $infoCard->country = '1';
        $infoCard->countryName = 'china';
        $infoCard->province = '1';
        $infoCard->provinceName = 'anhui';
        $infoCard->city = '1';
        $infoCard->cityName = 'chaohu';
        $infoCard->area = '1';
        $infoCard->areaName = 'juchaoqu';
        $infoCard->town = '1';
        $infoCard->townName = '131231';
        $infoCard->address = '123123';
        $infoCard->photo = '1';
        $infoCard->backgroundImg = '1';
        $infoCard->backgroundColor = '1';
        $infoCard->backgroundType = '0';
        $infoCard->company = '1';
        $infoCard->companyEn = '1';
        $infoCard->gender = '1';
        $infoCard->birthday = '2018-09-09';
        $infoCard->bloodType = '1';
        $infoCard->maritalStatus = '1';
        $infoCard->phone = '11231231231';
        $infoCard->qq = '781684123';
        $infoCard->email = '123@qq.com';
        $infoCard->companyProfile = '1';
        $infoCard->companyUrl = '123.com';
        $infoCard->bankAccount = '1234567890';
        $infoCard->longitude = 10;
        $infoCard->latitude = 10;
        $infoCard->skills = '';
        $infoCard->favorites = '';
        $infoCard->jobs = '';
        $infoCard->type = '2';
        $infoCard->identity = '';
        $infoCard->position = '';
        $infoCard->createBy = '1';
        $infoCard->updateBy = '1';
        $infoCard->isDefault = 0;
        $this->assertGreaterThan(1, $this->infoCardDB->addInfoCard($infoCard));

        $array = [[
            'id' => '3',
            'remark' => 'my info card',
            'userId' => '1',
            'familyId' => '1',
            'personId' => '1',
            'socialCircleId'=>'0',
            'name' => 'jiangpengfei',
            'nameEn' => 'jack',
            'company' => '1',
            'companyEn' => '1',
            'email' => '123@qq.com',
            'companyProfile' => '1',
            'companyUrl' => '123.com',
            'bankAccount' => '1234567890',
            'longitude' => '10',
            'latitude' => '10',
            'country' => '1',
            'countryName' => 'china',
            'province' => '1',
            'provinceName' => 'anhui',
            'city' => '1',
            'cityName' => 'chaohu',
            'area' => '1',
            'areaName' => 'juchaoqu',
            'town' => '1',
            'townName' => '131231',
            'address' => '123123',
            'photo' => '1',
            'backgroundImg' => '1',
            'backgroundColor' => '1',
            'backgroundType' => '0',
            'gender' => '1',
            'birthday' => '2018-09-09',
            'bloodType' => '1',
            'maritalStatus' => '1',
            'phone' => '11231231231',
            'qq' => '781684123',
            'type' => '2',
            'skills' => '',
            'favorites' => '',
            'jobs' => '',
            'position' => '',
            'identity' => '',
            'taxNumber' => '',
            'createBy' => '1',
            'updateBy' => '1',
            'chatPermission' => '1',
            'chatCost' => '0',
            'isShow' => '0',
            'isDefault' => '0',
            'openScope' => '0',
            'permission' => '1'
        ]];
        $result = $this->infoCardDB->getImportInfoCardsPaging(1, 1, 1, 1);
        unset($result[0]['createTime']);
        unset($result[0]['updateTime']);
        $result1 = $this->infoCardDB->getAllImportInfoCards(1, 1);
        unset($result1[0]['createTime']);
        unset($result1[0]['updateTime']);
        $this->assertEquals($array, $result);
        $this->assertEquals($array, $result1);
        $array1 = [[
            'familyId' => '1',
            'type' => '2',
            'name' => 'jiazu',
            'surname' => 'jia',
            'description' => 'description',
            'photo' => 'http://test.photo'
        ]];
        $this->assertEquals($array1, $this->infoCardDB->getImportFamilyList(1));
    }

    /**
     * 更改背景的类型
     * @covers DB\CDBInfoCard::changeBackgroundType()
     */
    public function testChangeBackgroundType()
    {
        $this->assertEquals(1, $this->infoCardDB->changeBackgroundType(2, 2, 1, 1));
    }

    /**
     * 更改用户id
     * @covers DB\CDBInfoCard::changeUserId()
     */
    public function testChangeUserId()
    {
        $this->assertEquals(5, $this->infoCardDB->changeUserId(1, 3));
        $this->assertEquals(5, $this->infoCardDB->changeUserIdWithoutTransaction(3, 1));
    }

    /**
     * @covers DB\CDBInfoCard::changeUserIdWithoutTransaction
     */
    public function testChangeUserIdWithoutTransaction()
    {
        $this->assertEquals(5, $this->infoCardDB->changeUserIdWithoutTransaction(1, 2));
    }

    /**
     * 添加公司
     * @covers DB\CDBInfoCard::addCompany()
     */
    public function testAddCompany()
    {
        $infoCard = new InfoCard();
        $infoCard->id = 2;
        $infoCard->company = 1;
        $infoCard->companyEn = 1;
        $infoCard->companyProfile = 1;
        $infoCard->companyUrl = '123.com';
        $infoCard->bankAccount = '1234567890';
        $infoCard->createBy = 1;
        $infoCard->updateBy = 1;
        $this->assertEquals(2, $this->infoCardDB->addCompany($infoCard));
    }

    /**
     * 通过id获取另外的公司信息、
     * @covers DB\CDBInfoCard::getOtherCompanyById()
     */
    public function testGetOtherCompanyById()
    {
        $array = [[
            "id" => "1",
            "company" => "1",
            "companyEn" => "1",
            "companyProfile" => "1",
            "companyUrl" => "123.com",
            "bankAccount" => "1234567890",
            "createBy" => "1",
            "createTime" => "2017-09-09 09:09:09",
            "updateBy" => "1",
            "updateTime" => "2017-09-09 09:09:09"
        ]];
        $this->assertEquals($array, $this->infoCardDB->getOtherCompanyById(1));
    }

    /**
     * 根据id删除另外的公司信息
     * @covers DB\CDBInfoCard::delOtherCompanyById()
     */
    public function testDelOtherCompanyById()
    {
        $this->assertEquals(1, $this->infoCardDB->delOtherCompanyById(1));
    }

    /**
     * 获取公司信息通过公司id
     * @covers DB\CDBInfoCard::getCompanyByOtherCompanyId()
     */
    public function testGetCompanyByOtherCompanyId()
    {
        $array = [
            "id" => "1",
            "company" => "1",
            "companyEn" => "1",
            "companyProfile" => "1",
            "companyUrl" => "123.com",
            "bankAccount" => "1234567890",
            "createBy" => "1",
            "createTime" => "2017-09-09 09:09:09",
            "updateBy" => "1",
            "updateTime" => "2017-09-09 09:09:09"
        ];
        $this->assertEquals($array, $this->infoCardDB->getCompanyByOtherCompanyId(1));
    }

    /**
     * 编辑公司通过公司id
     * @covers DB\CDBInfoCard::editOtherCompanyByCompanyId()
     */
    public function testEditOtherCompanyByCompanyId()
    {
        $infoCard = new InfoCard();
        $infoCard->company = 111;
        $infoCard->companyEn = 1;
        $infoCard->companyProfile = 1;
        $infoCard->companyUrl = '123.com';
        $infoCard->bankAccount = '1234567890';
        $infoCard->updateBy = 2;
        $infoCard->updateTime = '2018-05-16 01:01:01';
        $this->assertEquals(1, $this->infoCardDB->editOtherCompanyByCompanyId($infoCard, 1));
    }

    /**
     * 添加微官网
     * @covers DB\CDBInfoCard::addCompanyMinHomepage()
     */
    public function testAddCompanyMinHomepage()
    {
        $this->assertEquals(2, $this->infoCardDB->addCompanyMinHomepage(1, 1, 1, 1));
    }

    /**
     * 通过id删除微官网
     * @covers DB\CDBInfoCard::delCompanyMinHomepageById()
     */
    public function testDelCompanyMinHomepageById()
    {
        $this->assertEquals(1, $this->infoCardDB->delCompanyMinHomepageById(1));
        $this->assertEquals(null, $this->infoCardDB->delCompanyMinHomepageById(2));
    }

    /**
     * 编辑微官网
     * @covers DB\CDBInfoCard::editCompanyMinHomepageById()
     */
    public function testEditCompanyMinHomepageById()
    {
        $this->assertEquals(1, $this->infoCardDB->editCompanyMinHomepageById(1, 2, 2, 1));
    }

    /**
     * 获取所有的微官网列表
     * @covers DB\CDBInfoCard::getCompanyMinHomepagesByInfoCardId()
     */
    public function testGetCompanyMinHomepagesByInfoCardId()
    {
        $array = [[
            'id' => 1,
            'imgUrl' => '1',
            'imgProfile' => '1',
            'createBy' => 1,
            'createTime' => '2017-09-09 09:09:09',
            'updateBy' => 1,
            'updateTime' => '2017-09-09 09:09:09'
        ]];
        $this->assertEquals($array, $this->infoCardDB->getCompanyMinHomepagesByInfoCardId(1));
    }

    /**
     * 通过id获取微官网
     * @covers DB\CDBInfoCard::getCompanyMinHomepageById()
     */
    public function testGetCompanyMinHomepageById()
    {
        $array = [
            'id' => 1,
            'infoCardId' => 1,
            'imgUrl' => '1',
            'imgProfile' => '1',
            'createBy' => 1,
            'createTime' => '2017-09-09 09:09:09',
            'updateBy' => 1,
            'updateTime' => '2017-09-09 09:09:09'
        ];
        $this->assertEquals($array, $this->infoCardDB->getCompanyMinHomepageById(1));
    }

    /**
     * 更改转发权限
     * @covers DB\CDBInfoCard::updateInfoCardPermission
     */
    public function testUpdateInfoCardPermission()
    {
        $this->assertEquals(1, $this->infoCardDB->updateInfoCardPermission(1, 1));
    }

    /**
     * 测试添加转发记录
     * @covers DB\CDBInfoCard::addShareRecord
     */
    public function testAddShareRecord()
    {
        $record = new InfoCardShareRecord();
        $record->shareTime = '2018-09-09 09:09:09';
        $record->shareUserId = 1;
        $record->shareCardId = 1;
        $record->ownerUserId = 2;
        $record->targetUserId = 3;

        $this->assertGreaterThan(0, $this->infoCardDB->addShareRecord($record));
    }

    /**
     * 测试获取分享记录的一页数据
     * @covers DB\CDBInfoCard::getShareRecordPaging
     */
    public function testGetShareRecordPaging()
    {
        $this->assertEquals(1, count($this->infoCardDB->getShareRecordPaging(1, 1, 10)));
    }

    /**
     * @covers DB\CDBInfoCard::verifyIdentity()
     */
    public function testVerifyIdentity()
    {
        $this->assertTrue(true, $this->infoCardDB->verifyIdentity('f1e6e8812aa0aa76acd7704761c4041d'));
        $this->assertFalse($this->infoCardDB->verifyIdentity('f1ssssse6e8ss76acd7704761c4041d'));
    }

    /**
     * @covers DB\CDBInfoCard::UpdateHomepageImageOrder
     */
    public function testUpdateHomepageImageOrder()
    {
        $order = '1,3,4';
        $this->assertEquals(1, $this->infoCardDB->UpdateHomepageImageOrder(1, $order));
    }

    /**
     * @covers DB\CDBInfoCard::checkInfoCardIdExistsByUserId
     */
    public function testCheckInfoCardIdExistsByUserId()
    {
        $this->assertTrue($this->infoCardDB->checkInfoCardIdExistsByUserId(1));
        $this->assertFalse($this->infoCardDB->checkInfoCardIdExistsByUserId(11));
    }

    /**
     * @covers DB\CDBInfoCard::updateIsDefaultForMyInfoCard
     */
    public function testUpdateIsDefaultForMyInfoCard()
    {
        $this->assertEquals(1, $this->infoCardDB->updateIsDefaultForMyInfoCard(1, 1));
    }

    /**
     * @covers DB\CDBInfoCard::createInfoCardExchange
     */
    public function testCreateInfoCardExchange()
    {
        $infoCardExchange = new InfoCardExchange();
        $infoCardExchange->userId = 1;
        $infoCardExchange->title = 'group';
        $infoCardExchange->code = '123456';
        $infoCardExchange->password = '1';

        $this->assertGreaterThan(0, $this->infoCardDB->createInfoCardExchange($infoCardExchange, 1));
    }

    /**
     * @covers DB\CDBInfoCard::joinInfoCardExchange
     */
    public function testJoinInfoCardExchange()
    {
        $exchangeId = 1;
        $userId = 1;
        $cardId = 1;
        $this->assertGreaterThan(0, $this->infoCardDB->joinInfoCardExchange($exchangeId, $userId, $cardId));
    }

    /**
     * @covers DB\CDBInfoCard::getInfocardExchange
     */
    public function testGetInfocardExchange()
    {
        $exchange = [
            "id" => '1',
            "userId" => '4',
            "title" => 'group exchange',
            'code' => '123456',
            "isFinish" => '0',
            "isDelete" => '0',
            'total' => '0',
            'password' => '',
            "createBy" => '4',
            "createTime" => '2017-09-09 09:09:09',
            "updateBy" => '4',
            "updateTime" => '2017-09-09 09:09:09'
        ];

        $this->assertEquals($exchange, $this->infoCardDB->getInfocardExchange(1));
    }

    /**
     * @covers DB\CDBInfoCard::getUserInfoCardExchange
     */
    public function testGetUserInfoCardExchange() {

        $this->assertGreaterThan(0, count($this->infoCardDB->getUserJoinInfoCardExchange(1,10,1)));
    }

    /**
     * @covers DB\CDBInfoCard::getInfoCardExchangeByCode
     */
    public function testGetInfoCardExchangeByCode() {

        $this->assertGreaterThan(0, count($this->infoCardDB->getInfoCardExchangeByCode('123456')));
    }

    /**
     * @covers DB\CDBInfoCard::getJoinExchangeRecords
     */
    public function testGetJoinExchangeRecords() {

        $this->assertGreaterThan(0, count($this->infoCardDB->getJoinExchangeRecords(1)));
    }

    /**
     * @covers DB\CDBInfoCard::checkUserExistInExchange
     */
    public function testCheckUserExistInExchange() {
        $this->assertTrue($this->infoCardDB->checkUserExistInExchange(1,1));
        $this->assertFalse($this->infoCardDB->checkUserExistInExchange(1,20));
    }

    /**
     * @covers DB\CDBInfoCard::endInfoCardExchange
     */
    public function testEndInfoCardExchange() {
        $this->assertTrue($this->infoCardDB->endInfoCardExchange(1));
    }

    /**
     * @covers DB\CDBInfoCard::getUserJoinInfoCardExchange
     */
    public function testGetUserJoinInfoCardExchange()
    {
        $this->assertGreaterThan(0, count($this->infoCardDB->getUserJoinInfoCardExchange(1,10,1)));
    }

    /**
     * @covers DB\CDBInfoCard::getInfoCardShareRecords
     */
    public function testGetInfoCardShareRecords()
    {
        $records = $this->infoCardDB->getInfoCardShareRecords(1);

        $this->assertEquals(1, count($records));
    }

    /**
     * @covers DB\CDBInfoCard::addInfoCardTag
     */
    public function testAddInfoCardTag()
    {
        $this->assertGreaterThan(0, $this->infoCardDB->addInfoCardTag('123123', 1));
    }

    /**
     * @covers DB\CDBInfoCard::updateInfoCardTagName
     */
    public function testUpdateInfoCardTagName()
    {
        $this->assertGreaterThan(0, $this->infoCardDB->updateInfoCardTagName(1, 'ddd', '1'));
    }

    /**
     * @covers DB\CDBInfoCard::getTagById
     */
    public function testGetTagById() {
        $tag = [
            'id' => '1',
            'userId' => '1',
            'tag' => 'work',
            'total' => '1',
            'createBy' => '1',
            'createTime' => '2017-09-09 09:09:09',
            'updateBy' => '1',
            'updateTime' => '2017-09-09 09:09:09'
        ];

        $this->assertEquals($tag, $this->infoCardDB->getTagById(1));
    }

    /**
     * @covers DB\CDBInfoCard::deleteInfoCardTag
     */
    public function testDeleteInfoCardTag()
    {
        $this->assertEquals(1, $this->infoCardDB->deleteInfoCardTag(1, 1));
    }

    /**
     * @covers DB\CDBInfoCard::getUserTagsTotal
     */
    public function testGetUserTagsTotal()
    {
        $this->assertEquals(1, $this->infoCardDB->getUserTagsTotal(1));
    }

    /**
     * @covers DB\CDBInfoCard::getUserTagsPaging
     */
    public function testGetUserTagsPaging()
    {
        $this->assertEquals(1, count($this->infoCardDB->getUserTagsPaging(1, 0, 0, 10)));
    }

    /**
     * @covers DB\CDBInfoCard::shareMultiInfoCards
     */
    public function testShareMultiInfoCards() {
        $this->assertGreaterThan(0, strlen($this->infoCardDB->shareMultiInfoCards("和嘎嘎嘎", [1, 2, 3], 1)));
    }

    /**
     * @covers DB\CDBInfoCard::getAllInfoCardsByTagId
     */
    public function testGetAllInfoCardsByTagId() {
        $this->assertGreaterThan(0, count($this->infoCardDB->getAllInfoCardsByTagId("1")));
    }

    /**
     * @covers DB\CDBInfoCard::getInfoCardsPagingByTagId
     */
    public function testGetInfoCardsPagingByTagId() {
        $this->assertGreaterThan(0, count($this->infoCardDB->getInfoCardsPagingByTagId("1", 0, 0, 10)));
    }

    /**
     * @covers DB\CDBInfoCard::addInfoCardsToTag
     */
    public function testAddInfoCardsToTag() {
        $this->assertGreaterThan(0, $this->infoCardDB->addInfoCardsToTag(["2"], 1, 1));
    }

    /**
     * @covers DB\CDBInfoCard::removeInfoCardFromTag
     */
    public function testRemoveInfoCardFromTag() {
        $this->assertGreaterThan(0, $this->infoCardDB->removeInfoCardFromTag(1, 1, 1));
    }

    /**
     * @covers DB\CDBInfoCard::getInfoCardsByBatchShareCode
     */
    public function testGetInfoCardsByBatchShareCode() {
        $this->assertGreaterThan(0, count($this->infoCardDB->getInfoCardsByBatchShareCode('123456')));
    }

    /**
     * @covers DB\CDBInfoCard::receiveInfoCardByBatchShareCode
     */
    public function testReceiveInfoCardByBatchShareCode() {
        $this->assertGreaterThan(0, $this->infoCardDB->receiveInfoCardByBatchShareCode('123456', 1));
    }


}
