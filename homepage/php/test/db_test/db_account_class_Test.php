<?php

use DB\CDBAccount;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use Model\User;
use Util\Util;

class DBAccountTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;

    private $conn = null;

    private $accountDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        $this->accountDB = new CDBAccount();
        return $this->createMySQLXMLDataSet('db_test/xml/account_dataset.xml');     //默认的数据集
    }


    /**
     * 测试用户注册
     * @covers DB\CDBAccount::userRegister
     * @covers DB\CDBAccount::register
     * @covers DB\CDBAccount::userLogin
     * @covers DB\CDBAccount::userLoginWithWechat
     * @return list user和wxUser的list
     */
    public function testUserRegisterAndLogin()
    {

        $initUserNum = $this->getConnection()->getRowCount('gener_user');
        $initProfileNum = $this->getConnection()->getRowCount('gener_user_profile');

        $user = new User();
        $user->username = "test_username";
        $user->email = "786185931@qq.com";
        $user->phone = "15000895132";
        $user->photo = "";
        $user->password = hash("sha256", "1234567" . $GLOBALS['SHA256_SALT'], false);
        $user->hxUsername = "test_hxusername";
        $user->hxPassword = "123456";


        $this->assertEquals($initUserNum, $this->getConnection()->getRowCount('gener_user'), "用户数据总数不对");
        $this->assertEquals($initProfileNum, $this->getConnection()->getRowCount('gener_user_profile'), "用户资料总数不对");

        //正常注册
        $this->assertGreaterThan(0, $this->accountDB->userRegister($user), "用户注册失败");
        $this->assertEquals($initUserNum + 1, $this->getConnection()->getRowCount('gener_user'), "用户数据总数不对");
        $this->assertEquals($initProfileNum + 1, $this->getConnection()->getRowCount('gener_user_profile'), "用户资料总数不对");

        //正常注册的账号可以用来登录
        $this->assertGreaterThan(0, $this->accountDB->userLogin($user->username, $user->password));

        //微信注册
        $wxUser = new User();
        $username = "jiangpengfei" . Util::generateRandomCode(8);
        $wxUser->username = $username;
        $wxUser->email = "jiangpengfei@xinhuotech.com";
        $wxUser->phone = "15000895232";
        $wxUser->password = hash("sha256", "1234567" . $GLOBALS['SHA256_SALT'], false);
        $wxUser->openid = "wxopenidididid";
        $wxUser->unionid = "unionIdididid";
        $wxUser->gender = "0";
        $wxUser->country = "1";
        $wxUser->province = "1";
        $wxUser->city = "1";

        $this->assertGreaterThan(0, $this->accountDB->userRegister($wxUser), "微信用户注册失败");
        $this->assertEquals($initUserNum + 2, $this->getConnection()->getRowCount('gener_user'), "用户数据总数不对");
        $this->assertEquals($initProfileNum + 2, $this->getConnection()->getRowCount('gener_user_profile'), "用户资料总数不对");

        //微信注册的账号可以用来登录
        $this->assertGreaterThan(0, $this->accountDB->userLoginWithWechat($wxUser->openid));  //微信openid登录成功

        //小程序注册
        $wxUser = new User();
        $username = "jiangpengfei" . Util::generateRandomCode(8);
        $wxUser->username = $username;
        $wxUser->email = "jiangpengfei@xinhuotech.com";
        $wxUser->phone = "15000895232";
        $wxUser->password = hash("sha256", "1234567" . $GLOBALS['SHA256_SALT'], false);
        $wxUser->miniOpenid = "wxopenidididid";
        $wxUser->unionId = "unionIdididid";
        $wxUser->gender = "0";
        $wxUser->country = "1";
        $wxUser->province = "1";
        $wxUser->city = "1";

        $this->assertGreaterThan(0, $this->accountDB->userRegister($wxUser), "小程序用户注册失败");
        $this->assertEquals($initUserNum + 3, $this->getConnection()->getRowCount('gener_user'), "小程序用户数据总数不对");
        $this->assertEquals($initProfileNum + 3, $this->getConnection()->getRowCount('gener_user_profile'), "小程序用户资料总数不对");

        //小程序注册的账号可以用来登录
        $this->assertGreaterThan(0, $this->accountDB->userLoginWithMini($wxUser->miniOpenid, 1));  //小程序unionId登录成功

    }

    /**
     * 测试用户登录
     * @covers DB\CDBAccount::userLogin
     */
    public function testUserLogin()
    {

        $this->assertEquals(-2, $this->accountDB->userLogin("jiang123", hash("sha256", "1234567" . $GLOBALS['SHA256_SALT'], false))); //用户名不存在的情况

        $this->assertEquals(-1, $this->accountDB->userLogin("jiang", hash("sha256", "123456" . $GLOBALS['SHA256_SALT'], false))); //密码不正确的情况

        $this->assertGreaterThan(0, $this->accountDB->userLogin("jiang", hash("sha256", "1234567" . $GLOBALS['SHA256_SALT'], false))); //正确的情况
    }

    /**
     * 测试用户通过微信登录
     * @covers DB\CDBAccount::userLoginWithWechat
     */
    public function testUserLoginWithWechat()
    {

        $this->assertFalse($this->accountDB->userLoginWithWechat("wxopenidididid1")); //微信openid不存在的情况

        $this->assertGreaterThan(0, $this->accountDB->userLoginWithWechat("openid"));  //微信openid登录成功
    }

    /**
     * @covers DB\CDBAccount::userLoginWithMini
     */
    public function testUserLoginWithMini()
    {
        $this->assertEquals(1, $this->accountDB->userLoginWithMini('miniOpenid', 1));
        $this->assertFalse($this->accountDB->userLoginWithMini('miniOpenid111', 1));
    }

    /**
     * @covers DB\CDBAccount::userLoginWithUnionId()
     */
    public function testUserLoginWithUnionId()
    {
        $this->assertEquals(1, $this->accountDB->userLoginWithUnionId('wx_unionId'));
        $this->assertFalse($this->accountDB->userLoginWithUnionId('wx_unionId111'));
    }

    /**
     * 测试检查用户登录名
     * @covers DB\CDBAccount::checkUserLoginName
     */
    public function testCheckUserLoginName()
    {

        /* 账号不存在的情况 */
        $this->assertFalse($this->accountDB->checkUserLoginName("jiang1"));

        $this->assertTrue($this->accountDB->checkUserLoginName("jiang"));
    }

    /**
     * 测试检查微信账号
     * @covers DB\CDBAccount::checkWechatAccountExist
     */
    public function testCheckWechatAccountExist()
    {

        /* 账号不存在的情况 */
        $this->assertFalse($this->accountDB->checkWechatAccountExist("openid1312asdasd"));

        /* 账号存在的情况 */
        $this->assertTrue($this->accountDB->checkWechatAccountExist("openid"));
    }

    /**
     * @covers DB\CDBAccount::userRegisterWithoutTransaction
     * @covers DB\CDBAccount::userRegisterInit
     * @covers DB\CDBAccount::addUserProfile
     */
    public function testUserRegisterWithoutTransaction()
    {
        $user = new User();
        $user->username = "test_username";
        $user->email = "786185931@qq.com";
        $user->phone = "15000895132";
        $user->photo = "";
        $user->password = hash("sha256", "1234567" . $GLOBALS['SHA256_SALT'], false);
        $user->hxUsername = "test_hxusername";
        $user->hxPassword = "123456";
        $this->assertEquals(4, $this->accountDB->userRegisterWithoutTransaction($user));
    }

    /**
     * @covers DB\CDBAccount::userRegisterWithShareCode
     */
    public function testUserRegisterWithShareCode()
    {
        $user = new User();
        $user->username = "test_username";
        $user->email = "786185931@qq.com";
        $user->phone = "15000895132";
        $user->photo = "";
        $user->password = hash("sha256", "1234567" . $GLOBALS['SHA256_SALT'], false);
        $user->hxUsername = "test_hxusername";
        $user->hxPassword = "123456";
        $this->assertEquals(4, $this->accountDB->userRegisterWithShareCode($user, 'sharecode', 0, 1));
    }

    /**
     * 测试根据用户id获取用户信息
     * @covers DB\CDBAccount::getUserInfoById
     */
    public function testGetUserInfoById()
    {

        /* 用户不存在 */
        $this->assertNull($this->accountDB->getUserInfoById(20));

        $userdata = array("id" => "1",
            "username" => "jiang",
            "hx_username" => "hx_username",
            "phone" => "15000895132",
            "email" => "786185931@qq.com",
            "nickname" => "jiang111",
            "photo" => "http://test.photo",
            "gender" => "1",
            "birthday" => "2017-09-09",
            "address" => "test address",
            'country' => '1',
            'province' => '1',
            'city' => '1',
            'area' => '1',
            'town' => '1',
            'countryName' => '0',
            'provinceName' => '0',
            'cityName' => '0',
            'areaName' => '0',
            'townName' => '0',
            'alias' => []
        );

        /* 用户存在 */
        $this->assertEquals($userdata, $this->accountDB->getUserInfoById(1));
    }

    /**
     * @covers DB\CDBAccount::getUserInfosByHxUsernames
     */
    public function testGetUserInfosByHxUsernames()
    {
        $array = [[
            'id' => "1",
            'username' => "jiang",
            'nickname' => "jiang111",
            'hxUsername' => "hx_username",
            'photo' => "http://test.photo",
            'gender' => "1",
            'birthday' => "2017-09-09",
            'country' => "1",
            'countryName' => "0",
            'province' => "1",
            'provinceName' => "0",
            'city' => "1",
            'cityName' => "0",
            'area' => "1",
            'areaName' => "0",
            'town' => "1",
            'townName' => "0",
            'address' => "test address"
        ]];
        $this->assertEquals($array, $this->accountDB->getUserInfosByHxUsernames(['hx_username']));
        $this->assertEquals(2, count($this->accountDB->getUserInfosByHxUsernames(['hx_username', 'hx_username2'])));
    }

    /**
     * 测试根据用户id获取环信信息
     * @covers DB\CDBAccount::getHuanxinInfoById
     */
    public function testGetHuanxinInfoById()
    {
        /* 用户不存在 */
        $this->assertNull($this->accountDB->getHuanxinInfoById(10));

        $hxData = array("id" => "1", "hxUsername" => "hx_username", "hxPassword" => "hxpwd");
        /* 用户存在 */
        $this->assertEquals($hxData, $this->accountDB->getHuanxinInfoById(1));
    }

    /**
     * 测试获取用户信息
     * @covers DB\CDBAccount::getUserInfo
     *
     */
    public function testGetUserInfo()
    {
        /* 用户不存在 */
        $this->assertNull($this->accountDB->getUserInfo(10));

        $userData = array("id" => "1",
            "user_type" => 1,
            "username" => "jiang",
            "nickname" => "jiang111",
            "phone" => "15000895132",
            "email" => "786185931@qq.com",
            "photo" => "http://test.photo",
            "gender" => "1",
            'country' => "1",
            "province" => "1",
            "city" => "1",
            "area" => "1",
            "town" => "1",
            "birthday" => "2017-09-09",
            "address" => "test address",
            "createTime" => "2017-09-09 09:09:09",
            "updateTime" => "2017-09-09 09:09:09",
            'countryName' => '0',
            'provinceName' => '0',
            'cityName' => '0',
            'areaName' => '0',
            'townName' => '0',
            'hx_username' => 'hx_username');
        $this->assertEquals($userData, $this->accountDB->getUserInfo(1));
    }

    /**
     * 测试获取用户资料
     * @covers DB\CDBAccount::getUserProfile
     */
    public function testGetUserProfile()
    {
        /* 用户不存在 */
        $this->assertNull($this->accountDB->getUserProfile(10));

        $profileData = array("postNum" => "0",
            "collectionNum" => "0",
            "familyNum" => "0",
            'usePrivateDB' => '0');
        /* 用户存在 */
        $this->assertEquals($profileData, $this->accountDB->getUserProfile(1));
    }

    /**
     * 测试用户id存在
     * @covers DB\CDBAccount::isUserIdExist
     */
    public function testIsUserIdExist()
    {
        /* 用户id不存在 */
        $this->assertFalse($this->accountDB->isUserIdExist(10));

        /* 用户id存在 */
        $this->assertTrue($this->accountDB->isUserIdExist(1));
    }

    /**
     * 测试更新用户信息
     * @covers DB\CDBAccount::updateUserInfo
     */
    public function testUpdateUserInfo()
    {
        //int $gender, $birthday, int $country, string $countryName, int $province, string $provinceName, int $city, string $cityName, int $area, string $areaName, int $town, string $townName, string $address, int $userId
        /* 测试用户生日传null的情况 */
        $this->assertEquals(1, $this->accountDB->updateUserInfo(0, null, 2, "国家", 2, "省份", 2, "城市", 2, "区", 2, "镇", "具体地址", 1));

        /* 测试用户生日正常的情况 */
        $this->assertEquals(1, $this->accountDB->updateUserInfo(0, "2017-09-09", 2, "国家", 2, "省份", 2, "城市", 2, "区", 2, "镇", "具体地址", 1));
    }

    /**
     * 测试更新手机
     * @covers DB\CDBAccount::updatePhone
     */
    public function testUpdatePhone()
    {
        /* 改动了的情况 */
        $this->assertEquals(1, $this->accountDB->updatePhone("1500895133", 1));
    }

    /**
     * @covers DB\CDBAccount::updatePhoneAndPassword
     */
    public function testUpdatePhoneAndPassword()
    {
        $this->assertEquals(1, $this->accountDB->updatePhoneAndPassword(123345678, '1111111', 1));
    }

    /**
     * 测试更新用户昵称
     * @covers DB\CDBAccount::updateNickname
     */
    public function testUpdateNickname()
    {
        /* 改动了的情况 */
        $this->assertEquals(1, $this->accountDB->updateNickname("jiang1111", 1));
    }

    /**
     * 测试更新用户头像
     * @covers DB\CDBAccount::addPhotoforUser
     */
    public function testAddPhotoforUser()
    {
        /* 改动了的情况 */
        $this->assertEquals(1, $this->accountDB->addPhotoforUser(1, "http://test.photo1"));
    }

    /**
     * 测试验证指定设备下的用户id和token
     * @covers DB\CDBAccount::verifyUserIdAndTokenWithDevice
     */
    public function testVerifyUserIdAndTokenWithDevice()
    {
        /* 微信登录成功 */
        $this->assertTrue($this->accountDB->verifyUserIdAndTokenWithDevice(1, "123456323", 3));
        /* 微信登录失败 */
        $this->assertFalse($this->accountDB->verifyUserIdAndTokenWithDevice(1, "1234567", 3));

        /* app登录成功 */
        $this->assertTrue($this->accountDB->verifyUserIdAndTokenWithDevice(1, "1234563223", 2, "1234567"));
        /* app登录失败 */
        $this->assertFalse($this->accountDB->verifyUserIdAndTokenWithDevice(1, "1234567", 2, "1234567"));

        /* 网页登录成功 */
        $this->assertTrue($this->accountDB->verifyUserIdAndTokenWithDevice(1, "123456", 1));
        /* 网页登录失败 */
        $this->assertFalse($this->accountDB->verifyUserIdAndTokenWithDevice(1, "1234567", 1));
    }

    /**
     * 测试通过邮箱获取用户id
     * @covers DB\CDBAccount::getUserIdByEmail
     */
    public function testGetUserIdByEmail()
    {
        /* 用户不存在的情况 */
        $this->assertNull($this->accountDB->getUserIdByEmail("7861859311@qq.com"));

        /* 用户存在的情况 */
        $this->assertEquals(1, $this->accountDB->getUserIdByEmail("786185931@qq.com"));
    }

    /**
     * 测试根据用户名获取用户id
     * @covers DB\CDBAccount::getUserIdByUserName
     */
    public function testGetUserIdByUserName()
    {
        /* 用户不存在的情况 */
        $this->assertNull($this->accountDB->getUserIdByUserName("jiang1"));

        /* 用户存在的情况 */
        $this->assertEquals(1, $this->accountDB->getUserIdByUserName("jiang"));
    }

    /**
     * 测试根据手机号获取用户id
     * @covers DB\CDBAccount::getUserIdByPhone
     */
    public function testGetUserIdByPhone()
    {
        /* 用户不存在的情况 */
        $this->assertNull($this->accountDB->getUserIdByPhone("15000878781"));

        /* 用户存在的情况 */
        $this->assertEquals(1, $this->accountDB->getUserIdByPhone("15000895132"));
    }

    /**
     * 测试根据登录名获取userId
     * @covers DB\CDBAccount::getUserIdByLoginName
     */
    public function testGetUserIdByLoginName()
    {
        /* 用户不存在的情况 */
        $this->assertNull($this->accountDB->getUserIdByLoginName("150008"));

        /* 用户存在的情况-用户名 */
        $this->assertEquals(1, $this->accountDB->getUserIdByLoginName("jiang"));

        /* 用户存在的情况-手机号 */
        $this->assertEquals(1, $this->accountDB->getUserIdByLoginName("15000895132"));

        /* 用户存在的情况-邮箱 */
        $this->assertEquals(1, $this->accountDB->getUserIdByLoginName("786185931@qq.com"));
    }

    /**
     * 测试获取登录次数
     * @covers DB\CDBAccount::getLoginTimes
     */
    public function testGetLoginTimes()
    {
        $this->assertEquals(0,$this->accountDB->getLoginTimes(11));
        $this->assertEquals(9, $this->accountDB->getLoginTimes(1));
    }

    /**
     * 测试获取登录历史
     * @covers DB\CDBAccount::getLoginHistory
     */
    public function testGetLoginHistory()
    {
        /* 用户不存在 */
        $this->assertCount(0, $this->accountDB->getLoginHistory(11, 2, 3));

        /* 用户存在,数据全拿 */
        $this->assertCount(3, $this->accountDB->getLoginHistory(1, 2, 3));

        /* 用户存在,数据只有部分 */
        $this->assertCount(1, $this->accountDB->getLoginHistory(1, 3, 4));

        /* 用户存在，没有数据 */
        $this->assertCount(0, $this->accountDB->getLoginHistory(1, 2, 9));
    }

    /**
     * 测试插入登录历史
     * @covers DB\CDBAccount::insertLoginHistory
     */
    public function testInsertLoginHistory()
    {

        $this->assertGreaterThan(0, $this->accountDB->insertLoginHistory("jiang", 1, 1, "1234565", "192.168.3.51", "2018-09-09", "1"));
        $this->assertNotNull($this->accountDB->insertLoginHistory("jiang", 1, 1, "1234565", "192.168.3.51", "2018-09-09", "1", '', 'system_url'));
        $this->assertEquals(11, $this->getConnection()->getRowCount("gener_loginHistory"));
    }

    /**
     * 测试更新用户密码
     * @covers DB\CDBAccount::updateUserPassword
     */
    public function testUpdateUserPassword()
    {
        /* 更新有变化 */
        $this->assertEquals(1, $this->accountDB->updateUserPassword(1, hash("sha256", "123456" . $GLOBALS['SHA256_SALT'], false)));
    }

    /**
     * 测试绑定用户手机
     * @covers DB\CDBAccount::bindUserPhone
     */
    public function testBindUserPhone()
    {
        /* 更新有变化 */
        $this->assertTrue($this->accountDB->bindUserPhone(1, "15000895133"));
        $this->assertFalse($this->accountDB->bindUserPhone(111, "15000895133"));
    }

    /**
     * 测试绑定用户邮箱
     * @covers DB\CDBAccount::bindUserEmail
     */
    public function testBindUserEmail()
    {
        /* 更新有变化 */
        $this->assertTrue($this->accountDB->bindUserEmail(1, "15000895133@qq.com"));
        $this->assertFalse($this->accountDB->bindUserEmail(111, "15000895133@qq.com"));
    }

    /**
     * 测试绑定微信
     * @covers DB\CDBAccount::bindWechat
     */
    public function testBindWechat()
    {
        $this->assertTrue($this->accountDB->bindWechat(1, "openid12", "union_id"));
        $this->assertFalse($this->accountDB->bindWechat(111, "openid112", "union1_id"));
    }

    /**
     * 测试绑定小程序
     * @covers \DB\CDBAccount::bindMini
     */
    public function testBindMini()
    {
        $this->assertFalse($this->accountDB->bindMini(1, "openid12", "union_id", 1));
        $this->assertTrue($this->accountDB->bindMini(2, "openid12", "union_id", 1));

        $this->assertFalse($this->accountDB->bindMini(1, "openid12", "union_id", 2));
        $this->assertTrue($this->accountDB->bindMini(2, "openid12", "union_id", 2));
    }

    /**
     * 测试用户登出
     * @covers DB\CDBAccount::userLogout
     */
    public function testUserLogout()
    {
        /* 没有可更新的数据 */
        $this->assertEquals(0, $this->accountDB->userLogout(1, "123456789"));

        /* 更新有变化 */
        $this->assertEquals(1, $this->accountDB->userLogout(1, "123456"));
    }

    /**
     * @covers DB\CDBAccount::getAllSystemUsers
     */
    public function testGetAllSystemUsers()
    {
        $array = [
            ['userId' => 3],
            ['userId' => 1],
            ['userId' => 2]
        ];
        $this->assertEquals($array, $this->accountDB->getAllSystemUsers());
    }

    /**
     * @covers DB\CDBAccount::checkSubSystemToken
     */
    public function testCheckSubSystemToken()
    {
        $array = [
            'userId' => "1",
            'username' => "jiang",
            'nickname' => "jiang111",
            'photo' => "http://test.photo",
            'gender' => "1"

        ];
        $this->assertEquals($array, $this->accountDB->checkSubSystemToken('system_url', 'token'));
    }

    /**
     * @covers DB\CDBAccount::getPassword
     */
    public function testGetPassword()
    {
        $this->assertEquals('304f44c23fe9ad01475ef5fde6a2da57e8813a53064d6783da9217f44aa93f25', $this->accountDB->getPassword(1));
        $this->assertNull($this->accountDB->getPassword(1111));
    }

    /**
     * @covers DB\CDBAccount::checkAccountBindMini
     */
    public function testCheckAccountBindMini()
    {
        $this->assertTrue($this->accountDB->checkAccountBindMini(1));
        $this->assertFalse($this->accountDB->checkAccountBindMini(1111));
    }

    /**
     * @covers DB\CDBAccount::checkAccountBindPhoneOrEmail
     */
    public function testCheckAccountBindPhoneOrEmail()
    {
        $this->assertTrue($this->accountDB->checkAccountBindPhoneOrEmail(1));
        $this->assertFalse($this->accountDB->checkAccountBindPhoneOrEmail(1111));
    }

    /**
     * 测试转移小程序账号数据
     * @covers \DB\CDBAccount::moveMiniAccount
     */
    public function testMoveMiniAccount()
    {
        $this->assertEquals(-1, $this->accountDB->moveMiniAccount('jiang2222', '1234567', 3));
        $this->assertEquals(-2, $this->accountDB->moveMiniAccount('jiang', '1234567', 3));
        $this->assertEquals(-4, $this->accountDB->moveMiniAccount('jiang2', '1234567', 11));
        $this->assertTrue($this->accountDB->moveMiniAccount('jiang2', '1234567', 3));
    }

    /**
     * 测试检查小程序的openId是否存在
     * @covers \DB\CDBAccount::checkMiniOpenIdExist
     */
    public function testCheckMiniOpenIdExist() {
        $this->assertTrue($this->accountDB->checkMiniOpenIdExist('miniOpenid', 1));
        $this->assertFalse($this->accountDB->checkMiniOpenIdExist('mini111Openid', 1));

        $this->assertTrue($this->accountDB->checkMiniOpenIdExist('aiMiniOpenId', 2));
        $this->assertFalse($this->accountDB->checkMiniOpenIdExist('aiMini111OpenId', 2));
    }

    /**
     * 检查unionId是否存在
     * @covers \DB\CDBAccount::checkUnionIdExist
     */
    public function testCheckUnionIdExist() {
        $this->assertTrue($this->accountDB->checkUnionIdExist('wx_unionId'));
        $this->assertFalse($this->accountDB->checkUnionIdExist('mini111Openid'));
    }

    /**
     * 检查TencentIM云通讯账号
     * @covers \DB\CDBAccount::getTencentIMName
     */
    public function testGetTencentIMName() {
        $res = $this->accountDB->getTencentIMName();
        var_dump($res);
        $this->assertGreaterThan(0, $res['id']);
        $this->assertGreaterThan(0, $this->accountDB->updateTencentIMUserId($res['id'], 1));
    }
    
    /**
     * 检查登录失败次数
     * @covers \DB\CDBAccount::CheckLoginFalseTime
     */
    public function testCheckLoginFalseTime() {
        $loginName = 'jiang';
        $this->assertEquals(0, $this->accountDB->checkLoginFalseTime($loginName));
    }

    /**
     * 添加用户日志
     * @covers \DB\CDBAccount::recordUserOptionLog
     */
    public function testRecordUserOptionLog() {
         //记录用户日志
         $function_name = __CLASS__.'->'.__FUNCTION__;
         $option_detail = '添加日志测试记录';
         $other_msg = [
             'dbname' => 'family_test',
         ];
         $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->assertGreaterThan(0, $this->accountDB->recordUserOptionLog($function_name, $option_detail, 1, $other_msg));
    }

}