<?php

use DB\CDBFeedback;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-20
 * Time: 上午10:50
 */
class DBFeedbackTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;
    private $conn = null;
    private $feedbackDB = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    protected function getDataSet()
    {
        $this->feedbackDB = new CDBFeedback();
        return $this->createMySQLXMLDataSet('db_test/xml/feedback_dataset.xml');     //默认的数据集
    }

    /**
     * @covers DB\CDBFeedback::addFeedback()
     */
    public function testAddFeedback()
    {
        $userId = 1;
        $contact = '456789';
        $message = 'nnnn';
        $problem_type = 2;
        $photo = 'photo22';
        $clientFrom = 2;
        $versionNumber = 12;
        $this->assertEquals(2, $this->feedbackDB->addFeedback($userId, $contact, $message, $problem_type, $photo, $clientFrom, $versionNumber));
    }

    /**
     * @covers DB\CDBFeedback::handleFeedbackMessage()
     */
    public function testHandleFeedbackMessage()
    {
        $userId = 1;
        $feedbackId = 1;
        $status = 2;
        $remark = 'okay';
        $this->assertEquals(1, $this->feedbackDB->handleFeedbackMessage($userId, $feedbackId, $status, $remark));
    }

    /**
     * @covers DB\CDBFeedback::getFeedbackMessage()
     */
    public function testGetFeedbackMessage()
    {
        $array = [
            'id' => "1",
            'userId' => "1",
            'contact' => "45678979",
            'feedbackMessage' => "mmmmm",
            'handleStatus' => "1",
            'remark' => "remark",
            'problemType' => "2",
            'photo' => "photo",
            'clientFrom' => "1",
            'versionNumber' => "11",
            'createBy' => "1",
            'createTime' => "2018-06-20 12:00:00",
            'updateBy' => "1",
            'updateTime' => "2018-06-20 12:00:00"
        ];
        $this->assertEquals($array, $this->feedbackDB->getFeedbackMessage(1));
    }

    /**
     * @covers DB\CDBFeedback::getFeedbackMessageCountByStatus()
     */
    public function testGetFeedbackMessageCountByStatus()
    {
        $this->assertEquals(1, $this->feedbackDB->getFeedbackMessageCountByStatus(1));
    }

    /**
     * @covers DB\CDBFeedback::getFeedbackMessagePaging()
     */
    public function testGetFeedbackMessagePaging()
    {
        $array = [[
            'id' => "1",
            'userId' => "1",
            'contact' => "45678979",
            'feedbackMessage' => "mmmmm",
            'handleStatus' => "1",
            'remark' => "remark",
            'problemType' => "2",
            'photo' => "photo",
            'clientFrom' => "1",
            'versionNumber' => "11",
            'createBy' => "1",
            'createTime' => "2018-06-20 12:00:00",
            'updateBy' => "1",
            'updateTime' => "2018-06-20 12:00:00"
        ]];
        $this->assertEquals($array, $this->feedbackDB->getFeedbackMessagePaging(1, 1, 1));
    }
}