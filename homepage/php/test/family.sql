-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: 47.111.172.152    Database: family
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `family_greeting_card`
--

DROP TABLE IF EXISTS `family_greeting_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family_greeting_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `infoCardId` int(11) NOT NULL DEFAULT '0',
  `title` varchar(20) NOT NULL DEFAULT '',
  `template` int(11) NOT NULL,
  `data` text NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `family_greeting_card_page`
--

DROP TABLE IF EXISTS `family_greeting_card_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family_greeting_card_page` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page` int(11) NOT NULL,
  `cardId` int(11) NOT NULL,
  `content` varchar(200) NOT NULL DEFAULT '',
  `title` varchar(20) NOT NULL DEFAULT '',
  `photo` varchar(500) NOT NULL DEFAULT '',
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `family_greeting_card_share_record`
--

DROP TABLE IF EXISTS `family_greeting_card_share_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family_greeting_card_share_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cardId` int(11) NOT NULL,
  `receiveUserId` int(11) NOT NULL,
  `thanks` tinyint(4) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_action`
--

DROP TABLE IF EXISTS `gener_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_action` (
  `action` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` int(11) NOT NULL,
  PRIMARY KEY (`action`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_gener_action_module_idx` (`module`),
  CONSTRAINT `fk_gener_action_module` FOREIGN KEY (`module`) REFERENCES `gener_module` (`module`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_activity`
--

DROP TABLE IF EXISTS `gener_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(10) unsigned NOT NULL DEFAULT '0',
  `socialCircleId` int(11) NOT NULL DEFAULT '0',
  `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `deadline` datetime NOT NULL,
  `coorX` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coorY` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index5` (`start_time`),
  KEY `index6` (`deadline`)
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_activityComment`
--

DROP TABLE IF EXISTS `gener_activityComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_activityComment` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `activityId` int(30) NOT NULL,
  `userId` int(30) NOT NULL,
  `replyto` int(11) NOT NULL,
  `replyto_userId` int(11) NOT NULL DEFAULT '-1',
  `comment` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=305 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_activity_participator`
--

DROP TABLE IF EXISTS `gener_activity_participator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_activity_participator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activityId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `cancel` int(11) NOT NULL COMMENT '是否取消，0是未取消，1是取消',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_ai_image_process`
--

DROP TABLE IF EXISTS `gener_ai_image_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_ai_image_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `image` varchar(500) NOT NULL,
  `result` text NOT NULL,
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_album_person`
--

DROP TABLE IF EXISTS `gener_album_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_album_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photoId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=869 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_album_person_snapshot`
--

DROP TABLE IF EXISTS `gener_album_person_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_album_person_snapshot` (
  `backupId` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `photoId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `snapshotVersion` int(11) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`backupId`)
) ENGINE=InnoDB AUTO_INCREMENT=1477 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_answer_comment`
--

DROP TABLE IF EXISTS `gener_answer_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_answer_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answerId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `replyTo` int(11) NOT NULL,
  `replyTo_userId` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `answerId_index` (`replyTo`),
  KEY `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_area`
--

DROP TABLE IF EXISTS `gener_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_area` (
  `areaid` int(11) NOT NULL,
  `areaname` varchar(50) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`areaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_auth`
--

DROP TABLE IF EXISTS `gener_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auth` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  `operateId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_branch`
--

DROP TABLE IF EXISTS `gener_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(45) CHARACTER SET utf8mb4 NOT NULL COMMENT '分支名',
  `branch_personId` int(11) NOT NULL COMMENT '如果是主分支的话，会存在一个特殊的合并点',
  `type` int(11) NOT NULL COMMENT 'type代表分支的类型，1是主分支,2是子分支',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL COMMENT '分支的连接人物id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_collection`
--

DROP TABLE IF EXISTS `gener_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_collection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `moduleId` int(11) NOT NULL,
  `recordId` int(11) NOT NULL,
  `record_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `record_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_info1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_info2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=765 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_eventComment`
--

DROP TABLE IF EXISTS `gener_eventComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_eventComment` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `eventId` int(30) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `replyto` int(30) NOT NULL DEFAULT '-1',
  `replyto_userId` int(30) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_eventEditHistory`
--

DROP TABLE IF EXISTS `gener_eventEditHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_eventEditHistory` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `eventId` int(30) NOT NULL,
  `exp_time` date NOT NULL,
  `exp_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `exp_content` text CHARACTER SET utf8mb4 NOT NULL,
  `edit_type` int(10) NOT NULL,
  `writer` int(30) NOT NULL,
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  `is_delete` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_gener_eventEditHistory_1_idx` (`eventId`),
  CONSTRAINT `fk_gener_eventEditHistory_1` FOREIGN KEY (`eventId`) REFERENCES `gener_significantEvent` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1653 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family`
--

DROP TABLE IF EXISTS `gener_family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family` (
  `id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `surname` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `min_level` int(10) NOT NULL,
  `max_level` int(10) NOT NULL,
  `permission` int(10) NOT NULL,
  `originator` int(30) NOT NULL,
  `member_count` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `qrcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hx_chatroom` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `open_type` int(11) NOT NULL DEFAULT '0' COMMENT '开放类型，0是私密族群，1是开放示例族群。默认是0',
  `type` int(11) NOT NULL DEFAULT '1',
  `groupType` int(11) NOT NULL DEFAULT '1',
  `socialCircleId` int(11) NOT NULL DEFAULT '0',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_Family_originator_idx` (`originator`),
  KEY `surname_index` (`surname`)
) ENGINE=InnoDB AUTO_INCREMENT=685 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_familyAdmin`
--

DROP TABLE IF EXISTS `gener_familyAdmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_familyAdmin` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `familyId` int(30) unsigned NOT NULL,
  `adminId` int(30) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=646 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_familyNews`
--

DROP TABLE IF EXISTS `gener_familyNews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_familyNews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(30) NOT NULL,
  `module` int(6) NOT NULL,
  `action` int(6) NOT NULL,
  `operatorId` int(30) NOT NULL,
  `thingId` int(30) NOT NULL COMMENT '存储的是被操作的事物的id',
  `thing_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `extra_info1` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '额外信息1，例如某人为某人增加了照片，可以在这里存放照片的路径',
  `extra_info2` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '额外信息2',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_familyNews_module_idx` (`module`),
  KEY `fk_gener_familyNews_action_idx` (`action`),
  KEY `fk_gener_familyNews_familyId_idx` (`familyId`),
  CONSTRAINT `fk_gener_familyNews_action` FOREIGN KEY (`action`) REFERENCES `gener_action` (`action`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gener_familyNews_module` FOREIGN KEY (`module`) REFERENCES `gener_module` (`module`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5740 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_album`
--

DROP TABLE IF EXISTS `gener_family_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相册名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相册描述',
  `familyId` int(11) NOT NULL,
  `album_size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `update_by` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_album_share`
--

DROP TABLE IF EXISTS `gener_family_album_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_album_share` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `albumIdOrPhotoId` text NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `shareCode` varchar(45) NOT NULL,
  `shareFamilyId` int(11) NOT NULL DEFAULT '0',
  `timeLimit` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_album_snapshot`
--

DROP TABLE IF EXISTS `gener_family_album_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_album_snapshot` (
  `backupId` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `album_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相册名称',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相册描述',
  `familyId` int(11) NOT NULL,
  `album_size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `snapshotVersion` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `update_by` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`backupId`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_branch`
--

DROP TABLE IF EXISTS `gener_family_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `branchId` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_celebrities`
--

DROP TABLE IF EXISTS `gener_family_celebrities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_celebrities` (
  `id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(30) unsigned NOT NULL,
  `personId` int(30) NOT NULL,
  `personName` varchar(30) NOT NULL,
  `styleName` varchar(10) DEFAULT NULL,
  `pseudonym` varchar(10) DEFAULT NULL,
  `yearOfBirth` int(10) NOT NULL,
  `yearOfPass` int(10) NOT NULL,
  `profile` text NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `create_by` int(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_chatgroup`
--

DROP TABLE IF EXISTS `gener_family_chatgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_chatgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `chatGroupId` varchar(60) NOT NULL,
  `chatGroupName` varchar(80) NOT NULL,
  `maxusers` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `needPermit` int(11) NOT NULL,
  `allowinvites` int(11) NOT NULL,
  `owner` varchar(60) NOT NULL,
  `is_public` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `familyId_index` (`familyId`),
  KEY `chatGroupId_index` (`chatGroupId`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_heirloom`
--

DROP TABLE IF EXISTS `gener_family_heirloom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_heirloom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_time` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_inner_branch`
--

DROP TABLE IF EXISTS `gener_family_inner_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_inner_branch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `branchName` varchar(50) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_familyId` (`familyId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_inner_branch_admin`
--

DROP TABLE IF EXISTS `gener_family_inner_branch_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_inner_branch_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branchId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_inner_branch_person`
--

DROP TABLE IF EXISTS `gener_family_inner_branch_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_inner_branch_person` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branchId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_branchId` (`branchId`)
) ENGINE=InnoDB AUTO_INCREMENT=775 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_legend`
--

DROP TABLE IF EXISTS `gener_family_legend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_legend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(11) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_time` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_merge`
--

DROP TABLE IF EXISTS `gener_family_merge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_merge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src_familyId` int(11) NOT NULL,
  `merge_familyId` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`,`src_familyId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_permission`
--

DROP TABLE IF EXISTS `gener_family_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `personId` int(11) NOT NULL,
  `familyId` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_photo`
--

DROP TABLE IF EXISTS `gener_family_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `albumId` int(11) NOT NULL COMMENT '照片所在家族的相册',
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片url',
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `country` int(11) DEFAULT '-1',
  `country_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` int(11) DEFAULT '-1',
  `province_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` int(11) DEFAULT '-1',
  `city_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` int(11) DEFAULT NULL,
  `area_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` int(11) DEFAULT NULL,
  `town_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` int(11) NOT NULL COMMENT '图片的大小,单位为KB',
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1689 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_photo_snapshot`
--

DROP TABLE IF EXISTS `gener_family_photo_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_photo_snapshot` (
  `backupId` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `albumId` int(11) NOT NULL COMMENT '照片所在家族的相册',
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片url',
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `country` int(11) DEFAULT '-1',
  `country_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` int(11) DEFAULT '-1',
  `province_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` int(11) DEFAULT '-1',
  `city_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` int(11) DEFAULT NULL,
  `area_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` int(11) DEFAULT NULL,
  `town_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` int(11) NOT NULL COMMENT '图片的大小,单位为KB',
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `snapshotVersion` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`backupId`)
) ENGINE=InnoDB AUTO_INCREMENT=740 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_share`
--

DROP TABLE IF EXISTS `gener_family_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_share` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL COMMENT '分享的家族id',
  `shareCode` varchar(32) NOT NULL,
  `time_limit` int(11) NOT NULL COMMENT '时限',
  `level_limit` int(11) NOT NULL COMMENT '辈分限制',
  `views_limit` int(11) NOT NULL COMMENT '查看限制',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `create_time_index` (`create_time`),
  KEY `shareCode_index` (`shareCode`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_share_log`
--

DROP TABLE IF EXISTS `gener_family_share_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_share_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shareId` int(10) unsigned NOT NULL,
  `ip` varchar(20) NOT NULL,
  `times` int(11) NOT NULL COMMENT '代表这个ip的访问次数',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shareid_index` (`shareId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_snapshot`
--

DROP TABLE IF EXISTS `gener_family_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_snapshot` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `version` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_storage`
--

DROP TABLE IF EXISTS `gener_family_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_storage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `parent` int(11) NOT NULL,
  `parent_path` varchar(255) NOT NULL,
  `source_ip` varchar(100) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `remote_filename` varchar(250) NOT NULL,
  `file_url` varchar(500) NOT NULL,
  `size` int(11) NOT NULL,
  `file_type` int(11) NOT NULL,
  `md5` varchar(32) NOT NULL,
  `is_dir` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_index` (`parent`),
  KEY `familyId_index` (`familyId`)
) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_storage_limit`
--

DROP TABLE IF EXISTS `gener_family_storage_limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_storage_limit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) unsigned NOT NULL,
  `max_size` int(11) NOT NULL,
  `used_size` int(11) NOT NULL,
  `recycle_size` int(11) NOT NULL DEFAULT '0',
  `create_time` date NOT NULL,
  `update_time` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `familyId_UNIQUE` (`familyId`)
) ENGINE=InnoDB AUTO_INCREMENT=561 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_family_user`
--

DROP TABLE IF EXISTS `gener_family_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_family_user` (
  `id` int(13) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(13) unsigned NOT NULL,
  `userId` int(13) unsigned NOT NULL,
  `userType` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_feedback`
--

DROP TABLE IF EXISTS `gener_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feedback_message` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `handle_status` int(11) NOT NULL DEFAULT '2',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `problem_type` int(11) NOT NULL,
  `photo` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_by` char(255) CHARACTER SET latin1 NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` char(255) CHARACTER SET latin1 NOT NULL,
  `update_time` datetime NOT NULL,
  `sender` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_state` tinyint(1) DEFAULT NULL,
  `client_from` tinyint(1) DEFAULT '0',
  `version_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_gener_feedback_userId_idx` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_file`
--

DROP TABLE IF EXISTS `gener_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) CHARACTER SET utf8 NOT NULL,
  `module` int(11) NOT NULL,
  `create_by` int(8) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file_path_UNIQUE` (`file_path`)
) ENGINE=InnoDB AUTO_INCREMENT=9995 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_grave`
--

DROP TABLE IF EXISTS `gener_grave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_grave` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `personId` int(8) unsigned NOT NULL,
  `person_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(11) unsigned NOT NULL,
  `coor_x` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coor_y` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `personId_index` (`personId`),
  KEY `familyId_index` (`familyId`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_grave_snapshot`
--

DROP TABLE IF EXISTS `gener_grave_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_grave_snapshot` (
  `backupId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(8) unsigned NOT NULL,
  `personId` int(8) unsigned NOT NULL,
  `person_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(11) unsigned NOT NULL,
  `coor_x` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coor_y` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `snapshotVersion` int(11) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`backupId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_info_card`
--

DROP TABLE IF EXISTS `gener_info_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_info_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remark` varchar(45) NOT NULL,
  `userId` int(11) NOT NULL,
  `familyId` int(11) NOT NULL DEFAULT '0',
  `personId` int(11) NOT NULL DEFAULT '0',
  `socialCircleId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL,
  `name_en` varchar(125) NOT NULL DEFAULT '',
  `country` int(11) NOT NULL DEFAULT '0',
  `country_name` varchar(45) NOT NULL DEFAULT '0',
  `province` int(11) NOT NULL DEFAULT '0',
  `province_name` varchar(45) NOT NULL DEFAULT '0',
  `city` int(11) NOT NULL DEFAULT '0',
  `city_name` varchar(45) NOT NULL DEFAULT '0',
  `area` int(11) NOT NULL DEFAULT '0',
  `area_name` varchar(45) NOT NULL DEFAULT '0',
  `town` int(11) NOT NULL DEFAULT '0',
  `town_name` varchar(45) NOT NULL DEFAULT '0',
  `address` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `background_img` varchar(255) NOT NULL DEFAULT '',
  `background_color` varchar(10) NOT NULL DEFAULT '',
  `background_type` int(11) NOT NULL DEFAULT '0',
  `company` varchar(255) NOT NULL DEFAULT '',
  `company_en` varchar(255) NOT NULL DEFAULT '',
  `gender` int(11) NOT NULL DEFAULT '2',
  `birthday` date DEFAULT NULL,
  `blood_type` int(11) NOT NULL DEFAULT '0',
  `marital_status` int(11) NOT NULL DEFAULT '0',
  `phone` varchar(500) NOT NULL DEFAULT '[]',
  `qq` varchar(45) NOT NULL DEFAULT '',
  `skills` text,
  `favorites` text,
  `jobs` text,
  `identity` varchar(32) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0是自己，1是他人',
  `email` varchar(255) NOT NULL DEFAULT '',
  `company_profile` varchar(500) NOT NULL DEFAULT '',
  `company_url` varchar(255) NOT NULL DEFAULT '',
  `bank_account` varchar(125) NOT NULL DEFAULT '',
  `longitude` varchar(45) NOT NULL DEFAULT '',
  `latitude` varchar(45) NOT NULL DEFAULT '',
  `position` varchar(25) NOT NULL DEFAULT '',
  `is_show` int(11) NOT NULL DEFAULT '0' COMMENT '是否对外展示',
  `is_default` int(11) NOT NULL DEFAULT '0',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `permission` int(11) NOT NULL DEFAULT '1',
  `openScope` int(11) NOT NULL DEFAULT '0',
  `ocrcard_img` varchar(125) NOT NULL DEFAULT '',
  `tax_number` varchar(30) NOT NULL DEFAULT '',
  `imageOrder` varchar(300) NOT NULL DEFAULT '[]',
  `chatPermission` int(11) NOT NULL DEFAULT '1',
  `chatCost` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_identity` (`identity`),
  KEY `index_userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=537 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_batch_share`
--

DROP TABLE IF EXISTS `gener_infocard_batch_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_batch_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `shareName` varchar(40) NOT NULL,
  `shareCode` varchar(64) NOT NULL,
  `cardIds` text NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`,`shareCode`),
  KEY `shareCode_index` (`shareCode`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_chat_order`
--

DROP TABLE IF EXISTS `gener_infocard_chat_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_chat_order` (
  `id` bigint(20) NOT NULL,
  `userId` int(11) NOT NULL,
  `infoCardId` int(11) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `orderId` bigint(20) NOT NULL,
  `isDelete` tinyint(4) NOT NULL,
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderId_index` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_company`
--

DROP TABLE IF EXISTS `gener_infocard_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `infocard_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL DEFAULT '',
  `company_en` varchar(255) NOT NULL DEFAULT '',
  `company_profile` varchar(500) NOT NULL DEFAULT '',
  `company_url` varchar(255) NOT NULL DEFAULT '',
  `bank_account` varchar(125) NOT NULL DEFAULT '',
  `is_delete` varchar(45) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_exchange`
--

DROP TABLE IF EXISTS `gener_infocard_exchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_exchange` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `code` varchar(100) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL DEFAULT '',
  `isFinish` int(11) NOT NULL DEFAULT '0',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_exchange_record`
--

DROP TABLE IF EXISTS `gener_infocard_exchange_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_exchange_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exchangeId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `cardId` int(11) NOT NULL,
  `isDelete` tinyint(11) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exchange_index` (`exchangeId`),
  KEY `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_family_permission`
--

DROP TABLE IF EXISTS `gener_infocard_family_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_family_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `infoCardId` int(11) NOT NULL,
  `familyId` int(11) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_homepage`
--

DROP TABLE IF EXISTS `gener_infocard_homepage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_homepage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `infocard_id` int(11) NOT NULL,
  `img_url` varchar(255) NOT NULL DEFAULT '',
  `img_profile` varchar(300) NOT NULL DEFAULT '',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_share_record`
--

DROP TABLE IF EXISTS `gener_infocard_share_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_share_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `share_time` datetime NOT NULL,
  `share_userId` int(11) NOT NULL,
  `share_cardId` int(11) NOT NULL,
  `owner_userId` int(11) NOT NULL,
  `target_userId` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_tag`
--

DROP TABLE IF EXISTS `gener_infocard_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `tag` varchar(18) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_infocard_tag_card`
--

DROP TABLE IF EXISTS `gener_infocard_tag_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_infocard_tag_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagId` int(11) NOT NULL,
  `cardId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tagId_index` (`tagId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_information`
--

DROP TABLE IF EXISTS `gener_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '3',
  `isArticle` int(11) NOT NULL DEFAULT '1',
  `coorX` varchar(45) NOT NULL DEFAULT '',
  `coorY` varchar(45) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `photo` text,
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text,
  `sub_content` varchar(100) NOT NULL DEFAULT '',
  `comment` int(11) NOT NULL DEFAULT '0',
  `share` int(11) NOT NULL DEFAULT '0',
  `like` int(11) NOT NULL DEFAULT '0',
  `isAccept` int(11) NOT NULL DEFAULT '1',
  `is_md` int(11) NOT NULL DEFAULT '0',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30860 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_information_category`
--

DROP TABLE IF EXISTS `gener_information_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_information_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `avatar` varchar(500) NOT NULL,
  `name` varchar(125) NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '0',
  `follow` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `circleType` int(11) NOT NULL DEFAULT '2',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=893 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_information_comment`
--

DROP TABLE IF EXISTS `gener_information_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_information_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `infoId` int(11) NOT NULL,
  `replyToCommentId` int(11) NOT NULL,
  `replyToUserId` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_information_follow`
--

DROP TABLE IF EXISTS `gener_information_follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_information_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index` (`userId`),
  KEY `category_index` (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=651 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_information_like`
--

DROP TABLE IF EXISTS `gener_information_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_information_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `infoId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_information_resource`
--

DROP TABLE IF EXISTS `gener_information_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_information_resource` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `infoId` int(11) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `url` varchar(500) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_information_share`
--

DROP TABLE IF EXISTS `gener_information_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_information_share` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `infoId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_invitation_code`
--

DROP TABLE IF EXISTS `gener_invitation_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_invitation_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invitation_code` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL DEFAULT '0',
  `openid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invitation_code_UNIQUE` (`invitation_code`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_job`
--

DROP TABLE IF EXISTS `gener_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_job` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` date NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` date NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_like`
--

DROP TABLE IF EXISTS `gener_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_like` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `userId` int(30) NOT NULL,
  `status` int(1) NOT NULL,
  `postId` int(30) NOT NULL,
  `module` int(30) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId_postId_unique` (`userId`,`postId`)
) ENGINE=InnoDB AUTO_INCREMENT=610 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_loginHistory`
--

DROP TABLE IF EXISTS `gener_loginHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_loginHistory` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `device` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `device_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(30) NOT NULL,
  `token` varchar(64) CHARACTER SET utf8 NOT NULL,
  `is_logout` int(1) NOT NULL DEFAULT '0',
  `login_ip` varchar(56) CHARACTER SET utf8 NOT NULL,
  `login_time` datetime NOT NULL,
  `success` int(1) NOT NULL COMMENT '1代表成功，0代表失败',
  PRIMARY KEY (`id`),
  KEY `fk_gener_loginHistory_user_idx` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=15476 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_lookfor_family_task`
--

DROP TABLE IF EXISTS `gener_lookfor_family_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_lookfor_family_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_content` text NOT NULL,
  `exclude_familyIds` text NOT NULL COMMENT '排除的家族id\n',
  `priority` int(11) NOT NULL COMMENT '优先级，1~5',
  `threshold` int(11) NOT NULL COMMENT '阈值，当匹配度达到该阈值后才会通知用户，70~100',
  `email` varchar(100) DEFAULT NULL COMMENT '找到后可以选择邮件通知（可选）',
  `cycle` int(11) NOT NULL DEFAULT '3' COMMENT '周期，1~28',
  `is_finish` int(11) NOT NULL COMMENT '是否完成，0是未完成，1是已成功，-1是已失败',
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_lookfor_family_task_result`
--

DROP TABLE IF EXISTS `gener_lookfor_family_task_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_lookfor_family_task_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL,
  `is_find` int(11) NOT NULL COMMENT '是否查找到了，0没有，1有',
  `find_contents` text NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `taskId` (`taskId`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_message`
--

DROP TABLE IF EXISTS `gener_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_message` (
  `id` bigint(8) unsigned NOT NULL AUTO_INCREMENT,
  `message_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_user` int(8) NOT NULL,
  `to_user` int(8) NOT NULL,
  `content` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
  `module` int(3) NOT NULL,
  `action` int(11) NOT NULL DEFAULT '0' COMMENT '当一个模块中有几个消息动作时，可以使用这个字段来区分',
  `recordId` int(8) NOT NULL,
  `extra_recordId` int(11) NOT NULL,
  `isDelete` int(1) NOT NULL DEFAULT '0',
  `isRead` int(1) NOT NULL DEFAULT '0',
  `isAccept` int(11) NOT NULL DEFAULT '0' COMMENT '这个字段在大多数情况下没有作用，在家族邀请成员时，这个字段用来表示是否接受,0代表初始值，1代表接受，-1代表拒绝',
  `ext` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `create_by` int(8) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(8) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `message_code` (`message_code`),
  KEY `touser_index4` (`to_user`),
  KEY `from_user` (`from_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2661 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_minifamily_bind_apply`
--

DROP TABLE IF EXISTS `gener_minifamily_bind_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_minifamily_bind_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `miniFamilyId` int(11) NOT NULL,
  `miniFamilyPersonId` int(11) NOT NULL,
  `familyId` int(11) NOT NULL,
  `familyPersonId` int(11) NOT NULL,
  `remark` varchar(200) NOT NULL DEFAULT '',
  `userId` int(11) NOT NULL,
  `isAccept` tinyint(4) NOT NULL DEFAULT '0',
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `miniFamilyId_index` (`miniFamilyId`),
  KEY `familyId_index` (`familyId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_minifamily_bind_recommend`
--

DROP TABLE IF EXISTS `gener_minifamily_bind_recommend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_minifamily_bind_recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applyId` int(11) NOT NULL,
  `familyId` int(11) NOT NULL,
  `miniFamilyId` int(11) NOT NULL,
  `familyPersonId` int(11) NOT NULL,
  `miniFamilyPersonId` int(11) NOT NULL,
  `recommendData` text NOT NULL,
  `recommendType` int(11) NOT NULL DEFAULT '1',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_minifamily_bind_record`
--

DROP TABLE IF EXISTS `gener_minifamily_bind_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_minifamily_bind_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `miniFamilyId` int(11) NOT NULL,
  `familyPersonId` int(11) NOT NULL,
  `miniFamilyPersonId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `familyId_index` (`familyId`),
  KEY `miniFamilyId_index` (`miniFamilyId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_minifamily_bind_root_record`
--

DROP TABLE IF EXISTS `gener_minifamily_bind_root_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_minifamily_bind_root_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `miniFamilyId` int(11) NOT NULL,
  `familyPersonId` int(11) NOT NULL,
  `miniFamilyPersonId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `miniFamilyId_index` (`miniFamilyId`),
  KEY `familyId_index` (`familyId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_minifamily_update_apply`
--

DROP TABLE IF EXISTS `gener_minifamily_update_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_minifamily_update_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recommendId` int(11) NOT NULL,
  `remark` varchar(200) NOT NULL,
  `familyId` int(11) NOT NULL,
  `miniFamilyId` int(11) NOT NULL,
  `isAccept` tinyint(4) NOT NULL DEFAULT '0',
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_module`
--

DROP TABLE IF EXISTS `gener_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_module` (
  `module` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`module`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_ocr_task`
--

DROP TABLE IF EXISTS `gener_ocr_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_ocr_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  `result` text,
  `finish` int(11) NOT NULL DEFAULT '0' COMMENT '是否完成，0是未完成，1是已完成',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '处理的类型.0是自己的ocr服务,1是百度云的ocr服务',
  `create_by` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_order`
--

DROP TABLE IF EXISTS `gener_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_order` (
  `id` bigint(20) NOT NULL,
  `userId` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `totalPrice` varchar(50) NOT NULL,
  `realPrice` varchar(50) NOT NULL,
  `discountStrategyId` int(11) NOT NULL,
  `userRemark` varchar(200) NOT NULL,
  `failureTime` datetime NOT NULL,
  `province` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `area` varchar(20) NOT NULL,
  `town` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `name` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `goodsType` int(11) NOT NULL,
  `payway` int(11) NOT NULL,
  `wxPrepayId` varchar(64) NOT NULL,
  `isDelete` tinyint(4) NOT NULL,
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_order_log_record`
--

DROP TABLE IF EXISTS `gener_order_log_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_order_log_record` (
  `id` bigint(20) NOT NULL,
  `orderId` bigint(20) NOT NULL,
  `remark` varchar(200) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL,
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderId_index` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_pdf_task`
--

DROP TABLE IF EXISTS `gener_pdf_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_pdf_task` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) DEFAULT NULL,
  `startPersonId` int(11) NOT NULL DEFAULT '0',
  `preview_pdf` varchar(150) NOT NULL DEFAULT '',
  `print_pdf` varchar(150) NOT NULL DEFAULT '',
  `familyTreeName` varchar(125) NOT NULL DEFAULT '',
  `direction` int(11) NOT NULL DEFAULT '1',
  `eventIds` text,
  `photoIdOnes` text,
  `photoIds` text,
  `graveIds` text,
  `isHD` int(11) NOT NULL DEFAULT '0',
  `finish` int(11) NOT NULL DEFAULT '0',
  `options` varchar(2000) NOT NULL DEFAULT '',
  `create_time` datetime DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=402 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_person`
--

DROP TABLE IF EXISTS `gener_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_person` (
  `id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `same_personId` int(11) NOT NULL DEFAULT '0',
  `infoCardId` int(11) DEFAULT NULL,
  `branchId` int(11) NOT NULL DEFAULT '0',
  `familyId` int(30) unsigned NOT NULL,
  `userId` int(30) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '人物类型，1是代表本家族的人，则可以向外延伸，2是家族成员的配偶，不可以向外添加父母，兄弟姐妹，配偶等等',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `name` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `zpname` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字排名',
  `zi` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `birthday` date DEFAULT NULL,
  `gender` int(10) NOT NULL COMMENT '0是女性，1是男性',
  `photo` varchar(250) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `share_url` varchar(250) CHARACTER SET utf8 NOT NULL,
  `is_dead` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 代表未去世 默认为0',
  `is_adoption` int(11) NOT NULL DEFAULT '0',
  `dead_time` date DEFAULT NULL,
  `ref_familyId` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_personId` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `mother` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `son` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `daughter` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `spouse` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `sister` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `brother` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `level` int(30) NOT NULL DEFAULT '0',
  `confirm` int(1) DEFAULT NULL COMMENT 'confirm表示这条记录有没有被管理员确认',
  `blood_type` int(11) DEFAULT NULL,
  `marital_status` int(11) DEFAULT '0',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qq` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qrcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profileText` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sideText` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` int(11) DEFAULT '0',
  `country_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `province` int(11) DEFAULT '0',
  `province_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `city` int(11) DEFAULT '0',
  `city_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `area` int(11) DEFAULT '0',
  `area_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `town` int(11) DEFAULT '0',
  `town_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `address` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ranking` int(11) NOT NULL DEFAULT '0',
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_testPerson_user_idx` (`userId`),
  KEY `branchId_index` (`branchId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3210 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_personDetail`
--

DROP TABLE IF EXISTS `gener_personDetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_personDetail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `personId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `info` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT 'type代表Info保存的信息,比如1代表技能skill,2代表爱好favorite,',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personId` (`personId`,`info`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_person_adoption`
--

DROP TABLE IF EXISTS `gener_person_adoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_person_adoption` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `src_personId` int(11) NOT NULL DEFAULT '0',
  `dest_personId` int(11) NOT NULL DEFAULT '0',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `src_index` (`src_personId`),
  KEY `dest_index` (`dest_personId`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_person_bind_user_record`
--

DROP TABLE IF EXISTS `gener_person_bind_user_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_person_bind_user_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familyId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `acceptUserId` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_person_snapshot`
--

DROP TABLE IF EXISTS `gener_person_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_person_snapshot` (
  `backupId` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(30) unsigned NOT NULL,
  `same_personId` int(11) NOT NULL DEFAULT '0',
  `infoCardId` int(11) DEFAULT NULL,
  `branchId` int(11) NOT NULL DEFAULT '0',
  `familyId` int(30) unsigned NOT NULL,
  `userId` int(30) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '人物类型，1是代表本家族的人，则可以向外延伸，2是家族成员的配偶，不可以向外添加父母，兄弟姐妹，配偶等等',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `name` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `zpname` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字排名',
  `zi` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `birthday` date DEFAULT NULL,
  `gender` int(10) NOT NULL COMMENT '0是女性，1是男性',
  `photo` varchar(250) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `share_url` varchar(250) CHARACTER SET utf8 NOT NULL,
  `is_dead` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 代表未去世 默认为0',
  `is_adoption` int(11) NOT NULL DEFAULT '0',
  `dead_time` date DEFAULT NULL,
  `ref_familyId` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_personId` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `mother` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `son` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `daughter` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `spouse` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `sister` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `brother` varchar(2000) CHARACTER SET utf8 DEFAULT '',
  `level` int(30) NOT NULL DEFAULT '0',
  `confirm` int(1) DEFAULT NULL COMMENT 'confirm表示这条记录有没有被管理员确认',
  `blood_type` int(11) DEFAULT NULL,
  `marital_status` int(11) DEFAULT '0',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qq` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qrcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profileText` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sideText` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` int(11) DEFAULT '0',
  `country_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `province` int(11) DEFAULT '0',
  `province_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `city` int(11) DEFAULT '0',
  `city_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `area` int(11) DEFAULT '0',
  `area_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `town` int(11) DEFAULT '0',
  `town_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `address` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ranking` int(11) NOT NULL DEFAULT '0',
  `snapshotVersion` int(11) NOT NULL,
  `create_by` int(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`backupId`),
  KEY `fk_gener_testPerson_family_idx` (`familyId`),
  KEY `fk_gener_testPerson_user_idx` (`userId`),
  KEY `branchId_index` (`branchId`) USING BTREE,
  KEY `id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6368 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_phone_vcode`
--

DROP TABLE IF EXISTS `gener_phone_vcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_phone_vcode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(11) NOT NULL,
  `vcode` varchar(6) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `isUse` int(11) NOT NULL DEFAULT '0',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_point`
--

DROP TABLE IF EXISTS `gener_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_point` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `total_point` int(11) NOT NULL,
  `used_point` int(11) NOT NULL,
  `remain_point` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_point_record`
--

DROP TABLE IF EXISTS `gener_point_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_point_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index2` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_qrcode`
--

DROP TABLE IF EXISTS `gener_qrcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_qrcode` (
  `id` bigint(20) NOT NULL,
  `code` varchar(256) NOT NULL,
  `do` varchar(50) NOT NULL,
  `data` varchar(1000) NOT NULL,
  `userId` int(11) NOT NULL,
  `expire` int(11) NOT NULL DEFAULT '0',
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createBy` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_question_addition`
--

DROP TABLE IF EXISTS `gener_question_addition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_question_addition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionId` int(11) NOT NULL,
  `content` varchar(500) NOT NULL,
  `photo` text NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_question_answer`
--

DROP TABLE IF EXISTS `gener_question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `content` text NOT NULL,
  `comment_count` int(11) NOT NULL DEFAULT '0',
  `like_count` int(11) NOT NULL DEFAULT '0',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `isBest` tinyint(4) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index` (`userId`),
  KEY `questionId_index` (`questionId`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_receive_info_card`
--

DROP TABLE IF EXISTS `gener_receive_info_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_receive_info_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cardId` int(11) NOT NULL,
  `tag` varchar(45) NOT NULL DEFAULT '',
  `is_delete` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_cardId` (`cardId`),
  KEY `index_createBy` (`create_by`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_redpack`
--

DROP TABLE IF EXISTS `gener_redpack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_redpack` (
  `outOrderNo` varchar(64) NOT NULL,
  `authNo` varchar(64) DEFAULT NULL,
  `outRequestNo` varchar(64) NOT NULL,
  `operationId` varchar(64) DEFAULT NULL,
  `orderTitle` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `payTimeout` varchar(5) DEFAULT NULL,
  `extraParam` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `gmtTrans` datetime DEFAULT NULL,
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`outOrderNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_report`
--

DROP TABLE IF EXISTS `gener_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `moduleId` int(11) NOT NULL,
  `recordId` int(11) NOT NULL,
  `report_type` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_significantEvent`
--

DROP TABLE IF EXISTS `gener_significantEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_significantEvent` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) NOT NULL DEFAULT '0' COMMENT '源id，大事件是可以被复制的，所以这个字段用来标识大事件的源id',
  `personId` int(30) NOT NULL,
  `userId` int(30) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `iconograph` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(30) DEFAULT NULL,
  `resources` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '事件类型，1是大事件，2是秘密事件',
  `is_md` tinyint(1) NOT NULL DEFAULT '1',
  `open_time` date DEFAULT NULL COMMENT '如果是秘密事件，则有一个公开时间的属性',
  `exp_time` date NOT NULL,
  `exp_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `exp_content` text CHARACTER SET utf8mb4 NOT NULL,
  `abstract` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `write_mode` int(10) NOT NULL,
  `participate_num` int(11) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `create_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_significantEvent_person_idx` (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=905 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_significantEvent_snapshot`
--

DROP TABLE IF EXISTS `gener_significantEvent_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_significantEvent_snapshot` (
  `backupId` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(30) NOT NULL,
  `sourceId` int(11) NOT NULL DEFAULT '0' COMMENT '源id，大事件是可以被复制的，所以这个字段用来标识大事件的源id',
  `personId` int(30) NOT NULL,
  `userId` int(30) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `iconograph` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyId` int(30) DEFAULT NULL,
  `resources` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '事件类型，1是大事件，2是秘密事件',
  `is_md` tinyint(1) NOT NULL DEFAULT '1',
  `open_time` date DEFAULT NULL COMMENT '如果是秘密事件，则有一个公开时间的属性',
  `exp_time` date NOT NULL,
  `exp_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `exp_content` text CHARACTER SET utf8mb4 NOT NULL,
  `abstract` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `write_mode` int(10) NOT NULL,
  `participate_num` int(11) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '1',
  `snapshotVersion` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  PRIMARY KEY (`backupId`)
) ENGINE=InnoDB AUTO_INCREMENT=612 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_task_tender`
--

DROP TABLE IF EXISTS `gener_task_tender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_task_tender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `taskId` int(11) NOT NULL,
  `content` varchar(500) NOT NULL,
  `is_accept` int(11) NOT NULL DEFAULT '0',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_timeline`
--

DROP TABLE IF EXISTS `gener_timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refId` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `create_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user`
--

DROP TABLE IF EXISTS `gener_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL DEFAULT '1' COMMENT '用户类型，默认是1，为普通用户，2是系统用户',
  `username` varchar(45) CHARACTER SET utf8mb4 NOT NULL,
  `nickname` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `openid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户绑定微信后会有openid字段',
  `mini_openid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aiMiniOpenId` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wx_unionId` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qq_openid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wb_openid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hx_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hx_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gender` int(2) NOT NULL DEFAULT '2',
  `birthday` date DEFAULT NULL,
  `country` int(11) DEFAULT '0',
  `country_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `province` int(11) DEFAULT '0',
  `province_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `city` int(11) DEFAULT '0',
  `city_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `area` int(11) DEFAULT '0',
  `area_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `town` int(11) DEFAULT '0',
  `town_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `target_user` int(11) DEFAULT NULL COMMENT '数据迁移到的账户',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hx_username_index` (`hx_username`),
  KEY `username_index` (`username`),
  KEY `phone_index` (`phone`),
  KEY `email_index` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=543 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_alias`
--

DROP TABLE IF EXISTS `gener_user_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `aliasUserId` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_blacklist`
--

DROP TABLE IF EXISTS `gener_user_blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_blacklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `block_userId` int(11) NOT NULL,
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_code`
--

DROP TABLE IF EXISTS `gener_user_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codeId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_follow`
--

DROP TABLE IF EXISTS `gener_user_follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `followerId` int(11) NOT NULL,
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `updateTime` datetime NOT NULL,
  `updateBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_invitation_code`
--

DROP TABLE IF EXISTS `gener_user_invitation_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_invitation_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` int(11) NOT NULL,
  `code` varchar(8) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_profile`
--

DROP TABLE IF EXISTS `gener_user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `info_post_num` int(10) unsigned NOT NULL DEFAULT '0',
  `post_num` int(10) unsigned NOT NULL,
  `collection_num` int(10) unsigned NOT NULL,
  `family_num` int(10) unsigned NOT NULL,
  `usePrivateDB` tinyint(4) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(10) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=712 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_question`
--

DROP TABLE IF EXISTS `gener_user_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `content` varchar(500) NOT NULL,
  `photo` text NOT NULL,
  `addition` int(11) NOT NULL,
  `socialCircleId` int(11) NOT NULL DEFAULT '0',
  `answerCount` int(11) NOT NULL DEFAULT '0',
  `point` int(11) NOT NULL DEFAULT '0',
  `isFinish` tinyint(4) NOT NULL DEFAULT '0',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_sso`
--

DROP TABLE IF EXISTS `gener_user_sso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_sso` (
  `token` varchar(32) NOT NULL,
  `system_url` varchar(45) NOT NULL,
  `userId` int(11) NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_user_task`
--

DROP TABLE IF EXISTS `gener_user_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_user_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialCircleId` int(11) NOT NULL DEFAULT '0',
  `userId` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `content` varchar(500) NOT NULL,
  `photo` text NOT NULL,
  `point` int(11) NOT NULL,
  `state` int(1) NOT NULL DEFAULT '1',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_index` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_zone`
--

DROP TABLE IF EXISTS `gener_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_zone` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `userId` int(30) NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `can_share` int(11) NOT NULL DEFAULT '0' COMMENT '能否被分享，0不可以，1可以',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(30) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_zoneComment`
--

DROP TABLE IF EXISTS `gener_zoneComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_zoneComment` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `postId` int(30) NOT NULL,
  `userId` int(30) NOT NULL,
  `replyto` int(11) NOT NULL,
  `replyto_userId` int(11) NOT NULL DEFAULT '-1',
  `comment` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `module` int(11) NOT NULL DEFAULT '5',
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_zoneComment_user_idx` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=920 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gener_zonePost`
--

DROP TABLE IF EXISTS `gener_zonePost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gener_zonePost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zoneId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `familyId` int(11) NOT NULL DEFAULT '0',
  `content` varchar(256) CHARACTER SET utf8mb4 NOT NULL,
  `coor_x` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coor_y` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `comments` int(30) NOT NULL DEFAULT '0',
  `likes` int(30) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1是代表自己发送的,2是代表系统生成',
  `over_report` int(11) NOT NULL DEFAULT '0',
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gener_zonePost_zone_idx` (`zoneId`),
  KEY `fk_gener_zonePost_userId_idx` (`userId`),
  CONSTRAINT `fk_gener_zonePost_zone` FOREIGN KEY (`zoneId`) REFERENCES `gener_zone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2390 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-12 17:05:28
