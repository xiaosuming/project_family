<?php
use PHPUnit\Framework\TestCase;
use Util\Pager;

class PagerTest extends TestCase{
    public function testPrintPage(){
        $paging = ['1','2','3'];
        $pager = new Pager(10,$paging,1,3);
        $this->expectOutputString('{"errorCode":"0","data":{"firstPage":true,"lastPage":false,"pageIndex":1,"pageSize":3,"paging":["1","2","3"],"total":10}}');
        $pager->printPage();
    }
}