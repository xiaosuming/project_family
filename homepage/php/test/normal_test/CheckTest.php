<?php
use PHPUnit\Framework\TestCase;
use Util\Check;

class CheckTest extends TestCase{

    public function testCheck(){
        $html = "<html><body></body></html>";
        $this->assertFalse($html == Check::check($html));

        $sql = "SELECT id FROM table WHERE text = 'dasdas'\" ";
        $this->assertFalse($sql == Check::check($sql));
    }

    public function testCheckEmail(){
        $email = "7123_18593.1@qq.123.com";
        $this->assertEquals($email,Check::checkEmail($email,false));

        $email = "7123_185-93.1@q-q.1-2-3.com";
        $this->assertEquals($email,Check::checkEmail($email,false));

        $email = "dasdasdasd@.com";
        $this->assertEquals(false,Check::checkEmail($email,false));

        $email = "dasdasdasd";
        $this->assertEquals(false,Check::checkEmail($email,false));
        
    }

    public function testCheckUserName(){
        $username = "username";     
        $this->assertEquals($username,Check::checkUserName($username,false));

        $username = "123123";
        $this->assertFalse(Check::checkUserName($username,false));

        $username = "a7871@qq.com";
        $this->assertFalse(Check::checkUserName($username,false));

        $username = "测试中文";
        $this->assertEquals($username,Check::checkUserName($username,false));
    }

    public function testCheckPhoneNumber(){
        $phoneNumber = "15000903293";
        $this->assertEquals($phoneNumber,Check::checkPhoneNumber($phoneNumber,false));

        $phoneNumber = "1500090322";    //少位数
        $this->assertEquals(false,Check::checkPhoneNumber($phoneNumber,false));

        $phoneNumber = "150009032212";  //多位数
        $this->assertEquals(false,Check::checkPhoneNumber($phoneNumber,false));

        $phoneNumber = "1500098765a";  //包含字母
        $this->assertEquals(false,Check::checkPhoneNumber($phoneNumber,false));
    }

    public function testCheckInteger(){
        $num = 9;
        $this->assertEquals($num,Check::checkInteger($num,false));

        $num = 9.1;
        $this->assertFalse(Check::checkInteger($num,false));

        $num = "9";
        $this->assertEquals($num,Check::checkInteger($num,false));
    }

    public function testCheckArray(){
        $array = [1,2,3,4];
        $this->assertEquals($array,Check::checkArray($array,false));

        $noArray = "1234";
        $this->assertFalse(Check::checkArray($noArray,false));
    }

    public function testCheckBool(){
        $b = true;
        $this->assertEquals($b,Check::checkBool($b,false));

        $nb = "true";
        $this->assertFalse(Check::checkBool($nb,false));
    }

    public function testCheckModule(){
        $module = 11;
        $this->assertEquals($module,Check::checkModule($module,false));

        $module = -2;
        $this->assertFalse(Check::checkModule($module,false));
    }

    public function testCheckIdArray(){
        $idArray = "1,2,3,4,5,6,7,8";
        $this->assertEquals($idArray,Check::checkIdArray($idArray,false));

        $idArray = "1,2,3,4,5,6,7,8,";
        $this->assertFalse(Check::checkIdArray($idArray,false));
    }

    public function testCheckUrl(){
        $validUrl = "www.xinhuotech.com";
        $this->assertEquals($validUrl,Check::checkUrl($validUrl,false));

        $invalidUrl = "ada_ads.com";
        $this->assertFalse(Check::checkUrl($invalidUrl,false));
    }

    public function testCheckUrlWithLimit(){
        $validUrl = "www.xinhuotech.com:9089";
        $this->assertEquals($validUrl,Check::checkUrlWithLimit($validUrl));

        $validUrl = "www.izuqun.com";
        $this->assertEquals($validUrl,Check::checkUrlWithLimit($validUrl));

        $invalidUrl = "asa.xinhuotech.com";
        $this->assertFalse(Check::checkUrlWithLimit($invalidUrl));

        $validUrl = "http://www.xinhuotech.com:9089/dasda/adsadas";
        $this->assertEquals($validUrl,Check::checkUrlWithLimit($validUrl));
    }

    /**
     * 检查日期
     *
     */
    public function testCheckDate(){
        $validDate = "2017-01-01";
        $this->assertEquals($validDate,Check::checkDate($validDate,false));

        $validDate = "1000-12-31";
        $this->assertEquals($validDate,Check::checkDate($validDate,false));

        $invalidDate = "1000-00-12";
        $this->assertFalse(Check::checkDate($invalidDate,false));

        $invalidDate = "1000-12-00";
        $this->assertFalse(Check::checkDate($invalidDate,false));

        $invalidDate = "1000-1-1";
        $this->assertFalse(Check::checkDate($invalidDate,false));
    }

    /**
     * 检查日期时间
     */
    public function testCheckDatetime(){
        $validDate = "2017-01-01 00:00:00";
        $this->assertEquals($validDate,Check::checkDateTime($validDate,false));

        $validDate = "1000-12-31 23:59:59";
        $this->assertEquals($validDate,Check::checkDateTime($validDate,false));

        $invalidDate = "1000-00-12 23:59:59";
        $this->assertFalse(Check::checkDateTime($invalidDate,false));

        $invalidDate = "1000-12-00 23:59:59";
        $this->assertFalse(Check::checkDateTime($invalidDate,false));

        $invalidDate = "1000-1-1 23:59:59";
        $this->assertFalse(Check::checkDateTime($invalidDate,false));

        $invalidDate = "1000-01-01 23:9:59";
        $this->assertFalse(Check::checkDateTime($invalidDate,false));

        $invalidDate = "1000-01-01 24:00:00";
        $this->assertFalse(Check::checkDateTime($invalidDate,false));
    }
}