<?php
use PHPUnit\Framework\TestCase;
use Util\Util;

class UtilTest extends TestCase{

    public function testArrayToString() {
        $array = array('1','2','3','4','5');
        $this->assertEquals('1,2,3,4,5',Util::arrayToString($array));
    }

    public function testGetValidDomain() {

        $str = "https://wiki.izuqun.com/iada/dasd";
        $this->assertEquals("wiki.izuqun.com",Util::getValidDomain($str));

        $str = "http://wiki.izuqun.com:90/iada/dasd";
        $this->assertEquals("wiki.izuqun.com:90",Util::getValidDomain($str));

        $str = "wiki.izuqun.com:9090/das/dasda";
        $this->assertEquals('wiki.izuqun.com:9090',Util::getValidDomain($str));

        $str = "wiki.izuqun.com/das/dasda";
        $this->assertEquals('wiki.izuqun.com',Util::getValidDomain($str));
    }

    public function testGetAtUser(){
        $content = '你好啊。@{uid:2,nickname:joyme}';

        $result = Util::getAtUser($content);

        $this->assertEquals(1,count($result));

        $content = '你好啊。@{uid:2,nickname:joyme}@{uid:2,nickname:joyme}';

        $result = Util::getAtUser($content);

        $this->assertEquals(1,count($result));

        $content = '你好啊。@{uid:2,nickname:joyme}@{uid:3,nickname:joyme}';

        $result = Util::getAtUser($content);

        $this->assertEquals(2,count($result));

        $content = '你好啊。@{id:2,nickname:joyme}';

        $result = Util::getAtUser($content);

        $this->assertEquals(0,count($result));

    }

    public function testArrayToRelation() {
        $this->assertEquals('@1|@2|@3|@4|',Util::arrayToRelation([1,2,3,4]));
        $this->assertEquals('',Util::arrayToRelation([]));
    }

    public function testRelationSplit() {

        $this->assertEquals(array(1,2,3,4), Util::relationSplit("@1|@2|@3|@4|"));
        $this->assertEquals(array(), Util::relationSplit(""));
    }
}