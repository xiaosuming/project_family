<?php
use PHPUnit\Framework\TestCase;
use Util\FileUtil;

class CheckTest extends TestCase{

    public function setUp(){
    
    }

    /**
     * 测试获取文件类型
     * @cover FileUtil::getFileType
     */
    public function testGetFileType(){
        /* 文件是png */
        $this->assertEquals(FileUtil::FILE_TYPE_PNG,FileUtil::getFileType(__DIR__."/img1.png"));

        /* 文件是jpg */
        $this->assertEquals(FileUtil::FILE_TYPE_JPG,FileUtil::getFileType(__DIR__."/jpg1.jpg"));

        /* 文件是gif */
        $this->assertEquals(FileUtil::FILE_TYPE_GIF,FileUtil::getFileType(__DIR__."/gif.gif"));

        /* 文件是midi */
        $this->assertEquals(FileUtil::FILE_TYPE_MIDI,FileUtil::getFileType(__DIR__."/midi.mid"));

        /* 文件是bmp */
        $this->assertEquals(FileUtil::FILE_TYPE_BMP,FileUtil::getFileType(__DIR__."/bmp.bmp"));

        /* 文件是exe */
        $this->assertEquals(FileUtil::FILE_TYPE_EXE,FileUtil::getFileType(__DIR__."/exe1.exe"));

        /* 文件是zip */
        $this->assertEquals(FileUtil::FILE_TYPE_ZIP,FileUtil::getFileType(__DIR__."/zip1.zip"));

        /* 文件是rar */
        $this->assertEquals(FileUtil::FILE_TYPE_RAR,FileUtil::getFileType(__DIR__."/emoji.rar"));
    }

    public function tearDown(){

    }
};

?>