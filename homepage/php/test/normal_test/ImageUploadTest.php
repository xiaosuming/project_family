<?php
use PHPUnit\Framework\TestCase;
use Util\ImageUpload;

class ImageUploadTest extends TestCase{

    public $uploader;
    public $path;

    public function setUp(){
        $_FILES['file']['name'] = "test.jpg";
        $_FILES['file']['size'] = 347076;
        $_FILES['file']['type'] = "image/jpeg";
        $_FILES['file']['tmp_name'] = dirname(__FILE__)."/test.jpg";
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $this->uploader = new ImageUpload($_FILES['file'],"/home/jiang/family/homepage/images/upload/test/");
        $this->path = $this->uploader->getPath();
    }

    public function testGetPath(){
        $this->assertStringStartsWith("/home/jiang/family/homepage/images/upload/test/",$this->uploader->getPath());
    }

    public function testThumbnail(){
        $this->uploader->thumbnail(100,100);
        $this->assertFileExists($_SERVER['DOCUMENT_ROOT'].$this->path.'_thumb');
    }



    public function testUpload(){
        $this->uploader->upload();
        $this->assertFileExists($_SERVER['DOCUMENT_ROOT'].$this->path);
    }

}