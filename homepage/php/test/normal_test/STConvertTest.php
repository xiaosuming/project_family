<?php
use PHPUnit\Framework\TestCase;
use Util\STConvert\STConvert;

class STConvertTest extends TestCase{
    public function testConvert(){
        $stConvert = new STConvert(STConvert::CONVERT_TYPE_S2T);

        $this->assertEquals("趙", $stConvert->convert("赵"));

        $stConvert = new STConvert(STConvert::CONVERT_TYPE_T2S);

        $this->assertEquals("赵", $stConvert->convert("趙"));
    }

}