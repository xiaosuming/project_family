#!/bin/bash

# 在本地跑redis实例
dict=$PWD

cd docker-redis-instance/docker-redis-family-auth
bash startRedis.sh

cd ${dict}
cd docker-redis-instance/docker-redis-family-cache
bash startRedis.sh

cd ${dict}
cd docker-redis-instance/docker-redis-family-tempstorage
bash startRedis.sh

cd ${dict}