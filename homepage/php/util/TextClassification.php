<?php
/**
 * 文本分类
 */
namespace Util;

class TextClassification {
    public $src;
    public $type;

    public $phone = [];
    public $position = null;
    public $company = null;
    public $address = null;
    public $name = null;
    public $email = null;
    public $companyUrl = null;

    /**
     * @param $text 要分析的文本
     * @param $type 文本的来源类型,0是自己的ocr服务, 1是百度云的ocr服务
     */
    public function __construct($text, $type) {
        $this->src = $text;
        $this->type = $type;
    }

    private function ourselfServerAnalysis() {

        // 先给文字分行
        $lines = explode(PHP_EOL, $this->src);
        foreach($lines as $line) {
            //去除line中所有的空白字符

            $line = str_replace(' ', '', $line);

            if ($line == '') {
                continue;
            }


            // 匹配座机格式
            $matches = [];
            $regex = '@\d{3,4}-\d{7,8}@';
            if (preg_match($regex, $line, $matches)) {
                // 找到了
                array_push($this->phone, $matches[0]);
                continue;
            } else {
                // 匹配电话
                $matches = [];
                $regex = '@\d{7,11}@';
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    array_push($this->phone, $matches[0]);
                }

            }  
            
            // 匹配邮箱
            if ($this->email == null) {
                $matches = [];
                $regex = "|[a-zA-Z0-9_\-.]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z]+|";
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    $this->email = $matches[0];
                }
            }

            // 匹配网址
            if ($this->companyUrl == null) {
                $matches = [];
                $regex = '|[a-zA-Z0-9]{1,10}\.([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}|';
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    $this->companyUrl = $matches[0];
                }
            }

            // 匹配姓名
            if ($this->name == null) {
                $len = mb_strlen($line);
                if ($len >=2 && $len <= 3 && strlen($line) >= 2*$len) {
                    // 都是汉字
                    $this->name = $line;
                    continue;
                }
            }

            // 匹配公司
            if ($this->company == null) {
                $matches = [];
                $regex = "|(.*)公司|";
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    $this->company = $matches[0];
                    continue;
                }
            }

            // 匹配地址
            if ($this->address == null) {
                $matches = [];
                $regex = "|(.*)路(.*)号(.*)|";
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    $this->address = $matches[0];
                    continue;
                }
            }

            // 匹配职位
            if ($this->position == null) {
                $len = mb_strlen($line);
                if ($len >=2 && $len <= 15) {
                    // 有汉字
                    $this->position = $line;
                    continue;
                }
            }
        }
    }

    /**
     * 百度云的ocr服务
     */
    private function baiduOCRAnalysis() {
        $ocrResult = json_decode($this->src, true);
        $wordResult = $ocrResult['words_result'];
        foreach($wordResult as $word) {

            $line = $word['words'];

            // 匹配座机格式
            $matches = [];
            $regex = '@\d{3,4}-\d{7,8}@';
            if (preg_match($regex, $line, $matches)) {
                // 找到了
                array_push($this->phone, $matches[0]);
                continue;
            } else {
                // 匹配电话
                $matches = [];
                $regex = '@\d{7,11}@';
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    array_push($this->phone, $matches[0]);
                }

            }  

            // 匹配邮箱
            if ($this->email == null) {
                $matches = [];
                $regex = "|[a-zA-Z0-9_\-.]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z]+|";
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    $this->email = $matches[0];
                }
            }

            // 匹配网址
            if ($this->companyUrl == null) {
                $matches = [];
                $regex = '|[a-zA-Z0-9]{1,10}\.([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}|';
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    $this->companyUrl = $matches[0];
                }
            }

            // 匹配姓名
            if ($this->name == null) {
                $len = mb_strlen($line);
                if ($len >=2 && $len <= 3 && strlen($line) >= 2*$len) {
                    // 都是汉字
                    $this->name = $line;
                    continue;
                }
            }

            // 匹配公司
            if ($this->company == null) {
                $matches = [];
                $regex = "|(.*)公司|";
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    $this->company = $matches[0];
                    continue;
                }
            }

            // 匹配地址
            if ($this->address == null) {
                $matches = [];
                $regex = "|(.*)路(.*)号(.*)|";
                if (preg_match($regex, $line, $matches)) {
                    // 找到了
                    $this->address = $matches[0];
                    continue;
                }
            }

            // 匹配职位
            if ($this->position == null) {
                $len = mb_strlen($line);
                if ($len >=2 && $len <= 15) {
                    // 有汉字
                    $this->position = $line;
                    continue;
                }
            }
        }
    }

    public function analysis() {
        switch($this->type) {
            // 自己的ocr服务
            case 0:
                $this->ourselfServerAnalysis();
                break;

            case 1:
                $this->baiduOCRAnalysis();
                break;
        }
    }

}