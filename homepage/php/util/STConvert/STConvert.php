<?php
/**
 * 简繁体转换
 */
namespace Util\STConvert;

class STConvert {
    const CONVERT_TYPE_S2T = __DIR__."/opencc/s2t.json";
    const CONVERT_TYPE_T2S = __DIR__."/opencc/t2s.json";

    private $opencc;

    public function __construct($convertType)
    {
        $this->opencc = \opencc_open($convertType);
    }

    public function convert($text) 
    {
        return  \opencc_convert($text, $this->opencc);
    }

    public function __destruct()
    {
        \opencc_close($this->opencc);
    }
}