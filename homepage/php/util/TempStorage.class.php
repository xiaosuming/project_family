<?php
/**
 * 临时存储类，使用redis进行临时存储
 * @author jiang
 * @date: 2017-08-09
 */

namespace Util;

use DB\RedisInstanceManager;   

class TempStorage{
    private $redis = null;

    public function __construct(){
        $redisManager = RedisInstanceManager::getInstance();
        $this->redis = $redisManager->getTempStorageInstance();
    }

    /**
     * 设置临时存储
     * @param $key  键
     * @param $value 值
     * @param $expiration 过期时间,单位是秒
     * @return boolean 成功返回true，失败返回false
     */
    public function setTemp($key,$value,$expiration){

        $result = $this->redis->setEx($key,$expiration,$value);
        
        return $result;
    }

    /**
     * 获取临时存储
     * @param $key 键
     * @return mix 存在返回string，不存在返回false
     */
    public function get($key){
        $result = $this->redis->get($key);
        return $result;
    }

    /**
     * 删除临时存储
     */
    public function del($key) {

        $result = $this->redis->del($key);
        return $result;
    }

    /**
     * 变量自增 1
     * @param $key  键
     */
    public function increment($key){
        $result = $this->redis->incr($key,1);
        return $result;
    }
}