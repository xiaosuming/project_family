<?php
namespace util;

    class MailModel{
        public static $header = "<div style='background-color:#ECECEC; padding: 35px;'>
<table cellpadding='0' align='center' style='width: 600px; margin: 0px auto; text-align: left; position: relative; border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; font-size: 14px; font-family:微软雅黑, 黑体; line-height: 1.5; box-shadow: rgb(153, 153, 153) 0px 0px 5px; border-collapse: collapse; background-position: initial initial; background-repeat: initial initial;background:#fff;'>
<tbody><tr><th valign='middle' style='height: 25px; line-height: 25px; padding: 15px 35px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #C46200; background-color: #FEA138; border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px;'>
<font face='微软雅黑' size='5' style='color: rgb(255, 255, 255); '>爱族群</font></th></tr><tr><td><div style='padding:25px 35px 40px; background-color:#fff;'><h2 style='margin: 5px 0px; '>";
        public static $footer = "</div></td></tr></tbody></table></div>";

        public static function getVerifyModel($auth, $username, $link)
        {
            return self::$header."<p>亲爱的{$username}，您好</p>".
            "<p>欢迎加入爱族群！</p>".
            "<p>请点击以下按钮完成注册，30分钟内有效！</p>".
            "<a href={$link}><button>完成注册</button></a>".
            "<p>如果以上按钮无法点击，那么请将以下链接复制到您的浏览器进入爱族群</p>".
            $link.
            "<p>--爱族群</p>".self::$footer;
        }
        public static function getBindModel($auth, $username, $link)
        {
            return self::$header."<p>亲爱的{$username}，您好</p>".
            "<p>欢迎使用爱族群！</p>".
            "<p>请点击以下按钮完成邮箱绑定，30分钟内有效！</p>".
            "<a href={$link}><button>绑定邮箱</button></a>".
            "<p>如果以上按钮无法点击，那么请将以下链接复制到您的浏览器进入爱族群</p>".
            $link.
            "<p>--爱族群</p>".self::$footer;
        }

        public static function getRetrieveModel($auth, $username, $link){
            return self::$header."<p>亲爱的{$username}，您好</p>".
            "<p>欢迎使用爱族群！</p>".
            "<p>请点击以下按钮修改密码，30分钟内有效！</p>".
            "<a href={$link}><button>修改密码</button></a>".
            "<p>如果以上按钮无法点击，那么请将以下链接复制到您的浏览器进入爱族群</p>".
            $link.
            "<p>--爱族群</p>".self::$footer;
        }
    }
    
?>