<?php
/**
 * SysLogger 是将Mono简单封装后的日志
 * @Author: jiangpengfei
 * @Date:   2019-02-21
 */

namespace Util;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;

class SysLogger {

    public static $logger = null;
    public static $localLogger = [];

    private function __contructor() {

    }

    public static function getInstance($local = false, $logName = '', $logLevel = Logger::INFO) {
            
        if ($local) {
            // 判断本地文件日志对象是否存在
            if (!isset(self::$localLogger[$logName])) {
                $log = new Logger($logName);
                $logPath = \YaConf::get('log');
                $log->pushHandler(new StreamHandler($logPath['path'].'/'.$logName.'.log', $logLevel));

                $localLogger[$logName] = $log;
            }

            return $localLogger[$logName];
        } else {
            if (self::$logger == null) {
                $client = new \Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@test.xinhuotech.com:8080/2');
                self::$logger = new Logger("logger");         //用来记录全局的异常警告日志
                $handler = new \Monolog\Handler\RavenHandler($client);
                $handler->setFormatter(new \Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
                self::$logger->pushHandler($handler);
            }

            return self::$logger;
        }

    }

}