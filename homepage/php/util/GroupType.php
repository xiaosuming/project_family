<?php
/**
 * 群体类型值
 */
namespace Util;

// 1是家族，2是家庭，3是同学，4是朋友/兴趣
class GroupType {
    const BIG_FAMILY = '1';
    const SMALL_FAMILY = '2';
    const CLASSMATE = '3';
    const FRIENDS = '4';
}