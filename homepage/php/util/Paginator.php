<?php
/**
 * 分页器封装
 * @Author: jiangpengfei
 * @Date: 2019-01-09
 */

namespace Util;

class Paginator {
    private $total;
    private $maxId;
    private $sinceId;
    private $maxTimeStamp;
    private $sinceTimeStamp;
    private $data;
    private $lastPage;

    public function __construct($total, $data, $maxId, $sinceId, $lastPage, $maxTimeStamp = 0, $sinceTimeStamp = 0) {
        $this->total = $total;
        $this->data = $data;
        $this->maxId = $maxId;
        $this->sinceId = $sinceId;
        $this->maxTimeStamp = $maxTimeStamp;
        $this->sinceTimeStamp = $sinceTimeStamp;
        $this->lastPage = $lastPage;
    }


    public function printPage() {
        $result['lastPage'] = $this->lastPage;
        $result['total'] = $this->total;
        $result['next'] = [
            'maxId' => $this->maxId,
            'sinceId' => $this->sinceId,
            'maxTimeStamp' => $this->maxTimeStamp,
            'sinceTimeStamp' => $this->sinceTimeStamp
        ];
        $result['paging'] = $this->data;

        Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
    }

    /**
     * Get the value of total
     */ 
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set the value of total
     *
     * @return  self
     */ 
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get the value of maxId
     */ 
    public function getMaxId()
    {
        return $this->maxId;
    }

    /**
     * Set the value of maxId
     *
     * @return  self
     */ 
    public function setMaxId($maxId)
    {
        $this->maxId = $maxId;

        return $this;
    }

    /**
     * Get the value of sinceId
     */ 
    public function getSinceId()
    {
        return $this->sinceId;
    }

    /**
     * Set the value of sinceId
     *
     * @return  self
     */ 
    public function setSinceId($sinceId)
    {
        $this->sinceId = $sinceId;

        return $this;
    }

    /**
     * Get the value of maxTimeStamp
     */ 
    public function getMaxTimeStamp()
    {
        return $this->maxTimeStamp;
    }

    /**
     * Set the value of maxTimeStamp
     *
     * @return  self
     */ 
    public function setMaxTimeStamp($maxTimeStamp)
    {
        $this->maxTimeStamp = $maxTimeStamp;

        return $this;
    }

    /**
     * Get the value of sinceTimeStamp
     */ 
    public function getSinceTimeStamp()
    {
        return $this->sinceTimeStamp;
    }

    /**
     * Set the value of sinceTimeStamp
     *
     * @return  self
     */ 
    public function setSinceTimeStamp($sinceTimeStamp)
    {
        $this->sinceTimeStamp = $sinceTimeStamp;

        return $this;
    }

    /**
     * Get the value of data
     */ 
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */ 
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get the value of lastPage
     */ 
    public function getLastPage()
    {
        return $this->lastPage;
    }

    /**
     * Set the value of lastPage
     *
     * @return  self
     */ 
    public function setLastPage($lastPage)
    {
        $this->lastPage = $lastPage;

        return $this;
    }
}