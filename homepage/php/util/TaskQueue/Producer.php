<?php

namespace Util\TaskQueue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Producer {

    private $connection;
    private $channel;

    public function __construct() {
        $this->connection = new AMQPStreamConnection($GLOBALS['rabbitmq_ip'],$GLOBALS['rabbitmq_port'],$GLOBALS['rabbitmq_user'],$GLOBALS['rabbitmq_pass']);
        $this->channel = $this->connection->channel();
    }

    public function produce($queueName, $data) {
        $this->channel->queue_declare($queueName, false, true, false, false);

        $msg = new AMQPMessage(
            $data,
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );

        $this->channel->basic_publish($msg, '', $queueName);
    }

    public function close() {
        $this->channel->close();
        $this->connection->close();
    }

}