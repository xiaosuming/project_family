<?php
require_once('../../vendor/autoload.php');
use Util\TaskQueue\Producer;

$producer = new Producer();

$a = 10;

while($a > 0) {
    $producer->produce("test_task", $a);

    sleep(0.5);

    $a--;
}
