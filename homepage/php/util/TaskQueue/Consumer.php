<?php

namespace Util\TaskQueue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Consumer {

    private $connection;
    private $channel;

    public function __construct() {

    }

    private function connect() {
        // $host,
        // $port,
        // $user,
        // $password,
        // $vhost = '/',
        // $insist = false,
        // $login_method = 'AMQPLAIN',
        // $login_response = null,
        // $locale = 'en_US',
        // $connection_timeout = 3.0,
        // $read_write_timeout = 3.0,
        // $context = null,
        // $keepalive = false,
        // $heartbeat = 0
        $this->connection = new AMQPStreamConnection(
            $GLOBALS['rabbitmq_ip'],
            $GLOBALS['rabbitmq_port'],
            $GLOBALS['rabbitmq_user'],
            $GLOBALS['rabbitmq_pass'],
            '/',false,'AMQPLAIN',null,'en_US',3.0,3.0,null,true,0
        );
        $this->channel = $this->connection->channel();
    }

    public function cleanup_connection() {
        // Connection might already be closed.
        // Ignoring exceptions.
        try {
            if($this->connection !== null) {
                $this->connection->close();
            }
        } catch (\ErrorException $e) {
        }
    }

    public function consume($queueName, $callback) {
        while(true){
            try {
                $this->connect();
                $this->channel->queue_declare($queueName, false, true, false, false);
                $this->channel->basic_qos(null, 1, null);

                $ackCallback = function($msg) use ($callback) {
                    call_user_func($callback, $msg);
                    //ack
                    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                };

                $this->channel->basic_consume($queueName, '', false, false, false, false, $ackCallback);

                while(count($this->channel->callbacks)) {
                    $this->channel->wait();
                }
            } catch(AMQPIOException $e) {
                echo "AMQP IO exception " . PHP_EOL;
                $this->cleanup_connection();
                usleep(1000000);
            } catch(\RuntimeException $e) {
                echo "Runtime exception " . PHP_EOL;
                $this->cleanup_connection();
                usleep(1000000);
            } catch(\ErrorException $e) {
                echo "Error exception " . PHP_EOL;
                $this->cleanup_connection();
                usleep(1000000);
            }
        
        }
    }

    public function close() {
        $this->channel->close();
        $this->connection->close();
    }

}