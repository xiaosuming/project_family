<?php
/**
 * 接口速率限制类
 * 这个接口依赖于lua脚本，脚本位置：tool/check_and_set.lua
 * 使用Lua脚本前，需要跑一遍tool下的init_xxx_server.php
 * author： 江鹏飞
 * date: 2018-04-16
 */

namespace Util;

use DB\RedisInstanceManager;
use Model\Limiter;

class RateLimit {

    private $sha;
    private $redis;

    public function __construct($sha) {
        $this->sha = $sha;

        $redisManager = RedisInstanceManager::getInstance();
        $this->redis = $redisManager->getStorageInstance();
    }

    /**
     * 获取token
     * @param $bucket bucket的key
     * @param $capacity 容量，这个设的越大，容忍的突发请求越多
     * @param $cycle token的生成周期
     * 
     * @return bool true或者false
     */
    public function getToken($bucket,$capacity,$cycle){
        $time = time();
        $params = array($bucket, $capacity, $time, $cycle);
 
        $result = $this->redis->evalSha($this->sha, $params, 1);
        if($this->redis->getLastError() !== NULL){
            SysLogger::getInstance()->error($this->redis->getLastError());
            return false;
        }
        return $result;
    }

    /**
     * 重置桶,比如在用户登录成功后，会将桶直接重置为满容量的
     * @param $bucket 桶的名字
     * @param $limiter 该桶对应的限速策略
     * @return bool true或false
     */
    public function resetBucket($bucket, Limiter $limiter) {
        $time = time();
 
        $result = $this->redis->set($bucket, $limiter->capacity.' '.$time);
        return $result;
    }

}
