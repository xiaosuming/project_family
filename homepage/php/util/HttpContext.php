<?php
/**
 * 一次Http请求的上下文
 * @author: jiangpengfei
 * @date:   2019-04-25
 */
namespace Util;

class HttpContext {
    private static $instance = null;
    public $envs;

    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->envs = [];
    }

    public function setEnv($k, $v) {
        $this->envs[$k] = $v;
    }

    public function getEnv($k) {
        if (isset($this->envs[$k])) {
            return $this->envs[$k];
        } else {
            return false;
        }
       
    }
}
