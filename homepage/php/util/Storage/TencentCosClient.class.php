<?php

/**
 * 腾讯云的存储接口封装
 */

namespace Util\Storage;

use Util\Storage\Storage;
use Qcloud\Cos\Client;
use Util\Util;
use Config\IPMap;

class TencentCosClient implements Storage {

    private $bucket;
    private $cosClient;
    private $lastError;

    public function __construct() {
        $this->cosClient = new Client(array(
            'region' => 'ap-shanghai',
            'credentials'=> array(
                'secretId'    => 'AKIDlT4YsDA3WvDPPEI8A1jFZi2FIYdW3k8h',
                'secretKey' => 'c4OXZQ6as25A9xa1KVWRAi6SqIBuSzib'
            )
        ));
    }

    public function setBucket($bucket) {
        $this->bucket = $bucket;
    }

    /**
     * 上传文件
     * @param $localFile 本地要上传的文件
     * @param $ext_name 文件的扩展名
     * @param $bucketName 要上传的桶名
     * @return mix 成功返回数组信息，失败返回false
     */
    public function upload($localFile,$ext_name) : UploadResult{
        $uploadResult = new UploadResult();

        try {

            $randomName = Util::generateRandomCode(20).time().'.'.$ext_name;
            
            $readFile = fopen($localFile, 'rb');

            $result = $this->cosClient->upload($this->bucket,$randomName,$readFile,array());

            $result_location = $result->get('Location');    // 这里的location有的时候没有http,非常奇怪

            if (substr($result_location, 0, 4) != 'http') {
                $result_location = 'http://'.$result_location;
            }

            $url_arr = parse_url($result_location);

            $port = isset($url_arr['port']) ? ':'.$url_arr['port'] : '';

            $source_ip = $url_arr['host'].$port;
            $companyDomain = IPMap::getInstance()->getCompanyDomain($source_ip);

            $uploadResult->location = "https://" . $companyDomain .$url_arr['path'];
            $uploadResult->remoteFileName = $result->get('Key');
            $uploadResult->size = filesize($localFile);
            $uploadResult->bucketName = $result->get('Bucket');
            $uploadResult->sourceIp = $source_ip;
        } catch (\Exception $e) {
            $this->lastError = $e;
            $uploadResult->success = false;
        }

        return $uploadResult;
    }

    /**
     * 使用二进制流上传文件
     * @param $blob 二进制流
     * @param $ext_name 扩展名
     * @return mix 成功返回数组信息，失败返回false
     */
    public function uploadBlob($blob,$ext_name) : UploadResult{

        $uploadResult = new UploadResult();

        try {

            $randomName = Util::generateRandomCode(20).time().'.'.$ext_name;
            

            $result = $this->cosClient->upload($this->bucket,$randomName,$blob,array());

            $result_location = $result->get('Location');    // 这里的location有的时候没有http,非常奇怪

            if (substr($result_location, 0, 4) != 'http') {
                $result_location = 'http://'.$result_location;
            }

            $url_arr = parse_url($result_location);
            
            $port = isset($url_arr['port']) ? ':'.$url_arr['port'] : '';

            $source_ip = $url_arr['host'].$port;
            $companyDomain = IPMap::getInstance()->getCompanyDomain($source_ip);

            $uploadResult->location = "https://" . $companyDomain .$url_arr['path'];
            $uploadResult->remoteFileName = $result->get('Key');
            $uploadResult->size = strlen($blob);
            $uploadResult->bucketName = $result->get('Bucket');
            $uploadResult->sourceIp = $source_ip;
        } catch (\Exception $e) {
            $this->lastError = $e;
            $uploadResult->success = false;
        }

        return $uploadResult;
    }


    /**
     * 下载文件到内存中
     * @param $bucketName 文件所在的桶名
     * @param $remote_filename 远程存储的文件名
     * @return bytes 内存buffer
     */
    public function download_to_buff($bucketName,$remote_filename) {

        try {
            $result = $this->cosClient->getObject(array(
                //bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
                'Bucket' => $bucketName,
                'Key' => $remote_filename));

            return $result->get('Body');
        } catch (\Exception $e) {
            $this->lastError = $e;
            return false;
        }
    }

    /** 
     * 下载文件到本地文件中
     * @param $bucketName 文件所在的桶名
     * @param $remote_filename  远程存储的文件名
     * @param $dst_localfile  下载到的文件名
     * @return boolean 下载成功返回true,失败返回false
     */
    public function download_to_file($bucketName,$remote_filename,$dst_localfile) {

        try {
            $result = $this->cosClient->getObject(array(
                //bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
                'Bucket' => $group_name,
                'Key' => $remote_filename,
                'SaveAs' => $dst_localfile));
        
            return true;
        } catch (\Exception $e) {
            $this->lastError = $e;
            return false;
        }
    }

    /**
     * 删除文件
     * @param $group_name 文件所在的组名（卷名)
     * @param $remote_filename 远程存储的文件名
     * @return boolean 删除成功返回true，失败返回false
     */
    public function delete($bucketName,$remote_filename) {

        try {

            $result = $this->cosClient->deleteObject(array(
                //bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
                'Bucket' => $bucketName,
                'Key' => $remote_filename));

            return true;

        } catch (\Exception $e) {
            $this->lastError = $e;
            return false;
        }
    }

    /**
     * 检查文件是否存在
     * @param $bucketName 文件所在的组名（卷名)
     * @param $remote_filename 远程存储的文件名
     * @return boolean 存在返回true,失败返回false
     */
    public function exists($bucketName,$remote_filename) {
        try {
            $result = $this->cosClient->headObject(
                array('Bucket' => bucketName, 'Key' => $remote_filename)
            );
            return true;
        } catch (\Exception $e) {
            $this->lastError = $e;
            return false;
        }
    }

    /**
     * 获取文件信息
     * @param $group_name 文件所在的组名（卷名)
     * @param $remote_filename 远程存储的文件名
     * @return mix 成功时返回文件信息数组，失败时返回false
     */
    public function get_file_info($bucketName,$remote_filename) : FileInfo {
        try {
            $result = $this->cosClient->headObject(
                array('Bucket' => $bucketName, 'Key' => $remote_filename)
            );

            $fileInfo = new FileInfo();
            $fileInfo->sourceIp = '';
            $fileInfo->size = $result->get('ContentLength');

            return $result;
        } catch (\Exception $e) {
            $this->lastError = $e;
            return false;
        }
    }

    /**
     * 获取最后一个错误信息
     * @return string 错误信息
     */
    public function get_last_error_info() {
        return $this->lastError;
    }
}