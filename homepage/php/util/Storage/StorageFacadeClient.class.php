<?php

namespace Util\Storage;


class StorageFacadeClient {

    private $bucket;
    private $client;

    public function __construct() {
        // $this->client = new FastDFSClient();

        $this->client = new TencentCosClient();
    }

    public function setBucket($bucket) {
        $this->client->setBucket($bucket);
    }

    /**
     * 上传文件
     * @param $localFile 本地要上传的文件
     * @param $ext_name 文件的扩展名
     * @param $bucketName 要上传的桶名
     * @return mix 成功返回数组信息，失败返回false
     */
    public function upload($localFile,$ext_name) : UploadResult{
        return $this->client->upload($localFile,$ext_name);
    }

    /**
     * 使用二进制流上传文件
     * @param $blob 二进制流
     * @param $ext_name 扩展名
     * @return mix 成功返回数组信息，失败返回false
     */
    public function uploadBlob($blob,$ext_name) : UploadResult{
        return $this->client->uploadBlob($blob,$ext_name);
    }


    /**
     * 下载文件到内存中
     * @param $group_name 文件所在的组名（卷名）
     * @param $remote_filename 远程存储的文件名
     * @return bytes 内存buffer
     */
    public function download_to_buff($group_name,$remote_filename) {
        return $this->client->download_to_buff($group_name,$remote_filename);
    }

    /** 
     * 下载文件到本地文件中
     * @param $group_name 文件所在的组名（卷名)
     * @param $remote_filename  远程存储的文件名
     * @param $dst_localfile  下载到的文件名
     * @return boolean 下载成功返回true,失败返回false
     */
    public function download_to_file($group_name,$remote_filename,$dst_localfile) {
        return $this->client->download_to_file($group_name,$remote_filename,$dst_localfile);
    }

    /**
     * 删除文件
     * @param $group_name 文件所在的组名（卷名)
     * @param $remote_filename 远程存储的文件名
     * @return boolean 下载成功返回true，失败返回false
     */
    public function delete($group_name,$remote_filename) {
        return $this->client->delete($group_name,$remote_filename);
    }

    /**
     * 检查文件是否存在
     * @param $group_name 文件所在的组名（卷名)
     * @param $remote_filename 远程存储的文件名
     * @return boolean 存在返回true,失败返回false
     */
    public function exists($group_name,$remote_filename) {
        return $this->client->exists($group_name,$remote_filename);
    }

    /**
     * 获取文件信息
     * @param $group_name 文件所在的组名（卷名)
     * @param $remote_filename 远程存储的文件名
     * @return mix 成功时返回文件信息数组，失败时返回false
     */
    public function get_file_info($group_name,$remote_filename) : FileInfo {
        return $this->client->get_file_info($group_name,$remote_filename);
    }

    /**
     * 获取最后一个错误信息
     * @return string 错误信息
     */
    public function get_last_error_info() {
        return $this->client->get_last_error_info();
    }
}