<?php
/**
 * 这里用来封装一些系统常量
 * @Author: jiangpengfei
 * @Date:   2019-02-20
 */

namespace Util;

class SysConst {
    public static $FAMILY_SNAPSHOT_LOCK = 'family_snapshot_lock';
    public static $FAMILY_LOCK = 'family_lock';
    public static $GROUP_TYPE_FAMILY = 1; // 1是家族
    public static $GROUP_TYPE_MINIFAMILY = 2; // 2是家庭，
    public static $GROUP_TYPE_CLASSMATE = 3;  // 3是同学，
    public static $GROUP_TYPE_MENTOR = 4;       // 4是师徒
    public static $GROUP_TYPE_WORKMATE = 5;  // 5是同事

    public static $ORDER_STATUS_UNPAY = 0;          // 订单未支付
    public static $ORDER_STATUS_NEED_CONFIRM = 1;   // 订单需要确认
    public static $ORDER_STATUS_SUCCESS = 2;        // 订单支付成功
    public static $ORDER_STATUS_FAIL = 3;           // 订单支付失败
    public static $ORDER_STATUS_SHIP = 4;           // 订单发货
    public static $ORDER_STATUS_SHIP_CONFIRM = 5;   // 订单发货已确认
    public static $ORDER_STATUS_CANCEL = 99;        // 订单取消

    public static $ORDER_GOODS_TYPE_VIRTUAL = 1;     // 虚拟商品
    public static $ORDER_GOODS_TYPE_REAL = 2;        // 实物商品
    public static $ORDER_GOODS_TYPE_INFOCARD_CHAT = 3;  // 私信收费

    // 支付方式
    public static $ORDER_PAYWAY_WEIXIN = 1;
    public static $ORDER_PAYWAY_ALIPAY = 2;

    public static $ORDER_WXPAY_DEVICE = 'APP';
    public static $LOOK_FOR_FAMILY_TASK = 'look_for_family_task';
}
