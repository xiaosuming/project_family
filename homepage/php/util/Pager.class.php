<?php

    namespace Util;

    /**
     * 分页类
     * @author jiangpengfei
     * @date 2016-12-10
     */
    use Util\Util;
    class Pager{
        public $firstPage = FALSE;			//boolean型，表示当前页是否是第一页
        public $lastPage = FALSE;			//boolean型，表示当前页是否是最后一页
        public $pageIndex;					//当前页码
        public $pageSize;					//当前页的大小
        public $data;						//数组,当前页的数据列表
        public $total;						//总数
        
        public function __construct($total,$paging,$pageIndex,$pageSize){
            $this->total = $total;
            $this->pageIndex = $pageIndex;
            $this->pageSize = $pageSize;
            $this->data = $paging;
            if($this->pageIndex == 1){
                $this->firstPage = TRUE;
            }
            
            if($this->pageIndex*$this->pageSize >= $this->total){	
                $this->lastPage = TRUE;
            }
        }
        
        public function printPage(){
            $result['firstPage'] = $this->firstPage;
            $result['lastPage'] = $this->lastPage;
            $result['pageIndex'] = $this->pageIndex;
            $result['pageSize'] = $this->pageSize;
            $result['paging'] = $this->data;
            $result['total'] = $this->total;
            
            Util::printResult($GLOBALS['ERROR_SUCCESS'], $result);
            
        }
    }
?>