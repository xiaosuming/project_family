<?php

namespace Util;

class Util
{

    //生成随机码
    public static function generateRandomCode($len)
    {
        $code = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $str = "";
        $strlen = strlen($code);
        for ($i = 0; $i < $len; $i++) {
            $str = $str . substr($code, rand(0, $strlen - 1), 1);
        }
        return $str;
    }

    /**
     * 获取当前时间
     */
    public static function getCurrentTime()
    {
        return date("Y-m-d H:i:s");
    }

    /**
     * 获取当前日期
     */
    public static function getCurrentDate()
    {
        return date("Y-m-d");
    }

    /**
     * 获取请求ip
     */

    public static function getRequestIp()
    {
        if(isset($_SERVER['HTTP_X_REAL_IP']) && !empty($_SERVER['HTTP_X_REAL_IP'])){
            return $_SERVER['HTTP_X_REAL_IP'];
        }
        else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }elseif(isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR'])){
            return $_SERVER["REMOTE_ADDR"];
        }else{
            return '0.0.0.0';
        }
    }

    
    /**
     * 获取请求端口号
     */

    public static function getRequestPort()
    {
        if(isset($_SERVER['HTTP_X_REAL_PORT']) && !empty($_SERVER['HTTP_X_REAL_PORT'])){
            return $_SERVER['HTTP_X_REAL_PORT'];
        }elseif(isset($_SERVER['REMOTE_PORT']) && !empty($_SERVER['REMOTE_PORT'])){
            return $_SERVER["REMOTE_PORT"];
        }else{
            return '80';
        }
    }


    /**
     * 输出结果的函数
     * @param success 是否成功 true是成功，false是失败
     */
    public static function printResult($errorCode, $data)
    {
        $result['errorCode'] = $errorCode;
        if (is_array($data)) {
            $result['msg'] = "";
            $result['data'] = $data;
        } else {
            $result['msg'] = $data;
            $result['data'] = new \stdClass();
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 格式化日期,yy-mm-dd hh:ii:ss 转换成yy-mm-dd
     */
    public static function dateFormat($date)
    {
        if ($date == '' || $date == null)
            return null;
        $date = date_create($date);
        $newDate = date_format($date, "Y-m-d");
        return $newDate;
    }

    /**
     * 日期比较
     * @param $date1 日期一
     * @param $date2 日期二
     * @return 负数代表日期一早于日期二，0代表日期一等于日期二，正数代表日期一迟于日期二
     */
    public static function dateCompare($date1, $date2)
    {
        $unixDateTime1 = strtotime($date1);
        $unixDateTime2 = strtotime($date2);
        return $unixDateTime1 - $unixDateTime2;
    }

    /**
     * 获取当前时间, 1547002747.0286901 单位是秒
     */
    public static function getMicroTime()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * 获取当前的毫秒数
     */
    public static function getTotalMicroTime()
    {
        $time = explode(" ", microtime());
        $time = $time [1] . ($time [0] * 1000);
        $time2 = explode(".", $time);
        $time = $time2 [0];
        return $time;
    }

    public static function exceptionFormat($e)
    {
        $line = $e->getLine();
        $file = $e->getFile();
        $message = $e->getMessage();
        $trace = json_encode($e->getTrace());
        return "$file($line):$message\n$trace\n";
    }

    public static function generateRegisterEmail($auth, $username)
    {
        $link = $GLOBALS['DOMAIN_NAME'] . $GLOBALS['ROUTER_TYPE'] . "/auth/{$auth}";
        return MailModel::getVerifyModel($auth, $username, $link);
    }

    public static function generateEmailBindingEmail($auth, $username)
    {
        $link = $GLOBALS['DOMAIN_NAME'] . $GLOBALS['ROUTER_TYPE'] . "/authemailbind/{$auth}";
        return MailModel::getBindModel($auth, $username, $link);
    }

    public static function generateRetrieveEmail($auth, $username)
    {
        $link = $GLOBALS['DOMAIN_NAME'] . $GLOBALS['ROUTER_TYPE'] . "/retrieve/{$auth}";
        //$link = "http://{$_SERVER['HTTP_HOST']}".$GLOBALS['ROUTER_TYPE']."/retrieve/{$auth}";
        return MailModel::getRetrieveModel($auth, $username, $link);
    }


    /**
     * 生成验证码
     * @param $num 验证码位数
     * @return string 验证码
     */
    public static function generateVcode($num)
    {
        $res = "";
        while (($num--) > 0) {
            $res .= rand(0, 9);
        }
        return $res;
    }

    /**
     * 生成短信验证序号
     * @param $num 验证码位数
     * @return string 验证码
     */
    public static function generateOrder($num)
    {
        $res = "";
        while (($num--) > 0) {
            $res .= rand(0, 9);
        }
        return $res;
    }

    /**
     * 生成二维码
     * @param $string 二维码存储的信息
     * @return mix 二维码的存储地址string,-1代表网络错误，-2代表上传失败
     */
    public static function generateQRCode($string)
    {
        $tmpName = Util::generateRandomCode(18);//18为随机数字
        $path = "/tmp/$tmpName.png";

        //二维码的缓存地址
        //第一个参数是二维码的内容，
        //第二个参数是文件路径，
        //第三个参数可以是'L',M','Q','H',ECC level，应该是二维码的容错率
        //第三个参数是点的大小,
        //第五个参数是边框的大小,
        \PHPQRCode\QRcode::png($string, $path, 'H', 4, 2);
        //添加公司logo位于二维码中央
        $QR = $path;
        $logo = "assets/logo.png"; //放logo图片的地址
        if ($logo !== FALSE) {
            $QR = imagecreatefromstring(file_get_contents($QR));
            $logo = imagecreatefromstring(file_get_contents($logo));
            $QR_width = imagesx($QR);//二维码图片宽度
            $QR_height = imagesy($QR);//二维码图片高度
            $logo_width = imagesx($logo);//logo图片宽度
            $logo_height = imagesy($logo);//logo图片高度
            $logo_qr_width = $QR_width / 3.5; //3.5 控制logo与二维码的比例
            $scale = $logo_width / $logo_qr_width;
            $logo_qr_height = $logo_height / $scale;
            $from_width = ($QR_width - $logo_qr_width) / 2;
            //重新组合图片并调整大小
            imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        }
        imagepng($QR, $path);
        imagedestroy($QR);

        $qrModule = $GLOBALS['QR_MODULE'];
        //使用curl传到远程服务器
        $data = array(
            'id_token' => $GLOBALS['userId'] . '|' . $GLOBALS['token'] . '|' . $GLOBALS['usingDevice'],
            'file' => new \CURLFile($path),
            'action' => 'file_action',
            'sub_action' => 'addFile',
            'module' => $qrModule,
            'deviceCode' => $GLOBALS['deviceCode']
        );

        //上传到图片服务器
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $GLOBALS['IMGSERVER']);
        curl_setopt($ch, CURLOPT_POST, true); //post方式上传
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //成功返回true，输出内容
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); //提交的数据
        $curl_result = curl_exec($ch); //执行提交
        curl_close($ch);//关闭
        if ($res = json_decode($curl_result, true)) {
            if ($res['errorCode'] == 0) {
                //获取到上传的地址
                $location = $res['data']['location'];
                //删除临时缓存文件
                if (is_file($path)) {
                    unlink($path);
                }
                return $location;
            } else {
                return $res['errorCode'];       //返回错误码
            }
        } else {
            return -1;
        }
    }

    public static function decodeUnicode($str)
    {
        return preg_replace_callback('/\\\\u([0-9a-f]{4})/i',
            create_function(
                '$matches',
                'return mb_convert_encoding(pack("H*", $matches[1]), "UTF-8", "UCS-2BE");'
            ),
            $str);
    }

    public static function generateSigKey($identifier)
    {
        return 'user_sig_' . $identifier;
    }

    /**
     * 将数组转换成以逗号分割的字符串
     */
    public static function arrayToString($array)
    {
        if (!is_array($array)) {
            $array = json_decode($array, true);
        }

        $str = '';
        $isFirst = true;
        foreach ($array as $item) {
            if ($isFirst) {
                $str .= $item;
                $isFirst = false;
            } else {
                $str .= ",$item";
            }
        }

        return $str;
    }

    /**
     * 获取@的用户信息,一般格式为@{uid:2,nickname:joyme}
     */
    public static function getAtUser(string $content)
    {
        $pattern = '|@{uid:(\d+),nickname:(.+)}|U';

        $result = array();
        preg_match_all($pattern, $content, $result, PREG_SET_ORDER);

        //为了防止重复at,这里要进行去重
        $len = count($result);

        $users = array();

        foreach ($result as $value) {
            $users[$value[1]] = $value[2];
        }

        return $users;

    }

    /**
     * 从字符串中截取有效的域名
     * @param string $str 字符串
     * @return string 有效域名或者空字符串
     */
    public static function getValidDomain(string $str)
    {

        if (strpos($str, 'http') === false) {
            // 不是http开头，补上
            $str = 'http://' . $str;
        }
        $arr = parse_url($str);

        if (isset($arr['host'])) {
            $url = $arr['host'];
            if (isset($arr['port'])) {
                $url .= ':' . $arr['port'];
            }

            return $url;
        } else {
            return null;
        }
    }

    /**
     * 关系数组的解析
     */
    public static function relationSplit($str)
    {
        if ($str != "" && $str != NULL) {
            $res = array();
            $tmpRes = explode("|", substr($str, 0, strlen($str) - 1));        //返回一个@id数组
            foreach ($tmpRes as $tmpId) {
                array_push($res, substr($tmpId, 1));             // 截掉@
            }

            return $res;
        } else
            return array();                                            //返回一个空数组
    }

    /**
     * 将数组解析成关系字符串
     */
    public static function arrayToRelation($arr)
    {
        $resStr = '';
        foreach ($arr as $id) {
            $resStr .= '@' . $id . '|';
        }

        return $resStr;
    }

    public static function getDiffArrayByPk($arr1, $arr2, $pk = 'pid')
    {
        try {
            $res = [];
            foreach ($arr2 as $item) $tmpArr[$item[$pk]] = $item;
            foreach ($arr1 as $v) if (!isset($tmpArr[$v[$pk]])) $res[] = $v;
            return $res;
        } catch (\Exception $exception) {
            return $arr1;
        }
    }

    /**
     * 从名片的Jobs结构中获取一个职位字符串,如果不存在则返回空字符串
     * @param $jobs 工作经历数组
     * @return string 最新的一个工作职位
     */
    public static function getJobStrFormJobs($jobs)
    {

        if ($jobs == null) {
            return '';
        }

        if (!is_array($jobs)) {
            $jobs = json_decode($jobs, true);
        }

        $jobStr = '';
        $job = array_pop($jobs);
        if ($job != null) {
            $jobStr = $job['job'];
        }

        return $jobStr;
    }
//
//    public static function generateTree($array){
//        //第一步 构造数据
//        $items = array();
//        foreach($array as $value){
//            $items[$value['id']] = $value;
//        }
//        //第二部 遍历数据 生成树状结构
//        $tree = array();
//        //遍历构造的数据
//        foreach($items as $key => $value){
//            //如果pid这个节点存在
//            if(isset($items[$value['pid']])){
//                //把当前的$value放到pid节点的son中 注意 这里传递的是引用 为什么呢？
//                $items[$value['pid']]['son'][] = &$items[$key];
//            }else{
//                $tree[] = &$items[$key];
//            }
//        }
//        return $tree;
//    }

    public static function getChildId($data, $pid)
    {
        static $ret = array();
        foreach ($data as $k => $v) {
            if ($v['pid'] == $pid) {
                $ret[] = $v['id'];
                self::getChildId($data, $v['id']);
            }
        }
        return $ret;
    }

    public static function generateTree($array, $node)
    {
        //第一步 构造数据
        $items = array();
        foreach ($array as $value) {
            $items[$value['id']] = $value;
        }
        //第二部 遍历数据 生成树状结构
        $tree = array();
        //遍历构造的数据
        foreach ($items as $key => $value) {
            //如果pid这个节点存在
            if (isset($items[$value[$node]])) {
                //把当前的$value放到pid节点的son中 注意 这里传递的是引用 为什么呢？
                $items[$value[$node]]['son'][] = &$items[$key];
            } else {
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /**
     * 基于donkeyId的扩展生成唯一的主键id
     */
    public static function getNextId()
    {
        return \dk_get_next_id();
    }

    /**
     * json字符串验证
     * @param $string
     * @return bool
     */
    public static function isJson($string)
    {
        if (is_string($string)) {
            @json_decode($string);
            return (json_last_error() === JSON_ERROR_NONE);
        }
        return false;
    }

    public static function mb_strlen($str, $encoding = null)
    {
        $length = \mb_strlen($str, $encoding);
        if ($length === false) {
            Util::printResult($GLOBALS['ERROR_ENCODING_WRONG'], 'encoding无效');
            exit;
        }

        return $length;
    }

    public static function filterContentReplace($content, $replaceChar = '***')
    {
        if ($content != '') {
            $dirname = dirname(__FILE__);
            $wordData = explode(PHP_EOL, @file_get_contents($dirname . '/../config/keyword.txt'));

            $handle = SensitiveHelper::init()->setTree($wordData);

            $isLegal = $handle->islegal($content);

            if ($isLegal) {
                $filterContent = $handle->replace($content, $replaceChar);
                return $filterContent;
            } else {
                return $content;
            }
        } else {
            return $content;
        }

    }

    /*
     * 解析gener_person中关系人物的数组id
     *
     * */
    public static function idSplit($str)
    {
        if ($str != "" && $str != NULL) {
            $res = array();
            $tmpRes = explode("|", substr($str, 0, strlen($str) - 1));        //返回一个@id数组
            foreach ($tmpRes as $tmpId) {
                array_push($res, substr($tmpId, 1));             // 截掉@
            }

            return $res;
        } else
            return array();
    }

    /**
 *  遍历数组,删除数组中某个值
 */
public static  function array_remove($array,$v){ 

    foreach($array as $key=>$value){
    
            if($value == $v){ 
                    unset($array[$key]); 
    
            } 
    }
    
    return $array;
    
    }

    /*
     * 检查权限
     *
     * */
    public static function checkPermission($permission,$familyIndex=[],$needCheckBranch=true,$index='permission')
    {
        if (in_array($permission[$index], [400, '400'])){return 4;}
        if (in_array($permission[$index], [500, '500'])){return 5;}
        if (in_array($permission[$index], [300, '300'])){
            if ($needCheckBranch && $familyIndex !== []){
                $n = 0;
                foreach ($familyIndex as $f){
                    if ($permission['startIndex'] <= $f && $f <= $permission['endIndex']){
                        $n += 1;
                    }else{
                        return -1;
                    }
                }
                if (count($familyIndex) === $n){
                    return 3;
                }else{
                    return -1;
                }
            }elseif (!$needCheckBranch && $familyIndex !== []){
                return -1;
            }else{
                return 3;
            }
        }
        if (in_array($permission[$index], [200, '200'])){return 2;}
        if (in_array($permission[$index], [100, '100'])){return 1;}
        return -1;
    }


     /**
     * number2chinese description
     *
     * · 个，十，百，千，万，十万，百万，千万，亿，十亿，百亿，千亿，万亿，十万亿，
     *   百万亿，千万亿，兆；此函数亿乘以亿为兆
     *
     * · 以「十」开头，如十五，十万，十亿等。两位数以上，在数字中部出现，则用「一十几」，
     *   如一百一十，一千零一十，一万零一十等
     *
     * · 「二」和「两」的问题。两亿，两万，两千，两百，都可以，但是20只能是二十，
     *   200用二百也更好。22,2222,2222是「二十二亿两千二百二十二万两千二百二十二」
     *
     * · 关于「零」和「〇」的问题，数字中一律用「零」，只有页码、年代等编号中数的空位
     *   才能用「〇」。数位中间无论多少个0，都读成一个「零」。2014是「两千零一十四」，
     *   20014是「二十万零一十四」，201400是「二十万零一千四百」
     *
     * 参考：https://jingyan.baidu.com/article/636f38bb3cfc88d6b946104b.html
     *
     * @param  minx  $number
     * @param  boolean $isRmb
     * @return string
     */
    public static function number2chinese($number, $isRmb = false)
    {
        // 判断正确数字
        if (!preg_match('/^-?\d+(\.\d+)?$/', $number)) {
            throw new Exception('number2chinese() wrong number', 1);
        }
        list($integer, $decimal) = explode('.', $number . '.0');
        // 检测是否为负数
        $symbol = '';
        if (substr($integer, 0, 1) == '-') {
            $symbol = '负';
            $integer = substr($integer, 1);
        }
        if (preg_match('/^-?\d+$/', $number)) {
            $decimal = null;
        }
        $integer = ltrim($integer, '0');
        // 准备参数
        $numArr = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九', '.' => '点'];
        $descArr = ['', '十', '百', '千', '万', '十', '百', '千', '亿', '十', '百', '千', '万亿', '十', '百', '千', '兆', '十', '百', '千'];
        if ($isRmb) {
            $number = substr(sprintf("%.5f", $number), 0, -1);
            $numArr = ['', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖', '.' => '点'];
            $descArr = ['', '拾', '佰', '仟', '万', '拾', '佰', '仟', '亿', '拾', '佰', '仟', '万亿', '拾', '佰', '仟', '兆', '拾', '佰', '仟'];
            $rmbDescArr = ['角', '分', '厘', '毫'];
        }
        // 整数部分拼接
        $integerRes = '';
        $count = strlen($integer);
        if ($count > max(array_keys($descArr))) {
            throw new Exception('number2chinese() number too large.', 1);
        } else if ($count == 0) {
            $integerRes = '零';
        } else {
            for ($i = 0; $i < $count; $i++) {
                $n = $integer[$i]; // 位上的数
                $j = $count - $i - 1; // 单位数组 $descArr 的第几位
                // 零零的读法
                $isLing = $i > 1// 去除首位
                 && $n !== '0' // 本位数字不是零
                 && $integer[$i - 1] === '0'; // 上一位是零
                $cnZero = $isLing ? '零' : '';
                $cnNum = $numArr[$n];
                // 单位读法
                $isEmptyDanwei = ($n == '0' && $j % 4 != 0) // 是零且一断位上
                 || substr($integer, $i - 3, 4) === '0000'; // 四个连续0
                $descMark = isset($cnDesc) ? $cnDesc : '';
                $cnDesc = $isEmptyDanwei ? '' : $descArr[$j];
                // 第一位是一十
                if ($i == 0 && $cnNum == '一' && $cnDesc == '十') {
                    $cnNum = '';
                }

                // 二两的读法
                $isChangeEr = $n > 1 && $cnNum == '二' // 去除首位
                 && !in_array($cnDesc, ['', '十', '百']) // 不读两\两十\两百
                 && $descMark !== '十'; // 不读十两
                if ($isChangeEr) {
                    $cnNum = '两';
                }

                $integerRes .= $cnZero . $cnNum . $cnDesc;
            }
        }
        // 小数部分拼接
        $decimalRes = '';
        $count = strlen($decimal);
        if ($decimal === null) {
            $decimalRes = $isRmb ? '整' : '';
        } else if ($decimal === '0') {
            $decimalRes = '零';
        } else if ($count > max(array_keys($descArr))) {
            throw new Exception('number2chinese() number too large.', 1);
        } else {
            for ($i = 0; $i < $count; $i++) {
                if ($isRmb && $i > count($rmbDescArr) - 1) {
                    break;
                }

                $n = $decimal[$i];
                $cnZero = $n === '0' ? '零' : '';
                $cnNum = $numArr[$n];
                $cnDesc = $isRmb ? $rmbDescArr[$i] : '';
                $decimalRes .= $cnZero . $cnNum . $cnDesc;
            }
        }
        // 拼接结果
        $res = $symbol . ($isRmb ?
            $integerRes . ($decimalRes === '零' ? '元整' : "元$decimalRes") :
            $integerRes . ($decimalRes === '' ? '' : "点$decimalRes"));
        return $res;
    }

}
