<?php
/**
 * Http请求的简单封装
 * @author jiangpengfei
 * @date 2017-09-06
 */
namespace Util;

class Http{
    private $params;
    private $url;
    private $timeout;

    public function __construct(){
        $this->timeout = 3000;
    }
    /**
     * 设置请求地址
     * @param $url 请求地址
     */
    public function setUrl($url){
        $this->url = $url;
    }

    /**
     * 设置超时时间
     */
    public function setTimeout($timeout){
        $this->timeout = $timeout;
    }

    /**
     * 设置要发送的数据参数
     * @param $params 参数map
     */
    public function setParams($params){
        $this->params = $params;
    }

    /**
     * 添加数据参数
     * @param $key 键
     * @param $value 值
     */
    public function appendParam($key,$value){
        $this->params[$key] = $value;
    }

    /**
     * get请求
     * @return string 请求到的内容
     */
    public function get(){
        $curl = curl_init(); 
        curl_setopt($curl,CURLOPT_URL,$this->url."?".$this->getParamsString()); 
        curl_setopt($curl,CURLOPT_TIMEOUT,$this->timeout); 
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true); 
        $res = curl_exec($curl); 
        curl_close($curl);

        return $res;
    }

    /**
     * post请求
     */
    public function post(){
        $curl = curl_init(); 
        curl_setopt($curl,CURLOPT_URL,$this->url); 
        curl_setopt($curl,CURLOPT_TIMEOUT,$this->timeout); 
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true); 
        curl_setopt($curl,CURLOPT_POST,true); 
        curl_setopt($curl,CURLOPT_POSTFIELDS,$this->params); 
        $res = curl_exec($curl); 
        curl_close($curl);

        return $res;
    }

    /**
     * 带有自定义header的GET请求
     * @param $header header头，是一个array
     * @return map 状态码和请求内容 {'status':'200','data':'...'}
     */
    public function getWithHeader($header){
        $curl = curl_init(); 
        curl_setopt($curl,CURLOPT_URL,$this->url); 
        curl_setopt($curl,CURLOPT_TIMEOUT,$this->timeout); 
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true); 
        curl_setopt($curl,CURLOPT_POST,true); 

        //设置请求头
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);

        curl_setopt($curl,CURLOPT_POSTFIELDS,$this->params); 
        $res = curl_exec($curl); 
        $httpCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);  
        curl_close($curl);

        $result['status'] = $httpCode;
        $result['data'] = $res;
        return $result;
    }

    /**
     * 带有自定义header的POST请求
     * @param $header header头，是一个array
     * @return  map 状态码和请求内容 {'status':'200','data':'...'}
     */
    public function postWithHeader($header){
        $curl = curl_init(); 
        curl_setopt($curl,CURLOPT_URL,$this->url); 
        curl_setopt($curl,CURLOPT_TIMEOUT,$this->timeout); 
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true); 

        //设置请求头
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);

        curl_setopt($curl,CURLOPT_POST,true); 
        curl_setopt($curl,CURLOPT_POSTFIELDS,$this->params); 
        $res = curl_exec($curl); 
        $httpCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);  
        curl_close($curl);

        $result['status'] = $httpCode;
        $result['data'] = $res;
        return $result;
    }

    /**
     * DELETE请求
     * 
     */
    public function delete(){
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$this->url); 
        curl_setopt($curl,CURLOPT_POSTFIELDS,$this->params);
        curl_setopt($curl,CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($curl,CURLOPT_HEADER,0);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"DELETE");
        curl_setopt($curl,CURLOPT_TIMEOUT,$this->timeout);
        $res = curl_exec($curl);
        $httpCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);

        $result['status'] = $httpCode;
        $result['data'] = $res;
        return $result;
    }

    /**
     * 自定义请求头的DELETE请求
     */
    public function deleteWithHeader($header){
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$this->url); 
        curl_setopt($curl,CURLOPT_POSTFIELDS,$this->params);
        curl_setopt($curl,CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($curl,CURLOPT_HEADER,0);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"DELETE");
        //设置请求头
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl,CURLOPT_TIMEOUT,$this->timeout);
        $res = curl_exec($curl);
        $httpCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);

        $result['status'] = $httpCode;
        $result['data'] = $res;
        return $result;
    }

    /**
     * 下载文件到缓存区
     * @param $url 要下载的文件地址
     * @return mix 文件buffer
     */
    public static function downloadToBuffer(string $url){
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$url); 
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_TIMEOUT,3000);
        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }

    /** 
     * 将参数构造成字符串
     * @return string 参数字符串
     */
    private function getParamsString(){
        $res = null;
        $isFirst = true;
        foreach($this->params as $key => $value){
            if($isFirst){
                $res = $res . $key . "=" .$value;
                $isFirst = false;
            }else{
                $res = $res . "&". $key . "=" .$value;
            }
        }

        return $res;
    }
};