<?php
/**
 * 二维码的动作封装
 * @author: jiangpengfei
 * @date:   2019-03-29
 */

namespace Util;

class QRCodeAction {
    public static $JOIN_GROUP = 'JOIN_GROUP'; // 加入族群
    
    // 动态码接收名片，动态码有过期时间
    public static $DYNAMIC_RECEIVE_INFOCARD = 'DYNAMIC_RECEIVE_INFOCARD';
    // 静态码接收名片，静态码长期有效
    public static $STATIC_RECEIVE_INFOCARD = 'STATIC_RECEIVE_INFOCARD';
    // 名片的发送码，5分钟内有效，其他用户扫一扫就可以接收
    public static $DYNAMIC_SEND_INFOCARD = 'DYNAMIC_SEND_INFOCARD';
    // 名片的发送码，永久有效的
    public static $STATIC_SEND_INFOCARD = 'STATIC_SEND_INFOCARD';
    // 名片的群体交换码
    public static $INFOCARD_GROUP_EXCHANGE = 'INFOCARD_GROUP_EXCHANGE';


    public static function inQRCodeAction($do, $data) {
        $reflection = new \ReflectionClass('Util\QRCodeAction');
        $keys = $reflection->getStaticProperties();
        
        foreach($keys as $key) {
            if ($do == $reflection->getStaticPropertyValue($key)) {
                // 检查这个动作中的数据是否完整
                return self::checkQRCodeData($do, $data);
            }
        }

        return false;
    }

    /**
     * 检查二维码中的数据字段是否完整
     * @param $do 动作
     * @param $data 数据
     */
    public static function checkQRCodeData($do, $data) {
        $data = json_decode($data, true);
        switch($do) {
            case self::$JOIN_GROUP:
                return self::checkJSONParams($data, ['identity']);
                break;
            case self::$DYNAMIC_RECEIVE_INFOCARD:
                return self::checkJSONParams($data, ['receiveCode']);
                break;
            case self::$STATIC_RECEIVE_INFOCARD:
                return self::checkJSONParams($data, ['receiveCode']);
                break;
            case self::$DYNAMIC_SEND_INFOCARD:
                return self::checkJSONParams($data, ['sendCode', 'receiveCode']);
                break;
            case self::$STATIC_SEND_INFOCARD:
                return self::checkJSONParams($data, ['sendCode', 'receiveCode']);
                break;
            case self::$INFOCARD_GROUP_EXCHANGE:
                return self::checkJSONParams($data, ['code', 'exchangeId', 'hasPassword']);
                break;
        }

        return new \Exception('不识别的动作');
    }

    /**
     * 检查json中的键是否完整
     * @param $json json对象
     * @param $keys 需要包含的key数组
     */
    public static function checkJSONParams($json, $keys) {
        foreach($json as $key => $value) {
            if (!in_array($key, $keys)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 根据动作返回相应的二维码前缀
     * @param $do 动作
     * @return string 前缀字符串
     */
    public static function prefix($do) {

        $aiFamilyPrefix = 'https://m.izuqun.com/family.html?data=';
        $infoCardPrefix = 'https://m.izuqun.com/ic.html?data=';

        switch($do) {
            case self::$JOIN_GROUP:
                return $aiFamilyPrefix;
                break;
            case self::$DYNAMIC_RECEIVE_INFOCARD:
                return $infoCardPrefix;
                break;
            case self::$STATIC_RECEIVE_INFOCARD:
                return $infoCardPrefix;
                break;
            case self::$DYNAMIC_SEND_INFOCARD:
                return $infoCardPrefix;
                break;
            case self::$STATIC_SEND_INFOCARD:
                return $infoCardPrefix;
                break;
            case self::$INFOCARD_GROUP_EXCHANGE:
                return $infoCardPrefix;
                break;
        }

        return new \Exception('不识别的动作');
    }
}