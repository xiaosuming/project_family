<?php
/**
 * 图片处理类:为图片添加文字（家族封面 + 家族姓氏）
 * 
 * 
 */

namespace Util;

use Util\Util;

class ImageAddtext{

	function __construct(){
		
		//文件输出路径
		$this->outpath = __DIR__.'/../assets/tmpfile/';
		//字体路径
		$this->fontpath = __DIR__.'/../assets/fonts/';
		//背景图路径
		$this->backpath =  __DIR__.'/../assets/';		
	}

	public function getCoverImage($params){

		$backimg = $this->checkBackImg($params);
		//var_dump($backimg);die;
		//$filename = $params['pinyin'] . '_' . $backimg['coverId'] . '.' .$backimg['suffix'];
		//新文件存放地址
		//$newImg = $this->outpath . $filename;

		//$url = 'http://'.$_SERVER['HTTP_HOST'].'/assets/tmpfile/' .$filename;

		//if(file_exists($newImg)){
			
			//return ['url' => $url];
		//}

		$img = imagecreatefromstring(file_get_contents($backimg['coverImg']));

		//旋转角度 
		$circleSize = 0;
		//字体文件 旁门正道粗体
		$font = $this->fontpath.'pangmenzdct.ttf';

		switch ($backimg['coverId']) {//封面ID

			case '1'://蓝底 单姓 从左到右 默认
				
		        //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 255, 255, 255); 
		        //字体大小 
		        $fontSize = 290; 
		        //左边距 
		        $left = 1918; 
		        //上边距 
		        $top = 990; 
				break;

			case '2'://蓝底 复姓 从左到右 默认

			    //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 255, 255, 255); 
		        //字体大小 
		        $fontSize = 290; 
		        //左边距 
		        $left = 1918; 
		        //上边距 
		        $top = 920; 

		        //文字拆分成两个
		        $surname_1 =  mb_substr($params['surname'], 0, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $surname_1);

		        $surname_2 =  mb_substr($params['surname'], 1, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top + 400 , $black, $font, $surname_2);

		        $params['surname'] = '';

				break;

			case '3'://白底 单姓 从左到右 默认

		        //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 0, 0, 0); 
		        //字体大小 
		        $fontSize = 420;
		        //左边距 
		        $left = 982; 
		        //上边距 
		        $top = 1150; 
		        //拼音左右加横线（矩形框）
		        imagefilledrectangle($img, 450, 2700, 750, 2705, $black);
		        imagefilledrectangle($img, 1730, 2700, 2030, 2705, $black);
		        //拼音字体 苹方
		        $Pfont = $this->fontpath.'pingfang.ttf'; 
		        //拼音字体大小
		        $PfontSize = 70;
		        $pinyin = strtoupper($params['pinyin'].' SHI ZONG PU'); //zhang氏宗谱
		        $fontBox = imagettfbbox($PfontSize, 0, $Pfont, $pinyin);
		        //拼音水平居中
		        imagettftext($img, $PfontSize, 0, ceil(($backimg['Width'] - $fontBox[2])/ 2), 2730, $black, $Pfont, $pinyin);

				break;

			case '4'://白底 复姓 从左到右 默认

			    //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 0, 0, 0); 
		        //字体大小 
		        $fontSize = 320;
		        //左边距 
		        $left = 982; 
		        //上边距 
		        $top = 950;

		        //文字拆分成两个
		        $surname_1 =  mb_substr($params['surname'], 0, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $surname_1);

		        $surname_2 =  mb_substr($params['surname'], 1, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top + 450 , $black, $font, $surname_2);

		        $params['surname'] = '';

		        //拼音左右加横线（矩形框）
		        imagefilledrectangle($img, 450, 2700, 750, 2705, $black);
		        imagefilledrectangle($img, 1730, 2700, 2030, 2705, $black);
		        //拼音字体 苹方
		        $Pfont = $this->fontpath.'pingfang.ttf'; 
		        //拼音字体大小
		        $PfontSize = 60;
		        $pinyin = strtoupper($params['pinyin'].' SHI ZONG PU'); //zhang氏宗谱
		        $fontBox = imagettfbbox($PfontSize, 0, $Pfont, $pinyin);
		        //拼音水平居中
		        imagettftext($img, $PfontSize, 0, ceil(($backimg['Width'] - $fontBox[2])/ 2), 2730, $black, $Pfont, $pinyin);

				break;

			case '5'://蓝底 单姓 从右到左 -RToL-
				
		        //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 255, 255, 255); 
		        //字体大小 
		        $fontSize = 290; 
		        //左边距 
		        $left = 170; 
		        //上边距 
		        $top = 990; 
				break;

			case '6'://蓝底 复姓 从右到左 -RToL-

			    //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 255, 255, 255); 
		        //字体大小 
		        $fontSize = 290; 
		        //左边距 
		        $left = 170; 
		        //上边距 
		        $top = 920; 

		        //文字拆分成两个
		        $surname_1 =  mb_substr($params['surname'], 0, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $surname_1);

		        $surname_2 =  mb_substr($params['surname'], 1, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top + 400 , $black, $font, $surname_2);

		        $params['surname'] = '';

				break;
			
			default:
				break;
		}

		//姓氏添加
		if($params['surname'] != ''){
		    imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $params['surname']);
		}
		switch ($backimg['Type']) {
		  case 1://gif
		    header("Content-type: image/gif");
		    imagegif($img); 
		    break; 
		  case 2://jpg
		    header("Content-type: image/jpg");
		    imagejpeg($img);
		    break; 
		  case 3://png
		    header("Content-type: image/png");
		    imagepng($img);
		    break; 
		  default: 
		    break; 
		 } 
		 //销毁照片 
		 imagedestroy($img);
		 //return ['url' => $url];

	}

	private function checkBackImg($params){
		
		$res = [
			'coverImg' => $this->backpath . 'backgroud-blue.png',
			'coverId' => 1,
	    ];//默认蓝底 单姓
		$surnameLength = mb_strlen($params['surname']);

		switch ($params['id']){
			case 1: //蓝底
			    if($surnameLength == 1){ //单姓
			    	if(isset($params['direction']) && $params['direction'] == 1 ){ // 从右到左 -RTOL-
			    		$res['coverImg'] = $this->backpath . 'backgroud-RToL-blue.png';
			    	    $res['coverId'] = 5;
			    	}else{	 // 从左到右 默认 		    		
			    	    $res['coverImg'] = $this->backpath . 'backgroud-blue.png';
			    	    $res['coverId'] = 1;
			    	}
			    }elseif($surnameLength == 2){//复姓
			    	if(isset($params['direction']) && $params['direction'] == 1 ){ // 从右到左 -RTOL-
			    		$res['coverImg'] = $this->backpath . 'backgroud-two-RToL-blue.png';
			    	    $res['coverId'] = 6;
			    	}else{	 // 从左到右 默认 		    		
			    	    $res['coverImg'] = $this->backpath . 'backgroud-two-blue.png';
			    	    $res['coverId'] = 2;
			    	}
			    }
			    break;
			case 2://白底
			    if($surnameLength == 1){ //单姓
			    	$res['coverImg'] = $this->backpath . 'backgroud-white.png';
			    	$res['coverId'] = 3;
			    }elseif($surnameLength == 2){//复姓
			    	$res['coverImg'] = $this->backpath . 'backgroud-two-white.png';
			    	$res['coverId'] = 4;
			    }
			default: 
		    break;
		}
		list($res['Width'], $res['Height'], $res['Type']) = getimagesize($res['coverImg']);
		switch ($res['Type']) {
			case 1://gif 
		    $res['suffix'] = 'gif';
		    break; 
		  case 2://jpg 
		    $res['suffix'] = 'jpg';
		    break; 
		  case 3://png 
		    $res['suffix'] = 'png';
		    break; 
		  default: 
		    break; 
		}
		return $res;

	}

}

;?>
