<?php
/**
 * 弃用
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/12/12 0012
 * Time: 10:29
 */

namespace DB;

use DB\CDBManager;
use DB\CDBPushQuestion;
use Util\Util;

class CDBTask
{
    var $pdo = null;
    var $TABLE = "gener_user_task";
    var $PERSON_TABLE = "gener_person";
    var $TENDER_TABLE = "gener_task_tender";
    var $USER_TABLE = "gener_user";


    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     *
     * 添加悬赏任务
     * @param $userId
     * @param $title
     * @param $content
     * @param $photo
     * @param $point
     */
    public function addTask($userId, $title, $content, $photo, $point)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO $this->TABLE
              (userId,title,content,photo,point,create_time,create_by,update_time,update_by)
              VALUES 
              ('$userId','$title','$content','$photo','$point',now(),'$userId',now(),'$userId')";
        $taskId = $this->pdo->insert($sql);
        if ($taskId > 0) {
            $pushUserIds = $this->getUsers($userId);
            $CDBPushQuestion = new CDBPushQuestion();
            $taskPushResult = $CDBPushQuestion->setPushQuestionsTask($pushUserIds, $taskId, time(), $userId, 2);
            return $taskId;
        }
        // @codeCoverageIgnoreStart
        return false;
        // @codeCoverageIgnoreEnd
    }


    /**
     * 获取所在家族的所有的用户集合
     * @param $userId
     * @return mixed
     */
    public function getUsers($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT distinct userId FROM $this->PERSON_TABLE WHERE userId != '$userId' AND userId != '0' AND familyId in(SELECT familyId FROM $this->PERSON_TABLE WHERE userId = '$userId' )";
        return $this->pdo->query($sql);
    }


    /**
     * 获取指定家族下的用户集合
     * @param int $familyId
     * @param int $userId
     * @return array
     */
    public function getSameFamilyUsers(int $familyId, int $userId): array
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT userId FROM $this->PERSON_TABLE WHERE userId != '$userId' AND userId > 0 AND familyId = '$familyId' ";
        return $this->pdo->query($sql);
    }

    /**
     * 验证用户Id和悬赏任务ID
     * @param $userId
     * @return bool
     */
    public function verifyTaskIdAndUserId($taskId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE WHERE id = '$taskId' AND userId = '$userId' AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);

        if ($result['count(id)'] > 0) {
            return true;
        }
        return false;
    }

    /**
     * 获取悬赏任务的积分
     * @param $taskId
     * @return mixed
     */
    public function getTaskPoint($taskId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT point  FROM $this->TABLE WHERE id='$taskId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        return $result['point'];
    }

    /**
     * 更新悬赏任务
     * @param $title
     * @param $content
     * @param $photo
     * @param $point
     * @param $updateTime
     * @param $updateBy
     * @return mixed
     */
    public function updateTask($taskId, $title, $content, $photo, $point, $updateTime, $updateBy)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET title='$title',content='$content',photo='$photo',point='$point',update_time='$updateTime',update_by='$updateBy' WHERE id='$taskId' AND isDelete=0";
        $updatedRowCount = $this->pdo->update($sql);

        return $updatedRowCount;
    }


    /**
     * 删除悬赏任务
     * @param $taskId
     * @param $userId
     * @return bool
     */
    public function deleteTask($taskId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET isDelete=1 WHERE id='$taskId' AND isDelete=0";
        $count = $this->pdo->update($sql);
        if ($count > 0) {
            $pushUserIds = $this->getUsers($userId);
            $CDBPushQuestion = new CDBPushQuestion();
            $CDBPushQuestion->setDeletePostTask($pushUserIds, $taskId, $userId, 2);
            return $count;
        }
        return false;
    }

    /**
     * 添加用户任务竞标
     * @param $userId
     * @param $taskId
     * @param $content
     * @return bool
     */
    public function addUserTaskTender($userId, $taskId, $content)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO $this->TENDER_TABLE(userId,taskId,content,is_accept,create_time,create_by,update_time,update_by) VALUES('$userId','$taskId','$content','0',now(),'$userId',now(),'$userId')";
        $result = $this->pdo->insert($sql);
        if ($result > 0) {
            return $result;
        }
        // @codeCoverageIgnoreStart
        return false;
        // @codeCoverageIgnoreEnd
    }

    /**
     * @param $userId
     * @param $taskId
     * @return bool
     */
    public function existsUserIdANDTaskIdInTender($userId, $taskId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT count(*) FROM $this->TENDER_TABLE WHERE userId='$userId' AND taskId='$taskId' AND isDelete = 0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['count(*)'] > 0) {
            return true;
        }
        // @codeCoverageIgnoreStart
        return false;
        // @codeCoverageIgnoreEnd
    }

    /**
     * 任务发起人接受用户的竞标
     * @param $taskId
     * @param $acceptUserId
     * @param $userId
     * @return mixed
     */
    public function updateTenderAccept($taskId, $acceptUserId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE SET state='0', update_time=NOW(), update_by='$userId' WHERE id='$taskId'";
            $this->pdo->update($sql);

            $sql = "UPDATE $this->TENDER_TABLE SET is_accept='1', update_time=NOW(), update_by='$userId' WHERE taskId='$taskId' AND userId='$acceptUserId' AND isDelete = 0";
            $result = $this->pdo->update($sql);

            $this->pdo->commit();
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            // @codeCoverageIgnoreEnd
        }
        return $result;
    }

    /**
     * 获取悬赏任务的详情
     * @param $taskId
     * @return bool
     */
    public function getTaskDetail($taskId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT userId,title,content,photo,point,state,create_time as createTime,create_by as createBy,update_time as updateTime, update_by as updateBy FROM $this->TABLE WHERE id='$taskId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        $userId = $result['userId'];
        $sql = "SELECT nickname FROM $this->USER_TABLE WHERE id='$userId'";
        $nickname = $this->pdo->uniqueResult($sql)['nickname'];
        $result['nickname'] = $nickname;
        if ($result != null) {
            return $result;
        }
        //@codeCoverageIgnoreStart
        return false;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 分页获取所有竞标内容
     * @param $taskId
     * @param $pageIndex
     * @param $pageSize
     * @return mixed
     */
    public function getUserTenderPaging($taskId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT t.id,t.userId, u.photo, u.nickname, 
                       t.taskId, t.content, t.is_accept,
                       t.create_time as createTime, t.create_by as createBy,
                       t.update_time as updateTime, t.update_by as updateBy 
                FROM 
                    (SELECT * FROM $this->TENDER_TABLE WHERE taskId='$taskId'AND isDelete = 0 ORDER BY id DESC LIMIT $offset,$pageSize) AS t
                LEFT JOIN
                    (SELECT id, photo, nickname FROM $this->USER_TABLE) AS u
                ON t.userId = u.id";
        return $this->pdo->query($sql);
    }

    /**
     * 获取所有竞标内容的总数
     * @param $taskId
     * @return mixed
     */
    public function getUserTenderTotal($taskId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT count(*) FROM $this->TENDER_TABLE WHERE taskId='$taskId' AND isDelete = 0";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(*)'];
    }

    /**
     * @param $taskId
     * @return bool
     */
    public function existsTaskId($taskId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT count(*) FROM $this->TABLE WHERE id='$taskId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['count(*)'] > 0) {
            return true;
        }
        // @codeCoverageIgnoreStart
        return false;
        // @codeCoverageIgnoreEnd
    }

    /**
     * 获取指定ID集合的任务详情
     * @param $taskIdSet 任務id集合
     * @return null
     */
    public function getTasksDetail($taskIdSet)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($taskIdSet == "()" || $taskIdSet == "" || $taskIdSet[0] != "(" || $taskIdSet[strlen($taskIdSet) - 1] != ")") {
            return null;
        }

        $sql = "SELECT ut.id, ut.userId, ut.title, ut.content, ut.photo,
                       ut.point, ut.state, tt.tenderCount,
                       ut.create_time AS createTime, ut.create_by AS createBy,
                       ut.update_time AS updateTime, ut.update_by AS updateBy
                FROM
	                (SELECT * FROM $this->TABLE WHERE isDelete=0 AND  id IN $taskIdSet) AS ut
                LEFT JOIN
	                (SELECT taskId, COUNT(*) AS tenderCount FROM $this->TENDER_TABLE WHERE isDelete = 0 GROUP BY taskId HAVING taskId IN $taskIdSet) AS tt
                ON ut.id = tt.taskId";

        return $this->pdo->query($sql);
    }

    /**
     * 获取竞标人的信息
     * @param $taskId
     * @return mixed
     */
    public function getTaskTenderInfoByTaskId($taskId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT distinct td.userId,u.nickname,u.photo  FROM $this->TENDER_TABLE as td LEFT JOIN gener_user as u ON td.userId = u.id
                WHERE td.taskId = '$taskId' AND td.isDelete = 0 limit 3";
        return $this->pdo->query($sql);
    }

    /**
     * 获取竞标详情
     * @param $tenderId
     * @return mixed
     */
    public function getTenderDetail($tenderId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,taskId,content,is_accept,create_time,create_by,update_time,update_by
                FROM  $this->TENDER_TABLE WHERE id = '$tenderId' and isDelete = 0";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 取消竞标
     * @param $tenderId
     * @return mixed
     */
    public function deleteTender($tenderId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TENDER_TABLE SET isDelete = 1 WHERE id = '$tenderId' AND isDelete = 0";

        return $this->pdo->update($sql);
    }
}
