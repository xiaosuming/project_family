<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/15 0015
 * Time: 14:22
 */

namespace DB;

use Util\Util;

class CDBFamilyStorage
{
    var $pdo = null;
    var $TABLE = 'gener_family_storage';
    var $LIMIT_TABLE = 'gener_family_storage_limit';

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 创建目录
     * @param $familyId
     * @param $dirName
     * @param $parent
     * @param $parent_path
     * @param $size
     * @param $file_type
     * @param $md5
     * @param $userId
     * @param $time
     * @return mixed
     */
    public function createDir($familyId, $dirName, $parent, $parent_path, $size, $file_type, $md5, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO gener_family_storage(familyId,name,parent,parent_path,source_ip,group_name,remote_filename,file_url,size,file_type,md5,is_dir,is_delete,create_by,create_time,update_by,update_time)VALUES('$familyId','$dirName','$parent','$parent_path','','','','','$size','$file_type','$md5','1','0','$userId',now(),'$userId',now())";

        try {
            $this->pdo->beginTransaction();
            $result = $this->pdo->insert($sql);
            $content = $this->getContentFromStorageLimit($familyId);
            $oldUsedSize = $content['used_size'];
            $usedSize = $oldUsedSize + $size;
            $this->updateUsedSize($familyId, $usedSize);
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            $result = '-1';
            //@codeCoverageIgnoreEnd
        }
        return $result;
    }

    /**
     * 家族  检查当前目录名字在该文件夹下是否存在
     * @param $familyId
     * @param $name
     * @param $parentId
     * @return bool
     */
    public function checkDirNameExistsInFamily($familyId, $name, $parentId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT count('id') FROM $this->TABLE WHERE familyId='$familyId' AND name='$name' AND parent='$parentId' AND is_dir='1' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return false;
        }
    }


    /**
     * 获取当前文件夹下的MD5值 防止文件的重复上传
     * @param $dirId
     * @param $familyId
     * @param $md5
     * @return mixed
     */
    public function checkFileExistsInDir($dirId, $familyId, $md5)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT count('id') FROM $this->TABLE WHERE parent='$dirId' AND familyId='$familyId' AND md5='$md5'  AND is_dir='0' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return false;
        }
    }

    /**
     * 通过MD5获取内容
     * @param $dirId
     * @param $familyId
     * @param $md5
     * @return mixed
     */
    public function getContentsInDirByMd5($dirId, $familyId, $md5)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT source_ip,group_name,remote_filename,file_url,size,file_type FROM $this->TABLE WHERE parent='$dirId' AND familyId='$familyId' AND md5='$md5'  AND is_dir='0' AND is_delete='0'";
        return $this->pdo->query($sql);
    }


    /**
     * 上传文件到指定的目录
     * @param $familyId
     * @param $fileName
     * @param $parentId
     * @param $parent_path
     * @param $source_ip
     * @param $group_name
     * @param $remote_filename
     * @param $file_url
     * @param $size
     * @param $file_type
     * @param $md5
     * @param $userId
     * @param $time
     * @return mixed
     */
    public function uploadFileToDir($familyId, $fileName, $parentId, $parent_path, $source_ip, $group_name, $remote_filename, $file_url, $size, $file_type, $md5, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO gener_family_storage(familyId,name,parent,parent_path,source_ip,group_name,remote_filename,file_url,size,file_type,md5,is_dir,is_delete,create_by,create_time,update_by,update_time)VALUES('$familyId','$fileName','$parentId','$parent_path','$source_ip','$group_name','$remote_filename','$file_url','$size','$file_type','$md5','0','0','$userId',now(),'$userId',now())";

        try {
            $this->pdo->beginTransaction();
            $result = $this->pdo->insert($sql);
            $content = $this->getContentFromStorageLimit($familyId);
            $oldUsedSize = $content['used_size'];
            $usedSize = $oldUsedSize + $size;
            $this->updateUsedSize($familyId, $usedSize);
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            $result = '-1';
            //@codeCoverageIgnoreEnd
        }
        return $result;
    }

    /**
     * 根据目录Id 获取目录名字 和 目录路径
     * @param $dirId
     * @return bool
     */
    public function getDirNameById($dirId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT name,parent_path FROM $this->TABLE WHERE id='$dirId' AND is_dir='1' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 根据目录或文件Id 获取familyId
     * @param $Id
     * @return bool
     */
    public function getFamilyIdById($Id)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT familyId FROM $this->TABLE WHERE id='$Id'  AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result['familyId'];
        }
    }

    /**
     * 通过ID获取familyId 在回收站
     * @param $fileId
     * @return bool
     */
    public function getFamilyIdByIdInRecycle($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT familyId FROM $this->TABLE WHERE id='$fileId' AND is_delete='1'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result['familyId'];
        }
    }

    /**
     * 检查目录是否存在 根据目录Id
     * @param $dirId
     * @return bool
     */
    public function checkDirExists($dirId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT count('id') FROM $this->TABLE WHERE id='$dirId' AND is_dir='1' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return false;
        }
    }

    /**
     * 检查文件是否存在
     * @param $fileId
     * @return bool
     */
    public function checkFileExists($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT count('id') FROM $this->TABLE WHERE id='$fileId'  AND is_delete='0' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return false;
        }
    }

    /**
     * 检查文件是否存在 在回收站中
     * @param $fileId
     * @return bool
     */
    public function checkFileExistsInRecycle($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT count('id') FROM $this->TABLE WHERE id='$fileId' AND is_delete='1'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return false;
        }
    }

    /**
     * 删除目录
     * @param $dirId
     * @return mixed
     */
    public function changeDirStatus($dirId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET is_delete='1' WHERE id='$dirId' AND is_dir='1' AND is_delete='0'";
        $result = $this->pdo->update($sql);
        return $result;
    }

    /**
     * 删除该目录下的子目录 如果有的话
     * @param $dirId
     * @return mixed
     */
    public function changeChildrenDirStatus($dirId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET is_delete='1' WHERE parent_path like '%$dirId%'  AND is_delete='0'";
        $result = $this->pdo->update($sql);
        return $result;
    }

    /**
     * 删除文件
     * @param $fileId
     * @return mixed
     */
    public function changeFileStatus($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET is_delete='1' WHERE id='$fileId' AND is_dir='0' AND is_delete='0'";
        $result = $this->pdo->update($sql);
        return $result;
    }

    /**
     * 获取所有的文件
     * @param $familyId
     * @return mixed
     */
    public function getAllFiles($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,familyId,name,parent,parent_path,file_url,size,file_type,md5,create_by,create_time,update_by,update_time FROM $this->TABLE WHERE familyId='$familyId' AND is_delete='0' AND is_dir='0' ORDER BY id DESC";
        return $this->pdo->query($sql);
    }

    /**
     * 通过文件Id 获取文件详情
     * @param $fileId
     * @return bool
     */
    public function getFilesByFileId($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,familyId,name,parent,parent_path,file_url,size,file_type,md5,create_by,create_time,update_by,update_time FROM $this->TABLE WHERE id='$fileId' AND is_dir='0' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 重命名文件
     * @param $fileName
     * @param $fileId
     * @return mixed
     */
    public function renameFileName($fileName, $fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET name='$fileName' WHERE id='$fileId'  AND is_delete='0'";
        return $this->pdo->update($sql);
    }

    /**
     * 获取家族回收站中的所有文件or目录
     * @param $familyId
     * @return mixed
     */
    public function getFilesByRecycle($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,familyId,name,parent,parent_path,group_name,remote_filename,file_url,size,file_type,md5,is_dir,create_by,create_time,update_by,update_time FROM $this->TABLE WHERE familyId='$familyId' AND is_delete='1'  ORDER BY id DESC";
        return $this->pdo->query($sql);
    }

    /**
     * 从回收站中恢复文件 or 目录
     * @param $fileId
     * @return mixed
     */
    public function recoverFileFromRecycle($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET is_delete='0' WHERE id='$fileId'  AND is_delete='1'";
        return $this->pdo->update($sql);
    }

    /**
     * 回复目录下的子目录
     * @param $fileId
     * @return mixed
     */
    public function recoverChildrenFileFromRecycle($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET is_delete='0' WHERE parent_path LIKE'%$fileId%'  AND is_delete='1'";
        return $this->pdo->update($sql);
    }

    /**
     * 从回收站删除文件
     * @return mixed
     */
    public function deleteFilesFromRecycle()
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "DELETE FROM $this->TABLE WHERE is_delete='1'";
        $deleteRows = $this->pdo->deletesql($sql);
        return $deleteRows;
    }

    /**
     * 通过id获取名字和父ID 在回收站中
     * @param $fileId
     * @return bool
     */
    public function getParentAndNameInRecycle($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT name,parent,is_dir FROM $this->TABLE WHERE id='$fileId' AND is_delete='1'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 通过id获取名字和父ID
     * @param $fileId
     * @return bool
     */
    public function getParentIdAndName($fileId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT name,parent,is_dir FROM $this->TABLE WHERE id='$fileId' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 根据路径获取名字
     * @param $parentId
     * @param $familyId
     * @return mixed
     */
    public function getFileNameByParentPath($parentId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT name FROM $this->TABLE WHERE parent_path Like '%$parentId%' AND familyId='$familyId' AND is_delete='0' AND is_dir='0'";
        return $this->pdo->query($sql);
    }

    /**
     * 重命名
     * @param $name
     * @param $id
     * @return mixed
     */
    public function rename($name, $id)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET name='$name' WHERE id='$id'";
        return $this->pdo->update($sql);
    }

    /**
     * 根据路径获取名字
     * @param $parentId
     * @param $familyId
     * @return mixed
     */
    public function getDirNameByParentPath($parentId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT name FROM $this->TABLE WHERE parent_path Like '%$parentId%' AND familyId='$familyId' AND is_delete='0' AND is_dir='1'";
        return $this->pdo->query($sql);
    }


    /**
     * 获取当前目录下的内容 包括文件 目录
     * @param $dirId
     * @return mixed
     */
    public function getContentByDirId($dirId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,familyId,name,parent,parent_path,group_name,remote_filename,file_url,size,file_type,md5,is_dir,create_by,create_time,update_by,update_time FROM $this->TABLE WHERE parent='$dirId' AND is_delete='0' ORDER BY id DESC";
        return $this->pdo->query($sql);
    }

    /**
     * 获取根目录下的目录或文件
     * @param $familyId
     * @return mixed
     */
    public function getRootDir($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,familyId,name,parent,parent_path,size,file_type,file_url,is_dir,create_by,create_time,update_by,update_time FROM $this->TABLE WHERE parent='0' AND familyId='$familyId' AND is_delete='0' ORDER BY id DESC";
        return $this->pdo->query($sql);
    }

    /**
     * 是否是文件、夹的创建者
     * @param $userId
     * @param $familyId
     * @param $id
     * @return bool
     */
    public function isCreatorForStorage($userId, $familyId, $id)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT create_by FROM $this->TABLE WHERE familyId='$familyId' AND id='$id' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            if ($result['create_by'] == $userId) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 获取家族存储的容量信息
     * @param $familyId
     * @return bool
     */
    public function getContentFromStorageLimit($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,familyId,max_size,used_size,recycle_size,create_time,update_time FROM $this->LIMIT_TABLE WHERE familyId='$familyId'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        }
        return $result;
    }

    /**
     *
     * 更新已经使用的容量
     * @param $familyId
     * @param $size
     * @return mixed
     */
    public function updateUsedSize($familyId, $size)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->LIMIT_TABLE SET used_size='$size' WHERE familyId='$familyId'";
        return $this->pdo->update($sql);
    }

    /**
     * 删除文件家的时候更新已经使用的容量 和 会回收站的容量
     * @param $familyId
     * @param $usedSize
     * @param $recycleSize
     * @return mixed
     */
    public function updateUsedSizeForDel($familyId, $usedSize, $recycleSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->LIMIT_TABLE SET used_size='$usedSize',recycle_size='$recycleSize' WHERE familyId='$familyId'";
        return $this->pdo->update($sql);
    }


    /**
     *
     * 通过dirid 获取总共的size
     * @param $dirId
     * @param $familyId
     * @return bool
     */
    public function getSumSizeByDirId($dirId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT size FROM $this->TABLE WHERE is_dir='1' AND id='$dirId' AND familyId='$familyId' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        }
        $sql = "SELECT size FROM $this->TABLE WHERE parent_path LIKE '%$dirId%' AND familyId='$familyId' AND is_delete='0'";
        $results = $this->pdo->query($sql);
        $size = $result['size'];
        foreach ($results as $v) {
            $_size = $v['size'];
            $size += $_size;
        }
        return $size;
    }

    /**
     * 通过文件Id获取文件大小
     * @param $fileId
     * @param $familyId
     * @return bool
     */
    public function getFileSizeByFileId($fileId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT size FROM $this->TABLE WHERE id='$fileId' AND familyId='$familyId' AND is_delete='0' AND is_dir='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        }
        return $result['size'];
    }

    /**
     * 获取回收站中的文件大小 根据文件Id
     * @param $fileId
     * @param $familyId
     * @return bool
     */
    public function getFileSizeByFileIdInRecycle($fileId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT size FROM $this->TABLE WHERE id='$fileId' AND familyId='$familyId' AND is_delete='1' AND is_dir='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        }
        return $result['size'];
    }

    /**
     * 获取回收站中文件夹及文件夹中文件的总大小
     * @param $dirId
     * @param $familyId
     * @return bool
     */
    public function getSumSizeByDirIdInRecycle($dirId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT size FROM $this->TABLE WHERE is_dir='1' AND id='$dirId' AND familyId='$familyId' AND is_delete='1'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        }
        $sql = "SELECT size FROM $this->TABLE WHERE parent_path LIKE '%$dirId%' AND familyId='$familyId' AND is_delete='1'";
        $results = $this->pdo->query($sql);
        $size = $result['size'];
        foreach ($results as $v) {
            //@codeCoverageIgnoreStart
            $_size = $v['size'];
            $size += $_size;
            //@codeCoverageIgnoreEnd
        }
        return $size;
    }

    /**
     * 清空回收站 recycle_size=0
     * @param $familyId
     * @return mixed
     */
    public function cleanRecycleSizeByFamilyId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->LIMIT_TABLE SET recycle_size='0' WHERE familyId='$familyId'";
        return $this->pdo->update($sql);
    }

    /**
     * @param $file_url
     * @param $familyId
     * @return null
     */
    public function checkFileUrlExists($file_url, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT count('file_url') FROM $this->TABLE WHERE file_url='$file_url' AND familyId='$familyId' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            //@codeCoverageIgnoreStart
            return null;
            //@codeCoverageIgnoreEnd
        }
        return $result["count('file_url')"];
    }
}
