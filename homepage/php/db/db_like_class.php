<?php
/**
 * 点赞模块
 * author: jiangpengfei
 * date: 2017-03-09
 */

namespace DB;

use DB\CDBManager;
use DB\CDBZone;
use DB\CDBQuestion;
use Util\Util;

class CDBLike
{
    public $pdo = null;
    public $TABLE = "gener_like";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 检查是否有点赞记录，有的话返回点赞记录的状态，没有的话返回-1
     * @param $userId 用户id
     * @param $postId 推文id
     * @param $module 模块
     * return int -1没有点过赞 0点过赞但是取消了 1点过赞并且处于有效状态
     */
    public function checkLike($userId, $postId, $module)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT status FROM $this->TABLE WHERE userId = '$userId' AND postId='$postId' AND module = '$module' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return -1;
        } else {
            return $result['status'];
        }
    }

    /**
     * 点赞或者取消赞
     * @param $userId 用户id
     * @param $postId 推文id
     * @param $module 属于哪个模块的推文
     * @return int -1取消赞成功 0动作失败 1点赞成功
     */
    public function like($userId, $postId, $module)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "";

        $check = $this->checkLike($userId, $postId, $module);
        $operation = 0;       //加一还是减一
        //这里会发生脏读,不能对数据库加读锁，会严重影响性能

        try {
            $this->pdo->beginTransaction();
            if ($check == -1) {
                //没有点过赞
                //@codeCoverageIgnoreStart
                $status = 1;
                $sql = "INSERT INTO $this->TABLE(userId,status,postId,module,create_by,create_time,update_by,update_time)VALUES('$userId','$status','$postId','$module','$userId',now(),'$userId',now())";
                $operation = 1;
                $result = $this->pdo->insert($sql);
                //@codeCoverageIgnoreEnd
            } else if ($check == 0) {
                //@codeCoverageIgnoreStart
                $status = 1;
                $operation = 1;
                $sql = "UPDATE $this->TABLE SET status = '$status' , update_time = now() , update_by= '$userId' WHERE userId = '$userId' AND postId = '$postId' AND module = '$module' ";
                $this->pdo->update($sql);
                //@codeCoverageIgnoreEnd
            } else {
                $status = 0;
                $operation = -1;
                $sql = "UPDATE $this->TABLE SET status = '$status' , update_time = now() , update_by= '$userId' WHERE userId = '$userId' AND postId = '$postId' AND module = '$module' ";
                $this->pdo->update($sql);
            }

            //重新count一次有多少赞
            $likeCount = 0;
            $sql = "SELECT count(*) FROM $this->TABLE WHERE postId = '$postId' AND status = '1' ";
            $result = $this->pdo->uniqueResult($sql);

            if ($result != null) {
                $likeCount = $result['count(*)'];
            }

            //更新日志表中的赞数

            if ($module == $GLOBALS['QUESTION_TASK_MODULE']) {//问题悬赏模块点赞
                //@codeCoverageIgnoreStart
                $likeCount = intval($likeCount);
                $questionDB = new CDBQuestion();
                $questionDB->updateAnswerLikeCount($postId, $likeCount);
                //@codeCoverageIgnoreEnd
            } else {
                $zoneDB = new CDBZone();
                $zoneDB->updatePostLikeCount($postId, $likeCount);
            }
            $this->pdo->commit();   //提交
            return $operation;
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            $logger->error(exceptionFormat($e));
            return 0;
            //@codeCoverageIgnoreEnd
        }
    }


}

;