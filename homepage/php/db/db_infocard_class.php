<?php
/**
 * 用户名片模块
 * author: jiangpengfei
 * date: 2017-12-01
 */

namespace DB;

use DB\CDBManager;
use Model\InfoCard;
use Util\Pager;
use Util\Util;
use Model\InfoCardExchange;
use DB\CDBPushInfoCard;
use DB\RedisInstanceManager;
use Util\SysLogger;
use Model\InfoCardChatOrder;

class CDBInfoCard
{
    private $redis = null;
    public $pdo = null;
    public $TABLE = "gener_info_card";
    public $TABLE_SHARE_RECORD = 'gener_infocard_share_record';
    public $TABLE_EXCHANGE = 'gener_infocard_exchange';
    public $TABLE_EXCHANGE_RECORD = 'gener_infocard_exchange_record';
    public $TABLE_USER = 'gener_user';
    public $TABLE_INFOCARD_FAMILY_PERMISSION = 'gener_infocard_family_permission';
    public $TABLE_INFOCARD_TAG = 'gener_infocard_tag';
    public $TABLE_INFOCARD_BATCH_SHARE = 'gener_infocard_batch_share';
    public $TABLE_INFOCARD_TAG_CARD = 'gener_infocard_tag_card';
    public $TABLE_INFOCARD_CHAT_ORDER = 'gener_infocard_chat_order';
    public $TABLE_INFORMATION = 'gener_information';
    public $TABLE_INFORMATION_CATEGORY = 'gener_information_category';
    public $TABLE_INFORMATION_FOLLOW = 'gener_information_follow';

    /**
     * @codeCoverageIgnore
     */
    public function __construct()
    {
        $redisManager = RedisInstanceManager::getInstance();
        $this->redis = $redisManager->getStorageInstance();
    }

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 增加用户信息名片
     * @param $infoCard  名片 model
     * @return int 增加的名片id
     */
    public function addInfoCard(InfoCard $infoCard)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        if (is_array($infoCard->phone)) {
            $infoCard->phone = json_encode($infoCard->phone);
        }

        if (is_array($infoCard->skills)) {
            $infoCard->skills = json_encode($infoCard->skills);
        }

        if (is_array($infoCard->favorites)) {
            $infoCard->favorites = json_encode($infoCard->favorites);
        }

        if (is_array($infoCard->jobs)) {
            $infoCard->jobs = json_encode($infoCard->jobs);
        }

        if (is_array($infoCard->imageOrder)) {
            $infoCard->imageOrder = json_encode($infoCard->imageOrder);
        }

        $birthdaySql = '';
        if ($infoCard->birthday == null) {
            //@codeCoverageIgnoreStart
            $birthdaySql = "NULL";
            //@codeCoverageIgnoreEnd
        } else {
            $birthdaySql = "'$infoCard->birthday'";
        }

        $sql = "INSERT INTO $this->TABLE
                        (remark,
                        userId,
                        familyId,
                        personId,
                        name,
                        name_en,
                        country,
                        country_name,
                        province,
                        province_name,
                        city,
                        city_name,
                        area,
                        area_name,
                        town,
                        town_name,
                        address,
                        photo,
                        background_img,
                        background_color,
                        background_type,
                        company,
                        company_en,
                        gender,
                        birthday,
                        blood_type,
                        marital_status,
                        phone,
                        qq,
                        skills,favorites,jobs,type,email,company_profile,company_url,bank_account,longitude,latitude,position,identity,ocrcard_img,tax_number,is_default,create_time,create_by,update_time,update_by)
                    VALUES(
                        '$infoCard->remark',
                        '$infoCard->userId',
                        '$infoCard->familyId',
                        '$infoCard->personId',
                        '$infoCard->name',
                        '$infoCard->nameEn',
                        '$infoCard->country',
                        '$infoCard->countryName',
                        '$infoCard->province',
                        '$infoCard->provinceName',
                        '$infoCard->city',
                        '$infoCard->cityName',
                        '$infoCard->area',
                        '$infoCard->areaName',
                        '$infoCard->town',
                        '$infoCard->townName',
                        '$infoCard->address',
                        '$infoCard->photo',
                        '$infoCard->backgroundImg',
                        '$infoCard->backgroundColor',
                        '$infoCard->backgroundType',
                        '$infoCard->company',
                        '$infoCard->companyEn',
                        '$infoCard->gender',
                         $birthdaySql,
                        '$infoCard->bloodType',
                        '$infoCard->maritalStatus',
                        '$infoCard->phone',
                        '$infoCard->qq',
                        '$infoCard->skills',
                        '$infoCard->favorites',
                        '$infoCard->jobs',
                        '$infoCard->type',
                        '$infoCard->email',
                        '$infoCard->companyProfile',
                        '$infoCard->companyUrl',
                        '$infoCard->bankAccount',
                        '$infoCard->longitude',
                        '$infoCard->latitude',
                        '$infoCard->position',
                        '$infoCard->identity',
                        '$infoCard->ocrCardImg',
                        '$infoCard->taxNumber',
                        '$infoCard->isDefault',
                        now(),'$infoCard->createBy',now(),'$infoCard->updateBy')";

        return $this->pdo->insert($sql);

    }

    /**
     * 添加名片的同时创建圈子
     * @param InfoCard $infoCard
     * @return array
     */
    public function addInfoCardAndCircle(InfoCard $infoCard)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $birthdaySql = '';
        if ($infoCard->birthday == null) {
            //@codeCoverageIgnoreStart
            $birthdaySql = "NULL";
            //@codeCoverageIgnoreEnd
        } else {
            $birthdaySql = "'$infoCard->birthday'";
        }

        try {
            $this->pdo->beginTransaction();
            //创建圈子
            $sql = "INSERT INTO $this->TABLE_INFORMATION_CATEGORY (userId,avatar,name,circleType,create_by,create_time,
            update_by,update_time) VALUES ('$infoCard->userId','$infoCard->photo','$infoCard->name',5,'$infoCard->userId',now(),'$infoCard->userId',now())";

            $socialCircleId = $this->pdo->insert($sql);

            if ($socialCircleId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '创建圈子失败');
                $this->pdo->rollback();
                exit;
            }

            $sql = "INSERT INTO $this->TABLE
                        (remark,
                        userId,
                        familyId,
                        personId,
                        socialCircleId,
                        name,
                        name_en,
                        country,
                        country_name,
                        province,
                        province_name,
                        city,
                        city_name,
                        area,
                        area_name,
                        town,
                        town_name,
                        address,
                        photo,
                        background_img,
                        background_color,
                        background_type,
                        company,
                        company_en,
                        gender,
                        birthday,
                        blood_type,
                        marital_status,
                        phone,
                        qq,
                        skills,favorites,jobs,type,email,company_profile,company_url,bank_account,longitude,latitude,position,identity,ocrcard_img,tax_number,is_default,create_time,create_by,update_time,update_by)
                    VALUES(
                        '$infoCard->remark',
                        '$infoCard->userId',
                        '$infoCard->familyId',
                        '$infoCard->personId',
                        '$socialCircleId',
                        '$infoCard->name',
                        '$infoCard->nameEn',
                        '$infoCard->country',
                        '$infoCard->countryName',
                        '$infoCard->province',
                        '$infoCard->provinceName',
                        '$infoCard->city',
                        '$infoCard->cityName',
                        '$infoCard->area',
                        '$infoCard->areaName',
                        '$infoCard->town',
                        '$infoCard->townName',
                        '$infoCard->address',
                        '$infoCard->photo',
                        '$infoCard->backgroundImg',
                        '$infoCard->backgroundColor',
                        '$infoCard->backgroundType',
                        '$infoCard->company',
                        '$infoCard->companyEn',
                        '$infoCard->gender',
                         $birthdaySql,
                        '$infoCard->bloodType',
                        '$infoCard->maritalStatus',
                        '$infoCard->phone',
                        '$infoCard->qq',
                        '$infoCard->skills',
                        '$infoCard->favorites',
                        '$infoCard->jobs',
                        '$infoCard->type',
                        '$infoCard->email',
                        '$infoCard->companyProfile',
                        '$infoCard->companyUrl',
                        '$infoCard->bankAccount',
                        '$infoCard->longitude',
                        '$infoCard->latitude',
                        '$infoCard->position',
                        '$infoCard->identity',
                        '$infoCard->ocrCardImg',
                        '$infoCard->taxNumber',
                        '$infoCard->isDefault',
                        now(),'$infoCard->createBy',now(),'$infoCard->updateBy')";
            $infoCardId = $this->pdo->insert($sql);
            if ($infoCardId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '创建名片失败');
                $this->pdo->rollback();
                exit;
            }
            //创始人关注圈子
            $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
            VALUES('$infoCard->userId','$socialCircleId',now(),'$infoCard->userId',now(),'$infoCard->userId')";
            $followId = $this->pdo->insert($sql);
            if ($followId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                $this->pdo->rollback();
                exit;
            }
            $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=1 WHERE id = '$socialCircleId'";
            $updateRow = $this->pdo->update($sql);
            if ($updateRow <= 0) {
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
                $this->pdo->rollback();
                exit;
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            $this->pdo->rollback();
            return -1;
        }

        return [$socialCircleId, $infoCardId];
    }

    /**
     * 通过通讯录创建名片
     * @param InfoCard $infoCard 名片 model
     * @return mixed
     */
    public function addInfoCardByAddressList(InfoCard $infoCard)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE 
        ( remark,
          userId,
          name,
          company,
          address,
          photo,
          background_color,
          background_type,
          phone,
          type,
          skills,
          favorites,
          jobs,
          identity,
          create_time,
          create_by,
          update_time,
          update_by)
          VALUES
        ('$infoCard->remark',
         '$infoCard->userId',
         '$infoCard->name',
         '$infoCard->company',
         '$infoCard->address',
         '$infoCard->photo',
         '#ccccdd',
         0,
         '$infoCard->phone',
          1,
          '$infoCard->skills',
          '$infoCard->favorites',
          '$infoCard->jobs',
          '$infoCard->identity',
          now(),
          '$infoCard->createBy',
          now(),
          '$infoCard->updateBy')";

        return $this->pdo->insert($sql);
    }

    /**
     * 编辑用户名片
     * @param $infoCard  名片 model
     * @return int 更新的记录数
     */
    public function editInfoCard(InfoCard $infoCard)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $birthdaySql = '';
        if ($infoCard->birthday == null) {
            //@codeCoverageIgnoreStart
            $birthdaySql = "NULL";
            //@codeCoverageIgnoreStart
        } else {
            $birthdaySql = "'$infoCard->birthday'";
        }

        $sql = "UPDATE $this->TABLE SET 
            remark = '$infoCard->remark',
            userId = '$infoCard->userId',
            name = '$infoCard->name',
            name_en = '$infoCard->nameEn',
            country = '$infoCard->country',
            country_name = '$infoCard->countryName',
            province = '$infoCard->province',
            province_name = '$infoCard->provinceName',
            city = '$infoCard->city',
            city_name = '$infoCard->cityName',
            area = '$infoCard->area',
            area_name = '$infoCard->areaName',
            town = '$infoCard->town',
            town_name = '$infoCard->townName',
            address = '$infoCard->address',
            photo = '$infoCard->photo',
            background_img = '$infoCard->backgroundImg',
            background_color = '$infoCard->backgroundColor',
            background_type = '$infoCard->backgroundType',
            company = '$infoCard->company',
            company_en = '$infoCard->companyEn',
            gender = '$infoCard->gender',
            birthday =  $birthdaySql,
            blood_type = '$infoCard->bloodType',
            marital_status = '$infoCard->maritalStatus',
            phone = '$infoCard->phone',
            qq = '$infoCard->qq',
            email = '$infoCard->email',
            company_profile = '$infoCard->companyProfile',
            company_url = '$infoCard->companyUrl',
            bank_account = '$infoCard->bankAccount',
            longitude = '$infoCard->longitude',
            latitude = '$infoCard->latitude',
            position = '$infoCard->position',
            skills = '$infoCard->skills',
            favorites = '$infoCard->favorites',
            jobs = '$infoCard->jobs',
            permission = '$infoCard->permission',
            tax_number = '$infoCard->taxNumber',
            update_time = now(),
            update_by = '$infoCard->updateBy' 
            WHERE id = '$infoCard->id' AND is_delete=0";

        return $this->pdo->update($sql);
    }

    /**
     * 删除用户名片
     * @param $cardId 名片id
     * @return int 删除的记录数
     */
    public function deleteInfoCard($cardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET is_delete = 1 WHERE id ='$cardId'";
        return $this->pdo->update($sql);
    }

    /**
     * 删除用户名片和圈子
     * @param $infoCardId
     * @param $socialCircleId
     * @return int
     */
    public function deleteInfoCardAndCircle($infoCardId, $socialCircleId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE SET is_delete = 1 WHERE id ='$infoCardId' AND is_delete = 0";
            $result = $this->pdo->update($sql);

            //删除圈子
            $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET is_delete=1 WHERE id='$socialCircleId' AND is_delete=0";
            $this->pdo->update($sql);
            //删除圈子关注列表
            $sql = "UPDATE $this->TABLE_INFORMATION_FOLLOW SET isDelete=1 WHERE categoryId='$socialCircleId' AND isDelete=0";
            $this->pdo->update($sql);
            //删除圈子资讯和动态
            $sql = "UPDATE $this->TABLE_INFORMATION SET is_delete=1 WHERE categoryId='$socialCircleId' AND is_delete=0";
            $this->pdo->update($sql);
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return $result;
    }


    /**
     * 检索用户名片
     * @param $cardId 名片id
     * @return InfoCard 名片model
     */
    public function getInfoCard($cardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,remark,
            userId,
            familyId,
            personId,
            socialCircleId,
            name,
            name_en as nameEn,
            country,
            country_name as countryName,
            province,
            province_name as provinceName,
            city,
            city_name as cityName,
            area,
            area_name as areaName,
            town,
            town_name as townName,
            address,
            photo,
            background_img as backgroundImg,
            background_color as backgroundColor,
            background_type as backgroundType,
            company,
            company_en as companyEn,
            gender,
            birthday,
            blood_type as bloodType,
            marital_status as maritalStatus,
            phone,
            qq,
            email,
            company_profile as companyProfile,
            company_url as companyUrl,
            bank_account as bankAccount,
            longitude,latitude,position,
            skills,favorites,jobs,identity,type,is_default as isDefault,permission,openScope,ocrcard_img as ocrCardImg,tax_number as taxNumber,imageOrder,
            chatPermission, chatCost,
            create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
            
            FROM $this->TABLE WHERE id = '$cardId' AND is_delete = 0";


        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return null;
        } else {
            return new InfoCard($result);
        }
    }

    /**
     * 获取所有的用户名片/ocr名片
     * @param $userId  用户id
     * @param $type 0用户自己创建的名片  4ocr名片
     * @return mixed
     */
    public function getAllInfoCards($userId, $type)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIds = $accountDB->getUserAliasStr($userId);


        $sql = "SELECT id,remark,
            userId,
            familyId,
            personId,
            socialCircleId,
            name,
            name_en as nameEn,
            country,
            country_name as countryName,
            province,
            province_name as provinceName,
            city,
            city_name as cityName,
            area,
            area_name as areaName,
            town,
            town_name as townName,
            address,
            photo,
            background_img as backgroundImg,
            background_color as backgroundColor,
            background_type as backgroundType,
            company,
            company_en as companyEn,
            gender,
            birthday,
            blood_type as bloodType,
            marital_status as maritalStatus,
            phone,
            qq,
            email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,
            skills,favorites,jobs,position,identity,type,is_default as isDefault,permission,openScope,ocrcard_img as ocrCardImg,tax_number as taxNumber,
            chatPermission, chatCost,
            create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
            
            FROM $this->TABLE WHERE userId in $userIds AND type=$type AND is_delete=0 ORDER BY id DESC";

        $result = $this->pdo->query($sql);

        foreach ($result as $key => $card) {
            $result[$key]['phone'] = json_decode($card['phone'], true);
            $result[$key]['skills'] = json_decode($card['skills'], true);
            $result[$key]['favorites'] = json_decode($card['favorites'], true);
            $result[$key]['jobs'] = json_decode($card['jobs'], true);
        }

        return $result;
    }

    /**
     * 分页获取导入的所有类型的名片   type 1 用户导入的,2 家族导入的,4 ocr扫描的名片
     * @param $pageIndex //当前也
     * @param $pageSize //每页数量
     * @param $userId //用户id
     * @return mixed
     */
    public function getAllTypeInfoCardsPageByImport($pageIndex, $pageSize, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $offset = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT id,remark,
            userId,
            familyId,
            personId,
            socialCircleId,
            name,
            name_en as nameEn,
            country,
            country_name as countryName,
            province,
            province_name as provinceName,
            city,
            city_name as cityName,
            area,
            area_name as areaName,
            town,
            town_name as townName,
            address,
            photo,
            background_img as backgroundImg,
            background_color as backgroundColor,
            background_type as backgroundType,
            company,
            company_en as companyEn,
            gender,
            birthday,
            blood_type as bloodType,
            marital_status as maritalStatus,
            phone,
            qq,
            email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,
            skills,favorites,jobs,position,identity,type,is_default as isDefault,permission,openScope,ocrcard_img as ocrCardImg,tax_number as taxNumber,
            chatPermission, chatCost,
            create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
            
            FROM $this->TABLE WHERE  type IN (1,2,4) AND userId in $userIdAliasStr AND is_delete=0 ORDER BY id DESC LIMIT $offset,$pageSize";

        $result = $this->pdo->query($sql);

        foreach ($result as $key => $card) {
            //@codeCoverageIgnoreStart
            $result[$key]['phone'] = json_decode($card['phone'], true);
            $result[$key]['skills'] = json_decode($card['skills'], true);
            $result[$key]['favorites'] = json_decode($card['favorites'], true);
            $result[$key]['jobs'] = json_decode($card['jobs'], true);
            //@codeCoverageIgnoreEnd
        }

        return $result;
    }

    /**
     * 指定自己的名片部分信息进行展示
     * @param $infoCardId 名片id
     * @return null
     */
    public function getSectionInfoFromCard($infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT userId,socialCircleId,openScope,name,name_en as nameEn,company,company_en as companyEn,email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,type,photo,background_img as backgroundImg,background_color as backgroundColor,background_type as backgroundType,company,country_name as countryName,province_name as provinceName,city_name as cityName,area_name as areaName,town_name as townName,address,gender,phone,jobs,position,tax_number as taxNumber,chatPermission, chatCost FROM $this->TABLE WHERE id='$infoCardId' AND is_show='1' AND is_delete = 0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return null;
        } else {
            return $result;
        }
    }

    /**
     * 指定名片展示
     * @param $infoCardId 名片id
     * @return mixed
     */
    public function showInfoCardById($infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET is_show = 1 WHERE id='$infoCardId'";
        return $this->pdo->update($sql);
    }

    /**
     * 扫描二维码 获取identity 通过identity获取信息
     * @param $identity  名片的identity
     * @return null
     */
    public function getInfoCardByIdentity($identity)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,socialCircleId,openScope,remark,name,name_en as nameEn,company,company_en as companyEn,email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,type,photo,background_img as backgroundImg,background_color as backgroundColor,background_type as backgroundType,company,country_name as countryName,province_name as provinceName,city_name as cityName,area_name as areaName,town_name as townName,address,gender,phone,birthday,blood_type as bloodType
,qq,skills,favorites,jobs,position,tax_number as taxNumber,chatPermission, chatCost,is_default as isDefault FROM $this->TABLE WHERE identity='$identity' AND is_delete = 0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return null;
        } else {
            return $result;
        }
    }

    /**
     * 接受名片 添加进数据库
     * @param $cardId  名片id
     * @param $userId  用户id
     * @return mixed
     */
    public function receiveInfoCardById($cardId, $userId, $tag = '')
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO gener_receive_info_card (cardId,is_delete,tag, create_time,create_by) VALUES ('$cardId',0, '$tag', now(),$userId)";
        return $this->pdo->insert($sql);
    }

    /**
     * 判断名片是否已经存在
     * @param $cardId  名片id
     * @param $userId   用户id
     * @return bool
     */
    public function existsCardIdOnReceive($cardId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT count('id') FROM gener_receive_info_card WHERE cardId='$cardId' AND create_by in $userIdAliasStr AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);

        if ($result["count('id')"] > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取所有的手机通讯录生成的名片
     * @param $userId  用户id
     * @return mixed
     */
    public function getAllInfoCardFromAddressList($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT id,name,name_en as nameEn,phone,type,socialCircleId,openScope,photo,company,address,company_en as companyEn,email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,identity,position,background_img as backgroundImg,background_color as backgroundColor,background_type as backgroundType,tax_number as taxNumber,chatPermission, chatCost,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy FROM $this->TABLE WHERE type='1' AND userId in $userIdAliasStr AND is_delete=0 ORDER BY id DESC";
        return $this->pdo->query($sql);

    }

    /**
     * 获取通讯录中的名字和手机号
     * @param $userId 用户id
     * @return mixed
     */
    public function getPhoneAndNameFromAddressBook($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT phone,name FROM $this->TABLE WHERE type='1' AND userId in $userIdAliasStr ORDER BY id DESC";
        return $this->pdo->query($sql);

    }

    /**
     * 分页获取接受到的名片的信息
     * @param $userId 用户id
     * @return mixed
     */
    public function getReceiveInfoCardPaging($userId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT ic.id,ic.remark,ic.userId,ic.familyId,ic.personId,ic.socialCircleId,ic.name,ic.name_en as nameEn,ic.company,ic.company_en as companyEn,ic.email,ic.company_profile as companyProfile,ic.company_url as companyUrl,ic.bank_account as bankAccount,ic.longitude,ic.latitude,ic.birthday,ic.type,ic.position,ic.identity,ic.permission,ic.openScope,ic.photo,ic.background_img as backgroundImg,ic.background_color as backgroundColor,ic.background_type as backgroundType,ic.country_name as countryName,ic.province_name as provinceName,ic.city_name as cityName,ic.area_name as areaName,ic.town_name as townName,ic.address,ic.gender,ic.phone,ic.jobs,rc.tag,ic.tax_number as taxNumber,chatPermission, chatCost FROM gener_receive_info_card as rc LEFT JOIN $this->TABLE as ic ON rc.cardId=ic.id WHERE rc.create_by in $userIdAliasStr AND rc.is_delete='0' AND ic.is_delete=0 ORDER BY rc.id DESC LIMIT $offset,$pageSize";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取所有接受到的名片的信息
     * @param $userId 用户id
     * @return mixed
     */
    public function getAllReceiveInfoCard($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT ic.id,ic.remark,ic.userId,ic.familyId,ic.personId,ic.socialCircleId,ic.name,ic.name_en as nameEn,ic.birthday,ic.type,ic.is_show as isShow,ic.is_default as isDefault,ic.permission,ic.openScope,ic.photo,ic.background_img as backgroundImg,ic.background_color as backgroundColor,ic.background_type as backgroundType,ic.company,ic.company_en as companyEn,ic.email,ic.identity,ic.position,ic.permission,ic.company_profile as companyProfile,ic.company_url as companyUrl,ic.bank_account as bankAccount,ic.longitude,ic.latitude,ic.country_name as countryName,ic.province_name as provinceName,ic.city_name as cityName,ic.area_name as areaName,ic.town_name as townName,ic.address,ic.gender,ic.phone,ic.jobs,rc.tag, ic.tax_number as taxNumber,chatPermission, chatCost FROM gener_receive_info_card as rc LEFT JOIN $this->TABLE as ic ON rc.cardId=ic.id WHERE rc.create_by in $userIdAliasStr AND rc.is_delete='0' AND ic.is_delete=0 ORDER BY rc.id DESC ";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取接受到名片的总数
     * @param $userId  用户id
     * @return null
     */
    public function countReceiveInfoCard($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT count('id') FROM gener_receive_info_card WHERE is_delete='0' AND create_by in $userIdAliasStr ";
        $result = $this->pdo->uniqueResult($sql);

        return $result["count('id')"];

    }

    /**
     * 通过名片id获取接收名片的人
     * @param $infoCardId
     * @return null
     */
    public function getCreateByInfoCardId($infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT create_by as createBy FROM gener_receive_info_card WHERE cardId = '$infoCardId' AND is_delete = 0";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 检查是否是接收的名片
     * @param $infoCardId
     * @param $userId
     * @return bool
     */
    public function verifyUserIdInReceive($infoCardId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT id FROM gener_receive_info_card WHERE cardId='$infoCardId' AND create_by in $userIdAliasStr AND is_delete=0";
        $id = $this->pdo->uniqueResult($sql);
        if ($id == null) {
            return false;
        } else {
            if ($id > 0) {
                return true;
            } else {
                //@codeCoverageIgnoreStart
                return false;
                //@codeCoverageIgnoreEnd
            }
        }
    }

    /**
     * 删除接受到的名片
     * @param $userId  用户id
     * @param $cardId   名片id
     * @return mixed
     */
    public function deleteReceiveInfoCard($userId, $cardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "UPDATE gener_receive_info_card SET is_delete='1' WHERE create_by in $userIdAliasStr AND cardId='$cardId'";
        return $this->pdo->update($sql);
    }

    /**
     * 指定显示的名片
     * @param $show  显示 1  不显示 0  默认1
     * @param $infoCardId  指定的名片id
     * @return mixed
     */
    public function updateIsShowForMyInfoCard($show, $infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET is_show='$show' WHERE id='$infoCardId' AND type='0' AND is_delete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 判断personId 是否存在导入名片列表中
     * @param $personId  人物id
     * @param $userId   用户id
     * @return bool
     */
    public function verifyPersonIdExists($personId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT id,familyId,personId FROM $this->TABLE WHERE  personId='$personId' AND userId in $userIdAliasStr AND type='2' AND  is_delete =0";

        $result = $this->pdo->uniqueResult($sql);

        return $result;
    }

    /**
     * 更新导入的名片
     * @param $infoCard  名片 model
     * @return mixed
     */
    public function updateImportInfoCard($infoCard)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $birthdaySql = '';
        if ($infoCard->birthday == null) {
            //@codeCoverageIgnoreStart
            $birthdaySql = "NULL";
            //@codeCoverageIgnoreEnd
        } else {
            $birthdaySql = "'$infoCard->birthday'";
        }

        $sql = "UPDATE $this->TABLE SET 
            remark = '$infoCard->remark',
            userId = '$infoCard->userId',
            name = '$infoCard->name',
            country = '$infoCard->country',
            country_name = '$infoCard->countryName',
            province = '$infoCard->province',
            province_name = '$infoCard->provinceName',
            city = '$infoCard->city',
            city_name = '$infoCard->cityName',
            area = '$infoCard->area',
            area_name = '$infoCard->areaName',
            town = '$infoCard->town',
            town_name = '$infoCard->townName',
            address = '$infoCard->address',
            gender = '$infoCard->gender',
            birthday =  $birthdaySql,
            blood_type = '$infoCard->bloodType',
            marital_status = '$infoCard->maritalStatus',
            phone = '$infoCard->phone',
            qq = '$infoCard->qq',
            update_time = now(),
            update_by = '$infoCard->updateBy' 
            WHERE personId = '$infoCard->personId' AND familyId = '$infoCard->familyId' AND type = '2' AND is_delete = 0";

        return $this->pdo->update($sql);

    }

    /**
     * 共导入多少条记录
     * @param $familyId  家族id
     * @param $userId   用户id
     * @return null
     */
    public function countImportInfoCards($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        if ($familyId == 0) {
            //@codeCoverageIgnoreStart
            $str = '';
            //@codeCoverageIgnoreEnd
        } else {
            $str = "AND familyId = '$familyId'";
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT count('id') FROM $this->TABLE WHERE type= 2 " . $str . " AND userId in $userIdAliasStr AND is_delete = 0";
        $result = $this->pdo->uniqueResult($sql);
        return $result["count('id')"];
    }

    /**
     * 分页获取导入的记录
     * @param $familyId  家族id
     * @param $userId   用户id
     * @param $pageIndex  当前页
     * @param $pageSize   每页显示多少
     * @return mixed
     */
    public function getImportInfoCardsPaging($familyId, $userId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        if ($familyId == 0) {
            //@codeCoverageIgnoreStart
            $str = '';
            //@codeCoverageIgnoreEnd
        } else {
            $str = "AND familyId='$familyId'";
        }
        $offset = ($pageIndex - 1) * $pageSize;

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT id,remark,
                userId,
                familyId,
                personId,
                socialCircleId,
                name,
                name_en as nameEn,
                country,
                country_name as countryName,
                province,
                province_name as provinceName,
                city,
                city_name as cityName,
                area,
                area_name as areaName,
                town,
                town_name as townName,
                address,
                photo,
                background_img as backgroundImg,
                background_color as backgroundColor,
                background_type as backgroundType,
                company,
                company_en as companyEn,
                gender,
                birthday,
                blood_type as bloodType,company_profile as companyProfile,company_url as companyUrl,email,bank_account as bankAccount,longitude,latitude,identity,
                marital_status as maritalStatus,phone,qq,skills,favorites,jobs,position,type,is_show as isShow,is_default as isDefault,permission,openScope,tax_number as taxNumber,chatPermission, chatCost,create_time as createTime ,create_by as createBy,update_time as updateTime,update_by as updateBy FROM  $this->TABLE  WHERE type='2' " . $str . " AND userId in $userIdAliasStr AND is_delete =0 ORDER BY id DESC LIMIT $offset,$pageSize";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取所有的导入的记录
     * @param $familyId  家族id
     * @param $userId   用户id
     * @return mixed
     */
    public function getAllImportInfoCards($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        if ($familyId == 0) {
            //@codeCoverageIgnoreStart
            $str = '';
            //@codeCoverageIgnoreEnd
        } else {
            $str = "AND familyId='$familyId'";
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT id,remark,
                userId,
                familyId,
                personId,
                socialCircleId,
                name,
                name_en as nameEn,
                country,
                country_name as countryName,
                province,
                province_name as provinceName,
                city,
                city_name as cityName,
                area,
                area_name as areaName,
                town,
                town_name as townName,
                address,
                photo,
                background_img as backgroundImg,
                background_color as backgroundColor,
                background_type as backgroundType,
                company,
                company_en as companyEn,
                gender,
                birthday,
                blood_type as bloodType,
                marital_status as maritalStatus,phone,qq,
                email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,identity,position,skills,favorites,jobs,type,is_show as isShow,is_default as isDefault,permission,openScope,tax_number as taxNumber,chatPermission, chatCost,create_time as createTime ,create_by as createBy,update_time as updateTime,update_by as updateBy FROM  $this->TABLE  WHERE type='2' " . $str . " AND userId in $userIdAliasStr AND is_delete =0 ORDER BY id DESC";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取导入的家族列表
     * @param $userId //用户Id
     * @return mixed
     */
    public function getImportFamilyList($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT ic.familyId,ic.type,gf.name,gf.surname,gf.description,gf.photo FROM $this->TABLE as ic LEFT JOIN gener_family as gf ON ic.familyId=gf.id WHERE ic.type='2' AND ic.userId in $userIdAliasStr AND ic.is_delete = 0";
        return $this->pdo->query($sql);
    }

    /**
     * 更改背景显示的类型
     * @param $backgroundImg  背景图
     * @param $backgroundColor 背景色
     * @param $backgroundType 背景类型
     * @param $infoCardId      名片id
     * @return mixed
     */
    public function changeBackgroundType($backgroundImg, $backgroundColor, $backgroundType, $infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET
                background_img = '$backgroundImg',
                background_color = '$backgroundColor', 
                background_type='$backgroundType' WHERE id='$infoCardId' AND is_delete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 函数不用了
     * 更改小程序中的用户id 有事务
     * @param $userId   名片中的用户id
     * @param $anotherUserId  指定的用户id
     * @return int
     */
    public function changeUserId($userId, $anotherUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE SET userId = '$anotherUserId',create_by='$anotherUserId',update_by = '$anotherUserId' WHERE userId='$userId' AND type = 0 ";
            $result = $this->pdo->update($sql);
            $sql = "UPDATE gener_receive_info_card SET create_by = '$anotherUserId' WHERE create_by= '$userId' AND is_delete = 0";
            $result1 = $this->pdo->update($sql);
            $sql = "UPDATE gener_infocard_homepage SET create_by = '$anotherUserId',update_by = '$anotherUserId' WHERE create_by = '$userId' AND is_delete=0";
            $result2 = $this->pdo->update($sql);
            $sql = "UPDATE gener_infocard_share_record SET owner_userId = '$anotherUserId' WHERE owner_userId = '$userId'";
            $result3 = $this->pdo->update($sql);
            $sql = "UPDATE gener_infocard_share_record SET share_userId = '$anotherUserId' WHERE share_userId = '$userId'";
            $result4 = $this->pdo->update($sql);
            $sql = "UPDATE gener_infocard_share_record SET target_userId = '$anotherUserId' WHERE target_userId = '$userId'";
            $result5 = $this->pdo->update($sql);
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $result + $result1 + $result2 + $result3 + $result4 + $result5;
    }

    /**
     * 更改小程序中的用户id 无事务
     * @param $userId 名片中的用户id
     * @param $anotherUserId  指定的用户id
     * @return mixed
     */
    public function changeUserIdWithoutTransaction($userId, $anotherUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET userId = '$anotherUserId',create_by='$anotherUserId',update_by = '$anotherUserId' WHERE userId='$userId' AND type = 0 ";
        $result = $this->pdo->update($sql);
        $sql = "UPDATE gener_receive_info_card SET create_by = '$anotherUserId' WHERE create_by= '$userId' AND is_delete = 0";
        $result1 = $this->pdo->update($sql);
        $sql = "UPDATE gener_infocard_homepage SET create_by = '$anotherUserId',update_by = '$anotherUserId' WHERE create_by = '$userId' AND is_delete=0";
        $result2 = $this->pdo->update($sql);
        $sql = "UPDATE gener_infocard_share_record SET owner_userId = '$anotherUserId' WHERE owner_userId = '$userId'";
        $result3 = $this->pdo->update($sql);
        $sql = "UPDATE gener_infocard_share_record SET share_userId = '$anotherUserId' WHERE share_userId = '$userId'";
        $result4 = $this->pdo->update($sql);
        $sql = "UPDATE gener_infocard_share_record SET target_userId = '$anotherUserId' WHERE target_userId = '$userId'";
        $result5 = $this->pdo->update($sql);
        return $result + $result1 + $result2 + $result3 + $result4 + $result5;
    }

    /**
     * 添加公司
     * @param $infoCard 实例化model
     * @return mixed
     */
    public function addCompany($infoCard)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO gener_infocard_company 
            (infocard_id,company,company_en,company_profile,company_url,bank_account,create_time,create_by,update_time,update_by)
            VALUES 
            ('$infoCard->id',
            '$infoCard->company',
            '$infoCard->companyEn',
            '$infoCard->companyProfile',
            '$infoCard->companyUrl',
            '$infoCard->bankAccount',
            now(),'$infoCard->createBy',now(),'$infoCard->updateBy')";
        return $this->pdo->insert($sql);
    }

    /**
     * 通过名片id获取其他的公司信息列表
     * @param $infoCardId
     * @return mixed
     */
    public function getOtherCompanyById($infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,company,company_en as companyEn,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime 
        FROM gener_infocard_company WHERE infocard_id = '$infoCardId' AND is_delete = 0";
        return $this->pdo->query($sql);
    }

    /**
     * 通过公司id删除公司
     * @param $companyId 公司id
     * @return mixed
     */
    public function delOtherCompanyById($companyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE gener_infocard_company SET is_delete = 1 WHERE id = '$companyId'";
        return $this->pdo->update($sql);
    }

    /**
     * 通过公司id获取公司详情
     * @param $companyId
     * @return mixed
     */
    public function getCompanyByOtherCompanyId($companyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,company,company_en as companyEn,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime 
        FROM gener_infocard_company WHERE id = '$companyId' AND is_delete = 0";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 通过公司id编辑公司信息
     * @param $infoCard  名片model
     * @param $companyId
     * @return mixed
     */
    public function editOtherCompanyByCompanyId($infoCard, $companyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE gener_infocard_company SET 
            company='$infoCard->company',
            company_en='$infoCard->companyEn',
            company_profile = '$infoCard->companyProfile',
            company_url = '$infoCard->companyUrl',
            bank_account = '$infoCard->bankAccount',
            update_by='$infoCard->updateBy',
            update_time = now() WHERE id='$companyId'";
        return $this->pdo->update($sql);
    }

    /**
     * 通过名片id添加一条微主页
     * @param $infoCardId
     * @param $imgUrl
     * @param $imgProfile
     * @param $userId
     * @return mixed
     */
    public function addCompanyMinHomepage($infoCardId, $imgUrl, $imgProfile, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO gener_infocard_homepage (infocard_id,img_url,img_profile,create_by,create_time,update_by,update_time)
                 VALUES ('$infoCardId','$imgUrl','$imgProfile','$userId',now(),'$userId',now())";
        return $this->pdo->insert($sql);

    }

    /**
     * 通过id删除微主页
     * @param $minHomepageId
     * @return mixed
     */
    public function delCompanyMinHomepageById($minHomepageId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE gener_infocard_homepage SET is_delete = 1 WHERE id='$minHomepageId'";
        return $this->pdo->update($sql);
    }

    /**
     * 通过id编辑微主页
     * @param $minHomepageId
     * @param $imgUrl
     * @param $imgProfile
     * @param $userId
     * @return mixed
     */
    public function editCompanyMinHomepageById($minHomepageId, $imgUrl, $imgProfile, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE gener_infocard_homepage SET
                img_url = '$imgUrl',
                img_profile = '$imgProfile',
                update_time = now(),
                update_by = '$userId' WHERE id = '$minHomepageId'";
        return $this->pdo->update($sql);
    }

    /**
     * 获取微主页列表
     * @param $infoCardId
     * @return mixed
     */
    public function getCompanyMinHomepagesByInfoCardId($infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,img_url as imgUrl ,img_profile as imgProfile , create_by as createBy ,create_time as createTime,
                update_by as updateBy,update_time as updateTime FROM gener_infocard_homepage WHERE is_delete = 0 AND infocard_id = '$infoCardId'";
        return $this->pdo->query($sql);
    }

    /**
     * 通过id获取微主页详情
     * @param $minHomepageId
     * @return mixed
     */
    public function getCompanyMinHomepageById($minHomepageId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,infocard_id as infoCardId,img_url as imgUrl ,img_profile as imgProfile , create_by as createBy ,create_time as createTime,
                update_by as updateBy,update_time as updateTime FROM gener_infocard_homepage WHERE id='$minHomepageId' AND is_delete = 0";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 更改名片的私密或公开(允许or不允许转发)
     * @param $permission
     * @param $infoCardId
     * @return mixed
     */
    public function updateInfoCardPermission($permission, $infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET permission='$permission' WHERE id='$infoCardId'  AND is_delete = 0";
        return $this->pdo->update($sql);
    }


    /**
     * 添加分享记录
     * @param InfoCardShareRecord $shareRecord 名片分享记录
     * @return int 记录存储的id
     */
    public function addShareRecord($shareRecord)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $shareUserId = $shareRecord->shareUserId;
        $shareCardId = $shareRecord->shareCardId;
        $ownerUserId = $shareRecord->ownerUserId;
        $targetUserId = $shareRecord->targetUserId;

        $sql = "INSERT INTO $this->TABLE_SHARE_RECORD (
                share_time, share_userId, share_cardId, owner_userId, target_userId, create_time, create_by
                )VALUES(
                now(),'$shareUserId','$shareCardId','$ownerUserId','$targetUserId',now(),'$targetUserId'
                )";

        return $this->pdo->insert($sql);
    }

    /**
     * 分页获取分享记录
     * @param $userId 用户id
     * @param $pageIndex 页码
     * @param $pageSize  每页数量
     * @return array 分页的数据
     */
    public function getShareRecordPaging($userId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $offset = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT id, share_time as shareTime, share_userId as shareUserId, share_cardId as shareCardId,
                owner_userId as ownerUserId, target_userId as targetUserId, create_time as createTime, create_by as createBy
                FROM $this->TABLE_SHARE_RECORD WHERE owner_userId in $userIdAliasStr ORDER BY id desc limit $offset, $pageSize;
                ";

        return $this->pdo->query($sql);
    }


    /**
     * 检查identity是否存在
     * @param $identity
     * @return bool
     */
    public function verifyIdentity($identity)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT count('id') FROM $this->TABLE WHERE identity = '$identity'";
        $result = $this->pdo->uniqueResult($sql);

        if ($result["count('id')"] > 0) {
            return true;
        } else {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }

    }

    /**
     * @codeCoverageIgnore
     */
    public function getInfoCardKey($infoCardId)
    {
        return 'ic' . $infoCardId;
    }

    /**
     * @codeCoverageIgnore
     * 名片浏览量增1
     * @param $infoCardId 名片id
     */
    public function increView($infoCardId)
    {
        $result = $this->redis->hIncrBy($this->getInfoCardKey($infoCardId), 'v', 1);

        return $result;
    }

    /**
     * @codeCoverageIgnore
     * 名片转发量增1
     * @param $infoCardId 名片id
     */
    public function increShare($infoCardId)
    {
        $result = $this->redis->hIncrBy($this->getInfoCardKey($infoCardId), 's', 1);
        return $result;
    }

    /**
     * @codeCoverageIgnore
     * 获取名片统计结果，目前是浏览量和转发量
     * @param $infoCardId 名片id
     */
    public function getInfoCardStatistic($infoCardId)
    {
        $result = $this->redis->hGetAll($this->getInfoCardKey($infoCardId));
        return $result;
    }

    /**
     * 更新微主页图片排序
     * @param $infoCardId
     * @param $imageOrder
     * @return mixed
     */
    public function updateHomepageImageOrder($infoCardId, $imageOrder)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET imageOrder='$imageOrder' WHERE id = '$infoCardId' AND is_delete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 检查用户是否已经创建过名片了，是否是第一次创建名片
     * @param $userId
     * @return bool
     */
    public function checkInfoCardIdExistsByUserId($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);


        $sql = "SELECT count('id') as sum FROM $this->TABLE WHERE userId in $userIdAliasStr AND type=0 AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['sum'] > 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 指定用户的默认名片
     * @param $infoCardId
     * @param $userId
     * @return int
     */
    public function updateIsDefaultForMyInfoCard($infoCardId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE SET is_default=0 WHERE userId='$userId' AND is_default=1 AND type=0 AND is_delete = 0";
            $result = $this->pdo->update($sql);
            $sql = "UPDATE $this->TABLE SET is_default=1 WHERE id='$infoCardId'";
            $result1 = $this->pdo->update($sql);
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $result1 + $result;
    }

    /**
     * 获取用户的默认名片或者第一张名片
     * @param $userId
     * @return InfoCard 名片对象
     */
    public function getUserDefaultInfoCardOrFirst(int $userId)
    {
        $this->init();

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT id,userId,socialCircleId,openScope,remark,name,name_en as nameEn,company,company_en as companyEn,email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,type,photo,background_img as backgroundImg,background_color as backgroundColor,background_type as backgroundType,company,country_name as countryName,province_name as provinceName,city_name as cityName,area_name as areaName,town_name as townName,address,gender,phone,birthday,blood_type as bloodType
,qq,skills,favorites,jobs,position,tax_number as taxNumber,chatPermission, chatCost,is_default as isDefault FROM $this->TABLE WHERE userId in $userIdAliasStr AND  is_default='1' AND is_delete = 0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {

            // 没有默认，指定第一张
            $sql = "SELECT id,userId,socialCircleId,openScope,remark,name,name_en as nameEn,company,company_en as companyEn,email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,type,photo,background_img as backgroundImg,background_color as backgroundColor,background_type as backgroundType,company,country_name as countryName,province_name as provinceName,city_name as cityName,area_name as areaName,town_name as townName,address,gender,phone,birthday,blood_type as bloodType
,qq,skills,favorites,jobs,position,tax_number as taxNumber,chatPermission, chatCost,is_default as isDefault FROM $this->TABLE WHERE userId in $userIdAliasStr AND is_delete = 0";

            $result = $this->pdo->uniqueResult($sql);

            if ($result == null) {
                return null;
            }

            return $result;
        } else {
            return $result;
        }
    }


    /**
     * 随机获取官方创建的名片
     * @param $count
     * @return mixed
     */
    public function getRandomInfoCardNoLogin($count)
    {
        $this->init();

        $sql = "SELECT id,userId,socialCircleId,openScope,remark,name,name_en as nameEn,company,company_en as companyEn,email,company_profile as companyProfile,company_url as companyUrl,bank_account as bankAccount,longitude,latitude,type,photo,background_img as backgroundImg,background_color as backgroundColor,background_type as backgroundType,company,country_name as countryName,province_name as provinceName,city_name as cityName,area_name as areaName,town_name as townName,address,gender,phone,birthday,blood_type as bloodType
,qq,skills,favorites,jobs,position,tax_number as taxNumber,
chatPermission, chatCost,is_default as isDefault
 FROM $this->TABLE WHERE  type = 5 AND is_delete = 0 ORDER BY RAND() LIMIT $count";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 创建名片交换活动
     * @param $exchange 交换
     * @param $infocardId 名片id
     * @return int 交换id，-1表示失败
     */
    public function createInfoCardExchange(InfoCardExchange $exchange, $infocardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {

            $sql = "INSERT INTO $this->TABLE_EXCHANGE (userId, title, code, password, isFinish, isDelete, createTime, createBy, updateTime, updateBy)
        VALUES('$exchange->userId', '$exchange->title', '$exchange->code', '$exchange->password', '0', '0', now(), '$exchange->userId', now(), '$exchange->userId')";

            $exchangeId = $this->pdo->insert($sql);

            // 向本次交换插入一条记录
            $this->joinInfoCardExchange($exchangeId, $exchange->userId, $infocardId);

            return $exchangeId;
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }


    }

    /**
     * 参加名片交换活动
     * @param $exchangeId 交换id
     * @param $userId 用户id
     * @param $cardId 名片id
     * @return int 插入的记录的id
     */
    public function joinInfoCardExchange($exchangeId, $userId, $cardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();

            $sql = "INSERT INTO $this->TABLE_EXCHANGE_RECORD (exchangeId, userId, cardId, createTime, createBy, updateTime, updateBy)
            VALUES('$exchangeId', '$userId', '$cardId', now(), '$userId', now(), '$userId')";

            $recordId = $this->pdo->insert($sql);

            $sql = "UPDATE $this->TABLE_EXCHANGE SET total = total + 1, updateBy = '$userId', updateTime = now() WHERE id = '$exchangeId' AND isDelete = '0' ";
            $this->pdo->update($sql);

            $this->pdo->commit();

        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }

        return $recordId;
    }

    /**
     * 获取名片交换的详情
     */
    public function getInfocardExchange($exchangeId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT * FROM $this->TABLE_EXCHANGE WHERE id = '$exchangeId' AND isDelete = '0' ";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取用户创建的名片交换
     */
    public function getUserInfoCardExchange($pageIndex, $pageSize, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT * FROM $this->TABLE_EXCHANGE WHERE userId in $userIdAliasStr AND isDelete = '0' ORDER BY id desc  limit $offset,$pageSize ";

        return $this->pdo->query($sql);
    }

    /**
     * 获取用户参与的名片交换
     */
    public function getUserJoinInfoCardExchange($pageIndex, $pageSize, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $offset = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT te.* FROM $this->TABLE_EXCHANGE_RECORD ter 
         left join $this->TABLE_EXCHANGE te on ter.exchangeId = te.id 
         WHERE ter.userId in $userIdAliasStr AND ter.isDelete = '0' AND te.isDelete = '0' 
         ORDER BY ter.exchangeId DESC 
         LIMIT $offset, $pageSize";

        return $this->pdo->query($sql);
    }

    /**
     * 根据code获取名片交换
     */
    public function getInfoCardExchangeByCode($code)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT * FROM $this->TABLE_EXCHANGE WHERE code = '$code' AND isDelete = '0' ";
        return $this->pdo->uniqueResult($sql);
    }


    /**
     * 获取参加交换的记录
     * @param $exchangeId 交换记录的id
     * @return array 本次参加的所有记录
     */
    public function getJoinExchangeRecords($exchangeId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT ter.id,ter.exchangeId,ter.cardId,ter.userId,ter.createTime,ter.createBy,ter.updateTime,ter.updateBy,
        tb.remark,tb.familyId,tb.personId,tb.name,tb.name_en as nameEn,
        tb.country,
        tb.country_name as countryName,
        tb.province,
        tb.province_name as provinceName,
        tb.city,
        tb.city_name as cityName,
        tb.area,
        tb.area_name as areaName,
        tb.town,
        tb.town_name as townName,
        tb.address,
        tb.photo,
        tb.background_img as backgroundImg,
        tb.background_color as backgroundColor,
        tb.background_type as backgroundType,
        tb.company,
        tb.company_en as companyEn,
        tb.gender,
        tb.birthday,
        tb.blood_type as bloodType,
        tb.marital_status as maritalStatus,
        tb.phone,
        tb.qq,
        tb.email,tb.company_profile as companyProfile,tb.company_url as companyUrl,tb.bank_account as bankAccount,tb.longitude,tb.latitude,
        tb.skills,tb.favorites,tb.jobs,tb.position,tb.identity,tb.type,tb.is_default as isDefault,tb.permission,tb.ocrcard_img as ocrCardImg,tb.tax_number as taxNumber 
        FROM $this->TABLE_EXCHANGE_RECORD ter 
        LEFT JOIN $this->TABLE tb on ter.cardId = tb.id WHERE exchangeId = '$exchangeId' ";
        return $this->pdo->query($sql);
    }

    /**
     * 根据code值检查当前用户是否已经参加过交换了
     */
    public function checkUserExistInExchangeByCode($code, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT count(*) FROM $this->TABLE_EXCHANGE_RECORD ter INNER JOIN $this->TABLE_EXCHANGE te on te.Id = ter.exchangeId WHERE te.code = '$code' AND ter.userId in $userIdAliasStr ";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(*)'] > 0;
    }

    /**
     * 检查当前用户是否已经参加过交换了
     * @param $exchangeId 交换id
     * @param $userId     用户id
     * @return bool true参加过，false没有参加过
     */
    public function checkUserExistInExchange($exchangeId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT count(*) FROM $this->TABLE_EXCHANGE_RECORD WHERE exchangeId = '$exchangeId' AND userId in $userIdAliasStr ";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(*)'] > 0;

    }

    /**
     * 结束名片交换活动
     * @param $exchangeId 名片交换活动id
     * @return bool true 更新成功，false 更新失败
     */
    public function endInfoCardExchange($exchangeId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $infoCardExchange = $this->getInfocardExchange($exchangeId);
        if ($infoCardExchange == null) {
            return false;
        }

        $tag = $infoCardExchange['title'];

        try {

            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE_EXCHANGE set isFinish = '1', updateTime = now() WHERE id = '$exchangeId' AND isDelete = '0' ";
            $this->pdo->update($sql);

            // 获取所有参加本次活动的用户
            $records = $this->getJoinExchangeRecords($exchangeId);

            foreach ($records as $record) {
                $userId = $record['userId'];
                $cardId = $record['cardId'];

                $needTagInfoCards = [];
                // 给当前用户增加所有其他人的名片
                foreach ($records as $record) {
                    if ($record['cardId'] != $cardId) {
                        $needTagInfoCards[] = $record['cardId'];
                        // 排除自己
                        if (!$this->existsCardIdOnReceive($record['cardId'], $userId)) {
                            $this->receiveInfoCardById($record['cardId'], $userId, $tag);
                        }
                    }
                }

                $tagId = $this->addInfoCardTag($tag, $userId);
                $this->addInfoCardsToTag($needTagInfoCards, $tagId, $userId, false);
            }

            $this->pdo->commit();

            // 接收名片成功,将该名片也作为索引任务推送到队列中
            $pushInfoCardDB = new CDBPushInfoCard();
            foreach ($records as $record) {
                $userId = $record['userId'];
                $cardId = $record['cardId'];
                // 给当前用户增加所有其他人的名片
                foreach ($records as $record) {
                    if ($record['cardId'] != $cardId) {
                        $modelId = $record['userId'] . '_' . $record['cardId'];
                        $action = 2;    // 更新操作
                        $source = [
                            "script" => [
                                "source" => "ctx._source.access.add(params.access)",
                                "params" => ["access" => $userId]
                            ]
                        ];
                        $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);
                    }
                }
            }

            return true;
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            //@codeCoverageIgnoreEnd
        }
    }


    /**
     * 获取名片的所有分享转发记录
     * 这里其实可以用图数据库去做
     * @param int $infoCardId 名片id
     * @return array 所有与改名片相关的转发记录
     */
    public function getInfoCardShareRecords(int $infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 查询出所有与该张名片相关的记录
        $sql = "SELECT tsr.*,tu.nickname as srcNickname,tu.photo as srcPhoto,
                tu2.nickname as dstNickname, tu2.photo as dstPhoto FROM $this->TABLE_SHARE_RECORD tsr 
                INNER JOIN $this->TABLE_USER tu on tsr.share_userId = tu.id 
                INNER JOIN $this->TABLE_USER tu2 on tsr.target_userId = tu2.id 
         WHERE tsr.share_cardId = '$infoCardId'";

        return $this->pdo->query($sql);
    }

    /**
     * 检查其他用户是否有当前用户的名片
     * @param int $otherUserId 其他用户id
     * @param int $userId 当前用户id
     * @return bool true有，false没有
     */
    public function checkOtherHasUserInfoCard($otherUserId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT count(*) FROM gener_receive_info_card where create_by = '$otherUserId' AND is_delete = '0' AND cardId in (SELECT id FROM $this->TABLE WHERE userId in $userIdAliasStr AND is_delete = '0') ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result['count(*)'] == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 删除群体名片交换
     * @param $exchangeId 名片交换id
     * @param $userId     用户id
     * @return int 删除的记录数
     */
    public function deleteGroupExchange($exchangeId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_EXCHANGE set isDelete = '1', updateBy = '$userId', updateTime = now() WHERE id = '$exchangeId' ";

        return $this->pdo->update($sql);
    }

    /**
     * 修改名片的公开范围
     * @param $openScope
     * @param $infoCardId
     * @param array $familyIdArr
     * @return mixed
     */
    public function updateInfoCardOpenScope($openScope, $infoCardId, $userId, $familyIdArr = array())
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();

            $sql = "UPDATE $this->TABLE_INFOCARD_FAMILY_PERMISSION SET isDelete=1 WHERE infoCardId='$infoCardId' AND isDelete=0";

            $delRow = $this->pdo->update($sql);

            if (!empty($familyIdArr)) {

                $str = '';

                foreach ($familyIdArr as $k => $familyId) {
                    $str .= "('$infoCardId','$familyId',now(),'$userId',now(),'$userId'),";
                }
                $str = rtrim($str, ',');

                $sql = "INSERT INTO $this->TABLE_INFOCARD_FAMILY_PERMISSION (infoCardId,familyId,createTime,createBy,updateTime,updateBy)
                        VALUES $str ";
                $insertRow = $this->pdo->insert($sql);
            }

            $sql = "UPDATE $this->TABLE SET openScope='$openScope',update_by='$userId',update_time = now() WHERE id='$infoCardId'  AND is_delete = 0";

            $updateRow = $this->pdo->update($sql);
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }

        return $updateRow;
    }

    public function getInfoCardIdByFamilyIdAndUserId($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT infoCardId FROM gener_person WHERE familyId='$familyId' AND userId in $userIdAliasStr AND is_delete=0";
        return $this->pdo->uniqueResult($sql);
    }

    public function getUserDefaultInfoCard($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT id,remark,
            userId,
            familyId,
            personId,
            socialCircleId,
            name,
            name_en as nameEn,
            country,
            country_name as countryName,
            province,
            province_name as provinceName,
            city,
            city_name as cityName,
            area,
            area_name as areaName,
            town,
            town_name as townName,
            address,
            photo,
            background_img as backgroundImg,
            background_color as backgroundColor,
            background_type as backgroundType,
            company,
            company_en as companyEn,
            gender,
            birthday,
            blood_type as bloodType,
            marital_status as maritalStatus,
            phone,
            qq,
            email,
            company_profile as companyProfile,
            company_url as companyUrl,
            bank_account as bankAccount,
            longitude,latitude,position,
            skills,favorites,jobs,identity,type,is_default as isDefault,permission,openScope,ocrcard_img as ocrCardImg,tax_number as taxNumber,imageOrder,chatPermission, chatCost,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
            
            FROM $this->TABLE WHERE userId in $userIdAliasStr AND is_default=1 AND is_delete = 0";


        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return null;
        } else {
            return new InfoCard($result);
        }
    }

    public function verifyFamilyIdAndUserIdPermission($familyId, $infoCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') FROM $this->TABLE_INFOCARD_FAMILY_PERMISSION WHERE familyId='$familyId' 
                AND infoCardId = '$infoCardId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);

        if ($result["count('id')"] > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 新建一个标签分组
     * @param $tag        标签
     * @param $userId     用户
     * @return int 标签的id
     */
    public function addInfoCardTag($tag, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_INFOCARD_TAG 
                (userId, tag, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$userId', '$tag', '$userId', now(), '$userId', now()) ";

        return $this->pdo->insert($sql);
    }

    /**
     * 更新标签名
     * @param $tagId  标签id
     * @param $tagName  标签名
     * @return int 更新的记录数
     */
    public function updateInfoCardTagName($tagId, $tagName, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_INFOCARD_TAG SET tag = '$tagName', updateTime = now(), updateBy = '$userId' WHERE id = '$tagId' ";

        return $this->pdo->update($sql);
    }

    /**
     * 根据tagId获取tag信息
     * @param $tagId 标签id
     * @return map tag信息
     */
    public function getTagById($tagId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, userId, tag, total, createBy, createTime, updateBy, updateTime FROM $this->TABLE_INFOCARD_TAG WHERE id = '$tagId' AND isDelete = '0' ";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 删除tag
     * @param $tagId 标签id
     * @param $userId  用户id
     * @return int 修改的记录数量
     */
    public function deleteInfoCardTag($tagId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_INFOCARD_TAG SET isDelete = '1', updateTime = now(), updateBy = '$userId' WHERE id = '$tagId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 获取用户的标签总数
     * @param $userId 用户id
     * @return int 总数
     */
    public function getUserTagsTotal($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT count(id) FROM $this->TABLE_INFOCARD_TAG WHERE userId in $userIdAliasStr AND isDelete = '0'";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取用户的所有标签
     * @param $userId 用户id
     * @param $maxId 最大的Id
     * @param $sinceId 起始的id
     * @param $count  需要的数量
     * @return array 用户的标签数组
     */
    public function getUserTagsPaging($userId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $where = '';

        if ($maxId != 0) {
            $where .= " AND id < $maxId ";
        }

        if ($sinceId != 0) {
            $where .= " AND id > $sinceId ";
        }

        // 获取关联的所有用户
        $accountDB = new CDBAccount();
        $userIdAliasStr = $accountDB->getUserAliasStr($userId);

        $sql = "SELECT id, userId, tag, total, createBy, createTime, updateBy, updateTime FROM $this->TABLE_INFOCARD_TAG WHERE userId in $userIdAliasStr AND isDelete = '0' $where ORDER BY id DESC limit 0, $count";

        return $this->pdo->query($sql);
    }

    /**
     * 分享多张名片
     * @param array $infocardIds 名片id数组
     * @return int 分享码
     */
    public function shareMultiInfoCards($shareName, $infocardIds, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $shareCode = md5($userId . Util::generateRandomCode(24));

        $str = json_encode($infocardIds);

        $sql = "INSERT INTO $this->TABLE_INFOCARD_BATCH_SHARE 
                (userId, shareName, shareCode, cardIds, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$userId', '$shareName', '$shareCode', '$str', '$userId', now(), '$userId', now())";

        $this->pdo->insert($sql);

        return $shareCode;
    }

    /**
     * 根据tagId获取所有的名片
     * @param $tagId 标签id
     * @return array 名片数组
     */
    public function getAllInfoCardsByTagId($tagId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT titc.id as tagRecordId, tb.id,tb.remark,
            tb.userId,
            tb.familyId,
            tb.personId,
            tb.socialCircleId,
            tb.name,
            tb.name_en as nameEn,
            tb.country,
            tb.country_name as countryName,
            tb.province,
            tb.province_name as provinceName,
            tb.city,
            tb.city_name as cityName,
            tb.area,
            tb.area_name as areaName,
            tb.town,
            tb.town_name as townName,
            tb.address,
            tb.photo,
            tb.background_img as backgroundImg,
            tb.background_color as backgroundColor,
            tb.background_type as backgroundType,
            tb.company,
            tb.company_en as companyEn,
            tb.gender,
            tb.birthday,
            tb.blood_type as bloodType,
            tb.marital_status as maritalStatus,
            tb.phone,
            tb.qq,
            tb.email,tb.company_profile as companyProfile,tb.company_url as companyUrl,tb.bank_account as bankAccount,tb.longitude,tb.latitude,
            tb.skills,tb.favorites,tb.jobs,tb.position,tb.identity,tb.type,tb.is_default as isDefault,tb.permission,tb.ocrcard_img as ocrCardImg,tb.tax_number as taxNumber,tb.chatPermission, tb.chatCost,tb.create_time as createTime,tb.create_by as createBy,tb.update_time as updateTime,tb.update_by as updateBy 
            FROM $this->TABLE_INFOCARD_TAG_CARD titc 
            LEFT JOIN $this->TABLE tb ON tb.id = titc.cardId 
            where titc.tagId = $tagId AND titc.isDelete = 0 ORDER BY titc.id DESC";

        $result = $this->pdo->query($sql);

        foreach ($result as $key => $card) {
            $result[$key]['phone'] = json_decode($card['phone'], true);
            $result[$key]['skills'] = json_decode($card['skills'], true);
            $result[$key]['favorites'] = json_decode($card['favorites'], true);
            $result[$key]['jobs'] = json_decode($card['jobs'], true);
        }

        return $result;
    }

    /**
     * 根据tagId分页获取其中的名片
     * @param $tagId 标签id
     * @param $maxId  最大id
     * @param $sicneId 起始id
     * @param $count  总数
     * @return array 名片数组
     */
    public function getInfoCardsPagingByTagId($tagId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $where = '';

        if ($maxId != 0) {
            $where .= " AND titc.id < $maxId ";
        }

        if ($sinceId != 0) {
            $where .= " AND titc.id > $sinceId";
        }

        $sql = "SELECT titc.id as tagRecordId, tb.id,tb.remark,
            tb.userId,
            tb.familyId,
            tb.personId,
            tb.socialCircleId,
            tb.name,
            tb.name_en as nameEn,
            tb.country,
            tb.country_name as countryName,
            tb.province,
            tb.province_name as provinceName,
            tb.city,
            tb.city_name as cityName,
            tb.area,
            tb.area_name as areaName,
            tb.town,
            tb.town_name as townName,
            tb.address,
            tb.photo,
            tb.background_img as backgroundImg,
            tb.background_color as backgroundColor,
            tb.background_type as backgroundType,
            tb.company,
            tb.company_en as companyEn,
            tb.gender,
            tb.birthday,
            tb.blood_type as bloodType,
            tb.marital_status as maritalStatus,
            tb.phone,
            tb.qq,
            tb.email,tb.company_profile as companyProfile,tb.company_url as companyUrl,tb.bank_account as bankAccount,tb.longitude,tb.latitude,
            tb.skills,tb.favorites,tb.jobs,tb.position,tb.identity,tb.type,tb.is_default as isDefault,tb.permission,tb.ocrcard_img as ocrCardImg,tb.tax_number as taxNumber,tb.chatPermission, tb.chatCost,tb.create_time as createTime,tb.create_by as createBy,tb.update_time as updateTime,tb.update_by as updateBy 
            FROM $this->TABLE_INFOCARD_TAG_CARD titc 
            LEFT JOIN $this->TABLE tb ON tb.id = titc.cardId 
            where titc.tagId = $tagId AND titc.isDelete = 0 $where ORDER BY titc.id DESC LIMIT 0, $count";

        $result = $this->pdo->query($sql);

        foreach ($result as $key => $card) {
            $result[$key]['phone'] = json_decode($card['phone'], true);
            $result[$key]['skills'] = json_decode($card['skills'], true);
            $result[$key]['favorites'] = json_decode($card['favorites'], true);
            $result[$key]['jobs'] = json_decode($card['jobs'], true);
        }

        return $result;
    }


    /**
     * 向tag中添加名片
     * @param $infoCardIds 名片id数组
     * @param $tagId   标签id
     * @param $needTransaction 是否需要事务（在外部有事务的情况下，可以通过这个参数关闭)
     * @return int 有效的添加数量
     */
    public function addInfoCardsToTag($infoCardIds, $tagId, $userId, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT cardId FROM $this->TABLE_INFOCARD_TAG_CARD WHERE tagId = '$tagId' AND isDelete = '0' ";
        $result = $this->pdo->query($sql);

        $existMap = [];
        foreach ($result as $card) {
            $existMap[$card['cardId']] = true;
        }

        $insertInfoCardIds = [];

        foreach ($infoCardIds as $infoCardId) {
            if (isset($existMap[$infoCardId])) {
                continue;
            }

            $insertInfoCardIds[] = $infoCardId;
        }

        if (count($insertInfoCardIds) == 0) {
            return 0;
        }

        $insertValue = '';
        $isFirst = true;

        foreach ($insertInfoCardIds as $cardId) {
            if ($isFirst) {
                $insertValue .= "('$tagId', '$cardId', '$userId', now(), '$userId', now() )";
                $isFirst = false;
            } else {
                $insertValue .= ",('$tagId', '$cardId', '$userId', now(), '$userId', now() )";
            }
        }

        try {
            if ($needTransaction) {
                $this->pdo->beginTransaction();
            }
            $sql = "INSERT INTO $this->TABLE_INFOCARD_TAG_CARD 
                    (tagId, cardId, createBy, createTime, updateBy, updateTime)
                    VALUES 
                    $insertValue ";

            $this->pdo->insert($sql);

            $rowNum = count($insertInfoCardIds);

            $sql = "UPDATE $this->TABLE_INFOCARD_TAG SET total = total + $rowNum, updateTime = now() WHERE id = '$tagId' ";

            $this->pdo->update($sql);

            if ($needTransaction) {
                $this->pdo->commit();
            }

        } catch (PDOException $e) {
            if ($needTransaction) {
                $this->pdo->rollback();
            }
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return false;
        }


        return count($insertInfoCardIds);
    }

    /**
     * 从标签中移除名片
     * @param $infoCardId 名片id
     * @param $tagId   标签id
     * @param $userId  用户Id
     * @return int 修改的记录数
     */
    public function removeInfoCardFromTag($infoCardId, $tagId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();

            $sql = "UPDATE $this->TABLE_INFOCARD_TAG_CARD 
                SET isDelete = '1', updateTime = now(), updateBy = '$userId' 
                WHERE tagId = '$tagId' AND cardId = '$infoCardId' AND isDelete = '0' ";

            $rows = $this->pdo->update($sql);

            if ($rows > 0) {
                $sql = "UPDATE $this->TABLE_INFOCARD_TAG SET total = total - $rows, updateTime = now() WHERE id = '$tagId' ";

                $this->pdo->update($sql);
            }

            $this->pdo->commit();

            return $rows;
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return 0;
        }
    }


    /**
     * 根据批量分享码分页获取其中的名片
     * @param $shareCode 批量分享码
     * @return array 名片数组
     */
    public function getInfoCardsByBatchShareCode($shareCode)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT cardIds FROM $this->TABLE_INFOCARD_BATCH_SHARE WHERE shareCode = '$shareCode' AND isDelete = '0' ";

        $infoCardsResult = $this->pdo->uniqueResult($sql);

        if ($infoCardsResult == null) {
            return [];
        }

        $infoCardIds = json_decode($infoCardsResult['cardIds'], true);

        $idsStr = '(' . Util::arrayToString($infoCardIds) . ')';

        $sql = "SELECT tb.id,tb.remark,
            tb.userId,
            tb.familyId,
            tb.personId,
            tb.socialCircleId,
            tb.name,
            tb.name_en as nameEn,
            tb.country,
            tb.country_name as countryName,
            tb.province,
            tb.province_name as provinceName,
            tb.city,
            tb.city_name as cityName,
            tb.area,
            tb.area_name as areaName,
            tb.town,
            tb.town_name as townName,
            tb.address,
            tb.photo,
            tb.background_img as backgroundImg,
            tb.background_color as backgroundColor,
            tb.background_type as backgroundType,
            tb.company,
            tb.company_en as companyEn,
            tb.gender,
            tb.birthday,
            tb.blood_type as bloodType,
            tb.marital_status as maritalStatus,
            tb.phone,
            tb.qq,
            tb.email,tb.company_profile as companyProfile,tb.company_url as companyUrl,tb.bank_account as bankAccount,tb.longitude,tb.latitude,
            tb.skills,tb.favorites,tb.jobs,tb.position,tb.identity,tb.type,tb.is_default as isDefault,tb.permission,tb.ocrcard_img as ocrCardImg,tb.tax_number as taxNumber,tb.chatPermission, tb.chatCost,tb.create_time as createTime,tb.create_by as createBy,tb.update_time as updateTime,tb.update_by as updateBy 
            
            FROM $this->TABLE tb 
            where tb.id in $idsStr AND tb.is_delete = 0";

        $result = $this->pdo->query($sql);

        foreach ($result as $key => $card) {
            $result[$key]['phone'] = json_decode($card['phone'], true);
            $result[$key]['skills'] = json_decode($card['skills'], true);
            $result[$key]['favorites'] = json_decode($card['favorites'], true);
            $result[$key]['jobs'] = json_decode($card['jobs'], true);
        }

        return $result;
    }


    /**
     * 根据批量分享码接收名片
     * @param $batchShareCode 批量分享码
     * @param $userId  用户id
     * @return int 接收成功的名片数量
     */
    public function receiveInfoCardByBatchShareCode($batchShareCode, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT shareName, cardIds FROM $this->TABLE_INFOCARD_BATCH_SHARE WHERE shareCode = '$batchShareCode' AND isDelete = '0' ";
        $shareContent = $this->pdo->uniqueResult($sql);

        if ($shareContent == null) {
            return 0;
        }

        $cardIds = json_decode($shareContent['cardIds'], true);
        $shareName = $shareContent['shareName'];

        $receiveNum = 0;        // 接收计数
        $pushInfoCardDB = new CDBPushInfoCard();
        foreach ($cardIds as $cardId) {

            if (!$this->existsCardIdOnReceive($cardId, $userId)) {
                $infoCard = $this->getInfoCard($cardId);
                if ($infoCard != null && $infoCard->permission == 1) {
                    if ($infoCard->type == 1 || $infoCard->type == 2 || $infoCard->type == 4) {
                        // 名片导入
                        $infoCard->userId = $userId;
                        $newInfoCardId = $this->addInfoCard($infoCard);

                        $modelId = $userId . '_' . $newInfoCardId;
                        $action = 1;
                        $access = [$userId];
                        $source = [
                            'id' => intval($newInfoCardId),
                            'remark' => $infoCard->remark,
                            'userId' => intval($userId),
                            'familyId' => 0,
                            'personId' => 0,
                            'name' => $infoCard->name,
                            'nameEn' => $infoCard->nameEn,
                            'address' => $infoCard->address,
                            'photo' => $infoCard->photo,
                            'backgroundImg' => $infoCard->backgroundImg,
                            'backgroundColor' => $infoCard->backgroundColor,
                            'backgroundType' => $infoCard->backgroundType,
                            'company' => $infoCard->company,
                            'companyEn' => $infoCard->companyEn,
                            'phone' => Util::arrayToString($infoCard->phone),
                            'jobs' => $infoCard->position,
                            'type' => $infoCard->type,
                            'email' => $infoCard->email,
                            'companyProfile' => $infoCard->companyProfile,
                            'access' => $access
                        ];

                        $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);
                    } else {
                        // 名片存在，并且允许转发,则接收
                        $this->receiveInfoCardById($cardId, $userId, $shareName);

                        // 推送到搜索服务器中
                        $modelId = $infoCard->userId . '_' . $cardId;
                        $action = 2;    // 更新操作
                        $source = [
                            "script" => [
                                "source" => "ctx._source.access.add(params.access)",
                                "params" => ["access" => $userId]
                            ]
                        ];
                        $pushInfoCardDB->setPushInfoCardTask($modelId, $action, $source);
                    }

                    $receiveNum++;
                }
            }
        }

        return $receiveNum;
    }


    /**
     * 设置名片的私信权限
     * @param $infoCardId 名片id
     * @param $chatPermission 1是公开，2是付费，3是禁止
     * @param $chatCost 费用值
     * @return int 修改的记录数
     */
    public function setChatPermission($infoCardId, $chatPermission, $chatCost)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set chatPermission = '$chatPermission', chatCost = '$chatCost' WHERE id = '$infoCardId' AND is_delete = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 添加名片私信订单
     * @param InfoCardChatOrder $chatOrder 私信订单对象
     * @return int 添加的记录id
     */
    public function addInfoCardChatOrder(InfoCardChatOrder $chatOrder)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_INFOCARD_CHAT_ORDER 
                (id, userId, infoCardId, content, orderId, isDelete, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$chatOrder->id', '$chatOrder->userId', '$chatOrder->infoCardId', '$chatOrder->content', '$chatOrder->orderId', '$chatOrder->isDelete', '$chatOrder->createBy', now(), '$chatOrder->updateBy', now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 根据订单id获取私信的详情
     * @param int $orderId 订单id
     * @return InfoCardChatOrder 私信详情对象
     */
    public function getInfoCardChatOrderByOrderId(int $orderId)
    {
        $this->init();

        $sql = "SELECT * FROM $this->TABLE_INFOCARD_CHAT_ORDER WHERE orderId = '$orderId' AND isDelete = '0' ";

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return null;
        } else {
            return new InfoCardChatOrder($result);
        }
    }

}
