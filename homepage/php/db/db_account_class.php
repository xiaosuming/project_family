<?php

namespace DB;

use Model\User;
use Util\PhotoGenerator;
use Util\Util;
use DB\CDBPoint;
use DB\CDBPerson;

class CDBAccount
{

    var $pdo = null;
    var $TABLE = "gener_user";
    var $TABLE_HISTORY = "gener_loginHistory";
    var $TABLE_PROFILE = "gener_user_profile";
    var $TABLE_SSO = "gener_user_sso";
    var $TABLE_PERSON = "gener_person";
    public $TABLE_INFORMATION_FOLLOW = 'gener_information_follow';
    public $TABLE_INFORMATION_CATEGORY = 'gener_information_category';
    public $TABLE_PERMISSION = 'gener_permission';


    /**
     * @codeCoverageIgnore
     */
    private function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 用户以任何方式登录
     * @param $loginName 登录名
     * @param $password  用户密码
     * @return int 用户id或-2表示用户不存在，-1代表密码错误
     */
    public function userLogin(string $loginName, string $password): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,password FROM gener_user WHERE email='{$loginName}' OR username = '$loginName' OR phone = '$loginName'";
        $result = $this->pdo->query($sql);
        if (count($result) > 0) {
            if ($result[0]['password'] == $password) {
                return $result[0]['id'];
            } else {
                return -1;
            }
        } else {
            return -2;
        }
    }

    /**
     * 用户以微信登录
     * @param $openid 微信的openid
     * @return mix 登录成功返回用户id，false登录失败
     */
    public function userLoginWithWechat(string $openid)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM gener_user WHERE openid = '$openid' ";

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result['id'];
        }
    }

    /**
     * 用户通过小程序登录
     * @param string $openid 小程序的openId
     * @param int $miniType 小程序的类型
     * @return mix 登录成功返回用户id，false登录失败
     */
    public function userLoginWithMini(string $openid, int $miniType)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $param = '';
        if ($miniType == 1) {
            $param = 'mini_openid';
        } else if ($miniType == 2) {
            $param = 'aiMiniOpenId';
        }

        $sql = "SELECT id FROM gener_user WHERE $param = '$openid' ";

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result['id'];
        }
    }

    /**
     * 用户通过unionId登录
     * @param string $unionId 微信开放平台的unionId
     * @return mix 登录成功返回用户id，false登录失败
     */
    public function userLoginWithUnionId(string $unionId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM gener_user WHERE wx_unionId = '$unionId' ";

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result['id'];
        }
    }

    /**
     * 检查登录依据是否存在
     * @param $loginName 登录依据，比如邮箱，用户名，手机
     * @return bool true为存在，false为不存在
     */
    public function checkUserLoginName(string $loginName): bool
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM gener_user WHERE email='$loginName' OR username = '$loginName' 
            OR phone = '$loginName' ";
        $result = $this->pdo->query($sql);
        if (count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 检查微信用户是否已经绑定
     * @param $openid 微信的openid
     * @return boolean true为绑定,false为未绑定
     */
    public function checkWechatAccountExist(string $openid): bool
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM gener_user WHERE openid = '$openid'";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 注册
     * @param $user 用户对象
     * @return int 插入的id
     */
    private function register(User $user): int
    {
        $ip = Util::getRequestIp();
        $port = Util::getRequestPort();
        $sql = "";
        if ($user->openid) {
            $sql = "INSERT INTO gener_user
                (
                nickname,
                username,
                email,
                phone,
                password,
                hx_username,
                hx_password,
                openid,
                wx_unionId,
                photo,
                gender,
                country,
                province,
                city,
                create_time,
                update_time,
                regis_ip,
                regis_port)
                VALUES
                (
                '{$user->username}',
                '{$user->username}',
                '{$user->email}',
                '{$user->phone}',
                '{$user->password}',
                '{$user->hxUsername}',
                '{$user->hxPassword}',
                '{$user->openid}',
                '{$user->unionid}',
                '{$user->photo}',
                '{$user->gender}',
                '{$user->country}',
                '{$user->province}',
                '{$user->city}',
                now(),
                now(),
                '$ip',
                '$port')";
        } else if ($user->miniOpenid || $user->aiMiniOpenId) {
            $sql = "INSERT INTO gener_user
            (
            nickname,
            username,
            email,
            phone,
            password,
            hx_username,
            hx_password,
            mini_openid,
            aiMiniOpenId,
            wx_unionId,
            photo,
            gender,
            country,
            province,
            city,
            create_time,
            update_time,
            regis_ip,
            regis_port)
            VALUES
            (
            '{$user->username}',
            '{$user->username}',
            '{$user->email}',
            '{$user->phone}',
            '{$user->password}',
            '{$user->hxUsername}',
            '{$user->hxPassword}',
            '{$user->miniOpenid}',
            '{$user->aiMiniOpenId}',
            '{$user->unionid}',
            '{$user->photo}',
            '{$user->gender}',
            '{$user->country}',
            '{$user->province}',
            '{$user->city}',
            now(),
            now(),
                regis_ip,
                regis_port)";
        } else {
            $sql = "INSERT INTO gener_user
                (
                nickname,
                username,
                email,
                phone,
                photo,
                password,
                hx_username,
                hx_password,
                create_time,
                update_time,
                regis_ip,
                regis_port)
                VALUES
                (
                '{$user->username}',
                '{$user->username}',
                '{$user->email}',
                '{$user->phone}',
                '{$user->photo}',
                '{$user->password}',
                '{$user->hxUsername}',
                '{$user->hxPassword}',
                now(),
                now(),
                regis_ip,
                regis_port)";
        }

        return $this->pdo->insert($sql);
    }

    private function addUserProfile($userId)
    {
        $sql = "INSERT INTO $this->TABLE_PROFILE(userId,post_num,collection_num,family_num,create_time,create_by,update_time,update_by)
        VALUES('$userId',0,0,0,now(),'$userId',now(),'$userId')";
        return $this->pdo->insert($sql);
    }

    /**
     * 用户注册的初始化操作,所有的初始化操作都写在这里
     */
    private function userRegisterInit($userId)
    {
        //插入用户之后更新它的profile记录
        $this->addUserProfile($userId);

        //初始化用户的积分
        $pointDB = new CDBPoint();
        $pointDB->initUserPoint($userId);
    }

    /**
     * 没有事务的用户注册,用户注册事件中只会调用这个函数或者userRegister()
     * @param $user 用户对象
     * @return int 用户id
     */
    public function userRegisterWithoutTransaction(User $user): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($user->photo === "")
            $user->photo = PhotoGenerator::generateUserPhotoFromResource();
        $userId = $this->register($user);

        $this->userRegisterInit($userId);

        return $userId;
    }

    /**
     * 有事务的用户注册,用户注册事件中只会调用这个函数或者userRegisterWithoutTransaction()
     * @param $user 用户对象
     * @return int 用户id或者-1
     */
    public function userRegister(User $user): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();

            if ($user->photo === "")
                $user->photo = PhotoGenerator::generateUserPhotoFromResource();
            $userId = $this->register($user);

            $this->userRegisterInit($userId);

            $this->pdo->commit();
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            // @codeCoverageIgnoreEnd
        }
        return $userId;
    }

    /**
     * 用户通过分享链接注册
     * @param $user 用户对象
     * @param $shareCode 分享码
     * @return int 用户id或者-1
     */
    public function userRegisterWithShareCode(User $user, string $shareCode, $fromUserId, $type): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        //检查是否已经在家族中
        $sql = "SELECT familyId FROM $this->TABLE_PERSON WHERE share_url = '$shareCode' and is_delete = '0' ";
        $shareCodeFamilyId = $this->pdo->uniqueResult($sql);

        if ($shareCodeFamilyId == null) {
            return -1;
        }

        $familyId = $shareCodeFamilyId['familyId'];

        $familyDB = new CDBFamily();
        $family = $familyDB->getFamilyById($familyId);
        $socialCircleId = $family->socialCircleId;

        try {
            $this->pdo->beginTransaction();
            $userId = $this->userRegisterWithoutTransaction($user);    //注册用户
            //绑定身份到shareCode指向的人物
            $personDao = new CDBPerson();
            $count = $personDao->bindUserToPersonWithoutTransaction($userId, $shareCode, $fromUserId, $type, $familyId);

            $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
                VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
            $followId = $this->pdo->insert($sql);
            if ($followId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                $this->pdo->rollback();
                exit;
            }
            $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow+1 WHERE id='$socialCircleId' AND is_delete=0";
            $updateRow = $this->pdo->update($sql);
            if ($updateRow < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
                $this->pdo->rollback();
                exit;
            }
            $this->pdo->commit();
            return $userId;
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * 根据id查询用户信息,这个函数是用来给其他用户请求的，不涉及私密信息
     * @param $id 用户id
     * @return array 获取用户信息的array
     */
    public function getUserInfoById(int $id)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,username,hx_username,phone,email,nickname,photo,gender,birthday,country,country_name as countryName,province,province_name as provinceName,city,city_name as cityName,area,area_name as areaName,town,town_name as townName,address,defaultFamilyId,pdf_permission as pdfPermission FROM gener_user WHERE id='{$id}'";

        $result = $this->pdo->query($sql);   //true代表设置缓存
        if (count($result) > 0) {

            // 这里还要查出来aliasUserId
            $alias = $this->getUserAlias($id);
            $result[0]['alias'] = $alias;
            return $result[0];
        } else {
            return null;
        }
    }

    /**
     * 根据HxId查询用户信息,这个函数是用来给其他用户请求的，不涉及私密信息
     * @param $hxUsernames 环信用户名数组
     * @return array 获取用户信息的array
     */
    public function getUserInfosByHxUsernames(array $hxUsernames)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $in_sql = "";
        $isFirst = true;
        foreach ($hxUsernames as $hxUsername) {
            if ($isFirst) {
                $in_sql .= "'" . $hxUsername . "'";
                $isFirst = false;
            } else {
                $in_sql .= ",'" . $hxUsername . "'";
            }
        }

        $sql = "SELECT id,username,nickname,hx_username as hxUsername,photo,gender,birthday,country,country_name as countryName,province,province_name as provinceName,city,city_name as cityName,area,area_name as areaName,town,town_name as townName,address FROM gener_user 
        WHERE hx_username in ($in_sql)";

        return $this->pdo->query($sql);   //true代表设置缓存
    }

    /**
     * 根据用户id获取环信的信息
     * @param $userId 用户id
     * @return array or null 环信信息
     */
    public function getHuanxinInfoById(int $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,hx_username as hxUsername,hx_password as hxPassword FROM gener_user WHERE id='{$userId}'";

        return $this->pdo->uniqueResult($sql);   //true代表设置缓存
    }

    /**
     * 查询用户信息，只能是用户自己请求，涉及到个人的私密信息
     * @param $id  用户id
     * @return map 返回用户资料
     */
    public function getUserInfo(int $id)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,user_type,username,hx_username,nickname,phone,email,photo,gender,birthday,country,country_name as countryName,province,province_name as provinceName,city,city_name as cityName,area,area_name as areaName,town,town_name as townName,address,create_time as createTime,update_time as updateTime FROM gener_user where id = '{$id}'";
        $result = $this->pdo->uniqueResult($sql, false, $id . 'info');
        return $result;
    }

    /**
     * 获取用户资料，比如家族数，推文数，收藏数
     * @param $userId 用户id
     * @return map 返回用户资料
     */
    public function getUserProfile(int $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT post_num as postNum,collection_num as collectionNum,family_num as familyNum, usePrivateDB FROM $this->TABLE_PROFILE WHERE userId = '$userId'";

        $result = $this->pdo->uniqueResult($sql);
        return $result;
    }

    /**
     * 检查用户id是否存在
     * @param int $userId
     * @return bool true存在 false不存在
     */
    public function isUserIdExist(int $userId): bool
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT count(*) FROM gener_user where id = '{$userId}'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['count(*)'] > 0) {
            return true;
        }
        return false;
    }

    /**
     * 更新用户信息
     * @param  $gender 用户性别
     * @param $birthday 用户生日
     * @param $country 国家编码
     * @param $province 省份编码
     * @param $city     城市编码
     * @param $area     城区编码
     * @param $town     乡镇编码
     * @param $address  地址
     * @param $userId   用户id
     * @return int 更新的用户记录的条数
     */
    public function updateUserInfo(int $gender, $birthday, int $country, string $countryName, int $province, string $provinceName, int $city, string $cityName, int $area, string $areaName, int $town, string $townName, string $address, int $userId): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

         //记录用户日志
         $function_name = __CLASS__.'->'.__FUNCTION__;
         $option_detail = '更新用户基本信息';
         $other_msg = [
             'gender' => $gender,
             'birthday' => $birthday,
             'country' => $country,
             'countryName' => $countryName,
             'province' => $province,
             'provinceName' => $provinceName,
             'city' => $city,
             'cityName' => $cityName,
             'area' => $area,
             'areaName' => $areaName,
             'town' => $town,
             'townName' => $townName,
             'address' => $address,
            ];
         $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
         $optionTableName = 'gener_user';
         $tableId = $userId;
 
         $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

         
        if ($birthday == null || $birthday == "") {
            $birthday = "NULL";
        } else {
            $birthday = "'$birthday'";
        }

        $sql = "UPDATE gener_user SET gender = '$gender' , birthday = $birthday ,country='$country',country_name='$countryName', province='$province',province_name='$provinceName',city='$city',city_name='$cityName',area='$area',area_name='$areaName',town='$town',town_name='$townName', address = '$address',update_time = now() WHERE id = '$userId' ";

        $count = $this->pdo->update($sql, true, $userId . 'info');
        return $count;
    }

    /**
     * 绑定手机
     * @param $phone
     * @param $userId
     *
     */
    public function updatePhone(string $phone, int $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新用户手机号';
        $other_msg = [
            'phone' => $phone
           ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = 'gener_user';
         $tableId = $userId;

        $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        $sql = "UPDATE gener_user SET phone = '$phone',update_time=now() WHERE id = '$userId'";
        return $this->pdo->update($sql, true, $userId . 'info');
    }

    /**
     * 绑定手机和密码，这个接口是小程序使用的
     * @param string $phone
     * @param string $password
     * @param int $userId
     * @return mixed
     */
    public function updatePhoneAndPassword(string $phone, string $password, int $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '小程序更新用户手机号和密码';
        $other_msg = [
            'phone' => $phone
           ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = 'gener_user';
         $tableId = $userId;

        $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        $sql = "UPDATE gener_user SET phone = '$phone',update_time=now(), password='$password' WHERE id = '$userId'";
        return $this->pdo->update($sql, true, $userId . 'info');
    }

    /*
     *修改用户昵称
     * @param $nickname 用户昵称
     *
     * */
    public function updateNickname(string $nickname, int $userId): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        
        $sql = " SELECT nickname FROM gener_user WHERE id = '$userId' ";
        $res = $this->pdo->uniqueResult($sql);
        $oldNickName = $res['nickname'];

        if($oldNickName == $nickname){
            return 1;
        }


        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新用户昵称';
        $other_msg = [
            'newNickname' => $nickname,
            ' oldNickName' =>  $oldNickName
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = 'gener_user';
         $tableId = $userId;

        $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        $sql = "UPDATE gener_user SET nickname = '$nickname',update_time=now() WHERE id = '$userId'";
        $count = $this->pdo->update($sql, true, $userId . 'info');
        return $count;
    }

    /**
     * 用户添加照片
     * @param $userId 用户id
     * @param $path   图片路径
     * @return 更新的记录数
     */
    public function addPhotoforUser(int $userId, string $path): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新用户头像';
        $other_msg = [
            'path' => $path
           ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = 'gener_user';
         $tableId = $userId;

        $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        $sql = "UPDATE gener_user SET photo = '$path',update_time = now() WHERE id = $userId";
        $count = $this->pdo->update($sql, true, $userId . 'info');

        return $count;
    }

    /**
     * 指定设备下的检验userId和token的有效性，即web登录和app登录不会互相干扰
     * @param $userId 用户id
     * @param $token  生成的token
     * @param $device 设备类型
     * @param $deviceCode 设备code
     * @return bool true 验证成功 false 验证失败
     */
    public function verifyUserIdAndTokenWithDevice(int $userId, string $token, int $device, string $deviceCode = ""): bool
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($device == $GLOBALS['LOGIN_DEVICE_WECHAT']) {
            //微信登录会挤掉
            $sql = "SELECT count(id) FROM $this->TABLE_HISTORY WHERE userId = '$userId'  AND device = '$device' AND success = '1' AND is_logout = '0' AND token = '$token'  order by id desc limit 0,1";

            $res = $this->pdo->uniqueResult($sql);          //这里只取了第一个

            if ($res == null) {
                return false;
            }

            if ($res['count(id)'] > 0) {
                return true;
            }
            return false;
        } else if ($device == $GLOBALS['LOGIN_DEVICE_APP']) {
            //手机登录会挤掉
            $sql = "SELECT token FROM $this->TABLE_HISTORY WHERE userId = '$userId'  AND device = '$device' AND success = '1' AND is_logout = '0' order by id desc limit 0,1";

            $res = $this->pdo->uniqueResult($sql);          //这里只取了第一个
            if ($res == null) {
                return false;
            }
            $storageToken = $res['token'];

            if ($storageToken == $token) {
                return true;
            }
            return false;
        } else if ($device == $GLOBALS['LOGIN_DEVICE_BROWSER']) {
            //网页登录默认不会挤掉
            $sql = "SELECT count(id) FROM $this->TABLE_HISTORY WHERE userId = '$userId' AND success = '1' AND token = '$token'  AND is_logout = '0' ";

            $res = $this->pdo->uniqueResult($sql);
            if ($res == null) {
                return false;
            }
            if ($res['count(id)'] > 0) {
                return true;
            }
            return false;
        }
        // @codeCoverageIgnoreStart
    }
    // @codeCoverageIgnoreEnd

    /**
     * 根据邮箱获取用户id
     * @param $email 邮箱
     * @return int 用户id或者null
     */
    public function getUserIdByEmail(string $email)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM gener_user WHERE email = '$email'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null)
            return $result['id'];
        return null;
    }

    /**
     * 根据用户名获取用户id
     * @param $username 用户名
     * @return int 用户id或者null
     */
    public function getUserIdByUserName(string $username)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM gener_user WHERE username = '$username'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null)
            return $result['id'];
        return null;
    }

    /**
     * 根据手机获取用户id
     * @param $phone 用户名
     * @return int 用户id或者null
     */
    public function getUserIdByPhone(string $phone)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM gener_user WHERE phone = '$phone'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null)
            return $result['id'];
        return null;
    }

    /**
     * 根据登录依据获取用户id
     * @param $loginName 用户名
     * @return int 用户id或者null
     */
    public function getUserIdByLoginName(string $loginName)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM gener_user WHERE phone = '$loginName' OR username = '$loginName'
            OR email = '$loginName' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null)
            return $result['id'];
        return null;
    }

    /**
     * 获取登录的总次数
     * @param $userId 用户id
     * @return int 登录的次数
     */
    public function getLoginTimes(int $userId): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT count(*) FROM $this->TABLE_HISTORY WHERE userId = $userId";

        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result['count(*)'];
        } else {
            //@codeCoverageIgnoreStart
            return 0;
            //@codeCoverageIgnoreEnd
        }
    }


    /**
     *  获取登录历史，支持分页查询，倒序输出
     * @param $userId 用户id
     * @param $pageIndex 页号
     * @param $pageSize  每页的数据数量
     * @return 登录历史的结果
     */
    public function getLoginHistory(int $userId, int $pageIndex, int $pageSize): array
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $limit = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT login_name as loginName,device,login_ip as loginIp,login_time as loginTime,success FROM $this->TABLE_HISTORY WHERE userId = $userId ORDER BY id DESC limit $limit,$pageSize";

        return $this->pdo->query($sql);
    }

    /**
     * 插入登录历史
     * @param $loginName 登录名
     * @param $device    登录设备
     * @param $userId    用户id
     * @param $token     产生的token
     * @param $loginIp   登录ip
     * @param $loginTime 登录时间
     * @param $success   是否登录成功
     * @param $deviceCode 设备码（手机APP要求)
     * @return int 插入的记录数
     */
    public function insertLoginHistory(string $loginName, int $device, int $userId, string $token, string $loginIp, string $loginTime, int $success, string $deviceCode = '', $systemUrl = '')
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $subToken = '';

        try {

            $this->pdo->beginTransaction();

            $port = Util::getRequestPort();

            //插入登录历史
            $sql = "INSERT INTO $this->TABLE_HISTORY(login_name,device,device_code,userId,token,login_ip,login_port,
            login_time,success)
            VALUES('$loginName','$device','$deviceCode','$userId','$token','$loginIp','$port',
            '$loginTime','$success')";

            $result = $this->pdo->insert($sql);

            if ($systemUrl != '' && $systemUrl != null) {
                $subToken = md5(time() . Util::generateRandomCode(6) . $loginIp);   //随机生成子系统的token
                //插入子系统授权认证
                $sql = "INSERT INTO $this->TABLE_SSO
                (token,system_url,userId,is_delete,create_time)
                VALUES
                ('$subToken','$systemUrl',$userId,0,now())";

                $this->pdo->insert($sql);
            }

            $this->pdo->commit();
            // @codeCoverageIgnoreStart
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "SQL插入失败");
            exit;
            // @codeCoverageIgnoreEnd
        }
        if ($subToken == '') {
            return $result;
        } else {
            return $subToken;
        }
    }


    /**
     * 更新用户密码
     * @param $userId 用户id
     * @param $password 密码
     * @return int 更新的记录数
     */
    public function updateUserPassword(int $userId, string $password): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE gener_user SET password = '$password',update_time = now() WHERE id = '$userId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 绑定用户手机号
     * @param $userId 用户id
     * @param $phone  用户手机号
     * @return bool true成功,false失败
     */
    public function bindUserPhone(int $userId, string $phone): bool
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }
        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新用户手机号';
        $other_msg = [
            'phone' => $phone
           ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = 'gener_user';
         $tableId = $userId;

        $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        $sql = "UPDATE gener_user SET phone = '$phone',update_time = now() WHERE id = '$userId' ";
        $rows = $this->pdo->update($sql, true, $userId . 'info');
        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 绑定用户邮箱
     * @param $userId 用户id
     * @param $email  用户邮箱
     * @return bool true成功,false失败
     */
    public function bindUserEmail(int $userId, string $email): bool
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新用户邮箱';
        $other_msg = [
            'email' => $email
           ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = 'gener_user';
         $tableId = $userId;

        $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        $sql = "UPDATE gener_user SET email = '$email',update_time = now() WHERE id = '$userId' ";
        $rows = $this->pdo->update($sql, true, $userId . 'info');
        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 绑定微信账号
     * @param $userId 用户id
     * @param $openid 微信的openid
     * @return boolean 绑定成功true,失败false
     */
    public function bindWechat(int $userId, string $openid, $unionId): bool
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE gener_user SET openid = '$openid', wx_unionId = '$unionId', update_time = now() WHERE id = '$userId' ";
        $rows = $this->pdo->update($sql);
        if ($rows > 0) {
                
            //记录用户日志
            $function_name = __CLASS__.'->'.__FUNCTION__;
            $option_detail = '用户绑定微信';
            $other_msg = [
                'openid' => $openid,
                'unionId' => $unionId
            ];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $optionTableName = 'gener_user';
            $tableId = $userId;

            $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

            return true;
        } else {
            return false;
        }
    }

    /**
     * 绑定微信小程序
     * @param $userId 用户id
     * @param $openid 小程序的openid
     * @param $unionId 联合id
     * @param $miniType 小程序的类型，1是递名帖，2是AI族群
     * @return boolean 绑定成功true,失败false
     */
    public function bindMini(int $userId, string $openid, $unionId, $miniType): bool
    {
        if (!$this->init()) { // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $miniOPenIdParam = '';
        if ($miniType == 1) {
            $miniOPenIdParam = 'mini_openid';
        } else if ($miniType == 2) {
            $miniOPenIdParam = 'aiMiniOpenId';
        }

        $sql = "UPDATE gener_user SET $miniOPenIdParam = '$openid', wx_unionId = '$unionId', update_time = now() WHERE id = '$userId' and ($miniOPenIdParam is null OR $miniOPenIdParam = '') ";
        $rows = $this->pdo->update($sql);
        if ($rows > 0) {
            
            //记录用户日志
            $function_name = __CLASS__.'->'.__FUNCTION__;
            $option_detail = '用户绑定小程序';
            $other_msg = [
                'openid' => $openid,
                'unionid' => $unionId,
                'minitype' => $miniType
            ];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $optionTableName = 'gener_user';
            $tableId = $userId;

            $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 用户退出登录
     * @param $userId 用户id
     * @param $token  登录的token
     * @return int 更新的记录数
     */
    public function userLogout(int $userId, string $token): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_HISTORY SET is_logout = '1' WHERE userId = '$userId' AND success = '1' AND is_logout = '0' AND token = '$token'   ";

        return $this->pdo->update($sql);
    }

    /**
     * 获取全网的用户
     */
    public function getAllSystemUsers()
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id as userId FROM $this->TABLE";
        return $this->pdo->query($sql);
    }

    /**
     * 检查子系统令牌是否正确
     * @param $systemUrl
     * @param $subToken
     * @return mixed
     */
    public function checkSubSystemToken($systemUrl, $subToken)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT ts.userId,tu.username,tu.nickname,tu.photo,tu.gender FROM $this->TABLE_SSO ts 
                left join $this->TABLE tu on tu.id = ts.userId 
                WHERE ts.system_url = '$systemUrl' AND ts.token = '$subToken' AND ts.is_delete = 0 ";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取用户密码
     * @param int $userId 用户id
     * @return string 密码
     */
    public function getPassword($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT password FROM $this->TABLE WHERE id = '$userId' ";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return null;
        } else {
            return $result['password'];
        }
    }

    /**
     * 检查账号是否绑定了小程序
     * @param int $userId 用户id
     * @return bool 绑定true,未绑定false
     */
    public function checkAccountBindMini(int $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT mini_openid FROM $this->TABLE WHERE id = '$userId' ";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null || $result['mini_openid'] == null || $result['mini_openid'] == '') {
            return false;
        }

        return true;
    }

    /**
     * 检查账号是否绑定了手机或邮箱
     */
    public function checkAccountBindPhoneOrEmail(int $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT phone, email FROM $this->TABLE WHERE id = '$userId' ";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null || (empty($result['phone']) && empty($result['email']))) {
            return false;
        }

        return true;
    }

    /**
     * 将小程序账号移动到爱族群账号
     * @param string $loginName 登录名
     * @param string $password 密码
     * @return mixed true成功,-1 账号错误, -2已绑定小程序, -3数据库异常
     */
    public function moveMiniAccount($loginName, $password, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $password = hash("sha256", $password . $GLOBALS['SHA256_SALT']);
            $sql = "SELECT id FROM $this->TABLE WHERE email='{$loginName}' OR username = '$loginName' OR phone = '$loginName' AND password = '$password' ";
            $account = $this->pdo->uniqueResult($sql);
            if ($account == null) {
                return -1;
            }

            $targetUserId = $account['id'];

            if ($this->checkAccountBindMini($targetUserId)) {
                // 已经绑定了小程序,则失败
                return -2;
            }

            if ($this->checkAccountBindPhoneOrEmail($userId)) {
                // 小程序账号已经绑定了手机或邮箱
                // @codeCoverageIgnoreStart
                return -4;
                // @codeCoverageIgnoreEnd
            }


            $sql = "SELECT mini_openid,wx_unionId FROM $this->TABLE WHERE id = '$userId' ";
            $currentUser = $this->pdo->uniqueResult($sql);
            $miniOpenid = $currentUser['mini_openid'];
            $wxUnionId = $currentUser['wx_unionId'];

            $this->pdo->beginTransaction();

            $infoCardDB = new CDBInfoCard();
            $infoCardDB->changeUserIdWithoutTransaction($userId, $targetUserId);    // 修改名片

            $unionSql = '';
            if (!empty($wxUnionId)) {
                // @codeCoverageIgnoreStart
                $unionSql = ",wx_unionId = '$wxUnionId' ";
                // @codeCoverageIgnoreEnd
            }
            // 修改小程序openid绑定
            $sql = "UPDATE $this->TABLE SET mini_openid = '$miniOpenid' $unionSql WHERE id = '$targetUserId' ";
            $this->pdo->update($sql);

            // 删除小程序账号
            $sql = "UPDATE $this->TABLE SET is_delete = '1', mini_openid = '', wx_unionId = '', target_user = '$targetUserId' WHERE id = '$userId' ";

            $this->pdo->update($sql);

            $this->pdo->commit();

            return true;
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -3;
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * 将小程序账号移动到爱族群账号,这里是通过手机号验证码的方式
     * @param string $phone 手机号
     * @param string $vcode 验证码
     * @return mixed true成功,-1 账号错误, -2已绑定小程序, -3数据库异常
     */
    public function moveMiniAccountWithPhone($phone, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $sql = "SELECT id FROM $this->TABLE WHERE phone = '$phone' ";
            $account = $this->pdo->uniqueResult($sql);
            if ($account == null) {
                return -1;
            }

            $targetUserId = $account['id'];

            if ($this->checkAccountBindMini($targetUserId)) {
                // 已经绑定了小程序,则失败
                return -2;
            }

            if ($this->checkAccountBindPhoneOrEmail($userId)) {
                // 小程序账号已经绑定了手机或邮箱
                // @codeCoverageIgnoreStart
                return -4;
                // @codeCoverageIgnoreEnd
            }


            $sql = "SELECT mini_openid,wx_unionId FROM $this->TABLE WHERE id = '$userId' ";
            $currentUser = $this->pdo->uniqueResult($sql);
            $miniOpenid = $currentUser['mini_openid'];
            $wxUnionId = $currentUser['wx_unionId'];

            $this->pdo->beginTransaction();

            // $infoCardDB = new CDBInfoCard();
            // $infoCardDB->changeUserIdWithoutTransaction($userId, $targetUserId);    // 修改名片

            $unionSql = '';
            if (!empty($wxUnionId)) {
                // @codeCoverageIgnoreStart
                $unionSql = ",wx_unionId = '$wxUnionId' ";
                // @codeCoverageIgnoreEnd
            }
            // 修改小程序openid绑定
            $sql = "UPDATE $this->TABLE SET mini_openid = '$miniOpenid' $unionSql WHERE id = '$targetUserId' ";
            $this->pdo->update($sql);

            // 删除小程序账号
            $sql = "UPDATE $this->TABLE SET is_delete = '1', mini_openid = '', wx_unionId = '', target_user = '$targetUserId' WHERE id = '$userId' ";

            $this->pdo->update($sql);

            // 添加主账号和小程序账号的关联
            $this->addAliasUserIdRecord($targetUserId, $userId);

            $this->pdo->commit();

            return true;
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -3;
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * 检查MiniOpenId是否存在
     * @param $miniOpenId 小程序的openId
     * @param $miniType 小程序类型，1是递名帖，2是AI族群
     * @return bool  存在true,不存在false
     */
    public function checkMiniOpenIdExist($miniOpenId, $miniType)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $openIdParam = 'mini_openid';

        if ($miniType == 1) {
            $openIdParam = 'mini_openid';
        } else {
            $openIdParam = 'aiMiniOpenId';
        }

        $sql = "SELECT id FROM $this->TABLE WHERE $openIdParam = '$miniOpenId' AND is_delete = '0' ";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 检查unionId是否存在
     * @param $unionId 微信公众平台的联合id
     * @return bool  存在true,不存在false
     */
    public function checkUnionIdExist($unionId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE wx_unionId = '$unionId' AND is_delete = '0' ";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return false;
        } else {
            return true;
        }
    }

    public function addPhoneVcodeRecord($phone, $vcode, $type)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO gener_phone_vcode (phone,vcode,type,isUse,createTime,updateTime)
        VALUES ('$phone','$vcode','$type',0,now(),now())";
        return $this->pdo->insert($sql);
    }

    public function updatePhoneVcodeRecord($recordId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "UPDATE gener_phone_vcode SET isUse=1 WHERE id='$recordId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    public function getPhoneVcodeRecord($recordId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,phone,vcode,type,isUse,createTime,updateTime FROM gener_phone_vcode WHERE id='$recordId' AND isDelete=0";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 添加用户id的关联
     * @param $userId 用户id
     * @param $aliasUserId 要关联的用户id
     */
    public function addAliasUserIdRecord($userId, $aliasUserId) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO gener_user_alias
                (userId, aliasUserId, createTime, createBy, updateTime, updateBy)
                VALUES
                ('$userId', '$aliasUserId', now(), '$userId', now(), '$userId')";

        return $this->pdo->update($sql);
    }

    /**
     * 获取用户所有关联的用户id
     * @param $userId 用户id
     * @return array 用户id数组
     */
    public function getUserAlias($userId) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT aliasUserId FROM gener_user_alias WHERE userId = '$userId' ";
        $result = $this->pdo->query($sql);

        $arr = [];

        foreach($result as $user) {
            $arr[] = $user['aliasUserId'];
        }

        $arr[] = $userId;

        return $arr;
    }

    /**
     * 获取用户所有关联的用户id
     * @param $userId 用户id
     * @return string 用户id数组, (1,2,3)
     */
    public function getUserAliasStr($userId) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $arr = $this->getUserAlias($userId);

        $str = '('.Util::arrayToString($arr).')';

        return $str;
    }


    /**
     * 检查名片的权限
     * @param $cardUserId 名片上的用户id
     * @param $targetUserId 要判断的用户id
     * @return bool true 有权限，false没有权限
     */
    public function checkUserPermission($cardUserId, $targetUserId) {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取关联的所有用户
        $userIdAlias = $this->getUserAlias($targetUserId);

        foreach($userIdAlias as $userId) {
            if ($cardUserId == $userId) {
                return true;
            }
        }

        return false;
    }

    /**
     * 根据即时聊天的用户名来获取用户id
     * @param $imUser 即时聊天系统的用户
     * @return mix int/null
     */
    public function getUserIdByIMUsername($imUser) {
        $this->init();
        $sql = "SELECT id FROM $this->TABLE WHERE hx_username = '$imUser' AND is_delete = '0' ";
        $res = $this->pdo->uniqueResult($sql);

        if ($res == null) {
            return null;
        } else {
            return $res['id'];
        }
    }

    /**
     * 检查是否是系统用户
     * @param $userId
     * @return bool
     */
    public function isSystemUser($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT user_type FROM $this->TABLE WHERE id='$userId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            if ($result['user_type'] == 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 开通私有数据库
     * @param int $userId 用户id
     * @return int 修改的记录数
     */
    public function enablePrivateDB($userId) {    
        $this->init();

        $sql = "UPDATE $this->TABLE_PROFILE SET usePrivateDB = 1 where userId = '$userId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 获取腾讯云通讯TencentIM账号
     */
    public function getTencentIMName(){
        $this->init();
        $sql = "SELECT id, name FROM tencent_IM_user where userId = 0 order by id asc limit 1";
        $res = $this->pdo->uniqueResult($sql);
        if(!empty($res)){
            return $res;
        }else{
            //如果为空， 说明数据用完了。 给一个默认空值
            return ['name' => '', 'id' => '0'];
        }
    }
    
    /**
     * 云通讯账号绑定userId
     */
    public function updateTencentIMUserId($id, $userId){
        $this->init();
        $sql = "UPDATE tencent_IM_user set userId = '$userId' where id = '$id' and userId = 0 ";
        return $this->pdo->update($sql);
        
    }


    /**
     * 检测登录用户名 一定时间内 的登录失败的次数
     * 默认是十分钟内
     * 返回失败的次数
     */
    public function checkLoginFalseTime($loginName, $timeLength = 600){
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        
        $time = date('Y-m-d H:i:s', time() - $timeLength);
        $sql = "SELECT count(*) FROM $this->TABLE_HISTORY WHERE login_name = '$loginName' AND success = 0 AND  login_time > '$time'  ";

        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result['count(*)'];
        } else {
            //@codeCoverageIgnoreStart
            return 0;
            //@codeCoverageIgnoreEnd
        }

    }

        /**
     * 记录人物操作日志
     */
    public function recordUserOptionLog($function_name, $option_detail, $userId, $other_msg = NULL, $optionTableName = '0', $tableId = '0'){
        $this->init();
        $ip = Util::getRequestIp();
        $port = Util::getRequestPort();
        $tableName = 'gener_user_option_log';
        $userId = intval($userId);
        if($userId == 0 && isset($GLOBALS['userId'])){
            $userId = $GLOBALS['userId'];
        }
        $deviceDetail = $GLOBALS['DeviceDetail'] ?? 'null';
        $sql = "INSERT INTO $tableName (
            function_name, option_detail, userId, record_time, update_time, other_msg, option_ip, option_port, table_name, table_id, option_device_detail
            ) VALUES (
            '$function_name', '$option_detail', $userId, now(), now(), '$other_msg', '$ip', '$port', '$optionTableName', '$tableId', '$deviceDetail'
            )";
        return $this->pdo->insert($sql);
    }


    #v3
    #v3
    #v3

//    /**
//     *
//     * 获取家族相关的用户
//     *
//     */
//    public function getUserByFamily($familyId){
//        if (!$this->init()) {
//            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
//            exit;
//        }
//
//        $sql = "SELECT *,t.id id, t.username username, tp.name name,tp.father_name fatherName,tp.create_time createTime,
//                tp.permissionType permissionType,tpe.name personName FROM $this->TABLE as t left join $this->TABLE_PERMISSION as tp
//                on t.id = tp.administratorId left join $this->TABLE_PERSON as tpe on tpe.userId = t.id
//                where tp.familyId = $familyId and t.is_delete = 0 and tp.is_delete = 0";
//
//        $result = $this->pdo->query($sql);
//
//        return $result;
//
//    }

    /**
     *
     * 更新
     *
     */
    public function updateFamilyUser($familyId){
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $sql = "SELECT *,t.id id, t.username username, tp.name name,tp.father as FROM $this->TABLE as t inner join $this->TABLE_PERSON as tp on t.id = tp.userId  where
            tp.familyId = $familyId and t.is_delete = 0 and tp.is_delete = 0";

        $result = $this->pdo->query($sql);

        return $result;

    }

    /**
     *
     * 检查用户是否存在或是否绑定
     * return int 0表示没有此用户，-1表示用在该家族已绑定，返回id则可以绑定
     */
    public function checkFamilyUser($familyId, $userName){
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $sql = "select id from $this->TABLE where is_deletd = 0 and name = $userName limit 1";
        $user = $this->pdo->uniqueResult($sql);
        if (!$user){
            return 0;
        }
        $userId = $user['id'];
        $sql = "select id from $this->TABLE_PERMISSION where is_deletd = 0 and userId =$userId 
                and familyId = $familyId limit 1";

        $result = $this->pdo->uniqueResult($sql);

        if($result){
            return -1;
        }

        return $userId;

    }

    /**
     *
     * 获取家族用户列表
     * return int 0表示没有此用户，-1表示用在该家族已绑定，返回id则可以绑定
     */
    public function getFamilyUserList($familyId, $page, $pageSize, $permission = null,$isBind = null, $createTime = null, $name = null ,$nameRange = null){
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }
        $permissionSql = $permission !== null ? " and permissionType = $permission " : "";
        $isBindSql = $isBind !== null ? " having isBind = $isBind " : "";
        $createTimeSql = $createTime !== null ? " order by p.create_time " . ($createTime ? " asc ":" desc "): "";

        if ($name !== null){
            $nameSql = $nameRange == 2 ? " and (u.username = '$name' or  u.realName = '$name') ":" and (u.username = '$name' or  u.realName = '$name' or u.fatherName = '$name') ";
        }else{
            $nameSql = "";
        }
        $offset = intval($page - 1) * $pageSize;
        $sql = "select SQL_CALC_FOUND_ROWS u.id as id, realName, fatherName, nickname,p.create_time as createTime, if(t.id >0,1,0) as isBind, 
                permissionType as permission ,username, photo,t.id as personId, t.name as personName from $this->TABLE u left join 
                (select userId,familyId,permissionType,create_time from $this->TABLE_PERMISSION where is_delete = 0) p 
                on u.id = p.userId  left join 
                (select id,userId,name,familyId from $this->TABLE_PERSON where is_delete = 0) t on u.id = t.userId and p.familyId = t.familyId
                where u.is_delete = 0 and p.familyId = $familyId" . $permissionSql .$nameSql . $isBindSql . $createTimeSql . " limit $offset,$pageSize";
        $sqlForCount = "SELECT FOUND_ROWS() as count";

        $result = $this->pdo->query($sql);
        $length = $this->pdo->uniqueResult($sqlForCount)['count'];
        return [
            'page' => $result ? $page:1,
            'count' => $length,
            'data' => $result,
        ];
    }

    /**
     *
     * 获取家族用户列表
     * return array
     */
    public function getUserList($data, $page=1, $pageSize=20){
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $filterSql = "";
        foreach ($data as $k =>$v){
            $filterSql .= " and $k = '$v' ";
        }

        $offset = intval($page - 1) * $pageSize;
        $sql = "select SQL_CALC_FOUND_ROWS u.id as id, realName, fatherName,
                username, photo from $this->TABLE u 
                where u.is_delete = 0  $filterSql order by u.create_time limit $offset,$pageSize";
        $sqlForCount = "SELECT FOUND_ROWS() as count";

        $result = $this->pdo->query($sql);
        $length = $this->pdo->uniqueResult($sqlForCount)['count'];
        return [
            'page' => $result ? $page:1,
            'count' => $length,
            'data' => $result,
        ];
    }

    /**
     * 查询用户信息，只能是用户自己请求，涉及到个人的私密信息
     * @param $id  用户id
     * @return map 返回用户资料
     */
    public function getUserInfoV3(int $id)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,user_type,username,hx_username,nickname,phone,email,photo,gender,birthday,
                address,create_time as createTime,maritalStatus,
                update_time as updateTime, realName,fatherName,birthPlace,defaultFamilyId,pdf_permission as pdfPermission FROM gener_user where id = '{$id}'";
        $result = $this->pdo->uniqueResult($sql, false, $id . 'info');
        return $result;
    }

    /**
     * 更新用户信息
     * @param array $data 更新数据
     * @param int $userId 用户id
     * @return int 更新的用户记录的条数
     */
    public function updateUserInfoV3(int $userId,array $data): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新用户基本信息';
        $other_msg = $data;
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = 'gener_user';
        $tableId = $userId;

        $sql = "UPDATE gener_user SET";

        foreach ($data as $k=>$v){
            $sql .= " $k = '$v',";
        }

        $sql .= " update_time = now() WHERE id = $userId and is_delete = 0";

        $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);
        $count = $this->pdo->update($sql, true, $userId . 'info');
        return $count;
    }

    /**
     * 更新用户信息
     * @param array $data 更新数据
     * @param int $userId 用户id
     * @return int 更新的用户记录的条数
     */
    public function updatePdfPermission(int $userId,array $data, $beUpdateUserId): int
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新用户基本信息';
        $other_msg = $data;
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = 'gener_user';
        $tableId = $userId;

        $sql = "UPDATE gener_user SET";

        foreach ($data as $k=>$v){
            $sql .= " $k = '$v',";
        }

        $sql .= " update_time = now() WHERE id in (". implode(',', $beUpdateUserId) .") and is_delete = 0";

        $this->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);
        $count = $this->pdo->update($sql, true, $userId . 'info');
        return $count;
    }

    public function getUserIdsByPhone(array $phoneList): array
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }



        $sql = "select id from $this->TABLE where is_delete = 0 and phone in (" . implode(',', $phoneList) . ")";

        return $this->pdo->query($sql);
    }
}
