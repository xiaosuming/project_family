<?php

namespace DB;

use Model\InfoCard;
use Model\Person;
use Util\PersonRecord;
use Util\RelationParser;
use Util\SysLogger;
use Util\Util;
use DB\CDBEvent;
use DB\CDBFamily;
use DB\CDBFamilyShare;
use Model\PersonByMerge;
use Model\PersonInfo;
use PDO;

use DB\CDBPushPerson;

class CDBPerson
{
    public $pdo = null;
    public $TABLE = "gener_person";
    public $TABLE_DETAIL = "gener_personDetail";
    public $TABLE_FAMILY = "gener_family";
    public $TABLE_FAMILYADMIN = "gener_familyAdmin";
    public $TABLE_EVENT = "gener_significantEvent";
    public $TABLE_JOB = "gener_job";
    public $TABLE_USER_PROFILE = "gener_user_profile";
    public $TABLE_USER = "gener_user";
    public $TABLE_PERMISSION = "gener_family_permission";
    public $TABLE_FAMILY_USER = 'gener_family_user';
    public $TABLE_FAMILY_INNER_BRANCH_PERSON = 'gener_family_inner_branch_person';
    public $TABLE_PERSON_SNAPSHOT = 'gener_person_snapshot';
    public $TABLE_INFORMATION_FOLLOW = 'gener_information_follow';
    public $TABLE_INFORMATION_CATEGORY = 'gener_information_category';
    public $TABLE_PERSON_BIND_USER_RECORD = 'gener_person_bind_user_record';
    public $TABLE2 = "gener_person2";
    public $TABLE_NEW_PERMISSION = 'gener_permission';
    public $TABLE_FAMILY_MERGE = 'gener_family_merge';
    public $TABLE_PERSON_PDF_TITLE = 'gener_person_pdf_title';
    public $TABLE_PERSON_OTHER_INFO = 'gener_person_info';


    /**
     * @codeCoverageIgnore
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }

        return true;
    }


    /**
     * @Deprecated 2017-11-22
     *
     * 验证用户id和familyId是否匹配,即该用户是属于这个家族的
     * 这个用户有查看家族内公开的信息的权限
     * @param $userId 用户id
     * @param $familyId 家族id
     * @return true 匹配，false不匹配
     */
    public function verifyUserIdAndFamilyId($userId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT * FROM $this->TABLE 
                    WHERE userId = '$userId' AND is_delete = '0' AND familyId = '$familyId'";
        $result = $this->pdo->query($sql);
        if (count($result) > 0) {
            return true;
        }
        return false;
    }


    /**
     * 验证用户是否有对某条人物记录的管理权限,这当中已经考虑了家族创始人和管理员的权限验证
     * 验证规则:1、用户是家族的管理员;2、用户是记录的创建者 3.用户是绑定的人物，或者是绑定的人物的直系亲属,4、用户是人物的分支管理员
     * 编辑人物需要验证这个
     * @param $userId 用户id
     * @param $personId 人物id
     * @return bool true代表有权限，false代表无权限
     */
    public function verifyUserIdAndPersonId($userId, $personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $person = $this->getPersonById($personId);


        if ($person == null) {
            return false;
        }

        /* 创始人权限判定 */
        $sql = "SELECT originator FROM $this->TABLE_FAMILY WHERE id = '$person->familyId' ";
        $originator = $this->pdo->uniqueResult($sql);

        if ($originator == null) {
            //家族不存在，返回false
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }

        $originatorId = $originator['originator'];
        //表示是该人物记录的创始人
        if ($originatorId == $userId) {
            return true;
        }

        /* 管理员权限判定 */
        $sql = "SELECT adminId FROM $this->TABLE_FAMILYADMIN WHERE familyId = '$person->familyId' AND isDelete=0 ";
        $admins = $this->pdo->query($sql);
        if ($admins != null) {
            //检查userId是否在admin数组中
            foreach ($admins as $admin) {
                if ($admin['adminId'] == $userId) {
                    //是管理员
                    return true;
                }
            }
        }
        /* 记录创建者权限判定 */
        if ($person->createBy != null && $person->createBy == $userId) {
            //表明当前记录由该用户创建
            return true;
        }

        /* 用户本人判定 */
        if ($person->userId == $userId) {
            return true;
        }

        /* 用户直系亲属判定 */
        $sql = "SELECT id FROM $this->TABLE WHERE userId = '$userId' AND familyId = '$person->familyId' ";
        $personId = $this->pdo->uniqueResult($sql);   //找到人物记录
        if ($person != null) {
            $personId = $personId['id'];

            //父亲
            foreach ($person->father as $linealPerson) {
                if ($linealPerson == $personId) {
                    return true;
                }
            }

            //母亲
            foreach ($person->mother as $linealPerson) {
                //@codeCoverageIgnoreStart
                if ($linealPerson == $personId) {
                    return true;
                }
                //@codeCoverageIgnoreEnd
            }

            //配偶
            foreach ($person->spouse as $linealPerson) {
                if ($linealPerson == $personId) {
                    //@codeCoverageIgnoreStart
                    return true;
                    //@codeCoverageIgnoreEnd
                }
            }

            //兄弟
            foreach ($person->brother as $linealPerson) {
                if ($linealPerson == $personId) {
                    //@codeCoverageIgnoreStart
                    return true;
                    //@codeCoverageIgnoreEnd
                }
            }

            //姐妹
            foreach ($person->sister as $linealPerson) {
                //@codeCoverageIgnoreStart
                if ($linealPerson == $personId) {
                    return true;
                }
                //@codeCoverageIgnoreEnd
            }

            //儿子
            foreach ($person->son as $linealPerson) {
                if ($linealPerson == $personId) {
                    //@codeCoverageIgnoreStart
                    return true;
                    //@codeCoverageIgnoreEnd
                }
            }

            //女儿
            foreach ($person->daughter as $linealPerson) {
                //@codeCoverageIgnoreStart
                if ($linealPerson == $personId) {
                    return true;
                    //@codeCoverageIgnoreEnd
                }
            }
        }

        // 用户是人物的分支管理员
        $familyDB = new CDBFamily();
        if ($familyDB->isAdminForPersonInBranch($userId, $personId)) {
            return true;
        }

        return false;
    }

    /**
     * 获取一个人在家族中的personId
     * @param $userId    用户id
     * @param $familyId 家族id
     * @return personId 人物id
     */
    public function getPersonId($userId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE where userId = '$userId' and familyId = '$familyId'  AND is_delete = '0' ";
        $res = $this->pdo->query($sql);
        return $res[0]['id'];
    }

    /**
     * 根据personId获取userId
     * @param $personId 人物Id
     * @return int 用户Id
     */
    public function getUserIdByPersonId($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT userId FROM $this->TABLE where id = '$personId'  AND is_delete = '0' ";
        $res = $this->pdo->query($sql);
        return $res[0]['userId'];
    }

    /**
     * 向家族中添加人物,这个方法仅在创建家族时调用
     * @param $userId   该人物对应的用户id
     * @param $familyId 该人物所属于的家族
     * @param $name     该人物的姓名
     * @param $gender   该人物的性别
     * @param $address  该人物的住址
     * @param $birthday 该人物的生日
     * @param $photo    该人物的照片
     * @return int   人物的id或者失败返回-1
     */
    public function addPersonWithNoTransaction($userId, $familyId, $name, $gender, $address, $birthday, $photo)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $userId_t = $GLOBALS['userId'];
        $shareUrl = md5($name . Util::generateRandomCode(24));    //生成唯一share_url的code

        $personId = Util::getNextId();

        $sql = "";
        if ($birthday === '' || $birthday === NULL) {
            $sql = "INSERT INTO $this->TABLE
                (id, name,address,birthday,gender,photo,share_url,level,familyId,userId,confirm,create_by,create_time,update_by,update_time)
                VALUES
                ('$personId', '$name','$address',NULL,'$gender','$photo','$shareUrl','0','$familyId','$userId','0','$userId_t',now(),'$userId',now())";
        } else {
            $sql = "INSERT INTO $this->TABLE
                (id, name,address,birthday,gender,photo,share_url,level,familyId,userId,confirm,create_by,create_time,update_by,update_time)
                VALUES
                ('$personId', '$name','$address','$birthday','$gender','$photo','$shareUrl','0','$familyId','$userId','0','$userId_t',now(),'$userId',now())";
        }
        if ($this->pdo->insert($sql) >= 0) {        //返回插入人物的id,失败则返回-1
            $id = $personId;
        } else {
            $id = -1;
        }

        //插入成功,更新自己的sister和brother字段
        if ($id > 0) {
            if ($gender == $GLOBALS['GENDER_FEMALE']) {
                $updateSql = "UPDATE $this->TABLE SET sister = '@$id|',update_time = now() WHERE id = $id";
            } else if ($gender == $GLOBALS['GENDER_MALE']) {
                $updateSql = "UPDATE $this->TABLE SET brother = '@$id|',update_time = now() WHERE id = $id";
            } else {
                return false;
            }
            $this->pdo->update($updateSql);
        }

        //更新家族成员数
        $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now()  WHERE id = '$familyId'";
        $this->pdo->update($memberCountSql);

        return $id;
    }

    /**
     * 向家族中添加人物,这个方法仅在创建家族时调用
     * @param $userId   该人物对应的用户id
     * @param $familyId 该人物所属于的家族
     * @param $name     该人物的姓名
     * @param $gender   该人物的性别
     * @param $address  该人物的住址
     * @param $birthday 该人物的生日
     * @param $photo    该人物的照片
     * @return int   人物的id或者失败返回-1
     */
    public function addPerson($userId, $familyId, $name, $gender, $address, $birthday, $photo)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        try {
            //开启事务处理
            $this->pdo->beginTransaction();
            $userId_t = $GLOBALS['userId'];
            $shareUrl = md5($name . Util::generateRandomCode(24));    //生成唯一share_url的code
            $personId = Util::getNextId();

            $sql = "INSERT INTO $this->TABLE
                (id, name,address,birthday,gender,photo,share_url,level,familyId,userId,confirm,create_by,create_time,update_by,update_time)
                VALUES
                ('$personId', '$name','$address','$birthday','$gender','$photo','$shareUrl','0','$familyId','$userId','0','$userId_t',now(),'$userId',now())";
            if ($this->pdo->insert($sql) >= 0) {        //返回插入人物的id,失败则返回-1
                $id = $personId;
            } else {
                $id = -1;
            }

            //插入成功,更新自己的sister和brother字段
            if ($id > 0) {
                //添加家族的同时要更新用户资料中的家族数
                $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1,update_time = now() WHERE userId = '$userId' ";
                $this->pdo->update($sql);

                if ($gender == $GLOBALS['GENDER_FEMALE']) {
                    $updateSql = "UPDATE $this->TABLE SET sister = '@$id|',update_time = now() WHERE id = $id";
                } else if ($gender == $GLOBALS['GENDER_MALE']) {
                    $updateSql = "UPDATE $this->TABLE SET brother = '@$id|',update_time = now() WHERE id = $id";
                }
                $this->pdo->update($updateSql);
            }

            //更新家族成员数
            $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now()  WHERE id = '$familyId'";
            $this->pdo->update($memberCountSql);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            //@codeCoverageIgnoreEnd
        }

        return $id;
    }

    /**
     * @param $personId 人物id
     * @param $zi
     * @param $remark
     * @param $name     人物姓名
     * @param $isDead
     * @param $deadTime
     * @param $country
     * @param $countryName
     * @param $province
     * @param $provinceName
     * @param $city
     * @param $cityName
     * @param $area
     * @param $areaName
     * @param $town
     * @param $townName
     * @param $address  人物地址
     * @param $birthday 人物生日
     * @param $phone
     * @param $ranking  兄弟姐妹间的排行
     * @return int 修改的记录数或者失败返回0
     */
    public function editPerson($personId, $zi, $remark, $name, $isDead, $deadTime, $country, $countryName, $province, $provinceName, $city, $cityName, $area, $areaName, $town, $townName, $address, $birthday, $phone, $zpname, $ranking)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $birthday_sql = "";

        if ($birthday == null || $birthday == "") {
            $birthday_sql = "birthday = NULL,";
        } else {
            $birthday_sql = "birthday = '$birthday',";
        }

        $deadTime_sql = "";
        if ($deadTime == null || $deadTime == "") {
            $deadTime_sql = "dead_time = NULL,";
        } else {
            $deadTime_sql = "dead_time = '$deadTime',";
        }

        //记录人物操作日志
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $person = $this->getPersonById($personId);
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $familyId = $person->familyId;
        $option_detail = '编辑人物基本信息';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = [
            'zi' => $zi,
            'zpname' => $zpname,
            'remark' => $remark,
            'name' => $name,
            'is_dead' => $isDead,
            'deadTime' => $deadTime,
            'country' => $country,
            'country_name' => $countryName,
            'province' => $province,
            'province_name' => $provinceName,
            'city' => $city,
            'city_name' => $cityName,
            'area' => $area,
            'area_name' => $areaName,
            'town' => $town,
            'town_name' => $townName,
            'address' => $address,
            'birthday' => $birthday,
            'phone' => $phone,
            'ranking' => $ranking,
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
        //end


        $sql = "UPDATE $this->TABLE 
                    SET
                    zi='$zi',
                    zpname='$zpname',
                    remark='$remark', 
                    name = '$name',
                    is_dead = '$isDead',
                    $deadTime_sql
                    country='$country',
                    country_name='$countryName',
                    province='$province',
                    province_name='$provinceName',
                    city='$city',
                    city_name='$cityName',
                    area='$area',
                    area_name='$areaName',
                    town='$town',
                    town_name='$townName',
                    address = '$address',
                    $birthday_sql
                    phone = '$phone',
                    ranking = '$ranking',
                    update_time = now() 
                    WHERE id = '$personId' AND is_delete = '0' ";

        $count = $this->pdo->update($sql);            //返回修改的数量，失败则返回-1
        return $count;
    }


    /**
     * @param $personId 人物id
     * @param $name     人物姓名
     * @param $gender   人物性别
     * @param $address  人物地址
     * @param $birthday 人物生日
     * @param $phone    电话号码
     * @param $bloodType 血型
     * @param $maritalStatus 婚姻状况
     * @param $qq       QQ联系方式
     * @return int 修改的记录数或者失败返回0
     */
    public function editPersonDetail($person, $personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $birthday_sql = "";

        if ($person->birthday == null || $person->birthday == "") {
            $birthday_sql = "birthday = NULL,";
        } else {
            $birthday_sql = "birthday = '$person->birthday',";
        }
        $deadTime_sql = "";
        if ($person->deadTime == null || $person->deadTime == "") {
            $deadTime_sql = "dead_time = NULL,";
        } else {
            $deadTime_sql = "dead_time = '$person->deadTime',";
        }

        //记录人物操作日志
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $old_person = $this->getPersonById($personId);
        $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
        $familyId = $old_person->familyId;
        $option_detail = '编辑人物基本信息';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = [
            'zi' => $person->zi,
            'zpname' => $person->zpname,
            'remark' => $person->remark,
            'name' => $person->name,
            'is_dead' => $person->isDead,
            'deadTime' => $person->deadTime,
            'country' => $person->country,
            'country_name' => $person->countryName,
            'province' => $person->province,
            'province_name' => $person->provinceName,
            'city' => $person->city,
            'city_name' => $person->cityName,
            'area' => $person->area,
            'area_name' => $person->areaName,
            'town' => $person->town,
            'town_name' => $person->townName,
            'address' => $person->address,
            'birthday' => $person->birthday,
            'phone' => $person->phone,
            'ranking' => $person->ranking,
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
        //end


        $sql = "UPDATE $this->TABLE
            SET name = '$person->name',
            zi='$person->zi',
            zpname='$person->zpname',
            remark='$person->remark',
            is_dead='$person->isDead',
            $deadTime_sql
            address = '$person->address',
            $birthday_sql
            phone = '$person->phone',
            blood_type = '$person->bloodType',
            marital_status = '$person->maritalStatus',
            country='$person->country',
            country_name='$person->countryName',
            province='$person->province',
            province_name='$person->provinceName',
            city='$person->city',
            city_name='$person->cityName',
            area='$person->area',
            area_name='$person->areaName',
            town='$person->town',
            town_name='$person->townName',
            sideText='$person->sideText',
            profileText='$person->profileText',
            qq = '$person->qq', ranking='$person->ranking',update_time = now() WHERE id = '$personId' AND is_delete = '0'";


        $count = $this->pdo->update($sql);            //返回修改的数量，失败则返回-1
        return $count;
    }

    /**
     * 师徒关系中录入人物
     * @param $person
     * @param $level
     * @param int $type
     * @return mixed
     */
    private function addPersonWithLevelForMentor($person, $level, $type = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $userId = $GLOBALS['userId'];
        $shareUrl = md5($person->name . Util::generateRandomCode(24));    //生成唯一share_url的code

        $sql = "INSERT INTO $this->TABLE (id, familyId,name,gender,photo,share_url,type,level,create_by,create_time,update_by,update_time) VALUES
        ('$person->id', '$person->familyId',
        '$person->name',
        '$person->gender',
        '$person->photo',
        '$shareUrl',
        '$type','$level','$userId',now(),'$userId',now())";

        if ($this->pdo->insert($sql) >= 0) {        //返回插入人物的id,失败则返回-1
            $id = $person->id;
        } else {
            $id = -1;
        }

        //插入成功
        if ($id > 0) {
            if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                //@codeCoverageIgnoreStart
                $updateSql = "UPDATE $this->TABLE SET sister = '@$id|',update_time = now() WHERE id = $id";
                //@codeCoverageIgnoreEnd
            } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                $updateSql = "UPDATE $this->TABLE SET brother = '@$id|',update_time = now() WHERE id = $id";
            }
            $this->pdo->update($updateSql);
        }

        //更新家族成员数
        $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now() WHERE id = '$person->familyId'";
        $this->pdo->update($memberCountSql);
        return $id;
    }


    /**
     * 向家族中添加带有层次级别的人物,添加时根据性别，向其姐妹或者兄弟字段中更新自己的id
     * @param $person person  该人物
     * @param $level    该人物的级别
     * @param $type     该人物的类型 1是本家族的，2是本家族的配偶
     * @return  int  人物的id或者失败返回-1
     */
    private function addPersonWithLevel($person, $level, $type = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $userId = $GLOBALS['userId'];
        $shareUrl = md5($person->name . Util::generateRandomCode(24));    //生成唯一share_url的code

        if ($person->birthday == null || $person->birthday == '') {
            $person->birthday = "NULL";
        } else {
            $person->birthday = "'$person->birthday'";
        }
        if ($person->deadTime == null || $person->deadTime == '') {
            $person->deadTime = "NULL";
        } else {
            $person->deadTime = "'$person->deadTime'";
        }


        $sql = "INSERT INTO $this->TABLE(id, zi,zpname,remark,branchId,name,type,profileText,sideText,country,country_name,province,province_name,city,city_name,area,area_name,town,town_name,address,birthday,gender,photo,share_url,is_dead,dead_time,level,familyId,userId,ranking,create_by,create_time,update_by,update_time)
       VALUES('$person->id', '$person->zi','$person->zpname','$person->remark','$person->branchId','$person->name','$type','$person->profileText','$person->sideText','$person->country','$person->countryName','$person->province','$person->provinceName','$person->city','$person->cityName','$person->area','$person->areaName','$person->town','$person->townName','$person->address',$person->birthday,'$person->gender','$person->photo','$shareUrl','$person->isDead',$person->deadTime,'$level','$person->familyId','0','$person->ranking','$userId',now(),'$userId',now())";


        if ($this->pdo->insert($sql) >= 0) {        //返回插入人物的id,失败则返回-1
            $id = $person->id;
        } else {
            $id = -1;
        }

        //插入成功
        if ($id > 0) {
            if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                //@codeCoverageIgnoreStart
                $updateSql = "UPDATE $this->TABLE SET sister = '@$id|',update_time = now() WHERE id = $id";
                //@codeCoverageIgnoreEnd
            } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                $updateSql = "UPDATE $this->TABLE SET brother = '@$id|',update_time = now() WHERE id = $id";
            }
            $this->pdo->update($updateSql);
        }

        //更新家族成员数
        $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now() WHERE id = '$person->familyId'";
        $this->pdo->update($memberCountSql);
        return $id;
    }

    /**
     * @param $personId
     * @return object Person对象
     */
    public function getPersonById($corePersonId, $checkDelete = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        if ($checkDelete == false) {
            $whereIsDelete = '';
        } else {
            $whereIsDelete = " AND tb.is_delete = '0' ";
        }


        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.branchId,tb.zi,tb.zpname,tb.remark,tb.name,tb.type,tb.level,
                tb.country,tb.country_name as countryName,tb.province,tb.province_name as provinceName,tb.city,tb.city_name as cityName,
                tb.area,tb.area_name as areaName,tb.town,tb.town_name as townName,tb.address,
                tb.birthday,tb.gender,tb.photo,tb.phone,tb.blood_type as bloodType,
                tb.marital_status as maritalStatus,tb.qq,tb.share_url as shareUrl,
                tb.is_dead as isDead,tb.dead_time as deadTime,
                tb.familyId,tf.groupType,tf.name as familyName,tf.photo as familyPhoto,tb.ref_familyId as refFamilyId,
                tb.userId,tb.father,tb.mother,tb.brother,tb.sister,tb.spouse,tb.son,tb.daughter,
                tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,
                tb.update_time as updateTime,tu.hx_username as hxUsername,tb.ranking ,tb.sideText, tb.profileText 

                    FROM $this->TABLE tb  
                    LEFT JOIN $this->TABLE_USER tu on tb.userId = tu.id 
                    LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId
                    WHERE tb.id = $corePersonId " . $whereIsDelete;

        $queryResult = $this->pdo->uniqueResult($sql);
        if ($queryResult != null) {
            $person = new Person($queryResult);
            $parser = new RelationParser($person);
            $person->father = $parser->getFathers();
            $person->mother = $parser->getMothers();
            $person->son = $parser->getSons();
            $person->daughter = $parser->getDaughters();
            $person->spouse = $parser->getSpouses();
            $person->brother = $parser->getBrothers();
            $person->sister = $parser->getSisters();
            return $person;
        }
        //@codeCoverageIgnoreStart
        return null;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 根据人物id数组获取人物的姓名数组
     * @param $personsId 人物的id数组
     * @return map 人物id和name的对应
     */
    public function getPersonsNameByPersonsId($personsId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,name from $this->TABLE WHERE ";

        if (count($personsId) > 0) {
            $where = "";
            $isFirst = true;
            foreach ($personsId as $id) {
                if ($isFirst) {
                    $where .= " id = $id ";
                    $isFirst = false;
                } else {
                    //@codeCoverageIgnoreStart
                    $where .= " OR id = $id";
                    //@codeCoverageIgnoreEnd
                }
            }

            $sql .= $where . " AND is_delete = '0'";
            $queryResult = $this->pdo->query($sql);
            return $queryResult;
        } else {
            //@codeCoverageIgnoreStart
            return null;
            //@codeCoverageIgnoreEnd
        }
    }


    public function updateRecord($records)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        foreach ($records as $key => $value) {
            $str = '@' . $records[$key]->targetId . "|";

            $sql = "UPDATE  $this->TABLE SET {$records[$key]->field} = CONCAT({$records[$key]->field},\"{$str}\"), update_time = now() WHERE id = {$records[$key]->id}";

            if ($this->pdo->update($sql) < 0) {
                //@codeCoverageIgnoreStart
                return false;
                //@codeCoverageIgnoreEnd
            }
        }

        return true;
    }




    /**
     * addRelatePerson时调用，更新配偶的婚姻状态
     */
    private function updateSpouseStatus($personsId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $firstFlag = true;
        $where = '';
        foreach ($personsId as $personId) {
            if ($firstFlag) {
                $where .= ' id = ' . $personId;
                $firstFlag = false;
            } else {
                $where .= ' OR id = ' . $personId;
            }
        }

        $sql = "UPDATE $this->TABLE SET marital_status = '1', update_time = now() WHERE $where AND is_delete = '0' ";


        return $this->pdo->update($sql);
    }

    /**
     * 改变人物类型
     */
    public function changePersonType($personId, $type)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物操作日志
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $old_person = $this->getPersonById($personId);
        $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
        $familyId = $old_person->familyId;
        $option_detail = '编辑人物基本信息';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['type' => $type];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
        //end

        $sql = "UPDATE $this->TABLE set type = $type , update_time = now() WHERE id = '$personId' AND is_delete = 0 ";

        return $this->pdo->update($sql);
    }

    /**
     * 向分支中添加人物
     * @param $branchId 分支id
     * @param $personId 人物id
     * @param $userId   用户id
     */
    public function addPersonToInnerBranch($branchId, $personId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_FAMILY_INNER_BRANCH_PERSON (branchId,personId,createBy,createTime,updateBy,updateTime)VALUES('$branchId', '$personId', '$userId', now(), '$userId', now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 向班级中录入人物
     * @param $person
     * @param $userId
     * @return mixed
     */
    public function addTeacherAndStudent($person, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $shareUrl = md5($person->name . Util::generateRandomCode(24));    //生成唯一share_url的code

        $sql = "INSERT INTO $this->TABLE (id, familyId,name,gender,photo,share_url,type,create_by,create_time,update_by,update_time) VALUES
        ('$person->id', '$person->familyId',
        '$person->name',
        '$person->gender',
        '$person->photo',
        '$shareUrl',
        '$person->type','$userId',now(),'$userId',now())";

        if ($this->pdo->insert($sql) >= 0) {
            $result = $person->id;
        } else {
            $result = -1;
        }

        $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now() WHERE id = '$person->familyId'";
        $this->pdo->update($memberCountSql);

        return $result;
    }

    /**
     * 向公司关系组中录入人物
     * @param $person
     * @param $userId
     * @return mixed
     */
    public function addPersonForCompany($person, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $shareUrl = md5($person->name . Util::generateRandomCode(24));    //生成唯一share_url的code

        $sql = "INSERT INTO $this->TABLE (id, familyId,name,gender,photo,share_url,create_by,create_time,update_by,update_time) VALUES
        ('$person->id','$person->familyId',
        '$person->name',
        '$person->gender',
        '$person->photo',
        '$shareUrl',
        '$userId',now(),'$userId',now())";

        if ($this->pdo->insert($sql) >= 0) {
            $result = $person->id;
        } else {
            $result = -1;
        }


        $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now() WHERE id = '$person->familyId'";
        $this->pdo->update($memberCountSql);

        return $result;
    }

    /**
     * 获取班级人物总数
     * @param $familyId
     * @return int
     */
    public function getCountPersonFromClass($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') FROM $this->TABLE WHERE familyId='$familyId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return -1;
        }
    }

    /**
     * 获取师徒关系人物总数
     * @param $familyId
     * @return int
     */
    public function getCountPersonFromMentor($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') FROM $this->TABLE WHERE familyId='$familyId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return -1;
        }
    }

    /**
     * 获取公司人物总数
     * @param $familyId
     * @return int
     */
    public function getCountPersonFromCompany($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') FROM $this->TABLE WHERE familyId='$familyId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return -1;
        }
    }

    /**
     * 分页获取班级人物
     * @param $familyId
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @return mixed
     */
    public function getClassPersonPaging($familyId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND p.id<'$maxId' AND p.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND p.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND p.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT p.id,p.familyId,f.name as familyName,p.name,p.type,p.photo,p.gender,p.create_by as createBy,p.create_time as createTime,p.update_by as updateBy,p.update_time as updateTime
            FROM $this->TABLE as p  
            LEFT JOIN $this->TABLE_FAMILY as f  ON p.familyId = f.id 
            WHERE p.familyId = '$familyId'  AND p.is_delete=0 " . $where . "  ORDER BY p.id DESC LIMIT $count;";
        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * 分页获取公司关系中的人物
     * @param $familyId
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @return mixed
     */
    public function getCompanyPersonPaging($familyId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND p.id<'$maxId' AND p.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND p.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND p.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT p.id,p.familyId,f.name as familyName,p.name,p.photo,p.gender,p.create_by as createBy,p.create_time as createTime,p.update_by as updateBy,p.update_time as updateTime
            FROM $this->TABLE as p  
            LEFT JOIN $this->TABLE_FAMILY as f  ON p.familyId = f.id 
            WHERE p.familyId = '$familyId'  AND p.is_delete=0 " . $where . "  ORDER BY p.id DESC LIMIT $count;";
        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * 分页师徒关系班级人物
     * @param $familyId
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @return mixed
     */
    public function getMentorPersonPaging($familyId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND p.id<'$maxId' AND p.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND p.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND p.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT p.id,p.familyId,f.name as familyName,p.name,p.type,p.photo,p.gender,p.father,p.mother,p.son,p.daughter,p.brother,p.sister,p.level,p.create_by as createBy,p.create_time as createTime,p.update_by as updateBy,p.update_time as updateTime
            FROM $this->TABLE as p  
            LEFT JOIN $this->TABLE_FAMILY as f  ON p.familyId = f.id 
            WHERE p.familyId = '$familyId'  AND p.is_delete=0 " . $where . "  ORDER BY p.id DESC LIMIT $count;";
        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * 获取班级所有的人物
     * @param $familyId
     * @return mixed
     */
    public function getAllClassPersonList($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT p.id,p.familyId,f.name as familyName,p.name,p.type,p.photo,p.gender,p.create_by as createBy,p.create_time as createTime,p.update_by as updateBy,p.update_time as updateTime
            FROM $this->TABLE as p  
            LEFT JOIN $this->TABLE_FAMILY as f  ON p.familyId = f.id 
            WHERE p.familyId = '$familyId'  AND p.is_delete=0   ORDER BY p.id DESC ";
        return $this->pdo->query($sql);
    }

    /**
     * 获取公司的所有的人物
     * @param $familyId
     * @return mixed
     */
    public function getAllCompanyPersonList($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT p.id,p.familyId,f.name as familyName,p.name,p.photo,p.gender,p.create_by as createBy,p.create_time as createTime,p.update_by as updateBy,p.update_time as updateTime
            FROM $this->TABLE as p  
            LEFT JOIN $this->TABLE_FAMILY as f  ON p.familyId = f.id 
            WHERE p.familyId = '$familyId'  AND p.is_delete=0   ORDER BY p.id DESC ";
        return $this->pdo->query($sql);
    }

    /**
     * 获取师徒关系所有的人物
     * @param $familyId
     * @return mixed
     */
    public function getAllMentorPersonList($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT p.id,p.familyId,f.name as familyName,p.name,p.type,p.photo,p.gender,p.father,p.mother,p.son,p.daughter,p.brother,p.sister,p.level,p.create_by as createBy,p.create_time as createTime,p.update_by as updateBy,p.update_time as updateTime
            FROM $this->TABLE as p  
            LEFT JOIN $this->TABLE_FAMILY as f  ON p.familyId = f.id 
            WHERE p.familyId = '$familyId'  AND p.is_delete=0  ORDER BY p.id DESC ";
        return $this->pdo->query($sql);
    }


    /**
     * 师徒关系中录入人物
     * @param $person
     * @param bool $needTransaction
     * @return int|mixed
     */
    public function addPersonForMentor($person, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $corePerson = $this->getPersonById($person->corePersonId);                            //获取Person对象

        $corePersonLevel = $corePerson->level;
        $level = $corePersonLevel;                                //默认是平辈的
        if ($person->relation == $GLOBALS['RELATION_FATHER']) {
            $level--;            //辈份越高,level越小
        } else if ($person->relation == $GLOBALS['RELATION_SON'] || $person->relation == $GLOBALS['RELATION_DAUGHTER']) {
            $level++;            //辈份越低,level越大
        }

        /*******同时也要更新家族的level标志**************/
        $changed = FALSE;
        $familyDao = new CDBFamily();
        $familyId = $person->familyId;
        $family = $familyDao->getFamilyById($familyId);
        $minLevel = $family->minLevel;
        $maxLevel = $family->maxLevel;
        if ($level < $minLevel) {
            $minLevel = $level;
            $changed = TRUE;
        }
        if ($level > $maxLevel) {
            $maxLevel = $level;
            $changed = TRUE;
        }

        try {

            //开启事务处理
            if ($needTransaction)
                $this->pdo->beginTransaction();


            if ($changed) {
                $familyDao->updateFamilyLevel($person->familyId, $minLevel, $maxLevel);
            }

            $personId = $this->addPersonWithLevelForMentor($person, $level); //增加要增加的人物
            $records = array();                                                                //需要更新的人物记录数组
            $count = 0;

            switch ($person->relation) {

                case $GLOBALS['RELATION_FATHER']:
                    //添加自己和父亲关系的记录
                    $meFatherRecord = new PersonRecord($person->corePersonId, "father", $personId);
                    $records[$count] = $meFatherRecord;                        //自己和父亲的记录
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    }

                    //更新父亲的儿子和兄弟的父亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $fatherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $fatherSonRecord;
                        $count++;

                        $brotherFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $brotherFatherRecord;
                        $count++;
                    }

                    //更新父亲的女儿和姐妹的父亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $fatherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;

                        $sisterFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $sisterFatherRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_MOTHER']:
                    //添加自己和母亲关系的记录
                    $meMotherRecord = new PersonRecord($person->corePersonId, "mother", $personId);
                    $records[$count] = $meMotherRecord;
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $motherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $motherMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $motherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $motherMeRecord;
                        $count++;
                    }


                    //更新母亲的儿子和兄弟的母亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $motherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $motherSonRecord;
                        $count++;

                        $brotherMotherRecord = new PersonRecord($value, "mother", $personId);
                        $records[$count] = $brotherMotherRecord;
                        $count++;
                    }

                    //更新母亲的女儿和姐妹的母亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $motherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $motherDaughterRecord;
                        $count++;

                        $sisterMotherRecord = new PersonRecord($value, "mother", $personId);
                        $records[$count] = $sisterMotherRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_BROTHER']:
                    //添加自己的兄弟关系的记录
                    $meBrotherRecord = new PersonRecord($person->corePersonId, "brother", $personId);
                    $records[$count] = $meBrotherRecord;
                    $count++;

                    /****根据我的性别判定是兄弟还是姐妹****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $brotherMeRecord = new PersonRecord($personId, "brother", $person->corePersonId);
                        $records[$count] = $brotherMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $brotherMeRecord = new PersonRecord($personId, "sister", $person->corePersonId);
                        $records[$count] = $brotherMeRecord;
                        $count++;
                    }


                    //更新兄弟的父亲和父亲的儿子
                    foreach ($corePerson->father as $key => $value) {
                        $brotherFatherRecord = new PersonRecord($personId, "father", $value);
                        $records[$count] = $brotherFatherRecord;
                        $count++;

                        $fatherSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $fatherSonRecord;
                        $count++;
                    }

                    //更新兄弟的母亲和母亲的儿子
                    foreach ($corePerson->mother as $key => $value) {
                        $brotherMotherRecord = new PersonRecord($personId, "mother", $value);
                        $records[$count] = $brotherMotherRecord;
                        $count++;

                        $motherSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $motherSonRecord;
                        $count++;
                    }

                    //更新兄弟的兄弟和兄弟的兄弟
                    foreach ($corePerson->brother as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $brotherBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $brotherBrotherRecord;
                        $count++;

                        $brotherBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $brotherBrotherRecord;
                        $count++;
                    }

                    //更新兄弟的姐妹和姐妹的兄弟
                    foreach ($corePerson->sister as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $brotherSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $brotherSisterRecord;
                        $count++;

                        $sisterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sisterBrotherRecord;
                        $count++;
                    }

                    break;
                case $GLOBALS['RELATION_SISTER']:
                    //添加自己的姐妹关系的记录
                    $meSisterRecord = new PersonRecord($person->corePersonId, "sister", $personId);
                    $records[$count] = $meSisterRecord;
                    $count++;

                    /****根据我的性别判定是兄弟还是姐妹****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sisterMeRecord = new PersonRecord($personId, "brother", $person->corePersonId);
                        $records[$count] = $sisterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sisterMeRecord = new PersonRecord($personId, "sister", $person->corePersonId);
                        $records[$count] = $sisterMeRecord;
                        $count++;
                    }

                    //更新姐妹的父亲和父亲的女儿
                    foreach ($corePerson->father as $key => $value) {
                        $sisterFatherRecord = new PersonRecord($personId, "father", $value);
                        $records[$count] = $sisterFatherRecord;
                        $count++;

                        $fatherDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;
                    }

                    //更新姐妹的母亲和母亲的女儿
                    foreach ($corePerson->mother as $key => $value) {
                        $sisterMotherRecord = new PersonRecord($personId, "mother", $value);
                        $records[$count] = $sisterMotherRecord;
                        $count++;

                        $motherDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $motherDaughterRecord;
                        $count++;
                    }

                    //更新姐妹的兄弟和兄弟的姐妹
                    foreach ($corePerson->brother as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $sisterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sisterBrotherRecord;
                        $count++;

                        $brotherSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $brotherSisterRecord;
                        $count++;
                    }

                    //更新姐妹的姐妹和姐妹的姐妹
                    foreach ($corePerson->sister as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $sisterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sisterSisterRecord;
                        $count++;

                        $sisterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sisterSisterRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_SON']:
                    //添加自己和儿子关系的记录
                    $meSonRecord = new PersonRecord($person->corePersonId, "son", $personId);
                    $records[$count] = $meSonRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sonMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sonMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    }


                    //更新儿子的父或母亲和配偶的儿子
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd
                    }

                    //更新儿子的兄弟和儿子的兄弟
                    foreach ($corePerson->son as $key => $value) {
                        $sonBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sonBrotherRecord;
                        $count++;

                        $sonBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sonBrotherRecord;
                        $count++;
                    }

                    //更新儿子的姐妹和女儿的兄弟
                    foreach ($corePerson->daughter as $key => $value) {
                        $sonSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sonSisterRecord;
                        $count++;

                        $daughterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_DAUGHTER']:
                    //添加自己和女儿关系的记录
                    $meDaughterRecord = new PersonRecord($person->corePersonId, "daughter", $personId);
                    $records[$count] = $meDaughterRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    }
                    //更新女儿的兄弟和儿子的姐妹
                    foreach ($corePerson->son as $key => $value) {
                        $daughterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;

                        $sonSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sonSisterRecord;
                        $count++;
                    }

                    //更新女儿的姐妹和女儿的姐妹
                    foreach ($corePerson->daughter as $key => $value) {
                        $daughterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $daughterSisterRecord;
                        $count++;

                        $daughterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $daughterSisterRecord;
                        $count++;
                    }
                    break;
            }
            if (!$this->updateRecord($records)) {
                //@codeCoverageIgnoreStart
                if ($needTransaction)
                    $this->pdo->rollback();
                return -1;                //表示添加相关记录时出错
                //@codeCoverageIgnoreEnd
            } else {
                if ($needTransaction)
                    $this->pdo->commit();
                return $personId;
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            if ($needTransaction)
                $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
        }
        return -1;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 向家族中添加相关联的人物
     * @param $familyId    家族id
     * @param $name        姓名
     * @param $gender        性别
     * @param $address        地址
     * @param $birthday        生日
     * @param $photo        相片
     * @param $relation        关系
     * @param $corePersonId 关系人物id
     * @param $needTransaction  是否需要事务,true代表需要(默认)，false代表不需要
     * @return int 添加的人物id或者-1代表记录插入出错，-2代表插入的人物不属于该家族
     */
    public function addRelatePerson($person, $needTransaction = true, $isCopyed = false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $corePerson = $this->getPersonById($person->corePersonId);                            //获取Person对象

        $userId = $GLOBALS['userId'];

        $corePersonLevel = $corePerson->level;
        $level = $corePersonLevel;                                //默认是平辈的
        if ($person->relation == $GLOBALS['RELATION_FATHER'] || $person->relation == $GLOBALS['RELATION_MOTHER']) {
            $level--;            //辈份越高,level越小
        } else if ($person->relation == $GLOBALS['RELATION_SON'] || $person->relation == $GLOBALS['RELATION_DAUGHTER']) {
            $level++;            //辈份越低,level越大
        }

        //非核心人物只能加子女 (这里的核心人物指的是type=1，即是本家，type为2是配偶)
        if (($person->relation != $GLOBALS['RELATION_SON'] && $person->relation != $GLOBALS['RELATION_DAUGHTER']) && $corePerson->type == 2) {
            //添加的人物不属于这个家族，返回错误
            return -2;
        }

        $familyDB = new CDBFamily();
        $family = $familyDB->getFamilyById($corePerson->familyId);
        //$memberCount = $family->memberCount;    // 族群中的人物
        //如果是复制的操作，则不需要改变当前人物type 也可以写在下面的判断，这里就是减少一步数据库查询（查询出来的membercount不是实时数据）
        $memberCount = $isCopyed ? 2 : $family->memberCount;
        $needChangeCorePersonType = false;      // 是否需要改变核心人物的type

        $personType = 1;
        //判断得到当前添加人物的type
        if ($person->relation == $GLOBALS['RELATION_SPOUSE']) {
            if ($memberCount == 1) {
                // 添加的是妻系的，将原来的人物的type变为2
                //@codeCoverageIgnoreStart
                $needChangeCorePersonType = true;
                $personType = 1;
                //@codeCoverageIgnoreEnd
            } else {
                if ($corePerson->type == 1) {
                    $personType = 2;
                } else {
                    $personType = 1;
                }
            }
        } else if ($person->relation == $GLOBALS['RELATION_MOTHER']) {

            //如果是1，母系
            if ($memberCount == 1) {
                //@codeCoverageIgnoreStart
                $personType = 1;
                //@codeCoverageIgnoreEnd
            } else if ($memberCount == 2) {
                //判断是否是妻子的母系
                //@codeCoverageIgnoreStart
                $userPerson = $this->getPersonByFamilyIdAndUserId($corePerson->familyId, $userId);
                if (count($userPerson->spouse) == 1 && $userPerson->spouse[0] == $person->corePersonId) {
                    // corePerson是配偶
                    $personType = 1;
                } else {
                    $personType = 2;
                }
                //@codeCoverageIgnoreEnd
            } else {
                $personType = 2;
            }
        } else if ($person->relation == $GLOBALS['RELATION_FATHER']) {
            //添加父亲，需要判断母亲type是否为1
            if (count($corePerson->mother) > 0) {
                $mother = $this->getPersonById($corePerson->mother[0]);
                if ($mother != null) {
                    if ($mother->type == 1) {
                        //@codeCoverageIgnoreStart
                        $personType = 2;
                        //@codeCoverageIgnoreEnd
                    }
                }
            }
        } else {
            $personType = 1;
        }

        /*******同时也要更新家族的level标志**************/
        $changed = FALSE;
        $minLevel = $family->minLevel;
        $maxLevel = $family->maxLevel;
        if ($level < $minLevel) {
            $minLevel = $level;
            $changed = TRUE;
        }
        if ($level > $maxLevel) {
            $maxLevel = $level;
            $changed = TRUE;
        }

        try {

            //开启事务处理
            if ($needTransaction)
                $this->pdo->beginTransaction();


            if ($needChangeCorePersonType) {
                // 改变中心人物的type
                //@codeCoverageIgnoreStart
                $this->changePersonType($person->corePersonId, 2);
                //@codeCoverageIgnoreEnd
            }

            if ($changed)
                $familyDB->updateFamilyLevel($person->familyId, $minLevel, $maxLevel);

            /*********家族中的level标志更新完毕*********/


            if ($isCopyed == true) {
                $personId = $this->addPersonForCopy($person, $level, $personType); //复制人物独有的方法来增加人物
            } else {
                $personId = $this->addPersonWithLevel($person, $level, $personType); //增加要增加的人物
            }

            if ($personId <= 0) {
                return -1;
            }
            $records = array();                                                                //需要更新的人物记录数组
            $count = 0;

            switch ($person->relation) {

                case $GLOBALS['RELATION_FATHER']:
                    //添加自己和父亲关系的记录
                    $meFatherRecord = new PersonRecord($person->corePersonId, "father", $personId);
                    $records[$count] = $meFatherRecord;                        //自己和父亲的记录
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    }

                    //更新父亲的儿子和兄弟的父亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $fatherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $fatherSonRecord;
                        $count++;

                        $brotherFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $brotherFatherRecord;
                        $count++;
                    }

                    //更新父亲的女儿和姐妹的父亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $fatherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;

                        $sisterFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $sisterFatherRecord;
                        $count++;
                    }

                    $updateSpouseId = array();

                    //更新父亲的配偶和母亲的配偶
                    foreach ($corePerson->mother as $key => $value) {
                        $fatherSpouseRecord = new PersonRecord($personId, "spouse", $value);
                        $records[$count] = $fatherSpouseRecord;
                        $count++;

                        $motherSpouseRecord = new PersonRecord($value, "spouse", $personId);
                        $records[$count] = $motherSpouseRecord;
                        $count++;

                        //更新配偶的婚姻状态
                        array_push($updateSpouseId, $value);
                    }

                    array_push($updateSpouseId, $personId);
                    $this->updateSpouseStatus($updateSpouseId);
                    break;
                case $GLOBALS['RELATION_MOTHER']:
                    //添加自己和母亲关系的记录
                    $meMotherRecord = new PersonRecord($person->corePersonId, "mother", $personId);
                    $records[$count] = $meMotherRecord;
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $motherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $motherMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $motherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $motherMeRecord;
                        $count++;
                    }


                    //更新母亲的儿子和兄弟的母亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $motherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $motherSonRecord;
                        $count++;

                        $brotherMotherRecord = new PersonRecord($value, "mother", $personId);
                        $records[$count] = $brotherMotherRecord;
                        $count++;
                    }

                    //更新母亲的女儿和姐妹的母亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $motherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $motherDaughterRecord;
                        $count++;

                        $sisterMotherRecord = new PersonRecord($value, "mother", $personId);
                        $records[$count] = $sisterMotherRecord;
                        $count++;
                    }

                    $updateSpouseId = array();

                    //更新母亲的配偶和父亲的配偶
                    foreach ($corePerson->father as $key => $value) {
                        $motherSpouseRecord = new PersonRecord($personId, "spouse", $value);
                        $records[$count] = $motherSpouseRecord;
                        $count++;

                        $fatherSpouseRecord = new PersonRecord($value, "spouse", $personId);
                        $records[$count] = $fatherSpouseRecord;
                        $count++;
                        //更新配偶的婚姻状态
                        array_push($updateSpouseId, $value);
                    }
                    array_push($updateSpouseId, $personId);
                    //更新婚姻状态
                    $this->updateSpouseStatus($updateSpouseId);

                    break;
                case $GLOBALS['RELATION_BROTHER']:
                    //添加自己的兄弟关系的记录
                    $meBrotherRecord = new PersonRecord($person->corePersonId, "brother", $personId);
                    $records[$count] = $meBrotherRecord;
                    $count++;

                    /****根据我的性别判定是兄弟还是姐妹****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $brotherMeRecord = new PersonRecord($personId, "brother", $person->corePersonId);
                        $records[$count] = $brotherMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $brotherMeRecord = new PersonRecord($personId, "sister", $person->corePersonId);
                        $records[$count] = $brotherMeRecord;
                        $count++;
                    }


                    //更新兄弟的父亲和父亲的儿子
                    foreach ($corePerson->father as $key => $value) {
                        $brotherFatherRecord = new PersonRecord($personId, "father", $value);
                        $records[$count] = $brotherFatherRecord;
                        $count++;

                        $fatherSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $fatherSonRecord;
                        $count++;
                    }

                    //更新兄弟的母亲和母亲的儿子
                    foreach ($corePerson->mother as $key => $value) {
                        $brotherMotherRecord = new PersonRecord($personId, "mother", $value);
                        $records[$count] = $brotherMotherRecord;
                        $count++;

                        $motherSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $motherSonRecord;
                        $count++;
                    }

                    //更新兄弟的兄弟和兄弟的兄弟
                    foreach ($corePerson->brother as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $brotherBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $brotherBrotherRecord;
                        $count++;

                        $brotherBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $brotherBrotherRecord;
                        $count++;
                    }

                    //更新兄弟的姐妹和姐妹的兄弟
                    foreach ($corePerson->sister as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $brotherSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $brotherSisterRecord;
                        $count++;

                        $sisterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sisterBrotherRecord;
                        $count++;
                    }

                    break;
                case $GLOBALS['RELATION_SISTER']:
                    //添加自己的姐妹关系的记录
                    $meSisterRecord = new PersonRecord($person->corePersonId, "sister", $personId);
                    $records[$count] = $meSisterRecord;
                    $count++;

                    /****根据我的性别判定是兄弟还是姐妹****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sisterMeRecord = new PersonRecord($personId, "brother", $person->corePersonId);
                        $records[$count] = $sisterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sisterMeRecord = new PersonRecord($personId, "sister", $person->corePersonId);
                        $records[$count] = $sisterMeRecord;
                        $count++;
                    }

                    //更新姐妹的父亲和父亲的女儿
                    foreach ($corePerson->father as $key => $value) {
                        $sisterFatherRecord = new PersonRecord($personId, "father", $value);
                        $records[$count] = $sisterFatherRecord;
                        $count++;

                        $fatherDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;
                    }

                    //更新姐妹的母亲和母亲的女儿
                    foreach ($corePerson->mother as $key => $value) {
                        $sisterMotherRecord = new PersonRecord($personId, "mother", $value);
                        $records[$count] = $sisterMotherRecord;
                        $count++;

                        $motherDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $motherDaughterRecord;
                        $count++;
                    }

                    //更新姐妹的兄弟和兄弟的姐妹
                    foreach ($corePerson->brother as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $sisterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sisterBrotherRecord;
                        $count++;

                        $brotherSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $brotherSisterRecord;
                        $count++;
                    }

                    //更新姐妹的姐妹和姐妹的姐妹
                    foreach ($corePerson->sister as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $sisterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sisterSisterRecord;
                        $count++;

                        $sisterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sisterSisterRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_SPOUSE']:
                    //添加自己和配偶关系的记录
                    $meSpouseRecord = new PersonRecord($person->corePersonId, "spouse", $personId);
                    $records[$count] = $meSpouseRecord;
                    $count++;

                    $spouseMeRecord = new PersonRecord($personId, "spouse", $person->corePersonId);
                    $records[$count] = $spouseMeRecord;
                    $count++;

                    //更新配偶的儿子和儿子的父母
                    foreach ($corePerson->son as $key => $value) {
                        $spouseSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $spouseSonRecord;
                        $count++;

                        /*****根据配偶的性别判断******/
                        if ($person->gender == $GLOBALS['GENDER_MALE']) {
                            $sonFatherRecord = new PersonRecord($value, "father", $personId);
                            $records[$count] = $sonFatherRecord;
                            $count++;
                        } else if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonMotherRecord = new PersonRecord($value, "mother", $personId);
                            $records[$count] = $sonMotherRecord;
                            $count++;
                        }
                    }

                    //更新配偶的女儿和女儿的父母
                    foreach ($corePerson->daughter as $key => $value) {
                        $spouseDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $spouseDaughterRecord;
                        $count++;

                        /*****根据配偶的性别判断******/
                        if ($person->gender == $GLOBALS['GENDER_MALE']) {
                            //@codeCoverageIgnoreStart
                            $daughterFatherRecord = new PersonRecord($value, "father", $personId);
                            $records[$count] = $daughterFatherRecord;
                            $count++;
                            //@codeCoverageIgnoreEnd
                        } else if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                            $daughterMotherRecord = new PersonRecord($value, "mother", $personId);
                            $records[$count] = $daughterMotherRecord;
                            $count++;
                        }
                    }

                    //更新婚姻状态
                    $this->updateSpouseStatus(array($personId, $person->corePersonId));

                    break;
                case $GLOBALS['RELATION_SON']:
                    //添加自己和儿子关系的记录
                    $meSonRecord = new PersonRecord($person->corePersonId, "son", $personId);
                    $records[$count] = $meSonRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sonMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sonMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    }


                    //更新儿子的父或母亲和配偶的儿子
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd

                        $spouseSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $spouseSonRecord;
                        $count++;
                    }

                    //找出我的配偶的配偶  父亲->母亲1（corePerson)->母亲2
                    //TODO 不确定是否正确
                    if (count($corePerson->spouse) == 1) {
                        $mySpouse = $this->getPersonById($corePerson->spouse[0]);
                        foreach ($mySpouse->spouse as $key => $value) {
                            if ($value != $corePerson->id) {  //排除自己
                                //@codeCoverageIgnoreStart
                                /*****根据我的性别判断******/
                                if ($mySpouse->gender == $GLOBALS['GENDER_MALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                }

                                $spouseSonRecord = new PersonRecord($value, "son", $personId);
                                $records[$count] = $spouseSonRecord;
                                $count++;
                                //@codeCoverageIgnoreEnd
                            }
                        }
                    }


                    //更新儿子的兄弟和儿子的兄弟
                    foreach ($corePerson->son as $key => $value) {
                        $sonBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sonBrotherRecord;
                        $count++;

                        $sonBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sonBrotherRecord;
                        $count++;
                    }

                    //更新儿子的姐妹和女儿的兄弟
                    foreach ($corePerson->daughter as $key => $value) {
                        $sonSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sonSisterRecord;
                        $count++;

                        $daughterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_DAUGHTER']:
                    //添加自己和女儿关系的记录
                    $meDaughterRecord = new PersonRecord($person->corePersonId, "daughter", $personId);
                    $records[$count] = $meDaughterRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    }


                    //更新女儿的父或母亲和配偶的女儿
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $daughterSpouseRecord = new PersonRecord($personId, "mother", $value);
                            $records[$count] = $daughterSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $daughterSpouseRecord = new PersonRecord($personId, "father", $value);
                            $records[$count] = $daughterSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd


                        $spouseDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $spouseDaughterRecord;
                        $count++;
                    }

                    //找出我的配偶的配偶  父亲->母亲1（corePerson)->母亲2
                    //TODO 不确定是否正确
                    //@codeCoverageIgnoreStart
                    if (count($corePerson->spouse) == 1) {
                        $mySpouse = $this->getPersonById($corePerson->spouse[0]);
                        foreach ($mySpouse->spouse as $key => $value) {
                            if ($value != $corePerson->id) {  //排除自己
                                /*****根据我的性别判断******/
                                if ($mySpouse->gender == $GLOBALS['GENDER_MALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                }

                                $spouseSonRecord = new PersonRecord($value, "daughter", $personId);
                                $records[$count] = $spouseSonRecord;
                                $count++;
                                //@codeCoverageIgnoreEnd
                            }
                        }
                    }

                    //更新女儿的兄弟和儿子的姐妹
                    foreach ($corePerson->son as $key => $value) {
                        $daughterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;

                        $sonSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sonSisterRecord;
                        $count++;
                    }

                    //更新女儿的姐妹和女儿的姐妹
                    foreach ($corePerson->daughter as $key => $value) {
                        $daughterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $daughterSisterRecord;
                        $count++;

                        $daughterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $daughterSisterRecord;
                        $count++;
                    }
                    break;
            }
            if (!$this->updateRecord($records)) {
                //@codeCoverageIgnoreStart
                if ($needTransaction)
                    $this->pdo->rollback();
                return -1;                //表示添加相关记录时出错
                //@codeCoverageIgnoreEnd
            } else {

                // 获取添加的人物的父母，然后查询父母所在的分支，然后更新分支记录
                $createPerson = $this->getPersonById($personId);

                $parents = array_merge($createPerson->father, $createPerson->mother);

                if (count($parents) > 0) {
                    // 判断父母的所在分支
                    $parentId = $parents[0];
                    $branchIds = $this->getPersonBranchList($parentId);
                    foreach ($branchIds as $branchId) {
                        $this->addPersonToInnerBranch($branchId['branchId'], $personId, $userId);
                    }
                }

                if ($needTransaction)
                    $this->pdo->commit();
                return $personId;
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            if ($needTransaction)
                $this->pdo->rollback();
        }
        return -1;
        //@codeCoverageIgnoreEnd
    }


    /**
     * 根据personId删除某个人物
     * 删除的时候要找出和他有关系的人物，删除其中的关系
     * @param $personId 要删除的人物id
     * @param $needTransaction 是否需要事务
     * @return bool 删除成功 true 删除失败false
     */
    public function deletePerson($personId, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        //先检索出这个人物对象
        $person = $this->getPersonById($personId);

        try {

            if ($needTransaction) {
                //开启事务处理
                $this->pdo->beginTransaction();
            }

            //记录人物操作日志
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $this->getPersonById($personId);
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $familyId = $old_person->familyId;
            $option_detail = '删除人物';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['isDelete' => 1];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
            //end

            //更新父母的记录
            $fartherAndMother = array_merge($person->father, $person->mother);
            $setParam = "";
            if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                $setParam = "daughter";
            } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                $setParam = "son";
            }
            $from = '@' . $personId . "|";
            $to = "";
            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to')";
            $where = "";
            foreach ($fartherAndMother as $key => $value) {
                if ($where != "") {
                    $where = $where . " or id = $value";
                } else {
                    $where = $where . "id = $value";
                }
            }

            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);

            //更新兄弟姐妹的记录
            $brotherAndSister = array_merge($person->brother, $person->sister);
            $setParam = "";
            if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                $setParam = "sister";
            } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                $setParam = "brother";
            }
            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to')";
            $where = "";
            foreach ($brotherAndSister as $key => $value) {
                if ($where != "") {
                    $where = $where . " or id = $value";
                } else {
                    $where = $where . "id = $value";
                }
            }

            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);

            //更新配偶的记录
            $spouses = $person->spouse;
            $setParam = "spouse";

            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to'),update_time = now()";
            $where = "";
            foreach ($spouses as $key => $value) {
                if ($where != "") {
                    //@codeCoverageIgnoreStart
                    $where = $where . " or id = $value";
                    //@codeCoverageIgnoreEnd
                } else {
                    $where = $where . "id = $value";
                }
            }
            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);

            //更新儿女的记录
            $sonAndDaughter = array_merge($person->son, $person->daughter);
            $setParam = "";
            if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                $setParam = "mother";
            } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                $setParam = "father";
            }
            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to') , update_time = now()";
            $where = "";
            foreach ($sonAndDaughter as $key => $value) {
                if ($where != "") {
                    $where = $where . " or id = $value";
                } else {
                    $where = $where . "id = $value";
                }
            }
            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);


            //$sql = "DELETE FROM $this->TABLE WHERE id = $personId";		//最后删除自己,不是真删，而是将is_delete置为1
            $sql = "UPDATE $this->TABLE SET is_delete = '1',update_time = now() WHERE id = $personId ";
            //更新家族的代数 TODO
            $count = $this->pdo->update($sql);

            //删除人物时检查人物是否绑定用户，如果绑定则更改用户的家族数
            $userId = $person->userId;
            if ($userId > 0) {
                //这里先删除管理员表（如果是管理员的话）
                /*$deleteSql = "UPDATE $this->TABLE_FAMILYADMIN SET isDelete=1 WHERE familyId = '$person->familyId' AND adminId = '$userId' AND isDelete=0 ";
                $delete_res = $this->pdo->update($deleteSql);
                $sql = "SELECT family_num FROM $this->TABLE_USER_PROFILE WHERE userId='$userId'";
                $result = $this->pdo->uniqueResult($sql);
                if ($result != null && $result['family_num'] != '0') {
                    $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num - 1,update_time = now() WHERE userId = '$userId'";
                    $this->pdo->update($sql);
                }*/
                //取消用户绑定
                $this->cancelUserBindForTmp($personId, $person->familyId, $userId, false);
            }

            // 删除人物的分支记录
            $branchSql = "UPDATE $this->TABLE_FAMILY_INNER_BRANCH_PERSON SET isDelete = 1 WHERE personId = '$personId' ";
            $this->pdo->update($branchSql);

            //更新家族成员数
            $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count - 1,update_time = now() WHERE id = '$person->familyId'";
            $this->pdo->update($memberCountSql);


            if ($needTransaction) {
                $this->pdo->commit();
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            if ($needTransaction) {
                $this->pdo->rollback();
            }
            //@codeCoverageIgnoreEnd
        }
        if ($count > 0)
            return true;
        //@codeCoverageIgnoreStart
        return false;
        //@codeCoverageIgnoreEnd

    }


    /**
     * 删除班级or公司人物
     * @param $personId
     * @return bool
     */
    public function deleteClassOrCompanyPerson($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        //先检索出这个人物对象
        $person = $this->getPersonById($personId);

        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            $sql = "UPDATE $this->TABLE SET is_delete = '1',update_time = now() WHERE id = $personId ";
            //更新家族的代数 TODO
            $count = $this->pdo->update($sql);

            //删除人物时检查人物是否绑定用户，如果绑定则更改用户的家族数
            $userId = $person->userId;
            if ($userId > 0) {
                //这里先删除管理员表（如果是管理员的话）
                /*$deleteSql = "UPDATE $this->TABLE_FAMILYADMIN SET isDelete=1 WHERE familyId = '$person->familyId' AND adminId = '$userId' AND isDelete=0 ";
                $delete_res = $this->pdo->update($deleteSql);
                $sql = "SELECT family_num FROM $this->TABLE_USER_PROFILE WHERE userId='$userId'";
                $result = $this->pdo->uniqueResult($sql);
                if ($result != null && $result['family_num'] != '0') {
                    $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num - 1,update_time = now() WHERE userId = '$userId'";
                    $this->pdo->update($sql);
                }*/
                //取消用户绑定
                $this->cancelUserBindForTmp($personId, $person->familyId, $userId, false);
            }

            //更新家族成员数
            $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count - 1,update_time = now() WHERE id = '$person->familyId'";
            $this->pdo->update($memberCountSql);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            //@codeCoverageIgnoreEnd
        }
        if ($count > 0)
            return true;
        //@codeCoverageIgnoreStart
        return false;
        //@codeCoverageIgnoreEnd

    }


    /**
     * 获取家族的所有人员,供前端家族树使用
     * @param $familyId
     * @return array 所有人员的数组
     */
    public function getFamilyPersons($familyId, int $levelLimit = null, $startId = 0, $needPermission = false, $needOtherInfo=false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $levelLimitSql = "";
        if (!is_null($levelLimit)) {
            //如果levelLimit不为空，则查询要通过level过滤
            $levelLimitSql .= " AND tb.level <= $levelLimit ";
        }

        $needPermissionSql = "";
        $permissionSql = "";
        if ($needPermission) {
            $needPermissionSql = " left join (select userId,permissionType as permission from $this->TABLE_NEW_PERMISSION 
                where is_delete = 0 and familyId = $familyId) tp on tb.userId = tp.userId  ";
            $permissionSql = " tp.permission, ";
        }
        $otherInfoSql = '';
        $otherInfoJoinSql = '';
        if ($needOtherInfo){
            $otherInfoSql = " , tf.branch_name as branchName, tot.trash_field as trashField ";
            $otherInfoJoinSql = "left join $this->TABLE_PERSON_PDF_TITLE tf on tb.id = tf.person_id 
                                  left join $this->TABLE_PERSON_OTHER_INFO tot on tb.id = tot.person_id ";
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.zpname,tb.qq,tb.remark,tb.type,tb.country,tb.country_name as countryName,tb.province,tb.province_name as provinceName,tb.city,tb.city_name as cityName,tb.area,tb.area_name as areaName,tb.town,tb.town_name as townName,tb.address,tb.birthday,tb.gender,tb.phone,tb.photo,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamilyId,tb.ref_personId as refPersonId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.sideText,tb.profileText,tb.userId,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.username, $permissionSql
                    tu.photo as userPhoto,
                    tu.nickname,
                    tu.hx_username as hxUsername,
                    tb.ranking, tb.family_index as familyIndex
                    $otherInfoSql
                    FROM $this->TABLE tb 
                    left join $this->TABLE_USER tu on tb.userId = tu.id 
                    $otherInfoJoinSql 
                    $needPermissionSql
                    WHERE tb.familyId = '$familyId' AND tb.is_delete = '0' $levelLimitSql order by tb.level asc , 
                    tb.type asc";

        $this->pdo->setBuffered(false); // 不允许缓存

        $result = $this->pdo->query($sql);
        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        if ($startId > 0) {

            $personmap = [];
            //拼
            foreach ($result as $v) {
                $personmap[$v['id']] = $v;
            }
            if (isset($personmap[$startId])) {
                $personmap = $this->cutTreesForFamilyPerson($personmap, $startId);
            }
            $result = [];
            //还原
            foreach ($personmap as $person) {
                $result[] = $person;
            }
        }
        return $result;
    }

    private function cutTreesForFamilyPerson($person, $personId)
    {

        //过滤兄弟姐妹的儿女
        $tmpBrother = array_merge($person[$personId]['brother'], $person[$personId]['sister']);
        foreach ($tmpBrother as $brotherId) {
            if ($brotherId == $personId) {
                continue;
            }
            $person[$brotherId]['son'] = [];
            $person[$brotherId]['daughter'] = [];
        }

        $parents = array_merge($person[$personId]['father'], $person[$personId]['mother']);
        //对父母的其他儿女进行过滤
        foreach ($parents as $parentId) {
            if ($person[$parentId]['type'] == 1) {
                //父母不过滤 不过需要
                $person = $this->cutTreesForFamilyPerson($person, $parentId);
            }
        }
        return $person;
    }

    /**
     * 获取家族的所有人员,供前端家族树使用
     * @param $familyId
     * @return array 所有人员的数组
     */
    public function getFamilyPersonsByTimestamp($familyId, $timestamp = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.zpname,tb.qq,tb.remark,tb.type,tb.country,tb.country_name as countryName,tb.province,tb.province_name as provinceName,tb.city,tb.city_name as cityName,tb.area,tb.area_name as areaName,tb.town,tb.town_name as townName,tb.address,tb.birthday,tb.gender,tb.phone,tb.photo,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
        tb.ref_familyId as refFamilyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
        tb.level,tb.sideText,tb.profileText,tb.userId,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
        tu.username,
        tu.photo as userPhoto,
        tu.nickname,
        tu.hx_username as hxUsername,
        tb.ranking 
        FROM $this->TABLE tb 
        left join $this->TABLE_USER tu on tb.userId = tu.id 
        WHERE tb.familyId = '$familyId' ";

        $this->pdo->setBuffered(false); // 不允许缓存

        if ($timestamp != 0) {
            //更新的人物信息
            $updatewhere = " AND tb.is_delete = '0' AND tb.update_time >='$timestamp' order by tb.level";
            $updatesql = $sql . $updatewhere;
            $result = $this->pdo->query($updatesql);
            for ($i = 0; $i < count($result); $i++) {
                $person = new Person($result[$i]);        //转换为person对象
                $parser = new RelationParser($person);
                $result[$i]['father'] = $parser->getFathers();
                $result[$i]['mother'] = $parser->getMothers();
                $result[$i]['son'] = $parser->getSons();
                $result[$i]['daughter'] = $parser->getDaughters();
                $result[$i]['spouse'] = $parser->getSpouses();
                $result[$i]['brother'] = $parser->getBrothers();
                $result[$i]['sister'] = $parser->getSisters();
            }
            $res['updatePersons'] = $result;

            //删除的人物列表
            $deletesql = "SELECT id from  $this->TABLE where familyId = '$familyId' AND is_delete = '1' AND update_time>='$timestamp' ";
            $deleted = $this->pdo->query($deletesql);
            $res['deletePersons'] = [];
            foreach ($deleted as $v) {
                $res['deletePersons'][] = $v['id'];
            }
            return $res;
        } else {
            //获取所有人物信息
            $allsql = $sql . " AND tb.is_delete = '0' order by tb.level";

            $result = $this->pdo->query($allsql);
            for ($i = 0; $i < count($result); $i++) {
                $person = new Person($result[$i]);        //转换为person对象
                $parser = new RelationParser($person);
                $result[$i]['father'] = $parser->getFathers();
                $result[$i]['mother'] = $parser->getMothers();
                $result[$i]['son'] = $parser->getSons();
                $result[$i]['daughter'] = $parser->getDaughters();
                $result[$i]['spouse'] = $parser->getSpouses();
                $result[$i]['brother'] = $parser->getBrothers();
                $result[$i]['sister'] = $parser->getSisters();
            }

            return $result;
        }
    }

    /**
     * 获取家族内建分支的所有人物
     * @param $familyId
     * @return array 所有人员的数组
     */
    public function getInnerBranchPersons($branchId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.type,tb.country_name as countryName,tb.province_name as provinceName,tb.city_name as cityName,tb.area_name as areaName,tb.town_name as townName,tb.address,tb.birthday,tb.gender,tb.phone,tb.photo,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamilyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.sideText,tb.profileText,tb.userId,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.username,
                    tu.photo as userPhoto,
                    tu.nickname,
                    tu.hx_username as hxUsername,
                    tb.ranking 
                    FROM $this->TABLE_FAMILY_INNER_BRANCH_PERSON tbp 
                    left join $this->TABLE tb on tb.id = tbp.personId 
                    left join $this->TABLE_USER tu on tb.userId = tu.id 
                    WHERE tbp.branchId = '$branchId' AND tb.is_delete = '0' order by tb.level";

        $this->pdo->setBuffered(false); // 不允许缓存

        $result = $this->pdo->query($sql);
        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        return $result;
    }

    /**
     * 通过合并的用户获取家族的所有人员,供前端家族树使用
     * @param $familyId
     * @return array 所有人员的数组
     */
    public function getFamilyPersonsByMerge($familyId, int $levelLimit = null)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $levelLimitSql = "";
        if (!is_null($levelLimit)) {
            //如果levelLimit不为空，则查询要通过level过滤
            $levelLimitSql .= " AND tb.level <= $levelLimit ";
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.type,tb.gender,tb.photo,tb.familyId,tb.ref_familyId as refFamilyId,
                       tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.userId,tb.ranking 
                    FROM $this->TABLE tb 
                    WHERE tb.familyId = '$familyId' AND tb.is_delete = '0' $levelLimitSql order by tb.level";

        $result = $this->pdo->query($sql);
        for ($i = 0; $i < count($result); $i++) {
            $person = new PersonByMerge($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        return $result;
    }

    /**
     * 根据分支id获取家族人物
     * @param $branchId 分支id
     * @return array 分支人物数组
     */
    public function getFamilyPersonsByBranchId(int $branchId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.address,tb.type,tb.birthday,tb.gender,tb.phone,tb.photo,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamilyId,tb.ref_personId as refPersonId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.userId,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.hx_username as hxUsername,tb.ranking  
                    FROM $this->TABLE tb 
                    left join $this->TABLE_USER tu on tb.userId = tu.id 
                    WHERE tb.branchId = '$branchId' AND tb.is_delete = '0' order by level";

        $result = $this->pdo->query($sql);
        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        return $result;
    }

    /**
     * 获取家族人物列表，供前台select使用
     * @param $familyId
     * @param array 人物数组
     */
    public function getFamilyPersonList($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,same_personId as samePersonId,is_adoption as isAdoption,name,photo,userId FROM $this->TABLE WHERE familyId = '$familyId'  AND is_delete = '0'  order by level";
        return $this->pdo->query($sql);
    }

    /**
     * 获得有填写手机号的人物列表
     * @param $familyId
     * @return mixed
     */
    public function getFamilyPersonListWithPhone($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,same_personId as samePersonId,is_adoption as isAdoption,name,birthday,gender,blood_type,marital_status,phone,qq,country,country_name,province,province_name,city,city_name,area,area_name,town,town_name,address,ranking FROM $this->TABLE WHERE familyId = '$familyId'  AND is_delete = '0' AND phone IS NOT NULL AND phone <> '' order by level";
        return $this->pdo->query($sql);
    }

    /**
     * 判断person在家族中是否存在
     * @param $str
     * @param $familyId
     * @return bool
     */
    public function verifyPersonExistsInFamily($str, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT count('id') FROM $this->TABLE WHERE $str AND familyId='$familyId' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);

        if ($result["count('id')"] > 0) {
            return $result["count('id')"];
        } else {
            return false;
        }
    }

    /**
     * 根据用户id和家族id找到人物
     * @param $familyId 家族id
     * @param $userId   用户id
     */
    public function getPersonByFamilyIdAndUserId($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.userId,tb.type,tb.address,tb.birthday,tb.gender,tb.photo,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamiyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.hx_username as hxUsername,tb.ranking   
                    FROM $this->TABLE tb 
                    left join $this->TABLE_USER tu on tb.userId = tu.id 
                    WHERE tb.familyId = '$familyId' AND tb.userId = '$userId'  AND tb.is_delete = '0' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return null;
        }
        $person = new Person($result);
        $parser = new RelationParser($person);
        $person->father = $parser->getFathers();
        $person->mother = $parser->getMothers();
        $person->son = $parser->getSons();
        $person->daughter = $parser->getDaughters();
        $person->spouse = $parser->getSpouses();
        $person->brother = $parser->getBrothers();
        $person->sister = $parser->getSisters();

        return $person;
    }

    public function getPersonListByFamilyId($familyId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $where = '';
        if ($maxId > 0 && $sinceId > 0) {
            $where .= "  AND tb.id<'$maxId' AND tb.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where .= "  AND tb.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where .= "  AND tb.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where .= '';
        }
        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.userId,tb.type,tb.address,tb.birthday,tb.gender,tb.photo,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamiyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.hx_username as hxUsername,tb.ranking   
                    FROM $this->TABLE tb 
                    left join $this->TABLE_USER tu on tb.userId = tu.id 
                    WHERE tb.familyId = '$familyId'  $where   AND tb.is_delete = '0' ORDER BY tb.id DESC LIMIT $count";

        $result = $this->pdo->query($sql);
        foreach ($result as $k => $v) {
            $person = new Person($v);
            $parser = new RelationParser($person);
            $result[$k]['father'] = $parser->getFathers();
            $result[$k]['mother'] = $parser->getMothers();
            $result[$k]['son'] = $parser->getSons();
            $result[$k]['daughter'] = $parser->getDaughters();
            $result[$k]['spouse'] = $parser->getSpouses();
            $result[$k]['brother'] = $parser->getBrothers();
            $result[$k]['sister'] = $parser->getSisters();
        }

        return $result;
    }

    public function getCountPersonsByFamilyId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) 
                    FROM $this->TABLE 
                    WHERE familyId = '$familyId' AND is_delete = '0' ";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    public function getUserCreatePersonListByFamilyIdAndUserId($familyId, $userId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $where = '';
        if ($maxId > 0 && $sinceId > 0) {
            $where .= "  AND tb.id<'$maxId' AND tb.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where .= "  AND tb.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where .= "  AND tb.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where .= '';
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.userId,tb.type,tb.address,tb.birthday,tb.gender,tb.photo,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamiyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.hx_username as hxUsername,tb.ranking   
                    FROM $this->TABLE tb 
                    left join $this->TABLE_USER tu on tb.userId = tu.id 
                    WHERE tb.familyId = '$familyId'  $where  AND tb.create_by = '$userId'  AND tb.is_delete = '0' ORDER BY tb.id DESC LIMIT $count";

        $result = $this->pdo->query($sql);
        foreach ($result as $k => $v) {
            $person = new Person($v);
            $parser = new RelationParser($person);
            $result[$k]['father'] = $parser->getFathers();
            $result[$k]['mother'] = $parser->getMothers();
            $result[$k]['son'] = $parser->getSons();
            $result[$k]['daughter'] = $parser->getDaughters();
            $result[$k]['spouse'] = $parser->getSpouses();
            $result[$k]['brother'] = $parser->getBrothers();
            $result[$k]['sister'] = $parser->getSisters();
        }

        return $result;
    }

    public function getCountUserCreatePersonsByFamilyIdAndUserId($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "SELECT count(id)  
                    FROM $this->TABLE  
                    WHERE familyId = '$familyId'   AND  create_by = '$userId'  AND  is_delete = '0'";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 根据人物对象获取家族子树,也就是直系亲属
     * @param $familyId 家族id
     * @param $userId   用户id
     * @return array 返回的子树集合
     */
    public function getFamilyPersonsSubTreeByPerson($familyId, $person)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($person == null) {
            return array();
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.address,tb.type,tb.birthday,tb.gender,tb.photo,tb.userId,tb.phone,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamilyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.hx_username as hxUsername,tb.ranking   
                    FROM $this->TABLE tb 
                    left join $this->TABLE_USER tu on tb.userId = tu.id ";
        $where = "WHERE ";

        foreach ($person->father as $value) {
            if ($where != "WHERE ")
                //@codeCoverageIgnoreStart
                $where = $where . "OR tb.id = $value ";
            //@codeCoverageIgnoreEnd
            else
                $where = $where . "tb.id = $value ";
        }
        foreach ($person->mother as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->spouse as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->brother as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->sister as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->son as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->daughter as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        if ($where != "WHERE ")
            $sql = $sql . $where . "AND tb.is_delete = 0 ORDER BY level";


        $resultRelate = $this->pdo->query($sql);        //当前人物的附加关系


        for ($i = 0; $i < count($resultRelate); $i++) {
            $person = new Person($resultRelate[$i]);        //转换为person对象
            $parser_new = new RelationParser($person);
            $resultRelate[$i]['father'] = $parser_new->getFathers();
            $resultRelate[$i]['mother'] = $parser_new->getMothers();
            $resultRelate[$i]['son'] = $parser_new->getSons();
            $resultRelate[$i]['daughter'] = $parser_new->getDaughters();
            $resultRelate[$i]['spouse'] = $parser_new->getSpouses();
            $resultRelate[$i]['brother'] = $parser_new->getBrothers();
            $resultRelate[$i]['sister'] = $parser_new->getSisters();
        }

        return $resultRelate;
    }

    /**
     * 获取家族子树,也就是直系亲属
     * @param $familyId 家族id
     * @param $userId   用户id
     * @return array 返回的子树集合
     */
    public function getFamilyPersonsSubTree($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $person = $this->getPersonByFamilyIdAndUserId($familyId, $userId);

        if ($person == null) {
            return array();
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.address,tb.type,tb.birthday,tb.gender,tb.photo,tb.userId,tb.phone,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamilyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.hx_username as hxUsername,tb.ranking   
                    FROM $this->TABLE tb 
                    left join $this->TABLE_USER tu on tb.userId = tu.id ";
        $where = "WHERE ";

        foreach ($person->father as $value) {
            if ($where != "WHERE ")
                //@codeCoverageIgnoreStart
                $where = $where . "OR tb.id = $value ";
            //@codeCoverageIgnoreEnd
            else
                $where = $where . "tb.id = $value ";
        }
        foreach ($person->mother as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->spouse as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->brother as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->sister as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->son as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        foreach ($person->daughter as $value) {
            if ($where != "WHERE ")
                $where = $where . "OR tb.id = $value ";
            else
                //@codeCoverageIgnoreStart
                $where = $where . "tb.id = $value ";
            //@codeCoverageIgnoreEnd
        }
        if ($where != "WHERE ")
            $sql = $sql . $where . "AND tb.is_delete = 0 ORDER BY level";


        $resultRelate = $this->pdo->query($sql);        //当前人物的附加关系


        for ($i = 0; $i < count($resultRelate); $i++) {
            $person = new Person($resultRelate[$i]);        //转换为person对象
            $parser_new = new RelationParser($person);
            $resultRelate[$i]['father'] = $parser_new->getFathers();
            $resultRelate[$i]['mother'] = $parser_new->getMothers();
            $resultRelate[$i]['son'] = $parser_new->getSons();
            $resultRelate[$i]['daughter'] = $parser_new->getDaughters();
            $resultRelate[$i]['spouse'] = $parser_new->getSpouses();
            $resultRelate[$i]['brother'] = $parser_new->getBrothers();
            $resultRelate[$i]['sister'] = $parser_new->getSisters();
        }

        return $resultRelate;
    }

    /**
     * 根据过滤器获取家族人物列表
     * @param pageIndex  页码
     * @param pageSize   页大小
     * @param $familyId  家族id
     * @param $isUser    是否是用户,true代表需要绑定用户,false代表不作为过滤条件
     * @param $hasPhone  是否有手机号,,true代表需要有手机号,false代表不作为过滤条件
     * @param array 人物数组
     */
    public function getFamilyPersonsPagingByFilter($pageIndex, $pageSize, $familyId, $isUser, $hasPhone)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.name,tb.zi,tb.remark,tb.address,tb.type,tb.birthday,tb.userId,tb.gender,tb.photo,tb.phone,tb.userId,tb.share_url as shareUrl,tb.familyId,
        tb.ref_familyId as refFamilyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
        tb.level,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
        tu.hx_username as hxUsername,tb.ranking   
        FROM $this->TABLE tb 
        left join $this->TABLE_USER tu on tb.userId = tu.id ";

        $where = "WHERE tb.familyId = '$familyId' ";

        if ($isUser && $hasPhone) {
            $where = $where . " AND (tb.userId > 0 OR (tb.phone IS NOT NULL AND tb.phone != '') ) ";
        } else if ($hasPhone) {
            $where = $where . " AND (tb.phone IS NOT NULL AND tb.phone != '') ";
        } else if ($isUser) {
            $where = $where . " AND tb.userId > 0";
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $where = $where . "  AND tb.is_delete = 0 AND tb.is_dead = 0 ORDER BY tb.level ASC limit $offset,$pageSize";

        $result = $this->pdo->query($sql . $where);        //当前人物的附加关系

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser_new = new RelationParser($person);
            $result[$i]['father'] = $parser_new->getFathers();
            $result[$i]['mother'] = $parser_new->getMothers();
            $result[$i]['son'] = $parser_new->getSons();
            $result[$i]['daughter'] = $parser_new->getDaughters();
            $result[$i]['spouse'] = $parser_new->getSpouses();
            $result[$i]['brother'] = $parser_new->getBrothers();
            $result[$i]['sister'] = $parser_new->getSisters();
        }

        return $result;
    }

    /**
     * 根据过滤器获取家族人物总数
     * @param $familyId  家族id
     * @param $isUser    是否是用户,true代表需要绑定用户,false代表不作为过滤条件
     * @param $hasPhone  是否有手机号,,true代表需要有手机号,false代表不作为过滤条件
     * @param int 人物总数
     */
    public function getFamilyPersonsCountByFilter($familyId, $isUser = false, $hasPhone = false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*) FROM $this->TABLE ";

        $where = " WHERE familyId = '$familyId' ";

        if ($isUser && $hasPhone) {
            $where = $where . " AND (userId > 0 OR (phone IS NOT NULL AND phone != '')) ";
        } else if ($hasPhone) {
            $where = $where . " AND (phone IS NOT NULL AND phone != '')";
        } else if ($isUser) {
            $where = $where . " AND userId > 0";
        }

        $result = $this->pdo->uniqueResult($sql . $where);        //当前人物的附加关系

        return $result['count(*)'];
    }


    /**
     * 获取家族的成员的数量
     * @param $familyId 要查询的家族ID
     * @param $name     检索的人物姓名
     * @param $level    检索的人物等级
     * @return int 返回人数
     */
    public function getFamilyPersonsCount($familyId, $name, $level)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*)	FROM $this->TABLE WHERE familyId = '$familyId'  AND is_delete = '0' ";
        $where = "";
        if ($name != "" && $name != NULL) {
            $where = $where . " AND name = '%$name%'";
        }

        if ($level != "" && $level != NULL) {
            $where = $where . " AND level = '$level'";
        }

        $sql = $sql . $where;

        $result = $this->pdo->query($sql);

        return $result[0]['count(*)'];
    }

    /**
     * 以分页的方式查询家族成员
     * @param $familyId
     * @param $pageIndex int
     * @param $pageSize int
     * @param $name
     * @param $level
     * @return int 返回人数
     */
    public function getFamilyPersonsPaging($familyId, $pageIndex, $pageSize, $name, $level, $orderby)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $offset = ($pageIndex - 1)*$pageSize;

        $sql = "SELECT t.id,t.same_personId as samePersonId,t.is_adoption as isAdoption,t.name,t.zi,t.remark,t.country,t.type,t.country_name as countryName,t.province ,t.province_name as provinceName,t.city ,t.city_name as cityName,t.area ,t.area_name as areaName,t.town ,t.town_name as townName,t.address,t.birthday,t.phone,t.gender,t.photo,t.share_url as shareUrl,t.is_dead as isDead,t.dead_time as deadTime,t.familyId,
                    t.ref_familyId as refFamilyId,t.father,t.mother,t.son,t.daughter,t.spouse,t.sister,t.brother,
                    t.level,t.userId,tu.username,tu.nickname,tu.hx_username as hxUsername,count(te.id) as eventCount,t.create_by as createBy,t.create_time as createTime,t.update_by as updateBy,t.update_time as updateTime,t.ranking  
                    FROM $this->TABLE t 
                    LEFT JOIN $this->TABLE_EVENT te on te.personId = t.id 
                    LEFT JOIN $this->TABLE_USER tu on tu.id = t.userId 
                    WHERE t.familyId = '$familyId'  AND t.is_delete = '0' ";
        if ($orderby == "create_time") {
            $limit = "  ORDER BY t.create_time ASC LIMIT $offset,$pageSize ";
        } else {
            $limit = "  ORDER BY t.level ASC LIMIT $offset,$pageSize ";
        }

        $where = "";
        if ($level != "" && $level != NULL) {
            $where = $where . " AND t.level = '$level'";
        }

        if ($name != "" && $name != NULL) {
            $where = $where . " AND t.name LIKE '%$name%'";
        }


        $sql = $sql . $where . " GROUP BY t.id" . $limit;

        $result = $this->pdo->query($sql);

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['birthday'] = $person->birthday;
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
            if (count($result[$i]['father']) > 0) {
                $fatherIds = implode(',', $result[$i]['father']);
                $sql = "SELECT id, name, level, photo, is_delete, birthday FROM $this->TABLE WHERE id in($fatherIds) ";
                $result[$i]['other']['fathers'] = $this->pdo->query($sql);
            } else {
                $result[$i]['other']['fathers'] = [];
            }
            if (count($result[$i]['mother']) > 0) {
                $motherIds = implode(',', $result[$i]['mother']);
                $sql = "SELECT id, name, level, photo, is_delete, birthday FROM $this->TABLE WHERE id in($motherIds) ";
                $result[$i]['other']['mothers'] = $this->pdo->query($sql);
            } else {
                $result[$i]['other']['mothers'] = [];
            }
            if (count($result[$i]['son']) > 0) {
                $sonIds = implode(',', $result[$i]['son']);
                $sql = "SELECT id, name, level, photo, is_delete, birthday FROM $this->TABLE WHERE id in($sonIds) ";
                $result[$i]['other']['sons'] = $this->pdo->query($sql);
            } else {
                $result[$i]['other']['sons'] = [];
            }
            if (count($result[$i]['daughter']) > 0) {
                $daughterIds = implode(',', $result[$i]['daughter']);
                $sql = "SELECT id, name, level, photo, is_delete, birthday FROM $this->TABLE WHERE id in($daughterIds) ";
                $result[$i]['other']['daughters'] = $this->pdo->query($sql);
            } else {
                $result[$i]['other']['daughters'] = [];
            }
            if (count($result[$i]['spouse']) > 0) {
                $spouseIds = implode(',', $result[$i]['spouse']);
                $sql = "SELECT id, name FROM $this->TABLE WHERE id in($spouseIds) ";
                $result[$i]['other']['spouses'] = $this->pdo->query($sql);
            } else {
                $result[$i]['other']['spouses'] = [];
            }
            if (count($result[$i]['brother']) > 0) {
                $brotherIds = implode(',', $result[$i]['brother']);
                $sql = "SELECT id, name FROM $this->TABLE WHERE id in($brotherIds) ";
                $result[$i]['other']['brothers'] = $this->pdo->query($sql);
            } else {
                $result[$i]['other']['brothers'] = [];
            }
            if (count($result[$i]['sister']) > 0) {
                $sisterIds = implode(',', $result[$i]['sister']);
                $sql = "SELECT id, name FROM $this->TABLE WHERE id in($sisterIds) ";
                $result[$i]['other']['sisters'] = $this->pdo->query($sql);
            } else {
                $result[$i]['other']['sisters'] = [];
            }
        }

        return $result;
    }

    /**
     * 为家族成员添加照片
     * @param $personId 人物id
     * @param $path    照片路径
     * @return int 返回受影响的记录的条数
     */
    public function addPhoto($personId, $path)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE	SET photo = '$path',update_time = now() WHERE id = $personId AND is_delete = '0' ";
        $count = $this->pdo->update($sql);

        return $count;
    }

    /**
     * 检查分享码是否被使用
     * @param $shareCode 分享码
     * @return bool true已使用，false未使用
     */
    public function checkShareCodeIsUsed(string $shareCode)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE share_url = '$shareCode' AND is_delete = '0' AND userId <= 0 ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据邀请码获取人物id
     * @param $shareCode 分享码
     * @return mix false代表人物不存在,int是人物的id
     */
    public function getPersonIdByShareCode($shareCode)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE share_url = '$shareCode' AND is_delete = '0' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        } else {
            return $result['id'];
        }
    }

    /**
     * 通过shareCode获取人物信息
     * @param $shareCode
     * @return bool
     */
    public function getPersonByShareCode($shareCode)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.branchId,tb.zi,tb.zpname,tb.remark,tb.name,tb.type,tb.level,
                tb.country,tb.country_name as countryName,tb.province,tb.province_name as provinceName,tb.city,tb.city_name as cityName,
                tb.area,tb.area_name as areaName,tb.town,tb.town_name as townName,tb.address,
                tb.birthday,tb.gender,tb.photo,tb.phone,tb.blood_type as bloodType,
                tb.marital_status as maritalStatus,tb.qq,tb.share_url as shareUrl,
                tb.is_dead as isDead,tb.dead_time as deadTime,
                tb.familyId,tf.groupType,tf.name as familyName,tf.photo as familyPhoto,tb.ref_familyId as refFamilyId,
                tb.userId,tb.father,tb.mother,tb.brother,tb.sister,tb.spouse,tb.son,tb.daughter,
                tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,
                tb.update_time as updateTime,tu.hx_username as hxUsername,tb.ranking ,tb.sideText, tb.profileText  
                    
                    FROM $this->TABLE tb  
                    LEFT JOIN $this->TABLE_USER tu on tb.userId = tu.id 
                    LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId
                    WHERE tb.share_url = '$shareCode' and tb.is_delete = '0' ";

        $queryResult = $this->pdo->uniqueResult($sql);
        if ($queryResult != null) {
            $person = new Person($queryResult);
            $parser = new RelationParser($person);
            $person->father = $parser->getFathers();
            $person->mother = $parser->getMothers();
            $person->son = $parser->getSons();
            $person->daughter = $parser->getDaughters();
            $person->spouse = $parser->getSpouses();
            $person->brother = $parser->getBrothers();
            $person->sister = $parser->getSisters();
            return $person;
        }
        //@codeCoverageIgnoreStart
        return null;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 将用户绑定到人物的公共更新部分
     * @param $userId 用户id
     * @param $personId 人物id
     */
    private function bindUserToPersonCommonUpdate($userId, $personId)
    {
        //绑定用户到人物时，更新该用户的家族数资料
        $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1 , update_time = now() WHERE userId = '$userId' ";
        $this->pdo->update($sql);

        //用户继承该人物的大事件
        $eventDB = new CDBEvent();
        $eventDB->linkAllPersonEventsToUser($personId, $userId);

        //用户绑定成功，更新用户follow
        $followIds = $this->getPersonsIdByPathLen($personId, 3);     //路径3以内
        if (count($followIds) > 0) {
            $followIds = $this->getUserIdsFromPersonIds($followIds, $userId);

            if (count($followIds) > 0) {

                $userFollowDB = new UserFollowDB();
                $userFollowDB->appendUserFollow($userId, $followIds);

                $appendUserArr = array($userId);
                //用户绑定成功，将该用户放到其他用户的follow集合中
                foreach ($followIds as $followId) {
                    $userFollowDB->appendUserFollow($followId, $appendUserArr);
                }
            }
        }
    }

    /**
     * 用户通过分享链接注册或登录，将用户id绑定到指定的人物上
     * @param $userId 用户id
     * @param $shareCode 分享码
     * @return int 更新的记录数,-2代表用户已存在家族中，-1代表出现异常
     */
    public function bindUserToPerson($userId, $shareCode, $fromUserId, $type)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //检查是否已经在家族中
        $sql = "SELECT familyId FROM $this->TABLE WHERE share_url = '$shareCode' and is_delete = '0' ";
        $shareCodeFamilyId = $this->pdo->uniqueResult($sql);
        $familyId = $shareCodeFamilyId['familyId'];

        $sql = "SELECT familyId FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0' ";
        $userFamilyIds = $this->pdo->query($sql);
        foreach ($userFamilyIds as &$userFamilyId) {
            //@codeCoverageIgnoreStart
            if ($userFamilyId['familyId'] == $familyId) {
                return -2;
                //@codeCoverageIgnoreEnd
            }
        }


        try {
            $this->pdo->beginTransaction();

            $sql = "UPDATE $this->TABLE	SET userId = '$userId' ,update_time = now() WHERE share_url = '$shareCode' AND userId = '0' and is_delete = '0' ";
            $count = $this->pdo->update($sql);

            if ($count > 0) {
                $personId = $this->getPersonIdByShareCode($shareCode);

                //记录人物操作日志
                $function_name = __CLASS__ . '->' . __FUNCTION__;
                $old_person = $this->getPersonById($personId);
                $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
                $familyId = $old_person->familyId;
                $option_detail = '通过分享链接注册或登录，绑定userId';
                $option_user = $GLOBALS['userId'] ?? 0;
                $other_msg = ['share_url' => $shareCode, 'userId' => $userId];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
                //end

                //执行绑定人物的更新操作
                $this->bindUserToPersonCommonUpdate($userId, $personId);
                //添加绑定记录
                $this->insertPersonBindUserRecord($familyId, $personId, $fromUserId, $userId, $type, $userId);
            }

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $count;
    }

    /**
     * 用户通过分享链接注册或登录，将用户id绑定到指定的人物上,无事务管理
     * @param $userId 用户id
     * @param $shareCode 分享码
     * @return int 更新的记录数
     */
    public function bindUserToPersonWithoutTransaction($userId, $shareCode, $fromUserId, $type, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE	SET userId = '$userId',update_time = now() WHERE share_url = '$shareCode' AND userId = '0' AND is_delete = '0' ";

        $count = $this->pdo->update($sql);

        if ($count > 0) {
            //记录人物操作日志
            $personId = 0;
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $this->getPersonById($personId);
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $familyId = $old_person->familyId;
            $option_detail = '通过分享链接注册或登录，绑定userId';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['share_url' => $shareCode, 'userId' => $userId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
            //end

            $personId = $this->getPersonIdByShareCode($shareCode);
            //执行绑定人物的更新操作
            $this->bindUserToPersonCommonUpdate($userId, $personId);
            $this->insertPersonBindUserRecord($familyId, $personId, $fromUserId, $userId, $type, $userId);
        }

        return $count;
    }

    /**
     * 用户通过邀请，将用户id绑定到指定的人物上
     * @param $userId 用户id
     * @param $person 人物
     * @return int 更新的记录数,-1代表用户已绑定人物
     */
    public function bindUserToPersonWithPersonId($userId, Person $person)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0' AND familyId = '$person->familyId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            //表示这个用户绑定家族中某个人
            return -1;
        }

        try {
            $this->pdo->beginTransaction();

            //记录人物操作日志
            $personId = $person->id;
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $this->getPersonById($personId);
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $familyId = $old_person->familyId;
            $option_detail = '通过分享链接注册或登录，绑定userId';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['userId' => $userId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
            //end

            //更新人物绑定的用户id
            $sql = "UPDATE $this->TABLE	SET userId = '$userId',update_time = now() WHERE id = '$person->id' AND userId = '0' AND is_delete = '0'";
            $count = $this->pdo->update($sql);

            if ($count > 0) {
                //执行绑定人物的更新操作
                $this->bindUserToPersonCommonUpdate($userId, $person->id);
                //将申请用户表中的记录也删除
                $familyDB = new CDBFamily();
                $familyDB->deleteFamilyUser($person->familyId, $userId);
            }

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $count;
    }

    /**
     * 用户通过邀请，将用户id绑定到指定的人物上
     * @param $userId 用户id
     * @param $person 人物
     * @return int 更新的记录数,-1代表用户已绑定人物
     */
    public function bindUserToPersonWithPersonIdV3($userId, Person $person, $userPermission, $operator)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0' AND familyId = '$person->familyId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            //表示这个用户绑定家族中某个人
            return -1;
        }

        try {
            $this->pdo->beginTransaction();
            $familyDB = new CDBFamily();

            //记录人物操作日志
            $personId = $person->id;
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $this->getPersonById($personId);
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $familyId = $old_person->familyId;
            $option_detail = '通过分享链接注册或登录，绑定userId';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['userId' => $userId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
            //end

            //更新人物绑定的用户id
            $sql = "UPDATE $this->TABLE	SET userId = '$userId',update_time = now() WHERE id = '$person->id' AND userId = '0' AND is_delete = '0'";
            $count = $this->pdo->update($sql);

            $permissionResult = $familyDB->modifyPermission(
                $person->familyId,
                $userPermission['permission'] === 100 ? 200 : null,
                $userId,
                0,
                0,
                $operator,
                0
            );

            if ($count > 0) {
                //执行绑定人物的更新操作
                $this->bindUserToPersonCommonUpdate($userId, $person->id);
                //将申请用户表中的记录也删除
                //                $familyDB = new CDBFamily();
                //                $familyDB->deleteFamilyUser($person->familyId, $userId);
            }

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $count;
    }

    /**
     * 用户通过邀请，将用户id绑定到指定的人物上
     * @param $userId
     * @param $socialCircleId
     * @param Person $person
     * @return int
     */
    public function bindUserToPersonWithPersonIdAndCircle($userId, $socialCircleId, Person $person, $fromUserId, $type)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0' AND familyId = '$person->familyId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            //表示这个用户绑定家族中某个人
            return -1;
        }

        try {
            $this->pdo->beginTransaction();

            //更新人物绑定的用户id
            $sql = "UPDATE $this->TABLE	SET userId = '$userId',update_time = now() WHERE id = '$person->id' AND userId = '0' AND is_delete = '0'";
            $count = $this->pdo->update($sql);


            if ($count > 0) {
                //记录人物操作日志
                $personId = $person->id;
                $function_name = __CLASS__ . '->' . __FUNCTION__;
                $old_person = $this->getPersonById($personId);
                $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
                $familyId = $old_person->familyId;
                $option_detail = '通过分享链接注册或登录，绑定userId';
                $option_user = $GLOBALS['userId'] ?? 0;
                $other_msg = ['userId' => $userId, 'fromUserId' => $fromUserId, 'type' => $type, 'socialCircleId' => $socialCircleId];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
                //end
                //执行绑定人物的更新操作
                $this->bindUserToPersonCommonUpdate($userId, $person->id);
                //添加绑定记录
                $this->insertPersonBindUserRecord($person->familyId, $person->id, $fromUserId, $userId, $type, $userId);
                //将申请用户表中的记录也删除
                $familyDB = new CDBFamily();
                $familyDB->deleteFamilyUser($person->familyId, $userId);

                $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
                VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
                $followId = $this->pdo->insert($sql);
                if ($followId < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                    $this->pdo->rollback();
                    exit;
                }
                $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow+1 WHERE id='$socialCircleId' AND is_delete=0";
                $updateRow = $this->pdo->update($sql);
                if ($updateRow < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
                    $this->pdo->rollback();
                    exit;
                }
            }

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $count;
    }


    /**
     * 用户通过分享码绑定人物并且加入家族
     * @param $userId
     * @param $socialCircleId
     * @param Person $person
     * @param $fromUserId
     * @return bool
     */
    public function bindUserToPersonByShareCode($userId, $socialCircleId, Person $person, $fromUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $this->init();

        $this->pdo->beginTransaction();

        $sql = "SELECT id FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0' AND familyId = '$person->familyId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            //表示这个用户绑定家族中某个人
            Util::printResult($GLOBALS['ERROR_SQL_QUERY'], '用户已经绑定家族中某个人');
            exit;
        }

        //更新人物绑定的用户id
        $sql = "UPDATE $this->TABLE	SET userId = '$userId',update_time = now() WHERE id = '$person->id' AND userId = '0' AND is_delete = '0'";
        $count = $this->pdo->update($sql);

        if ($count > 0) {
            //记录人物操作日志
            $personId = $person->id;
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $this->getPersonById($personId);
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $familyId = $old_person->familyId;
            $option_detail = '通过分享链接注册或登录，绑定userId';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['userId' => $userId, 'fromUserId' => $fromUserId, 'socialCircleId' => $socialCircleId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
            //end
            //执行绑定人物的更新操作
            $this->bindUserToPersonCommonUpdate($userId, $person->id);
            $familyDb = new CDBFamily();
            $tmpFamilyUserId = $familyDb->addFamilyUser($person->familyId, $userId, 2);
            if ($tmpFamilyUserId <= 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加失败');
                $this->pdo->rollback();
                exit;
            }
            // 关注圈子
            $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
        VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
            $followId = $this->pdo->insert($sql);
            if ($followId <= 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                $this->pdo->rollback();
                exit;
            }
            // 更新圈子关注数量
            $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow+1 WHERE id='$socialCircleId' AND is_delete=0";
            $updateRow = $this->pdo->update($sql);
            if ($updateRow <= 0) {
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
                $this->pdo->rollback();
                exit;
            }

            $personDB = new CDBPerson();
            if ($fromUserId != 0) {
                $personDB->insertPersonBindUserRecord($person->familyId, $person->id, $fromUserId, $userId, 5, $userId);
            }
            $this->pdo->commit();
            return true;
        } else {
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '绑定人物失败');
            $this->pdo->rollback();
            exit;
        }
    }

    /**
     * 根据人物id获取家族id
     * @param $personId 人物id
     * @return int 家族id
     */
    public function getFamilyIdByPersonId($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT familyId FROM $this->TABLE WHERE id = '$personId'  AND is_delete = '0'  ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null)
            return $result['familyId'];
        return null;
    }

    /**
     * 为人物添加信息,技能,爱好
     * @param $personId 人物id
     * @param $userId   用户id
     * @param $info 人物信息
     * @param $type 信息类型,1是技能,2是爱好
     * @param $operateUserId 当前记录的操作人
     * @return int 插入的记录id
     */
    public function addForPerson($personId, $userId, $info, $type, $operateUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($userId === '' || $userId === null)
            //@codeCoverageIgnoreStart
            $userId = -1;
        //@codeCoverageIgnoreEnd

        $sql = "INSERT INTO $this->TABLE_DETAIL 
                    (personId,userId,info,type,create_time,create_by,update_time,update_by) 
                    VALUES 
                    ('$personId','$userId','$info','$type',now(),'$operateUserId',now(),'$operateUserId')";

        $result = $this->pdo->insert($sql);
        return $result;
    }

    /**
     * 检查标签是否重复
     * @param $personId 人物id
     * @param $info 人物信息
     * @param $type 信息类型，1是技能，2是爱好
     * @return boolean true是重复，false不重复
     */
    public function checkTagDuplicate(int $personId, string $info, int $type)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE_DETAIL WHERE isDelete=0 AND personId = '$personId' AND info = '$info' AND type = '$type' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null)
            return false;

        return true;
    }

    /**
     * 为人物添加工作经历
     * @param $personId 人物id
     * @param $userId   用户id
     * @param $company 公司
     * @param $occupation 职位
     * @param $startTime 工作的开始时间
     * @param $endTime 工作的结束时间
     * @param $operateUserId 当前记录的操作人
     * @return int 插入的记录id
     */
    public function addJobForPerson($personId, $userId, $company, $occupation, $startTime, $endTime, $operateUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_JOB 
                    (personId,userId,start_time,end_time,company,occupation,create_time,create_by,update_time,update_by)
                    VALUES 
                    ('$personId','$userId','$startTime','$endTime','$company','$occupation',now(),'$operateUserId',now(),'$operateUserId')";


        $result = $this->pdo->insert($sql);
        return $result;
    }

    /**
     * 获取人物的标签,如技能,爱好
     * @param $personId 人物id
     * @return array 返回人物的标签数组
     */
    public function getPersonTag($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,personId,userId,info,type FROM $this->TABLE_DETAIL WHERE isDelete=0 AND personId = '$personId'";
        return $this->pdo->query($sql);
    }

    /**
     * 获取人物的工作经历
     * @param $personId 人物id
     * @return array 返回人物的工作经历
     */
    public function getPersonJob($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,personId,userId,company,occupation,start_time as startTime,end_time as endTime FROM $this->TABLE_JOB WHERE isDelete=0 AND personId = '$personId' ";
        return $this->pdo->query($sql);
    }

    public function getPhotosByPersonId($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT photo FROM $this->TABLE WHERE id='$personId'";

        return $this->pdo->uniqueResult($sql);
    }

    public function getTypeByPersonDetailId($personDetailId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT type FROM $this->TABLE_DETAIL WHERE isDelete=0 AND id='$personDetailId'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result;
        }
        return false;
    }

    /**
     * 离开家族,有事务管理
     * @param $familyId 家族id
     * @param $userId   用户id
     * @return int 删除的记录数量
     */
    public function leaveFamily($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "SELECT id FROM $this->TABLE WHERE familyId = '$familyId' AND userId = '$userId' AND is_delete = '0' ";
            $res = $this->pdo->uniqueResult($sql);
            $sql = "UPDATE $this->TABLE SET userId = 0,update_time = now()  WHERE familyId = '$familyId' AND userId = '$userId'  AND is_delete = '0'";
            $count = $this->pdo->update($sql);

            if ($count > 0) {
                //记录人物日志
                $personId = $res['id'];
                $familyId = '';
                $function_name = __CLASS__ . '->' . __FUNCTION__;
                $old_person = $this->getPersonById($personId);
                $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
                $option_detail = '用户离开家族';
                $option_user = $GLOBALS['userId'] ?? 0;
                $other_msg = ['userId' => $userId];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

                //检查是否是家族管理员，如果是也删除掉
                $deleteSql = "UPDATE $this->TABLE_FAMILYADMIN SET isDelete=1 WHERE familyId = '$familyId' AND adminId = '$userId' AND isDelete=0 ";
                $this->pdo->update($deleteSql);

                //绑定用户到人物时，更新该人物的家族数资料
                $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num - 1,update_time = now() WHERE userId = '$userId' ";
                $this->pdo->update($sql);
            }

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return $count;
    }

    /**
     * 获取人物的绑定二维码
     * @param $personId 人物id
     * @return string 二维码路径
     */
    public function getPersonQRCode($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT qrcode,share_url FROM  $this->TABLE WHERE id = '$personId'  AND is_delete = '0'";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 人物id
     * @param $personId 人物id
     * @return mix 聊天室string 或null
     */
    public function getChatRoomByPersonId($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tf.hx_chatroom as hxChatRoom FROM $this->TABLE_FAMILY tf
                    INNER JOIN $this->TABLE tb on tb.familyId = tf.id 
                    WHERE tb.id = '$personId'";

        $res = $this->pdo->uniqueResult($sql);
        if ($res != null) {
            return $res['hxChatRoom'];
        }

        return null;
    }

    /**
     * 设置人物的二维码
     * @param $personId 人物id
     * @param $path  路径
     */
    public function setPersonQRCode($personId, $path)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $old_person = $this->getPersonById($personId);
        $familyId = $old_person->familyId;
        $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
        $option_detail = '设置人物二维码';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['qrcode' => $path];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        $sql = "UPDATE $this->TABLE set qrcode = '$path',update_time = now() WHERE id = '$personId'  AND is_delete = '0'";
        return $this->pdo->update($sql);
    }

    /**
     * 取消用户和人物之间的绑定（踢人）
     * @param $personId 人物id
     * @return bool true成功,fasle失败
     */
    public function cancelUserBind(int $personId): bool
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $person = $this->getPersonById($personId);

            //记录人物日志
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $familyId = $person->familyId;
            $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
            $option_detail = '取消用户和人物之间的绑定';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['oldUserId' => $person->userId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $sql = "UPDATE $this->TABLE SET userId = '0',update_time = now() WHERE id = '$personId' AND is_delete = '0' ";
            $count = $this->pdo->update($sql);
            if ($count > 0) {
                $deleteSql = "UPDATE $this->TABLE_FAMILYADMIN SET isDelete=1 WHERE familyId='$person->familyId' AND adminId='$person->userId' AND isDelete=0";
                $this->pdo->update($deleteSql);
            }
            $this->pdo->commit();
            return true;
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            //@codeCoverageIgnoreEnd
        }
    }

    public function cancelUserBindAndCircle(int $personId): bool
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $person = $this->getPersonById($personId);

            //记录人物日志
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $familyId = $person->familyId;
            $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
            $option_detail = '取消用户和人物之间的绑定';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['oldUserId' => $person->userId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $sql = "UPDATE $this->TABLE SET userId = '0',update_time = now() WHERE id = '$personId' AND is_delete = '0' ";
            $count = $this->pdo->update($sql);
            if ($count > 0) {
                $deleteSql = "UPDATE $this->TABLE_FAMILYADMIN SET isDelete=1 WHERE familyId='$person->familyId' AND adminId='$person->userId' AND isDelete=0";
                $this->pdo->update($deleteSql);
                $familyDB = new CDBFamily();
                $family = $familyDB->getFamilyById($person->familyId);
                $socialCircleId = $family->socialCircleId;
                //删除圈子关注列表中的该用户记录
                $sql = "UPDATE $this->TABLE_INFORMATION_FOLLOW SET isDelete=1 WHERE categoryId='$socialCircleId' AND userId = '$person->userId' AND isDelete=0";
                $this->pdo->update($sql);
                $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow-1 WHERE id='$socialCircleId' AND is_delete=0";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
            return true;
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            //@codeCoverageIgnoreEnd
        }
    }

    /**
     * 取消用户和人物之间的绑定,用户留在临时用户表中
     * @param $personId 人物id
     * @return bool true成功,fasle失败
     */
    public function cancelUserBindForTmp(int $personId, $familyId, $userId, $openTransaction = true): bool
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        $person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '取消用户和人物之间的绑定,用户留在临时表';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['oldUserId' => $person->userId];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        if ($openTransaction == false) {
            $sql = "UPDATE $this->TABLE SET userId = '0',update_time = now() WHERE id = '$personId' AND is_delete = '0' ";
            $count = $this->pdo->update($sql);
            if ($count > 0) {
                //删除管理员身份
                $deleteSql = "UPDATE $this->TABLE_FAMILYADMIN SET isDelete=1 WHERE familyId='$familyId' AND adminId='$userId' AND isDelete=0";
                $this->pdo->update($deleteSql);

                // 把用户放到family_user表中
                $sql = "INSERT INTO $this->TABLE_FAMILY_USER 
                (familyId,userId,is_delete,create_time,update_time)
                VALUES
                ('$familyId', '$userId', '0', now(), now())";

                $this->pdo->insert($sql);

                //更新用户资料的家族数
                $sql = "SELECT family_num FROM $this->TABLE_USER_PROFILE WHERE userId='$userId'";
                $result = $this->pdo->uniqueResult($sql);
                if ($result != null && $result['family_num'] != '0') {
                    $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num - 1,update_time = now() WHERE userId = '$userId'";
                    $this->pdo->update($sql);
                }
            }
            return true;
        } else {
            try {
                $this->pdo->beginTransaction();
                $person = $this->getPersonById($personId);
                $sql = "UPDATE $this->TABLE SET userId = '0',update_time = now() WHERE id = '$personId' AND is_delete = '0' ";
                $count = $this->pdo->update($sql);
                if ($count > 0) {
                    $deleteSql = "UPDATE $this->TABLE_FAMILYADMIN SET isDelete=1 WHERE familyId='$person->familyId' AND adminId='$person->userId' AND isDelete=0";
                    $this->pdo->update($deleteSql);

                    // 把用户放到family_user表中
                    $sql = "INSERT INTO $this->TABLE_FAMILY_USER 
                    (familyId,userId,is_delete,create_time,update_time)
                    VALUES
                    ('$familyId', '$userId', '0', now(), now())";

                    $this->pdo->insert($sql);

                    //更新用户资料的家族数
                    $sql = "SELECT family_num FROM $this->TABLE_USER_PROFILE WHERE userId='$userId'";
                    $result = $this->pdo->uniqueResult($sql);
                    if ($result != null && $result['family_num'] != '0') {
                        $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num - 1,update_time = now() WHERE userId = '$userId'";
                        $this->pdo->update($sql);
                    }
                }
                $this->pdo->commit();
                return true;
                //@codeCoverageIgnoreStart
            } catch (PDOException $e) {
                $this->pdo->rollback();
                return false;
                //@codeCoverageIgnoreEnd
            }
        }
    }


    /**
     * 通过活动id获得家族id
     * @param $activityId
     * @return bool
     */
    public function getFamilyIdByActivityId($activityId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT familyId from gener_activity WHERE id='$activityId'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result['familyId'];
        }
    }


    /**
     * 更新家族人物权限
     * @param $personId
     * @param $level
     * @param $userId
     * @return mixed
     */
    public function updateFamilyPermission($personId, $level, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        //记录人物日志
        $person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '更新家族人物权限';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['level' => $level];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        $sql = "UPDATE $this->TABLE_PERMISSION SET  level='$level',update_by='$userId',update_time=now()
                WHERE personId='$personId'";
        return $this->pdo->update($sql);
    }

    /**
     * 检查用户是否有权限删除某个标签
     *
     * @param $userId              用户ID
     * @param $personDetailId      用户标签ID
     * @return boolean
     */
    public function checkUserIdAndPersonDetailId($userId, $personDetailId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT personId FROM $this->TABLE_DETAIL WHERE isDelete=0 AND id = '$personDetailId'";
        $res = $this->pdo->uniqueResult($sql);

        if (is_null($res)) {
            return false;
        }
        $personId = $res['personId'];
        if ($this->verifyUserIdAndPersonId($userId, $personId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $personDetailId
     * @return bool
     */
    public function getPersonIdByPersonDetailId($personDetailId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT personId FROM $this->TABLE_DETAIL WHERE isDelete=0 AND id = '$personDetailId'";
        $res = $this->pdo->uniqueResult($sql);
        if (is_null($res)) {
            return false;
        }
        $personId = $res['personId'];
        return $personId;
    }

    /**
     * @param $jobId
     * @return bool
     */
    public function getPersonIdByJobId($jobId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT personId FROM $this->TABLE_JOB WHERE isDelete=0 AND id = '$jobId'";
        $res = $this->pdo->uniqueResult($sql);
        if (is_null($res)) {
            return false;
        }
        $personId = $res['personId'];
        return $personId;
    }

    /**
     * 删除指定标签
     *
     * @param $personDetailId   用户标签ID
     * @return int  删除的行数
     */
    public function deletePersonInfo($personDetailId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE_DETAIL SET isDelete=1 where id = '$personDetailId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 验证用户是否有权限删除职业信息
     *
     * @param $userId
     * @param $jobId
     * @return boolean
     */
    public function checkUserIdAndJobId($userId, $jobId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT personId FROM $this->TABLE_JOB WHERE isDelete=0 AND id = '$jobId'";
        $res = $this->pdo->uniqueResult($sql);
        if (is_null($res)) {
            return false;
        }
        $personId = $res['personId'];
        if ($this->verifyUserIdAndPersonId($userId, $personId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除指定的职业信息
     *
     * @param $jobId    职业ID
     * @return int      删除的行数
     */
    public function deletePersonJob($jobId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE_JOB SET isDelete=1  WHERE id = '$jobId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 获取用户绑定的人物列表
     * @param $userId 用户id
     */
    public function getUserBindPersonList(int $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.branchId,tb.name,tb.zi,tb.remark,tb.type,tb.level,tb.country,tb.country_name as countryName,tb.province,tb.province_name as provinceName,tb.city,tb.city_name as cityName,tb.area,tb.area_name as areaName,tb.town,tb.town_name as townName,tb.address,tb.birthday,tb.gender,tb.photo,tb.phone,tb.blood_type as bloodType,tb.marital_status as maritalStatus,tb.qq,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,tb.ref_familyId as refFamilyId,tb.userId,tb.father,tb.mother,tb.brother,tb.sister,tb.spouse,tb.son,tb.daughter,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime,tu.hx_username as hxUsername,tf.name as familyName,tb.ranking  
        
        FROM $this->TABLE tb  
        LEFT JOIN $this->TABLE_USER tu on tb.userId = tu.id 
        LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId 
        WHERE tb.userId = '$userId' and tb.is_delete = '0' ";

        $result = $this->pdo->query($sql);

        $personList = array();

        foreach ($result as $personResult) {
            $person = new Person($personResult);
            $parser = new RelationParser($person);
            $person->father = $parser->getFathers();
            $person->mother = $parser->getMothers();
            $person->son = $parser->getSons();
            $person->daughter = $parser->getDaughters();
            $person->spouse = $parser->getSpouses();
            $person->brother = $parser->getBrothers();
            $person->sister = $parser->getSisters();
            $personList[] = $person;
        }
        return $personList;
    }

    /**
     * 根据姓名搜索人物
     * @param 根据姓名获取人物
     * @return object Person对象
     */
    public function getPersonByNameAndFamilyIds($name, $familyIds)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $familyIdSql = '';
        $isFirst = true;
        foreach ($familyIds as $familyId) {
            if ($isFirst) {
                $familyIdSql = $familyIdSql . $familyId;
                $isFirst = false;
            } else {
                $familyIdSql = $familyIdSql . "," . $familyId;
            }
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.branchId,tb.name,tb.zi,tb.remark,tb.type,tb.is_dead as isDead,tb.dead_time as deadTime,tb.level,tb.birthday,tb.gender,tb.photo,tb.familyId,tf.name as familyName,tb.ref_familyId as refFamilyId,tb.userId,tb.ranking  
        FROM $this->TABLE tb  
        LEFT JOIN $this->TABLE_FAMILY tf on tb.familyId = tf.id 
        WHERE familyId in ($familyIdSql) and tb.name like '%$name%' and tb.is_delete = '0' ";

        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * 获取家族的用户
     *
     * @param array $familys 家族id数组
     *
     */
    public function getFamilyUsers(array $familyIds)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $familySql = '';
        $isFirst = true;

        foreach ($familyIds as $familyId) {
            if ($isFirst) {
                $familySql .= " familyId =  '$familyId' ";
                $isFirst = false;
            } else {
                //@codeCoverageIgnoreStart
                $familySql .= " OR familyId =  '$familyId' ";
                //@codeCoverageIgnoreEnd
            }
        }

        $sql = "SELECT tb.id as personId,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tu.photo,tb.userId as id,tu.nickname,tb.name as personName,tb.familyId,tf.name as familyName FROM $this->TABLE tb 
                LEFT JOIN $this->TABLE_USER tu on tu.id = tb.userId 
                LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId 
                WHERE ($familySql) AND tb.userId > 0 AND tb.is_delete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 从人物id数组中获取具有用户绑定的用户id数组，并排除用户自己
     * @param array $personIds 人物id数组
     * @return array 有效的id数组
     */
    public function getUserIdsFromPersonIds(array $personIds, $userId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $arrStr = '(' . Util::arrayToString($personIds) . ')';

        $sql = "SELECT userId FROM $this->TABLE WHERE userId > 0 AND id in $arrStr AND is_delete = '0'";
        $result = $this->pdo->query($sql);

        $idArray = array();
        foreach ($result as &$item) {
            if ($item['userId'] != $userId) {
                array_push($idArray, $item['userId']);
            }
        }

        return $idArray;
    }

    /**
     *  增加过继关系的支持
     * @param $personId
     * @param $samePersonId
     * @return mixed
     */
    public function setAdoptionPersonByPersonId($personId, $samePersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            //开启事务处理
            $this->pdo->beginTransaction();
            //记录人物日志
            $person = $this->getPersonById($personId);
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $familyId = $person->familyId;
            $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
            $option_detail = '增加过继关系的支持';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['samePersonId' => $samePersonId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $sql = "UPDATE $this->TABLE SET same_personId = '$samePersonId',is_adoption=1,update_time = now() WHERE id='$personId'";
            $updateRow = $this->pdo->update($sql);
            $sql = "UPDATE $this->TABLE SET same_personId = '$personId',update_time = now() WHERE id = '$samePersonId'";
            $this->pdo->update($sql);
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            //@codeCoverageIgnoreEnd
        }
        return $updateRow;
    }

    /**
     * 增加过继关系的支持
     * @param $srcPersonId
     * @param $destPersonId
     * @return mixed
     */
    public function addAdoptionPersonByPersonId($srcPersonId, $destPersonId, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $userId = $GLOBALS['userId'];
        try {
            if ($needTransaction) {
                //开启事务处理
                $this->pdo->beginTransaction();
            }


            //记录人物日志
            $personId = $srcPersonId;
            $person = $this->getPersonById($personId);
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $familyId = $person->familyId;
            $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
            $option_detail = '增加过继关系的支持';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['srcPersonId' => $srcPersonId, 'destPersonId' => $destPersonId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $sql = "INSERT INTO gener_person_adoption (src_personId,dest_personId,create_time,create_by,update_time,update_by)
           VALUES ('$srcPersonId','$destPersonId',now(),'$userId',now(),'$userId')";
            $insertId = $this->pdo->insert($sql);
            $sql = "UPDATE $this->TABLE SET is_adoption=2,update_time = now() WHERE id='$srcPersonId'";
            $this->pdo->update($sql);
            $sql = "UPDATE $this->TABLE SET is_adoption=1,update_time = now() WHERE id = '$destPersonId'";
            $this->pdo->update($sql);

            if ($needTransaction) {
                $this->pdo->commit();
            }
        } catch (PDOException $e) {
            if ($needTransaction) {
                $this->pdo->rollback();
            }
        }
        return $insertId;
    }

    /**
     * 删除过继关系
     * @param $srcPersonId
     * @param $destPersonId
     * @return mixed
     */
    public function deleteAdoptionPersonByPersonId($srcPersonId, $destPersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        $personId = $srcPersonId;
        $person = $this->getPersonById($personId, false);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '删除过继关系';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['srcPersonId' => $srcPersonId, 'destPersonId' => $destPersonId];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        $sql = "UPDATE gener_person_adoption SET is_delete=1,update_time = now() WHERE src_personId='$srcPersonId' AND dest_personId='$destPersonId' AND is_delete=0";
        $updateRow = $this->pdo->update($sql);
        return $updateRow;
    }

    /**
     * 还原is_adoption的默认值
     * @param $personId 需要还原的人物id
     */
    public function restoreIsAdoption($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        $person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '还原isAdoption';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['isAdoption' => $person->isAdoption];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        $sql = "UPDATE $this->TABLE SET is_adoption=0,update_time = now() WHERE id='$personId'";
        return $this->pdo->update($sql);
    }

    /**
     * 检查人物是否在接收过继的人中间
     * @param $personId 目标人物id
     * @return bool
     */
    public function checkDestPersonIdIsExists($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT dest_personId  FROM gener_person_adoption WHERE dest_personId='$personId' AND  is_delete=0 LIMIT 0,1";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['dest_personId'] == $personId) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 检查人物是否在过继的人中间
     * @param $personId 源人物id
     * @return bool
     */
    public function checkSrcPersonIdIsExists($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT src_personId  FROM gener_person_adoption WHERE src_personId='$personId' AND  is_delete=0 LIMIT 0,1";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['src_personId'] == $personId) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 检查过继关系是否已经存在
     * @param $srcPersonId
     * @param $destPersonId
     * @return bool
     */
    public function checkAdoptionExists($srcPersonId, $destPersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id  FROM gener_person_adoption WHERE src_personId='$srcPersonId' AND dest_personId='$destPersonId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['id'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据id获取过继关系列表
     * @param $personId
     * @return mixed
     */
    public function getAdoptionPersonList($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT src_personId,dest_personId,is_delete,create_time,create_by,update_time,update_by FROM gener_person_adoption WHERE is_delete=0 AND (src_personId='$personId' OR dest_personId='$personId' )";
        return $this->pdo->query($sql);
    }


    /**
     * 根据路径长度获取人物id数组，主要用于推送列表的生成
     * FIXME:在外层有事务时，这里的查询会查不到新加入的人物id。导致添加粉丝失效
     * @param int $corePersonId 核心人物id`
     * @param int $len 路径长度
     * @return 指定路径长度内的人物id数组
     */
    public function getPersonsIdByPathLen(int $corePersonId, int $len)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $resArr = array();
        $findArr = array();
        array_push($findArr, $corePersonId);

        for ($i = 0; $i < $len; $i++) {
            if (count($findArr) == 0) {
                //@codeCoverageIgnoreStart
                break;
                //@codeCoverageIgnoreEnd
            }
            $inArr = '(' . Util::arrayToString($findArr) . ')';
            $sql = "SELECT father, mother, spouse, brother, sister, son, daughter FROM $this->TABLE WHERE id in $inArr AND is_delete = 0";
            $result = $this->pdo->query($sql);

            // 查询到的结果放在resArr中，并同时放到清空后的$findArr中
            $findArr = array();
            foreach ($result as &$value) {
                $fathers = Util::relationSplit($value['father']);
                $mothers = Util::relationSplit($value['mother']);
                $spouses = Util::relationSplit($value['spouse']);
                $brothers = Util::relationSplit($value['brother']);
                $sisters = Util::relationSplit($value['sister']);
                $sons = Util::relationSplit($value['son']);
                $daughters = Util::relationSplit($value['daughter']);

                $tmpArr = array_merge($fathers, $mothers, $spouses, $brothers, $sisters, $sons, $daughters);

                $resArr = array_merge($resArr, $tmpArr);
                $findArr = array_merge($findArr, $tmpArr);
            }

            $findArr = array_unique($findArr);      //去重
        }

        $resArr = array_unique($resArr);        //去重

        return $resArr;
    }

    /**
     * 删除人物的时候更新samePersonId字段
     * @param $samePersonId
     * @return mixed
     */
    public function updateSamePersonId($samePersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        $personId = $samePersonId;
        $person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '更新samePersonId';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['samePersonId' => $person->samePersonId];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);


        $sql = "UPDATE $this->TABLE SET same_personId = 0,update_time = now() WHERE id='$samePersonId' AND is_delete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 移动人物到另一个节点下，也就是将某一支迁移到另一个祖先下
     * @param int $personId 人物id
     * @param int $toPersonId 迁移到的人物id
     * @return bool 迁移成功或失败
     */
    public function movePerson($personId, $toPersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $person = $this->getPersonById($personId);
        $toPerson = $this->getPersonById($toPersonId);

        //记录人物日志
        //$person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '迁移分支至新节点';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['toPerson' => $toPerson];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        if ($person == null || $toPerson == null) {
            return false;
        }

        // 检查person和toPerson不是同一个分支


        // 移动分支的流程如下:
        // 1. 将当前人物的父/母关系转移到目标人物下, 并更新兄弟姐妹
        // 2. 更新目标人物的子/女关系以及配偶的子/女关系
        // 3. 更新目标人物其他子女的兄弟姐妹
        // 4. 删除原来留存的父母/兄弟姐妹关系
        // 5. 移动时level会变化,需要改动
        try {
            $this->pdo->beginTransaction();

            $personFatherIdArray = [];  // 当前人物的父亲
            $personMotherIdArray = [];  // 当前人物的母亲
            // 获取toPerson的性别
            if ($toPerson->gender == 1) {
                $fatherIdArray[] = $toPerson->id;
                $motherIdArray = $toPerson->spouse;
            } else {
                $motherIdArray[] = $toPerson->id;
                $fatherIdArray = $toPerson->spouse;
            }

            $personBrotherArray = $toPerson->son;   // 当前人物的兄弟
            $personSisterArray = $toPerson->daughter;   // 当前人物的姐妹

            // 根据当前人物的性别更新兄弟姐妹
            if ($person->gender == 1) {
                array_push($personBrotherArray, $person->id);
            } else {
                array_push($personSisterArray, $person->id);
            }

            $personFatherStr = Util::arrayToRelation($fatherIdArray);
            $personMotherStr = Util::arrayToRelation($motherIdArray);
            $personBrotherStr = Util::arrayToRelation($personBrotherArray);
            $personSisterStr = Util::arrayToRelation($personSisterArray);

            // 更新当前人物的父/母, 兄弟姐妹
            $sql = "UPDATE $this->TABLE SET father = '$personFatherStr', mother = '$personMotherStr', brother = '$personBrotherStr', sister = '$personSisterStr',update_time = now() WHERE id = '$personId' ";
            $this->pdo->update($sql);

            // 更新目标人物的子女

            if ($person->gender == 1) {
                // 更新儿子
                $toPersonSonArray = $toPerson->son;
                $toPersonSonArray[] = $person->id;
                $toPersonSonStr = Util::arrayToRelation($toPersonSonArray);
                $sql = "UPDATE $this->TABLE SET son = '$toPersonSonStr',update_time = now() WHERE id = '$toPersonId' ";
                $this->pdo->update($sql);

                // 更新目标人物配偶的儿子
                foreach ($toPerson->spouse as $spouseId) {
                    $sql = "UPDATE $this->TABLE SET son = '$toPersonSonStr',update_time = now() WHERE id = '$spouseId' ";
                    $this->pdo->update($sql);
                }
            } else {
                // 更新女儿
                $toPersonDaughterArray = $toPerson->daughter;
                $toPersonDaughterArray[] = $person->id;
                $toPersonDaughterStr = Util::arrayToRelation($toPersonDaughterArray);
                $sql = "UPDATE $this->TABLE SET daughter = '$toPersonDaughterStr',update_time = now() WHERE id = '$toPersonId' ";
                $this->pdo->update($sql);

                // 更新目标人物配偶的女儿
                foreach ($toPerson->spouse as $spouseId) {
                    $sql = "UPDATE $this->TABLE SET daughter = '$toPersonDaughterStr',update_time = now() WHERE id = '$spouseId' ";
                    $this->pdo->update($sql);
                }
            }

            // 更新目标人物子女的兄弟姐妹

            if ($person->gender == 1) {
                $key = 'brother';
            } else {
                $key = 'sister';
            }

            foreach ($toPerson->son as $sonId) {
                $str = '@' . $person->id . '|';
                $sql = "UPDATE $this->TABLE SET $key = CONCAT($key,\"$str\"),update_time = now() WHERE id = '$sonId' ";
                $this->pdo->update($sql);
            }

            foreach ($toPerson->daughter as $daughterId) {
                $str = '@' . $person->id . '|';
                $sql = "UPDATE $this->TABLE SET $key = CONCAT($key,\"$str\"),update_time = now() WHERE id = '$daughterId' ";
                $this->pdo->update($sql);
            }

            // 删除原来留存的父母，兄弟姐妹关系
            // $setParam = REPLACE($setParam,'$from','$to')";
            $from = '@' . $person->id . '|';
            $to = '';
            $parentArray = array_merge($person->father, $person->mother);
            foreach ($parentArray as $parentId) {
                if ($person->gender == 1) {
                    $key = "son";
                } else {
                    $key = "daughter";
                }
                $sql = "UPDATE $this->TABLE SET $key = REPLACE($key, '$from', '$to'),update_time = now() WHERE id = '$parentId' ";

                $this->pdo->update($sql);
            }

            $siblingArray = array_merge($person->sister, $person->brother);
            foreach ($siblingArray as $siblingId) {

                // 排除自己
                if ($siblingId == $person->id) {
                    continue;
                }

                if ($person->gender == 1) {
                    $key = "brother";
                } else {
                    $key = "sister";
                }
                $sql = "UPDATE $this->TABLE SET $key = REPLACE($key, '$from', '$to'),update_time = now() WHERE id = '$siblingId' ";

                $this->pdo->update($sql);
            }

            // 更新��有改动人物的level
            $levelChange = $toPerson->level + 1 - $person->level;
            if ($levelChange != 0) {
                // level发生了改变
                $needChangePersonArray[] = $person->id;

                while (count($needChangePersonArray) > 0) {

                    // 检查toPersonId在不在这个集合内，如果在，则表示两个人物是同一个分支下的
                    // 0 在这里也会判断为false，所以需要使用!==
                    if (array_search($toPersonId, $needChangePersonArray) !== false) {
                        $this->pdo->rollback();
                        return false;
                    }

                    $arrStr = Util::arrayToString($needChangePersonArray);
                    $sql = "UPDATE $this->TABLE SET level = level + $levelChange,update_time = now() WHERE id in ($arrStr) ";

                    $this->pdo->update($sql);

                    $tmpArr = array();
                    $spouseArr = array();

                    foreach ($needChangePersonArray as $changePersonId) {
                        $sql = "SELECT son,daughter,spouse FROM $this->TABLE WHERE id = '$changePersonId' ";
                        $result = $this->pdo->uniqueResult($sql);
                        if ($result != null) {
                            $sons = Util::relationSplit($result['son']);
                            $daughters = Util::relationSplit($result['daughter']);
                            $spouses = Util::relationSplit($result['spouse']);

                            $spouseArr = array_merge($spouseArr, $spouses);
                            $tmpArr = array_merge($tmpArr, $sons, $daughters);
                        }
                    }

                    // 更新spouse的level
                    if (count($spouseArr) > 0) {
                        // 检查toPersonId在不在这个集合内，如果在，则表示两个人物是同一个分支下的
                        if (array_search($toPersonId, $spouseArr) !== false) {
                            $this->pdo->rollback();
                            return false;
                        }
                        $spouseArrStr = Util::arrayToString($spouseArr);
                        $sql = "UPDATE $this->TABLE SET level = level + $levelChange,update_time = now() WHERE id in ($spouseArrStr) ";
                        $this->pdo->update($sql);
                    }

                    $needChangePersonArray = $tmpArr;
                }
            }

            // 更新族群level
            $familyDB = new CDBFamily();
            $familyDB->autoUpdateFamilyLevel($person->familyId);

            $this->pdo->commit();


            return true;

            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            //@codeCoverageIgnoreEnd
        }
    }


    /**
     * 指定兄弟姐妹的排行
     * @param $ranking
     * @param $personId
     * @return mixed
     */
    public function updateBrothersAndSistersRanking($ranking, $personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET ranking = '$ranking',update_time = now() WHERE id = '$personId' AND is_delete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 批量更新兄弟排行
     * @param $rankingArr
     * @param $personIdArr
     * @return mixed
     */
    public function batchUpdateBrothersAndSistersRanking($rankingArr, $personIdArr)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        $personId = $personIdArr['0'];
        $person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '批量更新兄弟排行';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['rankingArr' => $rankingArr, 'personIdArr' => $personIdArr];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        $str = '';
        $personIds = implode(',', $personIdArr);
        foreach ($personIdArr as $key => $personId) {
            $str .= "  WHEN $personId THEN $rankingArr[$key]  ";
        }

        $sql = "UPDATE $this->TABLE  
                SET ranking = CASE id 
                " . $str . "
                END ,
                    update_time = now()
                WHERE id IN ($personIds) AND is_delete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 用户可以设置在该族群中展示的默认名片
     * @param $infoCardId
     * @param $personId
     * @return mixed
     */
    public function bindUserInfoCardInFamily($infoCardId, $personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        //$personId = '';
        $person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '设置在该族群中展示的默认名片';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['infoCardId' => $infoCardId];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        $sql = "UPDATE $this->TABLE SET infoCardId = '$infoCardId',update_time = now() WHERE id = '$personId'";
        return $this->pdo->update($sql);
    }


    /**
     * 通过人物id获取名片内容
     * @param $personId
     * @return mixed
     */
    public function getInfoCardByPersonId($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,remark,
            userId,
            familyId,
            personId,
            name,
            name_en as nameEn,
            country,
            country_name as countryName,
            province,
            province_name as provinceName,
            city,
            city_name as cityName,
            area,
            area_name as areaName,
            town,
            town_name as townName,
            address,
            photo,
            background_img as backgroundImg,
            background_color as backgroundColor,
            background_type as backgroundType,
            company,
            company_en as companyEn,
            gender,
            birthday,
            blood_type as bloodType,
            marital_status as maritalStatus,
            phone,
            qq,
            email,
            company_profile as companyProfile,
            company_url as companyUrl,
            bank_account as bankAccount,
            longitude,latitude,position,
            skills,favorites,jobs,identity,type,is_default as isDefault,permission,ocrcard_img as ocrCardImg,tax_number as taxNumber,imageOrder,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
            
            FROM gener_info_card WHERE id = (SELECT infoCardId FROM $this->TABLE WHERE id='$personId') AND is_delete = 0";

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            //@codeCoverageIgnoreStart
            return null;
            //@codeCoverageIgnoreEnd
        } else {
            return new InfoCard($result);
        }
    }

    /**
     * 根据时间来判断改变的人物列表
     */
    public function getFamilyChangePersonListByTime($familyId, $datetime, $needPermission = false, $needOtherInfo=false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $otherInfoSql = '';
        $otherInfoJoinSql = '';
        if ($needOtherInfo){
            $otherInfoSql = " , tf.branch_name as branchName, tot.trash_field as trashField ";
            $otherInfoJoinSql = " left join $this->TABLE_PERSON_PDF_TITLE tf on tb.id = tf.person_id 
                                  left join $this->TABLE_PERSON_OTHER_INFO tot on tb.id = tot.person_id ";
        }

        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.infoCardId,tb.branchId,tb.familyId,tb.userId,tb.type,tb.name,tb.zpname,
                tb.zi,tb.birthday,tb.gender,tb.photo,tb.remark,tb.share_url as shareUrl,tb.is_dead as isDead,tb.is_adoption as isAdoption,
                tb.dead_time as deadTime,tb.ref_familyId as refFamilyId,tb.ref_personId as refPersonId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                tb.level,tb.confirm,tb.blood_type as bloodType,tb.marital_status as maritalStatus,tb.phone,tb.qq,tb.qrcode,tb.profileText,tb.sideText,
                tb.country,tb.country_name as countryName,tb.province,tb.province_name as provinceName,tb.city,tb.city_name as cityName,tb.area,tb.area_name as areaName,tb.town,tb.town_name as townName,
                tb.address,tb.is_delete as isDelete,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tu.hx_username as hxUsername,tb.ranking, tb.family_index as familyIndex
                    $otherInfoSql
                    FROM $this->TABLE tb 
                    left join $this->TABLE_USER tu on tb.userId = tu.id 
                    $otherInfoJoinSql 
                    WHERE tb.familyId = '$familyId' AND tb.update_time >= '$datetime'";

        $result = $this->pdo->query($sql);

        $personList = array();


        if ($needPermission) {
            $needPermissionSql = "select userId,permissionType as permission from $this->TABLE_NEW_PERMISSION 
                where is_delete = 0 and familyId = $familyId and update_time >= '$datetime'";
            $permissionResult = $this->pdo->query($needPermissionSql);
            $permissionDict = [];
            foreach ($permissionResult as $userPermission) {
                $permissionDict[$userPermission['userId']] = ['permission' => $userPermission['permission']];
            }

            foreach ($result as $i => $personResult) {
                $person = new Person($personResult);
                $parser = new RelationParser($person);
//                $person->father = $parser->getFathers();
//                $person->mother = $parser->getMothers();
//                $person->son = $parser->getSons();
//                $person->daughter = $parser->getDaughters();
//                $person->spouse = $parser->getSpouses();
//                $person->brother = $parser->getBrothers();
//                $person->sister = $parser->getSisters();

                $personPermission = $permissionDict[$person->userId]['permission'];
                $result[$i]['permission'] = $personPermission ? $personPermission : null;
//                $person->permission = $personPermission ? $personPermission : null;
//                $personList[] = $person;
                $result[$i]['father'] = $parser->getFathers();
                $result[$i]['mother'] = $parser->getMothers();
                $result[$i]['son'] = $parser->getSons();
                $result[$i]['daughter'] = $parser->getDaughters();
                $result[$i]['spouse'] = $parser->getSpouses();
                $result[$i]['brother'] = $parser->getBrothers();
                $result[$i]['sister'] = $parser->getSisters();
            }
            return $result;
        }



        foreach ($result as $i => $personResult) {
            $person = new Person($personResult);
            $parser = new RelationParser($person);
//            $person->father = $parser->getFathers();
//            $person->mother = $parser->getMothers();
//            $person->son = $parser->getSons();
//            $person->daughter = $parser->getDaughters();
//            $person->spouse = $parser->getSpouses();
//            $person->brother = $parser->getBrothers();
//            $person->sister = $parser->getSisters();
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
            //                $personList[] = $person;
        }
        return $result;
    }

    /**
     * 加入群体
     * @param $name         群体中展示的姓名
     * @param $groupId      群体id
     * @param $gender       性别
     * @param $photo        照片
     * @param $userId       用户id
     */
    public function joinGroup($name, $groupId, $gender, $photo, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE (name, userId, familyId, gender, photo, share_url, level, create_time, create_by, update_time, update_by) 
        VALUES ('$name', '$userId', '$groupId', '$gender', '$photo', '', '0', now(), '$userId', now(), '$userId')";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取人物所属的分支列表
     * @param $personId 人物id
     * @return array 分支数组
     */
    public function getPersonBranchList($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT branchId FROM $this->TABLE_FAMILY_INNER_BRANCH_PERSON WHERE personId = '$personId' AND isDelete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 根据快照版本获取人物总数
     * @param $familyId 人物id
     * @param $version  版本号
     * @return int 人物总数
     */
    public function getPersonsCountBySnapshotVersion($familyId, $version)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE_PERSON_SNAPSHOT WHERE familyId = '$familyId' AND snapshotVersion = '$version' AND is_delete = '0' ";

        $result = $this->pdo->uniqueResult($sql);

        return $result['count(id)'];
    }

    /**
     * 根据快照版本分页获取封存的人物信息
     * @param $familyId 家族id
     * @param $version  封存的版本号
     * @param $maxId    分页的最大id
     * @param $sinceId  分页的起始id
     * @return array 人物信息数组
     */
    public function getPersonsPagingBySnapshotVersion($familyId, $version, $count, $maxId = 0, $sinceId = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $where = '';

        if ($maxId != 0) {
            $where .= " AND id < $maxId";
        }

        if ($sinceId != 0) {
            $where .= " AND id > $sinceId";
        }

        $sql = "SELECT * FROM $this->TABLE_PERSON_SNAPSHOT WHERE familyId = '$familyId' AND snapshotVersion = '$version' AND is_delete = '0' $where ORDER BY id DESC limit 0, $count ";

        $result = $this->pdo->query($sql);
        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        return $result;
    }

    /**
     * 向家庭中添加人物
     * @param
     */
    public function addPersonForMiniFamily($person, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $corePerson = $this->getPersonById($person->corePersonId);                            //获取Person对象

        $corePersonLevel = $corePerson->level;
        $level = $corePersonLevel;                                //默认是平辈的
        if ($person->relation == $GLOBALS['RELATION_FATHER'] || $person->relation == $GLOBALS['RELATION_MOTHER']) {
            $level--;            //辈份越高,level越小
        } else if ($person->relation == $GLOBALS['RELATION_SON'] || $person->relation == $GLOBALS['RELATION_DAUGHTER']) {
            $level++;            //辈份越低,level越大
        }

        $familyDB = new CDBFamily();
        $family = $familyDB->getFamilyById($corePerson->familyId);
        $memberCount = $family->memberCount;    // 族群中的人物

        /*******同时也要更新家族的level标志**************/
        $changed = FALSE;
        $familyDao = new CDBFamily();
        $familyId = $person->familyId;
        $family = $familyDao->getFamilyById($familyId);
        $minLevel = $family->minLevel;
        $maxLevel = $family->maxLevel;
        if ($level < $minLevel) {
            $minLevel = $level;
            $changed = TRUE;
        }
        if ($level > $maxLevel) {
            $maxLevel = $level;
            $changed = TRUE;
        }

        try {

            //开启事务处理
            if ($needTransaction)
                $this->pdo->beginTransaction();

            if ($changed)
                $familyDao->updateFamilyLevel($person->familyId, $minLevel, $maxLevel);

            /*********家族中的level标志更新完毕*********/


            $personId = $this->addPersonWithLevel($person, $level); //增加要增加的人物
            $records = array();                                                                //需要更新的人物记录数组
            $count = 0;

            switch ($person->relation) {

                case $GLOBALS['RELATION_FATHER']:
                    //添加自己和父亲关系的记录
                    $meFatherRecord = new PersonRecord($person->corePersonId, "father", $personId);
                    $records[$count] = $meFatherRecord;                        //自己和父亲的记录
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    }

                    //更新父亲的儿子和兄弟的父亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $fatherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $fatherSonRecord;
                        $count++;

                        $brotherFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $brotherFatherRecord;
                        $count++;
                    }

                    //更新父亲的女儿和姐妹的父亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $fatherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;

                        $sisterFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $sisterFatherRecord;
                        $count++;
                    }

                    $updateSpouseId = array();

                    //更新父亲的配偶和母亲的配偶
                    foreach ($corePerson->mother as $key => $value) {
                        $fatherSpouseRecord = new PersonRecord($personId, "spouse", $value);
                        $records[$count] = $fatherSpouseRecord;
                        $count++;

                        $motherSpouseRecord = new PersonRecord($value, "spouse", $personId);
                        $records[$count] = $motherSpouseRecord;
                        $count++;

                        //更新配偶的婚姻状态
                        array_push($updateSpouseId, $value);
                    }

                    array_push($updateSpouseId, $personId);
                    $this->updateSpouseStatus($updateSpouseId);
                    break;
                case $GLOBALS['RELATION_MOTHER']:
                    //添加自己和母亲关系的记录
                    $meMotherRecord = new PersonRecord($person->corePersonId, "mother", $personId);
                    $records[$count] = $meMotherRecord;
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $motherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $motherMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $motherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $motherMeRecord;
                        $count++;
                    }


                    //更新母亲的儿子和兄弟的母亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $motherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $motherSonRecord;
                        $count++;

                        $brotherMotherRecord = new PersonRecord($value, "mother", $personId);
                        $records[$count] = $brotherMotherRecord;
                        $count++;
                    }

                    //更新母亲的女儿和姐妹的母亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $motherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $motherDaughterRecord;
                        $count++;

                        $sisterMotherRecord = new PersonRecord($value, "mother", $personId);
                        $records[$count] = $sisterMotherRecord;
                        $count++;
                    }

                    $updateSpouseId = array();

                    //更新母亲的配偶和父亲的配偶
                    foreach ($corePerson->father as $key => $value) {
                        $motherSpouseRecord = new PersonRecord($personId, "spouse", $value);
                        $records[$count] = $motherSpouseRecord;
                        $count++;

                        $fatherSpouseRecord = new PersonRecord($value, "spouse", $personId);
                        $records[$count] = $fatherSpouseRecord;
                        $count++;
                        //更新配偶的婚姻状态
                        array_push($updateSpouseId, $value);
                    }
                    array_push($updateSpouseId, $personId);
                    //更新婚姻状态
                    $this->updateSpouseStatus($updateSpouseId);

                    break;
                case $GLOBALS['RELATION_BROTHER']:
                    //添加自己的兄弟关系的记录
                    $meBrotherRecord = new PersonRecord($person->corePersonId, "brother", $personId);
                    $records[$count] = $meBrotherRecord;
                    $count++;

                    /****根据我的性别判定是兄弟还是姐妹****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $brotherMeRecord = new PersonRecord($personId, "brother", $person->corePersonId);
                        $records[$count] = $brotherMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $brotherMeRecord = new PersonRecord($personId, "sister", $person->corePersonId);
                        $records[$count] = $brotherMeRecord;
                        $count++;
                    }


                    //更新兄弟的父亲和父亲的儿子
                    foreach ($corePerson->father as $key => $value) {
                        $brotherFatherRecord = new PersonRecord($personId, "father", $value);
                        $records[$count] = $brotherFatherRecord;
                        $count++;

                        $fatherSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $fatherSonRecord;
                        $count++;
                    }

                    //更新兄弟的母亲和母亲的儿子
                    foreach ($corePerson->mother as $key => $value) {
                        $brotherMotherRecord = new PersonRecord($personId, "mother", $value);
                        $records[$count] = $brotherMotherRecord;
                        $count++;

                        $motherSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $motherSonRecord;
                        $count++;
                    }

                    //更新兄弟的兄弟和兄弟的兄弟
                    foreach ($corePerson->brother as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $brotherBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $brotherBrotherRecord;
                        $count++;

                        $brotherBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $brotherBrotherRecord;
                        $count++;
                    }

                    //更新兄弟的姐妹和姐妹的兄弟
                    foreach ($corePerson->sister as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $brotherSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $brotherSisterRecord;
                        $count++;

                        $sisterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sisterBrotherRecord;
                        $count++;
                    }

                    break;
                case $GLOBALS['RELATION_SISTER']:
                    //添加自己的姐妹关系的记录
                    $meSisterRecord = new PersonRecord($person->corePersonId, "sister", $personId);
                    $records[$count] = $meSisterRecord;
                    $count++;

                    /****根据我的性别判定是兄弟还是姐妹****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sisterMeRecord = new PersonRecord($personId, "brother", $person->corePersonId);
                        $records[$count] = $sisterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sisterMeRecord = new PersonRecord($personId, "sister", $person->corePersonId);
                        $records[$count] = $sisterMeRecord;
                        $count++;
                    }

                    //更新姐妹的父亲和父亲的女儿
                    foreach ($corePerson->father as $key => $value) {
                        $sisterFatherRecord = new PersonRecord($personId, "father", $value);
                        $records[$count] = $sisterFatherRecord;
                        $count++;

                        $fatherDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;
                    }

                    //更新姐妹的母亲和母亲的女儿
                    foreach ($corePerson->mother as $key => $value) {
                        $sisterMotherRecord = new PersonRecord($personId, "mother", $value);
                        $records[$count] = $sisterMotherRecord;
                        $count++;

                        $motherDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $motherDaughterRecord;
                        $count++;
                    }

                    //更新姐妹的兄弟和兄弟的姐妹
                    foreach ($corePerson->brother as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $sisterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sisterBrotherRecord;
                        $count++;

                        $brotherSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $brotherSisterRecord;
                        $count++;
                    }

                    //更新姐妹的姐妹和姐妹的姐妹
                    foreach ($corePerson->sister as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $sisterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sisterSisterRecord;
                        $count++;

                        $sisterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sisterSisterRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_SPOUSE']:
                    //添加自己和配偶关系的记录
                    $meSpouseRecord = new PersonRecord($person->corePersonId, "spouse", $personId);
                    $records[$count] = $meSpouseRecord;
                    $count++;

                    $spouseMeRecord = new PersonRecord($personId, "spouse", $person->corePersonId);
                    $records[$count] = $spouseMeRecord;
                    $count++;

                    //更新配偶的儿子和儿子的父母
                    foreach ($corePerson->son as $key => $value) {
                        $spouseSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $spouseSonRecord;
                        $count++;

                        /*****根据配偶的性别判断******/
                        if ($person->gender == $GLOBALS['GENDER_MALE']) {
                            $sonFatherRecord = new PersonRecord($value, "father", $personId);
                            $records[$count] = $sonFatherRecord;
                            $count++;
                        } else if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonMotherRecord = new PersonRecord($value, "mother", $personId);
                            $records[$count] = $sonMotherRecord;
                            $count++;
                        }
                    }

                    //更新配偶的女儿和女儿的父母
                    foreach ($corePerson->daughter as $key => $value) {
                        $spouseDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $spouseDaughterRecord;
                        $count++;

                        /*****根据配偶的性别判断******/
                        if ($person->gender == $GLOBALS['GENDER_MALE']) {
                            //@codeCoverageIgnoreStart
                            $daughterFatherRecord = new PersonRecord($value, "father", $personId);
                            $records[$count] = $daughterFatherRecord;
                            $count++;
                            //@codeCoverageIgnoreEnd
                        } else if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                            $daughterMotherRecord = new PersonRecord($value, "mother", $personId);
                            $records[$count] = $daughterMotherRecord;
                            $count++;
                        }
                    }

                    //更新婚姻状态
                    $this->updateSpouseStatus(array($personId, $person->corePersonId));

                    break;
                case $GLOBALS['RELATION_SON']:
                    //添加自己和儿子关系的记录
                    $meSonRecord = new PersonRecord($person->corePersonId, "son", $personId);
                    $records[$count] = $meSonRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sonMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sonMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    }


                    //更新儿子的父或母亲和配偶的儿子
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd

                        $spouseSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $spouseSonRecord;
                        $count++;
                    }

                    //找出我的配偶的配偶  父亲->母亲1（corePerson)->母亲2
                    if (count($corePerson->spouse) == 1) {
                        $mySpouse = $this->getPersonById($corePerson->spouse[0]);
                        foreach ($mySpouse->spouse as $key => $value) {
                            if ($value != $corePerson->id) {  //排除自己
                                //@codeCoverageIgnoreStart
                                /*****根据我的性别判断******/
                                if ($mySpouse->gender == $GLOBALS['GENDER_MALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                }

                                $spouseSonRecord = new PersonRecord($value, "son", $personId);
                                $records[$count] = $spouseSonRecord;
                                $count++;
                                //@codeCoverageIgnoreEnd
                            }
                        }
                    }


                    //更新儿子的兄弟和儿子的兄弟
                    foreach ($corePerson->son as $key => $value) {
                        $sonBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sonBrotherRecord;
                        $count++;

                        $sonBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sonBrotherRecord;
                        $count++;
                    }

                    //更新儿子的姐妹和女儿的兄弟
                    foreach ($corePerson->daughter as $key => $value) {
                        $sonSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sonSisterRecord;
                        $count++;

                        $daughterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_DAUGHTER']:
                    //添加自己和女儿关系的记录
                    $meDaughterRecord = new PersonRecord($person->corePersonId, "daughter", $personId);
                    $records[$count] = $meDaughterRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    }


                    //更新女儿的父或母亲和配偶的女儿
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $daughterSpouseRecord = new PersonRecord($personId, "mother", $value);
                            $records[$count] = $daughterSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $daughterSpouseRecord = new PersonRecord($personId, "father", $value);
                            $records[$count] = $daughterSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd


                        $spouseDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $spouseDaughterRecord;
                        $count++;
                    }

                    //找出我的配偶的配偶  父亲->母亲1（corePerson)->母亲2
                    //TODO 不确定是否正确
                    //@codeCoverageIgnoreStart
                    if (count($corePerson->spouse) == 1) {
                        $mySpouse = $this->getPersonById($corePerson->spouse[0]);
                        foreach ($mySpouse->spouse as $key => $value) {
                            if ($value != $corePerson->id) {  //排除自己
                                /*****根据我的性别判断******/
                                if ($mySpouse->gender == $GLOBALS['GENDER_MALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                }

                                $spouseSonRecord = new PersonRecord($value, "daughter", $personId);
                                $records[$count] = $spouseSonRecord;
                                $count++;
                                //@codeCoverageIgnoreEnd
                            }
                        }
                    }

                    //更新女儿的兄弟和儿子的姐妹
                    foreach ($corePerson->son as $key => $value) {
                        $daughterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;

                        $sonSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sonSisterRecord;
                        $count++;
                    }

                    //更新女儿的姐妹和女儿的姐妹
                    foreach ($corePerson->daughter as $key => $value) {
                        $daughterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $daughterSisterRecord;
                        $count++;

                        $daughterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $daughterSisterRecord;
                        $count++;
                    }
                    break;
            }
            if (!$this->updateRecord($records)) {
                //@codeCoverageIgnoreStart
                if ($needTransaction)
                    $this->pdo->rollback();
                return -1;                //表示添加相关记录时出错
                //@codeCoverageIgnoreEnd
            } else {
                if ($needTransaction)
                    $this->pdo->commit();
                return $personId;
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            if ($needTransaction)
                $this->pdo->rollback();
        }
        return -1;
        //@codeCoverageIgnoreEnd
    }


    /**
     * 以分页的方式查询家族成员
     * @param $familyId 要查询的家族ID
     * @param $maxId 最大id
     * @param $sinceId 起始id
     * @return int 返回人数
     */
    public function getMiniFamilyPersonsPaging($familyId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT t.id,t.same_personId as samePersonId,t.is_adoption as isAdoption,t.name,t.zi,t.remark,t.country,t.type,t.country_name as countryName,t.province ,t.province_name as provinceName,t.city ,t.city_name as cityName,t.area ,t.area_name as areaName,t.town ,t.town_name as townName,t.address,t.birthday,t.phone,t.gender,t.photo,t.share_url as shareUrl,t.is_dead as isDead,t.dead_time as deadTime,t.familyId,
                    t.ref_familyId as refFamilyId,t.father,t.mother,t.son,t.daughter,t.spouse,t.sister,t.brother,
                    t.level,t.userId,tu.username,tu.nickname,tu.hx_username as hxUsername,t.create_by as createBy,t.create_time as createTime,t.update_by as updateBy,t.update_time as updateTime,t.ranking  
                    FROM $this->TABLE t 
                    LEFT JOIN $this->TABLE_USER tu on tu.id = t.userId 
                    WHERE t.familyId = '$familyId'  AND t.is_delete = '0'  ";

        $where = '';

        if ($maxId != 0) {
            $where .= " AND t.id < $maxId ";
        }

        if ($sinceId != 0) {
            $where .= " AND t.id > $sinceId ";
        }

        $sql = $sql . $where . ' ORDER BY t.id desc';


        $result = $this->pdo->query($sql);

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['birthday'] = $person->birthday;
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        return $result;
    }

    /**
     * 插入用户进入族群的来源
     * @param $familyId 族群id
     * @param $personId 进入族群绑定的用户id
     * @param $fromUserId 来源用户id
     * @param $acceptUserId 被邀请的用户id
     * @param $type 1是分享绑定，2是二维码，3是申请，4是邀请， 5是通过活动，问答等分享进入
     * @param $userId 创建记录的用户id
     */
    public function insertPersonBindUserRecord($familyId, $personId, $fromUserId, $acceptUserId, $type, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_PERSON_BIND_USER_RECORD (familyId,personId,fromUserId,acceptUserId,type,
                createTime,createBy,updateTime,updateBy) VALUES ('$familyId','$personId','$fromUserId','$acceptUserId',
                '$type',now(),'$userId',now(),'$userId')";
        $this->pdo->insert($sql);
    }


    /**
     * 添加用户人物标记
     * @param $personId
     * @param $userId
     * @param $remarkType
     */
    public function addUserPersonIdRemark($familyId, $personId, $userId, $remarkType)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO family_user_person_remark 
                  (userId,familyId,personId,remarkType,isDelete,createBy,createTime,updateBy,updateTime)
          VALUES ('$userId','$familyId','$personId','$remarkType',0,'$userId',now(),'$userId',now())";
        return $this->pdo->insert($sql);
    }


    /**
     * 删除用户人物标记
     * @param $remarkId
     * @return mixed
     */
    public function deleteUserPersonRemarkRecord($remarkId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE family_user_person_remark SET isDelete = 1 WHERE id = '$remarkId' AND isDelete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 修改用户标记人物的标记类型
     * @param $remarkId
     * @param $remarkType
     * @param $userId
     * @return mixed
     */
    public function updateUserPersonRemarkType($remarkId, $remarkType, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE family_user_person_remark SET remarkType = '$remarkType',updateBy='$userId',updateTime=now() WHERE id = '$remarkId' AND isDelete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 用户标记的人物总数
     * @param $userId
     * @return mixed
     */
    public function countUserRemarkPersonRecord($userId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) as sum FROM family_user_person_remark WHERE userId = '$userId' AND familyId = '$familyId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        return $result['sum'];
    }

    /**
     * 分页获取用户标记人物
     * @param $userId
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @return mixed
     */
    public function getUserPersonRemarkListPage($familyId, $userId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND upr.id<'$maxId' AND upr.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND upr.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND upr.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT upr.id,upr.userId,upr.familyId,f.name as familyName,upr.personId,upr.remarkType,upr.createBy,upr.createTime,upr.updateBy,upr.updateTime ,
        p.name,p.zpname,p.zi,p.birthday,p.gender,p.photo,p.remark,p.share_url
        FROM  family_user_person_remark as upr
        LEFT JOIN gener_person as p ON upr.personId = p.id 
        LEFT JOIN gener_family as f ON upr.familyId = f.id
         WHERE upr.userId = '$userId' AND upr.familyId = '$familyId'  $where AND upr.isDelete = 0 LIMIT $count";
        return $this->pdo->query($sql);
    }

    /**
     * 检查标记是否是该用户创建的
     * @param $remarkId
     * @param $userId
     * @return bool
     */
    public function checkUserCreatedUserRemarkPerson($remarkId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) as sum FROM family_user_person_remark WHERE id='$remarkId' AND userId = '$userId' AND isDelete = 0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['sum'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 添加人物标记颜色
     * @param $userId
     * @param $familyId
     * @param $personId
     * @param $color
     * @return mixed
     */
    public function addUserRemarkPersonColor($userId, $familyId, $personId, $color)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO family_person_color (userId,familyId,personId,color,isDelete,createBy,createTime,updateBy,updateTime)
              VALUES ('$userId','$familyId','$personId','$color',0,'$userId',now(),'$userId',now())";
        return $this->pdo->insert($sql);
    }

    /**
     * 用户编辑人物标记颜色
     * @param $colorId
     * @param $color
     * @param $userId
     * @return mixed
     */
    public function updateUserRemarkPersonColor($colorId, $color, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE family_person_color SET color='$color',updateBy='$userId',updateTime=now()
        WHERE id = '$colorId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 用户删除人物标记颜色
     * @param $colorId
     * @return mixed
     */
    public function deleteUserRemarkPersonColor($colorId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE family_person_color SET isDelete = 1 WHERE id ='$colorId' AND isDelete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 检查用户是否创建该人物的标记颜色
     * @param $colorId
     * @param $userId
     * @return bool
     */
    public function checkUserCreatedUserRemarkPersonColor($colorId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) as sum FROM family_person_color WHERE id='$colorId' AND userId = '$userId' AND isDelete = 0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['sum'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 分页获取用户标记的人物颜色
     * @param $familyId
     * @param $userId
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @return mixed
     */
    public function getUserPersonRemarkColorListPage($familyId, $userId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND upr.id<'$maxId' AND upr.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND upr.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND upr.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT upr.id,upr.userId,upr.familyId,f.name as familyName,upr.personId,upr.color,upr.createBy,upr.createTime,upr.updateBy,upr.updateTime ,
        p.name,p.zpname,p.zi,p.birthday,p.gender,p.photo,p.remark,p.share_url
        FROM  family_person_color as upr
        LEFT JOIN gener_person as p ON upr.personId = p.id 
        LEFT JOIN gener_family as f ON upr.familyId = f.id
         WHERE upr.userId = '$userId' AND upr.familyId = '$familyId'  $where AND upr.isDelete = 0 LIMIT $count";
        return $this->pdo->query($sql);
    }


    /**
     * 标记人物颜色总数
     * @param $userId
     * @param $familyId
     * @return mixed
     */
    public function countUserRemarkPersonColorRecord($userId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) as sum FROM family_person_color WHERE userId = '$userId' AND familyId = '$familyId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        return $result['sum'];
    }

    /**
     * 向家族中添加带有层次级别的人物,添加时根据性别，向其姐妹或者兄弟字段中更新自己的id
     * 只用于复制家族分支时
     * @param $person 该人物
     * @param $level    该人物的级别
     * @return  int  人物的id或者失败返回-1
     */
    private function addPersonForCopy($person, $level, $type = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $userId = $GLOBALS['userId'];

        $shareUrl = md5($person->name . Util::generateRandomCode(24));    //生成唯一share_url的code

        if ($person->birthday == null || $person->birthday == '') {
            $person->birthday = "NULL";
        } else {
            $person->birthday = "'$person->birthday'";
        }
        if ($person->deadTime == null || $person->deadTime == '') {
            $person->deadTime = "NULL";
        } else {
            $person->deadTime = "'$person->deadTime'";
        }
        if ($person->infoCardId == null || $person->infoCardId == '') {
            $person->infoCardId = 'NULL';
        } else {
            $person->infoCardId = "'$person->infoCardId'";
        }
        if ($person->confirm == null || $person->confirm == '') {
            $person->confirm = 'NULL';
        } else {
            $person->confirm = "'$person->confirm'";
        }

        //sister,brother,
        if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
            $brothersql = " '@$person->id|', '',";
        } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
            $brothersql = " '', '@$person->id|',";
        }

        //原person对象没有的 ： infoCardId， confirm, qrcode
        //same_personId,father,mother,son,daughter,spouse,
        //sister,brother,
        $sql = "INSERT INTO $this->TABLE(
            id, zi,zpname,remark,branchId,name,type,
            profileText,sideText,country,country_name,province,province_name,
            city,city_name,area,area_name,town,town_name,address,
            birthday,gender,photo,share_url,is_dead,dead_time,level,
            infoCardId,is_delete,is_adoption,ref_familyId,ref_personId,
            sister,brother,
            confirm,blood_type,marital_status,phone,qq,qrcode,
            familyId,userId,ranking,create_by,create_time,update_by,update_time            
            )
       VALUES(
           '$person->id', '$person->zi','$person->zpname','$person->remark','$person->branchId','$person->name','$type',
           '$person->profileText','$person->sideText','$person->country','$person->countryName','$person->province','$person->provinceName',
           '$person->city','$person->cityName','$person->area','$person->areaName','$person->town','$person->townName','$person->address',
           $person->birthday,'$person->gender','$person->photo','$shareUrl','$person->isDead',$person->deadTime,'$level',
           $person->infoCardId, '0', '$person->isAdoption', '$person->refFamilyId', '$person->refPersonId',
           $brothersql
           $person->confirm, '$person->bloodType', '$person->maritalStatus', '$person->phone', '$person->qq', '$person->qrcode',
           '$person->familyId','$person->userId','$person->ranking','$userId',now(),'$userId',now()
           )";

        $res = $this->pdo->insert($sql);

        if ($res >= 0) {        //返回插入人物的id,失败则返回-1
            $id = $person->id;
        } else {
            $id = -1;
        }

        return $id;

        //更新家族成员数 不在这里更新人数 统一在插入后更新人数
    }

    /**
     * 记录人物操作日志
     */
    public function recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg = NULL)
    {
        $this->init();
        $ip = Util::getRequestIp();
        $port = Util::getRequestPort();
        $tableName = 'gener_person_option_log';
        $personId = intval($personId);
        $familyId = intval($familyId);
        $option_user = intval($option_user);
        if ($option_user == 0 && isset($GLOBALS['userId'])) {
            $option_user = $GLOBALS['userId'];
        }
        $sql = "INSERT INTO $tableName (
            personId, familyId, function_name, old_data, option_detail, option_user, record_time, update_time, other_msg, option_ip, option_port
            ) VALUES (
            '$personId', '$familyId', '$function_name', '$old_data', '$option_detail', '$option_user', now(), now(), '$other_msg', '$ip', '$port'
            )";
        return $this->pdo->insert($sql);
    }

    /**
     * 获取子嗣的排行
     */
    public function getChildrenRanking($personId, $theSameLevel = false, $isDelete = 0, $needId = false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //        $sql = "select ranking from $this->TABLE where father = '@$personId|' and is_delete = 0";
        if (!$theSameLevel) {
            $sql = "select son,daughter from $this->TABLE where id = '$personId' and is_delete = $isDelete";
            $queryResult = $this->pdo->uniqueResult($sql);
            $person = new Person($queryResult);
            $parser = new RelationParser($person);
            $personId = $parser->getSons();
            $personId = array_merge($personId, $parser->getDaughters());
        } else {
            $sql = "select brother,sister from $this->TABLE where id = '$personId' and is_delete = $isDelete";
            $queryResult = $this->pdo->uniqueResult($sql);
            $person = new Person($queryResult);
            $parser = new RelationParser($person);
            $personId = $parser->getBrothers();
            $personId = array_merge($personId, $parser->getSisters());
        }

        if ($personId) {

            $idStr = "";
            foreach ($personId as $id) {
                $idStr = $idStr . $id . ",";
            }

            $sql = "select id,ranking from $this->TABLE where is_delete = $isDelete and id in (" . substr($idStr, 0, -1) . ") order by ranking";

            return $needId ? $this->pdo->query($sql) : array_column($this->pdo->query($sql), 'ranking');
        }
        return [];
    }

    #v3
    #v3
    #v3


    /**
     * 向家族中添加人物,这个方法仅在创建家族时调用
     * @param $userId   该人物对应的用户id
     * @param $familyId 该人物所属于的家族
     * @param $name     该人物的姓名
     * @param $gender   该人物的性别
     * @param $address  该人物的住址
     * @param $birthday 该人物的生日
     * @param $photo    该人物的照片
     * @return int   人物的id或者失败返回-1
     */
    public function addPersonWithNoTransactionV3($familyId, $photo, $userId = 0, $name = '', $gender = 1, $address = '', $birthday = '')
    {   //创建虚拟人物
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $userId_t = $GLOBALS['userId'];
        $shareUrl = md5($name . Util::generateRandomCode(24));    //生成唯一share_url的code

        $personId = Util::getNextId();

        $sql = "";
        if ($birthday === '' || $birthday === NULL) {
            $sql = "INSERT INTO $this->TABLE
                (id, name,address,birthday,gender,photo,share_url,level,familyId,userId,confirm,create_by,create_time,update_by,update_time)
                VALUES
                ('$personId', '$name','$address',NULL,'$gender','$photo','$shareUrl','0','$familyId','$userId','0','$userId_t',now(),'$userId',now())";
        } else {
            $sql = "INSERT INTO $this->TABLE
                (id, name,address,birthday,gender,photo,share_url,level,familyId,userId,confirm,create_by,create_time,update_by,update_time)
                VALUES
                ('$personId', '$name','$address','$birthday','$gender','$photo','$shareUrl','0','$familyId','$userId','0','$userId_t',now(),'$userId',now())";
        }
        if ($this->pdo->insert($sql) >= 0) {        //返回插入人物的id,失败则返回-1
            $id = $personId;
        } else {
            $id = -1;
        }

        //插入成功,更新自己的sister和brother字段
        if ($id > 0) {
            if ($gender == $GLOBALS['GENDER_FEMALE']) {
                $updateSql = "UPDATE $this->TABLE SET sister = '@$id|',update_time = now() WHERE id = $id";
            } else if ($gender == $GLOBALS['GENDER_MALE']) {
                $updateSql = "UPDATE $this->TABLE SET brother = '@$id|',update_time = now() WHERE id = $id";
            } else {
                return false;
            }
            $this->pdo->update($updateSql);
        }
        //更新家族成员数
        $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now()  WHERE id = '$familyId'";
        $this->pdo->update($memberCountSql);

        return $id;
    }

    /**
     * 检查人物是否存在该家族或人物与父亲的关系是否存在
     * @param $name 真实姓名
     * @param $fatherName    人物父亲的姓名
     * @return  bool  是否存在，或者存在关系
     */
    public function checkPersonOrFather($name, $fatherName, $familyId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "select id,father from $this->TABLE where familyId = $familyId and name = '$name' and is_delete = 0";

        if (!$fatherName) {
            return boolval($this->pdo->uniqueResult($sql));
        }

        $personResult = $this->pdo->query($sql);
        if (!$personResult) {
            return false;
        }

        //        $personIds = array_column($personResult,'id');
        $fatherIds = array_map(function ($v) {
            $id = Util::idSplit($v);
            return $id ? $id[0] : 0;
        }, array_column($personResult, 'father'));

        $sql = "select id from $this->TABLE where familyId = $familyId and name = '$fatherName' and is_delete = 0";

        return boolval(array_intersect(
            array_column($this->pdo->query($sql), 'id'),
            $fatherIds
        ));
    }

    /**
     * 获取兄弟姐妹的排行
     * @param $personId    人物id
     * @return  array  兄弟姐妹id数组
     */
    public function getSiblingRelationship($personId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "select brother,sister from $this->TABLE where id = $personId and is_delete = 0 limit 1";

        $brothersAndSisters = $this->pdo->uniqueResult($sql);
        $brothers = Util::idSplit($brothersAndSisters['brother']);
        $sisters = Util::idSplit($brothersAndSisters['sister']);

        return array_merge($brothers, $sisters);
    }

    /**
     * 修改兄弟姐妹的排行
     * @param $personId    人物id
     * @return  array  兄弟姐妹信息数组
     */
    public function getSiblingRelationshipInfo($personId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $ids = $this->getSiblingRelationship($personId);

        if (!$ids) {
            return [];
        }
        $sql = "select id,photo,name,ranking,gender from $this->TABLE where is_delete = 0 and id in (" . implode(',', $ids) . ") order by ranking";

        return $this->pdo->query($sql);
    }

    /**
     * 修改兄弟姐妹的排行
     * @param $data    更新数组包含id，ranking
     * @param $peopleList    兄弟姐妹数组
     * @return  int  是否存在，或者存在关系
     */
    public function changePeopleRanking($data, $people, $personId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            $relationshipId = $this->getSiblingRelationship($personId);

            if (array_diff($relationshipId, $people) || array_diff($people, $relationshipId) || sizeof($relationshipId) !== sizeof($people)) {
                return -1;
            }
            $sql = "select id,family_index as familyIndex from $this->TABLE where is_delete = 0 and id in ( " . implode(',', $relationshipId) . " ) order by family_index ";
            $siblingPeople = $this->pdo->query($sql);

            $minIndex = $siblingPeople[0]['familyIndex'];

            $person = $this->getPersonObjectById($personId);
            $familyId = $person->familyId;
            $personLastIndex = $this->getLastPersonFamilyIndexV3($familyId, $person->level - 1, $minIndex - 1);
            $personDict = [];
            //            $siblingPeopleIds = [];
            $siblingPeopleLength = count($siblingPeople);

            foreach ($siblingPeople as $k => $v) {
                //                $siblingPeopleIds[] = $v['id'];
                if ($k >= $siblingPeopleLength - 1) {
                    $personDict[$v['id']] = [$v['familyIndex'], $personLastIndex + 1];
                    continue;
                }
                $personDict[$v['id']] = [$v['familyIndex'], $siblingPeople[$k + 1]['familyIndex']];
            }


            usort($data, function ($a, $b) {
                if ($a['ranking'] === $b['ranking']) {
                    return 0;
                }
                return $a['ranking'] > $b['ranking'] ? 1 : -1;
            });

            $bigNum = 10000000;
            # 先将要修改的familyIndex隔离
            $sql = "update $this->TABLE set family_index = family_index - $bigNum where familyId = $familyId and is_delete = 0 and family_index between $minIndex and $personLastIndex";
            $updateIndex = $this->pdo->update($sql);
            $oldIndex = $minIndex;
            foreach ($data as $k => $v) {
                $newIndex = $personDict[$v['id']][0];
                $newLastIndex = $personDict[$v['id']][1];
                $difference = $bigNum + $oldIndex - $newIndex;
                $sql = "update $this->TABLE set family_index = family_index + $difference where familyId = $familyId and is_delete = 0 
                        and family_index >= ($newIndex - $bigNum) and family_index < ($newLastIndex - $bigNum)";
                $this->pdo->update($sql);
                $oldIndex = $newLastIndex + $oldIndex - $newIndex;
            }


            $sql = "update $this->TABLE set ranking = (CASE id ";

            foreach ($data as $k => $v) {
                $sql .= " when " . $v['id'] . " then " . $v['ranking'];
            }

            $sql .= " END), update_time = now() where is_delete = 0 and id in (" . implode(',', $people) . ")";
            //            print_r($sql);
            //            var_dump(sizeof($data));

            $result = $this->pdo->update($sql);

            //        $this->pdo->commit();
            //        return $result;

            //            if ($result == sizeof($data)) {
            //                $this->pdo->commit();
            //                return $result;
            //            }

            $this->pdo->commit();
            return $result;
            //            $this->pdo->rollback();
            //            return 0;

            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            //@codeCoverageIgnoreEnd
            return -2;
        }
    }

    /**
     * 获取用来选择过继的嗣父
     * @param $familyId    int
     * @param $level    int
     * @param $page    int
     * @param $pageSize    int
     * @param $name    int
     * @return  array  人物信息数组
     */
    public function getPersonByLevelForAdopation($familyId, $person, $page, $pageSize, $name, $showNaturalParent = false, $showAdoption = true)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($page - 1) * $pageSize;

        $fatherId = implode(',', $person->father);
        $motherId = implode(',', $person->mother);
        if ($fatherId) {
            $sql = "select id from $this->TABLE where id in ($fatherId) and type = 1 and is_delete = 0";
            $father = $this->pdo->query($sql);
        } else {
            $father = [];
        }

        if ($motherId) {
            $sql = "select id from $this->TABLE where id in ($motherId) and type = 1 and is_delete = 0  ";
            $mother = $this->pdo->query($sql);
        } else {
            $mother = [];
        }
        //        if (!$mother && !$father) {
        //            return [
        //                'page' => 1,
        //                'count' => 0,
        //                'data' => [],
        //            ];
        //        }

        $parent = $father ? $father[0]['id'] : $mother[0]['id'];

        //        $sql = "select brother,sister from $this->TABLE where id = $parent limit 1";
        $sql = "select id from $this->TABLE where familyId = $person->familyId and level = $person->level - 1 and type = 1 and is_delete = 0 ";
        $people = $this->pdo->query($sql);
        $people = array_column($people, 'id');
        //        $sister = Util::idSplit($people['sister']);
        //        $brother = Util::idSplit($people['brother']);
        //        $people = array_merge($brother, $sister);


        if (!$showAdoption) {
            $adoptionParent = [];
            if ($person->isAdoption != 0) {
                $TABLE_ADOPTION = 'gener_person_adoption';
                $sql = "select father,mother from $this->TABLE where is_delete = 0 and id in 
            (select src_personId from $TABLE_ADOPTION where is_delete = 0 and dest_personId = $person->id) or id = $person->id";
                $adoptions = $this->pdo->query($sql);
                foreach ($adoptions as $parent) {
                    $father = Util::idSplit($parent['father'])[0];
                    if ($father) {
                        $sql = "select id,type,name from $this->TABLE where is_delete = 0 and id = $father limit 1";
                        $fatherInfo = $this->pdo->uniqueResult($sql);
                        if ($fatherInfo['type'] == 1) {
                            $adoptionParent[] = $fatherInfo['id'];
                            continue;
                        }
                    }
                    $mother = Util::idSplit($parent['mother'])[0];
                    if ($mother) {
                        $sql = "select id,type,name from $this->TABLE where is_delete = 0 and id = $mother limit 1";
                        $motherInfo = $this->pdo->uniqueResult($sql);
                        if ($motherInfo['type'] == 1) {
                            $adoptionParent[] = $motherInfo['id'];
                            continue;
                        }
                    }
                }
            }
            $people = array_values(array_filter($people, function ($v) use ($adoptionParent) {
                return in_array($v, $adoptionParent) ? false : true;
            }));
        }
        if (!$showNaturalParent) {
            $people = array_values(array_filter($people, function ($v) use ($parent) {
                return $v == $parent ? false : true;
            }));
        }
        if (!$people) {
            return [
                'page' => 1,
                'count' => 0,
                'data' => [],
            ];
        }

        $sql = "select SQL_CALC_FOUND_ROWS id,photo,name,ranking,father as fatherId,level,gender from $this->TABLE where is_delete = 0 and id in ( " . implode(',', $people) . " ) and type = 1 "
            . ($name ? "  and name like '%$name%'" : "") . " order by create_time limit $offset,$pageSize";
        $sqlForCount = "SELECT FOUND_ROWS() as count";

        $people = $this->pdo->query($sql);
        $count = $this->pdo->uniqueResult($sqlForCount)['count'];

        $data = $this->getCloseRelativePeople($familyId, $people, "fatherId", "father", $isList = false);

        return [
            'page' => $page,
            'count' => $count,
            'data' => $data,
        ];
    }

    /**
     * 检查人物是否存在
     * @param $personId    人物id
     * @return  array  人物信息
     */
    public function getPersonV3($personId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "select id, father,name,ranking,photo,family_index as familyIndex,userId,familyId,level from $this->TABLE where is_delete = 0 and id = $personId limit 1";

        $result = $this->pdo->uniqueResult($sql);
        if (!$result){
            return [];
        }

        $result['father'] = Util::idSplit($result['father']);

        return $result;
    }

    /**
     * 检查人物是否存在
     * @param $familyId
     * @param $userId
     * @return  array  人物信息
     */
    public function getPersonByUserAndFamily($familyId, $userId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "select id, father,name,ranking,photo,family_index as familyIndex,userId,familyId,level from $this->TABLE 
                where is_delete = 0 and familyId = $familyId and userId = $userId limit 1";

        $result = $this->pdo->uniqueResult($sql);

        $result['father'] = Util::idSplit($result['father']);

        return $result;
    }

    /**
     * 定位至列表模式的具体页码以及返回数据
     * @param $personId 人物id
     * @param $familyId 家族id
     * @return  array  结果数组
     */
    public function getPersonPositionBylevel($personId, $familyId, $pageSize)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,level,spouse,type  from $this->TABLE where id = $personId and is_delete = 0 limit 1;";

        $personLevel = $this->pdo->uniqueResult($sql);

        if (!$personLevel) {
            #查无此人，返回第一页数据
            $sqlForFirst = "select count(id) from $this->TABLE where familyId = $familyId and is_delete = 0 and type = 1 and
                            level = (SELECT min_level from $this->TABLE_FAMILY where id = $familyId and is_delete=0 limit 1) ";
            list($id, $index, $length, $msg) = [1, 1, $this->pdo->uniqueResult($sqlForFirst), 1];
        } else {
            if ($personLevel['type'] == 2) {
                $personId = Util::idSplit($personLevel['spouse'])[0];
            }
            $level = $personLevel['level'];
            //            print_r($personLevel);
            $sql = "SELECT * FROM 
                    (SELECT id,@rowno := @rowno + 1 as num, 
                        IF(id=$personId,(SELECT count(id) count from $this->TABLE 
                        where familyId = $familyId and is_delete = 0 and level = '$level' and type = 1),0) as result from $this->TABLE g,
                    (SELECT @rowno := 0) r WHERE  familyId = $familyId and is_delete = 0 and level = '$level' and type = 1 order by family_index) 
                as temp where temp.result != 0";
            //            print_r($sql."\n");
            $position = $this->pdo->uniqueResult($sql);


            if (!$position) {
                #查无此人，返回第一页数据
                $sqlForFirst = "select count(id) from $this->TABLE where familyId = $familyId and is_delete = 0 and type = 1 and
                            level = (SELECT min_level from $this->TABLE_FAMILY where id = $familyId and is_delete=0 limit 1) ";
                list($id, $index, $length, $msg) = [1, 1, $this->pdo->uniqueResult($sqlForFirst)['count'], 1];
            } else {
                $index = $position['num'];
                $length = $position['result'];
                $msg = 0;
            }
        }


        $page = intval(ceil($index / $pageSize));
        $offset = intval($page - 1) * $pageSize;

        $sql = "SELECT t.id as id,same_personId as samePersonId,is_adoption as isAdoption,t.name as name,photo,t.userId as userId,profileText,IF(t.userId=0,0,1) as isBind,level,zi,share_url as shareUrl,
                sideText,burialPlace,birthPlace,address,phone,spouse as spouseId,father as fatherId,mother as motherId,gender,ranking,marital_status as maritalStatus, birthday ,is_dead as isDead,bicolor,
                zpname, dead_time as deadTime, tfu.permissionType as permission,isNotMain,family_index as familyIndex,province, province_name as provinceName,city,city_name as cityName, area, area_name as areaName,
                ifnull(occupation,0) as occupation,ifnull(education,0) as education, ifnull(political_status,0) as politicalStatus,
                ifnull(birth_province,0) as birthProvince,ifnull(birth_province_name,'') as birthProvinceName, ifnull(birth_city,0) as birthCity,
                ifnull(birth_city_name,'') as birthCityName,ifnull(birth_area,0) as birthArea, ifnull(birth_area_name,'') as birthAreaName,ifnull(burial_province,0) as burialProvince,ifnull(burial_province_name,'') as burialProvinceName, 
                ifnull(burial_city,0) as burialCity,ifnull(burial_city_name,'') as burialCityName, ifnull(burial_area,0) as burialArea, ifnull(burial_area_name,'') as burialAreaName  
                FROM $this->TABLE t left join (SELECT * from  $this->TABLE_NEW_PERMISSION where is_delete = 0) tfu 
                on t.userId = tfu.userId and t.familyId =tfu.familyId 
                left join (select * from $this->TABLE_PERSON_OTHER_INFO where is_delete = 0) ti on ti.person_id = t.id
                where t.is_delete = 0 and t.familyId = $familyId and level = '$level' and type = 1  order by t.family_index limit $offset,$pageSize";
        //        print_r($sql);
        $people = $this->pdo->query($sql);

        if (!$people) {
            return [
                'page' => $page,
                'count' => $length,
                'data' => $people,
                'msg' => $msg,
                'level' => $level
            ];
        }

        $fathersId = [];
        $spousesId = [];
        $mothersId = [];

        foreach ($people as $person => $value) {
            $fatherId = Util::idSplit($value['fatherId']);
            $spouseId = Util::idSplit($value['spouseId']);
            $motherId = Util::idSplit($value['motherId']);
            $people[$person]['fatherId'] = $fatherId;
            $people[$person]['spouseId'] = $spouseId;
            $people[$person]['motherId'] = $motherId;

            if ($fatherId) {
                $fathersId = array_merge($fathersId, $fatherId);
            }
            if ($spouseId) {
                $spousesId = array_merge($spousesId, $spouseId);
            }
            if ($motherId) {
                $mothersId = array_merge($mothersId, $motherId);
            }
        }

        $fatherIdList = array_values(array_unique($fathersId));
        $spouseIdIdList = array_values(array_unique($spousesId));
        $motherIdList = array_values(array_unique($mothersId));

        if ($fatherIdList) {
            $sqlForFather = "select id,name from $this->TABLE where  is_delete = 0 and familyId = $familyId 
                            and id in (" . implode(',', $fatherIdList) . ")";
            $fathers = $this->pdo->query($sqlForFather);
        } else {
            $fathers = [];
        }
        if ($spouseIdIdList) {
            $sqlForSpouse = "select id,name from $this->TABLE where  is_delete = 0 and familyId = $familyId 
                            and id in (" . implode(',', $spouseIdIdList) . ")";
            $spouses = $this->pdo->query($sqlForSpouse);
        } else {
            $spouses = [];
        }
        if ($motherIdList) {
            $sqlForMother = "select id,name from $this->TABLE where  is_delete = 0 and familyId = $familyId 
                            and id in (" . implode(',', $motherIdList) . ")";
            $mothers = $this->pdo->query($sqlForMother);
        } else {
            $mothers = [];
        }

        foreach ($people as $k => $v) {
            if ($v['fatherId']) {
                $people[$k]['father'] = [];
                foreach ($v['fatherId'] as $fid) {
                    $f = array_values(array_filter($fathers, function ($x) use ($fid) {
                        if ($x['id'] === $fid) {
                            return true;
                        }
                        return false;
                    }));
                    $people[$k]['father'] = array_merge($people[$k]['father'], $f);
                }
            } else {
                $people[$k]['father'] = [];
            }

            if ($v['spouseId']) {
                $people[$k]['spouse'] = [];
                foreach ($v['spouseId'] as $sid) {
                    $s = array_values(array_filter($spouses, function ($x) use ($sid) {
                        if ($x['id'] === $sid) {
                            return true;
                        }
                        return false;
                    }));
                    $people[$k]['spouse'] = array_merge($people[$k]['spouse'], $s);
                }
            } else {
                $people[$k]['spouse'] = [];
            }

            if ($v['motherId']) {
                $people[$k]['mother'] = [];
                foreach ($v['motherId'] as $mid) {
                    $m = array_values(array_filter($mothers, function ($x) use ($mid) {
                        if ($x['id'] === $mid) {
                            return true;
                        }
                        return false;
                    }));
                    $people[$k]['mother'] = array_merge($people[$k]['mother'], $m);
                    //                    $people[$k]['mother'] = $s[0]['name'];
                }
            } else {
                $people[$k]['mother'] = [];
            }
        }

        return [
            'page' => $page,
            'count' => $length,
            'data' => $people,
            'msg' => $msg,
            'level' => $level
        ];
    }

    /**
     * 按世代分页获取家族人物列表，
     * @param $familyId
     * @param array 人物数组
     */
    public function getFamilyPersonListByLevelV3($familyId, $level, $page, $pageSize, array $data)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = intval($page - 1) * $pageSize;
        //        $sqlForCount = "SELECT count(t.id) count from $this->TABLE t left join $this->TABLE_NEW_PERMISSION tfu on t.userId = tfu.userId where t.is_delete = 0 and familyId = $familyId and level = '$level' and type = 1";
        $sql = "SELECT SQL_CALC_FOUND_ROWS t.id as id,same_personId as samePersonId,is_adoption as isAdoption,t.name as name,photo,t.userId as userId,profileText,IF(t.userId=0,0,1) as isBind,level,zi,
                sideText,burialPlace,birthPlace,address,phone,spouse as spouseId,father as fatherId,mother as motherId,gender,ranking,marital_status as maritalStatus, birthday ,is_dead as isDead,isNotMain,family_index as familyIndex,bicolor,
                zpname, dead_time as deadTime, tfu.permissionType as permission,share_url as shareUrl , province, province_name as provinceName,city,city_name as cityName, area, area_name as areaName,
                ifnull(occupation,0) as occupation,ifnull(education,0) as education, ifnull(political_status,0) as politicalStatus, 
                ifnull(birth_province,0) as birthProvince,ifnull(birth_province_name,'') as birthProvinceName, ifnull(birth_city,0) as birthCity,
                ifnull(birth_city_name,'') as birthCityName,ifnull(birth_area,0) as birthArea, ifnull(birth_area_name,'') as birthAreaName,ifnull(burial_province,0) as burialProvince,ifnull(burial_province_name,'') as burialProvinceName, 
                ifnull(burial_city,0) as burialCity,ifnull(burial_city_name,'') as burialCityName, ifnull(burial_area,0) as burialArea, ifnull(burial_area_name,'') as burialAreaName  
                FROM $this->TABLE t left join (SELECT * from  $this->TABLE_NEW_PERMISSION where is_delete = 0) tfu on t.userId = tfu.userId and t.familyId =tfu.familyId 
                left join (select * from $this->TABLE_PERSON_OTHER_INFO where is_delete = 0) ti on ti.person_id = t.id 
                where t.is_delete = 0 and t.familyId = $familyId and level = '$level' and type = 1";
        $sqlForCount = "SELECT FOUND_ROWS() as count";

        if ($data) {
            foreach ($data as $k => $v) {
                if ($k === 'isBind') {
                    $sql .= " having $k = $v ";
                    continue;
                }
                if ($k === 'name') {
                    $sql .= " and t.$k like '%$v%' ";
                    continue;
                }
                $sql .= " and  $k = $v ";
            }
        }

        $sql .= " order by t.family_index limit $offset,$pageSize";
        //        print_r($sql);
        $people = $this->pdo->query($sql);
        $length = $this->pdo->uniqueResult($sqlForCount)['count'];

        //        if ($page * $pageSize > ($length + $pageSize)){
        //            $offset = 0;
        //            $page = 1;
        //        }

        if (!$people) {
            return [
                'page' => $page,
                'count' => $length,
                'data' => $people,
                'msg' => 0,
                'level' => $level
            ];
        }

        $fathersId = [];
        $spousesId = [];
        $mothersId = [];

        foreach ($people as $person => $value) {
            $fatherId = Util::idSplit($value['fatherId']);
            $spouseId = Util::idSplit($value['spouseId']);
            $motherId = Util::idSplit($value['motherId']);
            $people[$person]['fatherId'] = $fatherId;
            $people[$person]['spouseId'] = $spouseId;
            $people[$person]['motherId'] = $motherId;

            if ($fatherId) {
                $fathersId = array_merge($fathersId, $fatherId);
            }
            if ($spouseId) {
                $spousesId = array_merge($spousesId, $spouseId);
            }
            if ($motherId) {
                $mothersId = array_merge($mothersId, $motherId);
            }
        }

        $fatherIdList = array_values(array_unique($fathersId));
        $spouseIdIdList = array_values(array_unique($spousesId));
        $motherIdList = array_values(array_unique($motherId));

        if ($fatherIdList) {
            $sqlForFather = "select id,name from $this->TABLE where  is_delete = 0 and familyId = $familyId 
                            and id in (" . implode(',', $fatherIdList) . ")";
            $fathers = $this->pdo->query($sqlForFather);
        } else {
            $fathers = [];
        }
        if ($spouseIdIdList) {
            $sqlForSpouse = "select id,name from $this->TABLE where  is_delete = 0 and familyId = $familyId 
                            and id in (" . implode(',', $spouseIdIdList) . ")";
            $spouses = $this->pdo->query($sqlForSpouse);
        } else {
            $spouses = [];
        }
        if ($motherIdList) {
            $sqlForMother = "select id,name from $this->TABLE where  is_delete = 0 and familyId = $familyId 
                            and id in (" . implode(',', $motherIdList) . ")";
            $mothers = $this->pdo->query($sqlForMother);
        } else {
            $mothers = [];
        }

        foreach ($people as $k => $v) {
            $people[$k]['father'] = [];
            if ($v['fatherId']) {
                foreach ($v['fatherId'] as $fid) {
                    $f = array_values(array_filter($fathers, function ($x) use ($fid) {
                        if ($x['id'] === $fid) {
                            return true;
                        }
                        return false;
                    }));
                    $people[$k]['father'] = array_merge($people[$k]['father'], $f);
                }
            } else {
                $people[$k]['father'] = [];
            }

            if ($v['spouseId']) {
                $people[$k]['spouse'] = [];
                foreach ($v['spouseId'] as $sid) {
                    $s = array_values(array_filter($spouses, function ($x) use ($sid) {
                        if ($x['id'] === $sid) {
                            return true;
                        }
                        return false;
                    }));
                    $people[$k]['spouse'] = array_merge($people[$k]['spouse'], $s);
                }
            } else {
                $people[$k]['spouse'] = [];
            }

            if ($v['motherId']) {
                $people[$k]['mother'] = [];
                foreach ($v['motherId'] as $mid) {
                    $m = array_values(array_filter($mothers, function ($x) use ($mid) {
                        if ($x['id'] === $mid) {
                            return true;
                        }
                        return false;
                    }));
                    //                    $people[$k]['mother'] = $m[0]['name'];
                    $people[$k]['mother'] = array_merge($people[$k]['mother'], $m);
                }
            } else {
                $people[$k]['mother'] = [];
            }
        }

        return [
            'page' => $page,
            'count' => $length,
            'data' => $people,
            'msg' => 0,
            'level' => $level
        ];
    }

    /**
     * V3
     * 掉线图按名字模糊搜索人物
     * @param $personId
     * @return array 数组
     */
    public function searchPeopleByNameInCatenary($familyId, $data, $page, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = intval($page - 1) * $pageSize;

        $sql = "SELECT SQL_CALC_FOUND_ROWS id,same_personId,is_adoption,name,ranking,type,level,gender,familyId,userId,father as fatherId,family_index as familyIndex,bicolor,photo 
                        FROM $this->TABLE
                        WHERE familyId = $familyId and is_delete = 0 ";
        $sqlForCount = "SELECT FOUND_ROWS() as count";

        if ($data) {
            foreach ($data as $k => $v) {
                if ($k === 'name') {
                    $sql .= " and $k like '%$v%' ";
                    continue;
                }
                $sql .= " and  $k = $v ";
            }
        }

        $sql .= " order by level limit $offset, $pageSize";

        $people = $this->pdo->query($sql);
        $length = $this->pdo->uniqueResult($sqlForCount)['count'];


        if (!$people) {
            return [
                'page' => $page,
                'count' => $length,
                'data' => $people,
            ];
        }

        $people = $this->getCloseRelativePeople($familyId, $people, 'fatherId', 'father', false);

        return [
            'page' => $page,
            'count' => $length,
            'data' => $people,
        ];
    }

    /**
     * 获取人物相近关系的亲属信息
     * 版本3
     * @param $need 需要解析的人物
     * @param $people    人物数组
     * @return  array  人物数组
     */
    public function getCloseRelativePeople($familyId, $people, $need, $key, $isList = true)
    {

        $needId = [];

        foreach ($people as $person => $value) {
            $id = Util::idSplit($value[$need]);
            $people[$person][$need] = $id;

            if ($id) {
                $needId = array_merge($needId, $id);
            }
        }

        $needIdList = array_values(array_unique($needId));

        if ($needIdList) {
            $sqlForNeed = "select id,name from $this->TABLE where  is_delete = 0 and familyId = $familyId 
                            and id in (" . implode(',', $needIdList) . ")";
            $peopleInfo = $this->pdo->query($sqlForNeed);
        } else {
            $peopleInfo = [];
        }
        //ssssss

        foreach ($people as $k => &$v) {
            if ($v[$need]) {
                foreach ($v[$need] as $fid) {
                    $f = array_values(array_filter($peopleInfo, function ($x) use ($fid) {
                        if ($x['id'] === $fid) {
                            return true;
                        }
                        return false;
                    }));
                    if (!$isList) {
                        $people[$k][$key] = $people[$k][$key] ? $people[$k][$key] . $f[0]['name'] : "" . $f[0]['name'];
                        continue;
                    }
                    $people[$k][$key] = $f;
                }
            } else {
                $people[$k][$key] = [];
            }
        }

        return $people;
    }


    /**
     * V3 对应的获取人物
     * @param $personId
     * @return object Person对象
     */
    public function getPersonSimpleInfoById($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,name,ranking,type,level,gender,familyId,userId,family_index as familyIndex
                        FROM $this->TABLE
                        WHERE id = $personId and is_delete = 0;";
        $queryResult = $this->pdo->uniqueResult($sql);
        if ($queryResult != null) {
            $person = new Person($queryResult);
            return $person;
        }
        //@codeCoverageIgnoreStar
        return null;
        //@codeCoverageIgnoreEnd
    }

    /**
     * V3 对应的获取人物
     * @param array $personId
     * @return array
     */
    public function getPersonSimpleInfoByIds($personIds, $isDelete=true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        if (!$personIds){
            return [];
        }
        $isDeleteSql = $isDelete ? " and is_delete = 0 " : "";
        $sql = "SELECT id,name,ranking,type,level,gender,familyId,userId,family_index as familyIndex,birthday,address 
                        FROM $this->TABLE
                        WHERE id in ( ". implode(',', $personIds) . " ) $isDeleteSql;";
        return $this->pdo->query($sql);

    }

    /**
     *  更新家族familyIndex
     * @param $familyId
     * @param $personId
     * @param $familyIndex
     */

    public function updateFamilyIndex($familyId, $personId, $familyIndex)
    {
        // if (!$this->init()) {
        //     //@codeCoverageIgnoreStart
        //     Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
        //     exit;
        //     //@codeCoverageIgnoreEnd
        // }
        $updateSql = "UPDATE $this->TABLE SET family_index =  $familyIndex WHERE familyId = $familyId and  id = $personId";
        return $this->pdo->update($updateSql);
    }

    /**
     *
     *   插入家族最下的FamilyIndex
     * @param familyId
     */
    public function insertDefaultLastIndex($familyId)
    {
        $personId = Util::getNextId();
        $sql = "INSERT INTO $this->TABLE
        (id, name,level,familyId,family_index,gender,photo,share_url,userId,confirm,create_by,update_by,create_time,update_time)
        VALUES
        ($personId, 'end',99999,$familyId,99999999,9,'','',0,1,-1,-1,now(),now())";
        return $this->pdo->insert($sql);
    }

    /**
     * 获取家族的所有人员,供前端家族树使用
     * @param $familyId
     * @param $showMain 是否显示外勤
     * @param $filter   是否显示配偶
     * @return array 所有人员的数组
     *
     */
    public function getFamilyPersons3($familyId, int $levelLimit = null, $startId = 0, $filter = 0, $showMain = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $levelLimitSql = "";
        if (!is_null($levelLimit)) {
            //如果levelLimit不为空，则查询要通过level过滤
            // $levelLimitSql .= " AND tb.level <= $levelLimit ";
            $levelLimitSql .= " AND level <= $levelLimit ";
        }

        $filterSql = "";
        if ($filter == 1) {
            $filterSql = "AND type = 1";
        }

        $showMainSql = "";
        if ($showMain === 0) {
            $showMainSql = "AND isNotMain = 0";
        }


        $sql = "SELECT  tb.family_index as familyIndex  ,tb.id,tb.name,tb.level,tb.father,tb.mother,tb.gender,tb.userId,tb.ranking,tb.share_url as shareUrl,
                        tb.son,tb.daughter,tb.type,tb.spouse,tb.isNotMain,tb.is_adoption as isAdoption,ref_familyId as refFamilyId,ref_personId as refPersonId,
                        tb.sideText,tb.bicolor,isNotMain 
                        FROM 
                        $this->TABLE  tb  
                        WHERE familyId = $familyId  AND is_delete = 0   $levelLimitSql $filterSql $showMainSql  order by family_index asc;";

        $this->pdo->setBuffered(false); // 不允许缓存
        $result = $this->pdo->query($sql);


        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        if ($startId > 0) {

            $personmap = [];
            //拼
            foreach ($result as $v) {
                $personmap[$v['id']] = $v;
            }
            if (isset($personmap[$startId])) {
                $personmap = $this->cutTreesForFamilyPerson($personmap, $startId);
            }
            $result = [];
            //还原
            foreach ($personmap as $person) {
                $result[] = $person;
            }
        }

        return $result;
    }


    /**
     * 向家族中添加人物,版本3
     * @param $userId   该人物对应的用户id
     * @param $familyId 该人物所属于的家族
     * @param $name     该人物的姓名
     * @param $gender   该人物的性别
     * @param $address  该人物的住址
     * @param $birthday 该人物的生日
     * @param $photo    该人物的照片
     * @return int   人物的id或者失败返回-1
     */
    public function addPerson2($userId, $familyId, $name, $gender, $address, $birthday, $photo, $familyIndex)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        try {
            //开启事务处理
            $this->pdo->beginTransaction();
            $userId_t = $GLOBALS['userId'];
            $shareUrl = md5($name . Util::generateRandomCode(24));    //生成唯一share_url的code
            $personId = Util::getNextId();

            // 修正mysql里面的其他familyIndex
            $sql = "UPDATE $this->TABLE SET family_index = family_index +1  where  familyId = $familyId and is_delete = 0 and  family_index > $familyIndex ";
            $this->pdo->update($sql);

            $sql = "INSERT INTO $this->TABLE
                (id, name,address,birthday,gender,photo,share_url,level,familyId,userId,confirm,create_by,create_time,update_by,update_time,family_index)
                VALUES
                ('$personId', '$name','$address','$birthday','$gender','$photo','$shareUrl','0','$familyId','$userId','0','$userId_t',now(),'$userId',now(),$familyIndex)";
            if ($this->pdo->insert($sql) >= 0) {        //返回插入人物的id,失败则返回-1
                $id = $personId;
            } else {
                $id = -1;
            }

            //插入成功,更新自己的sister和brother字段
            if ($id > 0) {
                //添加家族的同时要更新用户资料中的家族数
                $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1,update_time = now() WHERE userId = '$userId' ";
                $this->pdo->update($sql);

                if ($gender == $GLOBALS['GENDER_FEMALE']) {
                    $updateSql = "UPDATE $this->TABLE SET sister = '@$id|',update_time = now() WHERE id = $id";
                } else if ($gender == $GLOBALS['GENDER_MALE']) {
                    $updateSql = "UPDATE $this->TABLE SET brother = '@$id|',update_time = now() WHERE id = $id";
                }
                $this->pdo->update($updateSql);
            }

            //更新家族成员数
            $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now()  WHERE id = '$familyId'";
            $this->pdo->update($memberCountSql);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            //@codeCoverageIgnoreEnd
        }

        return $id;
    }

    /**
     * V3 对应的获取人物
     * @param $personId
     * @return object Person对象
     */
    public function getPersonById2($corePersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,ref_familyId as refFamilyId , ref_personId  as refPersonId ,same_personId,is_adoption as isAdoption,name,ranking,type,level,gender,familyId,userId,father,mother,brother,sister,spouse,son,daughter,family_index as familyIndex,bicolor
                        FROM $this->TABLE
                        WHERE id = $corePersonId and is_delete = 0;";
        $queryResult = $this->pdo->uniqueResult($sql);
        if ($queryResult != null) {
            $person = new Person($queryResult);
            $parser = new RelationParser($person);
            $person->father = $parser->getFathers();
            $person->mother = $parser->getMothers();
            $person->son = $parser->getSons();
            $person->daughter = $parser->getDaughters();
            $person->spouse = $parser->getSpouses();
            $person->brother = $parser->getBrothers();
            $person->sister = $parser->getSisters();
            return $person;
        }
        //@codeCoverageIgnoreStar
        return null;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 获取添加孩子的默认坐标值
     * @param level 爸爸辈份
     * @param familyId 家族id
     * @param familyIndex 爸爸的familyIndex值
     */
    public function getNewChildFamilyIndex($level, $familyId, $familyIndex)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT family_index 
        from $this->TABLE  
        where familyId= $familyId  and  level = $level  and  is_delete = 0 and  family_index > $familyIndex  and  type = 1  
        order by family_index limit 1";
        return $this->pdo->query($sql);
    }

    /**
     * 向家族中添加相关联的人物
     * 为什么一个函数八百多行啊?
     * @param $familyId    家族id
     * @param $name        姓名
     * @param $gender        性别
     * @param $relation        关系
     * @param $corePersonId 关系人物id
     * @param $lastPersonId  上一个人物的id
     * @param $checkFamilyIndex 用于检验家族是否被修改未更新
     * @param $needTransaction  是否需要事务,true代表需要(默认)，false代表不需要
     * @return int 添加的人物id或者-1代表记录插入出错，-2代表插入的人物不属于该家族
     */
    public function addRelatePerson2($person, $needTransaction = true, $isCopyed = false)
    {
        if (!$this->init()) {

            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $userId = $GLOBALS['userId'];

        $corePerson = $this->getPersonById2($person->corePersonId);


        $familyDB = new CDBFamily();
        $family = $familyDB->getFamilyById($person->familyId);

        //$memberCount = $family->memberCount;    // 族群中的人物
        //如果是复制的操作，则不需要改变当前人物type 也可以写在下面的判断，这里就是减少一步数据库查询（查询出来的membercount不是实时数据）
        // $memberCount = $isCopyed ? 2 : $family->memberCount;
        // $needChangeCorePersonType = false;      // 是否需要改变核心人物的type

        // $personType = 1;

        /*******同时也要更新家族的level标志**************/
        $changed = FALSE;
        $minLevel = $family->minLevel;
        $maxLevel = $family->maxLevel;
        if ($person->level < $minLevel) {
            $minLevel = $person->level;
            $changed = TRUE;
        }
        if ($person->level > $maxLevel) {
            $maxLevel = $person->level;
            $changed = TRUE;
        }

        try {

            //开启事务处理

            // 事务导致不更新？
            // if ($changed){
            //     $updateRow =  $familyDB->updateFamilyLevel($person->familyId, $minLevel, $maxLevel);
            // }

            if ($needTransaction) {

                $this->pdo->beginTransaction();
            }
            // 事务导致不更新？

            if ($isCopyed == true) {
                //                $personId = $this->addPersonForCopy($person, $level, $personType); //复制人物独有的方法来增加人物
            } else {

                $personId = $this->addPersonWithLevel3($person, false); //增加要增加的人物
            }


            if ($personId <= 0) {
                return -1;
            }
            //需要更新的人物记录数组
            $records = array();
            $count = 0;

            /*********家族中的level标志更新完毕*********/
            if ($changed) {
                $updateRow = $familyDB->updateFamilyLevel($person->familyId, $minLevel, $maxLevel);
            }


            switch ($person->relation) {

                case $GLOBALS['RELATION_FATHER']:
                    //添加自己和父亲关系的记录
                    $meFatherRecord = new PersonRecord($person->corePersonId, "father", $personId);
                    $records[$count] = $meFatherRecord;                        //自己和父亲的记录
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    }

                    //更新父亲的儿子和兄弟的父亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $fatherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $fatherSonRecord;
                        $count++;

                        $brotherFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $brotherFatherRecord;
                        $count++;
                    }

                    //更新父亲的女儿和姐妹的父亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $fatherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;

                        $sisterFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $sisterFatherRecord;
                        $count++;
                    }

                    $updateSpouseId = array();

                    //更新父亲的配偶和母亲的配偶
                    foreach ($corePerson->mother as $key => $value) {
                        $fatherSpouseRecord = new PersonRecord($personId, "spouse", $value);
                        $records[$count] = $fatherSpouseRecord;
                        $count++;

                        $motherSpouseRecord = new PersonRecord($value, "spouse", $personId);
                        $records[$count] = $motherSpouseRecord;
                        $count++;

                        //更新配偶的婚姻状态
                        array_push($updateSpouseId, $value);
                    }

                    array_push($updateSpouseId, $personId);
                    $this->updateSpouseStatus($updateSpouseId);
                    break;

                case $GLOBALS['RELATION_SPOUSE']:
                    //添加自己和配偶关系的记录
                    $meSpouseRecord = new PersonRecord($person->corePersonId, "spouse", $personId);
                    $records[$count] = $meSpouseRecord;
                    $count++;

                    $spouseMeRecord = new PersonRecord($personId, "spouse", $person->corePersonId);
                    $records[$count] = $spouseMeRecord;
                    $count++;

                    //更新配偶的儿子和儿子的父母
                    foreach ($corePerson->son as $key => $value) {
                        $spouseSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $spouseSonRecord;
                        $count++;

                        /*****根据配偶的性别判断******/
                        if ($person->gender == $GLOBALS['GENDER_MALE']) {
                            $sonFatherRecord = new PersonRecord($value, "father", $personId);
                            $records[$count] = $sonFatherRecord;
                            $count++;
                        } else if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonMotherRecord = new PersonRecord($value, "mother", $personId);
                            $records[$count] = $sonMotherRecord;
                            $count++;
                        }
                    }

                    //更新配偶的女儿和女儿的父母
                    foreach ($corePerson->daughter as $key => $value) {
                        $spouseDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $spouseDaughterRecord;
                        $count++;

                        /*****根据配偶的性别判断******/
                        if ($person->gender == $GLOBALS['GENDER_MALE']) {
                            //@codeCoverageIgnoreStart
                            $daughterFatherRecord = new PersonRecord($value, "father", $personId);
                            $records[$count] = $daughterFatherRecord;
                            $count++;
                            //@codeCoverageIgnoreEnd
                        } else if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                            $daughterMotherRecord = new PersonRecord($value, "mother", $personId);
                            $records[$count] = $daughterMotherRecord;
                            $count++;
                        }
                    }

                    //更新婚姻状态
                    $this->updateSpouseStatus(array($personId, $person->corePersonId));

                    break;

                case $GLOBALS['RELATION_SON']:

                    //添加自己和儿子关系的记录
                    $meSonRecord = new PersonRecord($person->corePersonId, "son", $personId);
                    $records[$count] = $meSonRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sonMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sonMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    }


                    //更新儿子的父或母亲和配偶的儿子
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd

                        $spouseSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $spouseSonRecord;
                        $count++;
                    }

                    //找出我的配偶的配偶  父亲->母亲1（corePerson)->母亲2
                    //TODO 不确定是否正确
                    if (count($corePerson->spouse) == 1) {
                        $mySpouse = $this->getPersonById2($corePerson->spouse[0]);
                        foreach ($mySpouse->spouse as $key => $value) {
                            if ($value != $corePerson->id) {  //排除自己
                                //@codeCoverageIgnoreStart
                                /*****根据我的性别判断******/
                                if ($mySpouse->gender == $GLOBALS['GENDER_MALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                }

                                $spouseSonRecord = new PersonRecord($value, "son", $personId);
                                $records[$count] = $spouseSonRecord;
                                $count++;
                                //@codeCoverageIgnoreEnd
                            }
                        }
                    }
                    //更新儿子的兄弟和儿子的兄弟
                    foreach ($corePerson->son as $key => $value) {
                        $sonBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sonBrotherRecord;
                        $count++;

                        $sonBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sonBrotherRecord;
                        $count++;
                    }

                    //更新儿子的姐妹和女儿的兄弟
                    foreach ($corePerson->daughter as $key => $value) {
                        $sonSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sonSisterRecord;
                        $count++;

                        $daughterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;
                    }


                    break;
                case $GLOBALS['RELATION_DAUGHTER']:
                    //添加自己和女儿关系的记录
                    $meDaughterRecord = new PersonRecord($person->corePersonId, "daughter", $personId);
                    $records[$count] = $meDaughterRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    }


                    //更新女儿的父或母亲和配偶的女儿
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $daughterSpouseRecord = new PersonRecord($personId, "mother", $value);
                            $records[$count] = $daughterSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $daughterSpouseRecord = new PersonRecord($personId, "father", $value);
                            $records[$count] = $daughterSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd


                        $spouseDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $spouseDaughterRecord;
                        $count++;
                    }

                    //找出我的配偶的配偶  父亲->母亲1（corePerson)->母亲2
                    //TODO 不确定是否正确
                    //@codeCoverageIgnoreStart
                    if (count($corePerson->spouse) == 1) {
                        $mySpouse = $this->getPersonById2($corePerson->spouse[0]);
                        foreach ($mySpouse->spouse as $key => $value) {
                            if ($value != $corePerson->id) {  //排除自己
                                /*****根据我的性别判断******/
                                if ($mySpouse->gender == $GLOBALS['GENDER_MALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                }

                                $spouseSonRecord = new PersonRecord($value, "daughter", $personId);
                                $records[$count] = $spouseSonRecord;
                                $count++;
                                //@codeCoverageIgnoreEnd
                            }
                        }
                    }

                    //更新女儿的兄弟和儿子的姐妹
                    foreach ($corePerson->son as $key => $value) {
                        $daughterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;

                        $sonSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sonSisterRecord;
                        $count++;
                    }

                    //更新女儿的姐妹和女儿的姐妹
                    foreach ($corePerson->daughter as $key => $value) {
                        $daughterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $daughterSisterRecord;
                        $count++;

                        $daughterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $daughterSisterRecord;
                        $count++;
                    }
                    break;
            }


            if (!$this->updateRecord($records)) {
                // if ($needTransaction)

                if ($needTransaction) {
                    $this->pdo->rollback();
                }
                return -1;                //表示添加相关记录时出错
            } else {
                if ($needTransaction) {
                    $this->pdo->commit();
                }
                return $personId;
            }
        } catch (PDOException $e) {
            if ($needTransaction) {
                $this->pdo->rollback();
            }
            return -1;
        }
        return -1;
    }


    /**
     * 向家族中添加带有层次级别的人物,添加时根据性别，向其姐妹或者兄弟字段中更新自己的id
     * 版本3
     * @param person 该人物
     * @param level    该人物的级别
     * @param type     该人物的类型 1是本家族的，2是本家族的配偶
     * @param neetTransaction 默认开启
     * @return  int  人物的id或者失败返回-1
     */
    private function addPersonWithLevel3($person, $neetTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $userId = $GLOBALS['userId'];
        $shareUrl = md5($person->name . Util::generateRandomCode(24));

        //生成唯一share_url的code
        try {
            if ($neetTransaction == true) {
                $this->pdo->beginTransaction();
            }

            // 后面的向后移动一位
            $sql = "UPDATE $this->TABLE  SET  family_index = family_index + 1 where familyId = $person->familyId and family_index>= $person->familyIndex and is_delete = 0 ";


            $id = $this->pdo->update($sql);

            if ($person->birthday == null || $person->birthday == '') {
                $person->birthday = "NULL";
                // $person->birthday = now();
            } else {
                $person->birthday = "'$person->birthday'";
            }
            if ($person->deadTime == null || $person->deadTime == '') {
                $person->deadTime = "NULL";
                // $person->deadTime = now();
            } else {
                $person->deadTime = "'$person->deadTime'";
            }
            // 再插入g
            // $sql = "INSERT INTO $this->TABLE(family_index,id, name,type,gender,share_url,level,familyId,userId,ranking,create_by,create_time,update_by,update_time,isNotMain,bicolor,photo)
            // VALUES($person->familyIndex,$person->id,'$person->name',$person->type,$person->gender,'$shareUrl',$person->level,'$person->familyId',0,$person->ranking,$userId,now(),'$userId',now(),$person->isNotMain,$person->bicolor,'$person->photo')";
            $sql = "INSERT INTO $this->TABLE(family_index,id, name,type,gender,share_url,level,familyId,userId,ranking,create_by,create_time,update_by,
                                                                                            update_time,isNotMain,bicolor,photo,country_name,province_name,city_name,area_name,town_name,country,
                                                                                                            province,city,area,town,address,birthday,zi,zpname,remark,is_dead,dead_time,profileText,sideText)
                                            VALUES($person->familyIndex,$person->id,'$person->name',$person->type,$person->gender,'$shareUrl',$person->level,'$person->familyId',0,$person->ranking,$userId,now(),
                                                                                    '$userId',now(),$person->isNotMain,$person->bicolor,'$person->photo','$person->countryName','$person->provinceName',
                                                                                                            '$person->cityName','$person->areaName','$person->townName','$person->country','$person->province','$person->city',
                                                                                                                '$person->area','$person->town','$person->address',$person->birthday,'$person->zi','$person->zpname','$person->remark','$person->isDead',$person->deadTime,'$person->profileText','$person->sideText')";
            if ($this->pdo->insert($sql) >= 0) {
                //返回插入人物的id,失败则返回-1
                $id = $person->id;
            } else {
                $id = -1;
            }
            //插入成功
            if ($id > 0) {
                if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                    //@codeCoverageIgnoreStart
                    $updateSql = "UPDATE $this->TABLE SET sister = '@$id|',update_time = now() WHERE id = $id";
                    //@codeCoverageIgnoreEnd
                } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                    $updateSql = "UPDATE $this->TABLE SET brother = '@$id|',update_time = now() WHERE id = $id";
                }
                $this->pdo->update($updateSql);
                //更新家族成员数
                $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count + 1,update_time = now() WHERE id = $person->familyId";
                $this->pdo->update($memberCountSql);
                if ($neetTransaction == true) {
                    $this->pdo->commit();
                }
            }
        } catch (PDOException $e) {
            if ($neetTransaction == true) {
                $this->pdo->rollback();
            }
        }
        return $id;
    }

    /**
     * 根据personId删除某个人物
     * 版本3
     * 删除的时候要找出和他有关系的人物，删除其中的关系
     * 重置familyIndex
     * @param $personId 要删除的人物id
     * @param $needTransaction 是否需要事务
     * @return bool 删除成功 true 删除失败false
     */
    public function deletePerson3($personId, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        //先检索出这个人物对象
        $person = $this->getPersonById2($personId);

        try {

            if ($needTransaction) {
                //开启事务处理
                $this->pdo->beginTransaction();
            }

            //记录人物操作日志
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $this->getPersonById($personId);
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $familyId = $old_person->familyId;


            $option_detail = '删除人物';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['isDelete' => 1];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
            //end

            //更新父母的记录
            $fartherAndMother = array_merge($person->father, $person->mother);
            $setParam = "";
            if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                $setParam = "daughter";
            } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                $setParam = "son";
            }
            $from = '@' . $personId . "|";
            $to = "";
            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to')";
            $where = "";
            foreach ($fartherAndMother as $key => $value) {
                if ($where != "") {
                    $where = $where . " or id = $value";
                } else {
                    $where = $where . "id = $value";
                }
            }

            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);

            //更新兄弟姐妹的记录
            $brotherAndSister = array_merge($person->brother, $person->sister);
            $setParam = "";
            if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                $setParam = "sister";
            } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                $setParam = "brother";
            }
            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to')";
            $where = "";
            foreach ($brotherAndSister as $key => $value) {
                if ($where != "") {
                    $where = $where . " or id = $value";
                } else {
                    $where = $where . "id = $value";
                }
            }

            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);

            //更新配偶的记录
            $spouses = $person->spouse;
            $setParam = "spouse";

            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to'),update_time = now()";
            $where = "";
            foreach ($spouses as $key => $value) {
                if ($where != "") {
                    //@codeCoverageIgnoreStart
                    $where = $where . " or id = $value";
                    //@codeCoverageIgnoreEnd
                } else {
                    $where = $where . "id = $value";
                }
            }
            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);

            //更新儿女的记录
            $sonAndDaughter = array_merge($person->son, $person->daughter);
            $setParam = "";
            if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                $setParam = "mother";
            } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
                $setParam = "father";
            }
            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to') , update_time = now()";
            $where = "";
            foreach ($sonAndDaughter as $key => $value) {
                if ($where != "") {
                    $where = $where . " or id = $value";
                } else {
                    $where = $where . "id = $value";
                }
            }
            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);


            $sql = "UPDATE $this->TABLE  SET  family_index = family_index - 1 where familyId = $person->familyId and family_index >= $person->familyIndex and is_delete = 0 ";
            $this->pdo->update($sql);


            //$sql = "DELETE FROM $this->TABLE WHERE id = $personId";		//最后删除自己,不是真删，而是将is_delete置为1
            $sql = "UPDATE $this->TABLE SET is_delete = '1',update_time = now() WHERE id = $personId ";
            //更新家族的代数 TODO
            $count = $this->pdo->update($sql);


            //删除人物时检查人物是否绑定用户，如果绑定则更改用户的家族数
            $userId = $person->userId;
            if ($userId > 0) {
                //这里先删除管理员表（如果是管理员的话）
                /*$deleteSql = "UPDATE $this->TABLE_FAMILYADMIN SET isDelete=1 WHERE familyId = '$person->familyId' AND adminId = '$userId' AND isDelete=0 ";
                $delete_res = $this->pdo->update($deleteSql);
                $sql = "SELECT family_num FROM $this->TABLE_USER_PROFILE WHERE userId='$userId'";
                $result = $this->pdo->uniqueResult($sql);
                if ($result != null && $result['family_num'] != '0') {
                    $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num - 1,update_time = now() WHERE userId = '$userId'";
                    $this->pdo->update($sql);
                }*/
                //取消用户绑定
                $this->cancelUserBindForTmp($personId, $person->familyId, $userId, false);
            }

            // 删除人物的分支记录
            $branchSql = "UPDATE $this->TABLE_FAMILY_INNER_BRANCH_PERSON SET isDelete = 1 WHERE personId = '$personId' ";
            $this->pdo->update($branchSql);

            //更新家族成员数
            $memberCountSql = "UPDATE $this->TABLE_FAMILY SET member_count = member_count - 1,update_time = now() WHERE id = '$person->familyId'";
            $this->pdo->update($memberCountSql);


            if ($needTransaction) {
                $this->pdo->commit();
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            if ($needTransaction) {
                $this->pdo->rollback();
            }
            //@codeCoverageIgnoreEnd
        }
        if ($count > 0)
            return true;
        //@codeCoverageIgnoreStart
        return false;
        //@codeCoverageIgnoreEnd

    }

    /**
     * @param $updateData 更新字段
     * @param $personId     人物id
     * @param $userId   更新者
     * @return int 修改的记录数或者失败返回0
     */
    public function editPersonDetailV3($updateData, $personId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物操作日志
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $old_person = $this->getPersonById($personId);
        $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
        $familyId = $old_person->familyId;
        $option_detail = '编辑人物基本信息';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = $updateData;
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
        //end


        $sql = "UPDATE $this->TABLE SET ";
        foreach ($updateData as $k => $v) {
            if (in_array($k, ['birthday', 'dead_time'])) {
                if ($v === null) {
                    $sql .= " $k = null ,";
                    continue;
                }
                $sql .= " $k = '$v',";
                continue;
            }
            $sql .= " $k = '$v',";
        }

        $sql .= " update_time = now(), update_by = $userId WHERE id = '$personId' AND is_delete = '0'";

        $count = $this->pdo->update($sql);

        return $count;
    }

    /**
     * @param $personId 人物id
     * @param $name     人物姓名
     * @param $gender   人物性别
     * @param $address  人物地址
     * @param $birthday 人物生日
     * @param $phone    电话号码
     * @param $bloodType 血型
     * @param $maritalStatus 婚姻状况
     * @param $qq       QQ联系方式
     * @return int 修改的记录数或者失败返回0
     */
    public function editPersonDetailV3Old($person, $personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $birthday_sql = "";

        if ($person->birthday == null || $person->birthday == "") {
            $birthday_sql = "birthday = NULL,";
        } else {
            $birthday_sql = "birthday = '$person->birthday',";
        }
        $deadTime_sql = "";
        if ($person->deadTime == null || $person->deadTime == "") {
            $deadTime_sql = "dead_time = NULL,";
        } else {
            $deadTime_sql = "dead_time = '$person->deadTime',";
        }

        //记录人物操作日志
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $old_person = $this->getPersonById($personId);
        $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
        $familyId = $old_person->familyId;
        $option_detail = '编辑人物基本信息';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = [
            'zi' => $person->zi,
            'zpname' => $person->zpname,
            'remark' => $person->remark,
            'name' => $person->name,
            'is_dead' => $person->isDead,
            'deadTime' => $person->deadTime,
            'country' => $person->country,
            'country_name' => $person->countryName,
            'province' => $person->province,
            'province_name' => $person->provinceName,
            'city' => $person->city,
            'city_name' => $person->cityName,
            'area' => $person->area,
            'area_name' => $person->areaName,
            'town' => $person->town,
            'town_name' => $person->townName,
            'address' => $person->address,
            'birthday' => $person->birthday,
            'phone' => $person->phone,
            'ranking' => $person->ranking,
            'birthPlace' => $person->birthPlace,
            'burialPlace' => $person->burialPlace,
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
        //end


        $sql = "UPDATE $this->TABLE
            SET name = '$person->name',
            zi='$person->zi',
            zpname='$person->zpname',
            remark='$person->remark',
            is_dead='$person->isDead',
            $deadTime_sql
            address = '$person->address',
            $birthday_sql
            phone = '$person->phone',
            blood_type = '$person->bloodType',
            marital_status = '$person->maritalStatus',
            country='$person->country',
            country_name='$person->countryName',
            province='$person->province',
            province_name='$person->provinceName',
            city='$person->city',
            city_name='$person->cityName',
            area='$person->area',
            area_name='$person->areaName',
            town='$person->town',
            town_name='$person->townName',
            sideText='$person->sideText',
            profileText='$person->profileText',
            burialPlace = '$person->burialPlace',
            birthPlace = '$person->birthPlace'
            qq = '$person->qq', ranking='$person->ranking',update_time = now() WHERE id = '$personId' AND is_delete = '0'";


        $count = $this->pdo->update($sql);            //返回修改的数量，失败则返回-1
        return $count;
    }

    /*
     * v3
     * 检查该用户的家族权限
     * @param $familyId
     * @param $userId
     * return
     * */
    public function checkPermission($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "select permissionType,startIndex,endIndex from $this->TABLE_NEW_PERMISSION where familyId = $familyId 
        and userId = $userId and is_delete = 0";

        return $this->pdo->uniqueResult($sql);
    }


    /**
     * 根据当前人物的level , familyId,familyIndex 查出当前人物深度遍历下最后一人的familyIndex
     * 1. 权限的判断
     * 2.人物添加的位置判断
     * 3. 家族合并被覆盖的人物范围,当前人物的孩子节点数
     * @param familyId
     * @param corePersonLevel
     * @param corePersonFamilyIndex
     * @return int familyIndex
     * @author yuanxin
     * @version 3
     */
    public function getLastPersonFamilyIndex($familyId, $corePersonLevel, $corePersonFamilyIndex)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }

        $sql = "SELECT min(family_index) as familyIndex
            FROM $this->TABLE 
               WHERE familyId = $familyId and is_delete = 0 and type = 1 and  level <= $corePersonLevel and  family_index > $corePersonFamilyIndex ";
        $lastPersonFamilyIndex = $this->pdo->query($sql);


        if ($lastPersonFamilyIndex[0]['familyIndex'] == null) {
            $sql = "SELECT family_index as familyIndex FROM $this->TABLE WHERE familyId = $familyId  and is_delete = 0  order by family_index desc limit 1;";
            $lastPersonFamilyIndex = $this->pdo->query($sql);
            return $lastPersonFamilyIndex[0]['familyIndex'];
        } else {
            return $lastPersonFamilyIndex[0]['familyIndex'] - 1;
        }
    }


    /**
     * 根据起始FamilyIndex 和结束FamilyIndex获取那一段的数据.Filter用于过滤配偶
     * @param familyId
     * @param startFamilyIndex
     * @param endFamilyIndex
     * @param filter
     * @return persons
     */

    public function getFamilyPersonsByFamilyIndex($familyId, $startFamilyIndex, $endFamilyIndex, $filter = 0)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $filterSql = "";
        if ($filter == 1) {
            $filterSql = "AND type = 1";
        }

        $sql = "SELECT    family_index as familyIndex,id,name,level,father,mother,gender,son,daughter,brother,sister,spouse,type,spouse,ranking,share_url as shareUrl
                        FROM  $this->TABLE 
                        WHERE familyId  = $familyId  AND is_delete = 0 $filterSql AND  family_index   BETWEEN  $startFamilyIndex AND $endFamilyIndex  
                        order by family_index;";

        $result = $this->pdo->query($sql);

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }


        return $result;
    }


    public function getMultiFamilyPersons($familyPeopleA, $personIdA, $familyIdB, $refPersonsStartId, $filter = 0)
    {
        $endIndex = 9999999;

        //  拿出familyPeopleAHead
        $num = 0;
        $flag = 0;
        $deleteNum = 0;
        $personA = $this->getPersonById2($personIdA);
        foreach ($familyPeopleA as $key => $person) {
            if ($personIdA == $person['id']) {
                $num = $key;


                for ($i = $num; $i < count($familyPeopleA); $i++) {
                    // 计算familyPeopleATail
                    if ($familyPeopleA[$i]['level'] < $personA->level) {
                        $deleteNum++;
                    } else {
                        break;
                    }
                }
                break;
            } elseif ($person['level'] == $personA->level) {
                if (in_array($personIdA, $person['brother'])) {
                    $familyPeopleA[$key]['brother'] = Util::array_remove($familyPeopleA[$key]['brother'], $personIdA);
                } elseif (in_array($personIdA, $person['sister'])) {
                    $familyPeopleA[$key]['sister'] = Util::array_remove($familyPeopleA[$key]['sister'], $personIdA);
                }
            }
        }


        $familyPeopleAHead = array_slice($familyPeopleA, 0, $num);
        $familyPeopleATail = array_slice($familyPeopleA, $deleteNum + $num + 1, count($familyPeopleA));


        //  处理B家族
        // 根据PersonId获取startIndex
        $personB = $this->getPersonById2($refPersonsStartId);
        $familyBFamilyIndex = $this->getLastPersonFamilyIndex($familyIdB, $personB->level, $personB->familyIndex);
        $familyPeopleB = $this->getFamilyPersonsByFamilyIndex($familyIdB, $personB->familyIndex, $familyBFamilyIndex, $filter);

        //B 家族人物辈分处理
        if ($familyPeopleB != null) {
            $level = $familyPeopleB[0]['level'] - $personA->level;
            foreach ($familyPeopleB as $key => $person) {
                $familyPeopleB[$key]['level'] -= $level;
            }
        }

        //B 家族人物关系处理
        $familyPeopleB[0]['father'] = $personA->father;
        if ($personA->gender == 0) {
            $familyPeopleB[0]['sister'] = Util::array_remove($personA->sister, $personIdA);
            $familyPeopleB[0]['brother'] = $personA->brother;
        } else {
            $familyPeopleB[0]['brother'] = Util::array_remove($personA->brother, $personIdA);
            $familyPeopleB[0]['sister'] = $personA->sister;
        }

        //  处理Ahead父母的儿子属性
        $parents = array();
        if ($filter == 0) {
            $parents = array_merge($personA->father, $person['mother']);
        } else {
            $parents = array_merge($personA->father);
        }
        $parentsCount = count($parents);
        $i = count($familyPeopleAHead) - 1;
        while ($parentsCount > 0) {
            if (in_array($familyPeopleAHead[$i]['id'], $parents)) {
                foreach ($familyPeopleAHead[$i]['son'] as $key => $value) {
                    if ($value == $personIdA) {
                        $familyPeopleAHead[$i]['son'][$key] = $familyPeopleB[0]['id'];
                        $parentsCount--;
                        break;
                    }
                }
            }
            $i--;
        }

        //    处理Ahead和Atail内兄弟姐妹的属性

        //    foreach($familyPeopleAHead as $key  => $person){
        //        if($personA ->level == $person['level']){
        //         //    if(in_array($personA->id,$person['brother'])){
        //             foreach($person['brother'] as $key=>$value){
        //                 if($value == $personA->id){
        //                         $person['brother'][$key]=$refPersonsStartId;
        //                 break;
        //                 }
        //             }
        //                 //   $familyPeopleAHead[$key]['brother'] = Util::array_remove($familyPeopleAHead[$key]['brother'],$personIdA);
        //                 //   $familyPeopleAHead[$key]['brother'] = array_push($familyPeopleAHead[$key]['brother'],$refPersonsStartId);
        //            }else{
        //             foreach($person['sister'] as $key=>$value){
        //                 if($value == $personA->id){
        //                         $person['sister'][$key]=$refPersonsStartId;
        //                 break;
        //                 }
        //             }
        //             //    $familyPeopleAHead[$key]['sister'] = Util::array_remove($familyPeopleAHead[$key]['sister'],$personIdA);
        //             //    $familyPeopleAHead[$key]['sister'] = array_push($familyPeopleAHead[$key]['sister'],$refPersonsStartId);
        //         }
        //        }
        //    }

        //    foreach($familyPeopleATail as $key  => $person){
        //        if($personA ->level == $person['level']){
        //            if(in_array($personA->id,$person['brother'])){
        //                   $familyPeopleATail[$key]['brother'] = Util::array_remove($familyPeopleATail[$key]['brother'],$personIdA);
        //                   $familyPeopleATail[$key]['brother'] = array_push($familyPeopleATail[$key]['brother'],$refPersonsStartId);
        //            }else  if(in_array($personA->id,$person['sister'])){
        //                $familyPeopleATail[$key]['sister'] = Util::array_remove($familyPeopleATail[$key]['sister'],$personIdA);
        //                $familyPeopleATail[$key]['sister'] = array_push($familyPeopleATail[$key]['sister'],$refPersonsStartId);
        //         }
        //        }
        //    }


        $persons = array_merge($familyPeopleAHead, $familyPeopleB, $familyPeopleATail);
        return $persons;
    }


    public function getMultiFamilyPersons2($familyPeopleA, $personIdA, $familyIdB, $refPersonsStartId, $filter = 0)
    {
        $endIndex = 9999999;

        //  拿出familyPeopleAHead
        $num = 0;
        $flag = 0;
        $deleteNum = 0;
        $personA = $this->getPersonById2($personIdA);
        foreach ($familyPeopleA as $key => $person) {
            if ($personIdA == $person['id']) {
                $num = $key;


                for ($i = $num; $i < count($familyPeopleA); $i++) {
                    // 计算familyPeopleATail
                    if ($familyPeopleA[$i]['level'] < $personA->level) {
                        $deleteNum++;
                    } else {
                        break;
                    }
                }
                break;
            } elseif ($person['level'] == $personA->level) {
                if (in_array($personIdA, $person['brother'])) {
                    $familyPeopleA[$key]['brother'] = Util::array_remove($familyPeopleA[$key]['brother'], $personIdA);
                } elseif (in_array($personIdA, $person['sister'])) {
                    $familyPeopleA[$key]['sister'] = Util::array_remove($familyPeopleA[$key]['sister'], $personIdA);
                }
            }
        }


        $familyPeopleAHead = array_slice($familyPeopleA, 0, $num);
        $familyPeopleATail = array_slice($familyPeopleA, $deleteNum + $num + 1, count($familyPeopleA));


        //  处理B家族
        // 根据PersonId获取startIndex
        $personB = $this->getPersonById2($refPersonsStartId);
        $familyBFamilyIndex = $this->getLastPersonFamilyIndex($familyIdB, $personB->level, $personB->familyIndex);
        $familyPeopleB = $this->getFamilyPersonsByFamilyIndex($familyIdB, $personB->familyIndex, $familyBFamilyIndex, $filter);

        //B 家族人物辈分处理
        if ($familyPeopleB != null) {
            $level = $familyPeopleB[0]['level'] - $personA->level;
            foreach ($familyPeopleB as $key => $person) {
                $familyPeopleB[$key]['level'] -= $level;
            }
        }

        //B 家族人物关系处理
        $familyPeopleB[0]['father'] = $personA->father;
        if ($personA->gender == 0) {
            $familyPeopleB[0]['sister'] = Util::array_remove($personA->sister, $personIdA);
            $familyPeopleB[0]['brother'] = $personA->brother;
        } else {
            $familyPeopleB[0]['brother'] = Util::array_remove($personA->brother, $personIdA);
            $familyPeopleB[0]['sister'] = $personA->sister;
        }

        //  处理Ahead父母的儿子属性
        $parents = array();
        if ($filter == 0) {
            $parents = array_merge($personA->father, $person['mother']);
        } else {
            $parents = array_merge($personA->father);
        }
        $parentsCount = count($parents);
        $i = count($familyPeopleAHead) - 1;
        while ($parentsCount > 0) {
            if (in_array($familyPeopleAHead[$i]['id'], $parents)) {
                foreach ($familyPeopleAHead[$i]['son'] as $key => $value) {
                    if ($value == $personIdA) {
                        $familyPeopleAHead[$i]['son'][$key] = $familyPeopleB[0]['id'];
                        $parentsCount--;
                        break;
                    }
                }
            }
            $i--;
        }


        //    处理Ahead和Atail内兄弟姐妹的属性

        foreach ($familyPeopleAHead as $key => $person) {
            if ($personA->level == $person['level']) {
                if (in_array($personA->id, $person['brother'])) {
                    $familyPeopleAHead[$key]['brother'] = Util::array_remove($familyPeopleAHead[$key]['brother'], $personIdA);
                    $familyPeopleAHead[$key]['brother'] = array_push($familyPeopleAHead[$key]['brother'], $refPersonsStartId);
                } else if (in_array($personA->id, $person['sister'])) {
                    $familyPeopleAHead[$key]['sister'] = Util::array_remove($familyPeopleAHead[$key]['sister'], $personIdA);
                    $familyPeopleAHead[$key]['sister'] = array_push($familyPeopleAHead[$key]['sister'], $refPersonsStartId);
                }
            }
        }

        foreach ($familyPeopleATail as $key => $person) {
            if ($personA->level == $person['level']) {
                if (in_array($personA->id, $person['brother'])) {
                    $familyPeopleATail[$key]['brother'] = Util::array_remove($familyPeopleATail[$key]['brother'], $personIdA);
                    $familyPeopleATail[$key]['brother'] = array_push($familyPeopleATail[$key]['brother'], $refPersonsStartId);
                } else if (in_array($personA->id, $person['sister'])) {
                    $familyPeopleATail[$key]['sister'] = Util::array_remove($familyPeopleATail[$key]['sister'], $personIdA);
                    $familyPeopleATail[$key]['sister'] = array_push($familyPeopleATail[$key]['sister'], $refPersonsStartId);
                }
            }
        }


        $persons = array_merge($familyPeopleAHead, $familyPeopleB, $familyPeopleATail);
        return $persons;
    }

    /**
     * 删除配偶
     */
    public function deleteSpouses($persons, $spousesStack)
    {
        while ($spousesStack != null) {
            $nextPersonId = array_shift($spousesStack);
            foreach ($persons as $key => $person) {
                if ($person['id'] == $nextPersonId) {
                    unset($persons[$key]);
                    break;
                }
            }
        }
        $data['persons'] = $persons;
        $data['spousesStack'] = $spousesStack;

        return $data;
    }

    /**
     * V3
     * @param $personId
     * @return array Person对象
     */
    public function getPersonByIdV3($corePersonId, $checkDelete = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        if ($checkDelete == false) {
            $whereIsDelete = '';
        } else {
            $whereIsDelete = " AND tb.is_delete = '0' ";
        }


        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.branchId,tb.zi,tb.zpname,tb.remark,tb.name,tb.type,tb.level,
                tb.country,tb.country_name as countryName,tb.province,tb.province_name as provinceName,tb.city,tb.city_name as cityName,
                tb.area,tb.area_name as areaName,tb.town,tb.town_name as townName,tb.address,
                tb.birthday,tb.gender,tb.photo,tb.phone,tb.blood_type as bloodType,
                tb.marital_status as maritalStatus,tb.qq,tb.share_url as shareUrl,
                tb.is_dead as isDead,tb.dead_time as deadTime,realMother,
                tb.familyId,tf.groupType,tf.name as familyName,tf.photo as familyPhoto,tb.ref_familyId as refFamilyId,
                tb.userId,tb.father,tb.mother,tb.brother,tb.sister,tb.spouse,tb.son,tb.daughter,
                tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,
                tb.update_time as updateTime,tu.hx_username as hxUsername,tb.ranking ,tb.sideText, tb.profileText, tu.username, 
                burialPlace, tb.birthPlace, if(userId=0,0,1) as isBind,isNotMain,family_index as familyIndex ,tb.bicolor

                    FROM $this->TABLE tb  
                    LEFT JOIN $this->TABLE_USER tu on tb.userId = tu.id 
                    LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId
                    WHERE tb.id = $corePersonId " . $whereIsDelete . "limit 1";

        $queryResult = $this->pdo->uniqueResult($sql);
        if (!$queryResult) {
            return [];
        }
        $userId = $queryResult['userId'];
        $sql_permission = "select permissionType from $this->TABLE_NEW_PERMISSION where userId = $userId and familyId = " . $queryResult['familyId'] . " and is_delete = 0";
        $permission = $this->pdo->uniqueResult($sql_permission);
        $queryResult['permission'] = $permission['permissionType'];

        $fatherId = Util::idSplit($queryResult['father']);
        $queryResult['fatherId'] = $fatherId[0] === '' ? [] : $fatherId;
        $queryResult['motherId'] = $motherId = Util::idSplit($queryResult['mother']);
        $queryResult['sonId'] = $sonId = Util::idSplit($queryResult['son']);
        $queryResult['daughterId'] = $daughterId = Util::idSplit($queryResult['daughter']);
        $queryResult['spouseId'] = $spouseId = Util::idSplit($queryResult['spouse']);
        $queryResult['brotherId'] = $brotherId = Util::idSplit($queryResult['brother']);
        $queryResult['sisterId'] = $sisterId = Util::idSplit($queryResult['sister']);

        $ids = array_merge(
            $queryResult['fatherId'],
            $queryResult['motherId'],
            $queryResult['sonId'],
            $queryResult['daughterId'],
            $queryResult['spouseId'],
            $queryResult['brotherId'],
            $queryResult['sisterId']
        );

        $relatedPeople = $this->getPersonIdAndName($ids);
        $queryResult['father'] = array_values(array_filter($relatedPeople, function ($v) use ($fatherId) {
            return in_array($v['id'], $fatherId) ? true : false;
        }));
        $mother = array_values(array_filter($relatedPeople, function ($v) use ($motherId) {
            return in_array($v['id'], $motherId) ? true : false;
        }));
        $queryResult['mother'] = $mother;
        $queryResult['son'] = array_values(array_filter($relatedPeople, function ($v) use ($sonId) {
            return in_array($v['id'], $sonId) ? true : false;
        }));
        $queryResult['daughter'] = array_values(array_filter($relatedPeople, function ($v) use ($daughterId) {
            return in_array($v['id'], $daughterId) ? true : false;
        }));
        $queryResult['spouse'] = array_values(array_filter($relatedPeople, function ($v) use ($spouseId) {
            return in_array($v['id'], $spouseId) ? true : false;
        }));
        $queryResult['brother'] = array_values(array_filter($relatedPeople, function ($v) use ($brotherId) {
            return in_array($v['id'], $brotherId) ? true : false;
        }));
        $queryResult['brother'] = array_values(array_filter($queryResult['brother'], function ($v) use ($corePersonId) {
            return $v['id'] != $corePersonId ? true : false;
        }));

        $queryResult['sister'] = array_values(array_filter($relatedPeople, function ($v) use ($sisterId) {
            return in_array($v['id'], $sisterId) ? true : false;
        }));
        $queryResult['sister'] = array_values(array_filter($queryResult['sister'], function ($v) use ($corePersonId) {
            return $v['id'] != $corePersonId ? true : false;
        }));

        $queryResult['realMother'] = $queryResult['realMother'] ? $queryResult['realMother'] : ($mother ? $mother[0]['name'] : '');

        //        $queryResult['father'] = $this->getPersonIdAndName($queryResult['fatherId']);
        //        $mother = $this->getPersonIdAndName($queryResult['motherId']);
        //        $queryResult['mother'] = $mother;
        //        $queryResult['son'] = $this->getPersonIdAndName($queryResult['sonId']);
        //        $queryResult['daughter'] = $this->getPersonIdAndName($queryResult['daughterId']);
        //        $queryResult['spouse'] = $this->getPersonIdAndName($queryResult['spouseId']);
        //        $queryResult['brother'] = $this->getPersonIdAndName($queryResult['brotherId']);
        //        $queryResult['sister'] = $this->getPersonIdAndName($queryResult['sisterId']);
        //        $queryResult['realMother'] = $queryResult['realMother'] ? $queryResult['realMother'] : ($mother ? $mother[0]['name'] : '');
        //        $queryResult['isBind'] = $queryResult['userId'] == 0 ? 0:1;

        $parentsList = [];
        if ($queryResult['isAdoption'] == 1) {
            $TABLE_ADOPTION = 'gener_person_adoption';
            $sql = "select father,mother from $this->TABLE where is_delete = 0 and id in 
            (select src_personId from $TABLE_ADOPTION where is_delete = 0 and dest_personId = $corePersonId)";
            $adoptions = $this->pdo->query($sql);
            foreach ($adoptions as $parent) {
                $father = Util::idSplit($parent['father'])[0];
                if ($father) {
                    $sql = "select id,type,name from $this->TABLE where is_delete = 0 and id = $father limit 1";
                    $fatherInfo = $this->pdo->uniqueResult($sql);
                    if ($fatherInfo['type'] == 1) {
                        $parentsList[] = ['id' => $fatherInfo['id'], 'name' => $fatherInfo['name']];
                        continue;
                    }
                }
                $mother = Util::idSplit($parent['mother'])[0];
                if ($mother) {
                    $sql = "select id,type,name from $this->TABLE where is_delete = 0 and id = $mother limit 1";
                    $motherInfo = $this->pdo->uniqueResult($sql);
                    if ($motherInfo['type'] == 1) {
                        $parentsList[] = ['id' => $motherInfo['id'], 'name' => $motherInfo['name']];
                        continue;
                    }
                }
            }
        } elseif ($queryResult['isAdoption'] == 2) {
            $TABLE_ADOPTION = 'gener_person_adoption';
            $sql = "select father,mother from $this->TABLE where is_delete = 0 and id in 
            (select dest_personId from $TABLE_ADOPTION where is_delete = 0 and src_personId = $corePersonId)";
            $adoptions = $this->pdo->query($sql);
            foreach ($adoptions as $parent) {
                $father = Util::idSplit($parent['father'])[0];
                if ($father) {
                    $sql = "select id,type,name from $this->TABLE where is_delete = 0 and id = $father limit 1";
                    $fatherInfo = $this->pdo->uniqueResult($sql);
                    if ($fatherInfo['type'] == 1) {
                        $parentsList[] = ['id' => $fatherInfo['id'], 'name' => $fatherInfo['name']];
                        continue;
                    }
                }
                $mother = Util::idSplit($parent['mother'])[0];
                if ($mother) {
                    $sql = "select id,type,name from $this->TABLE where is_delete = 0 and id = $mother limit 1";
                    $motherInfo = $this->pdo->uniqueResult($sql);
                    if ($motherInfo['type'] == 1) {
                        $parentsList[] = ['id' => $motherInfo['id'], 'name' => $motherInfo['name']];
                        continue;
                    }
                }
            }
        }
        $queryResult['stepparent'] = $parentsList;

        return $queryResult;
    }

    /**
     * V3
     * @param $personId
     * @return array 包含名字和id的數組
     */
    public function getPersonIdAndName($personIds)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if (!$personIds) {
            return [];
        }

        $sql = "SELECT id,name FROM $this->TABLE where id in (" . implode(',', $personIds) . ") and is_delete = 0";

        $queryResult = $this->pdo->query($sql);

        return $queryResult;
    }

    /**
     *   层级遍历加深度遍历获取家族所有人物
     *  Filter用于过滤配偶
     * @param familyId
     * @param filter
     * @return persons
     */

    public function getFamilyPersonsByFamilyId($familyId, $filter)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $filterSql = "";
        if ($filter == 1) {
            $filterSql = "AND type = 1";
        }

        $sql = "SELECT family_index,id,name,level,father,mother,gender,son,daughter,type,spouse,ranking,brother,sister
                            FROM  $this->TABLE
                            WHERE familyId = $familyId $filterSql AND is_delete = 0 
                            ORDER BY  level , family_index ASC; ";

        $result = $this->pdo->query($sql);

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }

        return $result;
    }

    /**
     * @param $personId
     * @return object Person对象
     */
    public function getPersonObjectById($corePersonId, $checkDelete = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        if ($checkDelete == false) {
            $whereIsDelete = '';
        } else {
            $whereIsDelete = " AND tb.is_delete = '0' ";
        }


        $sql = "SELECT tb.id,tb.same_personId as samePersonId,tb.is_adoption as isAdoption,tb.branchId,tb.zi,tb.zpname,tb.remark,tb.name,tb.type,tb.level,
                tb.country,tb.country_name as countryName,tb.province,tb.province_name as provinceName,tb.city,tb.city_name as cityName,
                tb.area,tb.area_name as areaName,tb.town,tb.town_name as townName,tb.address,
                tb.birthday,tb.gender,tb.photo,tb.phone,tb.blood_type as bloodType,
                tb.marital_status as maritalStatus,tb.qq,tb.share_url as shareUrl,
                tb.is_dead as isDead,tb.dead_time as deadTime,
                tb.familyId,tf.groupType,tf.name as familyName,tf.photo as familyPhoto,tb.ref_familyId as refFamilyId,
                tb.userId,tb.father,tb.mother,tb.brother,tb.sister,tb.spouse,tb.son,tb.daughter,
                tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,
                tb.update_time as updateTime,tu.hx_username as hxUsername,tb.ranking ,tb.sideText, tb.profileText,tb.isNotMain,family_index as familyIndex

                    FROM $this->TABLE tb  
                    LEFT JOIN $this->TABLE_USER tu on tb.userId = tu.id 
                    LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId
                    WHERE tb.id = $corePersonId " . $whereIsDelete;

        $queryResult = $this->pdo->uniqueResult($sql);
        if ($queryResult != null) {
            $person = new Person($queryResult);
            $parser = new RelationParser($person);
            $person->father = $parser->getFathers();
            $person->mother = $parser->getMothers();
            $person->son = $parser->getSons();
            $person->daughter = $parser->getDaughters();
            $person->spouse = $parser->getSpouses();
            $person->brother = $parser->getBrothers();
            $person->sister = $parser->getSisters();
            return $person;
        }
        //@codeCoverageIgnoreStart
        return null;
        //@codeCoverageIgnoreEnd
    }

    /**
     * v3
     * v3
     * v3
     * 取消用户和人物之间的绑定,用户留在临时用户表中
     * @param $personId 人物id
     * @return bool true成功,false失败
     */
    public function cancelUserBindV3(int $personId, $familyId, $userId, $openTransaction = true, $permission = 100): bool
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        $person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '取消用户和人物之间的绑定,用户留在临时表';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['oldUserId' => $person->userId];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        if ($openTransaction == false) {
            $sql = "UPDATE $this->TABLE SET userId = '0',update_time = now() WHERE id = '$personId' AND is_delete = '0' ";
            $count = $this->pdo->update($sql);
            if ($count > 0) {
                //删除管理员身份
                if ($permission !== null) {
                    $updateSql = "UPDATE $this->TABLE_NEW_PERMISSION SET permissionType=$permission WHERE familyId='$familyId' AND userId='$userId' AND is_delete=0";
                    $this->pdo->update($updateSql);
                }
            }
            return true;
        } else {
            try {
                $this->pdo->beginTransaction();
                //                $person = $this->getPersonById($personId);
                $sql = "UPDATE $this->TABLE SET userId = '0',update_time = now() WHERE id = '$personId' AND is_delete = '0' ";
                $count = $this->pdo->update($sql);
                if ($count > 0) {
                    if ($permission !== null) {
                        $updateSql = "UPDATE $this->TABLE_NEW_PERMISSION SET permissionType=$permission WHERE familyId='$familyId' AND userId='$userId' AND is_delete=0";
                        $this->pdo->update($updateSql);
                    }
                }
                $this->pdo->commit();
                return true;
                //@codeCoverageIgnoreStart
            } catch (PDOException $e) {
                $this->pdo->rollback();
                return false;
                //@codeCoverageIgnoreEnd
            }
        }
    }

    /**
     * v3
     * v3
     * v3新加了对familyIndex的调整
     * 移动人物到另一个节点下，也就是将某一支迁移到另一个祖先下
     * @param int $personId 人物id
     * @param int $toPersonId 迁移到的人物id
     * @return bool 迁移成功或失败
     */
    public function movePersonV3($personId, $toPersonId, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $person = $this->getPersonOBjectById($personId);
        $toPerson = $this->getPersonOBjectById($toPersonId);

        //记录人物日志
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '迁移分支至新节点';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['toPerson' => $toPerson];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        if ($person == null || $toPerson == null) {
            return false;
        }

        // 检查person和toPerson不是同一个分支


        // 移动分支的流程如下:
        // 1. 将当前人物的父/母关系转移到目标人物下, 并更新兄弟姐妹
        // 2. 更新目标人物的子/女关系以及配偶的子/女关系
        // 3. 更新目标人物其他子女的兄弟姐妹
        // 4. 删除原来留存的父母/兄弟姐妹关系
        // 5. 移动时level会变化,需要改动
        try {
            $needTransaction ? $this->pdo->beginTransaction() : null;

            # 更新familyIndex
            $personIndex = $person->familyIndex;
            $personLastIndex = $this->getLastPersonFamilyIndexV3($person->familyId, $person->level, $personIndex);
            $personCount = $personLastIndex - $personIndex + 1;
            $toPersonIndex = $toPerson->familyIndex;
            //            print_r([$toPersonIndex,'qqqq']);
            if ($toPerson->son || $toPerson->daughter) {
                $sqlBrotherSister = "select id,family_index as  familyIndex,level from $this->TABLE where id in ( " . implode(',', array_merge($toPerson->son, $toPerson->daughter)) . " ) and is_delete = 0 order by ranking desc, gender desc ,create_time desc limit 1";
                $brotherAndSister = $this->pdo->uniqueResult($sqlBrotherSister);
                //                var_dump($brotherAndSister);
                $toPersonIndex = $this->getLastPersonFamilyIndexV3($person->familyId, $brotherAndSister['level'], $brotherAndSister['familyIndex']);
                //                var_dump($toPersonIndex);
            }

            if ($personIndex < $toPersonIndex) {
                //                var_dump(222222);
                $bigNum = 10000000;
                # 先将迁移部分整体减去一个非常大的数字，用来区分
                $sql = "update $this->TABLE set family_index = family_index - $bigNum  where familyId = $person->familyId and family_index between $personIndex and $personLastIndex and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
                # 再更新被迁移处，把family_index整体减去迁移人物的总数
                $sql = "update $this->TABLE set family_index = family_index - $personCount where familyId = $person->familyId and family_index > $personLastIndex and family_index <= $toPersonIndex and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
                # 先更新迁移的部分，把family_index整体加上差值和大整数
                $difference = $bigNum + $toPersonIndex - $personLastIndex;
                $sql = "update $this->TABLE set family_index = family_index + $difference where familyId = $person->familyId and family_index between ($personIndex - $bigNum) and ($personLastIndex - $bigNum) and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
            } elseif ($personIndex > $toPersonIndex) {
                //                var_dump(3333);
                $bigNum = 10000000;
                # 先将迁移部分整体减去一个非常大的数字，用来区分
                $sql = "update $this->TABLE set family_index = family_index - $bigNum  where familyId = $person->familyId and family_index between $personIndex and $personLastIndex and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
                # 再更新被迁移处，把family_index整体加上迁移人物的总数
                $sql = "update $this->TABLE set family_index = family_index + $personCount where familyId = $person->familyId and family_index < $personIndex and family_index > $toPersonIndex and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
                # 再还原大整数并减去差值
                $difference = $bigNum - ($personIndex - $toPersonIndex - 1);
                $sql = "update $this->TABLE set family_index = family_index + $difference where familyId = $person->familyId and family_index between ($personIndex - $bigNum) and ($personLastIndex - $bigNum) and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
            }


            $personFatherIdArray = [];  // 当前人物的父亲
            $personMotherIdArray = [];  // 当前人物的母亲
            // 获取toPerson的性别
            if ($toPerson->gender == 1) {
                $fatherIdArray[] = $toPerson->id;
                $motherIdArray = $toPerson->spouse;
            } else {
                $motherIdArray[] = $toPerson->id;
                $fatherIdArray = $toPerson->spouse;
            }

            $personBrotherArray = $toPerson->son;   // 当前人物的兄弟
            $personSisterArray = $toPerson->daughter;   // 当前人物的姐妹

            // 根据当前人物的性别更新兄弟姐妹
            if ($person->gender == 1) {
                array_push($personBrotherArray, $person->id);
            } else {
                array_push($personSisterArray, $person->id);
            }

            $personFatherStr = Util::arrayToRelation($fatherIdArray);
            $personMotherStr = Util::arrayToRelation($motherIdArray);
            $personBrotherStr = Util::arrayToRelation($personBrotherArray);
            $personSisterStr = Util::arrayToRelation($personSisterArray);

            // 更新当前人物的父/母, 兄弟姐妹
            $sql = "UPDATE $this->TABLE SET father = '$personFatherStr', mother = '$personMotherStr', brother = '$personBrotherStr', sister = '$personSisterStr',update_time = now() WHERE id = '$personId' ";
            $this->pdo->update($sql);

            // 更新目标人物的子女

            if ($person->gender == 1) {
                // 更新儿子
                $toPersonSonArray = $toPerson->son;
                $toPersonSonArray[] = $person->id;
                $toPersonSonStr = Util::arrayToRelation($toPersonSonArray);
                $sql = "UPDATE $this->TABLE SET son = '$toPersonSonStr',update_time = now() WHERE id = '$toPersonId' ";
                $this->pdo->update($sql);

                // 更新目标人物配偶的儿子
                foreach ($toPerson->spouse as $spouseId) {
                    $sql = "UPDATE $this->TABLE SET son = '$toPersonSonStr',update_time = now() WHERE id = '$spouseId' ";
                    $this->pdo->update($sql);
                }
            } else {
                // 更新女儿
                $toPersonDaughterArray = $toPerson->daughter;
                $toPersonDaughterArray[] = $person->id;
                $toPersonDaughterStr = Util::arrayToRelation($toPersonDaughterArray);
                $sql = "UPDATE $this->TABLE SET daughter = '$toPersonDaughterStr',update_time = now() WHERE id = '$toPersonId' ";
                $this->pdo->update($sql);

                // 更新目标人物配偶的女儿
                foreach ($toPerson->spouse as $spouseId) {
                    $sql = "UPDATE $this->TABLE SET daughter = '$toPersonDaughterStr',update_time = now() WHERE id = '$spouseId' ";
                    $this->pdo->update($sql);
                }
            }

            // 更新目标人物子女的兄弟姐妹

            if ($person->gender == 1) {
                $key = 'brother';
            } else {
                $key = 'sister';
            }

            foreach ($toPerson->son as $sonId) {
                $str = '@' . $person->id . '|';
                $sql = "UPDATE $this->TABLE SET $key = CONCAT($key,\"$str\"),update_time = now() WHERE id = '$sonId' ";
                $this->pdo->update($sql);
            }

            foreach ($toPerson->daughter as $daughterId) {
                $str = '@' . $person->id . '|';
                $sql = "UPDATE $this->TABLE SET $key = CONCAT($key,\"$str\"),update_time = now() WHERE id = '$daughterId' ";
                $this->pdo->update($sql);
            }

            // 删除原来留存的父母，兄弟姐妹关系
            // $setParam = REPLACE($setParam,'$from','$to')";
            $from = '@' . $person->id . '|';
            $to = '';
            $parentArray = array_merge($person->father, $person->mother);
            foreach ($parentArray as $parentId) {
                if ($person->gender == 1) {
                    $key = "son";
                } else {
                    $key = "daughter";
                }
                $sql = "UPDATE $this->TABLE SET $key = REPLACE($key, '$from', '$to'),update_time = now() WHERE id = '$parentId' ";

                $this->pdo->update($sql);
            }

            $siblingArray = array_merge($person->sister, $person->brother);
            foreach ($siblingArray as $siblingId) {

                // 排除自己
                if ($siblingId == $person->id) {
                    continue;
                }

                if ($person->gender == 1) {
                    $key = "brother";
                } else {
                    $key = "sister";
                }
                $sql = "UPDATE $this->TABLE SET $key = REPLACE($key, '$from', '$to'),update_time = now() WHERE id = '$siblingId' ";

                $this->pdo->update($sql);
            }

            // 更新��有改动人物的level
            $levelChange = $toPerson->level + 1 - $person->level;
            if ($levelChange != 0) {
                // level发生了改变
                $needChangePersonArray[] = $person->id;

                while (count($needChangePersonArray) > 0) {

                    // 检查toPersonId在不在这个集合内，如果在，则表示两个人物是同一个分支下的
                    // 0 在这里也会判断为false，所以需要使用!==
                    if (array_search($toPersonId, $needChangePersonArray) !== false) {
                        $needTransaction ? $this->pdo->rollback() : null;
                        return false;
                    }

                    $arrStr = Util::arrayToString($needChangePersonArray);
                    $sql = "UPDATE $this->TABLE SET level = level + $levelChange,update_time = now() WHERE id in ($arrStr) ";

                    $this->pdo->update($sql);

                    $tmpArr = array();
                    $spouseArr = array();

                    foreach ($needChangePersonArray as $changePersonId) {
                        $sql = "SELECT son,daughter,spouse FROM $this->TABLE WHERE id = '$changePersonId' ";
                        $result = $this->pdo->uniqueResult($sql);
                        if ($result != null) {
                            $sons = Util::relationSplit($result['son']);
                            $daughters = Util::relationSplit($result['daughter']);
                            $spouses = Util::relationSplit($result['spouse']);

                            $spouseArr = array_merge($spouseArr, $spouses);
                            $tmpArr = array_merge($tmpArr, $sons, $daughters);
                        }
                    }

                    // 更新spouse的level
                    if (count($spouseArr) > 0) {
                        // 检查toPersonId在不在这个集合内，如果在，则表示两个人物是同一个分支下的
                        if (array_search($toPersonId, $spouseArr) !== false) {
                            $needTransaction ? $this->pdo->rollback() : null;
                            return false;
                        }
                        $spouseArrStr = Util::arrayToString($spouseArr);
                        $sql = "UPDATE $this->TABLE SET level = level + $levelChange,update_time = now() WHERE id in ($spouseArrStr) ";
                        $this->pdo->update($sql);
                    }

                    $needChangePersonArray = $tmpArr;
                }
            }

            # 更新旧人物的ranking为目前排行的最大ranking+1
            if ($ranking)
                $rankings = $this->getChildrenRanking($personId, true);
            $ranking = $rankings ? end($rankings) : 0;
            $sql = "update $this->TABLE set ranking = $ranking + 1 where id = $personId";
            $updateRanking = $this->pdo->update($sql);
            if ($updateRanking <= 0) {
                $needTransaction ? $this->pdo->rollback() : null;
                return false;
            }

            // 更新族群level
            $familyDB = new CDBFamily();
            $familyDB->autoUpdateFamilyLevel($person->familyId);


            $needTransaction ? $this->pdo->commit() : null;


            return true;

            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $needTransaction ? $this->pdo->rollback() : null;
            return false;
            //@codeCoverageIgnoreEnd
        }
    }


    /**
     * v3
     * v3
     * v3新加了对familyIndex的调整
     * 移动人物到另一个节点下，也就是将某一支迁移到另一个祖先下
     * @param int $personId 人物id
     * @param int $toPersonId 迁移到的人物id
     * @return bool 迁移成功或失败
     */
    public function movePersonV3ForAdoption($personId, $toPersonId, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $person = $this->getPersonOBjectById($personId);
        $toPerson = $this->getPersonOBjectById($toPersonId);

        //记录人物日志
        //$person = $this->getPersonById($personId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person->familyId;
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '过继人物子嗣迁移分支至新节点';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['toPerson' => $toPerson];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
        if ($person == null || $toPerson == null) {
            return false;
        }

        // 检查person和toPerson不是同一个分支


        // 移动分支的流程如下:
        // 1. 将当前人物的父/母关系转移到目标人物下, 并更新兄弟姐妹
        // 2. 更新目标人物的子/女关系以及配偶的子/女关系
        // 3. 更新目标人物其他子女的兄弟姐妹
        // 4. 删除原来留存的父母/兄弟姐妹关系
        // 5. 移动时level会变化,需要改动
        try {
            $needTransaction ? $this->pdo->beginTransaction() : null;

            # 更新familyIndex
            $personIndex = $person->familyIndex + 1;
            $personLastIndex = $this->getLastPersonFamilyIndexV3($person->familyId, $person->level, $personIndex);
            $personCount = $personLastIndex - $personIndex + 1;
            $toPersonIndex = $toPerson->familyIndex;
            //            print_r([$toPersonIndex,'qqqq']);
            if ($toPerson->son || $toPerson->daughter) {
                $sqlBrotherSister = "select id,family_index as  familyIndex,level from $this->TABLE where id in ( " . implode(',', array_merge($toPerson->son, $toPerson->daughter)) . " ) and is_delete = 0 order by ranking desc, gender desc ,create_time desc limit 1";
                $brotherAndSister = $this->pdo->uniqueResult($sqlBrotherSister);


                $toPersonIndex = $this->getLastPersonFamilyIndexV3($person->familyId, $brotherAndSister['level'], $brotherAndSister['familyIndex']);
                //                var_dump($toPersonIndex);
            }

            if ($personIndex < $toPersonIndex) {
                //                var_dump(222222);
                $bigNum = 10000000;
                # 先将迁移部分整体减去一个非常大的数字，用来区分
                $sql = "update $this->TABLE set family_index = family_index - $bigNum  where familyId = $person->familyId and family_index between $personIndex and $personLastIndex and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
                # 再更新被迁移处，把family_index整体减去迁移人物的总数
                $sql = "update $this->TABLE set family_index = family_index - $personCount where familyId = $person->familyId and family_index > $personLastIndex and family_index <= $toPersonIndex and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
                # 先更新迁移的部分，把family_index整体加上差值和大整数
                $difference = $bigNum + $toPersonIndex - $personLastIndex;
                $sql = "update $this->TABLE set family_index = family_index + $difference where familyId = $person->familyId and family_index between ($personIndex - $bigNum) and ($personLastIndex - $bigNum) and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
            } elseif ($personIndex > $toPersonIndex) {
                //                var_dump(3333);
                $bigNum = 10000000;
                # 先将迁移部分整体减去一个非常大的数字，用来区分
                $sql = "update $this->TABLE set family_index = family_index - $bigNum  where familyId = $person->familyId and family_index between $personIndex and $personLastIndex and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
                # 再更新被迁移处，把family_index整体加上迁移人物的总数
                $sql = "update $this->TABLE set family_index = family_index + $personCount where familyId = $person->familyId and family_index < $personIndex and family_index > $toPersonIndex and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
                # 再还原大整数并减去差值
                $difference = $bigNum - ($personIndex - $toPersonIndex - 1);
                $sql = "update $this->TABLE set family_index = family_index + $difference where familyId = $person->familyId and family_index between ($personIndex - $bigNum) and ($personLastIndex - $bigNum) and is_delete = 0";
                //                print_r($sql);
                $updateResult = $this->pdo->update($sql);
            }

            # 变更目标人物的子女以及配偶信息
            $sql = "select son,daughter,spouse from $this->TABLE where id = $personId";
            $children = $this->pdo->uniqueResult($sql);
            $sql = "update $this->TABLE set son = '{$children['son']}', daughter = '{$children['daughter']}', 
                        spouse = '{$children['spouse']}' where id = $toPersonId";
            $this->pdo->update($sql);
            # 变更原人物的配偶的配偶信息
            if ($person->spouse) {
                $spouse = implode(',', $person->spouse);
                $sql = "update $this->TABLE set spouse = '@$toPersonId|' where id in ($spouse)";
                $this->pdo->update($sql);
            }
            # 变更原人物子女的本家父母的信息
            if ($person->gender == 0) {
                $mothers = implode(',', $person->mother);
                $sql = "update $this->TABLE set mother = '@$toPersonId|' where id in ($mothers)";
                $this->pdo->update($sql);
            } else {
                $fathers = implode(',', $person->father);
                $sql = "update $this->TABLE set father = '@$toPersonId|' where id in ($fathers)";
                $this->pdo->update($sql);
            }
            # 变更原人物的子女配偶信息
            $sql = "update $this->TABLE set son = '', daughter = '', spouse = '' where id = $personId";
            $this->pdo->update($sql);


            //            // 更新族群level
            //            $familyDB = new CDBFamily();
            //            $familyDB->autoUpdateFamilyLevel($person->familyId);


            $needTransaction ? $this->pdo->commit() : null;


            return true;

            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $needTransaction ? $this->pdo->rollback() : null;
            return false;
            //@codeCoverageIgnoreEnd
        }
    }


    /**
     *
     * 人物性别转换
     * @param $personId
     * @param $gender
     * @param $familyId
     * @return updateRow
     * @version 3
     * @author yuanxin
     * @date  2020/08/24
     */
    public function changePersonGender($personId, $familyId, $gender)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }

        $sql = "UPDATE $this->TABLE	SET gender  =  $gender,update_time = now() WHERE id = $personId AND  is_delete = 0";
        return $this->pdo->update($sql);
    }


    /**
     * 根据当前人物的level , familyId,familyIndex 查出当前人物深度遍历下最后一人的familyIndex
     * 1. 权限的判断
     * 2.人物添加的位置判断
     * 3. 家族合并被覆盖的人物范围,当前人物的孩子节点数
     * @param familyId
     * @param corePersonLevel
     * @param corePersonFamilyIndex
     * @return int familyIndex
     */
    public function getLastPersonFamilyIndexV3($familyId, $corePersonLevel, $corePersonFamilyIndex)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($corePersonFamilyIndex >= 0) {
            $sql = "SELECT min(family_index) as familyIndex
            FROM $this->TABLE 
        WHERE familyId = $familyId and is_delete = 0  and type = 1 and  level <= $corePersonLevel and  family_index > $corePersonFamilyIndex ";
        } else {
            $sql = "SELECT min(family_index) as familyIndex
            FROM $this->TABLE 
        WHERE familyId = $familyId and is_delete = 0  and type = 1 and  level <= $corePersonLevel and  family_index > $corePersonFamilyIndex and family_index < 0";
        }

        $lastPersonFamilyIndex = $this->pdo->uniqueResult($sql);
        if ($lastPersonFamilyIndex['familyIndex'] === null) {
            if ($corePersonFamilyIndex >= -1) {

                $sql = "SELECT family_index as familyIndex FROM $this->TABLE WHERE familyId = $familyId  and is_delete = 0   order by family_index desc limit 1;";
                $lastPersonFamilyIndex = $this->pdo->uniqueResult($sql);
                return $lastPersonFamilyIndex['familyIndex'];
            } else {

                $sql = "SELECT family_index as familyIndex FROM $this->TABLE WHERE familyId = $familyId  and is_delete = 0  and family_index < 0 order by family_index desc limit 1;";
                $lastPersonFamilyIndex = $this->pdo->uniqueResult($sql);
                return $lastPersonFamilyIndex['familyIndex'];
            }

            //                        return $corePersonFamilyIndex;
        } else {
            return $lastPersonFamilyIndex['familyIndex'] - 1;
        }
    }


    /**
     *
     * 两点溯源
     * A的index 一定小于B 在调用时做好判断
     * @param personIndexA
     * @param personIndexB
     * @param familyId
     * @return  person
     * @version 3
     * @date 2020/08/24
     * @author  yuanxin
     */
    public function getTwoPointsSource($personIndexA, $personIndexB, $familyId)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }

        $sql = "SELECT min(family_index) as familyIndex  
                            FROM     $this->TABLE  
                           WHERE  family_index between  $personIndexA  AND  $personIndexB AND familyId = $familyId AND type = 1 AND is_delete = 0 
                           GROUP BY level  ORDER BY  level  ASC  limit 1;";
        return $this->pdo->query($sql);
    }

    /**
     *
     * 获取人物
     * @param personIndex int
     * @param familyId int
     * @return  person bigint
     * @author  yuanxin
     * @version 3
     * @date 2020/08/24
     */
    public function getPersonByFamilyIndex($personIndex, $familyId)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $sql = "SELECT id,father,mother,level,family_index AS familyIndex
                            FROM     $this->TABLE  
                           WHERE  family_index  = $personIndex AND familyId = $familyId AND is_delete = 0 ;";
        $result = $this->pdo->query($sql);
        $person = new Person($result[0]);
        $parser = new RelationParser($person);
        $person->father = $parser->getFathers();
        $person->mother = $parser->getMothers();
        return $person;
    }

    /**
     *  修改人物子女兄弟姐妹信息
     * @param person
     * @return updateRow
     * @version 3
     * @date 2020/08/25
     * @author  yuanxin
     */
    public function modifyPersonRelation($person)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $updateDaughter = '';
        $updateSon = '';
        $updateBrother = '';
        $updateSister = '';
        foreach ($person->sister as $sister) {
            $updateSister = $updateSister . '@' . $sister . '|';
        }
        foreach ($person->brother as $brother) {
            $updateBrother = $updateBrother . '@' . $brother . '|';
        }
        foreach ($person->daughter as $daughter) {
            $updateDaughter = $updateDaughter . '@' . $daughter . '|';
        }
        foreach ($person->son as $son) {
            $updateSon = $updateSon . '@' . $son . '|';
        }

        $sql = "UPDATE  $this->TABLE  SET  daughter =  '$updateDaughter' ,son = '$updateSon',brother = '$updateBrother',sister = '$updateSister',update_time = now()
                         WHERE  id = $person->id  and  is_delete = 0;";
        return $this->pdo->update($sql);
    }

    /**
     * 根据当前人物的level , familyId,familyIndex 查出当前人物深度遍历下最后一人的familyIndex
     * 1. 权限的判断
     * 2.人物添加的位置判断
     * 3. 家族合并被覆盖的人物范围,当前人物的孩子节点数
     * @param familyId
     * @param corePersonLevel
     * @param corePersonFamilyIndex
     * @return int familyIndex
     */
    public function updateIsNotMainByFamilyIndex(Person $person, int $isNotMain, int $updateBy)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $lastIndex = $this->getLastPersonFamilyIndexV3($person->familyId, $person->level, $person->familyIndex);

        $sql = "update $this->TABLE set isNotMain = $isNotMain,update_by =$updateBy, update_time = now() where familyId = $person->familyId and is_delete = 0 and 
                family_index > $person->familyIndex and family_index <= $lastIndex";

        return $this->pdo->update($sql);
    }

    /**
     * v3
     * v3
     * v3
     * 向家族中添加带有层次级别的人物,添加时根据性别，向其姐妹或者兄弟字段中更新自己的id
     * 只用于复制家族分支时
     * @param $person 该人物
     * @param $level    该人物的级别
     * @return  int  人物的id或者失败返回-1
     */
    private function addPersonForCopyV3($person, $level, $startIndex, $type = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $userId = $GLOBALS['userId'];

        $shareUrl = md5($person->name . Util::generateRandomCode(24));    //生成唯一share_url的code

        if ($person->birthday == null || $person->birthday == '') {
            $person->birthday = "NULL";
        } else {
            $person->birthday = "'$person->birthday'";
        }
        if ($person->deadTime == null || $person->deadTime == '') {
            $person->deadTime = "NULL";
        } else {
            $person->deadTime = "'$person->deadTime'";
        }
        if ($person->infoCardId == null || $person->infoCardId == '') {
            $person->infoCardId = 'NULL';
        } else {
            $person->infoCardId = "'$person->infoCardId'";
        }
        if ($person->confirm == null || $person->confirm == '') {
            $person->confirm = 'NULL';
        } else {
            $person->confirm = "'$person->confirm'";
        }

        //sister,brother,
        if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
            $brothersql = " '@$person->id|', '',";
        } else if ($person->gender == $GLOBALS['GENDER_MALE']) {
            $brothersql = " '', '@$person->id|',";
        }

        //原person对象没有的 ： infoCardId， confirm, qrcode
        //same_personId,father,mother,son,daughter,spouse,
        //sister,brother,
        $sql = "INSERT INTO $this->TABLE(
            id, zi,zpname,remark,branchId,name,type,
            profileText,sideText,country,country_name,province,province_name,
            city,city_name,area,area_name,town,town_name,address,
            birthday,gender,photo,share_url,is_dead,dead_time,level,
            infoCardId,is_delete,is_adoption,ref_familyId,ref_personId,
            sister,brother,
            confirm,blood_type,marital_status,phone,qq,qrcode,
            familyId,userId,ranking,create_by,create_time,update_by,update_time,family_index            
            )
       VALUES(
           '$person->id', '$person->zi','$person->zpname','$person->remark','$person->branchId','$person->name','$type',
           '$person->profileText','$person->sideText','$person->country','$person->countryName','$person->province','$person->provinceName',
           '$person->city','$person->cityName','$person->area','$person->areaName','$person->town','$person->townName','$person->address',
           $person->birthday,'$person->gender','$person->photo','$shareUrl','$person->isDead',$person->deadTime,'$level',
           $person->infoCardId, '0', '$person->isAdoption', '$person->refFamilyId', '$person->refPersonId',
           $brothersql
           $person->confirm, '$person->bloodType', '$person->maritalStatus', '$person->phone', '$person->qq', '$person->qrcode',
           '$person->familyId','$person->userId','$person->ranking','$userId',now(),'$userId',now(),$startIndex
           )";

        $res = $this->pdo->insert($sql);

        if ($res >= 0) {        //返回插入人物的id,失败则返回-1
            $id = $person->id;
        } else {
            $id = -1;
        }

        return $id;

        //更新家族成员数 不在这里更新人数 统一在插入后更新人数
    }


    /**
     * v3
     * v3
     * v3
     * 向家族中添加相关联的人物
     * @param $familyId    家族id
     * @param $name        姓名
     * @param $gender        性别
     * @param $address        地址
     * @param $birthday        生日
     * @param $photo        相片
     * @param $relation        关系
     * @param $corePersonId 关系人物id
     * @param $needTransaction  是否需要事务,true代表需要(默认)，false代表不需要
     * @return int 添加的人物id或者-1代表记录插入出错，-2代表插入的人物不属于该家族
     */
    public function  addRelatePersonV3($person, $startIndex, $needTransaction = true, $isCopyed = false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $corePerson = $this->getPersonById($person->corePersonId);                            //获取Person对象

        $userId = $GLOBALS['userId'];

        $corePersonLevel = $corePerson->level;
        $level = $corePersonLevel;                                //默认是平辈的
        if ($person->relation == $GLOBALS['RELATION_FATHER'] || $person->relation == $GLOBALS['RELATION_MOTHER']) {
            $level--;            //辈份越高,level越小
        } else if ($person->relation == $GLOBALS['RELATION_SON'] || $person->relation == $GLOBALS['RELATION_DAUGHTER']) {
            $level++;            //辈份越低,level越大
        }

        //非核心人物只能加子女 (这里的核心人物指的是type=1，即是本家，type为2是配偶)
        if (($person->relation != $GLOBALS['RELATION_SON'] && $person->relation != $GLOBALS['RELATION_DAUGHTER']) && $corePerson->type == 2) {
            //添加的人物不属于这个家族，返回错误
            return -2;
        }

        $familyDB = new CDBFamily();
        $family = $familyDB->getFamilyById($corePerson->familyId);
        //$memberCount = $family->memberCount;    // 族群中的人物
        //如果是复制的操作，则不需要改变当前人物type 也可以写在下面的判断，这里就是减少一步数据库查询（查询出来的membercount不是实时数据）
        $memberCount = $isCopyed ? 2 : $family->memberCount;
        $needChangeCorePersonType = false;      // 是否需要改变核心人物的type

        $personType = 1;
        //判断得到当前添加人物的type
        if ($person->relation == $GLOBALS['RELATION_SPOUSE']) {
            if ($memberCount == 1) {
                // 添加的是妻系的，将原来的人物的type变为2
                //@codeCoverageIgnoreStart
                $needChangeCorePersonType = true;
                $personType = 1;
                //@codeCoverageIgnoreEnd
            } else {
                if ($corePerson->type == 1) {
                    $personType = 2;
                } else {
                    $personType = 1;
                }
            }
        } else if ($person->relation == $GLOBALS['RELATION_MOTHER']) {

            //如果是1，母系
            if ($memberCount == 1) {
                //@codeCoverageIgnoreStart
                $personType = 1;
                //@codeCoverageIgnoreEnd
            } else if ($memberCount == 2) {
                //判断是否是妻子的母系
                //@codeCoverageIgnoreStart
                $userPerson = $this->getPersonByFamilyIdAndUserId($corePerson->familyId, $userId);
                if (count($userPerson->spouse) == 1 && $userPerson->spouse[0] == $person->corePersonId) {
                    // corePerson是配偶
                    $personType = 1;
                } else {
                    $personType = 2;
                }
                //@codeCoverageIgnoreEnd
            } else {
                $personType = 2;
            }
        } else if ($person->relation == $GLOBALS['RELATION_FATHER']) {
            //添加父亲，需要判断母亲type是否为1
            if (count($corePerson->mother) > 0) {
                $mother = $this->getPersonById($corePerson->mother[0]);
                if ($mother != null) {
                    if ($mother->type == 1) {
                        //@codeCoverageIgnoreStart
                        $personType = 2;
                        //@codeCoverageIgnoreEnd
                    }
                }
            }
        } else {
            $personType = 1;
        }

        /*******同时也要更新家族的level标志**************/
        $changed = FALSE;
        $minLevel = $family->minLevel;
        $maxLevel = $family->maxLevel;
        if ($level < $minLevel) {
            $minLevel = $level;
            $changed = TRUE;
        }
        if ($level > $maxLevel) {
            $maxLevel = $level;
            $changed = TRUE;
        }

        try {

            //开启事务处理
            if ($needTransaction)
                $this->pdo->beginTransaction();


            if ($needChangeCorePersonType) {
                // 改变中心人物的type
                //@codeCoverageIgnoreStart
                $this->changePersonType($person->corePersonId, 2);
                //@codeCoverageIgnoreEnd
            }

            if ($changed)
                $familyDB->updateFamilyLevel($person->familyId, $minLevel, $maxLevel);

            /*********家族中的level标志更新完毕*********/


            if ($isCopyed == true) {
                $personId = $this->addPersonForCopyV3($person, $level, $startIndex, $personType); //复制人物独有的方法来增加人物
            } else {
                $personId = $this->addPersonWithLevel($person, $level, $personType); //增加要增加的人物
            }

            if ($personId <= 0) {
                return -1;
            }
            $records = array();                                                                //需要更新的人物记录数组
            $count = 0;

            switch ($person->relation) {

                case $GLOBALS['RELATION_FATHER']:
                    //添加自己和父亲关系的记录
                    $meFatherRecord = new PersonRecord($person->corePersonId, "father", $personId);
                    $records[$count] = $meFatherRecord;                        //自己和父亲的记录
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $fatherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $fatherMeRecord;                        //父亲和我的关系记录
                        $count++;
                    }

                    //更新父亲的儿子和兄弟的父亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $fatherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $fatherSonRecord;
                        $count++;

                        $brotherFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $brotherFatherRecord;
                        $count++;
                    }

                    //更新父亲的女儿和姐妹的父亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $fatherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;

                        $sisterFatherRecord = new PersonRecord($value, "father", $personId);
                        $records[$count] = $sisterFatherRecord;
                        $count++;
                    }

                    $updateSpouseId = array();

                    //更新父亲的配偶和母亲的配偶
                    foreach ($corePerson->mother as $key => $value) {
                        $fatherSpouseRecord = new PersonRecord($personId, "spouse", $value);
                        $records[$count] = $fatherSpouseRecord;
                        $count++;

                        $motherSpouseRecord = new PersonRecord($value, "spouse", $personId);
                        $records[$count] = $motherSpouseRecord;
                        $count++;

                        //更新配偶的婚姻状态
                        array_push($updateSpouseId, $value);
                    }

                    array_push($updateSpouseId, $personId);
                    $this->updateSpouseStatus($updateSpouseId);
                    break;
                case $GLOBALS['RELATION_MOTHER']:
                    //添加自己和母亲关系的记录
                    $meMotherRecord = new PersonRecord($person->corePersonId, "mother", $personId);
                    $records[$count] = $meMotherRecord;
                    $count++;

                    /****根据我的性别判定是儿子还是女儿****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $motherMeRecord = new PersonRecord($personId, "son", $person->corePersonId);
                        $records[$count] = $motherMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $motherMeRecord = new PersonRecord($personId, "daughter", $person->corePersonId);
                        $records[$count] = $motherMeRecord;
                        $count++;
                    }


                    //更新母亲的儿子和兄弟的母亲
                    foreach ($corePerson->brother as $key => $value) {
                        //兄弟中存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $motherSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $motherSonRecord;
                        $count++;

                        $brotherMotherRecord = new PersonRecord($value, "mother", $personId);
                        $records[$count] = $brotherMotherRecord;
                        $count++;
                    }

                    //更新母亲的女儿和姐妹的母亲
                    foreach ($corePerson->sister as $key => $value) {
                        //姐妹中存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $motherDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $motherDaughterRecord;
                        $count++;

                        $sisterMotherRecord = new PersonRecord($value, "mother", $personId);
                        $records[$count] = $sisterMotherRecord;
                        $count++;
                    }

                    $updateSpouseId = array();

                    //更新母亲的配偶和父亲的配偶
                    foreach ($corePerson->father as $key => $value) {
                        $motherSpouseRecord = new PersonRecord($personId, "spouse", $value);
                        $records[$count] = $motherSpouseRecord;
                        $count++;

                        $fatherSpouseRecord = new PersonRecord($value, "spouse", $personId);
                        $records[$count] = $fatherSpouseRecord;
                        $count++;
                        //更新配偶的婚姻状态
                        array_push($updateSpouseId, $value);
                    }
                    array_push($updateSpouseId, $personId);
                    //更新婚姻状态
                    $this->updateSpouseStatus($updateSpouseId);

                    break;
                case $GLOBALS['RELATION_BROTHER']:
                    //添加自己的兄弟关系的记录
                    $meBrotherRecord = new PersonRecord($person->corePersonId, "brother", $personId);
                    $records[$count] = $meBrotherRecord;
                    $count++;

                    /****根据我的性别判定是兄弟还是姐妹****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $brotherMeRecord = new PersonRecord($personId, "brother", $person->corePersonId);
                        $records[$count] = $brotherMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $brotherMeRecord = new PersonRecord($personId, "sister", $person->corePersonId);
                        $records[$count] = $brotherMeRecord;
                        $count++;
                    }


                    //更新兄弟的父亲和父亲的儿子
                    foreach ($corePerson->father as $key => $value) {
                        $brotherFatherRecord = new PersonRecord($personId, "father", $value);
                        $records[$count] = $brotherFatherRecord;
                        $count++;

                        $fatherSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $fatherSonRecord;
                        $count++;
                    }

                    //更新兄弟的母亲和母亲的儿子
                    foreach ($corePerson->mother as $key => $value) {
                        $brotherMotherRecord = new PersonRecord($personId, "mother", $value);
                        $records[$count] = $brotherMotherRecord;
                        $count++;

                        $motherSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $motherSonRecord;
                        $count++;
                    }

                    //更新兄弟的兄弟和兄弟的兄弟
                    foreach ($corePerson->brother as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $brotherBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $brotherBrotherRecord;
                        $count++;

                        $brotherBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $brotherBrotherRecord;
                        $count++;
                    }

                    //更新兄弟的姐妹和姐妹的兄弟
                    foreach ($corePerson->sister as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $brotherSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $brotherSisterRecord;
                        $count++;

                        $sisterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sisterBrotherRecord;
                        $count++;
                    }

                    break;
                case $GLOBALS['RELATION_SISTER']:
                    //添加自己的姐妹关系的记录
                    $meSisterRecord = new PersonRecord($person->corePersonId, "sister", $personId);
                    $records[$count] = $meSisterRecord;
                    $count++;

                    /****根据我的性别判定是兄弟还是姐妹****/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sisterMeRecord = new PersonRecord($personId, "brother", $person->corePersonId);
                        $records[$count] = $sisterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sisterMeRecord = new PersonRecord($personId, "sister", $person->corePersonId);
                        $records[$count] = $sisterMeRecord;
                        $count++;
                    }

                    //更新姐妹的父亲和父亲的女儿
                    foreach ($corePerson->father as $key => $value) {
                        $sisterFatherRecord = new PersonRecord($personId, "father", $value);
                        $records[$count] = $sisterFatherRecord;
                        $count++;

                        $fatherDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $fatherDaughterRecord;
                        $count++;
                    }

                    //更新姐妹的母亲和母亲的女儿
                    foreach ($corePerson->mother as $key => $value) {
                        $sisterMotherRecord = new PersonRecord($personId, "mother", $value);
                        $records[$count] = $sisterMotherRecord;
                        $count++;

                        $motherDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $motherDaughterRecord;
                        $count++;
                    }

                    //更新姐妹的兄弟和兄弟的姐妹
                    foreach ($corePerson->brother as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            continue;
                        $sisterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sisterBrotherRecord;
                        $count++;

                        $brotherSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $brotherSisterRecord;
                        $count++;
                    }

                    //更新姐妹的姐妹和姐妹的姐妹
                    foreach ($corePerson->sister as $key => $value) {
                        //存在自己则跳过
                        if ($value == $person->corePersonId)
                            //@codeCoverageIgnoreStart
                            continue;
                        //@codeCoverageIgnoreEnd
                        $sisterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sisterSisterRecord;
                        $count++;

                        $sisterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sisterSisterRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_SPOUSE']:
                    //添加自己和配偶关系的记录
                    $meSpouseRecord = new PersonRecord($person->corePersonId, "spouse", $personId);
                    $records[$count] = $meSpouseRecord;
                    $count++;

                    $spouseMeRecord = new PersonRecord($personId, "spouse", $person->corePersonId);
                    $records[$count] = $spouseMeRecord;
                    $count++;

                    //更新配偶的儿子和儿子的父母
                    foreach ($corePerson->son as $key => $value) {
                        $spouseSonRecord = new PersonRecord($personId, "son", $value);
                        $records[$count] = $spouseSonRecord;
                        $count++;

                        /*****根据配偶的性别判断******/
                        if ($person->gender == $GLOBALS['GENDER_MALE']) {
                            $sonFatherRecord = new PersonRecord($value, "father", $personId);
                            $records[$count] = $sonFatherRecord;
                            $count++;
                        } else if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonMotherRecord = new PersonRecord($value, "mother", $personId);
                            $records[$count] = $sonMotherRecord;
                            $count++;
                        }
                    }

                    //更新配偶的女儿和女儿的父母
                    foreach ($corePerson->daughter as $key => $value) {
                        $spouseDaughterRecord = new PersonRecord($personId, "daughter", $value);
                        $records[$count] = $spouseDaughterRecord;
                        $count++;

                        /*****根据配偶的性别判断******/
                        if ($person->gender == $GLOBALS['GENDER_MALE']) {
                            //@codeCoverageIgnoreStart
                            $daughterFatherRecord = new PersonRecord($value, "father", $personId);
                            $records[$count] = $daughterFatherRecord;
                            $count++;
                            //@codeCoverageIgnoreEnd
                        } else if ($person->gender == $GLOBALS['GENDER_FEMALE']) {
                            $daughterMotherRecord = new PersonRecord($value, "mother", $personId);
                            $records[$count] = $daughterMotherRecord;
                            $count++;
                        }
                    }

                    //更新婚姻状态
                    $this->updateSpouseStatus(array($personId, $person->corePersonId));

                    break;
                case $GLOBALS['RELATION_SON']:
                    //添加自己和儿子关系的记录
                    $meSonRecord = new PersonRecord($person->corePersonId, "son", $personId);
                    $records[$count] = $meSonRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $sonMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $sonMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $sonMeRecord;
                        $count++;
                    }


                    //更新儿子的父或母亲和配偶的儿子
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                            $records[$count] = $sonSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd

                        $spouseSonRecord = new PersonRecord($value, "son", $personId);
                        $records[$count] = $spouseSonRecord;
                        $count++;
                    }

                    //找出我的配偶的配偶  父亲->母亲1（corePerson)->母亲2
                    //TODO 不确定是否正确
                    if (count($corePerson->spouse) == 1) {
                        $mySpouse = $this->getPersonById($corePerson->spouse[0]);
                        foreach ($mySpouse->spouse as $key => $value) {
                            if ($value != $corePerson->id) {  //排除自己
                                //@codeCoverageIgnoreStart
                                /*****根据我的性别判断******/
                                if ($mySpouse->gender == $GLOBALS['GENDER_MALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                }

                                $spouseSonRecord = new PersonRecord($value, "son", $personId);
                                $records[$count] = $spouseSonRecord;
                                $count++;
                                //@codeCoverageIgnoreEnd
                            }
                        }
                    }


                    //更新儿子的兄弟和儿子的兄弟
                    foreach ($corePerson->son as $key => $value) {
                        $sonBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $sonBrotherRecord;
                        $count++;

                        $sonBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $sonBrotherRecord;
                        $count++;
                    }

                    //更新儿子的姐妹和女儿的兄弟
                    foreach ($corePerson->daughter as $key => $value) {
                        $sonSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $sonSisterRecord;
                        $count++;

                        $daughterBrotherRecord = new PersonRecord($value, "brother", $personId);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;
                    }
                    break;
                case $GLOBALS['RELATION_DAUGHTER']:
                    //添加自己和女儿关系的记录
                    $meDaughterRecord = new PersonRecord($person->corePersonId, "daughter", $personId);
                    $records[$count] = $meDaughterRecord;
                    $count++;

                    /**根据我的性别判断**/
                    if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "father", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                        $daughterMeRecord = new PersonRecord($personId, "mother", $person->corePersonId);
                        $records[$count] = $daughterMeRecord;
                        $count++;
                    }


                    //更新女儿的父或母亲和配偶的女儿
                    foreach ($corePerson->spouse as $key => $value) {
                        /*****根据我的性别判断******/
                        if ($corePerson->gender == $GLOBALS['GENDER_MALE']) {
                            $daughterSpouseRecord = new PersonRecord($personId, "mother", $value);
                            $records[$count] = $daughterSpouseRecord;
                            $count++;
                            //@codeCoverageIgnoreStart
                        } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                            $daughterSpouseRecord = new PersonRecord($personId, "father", $value);
                            $records[$count] = $daughterSpouseRecord;
                            $count++;
                        }
                        //@codeCoverageIgnoreEnd


                        $spouseDaughterRecord = new PersonRecord($value, "daughter", $personId);
                        $records[$count] = $spouseDaughterRecord;
                        $count++;
                    }

                    //找出我的配偶的配偶  父亲->母亲1（corePerson)->母亲2
                    //TODO 不确定是否正确
                    //@codeCoverageIgnoreStart
                    if (count($corePerson->spouse) == 1) {
                        $mySpouse = $this->getPersonById($corePerson->spouse[0]);
                        foreach ($mySpouse->spouse as $key => $value) {
                            if ($value != $corePerson->id) {  //排除自己
                                /*****根据我的性别判断******/
                                if ($mySpouse->gender == $GLOBALS['GENDER_MALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "mother", $value);        //配偶是母亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                } else if ($corePerson->gender == $GLOBALS['GENDER_FEMALE']) {
                                    $sonSpouseRecord = new PersonRecord($personId, "father", $value);        //配偶是父亲
                                    $records[$count] = $sonSpouseRecord;
                                    $count++;
                                }

                                $spouseSonRecord = new PersonRecord($value, "daughter", $personId);
                                $records[$count] = $spouseSonRecord;
                                $count++;
                                //@codeCoverageIgnoreEnd
                            }
                        }
                    }

                    //更新女儿的兄弟和儿子的姐妹
                    foreach ($corePerson->son as $key => $value) {
                        $daughterBrotherRecord = new PersonRecord($personId, "brother", $value);
                        $records[$count] = $daughterBrotherRecord;
                        $count++;

                        $sonSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $sonSisterRecord;
                        $count++;
                    }

                    //更新女儿的姐妹和女儿的姐妹
                    foreach ($corePerson->daughter as $key => $value) {
                        $daughterSisterRecord = new PersonRecord($personId, "sister", $value);
                        $records[$count] = $daughterSisterRecord;
                        $count++;

                        $daughterSisterRecord = new PersonRecord($value, "sister", $personId);
                        $records[$count] = $daughterSisterRecord;
                        $count++;
                    }
                    break;
            }
            if (!$this->updateRecord($records)) {
                //@codeCoverageIgnoreStart
                if ($needTransaction)
                    $this->pdo->rollback();
                return -1;                //表示添加相关记录时出错
                //@codeCoverageIgnoreEnd
            } else {

                // 获取添加的人物的父母，然后查询父母所在的分支，然后更新分支记录
                $createPerson = $this->getPersonById($personId);

                $parents = array_merge($createPerson->father, $createPerson->mother);

                if (count($parents) > 0) {
                    // 判断父母的所在分支
                    $parentId = $parents[0];
                    $branchIds = $this->getPersonBranchList($parentId);
                    foreach ($branchIds as $branchId) {
                        $this->addPersonToInnerBranch($branchId['branchId'], $personId, $userId);
                    }
                }

                if ($needTransaction)
                    $this->pdo->commit();
                return $personId;
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            if ($needTransaction)
                $this->pdo->rollback();
        }
        return -1;
        //@codeCoverageIgnoreEnd
    }


    /**
     * 传参合理情况下
     *  增加双祧、三祧、想怎么祧怎么祧关系的支持
     * @param personId      A是被祧的人物
     * @param fatherList    多选的父亲们
     * @param familyId      需要同一个家族
     * @param biologicalFatherId      生物学父亲
     * @return updateRow
     * @author yuanxin
     */
    public function addBicolorRelate($personId, $fatherList, $familyId, $biologicalFatherId)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }

        try {
            $this->pdo->beginTransaction();
            $child = $this->getPersonById2($personId);
            $biologicalFather = $this->getPersonById2($biologicalFatherId);

            // 先去判断生父下是否有孩子等于选中人物
            if (in_array($personId, $biologicalFather->son)) {
                $sql = "UPDATE  $this->TABLE  SET  bicolor  =  1  WHERE  familyId = $familyId AND  is_delete = 0  AND  id = $personId;";
                $this->pdo->update($sql);
                // 没有分支迁移
            } else {
                $this->movePersonV33($child->id, $biologicalFatherId, false);
                $sql = "UPDATE  $this->TABLE  SET  bicolor  =  1  WHERE  familyId = $familyId AND  is_delete = 0  AND  id = $personId;";
                $this->pdo->update($sql);
            }

            // 建立假人
            foreach ($fatherList as &$father) {

                $newFather = $this->getPersonById2($father);
                $dummyPerson = new Person();
                // $dummyPerson = $child;
                $dummyPerson->id = Util::getNextId();
                $dummyPerson->name = $child->name;
                $dummyPerson->gender = $child->gender;
                $dummyPerson->level = $child->level;
                $dummyPerson->userId = $child->userId;
                $dummyPerson->type = $child->type;
                $dummyPerson->familyId = $child->familyId;
                $dummyPerson->bicolor = $child->id;
                $dummyPerson->familyIndex = $this->getLastPersonFamilyIndex($familyId, $newFather->level, $newFather->familyIndex) + 1;
                $dummyPerson->brother = $biologicalFather->son;
                $dummyPerson->sister = $biologicalFather->daughter;
                $dummyPerson->corePersonId = &$father;

                if ($child->gender == 1) {
                    $dummyPerson->relation = $GLOBALS['RELATION_SON'];
                } else {
                    $dummyPerson->relation = $GLOBALS['RELATION_DAUGHTER'];
                }
                if ($biologicalFather->gender == 1) {
                    $dummyPerson->father = $biologicalFatherId;
                    $dummyPerson->mother = $biologicalFather->spouse;
                } else {
                    $dummyPerson->mother = $biologicalFatherId;
                    $dummyPerson->father = $biologicalFather->spouse;
                }

                $result = $this->addRelatePerson2($dummyPerson, false);
            }

            $this->pdo->commit();
            if (0 < $result) {
                return $result;
            } else {
                $this->pdo->rollback();
                return -1;
            }
        } catch (PDOException $e) {
            //            print_r($e);
            $this->pdo->rollback();
            return false;
        }
    }

    /**
     * 取消祧子
     * @param personId      A是被祧的人物
     * @return updateRow
     * @author yuanxin
     */
    public function cancelBicolor($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $person = $this->getPersonById2($personId);
            $realPersonId = $person->bicolor > 1 ? $person->bicolor : $personId;
            $familyId = $person->familyId;
            $sql = "UPDATE  $this->TABLE SET bicolor =  0  where  familyId = $familyId  AND   is_delete = 0  AND  id = $realPersonId  or  bicolor  = $realPersonId;";
            $this->pdo->update($sql);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }


    public function leaveFamilyAndUnbindPerson(int $userId, int $familyId): bool
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $person = $this->getPersonByFamilyIdAndUserId($familyId, $userId);

            //记录人物日志
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
            $option_detail = '离开家族并解除人物绑定';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['oldUserId' => $userId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($person === null ? 0 : $person->id, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $familyDB = new CDBFamily();
            $deletePermission = $familyDB->deletePermission($familyId, $userId, $option_user);
            if (!$deletePermission) {
                $this->pdo->rollback();
                return false;
            }

            if ($person) {
                $sql = "UPDATE $this->TABLE SET userId = '0',update_time = now(),update_by = $option_user  WHERE id = '$person->id' AND is_delete = '0' ";
                $count = $this->pdo->update($sql);
                if (!$count) {
                    $this->pdo->rollback();
                    return false;
                }
            }

            $familyDB = new CDBFamily();
            $family = $familyDB->getFamilyById($person->familyId);
            $socialCircleId = $family->socialCircleId;
            //删除圈子关注列表中的该用户记录
            $sql = "UPDATE $this->TABLE_INFORMATION_FOLLOW SET isDelete=1 WHERE categoryId='$socialCircleId' AND userId = '$person->userId' AND isDelete=0";
            $this->pdo->update($sql);
            $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow-1 WHERE id='$socialCircleId' AND is_delete=0";
            $this->pdo->update($sql);

            $this->pdo->commit();
            return true;
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            //@codeCoverageIgnoreEnd
        }
    }

    /**
     * 获取当前人物相关信息
     * @param personId
     * @return array
     * @version 3
     * @author yuanxin
     */
    public function getPersonMergeInfoByPersonId($personId)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $personDB = new CDBPerson();
        $person = $personDB->getPersonById2($personId);
        $currentFamilyId = $person->familyId;
        $refFamily = $person->refFamilyId;
        $refPerson = $person->refPersonId;
        $result = array();
        foreach ($refFamily as $key => $family) {
            $sql = "SELECT  merge_way AS mergeWay  FROM $this->TABLE_FAMILY_MERGE 
                                            WHERE  (( src_familyId =  $family AND  merge_familyId =  $currentFamilyId) 
                                                                                        OR (src_familyId = $currentFamilyId AND merge_familyId  = $family))  AND is_delete = 0  AND  (merge_way = 14 OR merge_way = 13)";
            $temp = $this->pdo->query($sql);
            $personId = $refPerson[$key];
            $sql = "SELECT f.name AS familyName,p.name AS personName FROM $this->TABLE p  LEFT JOIN   $this->TABLE_FAMILY   f  ON  p.familyId = f.id  WHERE p.id =  $personId  AND  p.is_delete = 0 AND f.is_delete  = 0";
            $temp2 = $this->pdo->query($sql);
            $result[$key]['mergeWay'] = $temp[0]['mergeWay'];
            $result[$key]['familyId'] = $family;
            $result[$key]['familyName'] = $temp2[0]['familyName'];
            $result[$key]['personId'] = $personId;
            $result[$key]['personName'] = $temp2[0]['personName'];
        }
        return $result;
    }

    /**
     * 家族合并中批量添加人物进新的家族
     * @param persons  所有人物数据
     * @param familyId      新家族id
     * @param userId      进行操作的用户
     * @return bool
     * @author yuanxin
     */
    public function batchPersonAdd($persons, $familyId, $userId)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        try {
            foreach ($persons as $person) {
                $familyIndex = $person['familyIndex'];
                $id = $person['id'];
                $son = '';
                foreach ($person['son'] as $sonId) {
                    $son = $son . '@' . $sonId . '|';
                }
                $daughter = '';
                foreach ($person['daughter'] as $daughterId) {
                    $daughter = $daughter . '@' . $daughterId . '|';
                }
                $brother = '';
                foreach ($person['brother'] as $brotherId) {
                    $brother = $brother . '@' . $brotherId . '|';
                }
                $sister = '';
                foreach ($person['sister'] as $sisterId) {
                    $sister = $sister . '@' . $sisterId . '|';
                }
                $father = '';
                foreach ($person['father'] as $fatherId) {
                    $father = $father . '@' . $fatherId . '|';
                }
                $mother = '';
                foreach ($person['mother'] as $motherId) {
                    $mother = $mother . '@' . $motherId . '|';
                }
                $spouse = '';
                foreach ($person['spouse'] as $spouseId) {
                    $spouse = $spouse . '@' . $spouseId . '|';
                }
                $name = $person['name'];
                $type = $person['type'];
                $gender = $person['gender'];
                $shareUrl = $person['shareUrl'];
                $level = $person['level'];
                $familyId = $person['familyId'];
                $ranking = $person['ranking'];
                $sql = "INSERT INTO 
                                        $this->TABLE(family_index,id, spouse,father,mother,son,daughter,brother,sister,name,type,gender,share_url,level,familyId,userId,ranking,create_by,create_time,update_by,update_time)
                                  VALUES($familyIndex,$id,'$spouse','$father','$mother','$son','$daughter','$brother','$sister','$name',$type,$gender,'$shareUrl',$level,'$familyId',0,$ranking,$userId,now(),'$userId',now())";
                $this->pdo->insert($sql);
            }
            return true;
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
        }
    }

    /**
     * 获取家族五服内的所有人员,供前端家族树使用
     * @param familyId
     * @param showMain 是否显示外亲
     * @param filter  int  是否显示配偶
     * @param userId  用户人物
     * @param personId  中心人物
     * @return array 所有人员的数组
     *
     * @version 3
     * @author yuanxin
     */
    public function getFamilyPersonsLocal($familyId, $userId, $personId = 0, $filter, $showMain = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        // 没有personid默认当前用户
        if ($personId == 0) {
            $userPersonSql = "SELECT  id ,level,family_index as familyIndex    FROM $this->TABLE  WHERE  familyId = $familyId  AND userId = $userId  AND  is_delete =  0;";
        } else {
            $userPersonSql = "SELECT  id ,level,family_index as familyIndex    FROM $this->TABLE  WHERE  familyId = $familyId  AND id = $personId AND  is_delete =  0;";
        }

        $userPerson = $this->pdo->query($userPersonSql);


        // 获取level区间
        $familyLevelSql = "SELECT  min_level,max_level  FROM  $this->TABLE_FAMILY WHERE id = $familyId";
        $family = $this->pdo->query($familyLevelSql);

        // if( array() == $userPerson){
        //     $minLevel = $family[0]['min_level'];
        //     $maxLevel = $minLevel + 4 > $family[0]['max_level']? $family[0]['max_level'] : ($minLevel + 4);
        //     $personIndex = 0;
        // }else{
        //     $minLevel  =  $userPerson[0]['level'] - 4 < $family[0]['min_level']?  $family[0]['min_level']  : ($userPerson[0]['level'] - 4);
        //     $maxLevel  =  $userPerson[0]['level'] + 4 > $family[0]['max_level']?$family[0]['max_level'] : ($userPerson[0]['level'] + 4) ;
        //     $personIndex = $userPerson[0]['familyIndex'];
        // }


        if (array() == $userPerson) {
            $minLevel = $family[0]['min_level'];
            $maxLevel = $minLevel + 3 > $family[0]['max_level'] ? $family[0]['max_level'] : ($minLevel + 3);
            $personIndex = 0;
        } else {
            $minLevel = $userPerson[0]['level'] - 3 < $family[0]['min_level'] ? $family[0]['min_level'] : ($userPerson[0]['level'] - 3);
            $maxLevel = $userPerson[0]['level'] + 3 > $family[0]['max_level'] ? $family[0]['max_level'] : ($userPerson[0]['level'] + 3);
            $personIndex = $userPerson[0]['familyIndex'];
        }


        // 获取顶点信息
        $getFatherSql = "SELECT  max(family_index) as familyIndex  FROM  $this->TABLE WHERE familyId = $familyId AND level = $minLevel  AND family_index <= $personIndex  AND type = 1 AND is_delete = 0 ;";
        //    获取index区间
        $startFamilyIndex = $this->pdo->query($getFatherSql);
        $startFamilyIndex = $startFamilyIndex[0]['familyIndex'];

        $lastFamilyIndex = $this->getLastPersonFamilyIndex($familyId, $minLevel, $startFamilyIndex);

        $familyIndexSql = "AND family_index BETWEEN $startFamilyIndex AND  $lastFamilyIndex ";

        $levelSql = "AND level BETWEEN  $minLevel  AND  $maxLevel";
        $filterSql = "";
        if ($filter == 1) {
            $filterSql = " AND type = 1 ";
        }

        $showMainSql = "";
        if ($showMain === 0) {
            $showMainSql = " AND isNotMain = 0 ";
        }

        $sql = "SELECT  tb.family_index as familyIndex  ,tb.id,tb.name,tb.level,tb.father,tb.mother,tb.gender,tb.userId,tb.ranking,tb.share_url as shareUrl,
                        tb.son,tb.daughter,tb.type,tb.spouse,tb.isNotMain,tb.is_adoption as isAdoption,ref_familyId as refFamilyId,ref_personId as refPersonId,
                        tb.sideText,tb.bicolor,isNotMain
                        FROM 
                        $this->TABLE  tb  
                        WHERE familyId = $familyId  AND is_delete = 0  $filterSql $showMainSql $levelSql  $familyIndexSql order by family_index asc;";

        $result = $this->pdo->query($sql);

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        return $result;
    }

    /**
     * 用户通过邀请，将用户id绑定到指定的人物上
     * @param $userId 用户id
     * @param $person 人物
     * @return int 更新的记录数,-1代表用户已绑定人物
     */
    public function joinAndBindUserToPersonWithPersonId($userId, Person $person, $operator, $userPermission, $isJoin = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0' AND familyId = '$person->familyId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            //表示这个用户绑定家族中某个人
            return -1;
        }

        try {
            $this->pdo->beginTransaction();

            //记录人物操作日志
            $personId = $person->id;
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $this->getPersonById($personId);
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $familyId = $old_person->familyId;
            $option_detail = '通过分享链接注册或登录，绑定userId';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['userId' => $userId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
            //end

            //更新人物绑定的用户id
            $sql = "UPDATE $this->TABLE	SET userId = '$userId',update_time = now() WHERE id = '$person->id' AND userId = '0' AND is_delete = '0'";
            $count = $this->pdo->update($sql);

            if ($count > 0) {
                //执行绑定人物的更新操作
                $this->bindUserToPersonCommonUpdate($userId, $person->id);
                //将申请用户表中的记录也删除
                $familyDB = new CDBFamily();
                $familyDB->deleteFamilyUser($person->familyId, $userId);
                if (!$isJoin) {
                    $permissionResult = $familyDB->addPermission(
                        $person->familyId,
                        $userId,
                        0,
                        0,
                        200,
                        $operator
                    );
                } else {
                    $permissionResult = $familyDB->modifyPermission(
                        $person->familyId,
                        $userPermission['permission'] === 100 ? 200 : null,
                        $userId,
                        0,
                        0,
                        $operator,
                        0
                    );
                }
            }

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $count;
    }


    /**
     * 取消人物关联
     * @param mergeInfo
     * @return bool
     *
     * @author yuanxin
     */
    public function cancelRefPerson($mergeInfo)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $fromPersonId = $mergeInfo[0]['fromPersonId'];
        $fromFamilyId = $mergeInfo[0]['fromFamilyId'];
        $toPersonId = $mergeInfo[0]['toPersonId'];
        $toFamilyId = $mergeInfo[0]['toFamilyId'];
        $mergeWay = $mergeInfo[0]['mergeWay'];
        $mergeId = $mergeInfo[0]['id'];
        try {
            //    相同人物取消关联
            $this->pdo->beginTransaction();
            if ($mergeWay == $GLOBALS['MERGE_FAMILY_BY_PERSON']) {
                $result = $this->cancelSamePerson($fromPersonId, $toPersonId);
                $this->pdo->update($result['fromSql']);
                $this->pdo->update($result['toSql']);
            } //    姻亲取消关联
            elseif ($mergeWay == $GLOBALS['MERGE_FAMILY_BY_MARRIAGE']) {
                // 两个人取消，对四个人都不好
                $sql = "SELECT ref_personId , ref_familyId FROM $this->TABLE  WHERE  id = $fromPersonId OR id  = $toPersonId AND is_delete = 0";
                $result = $this->pdo->query($sql);
                $fromRefFamilyIds = json_decode($result[0]['ref_familyId']);
                $fromRefPersonIds = json_decode($result[0]['ref_personId']);
                $toRefFamilyIds = json_decode($result[1]['ref_familyId']);
                $toRefPersonIds = json_decode($result[1]['ref_personId']);
                $index = 0;
                foreach ($fromRefFamilyIds as $key => $familyId) {
                    if ($familyId == $toFamilyId) {
                        $index = $key;
                    }
                }
                $result1 = $this->cancelSamePerson($fromPersonId, $fromRefPersonIds[$index]);
                foreach ($toRefFamilyIds as $key => $familyId) {
                    if ($familyId == $fromFamilyId) {
                        $index = $key;
                    }
                }
                $result2 = $this->cancelSamePerson($toPersonId, $toRefPersonIds[$index]);
                $this->pdo->update($result1['fromSql']);
                $this->pdo->update($result1['toSql']);
                $this->pdo->update($result2['fromSql']);
                $this->pdo->update($result2['toSql']);
            }
            $deleteSql = "UPDATE $this->TABLE_FAMILY_MERGE SET is_delete = 1  WHERE  id = $mergeId;";
            $this->pdo->update($deleteSql);
            $this->pdo->commit();
            return true;
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
        }
    }

    /**
     * 增加过继关系的支持
     * @param $srcPersonId
     * @param $destPersonId
     * @return mixed
     */
    public function addAdoptionPersonByPersonIdV3($srcPersonId, $destPersonId, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $userId = $GLOBALS['userId'];
        try {
            if ($needTransaction) {
                //开启事务处理
                $this->pdo->beginTransaction();
            }


            //记录人物日志
            $personId = $srcPersonId;
            $person = $this->getPersonById($personId);
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $familyId = $person->familyId;
            $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
            $option_detail = '增加过继关系的支持';
            $option_user = $GLOBALS['userId'] ?? 0;
            $other_msg = ['srcPersonId' => $srcPersonId, 'destPersonId' => $destPersonId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $sql = "INSERT INTO gener_person_adoption (src_personId,dest_personId,create_time,create_by,update_time,update_by)
           VALUES ('$srcPersonId','$destPersonId',now(),'$userId',now(),'$userId')";
            $insertId = $this->pdo->insert($sql);
            $sql = "UPDATE $this->TABLE SET is_adoption=2,update_time = now() WHERE id='$srcPersonId'";
            $this->pdo->update($sql);
            $sql = "UPDATE $this->TABLE SET is_adoption=1,update_time = now() WHERE id = '$destPersonId'";
            $this->pdo->update($sql);

            if ($needTransaction) {
                $this->pdo->commit();
            }
        } catch (PDOException $e) {
            if ($needTransaction) {
                $this->pdo->rollback();
            }
        }
        return $insertId;
    }

    /**
     * 处理相同人物关系
     * @param fromPersonId
     * @param toPersonId
     * @return  array  4 sql
     * @author yuanxin
     * @version 3
     */

    private function cancelSamePerson($fromPersonId, $toPersonId)
    {
        $sql = "SELECT ref_personId,ref_familyId FROM $this->TABLE WHERE  id = $fromPersonId AND is_delete = 0 ; ";

        $result = $this->pdo->query($sql);
        $refPersonIds = json_decode($result[0]['ref_personId'], true);
        $index = 0;
        foreach ($refPersonIds as $key => $refPersonId) {
            if ($refPersonId == $toPersonId) {
                unset($refPersonIds[$key]);
                $index = $key;
                break;
            }
        }
        $refPersonIds = array_values($refPersonIds);
        $refFamilyIds = json_decode($result[0]['ref_familyId'], true);
        unset($refFamilyIds[$index]);
        //函数返回一个包含给定数组中所有键值的数组,但不保留键名
        $refFamilyIds = array_values($refFamilyIds);
        // 生成新sql，  family，Person
        if (array() == $refPersonIds) {
            $fromSql = "UPDATE $this->TABLE  SET ref_personId =  '' , ref_familyId = ''  WHERE  id  =  $fromPersonId AND is_delete = 0;";
        } else {
            $refPersonIds = json_encode($refPersonIds);
            $refFamilyIds = json_encode($refFamilyIds);
            $fromSql = "UPDATE $this->TABLE  SET ref_personId =  '$refPersonIds' , ref_familyId = '$refFamilyIds' WHERE  id  =  $fromPersonId AND is_delete = 0;";
        }
        // 同理 ，处理toPerson
        $sql = "SELECT ref_personId,ref_familyId FROM $this->TABLE WHERE  id = $toPersonId AND is_delete = 0 ; ";
        $result = $this->pdo->query($sql);
        $refPersonIds = json_decode($result[0]['ref_personId'], true);
        foreach ($refPersonIds as $key => $refPersonId) {
            if ($refPersonId == $fromPersonId) {
                unset($refPersonIds[$key]);
                $index = $key;
                break;
            }
        }
        $refPersonIds = array_values($refPersonIds);
        $refFamilyIds = json_decode($result[0]['ref_familyId'], true);
        unset($refFamilyIds[$index]);
        $refFamilyIds = array_values($refFamilyIds);
        if (array() == $refPersonIds) {
            $toSql = "UPDATE $this->TABLE  SET ref_personId =  '' , ref_familyId = '' WHERE  id  =  $toPersonId AND is_delete = 0;";
        } else {
            $refPersonIds = json_encode($refPersonIds);
            $refFamilyIds = json_encode($refFamilyIds);
            $toSql = "UPDATE $this->TABLE  SET ref_personId =  '$refPersonIds' , ref_familyId = '$refFamilyIds' WHERE  id  =  $toPersonId AND is_delete = 0;";
        }

        $resultSql['fromSql'] = $fromSql;
        $resultSql['toSql'] = $toSql;


        return $resultSql;
    }


    /**
     *
     * 合并两个家族生成新家族
     * @param  $personId   人物id
     * @param $isDelete    是否删除
     * @return array
     * @author liuzhenhao
     * @version 3
     */
    public function getMainParent(Person $person, $isDelete = 0)
    {
        $father = $person->father;
        $mother = $person->mother;
        if ($father) {
            $fatherStr = implode(',', $father);
            $sql = "select id,type,name from $this->TABLE where is_delete = 0 and id in ($fatherStr) limit 1";
            $fatherInfo = $this->pdo->uniqueResult($sql);
            if ($fatherInfo['type'] == 1) {
                return $fatherInfo;
            }
            return [];
        }
        if ($mother) {
            $motherStr = implode(',', $mother);
            $sql = "select id,type,name from $this->TABLE where is_delete = 0 and id in ($motherStr) limit 1";
            $motherInfo = $this->pdo->uniqueResult($sql);
            if ($motherInfo['type'] == 1) {
                return $motherInfo;
            }
            return [];
        }
        return [];
    }


    /**
     * 分支迁移
     * @param int $fromPersonId 人物id
     * @param int $toPersonId 迁移到的人物id
     * @return status
     * @author yuanxin
     */
    public function movePersonV33($fromPersonId, $toPersonId, $needTransaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $fromPerson = $this->getPersonOBjectById($fromPersonId);
        $toPerson = $this->getPersonOBjectById($toPersonId);

        //记录人物日志
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $fromPerson->familyId;
        $old_data = json_encode($fromPerson, JSON_UNESCAPED_UNICODE);
        $option_detail = '迁移分支至新节点';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['toPerson' => $toPerson];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $this->recordPersonOptionLog($fromPersonId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        if ($fromPerson == null || $toPerson == null) {
            return false;
        }

        // 移动分支的流程如下:
        // 1. 清除from相关人物父母，兄弟关系。
        // 2. 获取迁移变动人数
        // 3. 获取迁移变动的index范围
        // 4. 调整index
        // 5. 修改当前人物排行，父亲，兄弟姐妹,辈分
        // 6. 修改toPersonId下的关系，用添加人物那块
        try {
            /*********************************************清除当前人物父母，兄弟关系************************************* */
            if ($needTransaction) {
                $this->pdo->beginTransaction();
            }

            # 先获取toPerson子女的最大排行
            $rankings = $this->getChildrenRanking($toPersonId, false);

            $personFatherIdArray = [];  // 当前人物的父亲
            $personMotherIdArray = [];  // 当前人物的母亲
            // 获取toPerson的性别
            if ($toPerson->gender == 1) {
                $fatherIdArray[] = $toPerson->id;
                $motherIdArray = $toPerson->spouse;
            } else {
                $motherIdArray[] = $toPerson->id;
                $fatherIdArray = $toPerson->spouse;
            }

            $personBrotherArray = $toPerson->son;   // 当前人物的兄弟
            $personSisterArray = $toPerson->daughter;   // 当前人物的姐妹

            // 根据当前人物的性别更新兄弟姐妹
            if ($fromPerson->gender == 1) {
                array_push($personBrotherArray, $fromPerson->id);
            } else {
                array_push($personSisterArray, $fromPerson->id);
            }

            $personFatherStr = Util::arrayToRelation($fatherIdArray);
            $personMotherStr = Util::arrayToRelation($motherIdArray);
            $personBrotherStr = Util::arrayToRelation($personBrotherArray);
            $personSisterStr = Util::arrayToRelation($personSisterArray);

            // 更新当前人物的父/母, 兄弟姐妹
            $sql = "UPDATE $this->TABLE SET father = '$personFatherStr', mother = '$personMotherStr', brother = '$personBrotherStr', sister = '$personSisterStr',update_time = now() WHERE id = '$fromPersonId' ";
            $this->pdo->update($sql);
            /*********************************************获取迁移变动人数和范围************************************* */
            // 人数等于 fromLastFamilyIndex - fromPerson->familyIndex +1 
            //  范围是 $fromPerson->familyIndex 到 toLastFamilyIndex 或者 toPerson->familyIndex 到 fromLastFamilyIndex
            $fromLastFamilyIndex = $this->getLastPersonFamilyIndex($fromPerson->familyId, $fromPerson->level, $fromPerson->familyIndex);
            $toLastFamilyIndex = $this->getLastPersonFamilyIndex($toPerson->familyId, $toPerson->level, $toPerson->familyIndex);
            //自己迁移到自己分支下
            if ($toPerson->familyIndex < $fromLastFamilyIndex && ($toPerson->familyIndex > $fromPerson->familyIndex) && ($toPerson->level > $fromPerson->level)) {
                $status = -1;
                return $status;
            }
            $fromPersonCount = $fromLastFamilyIndex - $fromPerson->familyIndex + 1;
            /*********************************************修改辈分************************************* */
            $dLevel = $fromPerson->level - $toPerson->level;
            $updateSql = "UPDATE  $this->TABLE  SET level =  level -  $dLevel + 1
                                                    WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index BETWEEN  $fromPerson->familyIndex AND  $fromLastFamilyIndex;";
            $this->pdo->update($updateSql);
            /*********************************************移动index************************************* */
            // 一共两种情况
            //    from点在to点的后面
            // if($fromPerson->familyIndex >  $toPerson->familyIndex){
            // index往前迁移
            if ($fromPerson->familyIndex > $toLastFamilyIndex) {
                $dValue = $fromPerson->familyIndex - $toLastFamilyIndex;
                $firstSql = "UPDATE   $this->TABLE  SET  family_index = family_index  + $fromPersonCount 
                                                                    WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index > $toLastFamilyIndex;";
                $secondSql = "UPDATE   $this->TABLE SET family_index = family_index - $fromPersonCount - $dValue + 1
                                                                    WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index BETWEEN  ($fromPerson->familyIndex + $fromPersonCount)  AND ($fromLastFamilyIndex + $fromPersonCount);";
                $thirdSql = "UPDATE   $this->TABLE SET family_index = family_index - $fromPersonCount
                                                                    WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index >= ($fromLastFamilyIndex + $fromPersonCount);";
                $this->pdo->update($firstSql);
                $this->pdo->update($secondSql);
                $this->pdo->update($thirdSql);
            } elseif ($fromLastFamilyIndex == $toLastFamilyIndex) {
                // 不迁移Index不用动
            } else {
                // index往后迁移 
                $dValue = $toLastFamilyIndex - $fromPerson->familyIndex + 1;
                $firstSql = "UPDATE   $this->TABLE  SET  family_index = family_index  + $fromPersonCount
                                                                    WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index > $toLastFamilyIndex;";
                $secondSql = "UPDATE   $this->TABLE  SET  family_index = family_index  + $dValue
                                                                    WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index BETWEEN  $fromPerson->familyIndex AND  $fromLastFamilyIndex;";
                $thirdSql = "UPDATE   $this->TABLE SET family_index = family_index - $fromPersonCount
                                                                    WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index >= $fromPerson->familyIndex;";
                $this->pdo->update($firstSql);
                $this->pdo->update($secondSql);
                $this->pdo->update($thirdSql);
            }
            //  }else{
            //     //    from点在to点的前面
            //     // index不可能往前迁移 只能往后迁移
            //     $dValue  =  $toLastFamilyIndex - $fromPerson->familyIndex  + 1;
            //      $firstSql = "UPDATE   $this->TABLE  SET  family_index = family_index  + $fromPersonCount 
            //                                                         WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index > $toLastFamilyIndex;";
            //     $secondSql = "UPDATE   $this->TABLE  SET  family_index = family_index  + $dValue
            //                                                         WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index BETWEEN  $fromPerson->familyIndex AND  $fromLastFamilyIndex;";
            //     $thirdSql  = "UPDATE   $this->TABLE SET family_index = family_index - $fromPersonCount
            //                                                         WHERE  familyId = $fromPerson->familyId AND is_delete = 0 AND  family_index >= $fromPerson->familyIndex;";
            //     $this->pdo->update($firstSql);
            //     $this->pdo->update($secondSql);
            //     $this->pdo->update($thirdSql);
            // }
            /*********************************************更新目标人物的子女,兄弟姐妹************************************* */
            //更新父母的记录
            $fartherAndMother = array_merge($fromPerson->father, $fromPerson->mother);
            $setParam = "";
            if ($fromPerson->gender == $GLOBALS['GENDER_FEMALE']) {
                $setParam = "daughter";
            } else if ($fromPerson->gender == $GLOBALS['GENDER_MALE']) {
                $setParam = "son";
            }
            $from = '@' . $fromPerson->id . "|";
            $to = "";
            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to')";
            $where = "";
            foreach ($fartherAndMother as $key => $value) {
                if ($where != "") {
                    $where = $where . " or id = $value";
                } else {
                    $where = $where . "id = $value";
                }
            }
            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);

            // 更新原人物兄弟姐妹的记录
            $brotherAndSister = array_merge($fromPerson->brother, $fromPerson->sister);
            $setParam = "";
            if ($fromPerson->gender == $GLOBALS['GENDER_FEMALE']) {
                $setParam = "sister";
            } else if ($fromPerson->gender == $GLOBALS['GENDER_MALE']) {
                $setParam = "brother";
            }
            $from = '@' . $fromPerson->id . "|";
            $to = "";
            $sql = "UPDATE $this->TABLE SET $setParam = REPLACE($setParam,'$from','$to')";
            $where = "";
            foreach ($brotherAndSister as $key => $value) {
                if ($value != $fromPerson->id){
                    if ($where != "") {
                        $where = $where . " or id = $value";
                    } else {
                        $where = $where . "id = $value";
                    }
                }
            }
            if ($where != "")
                $this->pdo->update($sql . " WHERE " . $where);

            if ($fromPerson->gender == 1) {
                // 更新儿子
                $toPersonSonArray = $toPerson->son;
                $toPersonSonArray[] = $fromPerson->id;
                $toPersonSonStr = Util::arrayToRelation($toPersonSonArray);
                $sql = "UPDATE $this->TABLE SET son = '$toPersonSonStr',update_time = now() WHERE id = '$toPersonId' ";
                $this->pdo->update($sql);

                // 更新目标人物配偶的儿子
                foreach ($toPerson->spouse as $spouseId) {
                    $sql = "UPDATE $this->TABLE SET son = '$toPersonSonStr',update_time = now() WHERE id = '$spouseId' ";
                    $this->pdo->update($sql);
                }
            } else {
                // 更新女儿
                $toPersonDaughterArray = $toPerson->daughter;
                $toPersonDaughterArray[] = $fromPerson->id;
                $toPersonDaughterStr = Util::arrayToRelation($toPersonDaughterArray);
                $sql = "UPDATE $this->TABLE SET daughter = '$toPersonDaughterStr',update_time = now() WHERE id = '$toPersonId' ";
                $this->pdo->update($sql);

                // 更新目标人物配偶的女儿
                foreach ($toPerson->spouse as $spouseId) {
                    $sql = "UPDATE $this->TABLE SET daughter = '$toPersonDaughterStr',update_time = now() WHERE id = '$spouseId' ";
                    $this->pdo->update($sql);
                }
            }

            // 更新目标人物子女的兄弟姐妹
            if ($fromPerson->gender == 1) {
                $key = 'brother';
            } else {
                $key = 'sister';
            }

            foreach ($toPerson->son as $sonId) {
                $str = '@' . $fromPerson->id . '|';
                $sql = "UPDATE $this->TABLE SET $key = CONCAT($key,\"$str\"),update_time = now() WHERE id = '$sonId' ";
                $this->pdo->update($sql);
            }

            foreach ($toPerson->daughter as $daughterId) {
                $str = '@' . $fromPerson->id . '|';
                $sql = "UPDATE $this->TABLE SET $key = CONCAT($key,\"$str\"),update_time = now() WHERE id = '$daughterId' ";
                $this->pdo->update($sql);
            }

            /*********************************************更新人物排行************************************* */
            $ranking = $rankings ? end($rankings) : 0;
            $sql = "UPDATE $this->TABLE SET ranking = $ranking + 1  WHERE id = $fromPerson->id ";
            $this->pdo->update($sql);
            $familyDB = new CDBFamily();
            $result = $familyDB->autoUpdateFamilyLevel($fromPerson->familyId);
            if ($needTransaction) {
                $this->pdo->commit();
            }
        } catch (PDOException $e) {
            if ($needTransaction) {
                $this->pdo->rollback();
                return -2;
            }
        }
        return 1;
    }

    /**
     * 用户通过分享链接注册或登录，将用户id绑定到指定的人物上
     * @param $userId 用户id
     * @param $shareCode 分享码
     * @return int 更新的记录数,-2代表用户已存在家族中，-1代表出现异常
     */
    public function bindUserToPerson3($userId, $shareCode, $fromUserId, $type)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        //检查是否已经在家族中
        $sql = "SELECT familyId FROM $this->TABLE WHERE share_url = '$shareCode' and is_delete = '0' ";
        $shareCodeFamilyId = $this->pdo->uniqueResult($sql);
        $familyId = $shareCodeFamilyId['familyId'];

        $familyDB = new CDBFamily();

        $sql = "SELECT familyId FROM $this->TABLE WHERE userId = '$userId' AND familyId = $familyId AND is_delete = '0' limit 1";
        $userFamilyId = $this->pdo->uniqueResult($sql);
        print_r($userFamilyId);
        if ($userFamilyId) {
            return -2;
        }

        //        $sql = "SELECT familyId FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0' ";
        //        $userFamilyIds = $this->pdo->query($sql);

        //        foreach ($userFamilyIds as &$userFamilyId) {
        //            //@codeCoverageIgnoreStart
        //            if ($userFamilyId['familyId'] == $familyId) {
        //                return -2;
        //                //@codeCoverageIgnoreEnd
        //            }
        //        }


        try {
            $this->pdo->beginTransaction();

            $sql = "UPDATE $this->TABLE	SET userId = '$userId' ,update_time = now() WHERE share_url = '$shareCode' AND userId = '0' and is_delete = '0' ";
            $count = $this->pdo->update($sql);

            $permission = $familyDB->getUserPermission($userId, $familyId);

            if (!$permission) {
                $addPermission = $familyDB->addPermission($familyId, $userId, 0, 0, 200, $userId);
                if ($addPermission <= 0) {
                    $this->pdo->rollback();
                    return -1;
                }
            } else {
                if ($permission['permission'] < 200) {
                    $updatePermission = $familyDB->modifyPermission(
                        $familyId,
                        200,
                        $userId,
                        0,
                        0,
                        $userId,
                        0
                    );
                    if ($updatePermission <= 0) {
                        $this->pdo->rollback();
                        return -1;
                    }
                }
            }

            if ($count > 0) {
                $personId = $this->getPersonIdByShareCode($shareCode);

                //记录人物操作日志
                $function_name = __CLASS__ . '->' . __FUNCTION__;
                $old_person = $this->getPersonById($personId);
                $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
                $familyId = $old_person->familyId;
                $option_detail = '通过分享链接注册或登录，绑定userId';
                $option_user = $GLOBALS['userId'] ?? 0;
                $other_msg = ['share_url' => $shareCode, 'userId' => $userId];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
                //end

                //执行绑定人物的更新操作
                $this->bindUserToPersonCommonUpdate($userId, $personId);
                //添加绑定记录
                $this->insertPersonBindUserRecord($familyId, $personId, $fromUserId, $userId, $type, $userId);
            }

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $count;
    }

    /**
     * 获取当前人物到始祖的直通图
     * @param personId
     * @param samePersonFamilyIndex
     * @version 3
     * @date 2020/10/26
     * @author yuanxin
     * @return  array<person>
     */
    public function getPersonAncestorsInCurrentFamily($personId, $samePersonFamilyIndex)
    {
        // public function getPersonAncestorsInCurrentFamily($personId){
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $ancestors = array();
        $person = $this->getPersonById2($personId);

        if ($person == null) {
            return null;
        }

        $familyId = $person->familyId;
        $personIndex = $person->familyIndex;
        $personLevel = $person->level;
        // $sourcePersonIndex  = $this->getTwoPointsSource($personIndex,$samePersonFamilyIndex,$familyId);

        // $data['sourcePersonIndex'] = $sourcePersonIndex;

        array_push($ancestors, $person);
        $i = 0;
        while ($personIndex > 0 && $i < 20) {
            $i++;
            $indexSql = "SELECT MAX(family_index) FROM $this->TABLE  
                    WHERE  familyId = $familyId  AND family_index <= $personIndex AND level = $personLevel - 1  AND type = 1 AND  is_delete = 0";
            $sql  = "SELECT id,name,ranking,level,gender,father,mother,sister,brother,son,daughter,spouse,family_index AS familyIndex FROM $this->TABLE 
                                        WHERE familyId = $familyId AND  family_index = ($indexSql)";
            $ancestor = $this->pdo->query($sql);
            if ($ancestor != null) {
                // $person = new Person($ancestor[0]);
                $person = new PersonInfo($ancestor[0]);
                $parser = new RelationParser($person);
                $person->father = $parser->getFathers();
                $person->mother = $parser->getMothers();
                $person->son = $parser->getSons();
                $person->daughter = $parser->getDaughters();
                $person->spouse = $parser->getSpouses();
                $person->brother = $parser->getBrothers();
                $person->sister = $parser->getSisters();
            }
            array_push($ancestors, $person);
            $personIndex = $person->familyIndex;
            $personLevel = $person->level;
        }
        return $ancestors;
    }



    /**
     * 根据当前人物的level , familyId,familyIndex,deep  查出当前人物的后几代子孙的familyIndex
     * @param familyId  int
     * @param corePersonLevel       int
     * @param corePersonFamilyIndex  int
     * @param deep      int
     * @author yuanxin
     * @return int familyIndex
     */
    public function getOffspringFamilyIndexByDeep($familyId, $corePersonLevel, $corePersonFamilyIndex, $deep)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        if ($corePersonFamilyIndex >= 0) {
            $sql = "SELECT min(family_index) as familyIndex
            FROM $this->TABLE 
        WHERE familyId = $familyId and is_delete = 0  and type = 1 and  level <= $corePersonLevel and  family_index > $corePersonFamilyIndex ";
        } else {
            $sql = "SELECT min(family_index) as familyIndex
            FROM $this->TABLE 
        WHERE familyId = $familyId and is_delete = 0  and type = 1 and  level <= $corePersonLevel and  family_index > $corePersonFamilyIndex and family_index < 0";
        }

        $lastPersonFamilyIndex = $this->pdo->uniqueResult($sql);

        if ($lastPersonFamilyIndex['familyIndex'] === null) {
            if ($corePersonFamilyIndex >= -1) {

                $sql = "SELECT family_index as familyIndex FROM $this->TABLE WHERE familyId = $familyId  and is_delete = 0   order by family_index desc limit 1;";

                $lastIndex = $this->pdo->uniqueResult($sql);
                $lastPersonFamilyIndex =  $lastIndex['familyIndex'];
            } else {

                $sql = "SELECT family_index as familyIndex FROM $this->TABLE WHERE familyId = $familyId  and is_delete = 0  and family_index < 0 order by family_index desc limit 1;";
                $lastIndex = $this->pdo->uniqueResult($sql);
                $lastPersonFamilyIndex =  $lastIndex['familyIndex'];
            }
        }else{
            $lastPersonFamilyIndex =  $lastPersonFamilyIndex['familyIndex'];
        }
        $sql = "SELECT max(family_index) as familyIndex ,count(1) as personCount
                                     FROM $this->TABLE 
                                             WHERE familyId = $familyId and is_delete = 0  and type = 1 and  level  <= $corePersonLevel + $deep  and  family_index > $corePersonFamilyIndex and family_index < $lastPersonFamilyIndex";

        $result = $this->pdo->uniqueResult($sql);

        return $result;
    }

    /**
     * 获取家族五服内的所有人员,供前端家族树使用
     * //TODO  代码不好，需要优化
     * @param familyId
     * @param showMain 是否显示外亲
     * @param filter  int  是否显示配偶
     * @param userId  用户人物
     * @param personId  中心人物
     * @return array 所有人员的数组
     *
     * @version 3
     * @author yuanxin
     */
    public function getFamilyPersonsLocal2($familyId, $userId, $personId = 0, $filter, $showMain = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $filterSql = "";
        if ($filter == 1) {
            $filterSql = " AND type = 1 ";
        }

        $showMainSql = "";
        if ($showMain === 0) {
            $showMainSql = " AND isNotMain = 0 ";
        }
        // 获取level区间
        $familyLevelSql = "SELECT  min_level,max_level  FROM  $this->TABLE_FAMILY WHERE id = $familyId";
        $family = $this->pdo->query($familyLevelSql);
        $minLevel = $family[0]['min_level'];
        $maxLevel = $family[0]['min_level'];
        // 没有personId默认当前用户
        if ($personId == 0) {
            if($userId > 0 ){
                $userPersonSql = "SELECT  id ,level,family_index as familyIndex    FROM $this->TABLE  WHERE  familyId = $familyId  AND userId = $userId  AND  is_delete =  0;";
            }else{
                $userPersonSql = "SELECT  id ,level,family_index as familyIndex    FROM $this->TABLE  WHERE  familyId = $familyId  AND family_index = 0  AND  is_delete =  0;";
            }
        } else {
            $userPersonSql = "SELECT  id ,level,family_index as familyIndex    FROM $this->TABLE  WHERE  familyId = $familyId  AND id = $personId AND  is_delete =  0;";
        }

        $userPerson = $this->pdo->query($userPersonSql);

        /******************没有绑定用户，没有选中人物的时候**************** */
        if (array() == $userPerson) {
            $lastFamilyIndex = $this->getLastPersonFamilyIndex($familyId, $minLevel, 0);
            $deep = 0;
            if ($lastFamilyIndex  > 2000) {
                // 分支下所有人
                do {
                    $deep += 1;
                    $offspring = $this->getOffspringFamilyIndexByDeep($familyId, $minLevel, 0, $deep);
                    $lastFamilyIndex = $offspring['familyIndex'];
                } while ($offspring['personCount'] < 2000);
            }
              $sql = "SELECT  tb.family_index as familyIndex  ,tb.id,tb.name,tb.level,tb.father,tb.mother,tb.gender,tb.userId,tb.ranking,tb.share_url as shareUrl,
                                    tb.son,tb.daughter,tb.type,tb.spouse,tb.isNotMain,tb.is_adoption as isAdoption,ref_familyId as refFamilyId,ref_personId as refPersonId,
                                         tb.sideText,tb.bicolor,isNotMain
                                    FROM $this->TABLE  tb  
                 WHERE familyId = $familyId  AND  level  <   $minLevel + $deep-1  AND family_index  <= $lastFamilyIndex AND  is_delete = 0  $filterSql $showMainSql    order by family_index asc;";
           $result = $this->pdo->query($sql);
        } else {
        /******************有选中人物的时候**************** */
            $minLevel = $userPerson[0]['level'] - 3 < $family[0]['min_level'] ? $family[0]['min_level'] : ($userPerson[0]['level'] - 3);
            $personIndex = $userPerson[0]['familyIndex'];
            $personLevel = $userPerson[0]['level'];

            $getFatherSql = "SELECT  max(family_index) as familyIndex  FROM  $this->TABLE WHERE familyId = $familyId AND level = $minLevel  AND family_index <= $personIndex  AND type = 1 AND is_delete = 0 ;";
            //    获取index区间
            $startFamilyIndex = $this->pdo->query($getFatherSql);
            $startFamilyIndex = $startFamilyIndex[0]['familyIndex'];

            $lastFamilyIndex = $this->getLastPersonFamilyIndex($familyId, $minLevel, $startFamilyIndex);

            // 获取上三代头  
             $sql = "SELECT  tb.family_index as familyIndex  ,tb.id,tb.name,tb.level,tb.father,tb.mother,tb.gender,tb.userId,tb.ranking,tb.share_url as shareUrl,
                        tb.son,tb.daughter,tb.type,tb.spouse,tb.isNotMain,tb.is_adoption as isAdoption,ref_familyId as refFamilyId,ref_personId as refPersonId,
                        tb.sideText,tb.bicolor,isNotMain
                        FROM 
                        $this->TABLE  tb  
                        WHERE familyId = $familyId  AND is_delete = 0 AND  level BETWEEN $minLevel AND  $personLevel  AND  family_index BETWEEN   $startFamilyIndex   AND $lastFamilyIndex  $filterSql $showMainSql order by family_index asc;";

             $head = $this->pdo->query($sql);

            //  后代
            $lastBodyFamilyIndex = $this->getLastPersonFamilyIndex($familyId, $personLevel, $personIndex);

            $deep = $maxLevel;
            if ($lastBodyFamilyIndex  - $personIndex  > 2000) {
                $deep = 0;
                // 分支下所有人
                do {
                    $deep += 1;
                    $offspring = $this->getOffspringFamilyIndexByDeep($familyId, $personLevel, $personIndex, $deep);
                    $lastBodyFamilyIndex = $offspring['familyIndex'];
                } while ($offspring['personCount'] < 2000 || $maxLevel == ($personLevel + $deep));
                 $sql = "SELECT  tb.family_index as familyIndex  ,tb.id,tb.name,tb.level,tb.father,tb.mother,tb.gender,tb.userId,tb.ranking,tb.share_url as shareUrl,
                                    tb.son,tb.daughter,tb.type,tb.spouse,tb.isNotMain,tb.is_adoption as isAdoption,ref_familyId as refFamilyId,ref_personId as refPersonId,
                                         tb.sideText,tb.bicolor,isNotMain
                                    FROM $this->TABLE  tb  
                WHERE familyId = $familyId  AND  level  BETWEEN  $personLevel  + 1 AND  ($personLevel + $deep -1)   AND family_index  BETWEEN  $personIndex AND  $lastBodyFamilyIndex AND  is_delete = 0  $filterSql $showMainSql    order by family_index asc;";
            }else{
                 $sql = "SELECT  tb.family_index as familyIndex  ,tb.id,tb.name,tb.level,tb.father,tb.mother,tb.gender,tb.userId,tb.ranking,tb.share_url as shareUrl,
                                    tb.son,tb.daughter,tb.type,tb.spouse,tb.isNotMain,tb.is_adoption as isAdoption,ref_familyId as refFamilyId,ref_personId as refPersonId,
                                         tb.sideText,tb.bicolor,isNotMain
                                    FROM $this->TABLE  tb  
                WHERE familyId = $familyId  AND  level  >  $personLevel   AND family_index  BETWEEN  $personIndex AND  $lastBodyFamilyIndex AND  is_delete = 0  $filterSql $showMainSql    order by family_index asc;";

            }
              $body = $this->pdo->query($sql);
            // // 获取上三代尾
            //  $sql = "SELECT  tb.family_index as familyIndex  ,tb.id,tb.name,tb.level,tb.father,tb.mother,tb.gender,tb.userId,tb.ranking,tb.share_url as shareUrl,
            //             tb.son,tb.daughter,tb.type,tb.spouse,tb.isNotMain,tb.is_adoption as isAdoption,ref_familyId as refFamilyId,ref_personId as refPersonId,
            //             tb.sideText,tb.bicolor,isNotMain
            //             FROM 
            //             $this->TABLE  tb  
            //             WHERE familyId = $familyId  AND is_delete = 0 AND  level BETWEEN $minLevel AND  $personLevel  AND  family_index BETWEEN   $personIndex   AND $lastFamilyIndex  $filterSql $showMainSql order by family_index asc;";

            //  $tail = $this->pdo->query($sql);

             $result = array_merge($head,$body);
             $familyIndexArray = array();
             foreach($result as $key => $person) {
                 $familyIndexArray[$key] = $person['familyIndex'];
             }
             array_multisort($familyIndexArray, SORT_ASC, $result);
        }

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        return $result;
    }

    /**
     * 检查人物是否存在
     * @param $personId    人物id
     * @return  array  标题信息
     */
    public function getPersonPdfTitle($personId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "select id,title,branch_name as branchName, zan ,zan_type as zanType, origin_name as originName from $this->TABLE_PERSON_PDF_TITLE where is_delete = 0 and person_id = $personId limit 1";

        $result = $this->pdo->uniqueResult($sql);

        return $result;
    }

    /**
     * 依据字段名获取人物pdf表的内容
     * @param $personId    人物id
     * @param $fields
     * @return  array  标题信息
     */
    public function getPersonPdfTitleByfieldsName($personId, $fields)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $fieldsSql = "";
        foreach ($fields as $k => $v){
            $fieldsSql .= " $k as $v ,";
        }
        $fieldsSql = substr($fieldsSql, 0, -1);

        $sql = "select $fieldsSql from $this->TABLE_PERSON_PDF_TITLE where is_delete = 0 and person_id = $personId limit 1";

        $result = $this->pdo->uniqueResult($sql);

        return $result;
    }

    /**
     * 检查人物是否存在
     * @param $personId    人物id
     * @param $familyId    家族id
     * @param $userId    用户id
     * @param array    $data 标题id
     * @param $update    是否是更新
     * @return  array
     */
    public function UpdateOrCreatePdfTitle($personId, $familyId, $userId, $data, $update=true)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if (!$data){
            return 0;
        }

//        //记录人物操作日志
//        $function_name = __CLASS__ . '->' . __FUNCTION__;
//        $old_person = $this->getPersonById($personId);
//        $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
//        $familyId = $old_person->familyId;
//        $option_detail = '编辑人物基本信息';
//        $option_user = $GLOBALS['userId'] ?? 0;
//        $other_msg = $data;
//        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);

        if (!$update){

            $data = array_merge($data, [
                'person_id' => $personId,
                'family_id' => $familyId,
                'create_by' => $userId,
                'update_by' => $userId,
            ]);

            if (array_key_exists('branch_name', $data)){
                $checkSql = "select 1 from $this->TABLE_PERSON_PDF_TITLE where family_id = $familyId and is_delete = 0 
                      and branch_name != '' and '{$data['branch_name']}' limit 1";
                $checkExist = $this->pdo->uniqueResult($checkSql);
                if ($checkExist){
                    return [-1,  '支派名重复'];
                }
            }


            $sql = "insert into $this->TABLE_PERSON_PDF_TITLE set ";
            foreach ($data as $k => $v){
                $sql .= " $k = '$v',";
            }
//            $this->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);
            $result = $this->pdo->insert(substr($sql,0,-1));

            return [$result, $result ? $result: '添加失败'];
        }else{
            $data = array_merge($data, [
                'update_by' => $userId,
            ]);

            if (array_key_exists('branch_name', $data)){
                $checkSql = "select 1 from $this->TABLE_PERSON_PDF_TITLE where family_id = $familyId and is_delete = 0 
                      and branch_name != '' and '{$data['branch_name']}' and person_id != $personId limit 1";

                $checkExist = $this->pdo->uniqueResult($checkSql);
                if ($checkExist){
                    return [-1,  '支派名重复'];
                }
            }

            $sql = "update $this->TABLE_PERSON_PDF_TITLE set ";
            foreach ($data as $k => $v){
                $sql .= " $k = '$v',";
            }

            $sql = substr($sql,0,-1) . "where is_delete = 0 and person_id = $personId";

            $result = $this->pdo->update($sql);

            return [$result,  $result ? $result: '未修改'];
        }
    }

    /**
     * 创建人物关联表的关联信息
     * @param $personId
     * @param $createData array
     * @param $userId
     * @return  integer
     */
    public function createPersonOtherInfo($personId, $createData, $userId){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = " insert into $this->TABLE_PERSON_OTHER_INFO set person_id = $personId, create_by=$userId, 
                update_by=$userId, create_time=now(), update_time=now() ";
        foreach ($createData as $k => $v){
            $sql .= " ,$k = '$v' ";
        }
        return $this->pdo->insert($sql);
    }

    /**
     * 更新或创建人物关联表的关联信息
     * @param $personId
     * @param $createData array
     * @param $userId
     * @return  integer
     */
    public function createOrUpdatePersonOtherInfo($personId, $userId, $updateData, $isUpdate = true){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if (!$isUpdate){
            return $this->createPersonOtherInfo($personId, $updateData, $userId);
        }

        $sql = " update $this->TABLE_PERSON_OTHER_INFO set create_by=$userId, update_by=$userId ";
        foreach ($updateData as $k => $v){
            $sql .= " ,$k = '$v' ";
        }
        $sql .= " where person_id = $personId and is_delete = 0";
        return $this->pdo->update($sql);
    }

    /**
     * 查询人物关联表的关联信息
     * @param $personId
     * @return  integer
     */
    public function getPersonOtherInfo($personId){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = " select occupation,education,political_status as politicalStatus, birth_province as birthProvince,birth_province_name as birthProvinceName, birth_city as birthCity,
                birth_city_name as birthCityName,birth_area as birthArea, birth_area_name as birthAreaName,burial_province as burialProvince,burial_province_name as burialProvinceName, 
                burial_city as burialCity,burial_city_name as burialCityName, burial_area as burialArea, burial_area_name as burialAreaName 
                from $this->TABLE_PERSON_OTHER_INFO  where person_id = $personId and is_delete = 0 limit 1";
        return $this->pdo->uniqueResult($sql);
    }


    /**
     * 依据字段查询人物关联表的关联信息
     * @param $personId
     * @param $fields
     * @return  integer
     */
    public function getPersonOtherInfoByFieldsName($personId, $fields){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $fieldsSql = "";
        foreach ($fields as $k => $v){
            $fieldsSql .= " $k as $v ,";
        }
        $fieldsSql = substr($fieldsSql, 0, -1);

        $sql = " select  $fieldsSql
                from $this->TABLE_PERSON_OTHER_INFO  where person_id = $personId and is_delete = 0 limit 1";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取当前人物相关信息
     * @param personId
     * @return array
     * @version 3
     * @author yuanxin
     */
    public function getBranchNamesList($familyId)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }

        $sql = "select branchName as branch_name, person_id as personId,tp.family_index as familyIndex, tp.level 
                                    from $this->TABLE_PERSON_PDF_TITLE td left join 
                                    (select id,family_index,level from $this->TABLE where familyId = $familyId and is_delete = 0) tp 
                                    on td.person_id = tp.id
                                    where family_id = $familyId and td.is_delete = 0 and branch_name > '' order by tp.family_index desc";
        return $this->pdo->query($sql);
    }

    /**
     * 按世代分页获取家族人物列表，
     * @param $familyId
     * @param array 人物数组
     */
    public function getPeopleByLevelForApp($familyId, $page, $pageSize, array $data)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

//        $offset = intval($page - 1) * $pageSize;
//        if ($lastfamilyIndex !== null){
//            $familyIndexSql = " family_index > $lastfamilyIndex ";
//            $oderBySql = " order by family_index limit $pageSize ";
//        }else{
//            $familyIndexSql = " family_index < $firstFamilyIndex ";
//            $oderBySql = " order by family_index desc limit $pageSize ";
//        }
        $turnPreviousLevel = $page < 1;
//        if ($turnPreviousLevel){
//            $data['level'] -= 1;
//        }
        $offset = intval($page - 1) * $pageSize;
        $limitSql = " order by family_index limit $offset, $pageSize ";
        $sql = "select SQL_CALC_FOUND_ROWS id, name, sideText, profileText, remark, father as fatherId, type, level, gender,  
                family_index as familyIndex,case when type = 2 then '配偶' else ranking end as ranking from $this->TABLE 
                where familyId = $familyId and is_delete = 0 ";
        if ($page < 1){
            $sqlForCount = "select count(1) as count from $this->TABLE where familyId = $familyId and is_delete = 0 ";
        }else{
            $sqlForCount = "select FOUND_ROWS() as count";
        }

        if ($data) {
            foreach ($data as $k => $v) {
                if ($k === 'name') {
                    $sql .= " and $k like '%$v%' ";
                    $sqlForCount .= $turnPreviousLevel ? " and $k like '%$v%' ":'';
                    continue;
                }
                $sql .= " and  $k = $v ";
                $sqlForCount .= $turnPreviousLevel ? " and  $k = $v ":'';
            }
        }

        if ($turnPreviousLevel){
            $length = $this->pdo->uniqueResult($sqlForCount)['count'];
            $page = ceil($length / $pageSize);
            $offset = ($page - 1) * $pageSize;
            if ($offset < 0){
                return [
                    'next' => false,
                    'previous' => false,
                    'total' => '0',
                    'paging' => [],
                    'family'=>[
                        'maxLevel' => null,
                        'minLevel' => null
                    ]
                ];
            }
            $sql .= " order by family_index limit $offset, $pageSize ";
            $people = $this->pdo->query($sql);
        }else{
            $sql .= $limitSql;
            $people = $this->pdo->query($sql);
            $length = $this->pdo->uniqueResult($sqlForCount)['count'];
        }
        $familyDB = new CDBFamily();
        $familyInfo = $familyDB->getFamilyById($familyId);
        $maxLevel = $familyInfo->maxLevel;
        $minLevel = $familyInfo->minLevel;


//        $levelList = array_column($people, 'level');
//        $maxLevel = $people ? max($levelList) : null;
//        $minLevel = $people ? min($levelList) : null;
//        $levels = [($people ? end($people)['familyIndex']: null), $people ? $people[0]['familyIndex'] : null];
//        $levels = array_column($people, 'familyIndex');
//        $lastFamilyIndex = $people ? max($levels): null;
//        $firstFamilyIndex = $people ? min($levels): null;;

        $fathersId = [];

        foreach ($people as $person => $value) {
            $fatherId = Util::idSplit($value['fatherId']);
            $people[$person]['fatherId'] = $fatherId;

            if ($fatherId) {
                $fathersId = array_merge($fathersId, $fatherId);
            }
        }

        $fatherIdList = array_values(array_unique($fathersId));

        if ($fatherIdList) {
            $sqlForFather = "select id,name from $this->TABLE where  is_delete = 0 and familyId = $familyId 
                            and id in (" . implode(',', $fatherIdList) . ")";
            $fathers = $this->pdo->query($sqlForFather);
        } else {
            $fathers = [];
        }

        foreach ($people as $k => $v) {
            $people[$k]['fatherName'] = '';
            if ($v['fatherId']) {
                foreach ($v['fatherId'] as $fid) {
                    $f = array_values(array_filter($fathers, function ($x) use ($fid) {
                        if ($x['id'] === $fid) {
                            return true;
                        }
                        return false;
                    }));
                    $people[$k]['fatherName'] .= $f[0]['name'];
                }
            } else {
                $people[$k]['fatherName'] = '';
            }
            unset($people[$k]['fatherId']);
        }

        return [
            'next' => ceil($length / $pageSize) > $page  ? true : false,
            'previous' => $page > 1 ? true : false,
            'page' => $page,
            'total' => $length,
            'paging' => $people,
            'family'=>[
                'maxLevel' => $maxLevel,
                'minLevel' => $minLevel
            ]
        ];
    }

    /**
     * 按世代分页获取家族人物列表，
     * @param $familyId
     * @param array 人物数组
     */
    public function getPeopleForFileTypeInApp($familyId, $showMain)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

//        $offset = intval($page - 1) * $pageSize;
        $sql = "select SQL_CALC_FOUND_ROWS t.id, name, gender, sideText, profileText, father as fatherId, level, father, mother, 
                family_index as familyIndex, ranking, son, daughter,IFNULL(trash_field, 0) as trashField, branch_name as branchName from $this->TABLE t left join $this->TABLE_PERSON_OTHER_INFO tp on t.id = tp.person_id 
                left join $this->TABLE_PERSON_PDF_TITLE tpf on t.id = tpf.person_id 
                where familyId = $familyId and t.is_delete = 0 and type = 1 and isNotMain = 0 order by family_index";
        $sqlForCount = "select FOUND_ROWS() as count";

        $people = $this->pdo->query($sql);
//        $length = $this->pdo->uniqueResult($sqlForCount)['count'];
//        $levelList = array_column($people, 'level');
//        $maxLevel = $people ? max($levelList) : null;
//        $minLevel = $people ? min($levelList) : null;
//        $lastFamilyIndex = $people ? end($people)['familyIndex'] : null;

        $peopleIdDict = [];
        foreach ($people as $p){
            $peopleIdDict[$p['id']] = $p['name'];
        }

        foreach ($people as $person => $value) {
            $fatherId = Util::idSplit($value['fatherId']);
            $people[$person]['son'] = Util::idSplit($value['son']);
            $people[$person]['daughter'] = Util::idSplit($value['daughter']);
            $people[$person]['father'] = Util::idSplit($value['father']);
            $people[$person]['mother'] = Util::idSplit($value['mother']);
            $people[$person]['fatherId'] = $fatherId;
            $people[$person]['fatherName'] = $fatherId ? $peopleIdDict[$fatherId[0]] : '';
            unset($people[$person]['fatherId']);
        }

        return [
            'people' => $people,
        ];
    }

    /**
     * 按世代分页获取家族人物列表，
     * @param $familyId
     * @param array 人物数组
     */
    public function getPeoplePositionByLevelForApp($familyId, $personId, $pageSize, array $data)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $level = $data['level'];

//        $sql = "select SQL_CALC_FOUND_ROWS id, name, sideText, profileText, remark, father as fatherId, type, level, gender,
//                family_index as familyIndex,case when type = 2 then '配偶' else ranking end as ranking from $this->TABLE
//                where familyId = $familyId and is_delete = 0";
//        $sqlForCount = "select count(1) as count from $this->TABLE where familyId = $familyId and is_delete = 0";
        $sql = "SELECT * FROM 
                    (SELECT id,@rowno := @rowno + 1 as num,
                        IF(id=$personId,(SELECT count(id) count from $this->TABLE 
                        where familyId = $familyId and is_delete = 0 and level = '$level'),0) as result from $this->TABLE g,
                    (SELECT @rowno := 0) r WHERE  familyId = $familyId and is_delete = 0 and level = '$level' order by family_index) 
                as temp where temp.result != 0";
//        print_r($sql);
        $position = $this->pdo->uniqueResult($sql);

        if (!$position) {
            #查无此人
            return [
                'next' => false,
                'previous' => false,
                'total' => '0',
                'paging' => [],
                'family'=>[
                    'maxLevel' => null,
                    'minLevel' => null
                ]
            ];

        } else {
            $index = $position['num'];
//            $length = $position['result'];
            $page = intval(ceil($index / $pageSize));
//            $offset = intval($page - 1) * $pageSize;

            return $this->getPeopleByLevelForApp($familyId, $page, $pageSize, $data);
        }

    }

    /**
     * get children ranking
     * @param $familyId
     * @param $children
     * @param array 人物数组
     */
    public function getAddPersonChildrenRanking($familyId,$ranking,$corePerson)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $result = array();
        foreach ($children as $key => $id){
            $sql = "SELECT id,ranking FROM $this->TABLE WHERE id = $id";
            $person = $this->pdo->query($sql);
            $result[$key]['id'] =  $person[0]['id'];
            $result[$key]['ranking'] =  $person[0]['ranking'];
        }
        return  $result;

    }


}
