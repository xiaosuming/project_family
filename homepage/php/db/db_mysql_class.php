<?php
/**
 * mysql类中使用了pdo的长连接扩展，因此不需要实现连接池
 * php是运行完会销毁所有资源，因此不能实现连接池，使用swoole可以实现php数据库连接池
 * 
 */

namespace DB;

use \PDO;
use \Redis;
use DB\RedisInstanceManager;
use Util\HttpContext;
use GuzzleHttp\Client;

class CDBManager 
{

    public $version = '';
    public $querynum = 0;
    public $link = null;
    public $querys=array();
    
    public $dbhost=null;
    public $dbuser=null;
    public $dbpw=null;
    public $dbname=null;
    public $pconnect=null;
    public $halt=null;
    public $dbcharset2=null;
    public $redis = null;
    private $isBuffered;

    /**
     * 构造函数
     * @param $dbhost 数据库地址
     * @param $dbuser 用户名
     * @param $dbpw   密码
     * @param $dbname 数据库名
     * @param $pconnect 是否采用长连接
     * @param dbcharset2 数据库编码
     */
    function __construct($dbhost, $dbuser, $dbpw, $dbname = '', $pconnect = true, $dbcharset2 = 'utf8mb4')
    {
        $this->dbhost=$dbhost;
        $this->dbuser=$dbuser;
        $this->dbpw=$dbpw;
        $this->dbname=$dbname;
        $this->pconnect=$pconnect;
        $this->dbcharset2=$dbcharset2;
        $this->isBuffered = false;
    }

    /**
     * 建立连接
     * @param $dbhost 数据库地址
     * @param $dbuser 数据库用户
     * @param $dbpw   数据库密码
     * @param $dbname 数据库名
     * @param $pconnect 是否长连接
     * return Object PDO对象
     */
    public static function createLink($dbhost, $dbuser, $dbpw, $dbname, $pconnect)
    {
        //这里的array(PDO::ATTR_PERSISTENT=>$pconnect)使用了mysql的长连接
        return new PDO("mysql:host=".$dbhost.";dbname=".$dbname, $dbuser, $dbpw, array(PDO::ATTR_PERSISTENT=>$pconnect));
    }


    public function connect() 
    {
        $this->link = self::createLink($this->dbhost, $this->dbuser, $this->dbpw, $this->dbname, $this->pconnect);
        if ($this->link)
        {
            $this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   
            $this->link->exec("set names ".$this->dbcharset2);   
        }
    }

    /**
     * 使用私有的数据库
     */
    private function usePrivateDB($sql) {
        $userId = HttpContext::getInstance()->getEnv('userId');
        $client = new Client([
            'base_uri' => $GLOBALS['PRIVATE_DB_PROXY'],
            'timeout' => 3.0
        ]);

        $response = $client->request('POST', '/ExecSQL', [
            'form_params' => [
                'userId' => $userId,
                'sql' => $sql
            ]
        ]);
        $contents = $response->getBody()->getContents();
        $response = json_decode($contents, true);


        if ($response != null && $response['code'] == 0) {
            $res = $response['data']['Res'];

            return json_decode($res, true);
        } else {
            throw new \PDOException('private db sql exec error' . $response['msg']);
        }
    }

    /**
     * 连接到redis数据库
     * 
     *
     */
    public function connectRedis(){
        $redisManager = RedisInstanceManager::getInstance();
        $this->redis = $redisManager->getCacheInstance();
    }

    /**
     * 设置缓存
     * @param $key 键
     * @param $value 值
     */
    public function setCache($key,$value){
        $this->connectRedis();
        $this->redis->set($key,$value);
    }

    /**
     * 获取缓存
     * @param $key 键
     * @return string 键对应的值
     */
    public function getCache($key){
        $this->connectRedis();
        $result = $this->redis->get($key);

        return $result;
    }

    /**
     * 删除缓存
     * @param $key 键
     * @return 删除结果
     */
    public function delCache($key){
        $this->connectRedis();
        $result = $this->redis->del($key);
        
        return $result;
    }

    /**
     * 从sql数据库中查询
     * @param $querystr 查询语句
     * @return mix 查询结果
     */
    private function sqlQuery($querystr){
        $stmt = $this->prepare($querystr);

        $stmt->execute();

        return $this->fetch_array($stmt);
    }

    /**
     * 从缓存或者sql数据库中查询
     * @param $querystr 查询语句
     * @param $cache    缓存
     * @param $expire   缓存过期时间
     * @return mix 查询结果
     */
    public function query($querystr,$cache = false,$cacheKey = "",$expire = 3600)
    {
        if (HttpContext::getInstance()->getEnv('private_db_host')) {
            // 这里使用私有的数据库
            return $this->usePrivateDB($querystr);
        }

        if($cache){
            //如果设置了缓存，先从缓存中查询
            if($cacheKey == "")
                $cacheKey = md5($querystr);
            
            $result = unserialize($this->getCache($cacheKey));
            if($result == FALSE){
                //如果缓存不存在
                $result = $this->sqlQuery($querystr);                              //从数据库中查询
                $this->setCache($cacheKey,serialize($result),Array('nx', 'ex'=>$expire));          //存放到缓存中
            }
            
            return $result;
        }else{
            return $this->sqlQuery($querystr);
        }
    }

    // 此次查询不要缓存数据
    public function setBuffered($isBuffered) {
        $this->isBuffered = $isBuffered;
    }

    /**
     * 从stmt中取出数组的结果
     * @param $stmt 查询句柄
     * @return mix 查询结果
     */
    public function fetch_array($stmt)
    {
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * sql prepare语句
     * @param $str 语句
     * @return object prepare好的语句
     */
    protected function prepare($str)
    {
        if (!$this->link)
        {
            $this->connect();
        }

        if (!$this->link)
        {
            return null;
        }

        $this->link->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, $this->isBuffered);

        return $this->link->prepare($str);
    }

    /**
     * 更新语句
     * @param $updatestr 更新的sql语句
     * @param $cache  是否更新缓存
     * @return int 更新的记录数
     */
    public function update($updatestr,$cache = false,$cacheKey = "")
    {

        if (HttpContext::getInstance()->getEnv('private_db_host')) {
            // 这里使用私有的数据库
            return $this->usePrivateDB($updatestr);
        }
        
        if($cache){
            if($cacheKey == "")
                $cacheKey = md5($updatestr);

            $this->delCache($cacheKey);     //直接删除缓存，下次查询时就会更新最新的缓存
        }

        $stmt = $this->prepare($updatestr);

        $stmt->execute();

        return $stmt->rowCount(); 	
    }

    public function insert($insertstr)
    {

        if (HttpContext::getInstance()->getEnv('private_db_host')) {
            // 这里使用私有的数据库
            return $this->usePrivateDB($insertstr);
        }

        $stmt = $this->prepare($insertstr);

        $stmt->execute();
        
        return $this->link->lastinsertid();
    }

    public function deletesql($deletestr)
    {
        if (HttpContext::getInstance()->getEnv('private_db_host')) {
            // 这里使用私有的数据库
            return $this->usePrivateDB($deletestr);
        }

        $stmt = $this->prepare($deletestr);

        $stmt->execute();

        return $stmt->rowCount();
    }
    
    /**
     * 返回单个结果或者空
     * @param $queryStr 查询字符串
     * @param $cache    是否缓存
     * @param $expire   缓存时间
     * @return mix 查询结果或者null
     */
    public function uniqueResult($queryStr,$cache = false,$expire = 3600){
        $result = $this->query($queryStr,$cache,$expire);

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
    
    //开启事务处理
    public function beginTransaction(){
        if (HttpContext::getInstance()->getEnv('private_db_host')) {
            // 这里使用私有的数据库
            return $this->usePrivateDB('begint');
        }

        $this->connect();
        $this->link->beginTransaction();
    }
    
    //事务处理失败，开始回滚
    public function rollback(){
        if (HttpContext::getInstance()->getEnv('private_db_host')) {
            // 这里使用私有的数据库
            return $this->usePrivateDB('rbackt');
        }
        $this->link->rollBack();
    }
    
    //提交当前事务
    public function commit(){
        if (HttpContext::getInstance()->getEnv('private_db_host')) {
            // 这里使用私有的数据库
            return $this->usePrivateDB('commit');
        }
        $this->link->commit();
    }

}