<?php
/**
 * 验证模块
 * author: jiangpengfei
 * date: 2017-03-29
 */

namespace DB;

use DB\CDBManager;
use DB\RedisConnect;
use Util\Util;
use DB\RedisInstanceManager;

class CDBAuth
{
    public $redis = null;

    public function init()
    {

        if (!$this->redis) {
            $redisManager = RedisInstanceManager::getInstance();
            $this->redis = $redisManager->getAuthInstance();
        }
        return true;
    }

    /**
     * 添加验证
     * @param $auth 验证码
     * @param $username 用户名
     * @param $password 密码（已经进行了加密）
     * @param $email 邮箱
     * @param $shareCode 分享码
     * @return bool true代表成功，false代表失败
     */
    public function addEmailAuth($auth, $username, $password, $email, $shareCode)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $data['u'] = $username;
        $data['p'] = $password;
        $data['e'] = $email;
        $data['s'] = $shareCode;
        $jsonStr = \json_encode($data);
        //将验证码对应的信息放到redis中，设置为12小时过期

        $result = $this->redis->setEx($auth, 43200, $jsonStr);


        return $result;
    }

    /**
     * 添加验证以提供找回绑定服务
     * @param $auth 验证码
     * @param $userId 用户Id
     * @param $email 邮箱
     * @return bool true代表成功，false代表失败
     */
    public function addEmailAuthForBinding($auth, $userId, $email)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $data['u'] = $userId;
        $data['e'] = $email;
        $jsonStr = \json_encode($data);
        //将验证码对应的信息放到redis中，设置为30分钟过期
        $result = $this->redis->setEx($auth, 1800, $jsonStr);
        return $result;
    }

    /**
     * 根据验证码获取注册的用户信息
     * @param $auth 验证码
     * @return string 获取用户信息的json字符串
     */
    public function getEmailAuth($auth)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $result = $this->redis->get($auth);
        return $result;

    }

    /**
     * 根据验证码获取绑定邮箱的用户信息
     * @param $auth 验证码
     * @return string 获取用户信息的json字符串
     */
    public function getEmailBindAuth($auth)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $result = $this->redis->get($auth);
        return $result;

    }

    /**
     * 为找回密码添加邮箱验证，30分钟内有效
     * @param $auth 验证序列号
     * @param $userId 用户id
     * @param $email 邮箱
     * @return bool true代表成功，false代表失败
     */
    public function addEmailAuthForRetrieve($auth, $userId, $email)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $data['u'] = $userId;
        $data['e'] = $email;
        $jsonStr = \json_encode($data);
        //将验证码对应的信息放到redis中，设置为30分钟过期

        $result = $this->redis->setEx($auth, 1800, $jsonStr);
        return $result;
    }

    /**
     * 根据auth获取找回密码的验证信息
     * @param $auth 验证系列号
     * @return mix 包含u和e的json string或者是false,u是userId,e是email
     */
    public function getEmailAuthForRetrieve($auth)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $result = $this->redis->get($auth);
        return $result;
    }


    /**
     * 删除验证
     * @param $auth 验证码
     * @return bool 删除是否成功 true代表成功,false代表失败
     */
    public function deleteEmailAuth($auth)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $result = $this->redis->delete($auth);
        return $result;

    }


    /**
     * 设置手机注册的验证码
     * @param $phone 手机号
     * @param $vcode 验证码
     * @return bool true代表成功，false代表失败
     */
    public function setPhoneAuth($phone, $vcode)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        //手机号验证5分钟过期
        $result = $this->redis->setEx($phone, 300, $vcode);
        return $result;
    }

    /**
     * 设置手机注册的验证码和记录id
     * @param $phone
     * @param $vcode
     * @return mixed
     */
    public function setPhoneAuthAndRecord($phone, $vcode, $record)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }
        $data['v'] = $vcode;
        $data['r'] = $record;
        $str = json_encode($data);

        //手机号验证5分钟过期
        $result = $this->redis->setEx($phone, 300, $str);
        return $result;
    }

    /**
     * 获取手机注册的验证码
     * @param $phone 手机号
     * @return mix false或者验证码
     */
    public function getPhoneAuth($phone)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $result = $this->redis->get($phone);
        return $result;
    }

    /**
     * 删除手机验证码
     * @param $phone 手机号
     * @return bool true代表成功，false代表失败
     */
    public function deletePhoneAuth($phone)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
        }

        $result = $this->redis->delete($phone);

        return $result;
    }


}

;
