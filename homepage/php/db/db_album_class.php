<?php
/**
 * Created by PhpStorm.
 * User: Kaishun Zhang
 * Date: 2017/11/8
 * Time: 12:21
 */

namespace DB;

use Util\Util;
use Model\Album;
use Model\Photo;

class CDBAlbum
{
    public $pdo = null;
    public $TABLE_FAMILY_PHOTO = 'gener_family_photo';
    public $TABLE_FAMILY_ALBUM = 'gener_family_album';
    public $TABLE_FAMILY_ALBUM_SHARE = 'gener_family_album_share';
    public $TABLE_ALBUM_PERSON = 'gener_album_person';
    public $TABLE_FAMILY = 'gener_family';
    public $TABLE_PERSON = 'gener_person';

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo) {
                    $GLOBALS['pdo'] = $this->pdo;
                }
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /*
     * 创建一个家族相册
     * @param $album_name
     * @param $description
     * @param $familyId
     * @param $create_by
     * @param $update_by
     * @return int  id 相册的id
     */
    public function createAlbum($album_name, $description, $familyId, $createBy, $updateBy)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO $this->TABLE_FAMILY_ALBUM(album_name,description,familyId,album_size,create_time,update_time,is_delete,update_by,create_by)VALUES('$album_name','$description','$familyId',0,NOW(),NOW(),0,'$updateBy','$createBy')";
        return $this->pdo->insert($sql);
    }

    /**
     * 编辑相册
     * @param $albumId 相册id
     * @param $album_name 相册名
     * @param $description 相册描述
     * @param $userId 用户Id
     * @return int 更新的记录数
     */
    public function editAlbum(int $albumId, string $album_name, string $description, int $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_FAMILY_ALBUM SET album_name = '$album_name',description = '$description',update_by = '$userId',update_time = now() WHERE id = '$albumId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 获取相册
     * @param $albumId 相册id
     * @return  结果集
     */
    public function getAlbumById($albumId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,album_name as albumName,description,familyId,album_size as albumSize,create_time as createTime,update_time 
 				as updateTime ,update_by as updateBy,create_by as createBy FROM $this->TABLE_FAMILY_ALBUM WHERE id='$albumId' AND is_delete= '0'";

        $result = $this->pdo->uniqueResult($sql);

        if ($result != null) {
            return new Album($result);
        }
        return null;
    }


    /**
     * 根据id获取图片信息
     * @param $photoId
     * @return Photo|null
     */
    public function getPhotoById($photoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $querySql = "SELECT id,albumId,photo,description,country_name as countryName,province_name as provinceName,city_name as cityName,area_name as areaName,town_name as townName,size,address,create_time as 
 				createTime ,create_by as createBy FROM $this->TABLE_FAMILY_PHOTO WHERE  id = '$photoId' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($querySql);
        if ($result != null) {
            return new Photo($result);
        }
        return null;
    }

    public function getBatchPhotosById($photoIdArr)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $str = implode(',', $photoIdArr);
        $querySql = "SELECT id,albumId,photo,description,country_name as countryName,province_name as provinceName,city_name as cityName,area_name as areaName,town_name as townName,size,address,create_time as 
 				createTime ,create_by as createBy FROM $this->TABLE_FAMILY_PHOTO WHERE  id IN ($str) AND is_delete='0'";
        $result = $this->pdo->query($querySql);
        return $result;
    }

    /**
     * 批量刪除之後的更細size 獲取圖片詳情
     * @param $photoStr
     * @return mixed
     */
    public function getBatchPhotoHaveDelete($photoStr)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $querySql = "SELECT id,albumId,photo,description,country_name as countryName,province_name as provinceName,city_name as cityName,area_name as areaName,town_name as townName,size,address,create_time as 
 				createTime ,create_by as createBy FROM $this->TABLE_FAMILY_PHOTO WHERE  id IN ($photoStr) AND is_delete= 1";
        $result = $this->pdo->query($querySql);
        return $result;
    }

    /**
     * 删除家族相册
     * @param $albumId
     * @return bool
     */
    public function deleteAlbum($albumId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE_FAMILY_ALBUM SET is_delete = '1' WHERE id = '$albumId' AND is_delete = '0'";
            $num = $this->pdo->update($sql);
            if ($num > 0) {
                //删除该相册内的照片
                $updateSql = "UPDATE $this->TABLE_FAMILY_PHOTO SET is_delete='1' WHERE albumId = '$albumId' AND is_delete='0'";
                $this->pdo->update($updateSql);
            }
            $this->pdo->commit();
            return true;
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            // @codeCoverageIgnoreEnd
        }
    }


    /**
     * 改变相册大小
     * @param $size
     * @param $ablumId
     * @param $updateBy
     * @param $type
     * @return bool
     */
    public function changeAlbumSize($size, $ablumId, $updateBy, $type)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        if ($type == 1) {
            $updateSql = "UPDATE $this->TABLE_FAMILY_ALBUM SET album_size = album_size + '$size', update_by = '$updateBy',update_time = NOW() WHERE id='$ablumId' AND is_delete=0 ";
        } else {
            $updateSql = "UPDATE $this->TABLE_FAMILY_ALBUM SET album_size = album_size - '$size', update_by = '$updateBy',update_time = NOW() WHERE id='$ablumId' AND is_delete=0 AND album_size >= '$size' ";
        }
        $num = $this->pdo->update($updateSql);
        if ($num > 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 关联照片和人物
     * @param $personIds
     * @param $photoId
     * @param $userId
     * @return bool
     */
    public function createAlbumPerson($personIds, $photoId, $userId)
    {
        if ($personIds == '') {
            // @codeCoverageIgnoreStart
            return true;
            // @codeCoverageIgnoreEnd
        }
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $idArray = explode(",", $personIds);
            if (count($idArray) > 0) {
                foreach ($idArray as $personId) {
                    $sql = "INSERT INTO $this->TABLE_ALBUM_PERSON(personId,photoId,create_time,create_by) VALUES ('$personId','$photoId',NOW(),'$userId')";
                    $this->pdo->insert($sql);
                }
            }
            $this->pdo->commit();
            return true;
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            // @codeCoverageIgnoreEnd
        }
    }


    /**
     * 清空照片人物
     * @param $photoId
     * @return mixed
     */
    public function clearPhotoPerson($photoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_ALBUM_PERSON SET isDelete=1 WHERE photoId = '$photoId' AND isDelete=0 ";
        return $this->pdo->update($sql);
    }

    /**
     * 更新照片关联的人物
     * @param $photoId
     * @param $personIds
     * @param $userId
     * @return bool
     */
    public function updatePhotoPerson($photoId, $personIds, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($personIds == '') {
            // @codeCoverageIgnoreStart
            $this->clearPhotoPerson($photoId);
            return true;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();

            //先清空人物关联
            $this->clearPhotoPerson($photoId);

            $idArray = explode(",", $personIds);
            if (count($idArray) > 0) {
                foreach ($idArray as $personId) {
                    $sql = "INSERT INTO $this->TABLE_ALBUM_PERSON(personId,photoId,create_time,create_by) VALUES ('$personId','$photoId',NOW(),'$userId')";
                    $this->pdo->insert($sql);
                }
            }
            $this->pdo->commit();
            return true;
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            // @codeCoverageIgnoreEnd
        }
    }


    /**
     * 添加图片到相册
     * @param $albumId
     * @param $photo
     * @param $description
     * @param $country
     * @param $countryName
     * @param $province
     * @param $provinceName
     * @param $city
     * @param $cityName
     * @param $area
     * @param $areaName
     * @param $town
     * @param $townName
     * @param $size
     * @param $address
     * @param $userId
     * @return int
     */
    public function addPhotoToAlbum($albumId, $photo, $description, $country, $countryName, $province, $provinceName,
                                    $city, $cityName, $area, $areaName, $town, $townName, $size, $address, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->TABLE_FAMILY_PHOTO(albumId,photo,description,country,country_name,province,province_name,city,
				city_name,area,area_name,town,town_name,size,address,create_time,is_delete,create_by)VALUES('$albumId','$photo','$description','$country','$countryName','$province',
			'$provinceName','$city','$cityName','$area','$areaName','$town','$townName','$size','$address',NOW(),'0','$userId')";
            $photoId = $this->pdo->insert($sql);
            if ($photoId > 0) {
                //更新相册
                if ($this->changeAlbumSize($size, $albumId, $userId, 1)) {
                    $this->pdo->commit();
                    return $photoId;
                } else {
                    $this->pdo->rollback();
                    return -1;
                }
            }
            // @codeCoverageIgnoreStart
            //不能添加图片
            $this->pdo->commit();
            return -1;
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * 编辑相片
     * @param $photoId 相片id
     * @param $description 照片描述
     * @param $country
     * @param $countryName
     * @param $province
     * @param $provinceName
     * @param $city
     * @param $cityName
     * @param $area
     * @param $areaName
     * @param $town
     * @param $townName
     * @param $address
     * @return mixed
     * @internal param 编辑的记录数 $return
     */
    public function editPhoto($photoId, $description, $country, $countryName, $province, $provinceName,
                              $city, $cityName, $area, $areaName, $town, $townName, $address)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_FAMILY_PHOTO SET description = '$description',country = '$country',country_name = '$countryName',province = '$province',province_name = '$provinceName',city = '$city',city_name = '$cityName',area = '$area',area_name = '$areaName',town = '$town',town_name = '$townName',address = '$address' WHERE id = '$photoId' AND is_delete = 0 ";

        return $this->pdo->update($sql);
    }


    /**
     * 删除一张照片
     * @param $photoId
     * @param $userId
     * @return bool
     */
    public function deletePhotoFromAlbum($photoId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $photo = $this->getPhotoById($photoId);
            if ($photo != null) {
                $sql = "UPDATE $this->TABLE_FAMILY_PHOTO SET is_delete = '1' WHERE id = '$photoId' AND is_delete = '0'";
                if ($this->pdo->update($sql) > 0) {
                    //更新相册
                    if ($this->changeAlbumSize($photo->size, $photo->albumId, $userId, 0)) {
                        $this->pdo->commit();
                        return true;
                    } else {
                        $this->pdo->rollback();
                        return false;
                    }
                }
            }
            //无该图片或者该图片已经删除
            $this->pdo->commit();
            return true;
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return false;
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * 批量刪除圖片
     * @param $photoStr
     * @param $userId
     * @return bool
     */
    public function deleteBatchPhotoFromAlbum($photoStr, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE_FAMILY_PHOTO SET is_delete = '1' WHERE id IN ($photoStr) AND is_delete = '0'";
            $updateRow = $this->pdo->update($sql);
            if ($updateRow > 0) {
                $result = $this->getBatchPhotoHaveDelete($photoStr);
                foreach ($result as $photo) {
                    //更新相册
                    $this->changeAlbumSize($photo['size'], $photo['albumId'], $userId, 0);
                }
            }
            //无该图片或者该图片已经删除
            $this->pdo->commit();
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return '-1';
            // @codeCoverageIgnoreEnd
        }
        return $updateRow;
    }

    /**
     * 获取相册总数
     * @param $familyId 家族id -1代表全部
     * @param $userId   用户id
     * @return int 相册总数
     */
    public function getAlbumCount($familyId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $where = "";
        if ($familyId == -1) {
            $where = "WHERE familyId in 
                    (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId')";
        } else if ($familyId == 0) {
            $where = "WHERE familyId='$familyId' AND create_by = '$userId'";
        } else {
            $where = "WHERE familyId = '$familyId'";
        }

        $sql = "SELECT count(id) 
                    FROM $this->TABLE_FAMILY_ALBUM 
                    $where AND is_delete='0'";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 得到带有photo的相册列表
     * @param $id
     * @return Album|null
     */
    public function getAlbum($id)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "select gfa.id as id,gfa.album_name as albumName,gfa.description as description
			,gfa.familyId as familyId ,gfa.album_size as albumSize, gfa.create_time as createTime,gfa.update_time AS
			updateTime,gfa.update_by as updateBy,gfa.create_by as createBy,gfp.photo as photo from
			$this->TABLE_FAMILY_ALBUM gfa left JOIN $this->TABLE_FAMILY_PHOTO gfp ON gfa.id = gfp.albumId AND gfp.is_delete ='0' WHERE gfa.id  = '$id' LIMIT 0,1";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return new Album($result);
        }
        return null;
    }

    /**
     * 获取一页相册数据
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @param $familyId  家族id
     * @param $userId    用户id
     * @return array 一页相册数据
     */
    public function getAlbumPaging($pageIndex, $pageSize, $familyId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $where = "";
        if ($familyId == -1) {
            $where = "WHERE familyId in 
                    (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId')";
        } else if ($familyId == 0) {
            $where = "WHERE familyId='$familyId' AND create_by = '$userId'";
        }else {
            $where = "WHERE familyId = '$familyId'";
        }

        $sql = "SELECT id  
  				  	FROM $this->TABLE_FAMILY_ALBUM 
                    $where  AND is_delete='0' 
                    ORDER BY id DESC 
                    LIMIT $offset,$pageSize";

        $idResult = $this->pdo->query($sql);
        $result = Array();
        for ($i = 0; $i < sizeof($idResult); $i++) {
            $tmpAlbum = $this->getAlbum($idResult[$i]['id']);
            $tmpAlbum->photoCount = $this->getPhotoCount($idResult[$i]['id']);
            array_push($result, $tmpAlbum);
        }
        return $result;
    }

    public function getFamilyAlbumByFamilyId($familyId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id  
  				  	FROM $this->TABLE_FAMILY_ALBUM 
                    WHERE familyId = '$familyId'  AND is_delete='0' 
                    ORDER BY id DESC ";

        $idResult = $this->pdo->query($sql);
        $result = Array();
        for ($i = 0; $i < sizeof($idResult); $i++) {
            $tmpAlbum = $this->getAlbum($idResult[$i]['id']);
            $tmpAlbum->photoCount = $this->getPhotoCount($idResult[$i]['id']);
            array_push($result, $tmpAlbum);
        }
        return $result;
    }

    /**
     * 获取照片总数
     * @param $albumId 相册id
     * @return int 所在相册的照片总数
     */
    public function getPhotoCount($albumId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT count(id) 
                    FROM $this->TABLE_FAMILY_PHOTO
                   WHERE albumId='$albumId' AND is_delete='0'";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取上一张和下一张的相片详情
     * @param $albumId 相册id
     * @param $photoId 相片id
     * @return map 上一张和下一张的map
     */
    public function getPrevAndNextPhoto($albumId, $photoId) {
        $this->init();

        $sql = "SELECT id,albumId,photo,description,country_name as countryName,province_name as provinceName,
			area_name as areaName,town_name as townName,size,address,create_time as createTime,create_by as createBy 
			FROM $this->TABLE_FAMILY_PHOTO WHERE albumId = '$albumId' AND id < $photoId AND is_delete ='0'
			ORDER BY id DESC LIMIT 1";
        $next = $this->pdo->uniqueResult($sql);

        $sql = "SELECT id,albumId,photo,description,country_name as countryName,province_name as provinceName,
			area_name as areaName,town_name as townName,size,address,create_time as createTime,create_by as createBy 
			FROM $this->TABLE_FAMILY_PHOTO WHERE albumId = '$albumId' AND id > $photoId AND is_delete ='0'
			ORDER BY id ASC LIMIT 1";
        $prev = $this->pdo->uniqueResult($sql);

        return [
            'prev' => $prev,
            'next' => $next
        ];
    }

    /**
     * 获取一页照片数据
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @param $albumId  相册id
     * @return array 一页相册数据
     */
    public function getPhotoPaging($pageIndex, $pageSize, $albumId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT id,albumId,photo,description,country_name as countryName,province_name as provinceName,
			area_name as areaName,town_name as townName,size,address,create_time as createTime,create_by as createBy 
			FROM $this->TABLE_FAMILY_PHOTO WHERE albumId = '$albumId' AND is_delete ='0'
			ORDER BY id DESC 
            LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

//    /**
//     * 根据相册id获取相册下的所有图片
//     * @param $albumId
//     * @return mixed
//     */
//    public function getAllPhotosByAlbumId($albumId)
//    {
//        if (!$this->init()) {
//            // @codeCoverageIgnoreStart
//            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
//            exit;
//            // @codeCoverageIgnoreEnd
//        }
//
//        $sql = "SELECT id,albumId,photo,description,country_name as countryName,province_name as provinceName,
//			area_name as areaName,town_name as townName,size,address,create_time as createTime,create_by as createBy
//			FROM $this->TABLE_FAMILY_PHOTO WHERE albumId = '$albumId' AND is_delete ='0'
//			ORDER BY id DESC ";
//        return $this->pdo->query($sql);
//    }

    /**
     * @param familyId 家族Id
     * @param albumId 相册Id 为-1 时 搜索的范围为整个family
     * @param name 查找的人的姓名
     * @return 数量
     */
    public function getPersonPhotoCount($familyId, $albumId, $name)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = '';
        if ($name != '') {
            $sql = "SELECT count(fp.id) from $this->TABLE_PERSON person
						LEFT JOIN $this->TABLE_ALBUM_PERSON ap ON person.id = ap.personId
						LEFT JOIN $this->TABLE_FAMILY_PHOTO fp ON ap.photoId = fp.id
						WHERE person.is_delete = '0'  AND
					  fp.is_delete = '0' AND person.name = '$name' ";
        } else {
            $sql = "SELECT count(fp.id) from $this->TABLE_FAMILY_PHOTO fp
				WHERE fp.is_delete = '0' ";
        }
        if ($albumId == -1) {
            $sql = $sql . "AND fp.albumId IN 
					(
					SELECT id FROM  $this->TABLE_FAMILY_ALBUM fa WHERE  fa.is_delete = '0' AND  
					fa.familyId = '$familyId'
					)";

        } else {
            $sql = $sql . " AND fp.albumId = '$albumId'";
        }
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(fp.id)'];
    }

    /**
     * 获取指定人的一页照片数据
     * @param familyId 家族id
     * @param albumId 相册id
     * @param name 查找人的名称
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @return array 一页相册数据
     */
    public function getPersonPhotoPaging($familyId, $albumId, $pageIndex, $pageSize, $name)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $offset = ($pageIndex - 1) * $pageSize;
        $sql = '';
        if ($name != '') {
            $sql = "SELECT fp.id as id ,fp.albumId as albumId,fp.photo as photo,fp.description as description,fp.country_name as countryName,fp.province_name as provinceName,
					fp.area_name as areaName,fp.town_name as townName,fp.size,fp.address,fp.create_time as createTime,fp.create_by as createBy 
					from $this->TABLE_PERSON person
						LEFT JOIN $this->TABLE_ALBUM_PERSON ap ON person.id = ap.personId
						LEFT JOIN $this->TABLE_FAMILY_PHOTO fp ON ap.photoId = fp.id
						WHERE person.is_delete = '0'  AND
					  fp.is_delete = '0' AND person.name = '$name' ";
        } else {
            $sql = "SELECT fp.id as id ,fp.albumId as albumId,fp.photo as photo,fp.description as description,fp.country_name as countryName,fp.province_name as provinceName,
				fp.area_name as areaName,fp.town_name as townName,fp.size,fp.address,fp.create_time as createTime,fp.create_by as createBy 
				from $this->TABLE_FAMILY_PHOTO fp
				WHERE fp.is_delete = '0' ";
        }
        if ($albumId == -1) {
            $sql = $sql . "AND fp.albumId IN 
					(
					SELECT id FROM  $this->TABLE_FAMILY_ALBUM fa WHERE  fa.is_delete = '0' AND  
					fa.familyId = '$familyId'
					)";
        } else {
            $sql = $sql . " AND fp.albumId = '$albumId'";
        }
        $sql = $sql . "ORDER BY fp.id DESC 
            		LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    /**
     * 获取指定人的一页照片数据
     * @param familyId 家族id
     * @param albumId 相册id
     * @param name 查找人的名称
     * @return array 一页相册数据
     */
    public function getAllPhotosDetail($familyId, $albumId, $name)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = '';
        if ($name != '') {
            $sql = "SELECT fp.id as id ,fp.albumId as albumId,fp.photo as photo,fp.description as description,fp.country_name as countryName,fp.province_name as provinceName,
					fp.area_name as areaName,fp.town_name as townName,fp.size,fp.address,fp.create_time as createTime,fp.create_by as createBy 
					from $this->TABLE_PERSON person
						LEFT JOIN $this->TABLE_ALBUM_PERSON ap ON person.id = ap.personId
						LEFT JOIN $this->TABLE_FAMILY_PHOTO fp ON ap.photoId = fp.id
						WHERE person.is_delete = '0'  AND
					  fp.is_delete = '0' AND person.name = '$name' ";
        } else {
            $sql = "SELECT fp.id as id ,fp.albumId as albumId,fp.photo as photo,fp.description as description,fp.country_name as countryName,fp.province_name as provinceName,
				fp.area_name as areaName,fp.town_name as townName,fp.size,fp.address,fp.create_time as createTime,fp.create_by as createBy 
				from $this->TABLE_FAMILY_PHOTO fp
				WHERE fp.is_delete = '0' ";
        }
        if ($albumId == -1) {
            $sql = $sql . "AND fp.albumId IN 
					(
					SELECT id FROM  $this->TABLE_FAMILY_ALBUM fa WHERE  fa.is_delete = '0' AND  
					fa.familyId = '$familyId'
					)";
        } else {
            $sql = $sql . " AND fp.albumId = '$albumId'";
        }
        $sql = $sql . "ORDER BY fp.id DESC";
        return $this->pdo->query($sql);
    }

    /**
     * 得到一张照片中的人物
     */
    public function getPersonInPhoto($photoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT p.name as name  from $this->TABLE_ALBUM_PERSON ap 
		  INNER JOIN    $this->TABLE_PERSON  p ON  ap.personId = p.id
		  WHERE ap.photoId = '$photoId' AND ap.isDelete=0";;
        return $this->pdo->query($sql);
    }

    /**
     * 添加相册分享记录
     * @param $albumIdOrPhotoId
     * @param $type
     * @param $shareCode
     * @param $timeLimit
     * @param $userId
     * @return mixed
     */
    public function addShareAlbum($albumIdOrPhotoId, $type, $shareCode, $timeLimit, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_FAMILY_ALBUM_SHARE (albumIdOrPhotoId,type,shareCode,timeLimit,createTime,createBy,updateTime,updateBy)
      VALUES ('$albumIdOrPhotoId','$type','$shareCode','$timeLimit',now(),'$userId',now(),'$userId')";
        return $this->pdo->insert($sql);
    }

    /**
     * 通过分享码获取分享的信息
     * @param $shareCode
     * @return mixed
     */
    public function getAlbumShareByCode($shareCode)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT albumIdOrPhotoId,type,timeLimit,createBy,createTime,updateBy,updateTime 
                FROM $this->TABLE_FAMILY_ALBUM_SHARE 
                WHERE shareCode = '$shareCode' AND isDelete=0";
        return $this->pdo->uniqueResult($sql);
    }

}
