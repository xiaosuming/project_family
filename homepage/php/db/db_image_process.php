<?php
/**
 * 图片处理的相关模块
 * author: jiangpengfei
 * date: 2019-03-13
 */

namespace DB;

use DB\CDBManager;
use Util\Util;

class CDBImageProcess
{
    public $pdo = null;
    public $TABLE = "gener_ai_image_process";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 增加图片处理记录
     * @param $familyId 家族id
     * @param $imageUrl 图片地址
     * @param $result 结果
     * @return int 增加的祖坟Id
     */
    public function addAIImageProcess($familyId, $imageUrl, $result, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE(familyId,image,result,createTime,createBy, updateTime, updateBy)
                VALUES('$familyId','$imageUrl','$result',now(), '$userId', now(), '$userId')";

        return $this->pdo->insert($sql);

    }

    /**
     * 更新图片处理结果
     * @param $taskId 任务id
     * @param $result 结果
     * @return int 修改的记录个数
     */
    public function updateImageProcessResult($taskId, $result)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set result = '$result', updateTime = now() WHERE id = '$taskId' ";

        return $this->pdo->update($sql);
    }
}
