<?php
/**
 * 处理用户粉丝相关的类
 * author: jiangpengfei
 * date:  2017-05-07
 */

namespace DB;

use DB\CDBManager;
use Util\Util;
use Util\SysLogger;

/**
 * UserFollowDB封装了用户粉丝的处理。
 * Follow这里指的是用户的三代内的直系亲属，以及在名片上互相关注的,以及在社区中互相关注的
 */
class UserFollowDB {
    private $redis;
    private $pdo = null;

    private $TABLE = 'gener_user_follow';
    private $TABLE_USER = 'gener_user';

    public function __construct() {
        $redisManager = RedisInstanceManager::getInstance();
        $this->redis = $redisManager->getStorageInstance();
    }

    /**
     * @codeCoverageIgnore
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 获取用户的粉丝
     * @param int $userId 用户id
     * @return array 用户粉丝数组
     */
    public function getUserFollow(int $userId) {

        $key = $this->getKey($userId);
        $result = $this->redis->sMembers($key);
        return $result;
    }

    /**
     * 分页获取用户的粉丝列表
     * @param int $userId
     * @return array 粉丝数组
     */
    public function getUserFollowPaging(int $pageIndex, int $pageSize, int $userId) {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;
        
        $sql = "SELECT tu.nickname, tu.photo, t.createTime as followTime FROM $this->TABLE t 
                inner join $this->TABLE_USER tu on tu.id = t.followerId WHERE t.userId = '$userId' AND t.isDelete = '0' order by t.id limit $offset,$pageSize ";

        return $this->pdo->query($sql);
    }

    /**
     * 更新用户的粉丝
     * @param int $userId 用户id
     * @param int $appendIds 增加的粉丝id数组
     * @return bool true成功，false失败
     */
    public function appendUserFollow(int $userId, array $appendIds) {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        if (count($appendIds) == 0) {
            return false;
        }

        $sql = "INSERT INTO $this->TABLE (userId, followerId, createTime, createBy, updateTime, updateBy) VALUES ";

        $isFirst = true;
        foreach($appendIds as $appendId) {
            if ($isFirst) {
                $isFirst = false;
                $sql .= "('$userId', '$appendId', now(), '$appendId', now(), '$appendId')";
            } else {
                $sql .= ",('$userId', '$appendId', now(), '$appendId', now(), '$appendId')";
            }
        }
        $this->pdo->insert($sql);

        $key = $this->getKey($userId);

        foreach($appendIds as &$tmpId) {
            $result = $this->redis->sAdd($key, $tmpId);
        }
        return $result;
    }

    /**
     * 移除用户的粉丝
     * @param int $userId 用户id
     * @param array $removeId 要移除的用户id数组
     * @return bool true成功,false失败
     */
    public function removeUserFollow(int $userId, array $removeIds) {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        if (count($removeIds) == 0) {
            return false;
        }

        $removeIdStr = Util::arrayToString($removeIds);

        $sql = "UPDATE $this->TABLE set isDelete = '1' WHERE followerId in ($removeIdStr) ";

        try {
            $this->pdo->update($sql);

            $key = $this->getKey($userId);

            foreach($removeIds as $tmpId) {
                $result = $this->redis->sRem($key, $tmpId); 
            }

            return $result;
        } catch (\PDOException $e) {
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
        }

        return false;
    }

    /**
     * 获取存储的key值
     */
    private function getKey($userId) {
        return 'uc_'.$userId;
    }

}