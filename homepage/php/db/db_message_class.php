<?php
/**
 * 信息模块
 * author: jiangpengfei
 * date: 2017-02-08
 */

namespace DB;

use DB\CDBManager;
use Model\Message;
use Util\SysLogger;
use Util\Util;
use DB\CDBFamily;
use DB\CDBPerson;
use ThirdParty\InstantMessage;
use DB\CDBAccount;

class CDBMessage
{
    public $pdo = null;
    public $TABLE = "gener_message";
    public $TABLE_USER = "gener_user";
    public $TABLE_INFORMATION_FOLLOW = 'gener_information_follow';
    public $TABLE_INFORMATION_CATEGORY = 'gener_information_category';

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 新增消息
     * @codeCoverageIgnore
     * @param $fromUser 消息来源用户id
     * @param $toUser   消息接受用户id
     * @param $content  消息内容
     * @param $module   模块的id
     * @param $recordId 记录的id
     * @param $action   消息的动作，当一个模块中存在多种消息动作时，可以使用这个字段来区分
     * @param $messageCode 这个是用来标记是同一消息的code值，在申请加入家族中，消息会发送给所有的家族管理员
     * @return int 返回消息的id
     */
    public function addMessage(Message $message)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE(message_code,from_user,to_user,content,module,action,recordId,extra_recordId,ext,create_by,create_time,update_by,update_time)
                    VALUES('$message->messageCode','$message->fromUser','$message->toUser','$message->content','$message->module','$message->action','$message->recordId','$message->extraRecordId','$message->ext','$message->fromUser',now(),'$message->fromUser',now())";
        $msgId = $this->pdo->insert($sql);

        /* 通过IM系统也发送一份 */
        //查出要发送的用户的环信用户名
        $accountDB = new CDBAccount();
        $hxUserInfo = $accountDB->getHuanxinInfoById($message->toUser);

        $userInfo = $accountDB->getUserInfoById($message->fromUser);

        $im = new InstantMessage();

        $users = array();
        array_push($users, $hxUserInfo['hxUsername']);
        $message->id = $msgId;
        $message->fromUserName = $userInfo['nickname'];
        $message->fromUserPhoto = $userInfo['photo'];
        $message->createBy = $message->fromUser;
        $message->createTime = Util::getCurrentTime();

        // $im->sendTextMessageToUser($users, $message->content, $message);
        $im->sendCustomMessageToUser($users, $message->content, json_encode($message));

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        return $msgId;
    }

    /**
     * 获取消息详情
     * @param $messageId 消息id
     * @return object 消息的详细内容
     */
    public function getMessage($messageId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.message_code as  messageCode,tb.from_user as fromUser,tu.nickname as fromUserName,tb.to_user as toUser,tb.content,tb.module,tb.action,tb.recordId,tb.extra_recordId as extraRecordId,tb.ext,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime,tb.isDelete,tb.isRead,tb.isAccept FROM $this->TABLE tb 
                    INNER JOIN $this->TABLE_USER tu ON tu.id = tb.from_user 
                    WHERE tb.id = '$messageId' AND tb.isDelete = '0' ";
        
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return new Message($result);
        }
        return null;
    }

    /**
     * 删除消息,这里不能直接删除，通过更新delete字段为1标记为删除
     * @param $msgIds
     * @param $userId 消息所属的用户id
     * @return int 删除的记录数
     * @internal param 消息id $msgId
     */
    public function deleteMessage($msgIds, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $idArray = explode(",", $msgIds);
        $where = " WHERE to_user = '$userId' AND ";
        $isFirst = true;
        foreach ($idArray as $value) {
            if ($isFirst) {
                $where .= " id = '$value' ";
                $isFirst = false;
            } else
                //@codeCoverageIgnoreStart
                $where .= " OR id = '$value' ";
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET isDelete = '1' $where";
        $deleteRows = $this->pdo->update($sql);
        return $deleteRows;
    }

    /**
     * 获取一页消息
     * @param $pageIndex
     * @param $pageSize  每页大小
     * @param $toUser 接受消息的用户id
     * @param $module 模块id -1代表全部
     * @param $isRead -1代表全部,0代表未读,1代表已读
     * @return array 一页的数据
     * @internal param $pageIndex　页码
     */
    public function getMessagesPaging($pageIndex, $pageSize, $toUser, $module, $isRead)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;
        if ($isRead == 0) {
            $isRead = 1;
            //@codeCoverageIgnoreStart
        } else if ($isRead == 1) {
            $isRead = 0;
            //@codeCoverageIgnoreEnd
        }

        $where = "";

        if ($module != -1) {
            $where = "  AND tb.module = '$module'";
        }

        $sql = "SELECT tb.id, tb.from_user as fromUser,tb.content,tb.module,tb.action,tb.recordId,tb.extra_recordId as extraRecordId,tb.isRead,tb.isAccept,tb.ext,tb.create_by as createBy,tu.nickname as fromUserName,tu.photo as fromUserPhoto,tb.create_time as createTime FROM $this->TABLE tb 
            inner join $this->TABLE_USER as tu on tu.id = tb.from_user 
            WHERE tb.to_user = '$toUser' AND tb.isDelete = '0' AND tb.isRead != '$isRead' 
             $where  ORDER BY tb.id DESC limit $offset,$pageSize ";

        return $this->pdo->query($sql);
    }

    /**
     * 获取符合条件的消息总数
     * @param $toUser 接受消息的用户id
     * @param $module 模块id -1代表全部
     * @param $isRead -1代表全部，０代表未读，１代表已读
     * @return int 符合条件的消息总数
     */
    public function getMessagesCount($toUser, $module, $isRead)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($isRead == 0) {
            $isRead = 1;
            //@codeCoverageIgnoreStart
        } else if ($isRead == 1) {
            $isRead = 0;
            //@codeCoverageIgnoreEnd
        }

        $where = "";

        if ($module != -1) {
            $where = "  AND tb.module = '$module'";
        }

        $sql = "SELECT count(tb.id) FROM $this->TABLE as tb 
                    inner join $this->TABLE_USER as tu on tu.id = tb.from_user  
                    WHERE tb.to_user = '$toUser' AND tb.isDelete = '0' AND tb.isRead != '$isRead' $where ";
        $result = $this->pdo->uniqueResult($sql);

        return $result['count(tb.id)'];
    }


    /**
     * 批量更新消息的已读或未读状态
     * @param $messageIds 要更新状态的消息的id数组
     * @param $isRead 更新成已读还是未读 true代表更新成已读.false代表更新成未读
     * @param $userId 该操作的用户id，主要用来在where语句中做限制条件，防止用户越权操作
     * @return int 更新的记录数
     */
    public function updateMessageStatus($messageIds, $isRead, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $idArray = explode(",", $messageIds);
        $where = " WHERE to_user = '$userId' AND ";
        $isFirst = true;
        foreach ($idArray as $value) {
            if ($isFirst) {
                $where .= " id = '$value' ";
                $isFirst = false;
            } else
                //@codeCoverageIgnoreStart
                $where .= " OR id = '$value' ";
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET isRead = '$isRead' $where";
        return $this->pdo->update($sql);
    }

    /**
     * 更新消息的接受状态
     * @param $messageId
     * @return bool 接受状态 true代表成功,false代表失败
     * @internal param 消息id $messageId
     */
    public function acceptInvite($messageId): bool
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set isAccept = 1 WHERE id = '$messageId' AND isDelete = '0' ";
        if ($this->pdo->update($sql) > 0) {
            //@codeCoverageIgnoreStart
            return true;
            //@codeCoverageIgnoreEnd
        } else {
            return false;
        }
    }

    /**
     * 拒绝通过消息的邀请
     * @param $messageId
     * @return bool 接受状态 true代表成功,false代表失败
     * @internal param 消息id $messageId
     */
    public function refuseInvite($messageId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set isAccept = -1 WHERE id = '$messageId' AND isDelete = '0' ";
        if ($this->pdo->update($sql) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 接受申请加入家族
     * @param $messageId 消息id
     * @param $updateUserId 更新的用户id
     * @return boolean 接受状态,true代表成功,false代表失败
     */
    public function acceptApplyJoin($messageId, $updateUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT message_code,from_user,module,action,recordId FROM $this->TABLE WHERE id = '$messageId' AND isDelete = '0' ";
        $msgResult = $this->pdo->uniqueResult($sql);
        if ($msgResult == null) {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();

            $msgCode = $msgResult['message_code'];
            $fromUser = $msgResult['from_user'];
            $action = $msgResult['action'];

            $sql = "UPDATE $this->TABLE set isAccept = '1',update_by = '$updateUserId',update_time = now() 
                        WHERE isDelete = '0' AND message_code = '$msgCode' AND from_user = '$fromUser' AND action = '$action' AND isAccept = '0' ";

            if ($this->pdo->update($sql) > 0) {
                //向gener_family_user表中插入数据
                //@codeCoverageIgnoreStart
                $familyDb = new CDBFamily();
                $familyDb->addFamilyUser($msgResult['recordId'], $msgResult['from_user']);
                $this->pdo->commit();
                return true;
                //@codeCoverageIgnoreEnd
            } else {
                $this->pdo->commit();
                return false;
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            $logger->error(Util::exceptionFormat($e));
        }
    }

    //@codeCoverageIgnoreEnd

    /**
     * 接受申请加入家族并自动关注圈子
     * @param $messageId
     * @param $updateUserId
     * @param $socialCircleId
     * @return bool
     */
    public function acceptApplyJoinAndFollowCircle($messageId, $updateUserId, $socialCircleId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT message_code,from_user,module,action,recordId,ext FROM $this->TABLE WHERE id = '$messageId' AND isDelete = '0' ";
        $msgResult = $this->pdo->uniqueResult($sql);
        if ($msgResult == null) {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();

            $msgCode = $msgResult['message_code'];
            $fromUser = $msgResult['from_user'];
            $action = $msgResult['action'];
            $extData = json_decode($msgResult['ext'],true);
            $inviteUserId = $extData['inviteUserId'];

            $sql = "UPDATE $this->TABLE set isAccept = '1',update_by = '$updateUserId',update_time = now() 
                        WHERE isDelete = '0' AND message_code = '$msgCode' AND from_user = '$fromUser' AND action = '$action' AND isAccept = '0' ";

            if ($this->pdo->update($sql) > 0) {
                //向gener_family_user表中插入数据
                //@codeCoverageIgnoreStart
                $familyDb = new CDBFamily();
                $tmpFamilyUserId = $familyDb->addFamilyUser($msgResult['recordId'], $msgResult['from_user']);
                if ($tmpFamilyUserId < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加失败');
                    $this->pdo->rollback();
                    exit;
                }
                // 关注圈子
                $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
                VALUES('$fromUser','$socialCircleId',now(),'$fromUser',now(),'$fromUser')";
                $followId = $this->pdo->insert($sql);
                if ($followId < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                    $this->pdo->rollback();
                    exit;
                }
                // 更新圈子关注数量
                $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow+1 WHERE id='$socialCircleId' AND is_delete=0";
                $updateRow = $this->pdo->update($sql);
                if ($updateRow < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
                    $this->pdo->rollback();
                    exit;
                }

                $personDB = new CDBPerson();
                $personDB->insertPersonBindUserRecord($msgResult['recordId'], 0, $inviteUserId, $fromUser, 3, $updateUserId);
                $this->pdo->commit();
                return true;
                //@codeCoverageIgnoreEnd
            } else {
                $this->pdo->rollback();
                return false;
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
        }
    }

    /**
     * 拒绝申请加入家族
     * @param $messageId 消息id
     * @param $updateUserId 更新用户id
     * @return boolean 更新结果,true代表更新成功,false代表更新失败
     */
    public function refuseApplyJoin($messageId, $updateUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT message_code,from_user,action FROM $this->TABLE WHERE id = '$messageId' AND isDelete = '0'";
        $msgResult = $this->pdo->uniqueResult($sql);
        if ($msgResult == null) {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }

        $msgCode = $msgResult['message_code'];
        $fromUser = $msgResult['from_user'];
        $action = $msgResult['action'];

        $sql = "UPDATE $this->TABLE set isAccept = '-1',update_by = '$updateUserId',update_time = now() 
                    WHERE isDelete = '0' AND message_code = '$msgCode' AND from_user = '$fromUser' AND action = '$action'  AND isAccept = '0' ";

        if ($this->pdo->update($sql) > 0)
            //@codeCoverageIgnoreStart
            return true;
        //@codeCoverageIgnoreEnd
        else
            return false;
    }

    /**
     * 发送合并家族的消息
     * @codeCoverageIgnore
     * @param $fromfamilyId 发送消息的家族id
     * @param $toFamilyId   接受消息的家族id
     * @param $fromPersonId 发送消息的家族的合并人物id
     * @param $fromUserId   发送消息的用户id
     * @param $remark       合并家族消息的备注内容
     * @param $mergeType    合并类型，1是联姻，2是通过同一个人来合并 3是合并生成新家族
     * @param ext  额外的json信息
     * @return int 正数代表发送成功，为消息id，负数代表发送失败,-1代表家族不存在
     */
    public function sendMergeFamilyMessage(int $fromfamilyId, int $toFamilyId, int $fromPersonId, int $fromUserId, string $remark, int $mergeType)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $FAMILY_NOT_EXIST_ERROR = -1;
        $module = $GLOBALS['FAMILY_MODULE'];

        $familyDB = new CDBFamily();
        $personDB = new CDBPerson();

        //第一步:查询接受消息的家族创始人id
        $toOriginatorId = $familyDB->getFamilyOriginatorId($toFamilyId);
        if ($toOriginatorId == null) {
            //家族不存在
            return $FAMILY_NOT_EXIST_ERROR;
        }




        if ($mergeType == 1) {
            $action = $GLOBALS['MERGE_FAMILY_BY_MARRIAGE'];
        } else if ($mergeType == 2) {
            $action = $GLOBALS['MERGE_FAMILY_BY_PERSON'];
        }else if($mergeType == 3){
            $action = $GLOBALS['MERGE_FAMILYS_TO_NEW_FAMILY'];
        }

        $message = new Message();
        $fromFamilyInfo = $familyDB->getFamilyById($fromfamilyId);
        $toFamilyInfo = $familyDB->getFamilyById($toFamilyId);
        $fromPerson  = $personDB->getPersonById2($fromPersonId);
        //第二步:发送消息
        $message->fromUser = $fromUserId;
        $message->toUser = $toOriginatorId;
        $message->content = $remark;
        $message->module = $module;
        $message->recordId = $fromPersonId;
        $message->extraRecordId = $toFamilyId;
        $message->action = $action;
        $extData = [
            "fromFamilyName" => $fromFamilyInfo->name,
            "toFamilyName" => $toFamilyInfo->name,
            "fromPersonName" => $fromPerson->name,
            "fromFamilyId" =>  $fromfamilyId,
            "fromPersonId" => $fromPersonId
        ];
        $message->ext = json_encode($extData, JSON_UNESCAPED_UNICODE);


        $messageId = $this->addMessage($message);
        return $messageId;
    }

    /**
     * 接受家族合并
     * @param Message $message
     * @param 接受申请的家族要绑定的人物|int $toPersonId 接受申请的家族要绑定的人物
     * @return int 正数表示接受成功，为更新的记录id，非正数代表失败,-1代表人物不存在,-2代表人物是同一个家族的,-3代表人物已存在合并的家族
     * @internal param 消息id $messageId
     */
    public function acceptMergeFamily(Message $message, int $toPersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT message_code,from_user,module,action,recordId,ext FROM $this->TABLE WHERE id = $message->id  AND isDelete = 0 ";
        $msgResult = $this->pdo->uniqueResult($sql);

        $fromPersonId = $message->recordId;     //发送申请方要绑定的人物id
        $familyDB = new CDBFamily();
        $personDB = new CDBPerson();


        $fromPerson =  $personDB->getPersonById2($fromPersonId);
        $toPerson =  $personDB->getPersonById2($toPersonId);
        $updateRecord = $familyDB->mergeFamily($fromPersonId, $toPersonId, $message->action);
        // 非正数代表合并失败,-1代表人物不存在,-2代表人物是同一个家族的,-3代表人物已存在合并的家族
        if ($updateRecord <= 0) {
            return $updateRecord;
        }
        if($message->action  ==  $GLOBALS['MERGE_FAMILYS_TO_NEW_FAMILY']){
            $toPersonId = $updateRecord;
         }
        //@codeCoverageIgnoreStart
        //合并成功，更新消息的已读状态
        $status = $this->acceptInvite($message->id);
          // 添加合并记录
        $familyDB->addFamilyMergeRecord($fromPerson->familyId, $toPerson->familyId, $message->action,$fromPersonId,$toPersonId);
        return $updateRecord;
        //@codeCoverageIgnoreEnd
    }


    /**
     * 发送合并分支的消息
     * @codeCoverageIgnore
     * @param $fromfamilyId 发送消息的家族id
     * @param $toFamilyId   接受消息的家族id
     * @param $fromPersonId 发送消息的家族的合并人物id
     * @param $fromUserId   发送消息的用户id
     * @param $remark       合并家族消息的备注内容
     * @return int 正数代表发送成功，为消息id，负数代表发送失败,-1代表家族不存在
     */
    public function sendMergeBranchMessage(int $fromfamilyId, int $toFamilyId, int $fromPersonId, int $fromUserId, string $remark)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $FAMILY_NOT_EXIST_ERROR = -1;
        $module = $GLOBALS['FAMILY_MODULE'];

        $action = $GLOBALS['MERGE_BRANCH'];

        //第一步:查询接受消息的家族创始人id
        $familyDB = new CDBFamily();
        $toOriginatorId = $familyDB->getFamilyOriginatorId($toFamilyId);
        if ($toOriginatorId == null) {
            //家族不存在
            return $FAMILY_NOT_EXIST_ERROR;
        }

        //第二步:发送消息
        $message = new Message();
        $message->fromUser = $fromUserId;
        $message->toUser = $toOriginatorId;
        $message->content = $remark;
        $message->module = $module;
        $message->action = $action;
        $message->recordId = $fromPersonId;
        $message->extraRecordId = $toFamilyId;
        $messageId = $this->addMessage($message);
        return $messageId;
    }

    /**
     * 接受合并分支
     * @param Message $message
     * @param 接受申请的家族要绑定的人物|int $toPersonId 接受申请的家族要绑定的人物
     * @param string $branchName
     * @param int $userId
     * @return int 正数表示接受成功，为更新的记录id，非正数代表失败,-1代表人物不存在,-2代表人物是同一个家族的,-3代表人物已存在合并的家族
     * @internal param 消息id $messageId
     */
    public function acceptMergeBranch(Message $message, int $toPersonId, string $branchName, int $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $fromPersonId = $message->recordId;     //发送申请方要绑定的人物id
        $familyDB = new CDBFamily();
        $updateRecord = $familyDB->mergeBranch($toPersonId, $fromPersonId, $branchName, $userId);

        // 非正数代表合并失败,-1代表人物不存在,-2代表人物是同一个家族的,-3代表人物已存在合并的家族
        if ($updateRecord <= 0) {
            return $updateRecord;
        }
        //@codeCoverageIgnoreStart
        //合并成功，更新消息的已读状态
        $status = $this->acceptInvite($message->id);
        return $updateRecord;
        //@codeCoverageIgnoreEnd
    }

    /**
     * V3
     * 接受申请加入家族并自动关注圈子
     * @param $messageId
     * @param $updateUserId
     * @param $socialCircleId
     * @return bool
     */
    public function acceptApplyJoinAndFollowCircleV3($messageId, $updateUserId, $socialCircleId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT message_code,from_user,module,action,recordId,ext FROM $this->TABLE WHERE id = '$messageId' AND isDelete = '0' ";
        $msgResult = $this->pdo->uniqueResult($sql);
        if ($msgResult == null) {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();

            $msgCode = $msgResult['message_code'];
            $fromUser = $msgResult['from_user'];
            $action = $msgResult['action'];
            $extData = json_decode($msgResult['ext'],true);
            $inviteUserId = $extData['inviteUserId'];

            $sql = "UPDATE $this->TABLE set isAccept = '1',update_by = '$updateUserId',update_time = now() 
                        WHERE isDelete = '0' AND message_code = '$msgCode' AND from_user = '$fromUser' AND action = '$action' AND isAccept = '0' ";

            if ($this->pdo->update($sql) > 0) {
                //向gener_family_user表中插入数据
                //@codeCoverageIgnoreStart
                $familyDb = new CDBFamily();
                $familyPermisstion = $familyDb->addPermission($msgResult['recordId'], $GLOBALS['userId'],0,0,
                    100, $extData['applyUserId'],$extData['name'],$extData['fatherName']);
                if ($familyPermisstion < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加失败');
                    $this->pdo->rollback();
                    exit;
                }
                // 关注圈子
                $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
                VALUES('$fromUser','$socialCircleId',now(),'$fromUser',now(),'$fromUser')";
                $followId = $this->pdo->insert($sql);
                if ($followId < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                    $this->pdo->rollback();
                    exit;
                }
                // 更新圈子关注数量
                $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow+1 WHERE id='$socialCircleId' AND is_delete=0";
                $updateRow = $this->pdo->update($sql);
                if ($updateRow < 0) {
                    Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
                    $this->pdo->rollback();
                    exit;
                }

                $personDB = new CDBPerson();
                $personDB->insertPersonBindUserRecord($msgResult['recordId'], 0, $inviteUserId, $fromUser, 3, $updateUserId);
                $this->pdo->commit();
                return true;
                //@codeCoverageIgnoreEnd
            } else {
                $this->pdo->rollback();
                return false;
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
        }
    }


    /**
     * 获取消息详情
     * @param $messageId 消息id
     * @return object 消息的详细内容
     */
    public function getMessageAndExt($messageId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,ext,tb.message_code as  messageCode,tb.from_user as fromUser,tu.nickname as fromUserName,tb.to_user as toUser,tb.content,tb.module,tb.action,tb.recordId,tb.extra_recordId as extraRecordId,tb.ext,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime,tb.isDelete,tb.isRead,tb.isAccept FROM $this->TABLE tb 
                    INNER JOIN $this->TABLE_USER tu ON tu.id = tb.from_user 
                    WHERE tb.id = '$messageId' AND tb.isDelete = '0' ";

        $result = $this->pdo->uniqueResult($sql);
        $result['ext'] = json_decode($result['ext'],true);
        if ($result != null) {
            return new Message($result);
        }
        return null;
    }
}
