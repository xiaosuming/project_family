<?php
/**
 * 积分模块
 * author: jiangpengfei
 * date: 2017-11-29
 */

namespace DB;

use DB\CDBManager;
use Model\Point;
use Util\Util;
use Model\PointRecord;

class CDBPoint
{
    public $pdo = null;
    public $TABLE = "gener_point";
    public $TABLE_RECORD = "gener_point_record";

    /**
     * @codeCoverageIgnore
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 初始化用户的积分
     * @param $userId 用户id
     * @return int 增加的记录id
     */
    public function initUserPoint($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE
            (userId,total_point,used_point,remain_point,create_time,create_by,update_time,update_by)
            VALUES('$userId',0,0,0,now(),'$userId',now(),'$userId')";

        return $this->pdo->insert($sql);
    }


    /**
     * 更新用户积分,这个接口应该只能用户自己调用
     * @param $userId   用户id
     * @param $recordId 记录id
     * @param $amount   积分数
     * @param $type     更新类型,1是增加,2是减少
     * @param $module   积分所属模块
     * @param #action   积分所属动作
     * @return int      更新的记录数
     */
    public function updatePointForUser(PointRecord $record, $transaction = true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            if ($transaction) {
                $this->pdo->beginTransaction();
            }
            //先增加一条积分更新记录
            $sql = "INSERT INTO $this->TABLE_RECORD(userId,amount,type,module,action,createTime,createBy,updateTime,updateBy)VALUES('$record->userId','$record->amount','$record->type','$record->module','$record->action',now(),'$record->userId',now(),'$record->userId')";
            $this->pdo->insert($sql);

            $totalPoint = 0;
            $usedPoint = 0;
            $remainPoint = 0;

            if ($record->type == 2) {
                $usedPoint = $record->amount;
                $remainPoint = -$record->amount;
            } else if ($record->type == 1) {
                $totalPoint = $record->amount;
                $remainPoint = $record->amount;
            } else {
                //@codeCoverageIgnoreStart
                return 0;
                //@codeCoverageIgnoreEnd
            }
            //再去更新用户积分统计
            $sql = "UPDATE $this->TABLE SET total_point = total_point + $totalPoint,used_point = used_point + $usedPoint,remain_point = remain_point + $remainPoint,update_time = now() WHERE userId = '$record->userId' ";

            $updateRow = $this->pdo->update($sql);

            if ($transaction) {
                $this->pdo->commit();
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            if ($transaction) {
                $this->pdo->rollback();
            }
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return $updateRow;
    }

    /**
     * 获取用户积分信息
     * @param $userId 用户id
     * @return map 用户积分信息
     */
    public function getUserPointInfo($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,total_point as totalPoint,used_point as usedPoint,remain_point as remainPoint,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy FROM $this->TABLE WHERE userId = '$userId' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return null;
        } else {
            return new Point($result);
        }

    }

    /**
     * 检查用户积分是否够
     * @param $userId 用户id
     * @param $point  要使用的积分
     * @return bool true够，false不够
     */
    public function checkUserPoint($userId, $point) {
        $this->init();
        $sql = "SELECT remain_point FROM $this->TABLE WHERE userId = '$userId' ";
        $result = $this->pdo->uniqueResult($sql);
    
        if ($result == null) {
            return false;
        } else {
            return $point <= $result['remain_point'];
        }
    }

    /**
     * 分页获取用户积分更新事件
     * @param $pageIndex 页码
     * @param $pageSize  页大小
     * @param $userId    用户id
     * @return array     事件数组
     */
    public function getUserPointUpdateEventsPaging($pageIndex, $pageSize, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT id,userId,amount,type,module,action, createTime, createBy,updateTime,updateBy FROM $this->TABLE_RECORD WHERE userId = '$userId' order by id desc limit $offset,$pageSize ";
        return $this->pdo->query($sql);
    }

    /**
     * 获取用户积分更新事件的总数
     * @param $userId 用户id
     * @return int 总数
     */
    public function getUserPointUpdateEventsCount($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*) FROM $this->TABLE_RECORD WHERE userId = '$userId' ";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            //@codeCoverageIgnoreStart
            return 0;
            //@codeCoverageIgnoreEnd
        } else {
            return $result['count(*)'];
        }
    }

}