<?php
/**
 * create by Jiayao Li
 *
 */
namespace DB;

use DB\CDBManager;
use Util\Util;
use Model\Celebrity;

class CDBFamilyCelebrities
{
    var $pdo = null;
    var $TABLE = "gener_family_celebrities";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 添加家族传说人物
     * @param familyId      家族ID
     * @param personId      人物ID
     * @param personName    姓名
     * @param styleName     字
     * @param pseudonym     号
     * @param yearOfBirth   出生年份
     * @param yearOfPass    去世年份
     * @param profile       简介
     * @param photo         照片
     * @param userId        用户ID
     *
     * @return int          传说人物ID
     */
    public function addCelebrity($familyId, $personId, $personName, $styleName, $pseudonym, $yearOfBirth, $yearOfPass, $profile, $photo, $userId) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO gener_family_celebrities
	    (familyId, personId, personName, 
        styleName, pseudonym, yearOfBirth, 
        yearOfPass, profile, photo,
        create_time, create_by, update_time, update_by) 
        VALUES
        ('$familyId', '$personId', '$personName', 
        '$styleName', '$pseudonym', '$yearOfBirth', 
        '$yearOfPass', '$profile', '$photo', 
        NOW(), '$userId', NOW(), '$userId')";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取家族传说人物
     * @param $familyId 家族ID
     *
     * @return Array
     */
    public function getCelebrities($familyId) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, familyId, personId, personName, 
                       styleName, pseudonym, yearOfBirth, yearOfPass, 
                       profile, photo, create_time AS createTime, create_by AS createBy, 
                       update_time AS updateTime, update_by AS updateBy
                FROM $this->TABLE WHERE familyId = '$familyId'";

        return $this->pdo->query($sql);
    }

    /**
     * 根据ID获取家族传说人物
     *
     * @param $celebrityId  传说人物ID
     *
     * @return Object   返回一个Celebrity对象或者NULL
     */
    public function getCelebrityById($celebrityId) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, familyId, personId, personName, 
                       styleName, pseudonym, yearOfBirth, yearOfPass, 
                       profile, photo, create_time AS createTime, create_by AS createBy, 
                       update_time AS updateTime, update_by AS updateBy
                FROM $this->TABLE WHERE id = '$celebrityId'";

        $res = $this->pdo->uniqueResult($sql);
        if(is_null($res)) {
            return $res;
        } else {
            return new Celebrity($res);
        }
    }

    /**
     * 检查用户是否记录的创建者
     *
     * @param $celebrityId  传说人物ID
     * @param $userId       用户ID
     *
     * @return boolean
     */
    public function isCreaterOfCelebrity($celebrityId, $userId){
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT create_by AS userId FROM $this->TABLE WHERE id = '$celebrityId'";

        $res = $this->pdo->uniqueResult($sql);
        if($res['userId'] == $userId){
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除家族传说人物
     *
     * @param $celebrityId  传说人物ID
     *
     * @return int 删除的行数
     */
    public function deleteCelebrity($celebrityId) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }
        //删除
        $sql = "UPDATE gener_family_celebrities SET isDelete=1 WHERE id = '$celebrityId' AND isDelete=0";

        return $this->pdo->update($sql);
    }

    /**
     * 修改家族传说人物资料
     *
     * @param  $celebrityId 传说人物ID
     * @param  $styleName   字
     * @param  $pseudonym   号
     * @param  $yearOfPass  去世年份
     * @param  $profile     简介
     * @param  $userId      修改人
     *
     * @return int 修改的行数
     */
    public function setCelebrity($celebrityId, $styleName, $pseudonym, $yearOfPass, $profile, $userId) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }
        //修改
        $sql = "UPDATE $this->TABLE SET styleName = '$styleName', pseudonym = '$pseudonym', yearOfPass = '$yearOfPass', 
                profile = '$profile', update_by = '$userId', update_time = NOW() WHERE id = '$celebrityId' AND isDelete=0";

        $rows = $this->pdo->update($sql);
        return $rows;
    }

    /**
     * 更新家族传说人物资料
     *
     * @param $celebrityId  传说人物ID
     * @param $personName   人物名称
     * @param $yearOfBirth  出生年份
     * @param $photo        照片
     * @param $userId       修改人
     *
     * @return int 修改的行数
     */
    public function updateCelebrity($celebrityId, $personName, $yearOfBirth, $photo, $userId){
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }
        //更新操作
        $sql = "UPDATE $this->TABLE SET personName = '$personName', yearOfBirth = '$yearOfBirth', 
                photo = '$photo', update_by = '$userId', update_time = NOW() WHERE id = '$celebrityId' AND isDelete=0";

        $rows = $this->pdo->update($sql);
        return $rows;
    }

    /**
     * 检查某个人物是否已经被标记为名人
     *
     * @param $personId 人物ID
     *
     * @return boolean
     */
    public function checkCelebrityExist($personId){
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->TABLE WHERE personId = '$personId' AND isDelete=0";

        $res = $this->pdo->query($sql);
        if(count($res) == 0) {
            return false;
        } else {
            return true;
        }
    }

	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getCelebrityIdsByCreator($userId)
	{
		if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            // @codeCoverageIgnoreEnd
		}

		$sql = "SELECT id FROM  $this->TABLE WHERE create_by = '$userId' AND isDelete=0";
		return $this->pdo->query($sql);
	}
}
