<?php
namespace DB;

use DB\RedisInstanceManager;

class HuanxinDB{
    private $redis;
    private $huanxinDB;

    public function __construct(){
        $this->huanxinDB = $GLOBALS['redis_huanxin'];
        $this->connect();
    }

    public function connect(){
        $redisManager = RedisInstanceManager::getInstance();
        $this->redis = $redisManager->getStorageInstance();
    }

    /**
     * 将任务推送到环信任务队列中
     * @param $task 任务
     * @return boolean true代表成功,false代表失败
     */
    private function push($task):bool{
        $result = $this->redis->rPush($this->huanxinDB,$task);
    
        return $result;
    }

    /**
     * 将环信注册任务添加到队列中,这个是注册用户
     * @param $username 要发送的电话号码
     * @param $password 要发送的验证码
     * @return boolean true代表成功,false代表失败
     */
    public function pushUserTask(string $username,string $password):bool{
        $user['u'] = $username; //用户名
        $user['p'] = $password; //密码
        $user['t'] = 1;         //人物类型
        $user['c'] = 0;         //任务执行的次数，超过3次不再执行

        $userStr = json_encode($user);

        return $this->push($userStr);
    }

    /**
     * 推送创建聊天室的任务
     * @param $name 聊天室名称
     * @param $description 聊天室描述
     * @param $maxuser  最大的用户数
     * @param $owner   创建人
     * @param $members 成员
     * @return boolean true代表成功，false代表失败
     */
    public function pushChatRoomTask(string $name,string $description,int $maxuser,string $owner,array $members):bool{
        
        $chatroom['n'] = $name; //聊天室名称
        $chatroom['d'] = $description;
        $chatroom['m'] = $maxuser;
        $chatroom['o'] = $owner;
        $chatroom['me'] = $members;
        $chatroom['t'] = 2;     //代表类型，1是注册用户，2是创建聊天室
        $chatroom['c'] = 0;     //任务执行的次数，超过3次则不再执行

        return $this->push(json_encode($chatroom));
    }

    /**
     * 将环信注册任务返回到队列
     * @param $messageContent 环信注册任务
     * @return boolean true代表成功，false代表失败
     */
    public function returnBack($task){
        if($task['c'] >= 3){
            return true;
        }else{
            $task['c']++;
            return $this->push(\json_encode($task));
        }
    }

    /**
     * 从队列中返回当前需要处理的环信注册任务
     * @return string 需要处理的环信注册任务
     */
    public function pop(){
        $result = $this->redis->lPop($this->huanxinDB);
    
        return $result;
    }

    

};