<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-28
 * Time: 下午5:54
 */

namespace DB;

use DB\CDBManager;
use DB\RedisConnect;
use Util\Util;
use Util\TaskQueue\Producer;
use DB\RedisInstanceManager;

class CDBPdfTask
{
    public $redis = null;
    public $pdo = null;

    public $TABLE = 'gener_pdf_task';

    /**
     * @codeCoverageIgnore
     */
    public function __construct()
    {
        $redisManager = RedisInstanceManager::getInstance();
        $this->redis = $redisManager->getStorageInstance();

    }

    /**
     * @codeCoverageIgnore
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }

        return true;
    }

    /**
     * 获取OCR的任务队列
     * @codeCoverageIgnore
     * @return string key值
     */
    private function getPdfKey()
    {
        return 'pdf_task';
    }

    /**
     * 向数据库中增加pdf任务记录
     * @param $userId
     * @param $familyId
     * @param $familyTreeName
     * @param $direction
     * @return mixed
     * @internal param $photo
     */
    public function addPdfTask($userId, $familyId, $familyTreeName, $direction)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO $this->TABLE (familyId, create_time,create_by,update_time,update_by)
                    VALUES
                    ('$familyId', now(),'$userId',now(),'$userId')";
        $recordId = $this->pdo->insert($sql);

        if ($recordId > 0) {
            // 添加到ocr任务队列
            $this->setPdfTask($recordId, $familyId, $familyTreeName, $direction);
        }

        return $recordId;
    }


    /**
     * 向数据库中增加pdf任务记录, 新版的调用函数
     * @param $userId
     * @param $familyId
     * @param $familyTreeName
     * @param $direction
     * @param $eventIds
     * @param $photoIdOnes
     * @param $photoIds
     * @param $graveIds
     * @param $startPersonId 分支的起始点，0代表所有\
     * @param $isHD  是否是高清
     * @param $options 额外的选项
     * @return mixed
     */
    public function addFamilyPdfTask($userId, $familyId,$familyTreeName, $direction, $eventIds,
     $photoIdOnes, $photoIds, $graveIds, $startPersonId, $isHD, $options)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO $this->TABLE (familyId, familyTreeName,direction,eventIds,photoIdOnes,photoIds,graveIds,startPersonId,isHD,options, create_time,create_by,update_time,update_by)
                    VALUES
                    ('$familyId','$familyTreeName','$direction','$eventIds','$photoIdOnes','$photoIds','$graveIds', '$startPersonId', '$isHD', '$options', now(),'$userId',now(),'$userId')";
        $recordId = $this->pdo->insert($sql);
        if ($recordId > 0) {
            // 添加到ocr任务队列
            $this->setFamilyPdfTask($recordId);
        }

        return $recordId;
    }


    /**
     * 增加任务
     * @codeCoverageIgnore
     * @param $taskId      任务id
     * @param $familyId
     * @param $familyTreeName
     * @param $direction
     * @param $eventIds
     * @param $albumIds
     * @param $imgUrls
     * @param $graveIds
     * @return mix 任务推送状态
     */
    private function setFamilyPdfTask($taskId)
    {
        $data['t'] = $taskId;   //任务id
        $str = \json_encode($data);

        $producer = new Producer();
        $producer->produce($GLOBALS['redis_family_pdf_task'], $str);

        return true;
    }

    /**
     * 增加任务, 弃用
     * @codeCoverageIgnore
     * @param $taskId      任务id
     * @param $familyId
     * @param $familyTreeName
     * @param $direction
     * @return mix 任务推送状态
     * @internal param 要识别的照片地址 $photo
     */
    private function setPdfTask($taskId, $familyId, $familyTreeName, $direction)
    {
        $data['t'] = $taskId;   //任务id
        $data['f'] = $familyId; //家族id
        $data['n'] = $familyTreeName;   // 族谱名
        $data['d'] = $direction;        // 方向
        $str = \json_encode($data);
        $result = $this->redis->rPush($GLOBALS['redis_pdf_task'], $str);

        return $result;
    }

    /**
     * 获取pdf任务,弃用
     * @codeCoverageIgnore
     */
    public function getPdfTask()
    {
        $result = $this->redis->lPop($GLOBALS['redis_pdf_task']);

        return $result;
    }

    /**
     * 更新pdf的结果
     *
     * @param $pdfId
     * @param $previewPdf
     * @param $printPdf
     * @return mixed
     */
    public function updatePdf($pdfId, $previewPdf, $printPdf)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET preview_pdf = '$previewPdf',print_pdf = '$printPdf' , finish='1' WHERE id = '$pdfId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 获取pdf的结果
     * @param $taskId
     * @return mixed
     */
    public function getPdf($taskId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, familyId, preview_pdf,print_pdf, finish,if(finish_percent='0','0%',finish_percent) as finish_percent, finish_status,familyTreeName,options,family_name as familyName,
        create_by, create_time,update_by,update_time FROM $this->TABLE WHERE id = '$taskId' ";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取任务详情
     * @param $taskId
     * @return mixed
     */
    public function getFamilyPdfTaskDetail($taskId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,familyId,preview_pdf,print_pdf,familyTreeName,direction,eventIds,photoIdOnes,photoIds,graveIds,isHD,finish,startPersonId,create_time,create_by,update_time,update_by FROM $this->TABLE WHERE id='$taskId'";
        return $this->pdo->uniqueResult($sql);
    }


    /**
     * 删除导出任务记录
     * @param $taskId
     * @return mixed
     */
    public function deletePdfTask($taskId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET is_delete = 1  WHERE id = $taskId  and is_delete = 0";
        return $this->pdo->update($sql);
    }

}
