<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-12-29
 * Time: 下午4:34
 */

namespace DB;

use DB\CDBManager;
use Model\GreetingCard;
use Model\GreetingCardPage;
use Util\Util;

class CDBGreetingCard
{
    public $pdo = null;
    public $TABLE = "family_greeting_card";
    public $PAGE_TABLE = 'family_greeting_card_page';

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }


    public function addGreetingCard(GreetingCard $greetingCardModel)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE (userId,infoCardId,title,template,data,isDelete,createTime,createBy,updateTime,updateBy)
        VALUES (
        '$greetingCardModel->userId',
        '$greetingCardModel->infoCardId',
        '$greetingCardModel->title',
        '$greetingCardModel->template',
        '$greetingCardModel->data',0,now(),
        '$greetingCardModel->userId',now(),
        '$greetingCardModel->userId')";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取用户创建的贺卡
     */
    public function getUserGreetingCard(int $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,infoCardId,title,template,data,createTime,createBy,updateTime,updateBy 
                FROM $this->TABLE 
                WHERE userId = '$userId' AND isDelete = '0' order by id desc";

        return $this->pdo->query($sql);
    }

    /**
     * 获取贺卡的详情
     * @param int $greetingCardId 贺卡id
     * @return
     */
    public function getGreetingCard(int $greetingCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,infoCardId,title,template,data,createTime,createBy,updateTime,updateBy 
                FROM $this->TABLE t
                WHERE id = '$greetingCardId' and isDelete = '0' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return null;
        } else {
            return new GreetingCard($result);
        }
    }

    /**
     * 添加贺卡页
     */
    public function addGreetingCardPage(GreetingCardPage $page)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO 
                (id, page, cardId, content, title, photo, isDelete, createTime, createBy, updateTime, updateBy)
                VALUES 
                ($page->id, $page->page, $page->cardId, $page->content, $page->title, $page->photo,
                $page->isDelete, $page->createTime, $page->createBy, $page->updateTime, $page->updateBy)";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取贺卡的所有页
     */
    public function getGreetingCardPages(int $cardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,page,cardId,content,title,photo,createTime,createBy,updateTime,updateBy 
                FROM $this->PAGE_TABLE WHERE isDelete = '0' AND cardId = '$cardId' ";

        return $this->pdo->query($sql);
    }

    /**
     * 检查用户是否是贺卡的创建者
     * @param $userId
     * @param $greetingCardId
     * @return bool
     */
    public function checkUserHasPermissionForGreetingCard($userId, $greetingCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->TABLE WHERE id='$greetingCardId' AND userId = '$userId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return true;
        } else {
            return false;
        }
    }

    public function updateGreetingCardById(GreetingCard $greetingCardModel, $greetingCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET userId = '$greetingCardModel->userId',
                infoCardId='$greetingCardModel->infoCardId',
                title='$greetingCardModel->title',
                template='$greetingCardModel->template',
                data='$greetingCardModel->data',
                updateTime = now(),
                updateBy='$greetingCardModel->updateBy' WHERE id='$greetingCardId' AND isDelete=0";
        return $this->pdo->update($sql);

    }

    public function deleteGreetingCard($greetingCardId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE SET isDelete=1 WHERE id='$greetingCardId' AND isDelete=0";
        return $this->pdo->update($sql);
    }
}
