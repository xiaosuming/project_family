<?php
    /**
     * 资讯模块，使用zset实现的
     * @author jiangpengfei
     * @date 2018-11-26
     */
    
    namespace DB;

    use DB\CDBManager;
    use Util\TaskQueue\Producer;
    use DB\RedisInstanceManager;

    class CDBPushInformation
    {
        public $redis = null;

        public function __construct()
        {
            $redisManager = RedisInstanceManager::getInstance();
            $this->redis = $redisManager->getStorageInstance();
        }

        /**
         * 获取用户订阅资讯的timeline的key
         * @param $userId 用户id
         * @return string key值
         */
        private function getInformationTimelineKey($userId){
            return 'infot'.$userId;
        }

        /**
         * 设置推送资讯的任务
         * @param $pushUserIds 要推送的userId
         * @param $infoId      推文内容
         * @param $time    推文时间戳，int型
         * @param $userId      发资讯的用户id
         * @return mix 任务推送状态
         */
        public function setPushInfoTask($pushUserIds,$infoId,$time,$userId){
            //为了用户第一时间看到自己发的，这里特殊处理用户自己发的推文
            $this->redis->zAdd($this->getInformationTimelineKey($userId),$time,$infoId);  //time()是时间戳，用来排序

            $userIds = $pushUserIds;

            $data['u'] = $userIds;  //用户id数组
            $data['i'] = $infoId;   //推文id
            $data['t'] = $time; //推送时间
            $str = \json_encode($data);

            $producer = new Producer();
            $producer->produce($GLOBALS['redis_info'],$str);
        }

        /**
         * 获取一页时间轴上的推文id
         * @param $userId 用户id
         * @param $pageIndex 页码
         * @param $pageSize  页大小
         * @return array 一页postId
         */
        public function getTimeline($userId,$pageIndex,$pageSize){
            $end = (1 - $pageIndex) * $pageSize - 1;
			$start = $end - $pageSize + 1;

            $result = $this->redis->zRange($this->getInformationTimelineKey($userId),$start,$end);

            return $result;
        }

        /**
         * 获取用户时间轴上资讯的总数
         * @param $userId 用户id
         * @return int 总数
         */
        public function getTimelineInfoCount($userId){
            
            $result = $this->redis->zSize($this->getInformationTimelineKey($userId));

            if($result === FALSE){
                return 0;
            }

            return $result;
        }

        /**
         * 设置删除推文的任务
         * @param $pushUserIds 推送的用户id
         * @param $infoId      推文id
         */
        public function setDeleteInfoTask($pushUserIds,$infoId,$userId){
            //为了用户第一时间看到自己删的，这里特殊处理用户自己发的推文
            $this->redis->zRem($this->getInformationTimelineKey($userId),$infoId);

            $userIds = $pushUserIds;
 
            $data['u'] = $userIds;  //用户id数组
            $data['i'] = $infoId;   //推文id
            $str = \json_encode($data);
            
            $producer = new Producer();
            $producer->produce($GLOBALS['redis_info_delete'], $str);

            return true;
        }

		/**
		 * @param userId
		 * @param startTimestamp
		 * @param endTimestamp
		 * return int 返回在此区间的资讯的数量
		 */
		public function getInfoCountByTimestamp($userId,$startTimestamp,$endTimestamp){
			$result = $this->redis->zCount($this->getUserTimelineKey($userId),$startTimestamp,$endTimestamp);
			return $result;
		}
    };