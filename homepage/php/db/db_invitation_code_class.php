<?php
/**
 * 邀请码模块
 * author: jiangpengfei
 * date: 2017-09-28
 */

namespace DB;

use DB\CDBManager;
use Util\Util;

class CDBInvitationCode
{
    public $pdo = null;
    public $TABLE = "gener_invitation_code";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 后台用来生成新的邀请码
     * @return mix 生成成功则返回string,否则false
     */
    public function getNewInvitationCode()
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $invitationCode = strtolower(Util::generateRandomCode(6));      //生成6位随机的邀请码,转换成小写存储

        $sql = "INSERT INTO $this->TABLE
                    (invitation_code,create_time,update_time)
                    VALUES
                    ('$invitationCode',now(),now())";

        if ($this->pdo->insert($sql)) {
            return $invitationCode;
        } else {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }
    }

    /**
     * 检查邀请码是否合法
     * @param $invitationCode 邀请码
     * @return boolean true代表邀请码可用，false代表不可用
     */
    public function checkInvitationCode($invitationCode)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId FROM $this->TABLE WHERE invitation_code = '$invitationCode'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            if ($result['userId'] != null && $result['userId'] > 0) {
                return false;
            } else {
                //@codeCoverageIgnoreStart
                return true;
                //@codeCoverageIgnoreEnd
            }
        }
    }

    /**
     * 绑定用户id到邀请码中
     * @param $userId 用户id
     * @param $invitationCode  邀请码
     * @return int 更新的记录数
     */
    public function bindUserIdToInvitationCode($userId, $invitationCode)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set userId = '$userId',update_time = now() WHERE invitation_code = '$invitationCode' ";
        return $this->pdo->update($sql);
    }

    /**
     * 获取邀请码的总数
     * @param isUsed 是否已使用
     * @return int 总数
     */
    public function getInvitationCodeTotal(int $isUsed)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE ";
        $where = '';

        if ($isUsed === 0) {
            //@codeCoverageIgnoreStart
            $where = "WHERE userId = 0";
            //@codeCoverageIgnoreEnd
        } else if ($isUsed === 1) {
            $where = "WHERE userId != 0";
        }

        $sql = $sql . $where;

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取邀请码分页
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @param $isUsed 邀请码是否已用，-1代表不作为查找选项，0代表未使用，1代表已使用
     * @return array 邀请码的数组
     */
    public function getInvitationCodePaging(int $pageIndex, int $pageSize, int $isUsed): array
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT id,invitation_code,userId,create_time,update_time FROM $this->TABLE ";
        $where = '';

        if ($isUsed === 0) {
            //@codeCoverageIgnoreStart
            $where = "WHERE userId = 0";
            //@codeCoverageIgnoreEnd
        } else if ($isUsed === 1) {
            $where = "WHERE userId != 0";
        }

        $sql = $sql . $where . " order by id desc limit $offset,$pageSize";

        return $this->pdo->query($sql);
    }

    /**
     * 检查openid是否已经绑定邀请码
     *
     * @param $openid
     *
     * @return mix 查询成功则返回string, 否则false
     */
    public function checkOpenidHasCode($openid)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT invitation_code as invitationCode FROM $this->TABLE WHERE openid = '$openid'";

        $res = $this->pdo->uniqueResult($sql);

        if ($res != null) {
            return $res['invitationCode'];
        } else {
            return false;
        }
    }

    /**
     * 绑定openid和邀请码
     *
     * @param $openid
     * @return int      影响的行数
     */
    public function bindOpenidWithCode($openid)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $code = $this->getNewInvitationCode();
            $sql = "UPDATE $this->TABLE SET openid='$openid' WHERE invitation_code='$code'";
            $rows = $this->pdo->update($sql);
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $code;
    }
}