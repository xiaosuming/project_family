<?php
namespace DB;
use DB\RedisConnect;
use Util\TempStorage;
use Util\TaskQueue\Producer;

class PhoneDB{
    private $messageDB;

    public function __construct(){
        $this->messageDB = $GLOBALS['redis_message'];
    }

    /**
     * 将短信添加到队列中，准备发送
     * @param $phone 要发送的电话号码
     * @param $vcode 要发送的验证码
     * @param $order 短信验证序号
     * @return boolean true代表成功,false代表失败
     */
    public function pushMessage($phone,$vcode,$order){
        $tempStorage =new TempStorage();
        $tempValue=$tempStorage->get($phone);
        if(!$tempValue){
            $tempStorage->setTemp($phone,1,86400);
        }
        else{
            if($tempValue >= 11){
                return false;
            }
            else{
                $tempStorage->increment($phone);
            }
        }

        $producer = new Producer();
        $producer->produce($this->messageDB,$phone.",".$vcode."|".$order);

        return true;  
    }

};