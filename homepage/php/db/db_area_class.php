<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/1 0001
 * Time: 11:50
 */

namespace DB;

use Util\Util;
use DB\CDBManager;

class CDBArea
{

    var $pdo = null;

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    public function getFirstArea()
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT areaid as areaId,areaname as areaName FROM gener_area WHERE parentId='4744'";
        $result = $this->pdo->query($sql);
        return $result;
    }

    public function getOtherArea($areaId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT areaid as areaId,areaname as areaName FROM gener_area WHERE parentId='$areaId'";
        $result = $this->pdo->query($sql);
        return $result;
    }

    public function getAreaNameById($areaId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT areaname as areaName from gener_area WHERE areaid='$areaId'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result;
        }
        return false;
    }

}
