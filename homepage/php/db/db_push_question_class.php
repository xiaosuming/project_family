<?php

    namespace DB;

    use DB\CDBManager;
    use DB\RedisInstanceManager;
    use Util\TaskQueue\Producer;

    class CDBPushQuestion
    {
        public $redis = null;

        public function __construct()
        {
            $redisManager = RedisInstanceManager::getInstance();
            $this->redis = $redisManager->getStorageInstance();
        }

        /**
         * 获取用户的timeline的key
         * @param $userId 用户id
         * @return string key值
         */
        private function getUserTimelineKey($userId){
            return 'qt'.$userId;
        }

        /**
         * 设置推送任务
         * @param $pushUserIds 要推送的userId
         * @param $questionId      推文内容
         * @param $postTime    推文时间戳，int型
         * @param $userId      用户id
         * @param $type        类型，1为问题，2为悬赏
         * @return mix 任务推送状态
         */
        public function setPushQuestionsTask($pushUserIds, $questionId, $postTime, $userId, $type){
            //为了用户第一时间看到自己发的，这里特殊处理用户自己发的推文
            $this->redis->zAdd($this->getUserTimelineKey($userId), $postTime, $type.$questionId);  //time()是时间戳，用来排序

            $userIds = array();
            foreach($pushUserIds as $value){
                array_push($userIds, $value['userId']);
            }
            $data['u'] = $userIds;  //用户id数组
            $data['q'] = $questionId;   //推文id
            $data['t'] = $postTime; //推送时间
            $data['ty'] = $type; //类型
            $str = \json_encode($data);

            $producer = new Producer();
            $producer->produce($GLOBALS['redis_question'],$str);

            return true;
        }

        /**
         * 获取一页时间轴上的推文id
         * @param $userId 用户id
         * @param $pageIndex 页码
         * @param $pageSize  页大小
         * @return array 一页postId
         */
        public function getTimeline($userId, $pageIndex, $pageSize){
            $end = (1 - $pageIndex) * $pageSize - 1;
            $start = $end - $pageSize + 1;

            $result = $this->redis->zRange($this->getUserTimelineKey($userId), $start, $end);
            return $result;
        }

        /**
         * 获取用户时间轴上推文的总数
         * @param $userId 用户id
         * @return int 总数
         */
        public function getTimelinePostCount($userId){
            
            $result = $this->redis->zSize($this->getUserTimelineKey($userId));

            if($result === FALSE){
                return 0;
            }

            return $result;
        }

        /**
         * 设置删除问题的任务
         * @param $pushUserIds 推送的用户id
         * @param $questionId      推文id
         * @param $userId      用户id
         * @param $type        类型，1为问题，2为悬赏
         */
        public function setDeletePostTask($pushUserIds, $questionId, $userId, $type){
            //为了用户第一时间看到自己删的，这里特殊处理用户自己发的推文
            $this->redis->zRem($this->getUserTimelineKey($userId), $type.$questionId);

            $userIds = array();
            foreach($pushUserIds as $value){
                array_push($userIds,$value['userId']);
            }
            $data['u'] = $userIds;  //用户id数组
            $data['q'] = $questionId;   //问题id
            $data['ty'] = $type;    //类型
            $str = \json_encode($data);

            $producer = new Producer();
            $producer->produce($GLOBALS['redis_question_delete'],$str);

            return true;
        }

    };