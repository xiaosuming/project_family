<?php
/**
 * 人物的大事件模块
 * @author jiangpengfei
 * @date 2016-12-10
 */

namespace DB;

use DB\CDBManager;
use DB\CDBFamily;
use Model\Event;
use Model\EventHistory;
use Util\Util;
use DB\CDBAccount;

class CDBEvent
{
    var $pdo = null;
    var $TABLE = "gener_significantEvent";
    var $TABLE_HISTORY = "gener_eventEditHistory";
    var $TABLE_USER = "gener_user";
    var $TABLE_PERSON = "gener_person";
    var $TABLE_EVENT = "gener_significantEvent";
    var $TABLE_EVENT_COMMENT = "gener_eventComment";
    private $TABLE_PERMISSION = "gener_permission";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }


    /**
     * 检查用户id和事件id的权限是否匹配,这里检查的是查看的权限
     * @param $userId 用户Id
     * @param $eventId 事件id
     * @return bool true是有权限，false为没有权限
     */
    public function verifyUserIdAndEventId($userId, $eventId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

//        $sql = "SELECT count(id) FROM $this->TABLE WHERE id = '$eventId' AND (userId = '$userId' OR familyId in (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId') )";
        $sql = "SELECT count(id) FROM $this->TABLE WHERE id = '$eventId' AND (userId = '$userId' OR familyId in (SELECT familyId FROM $this->TABLE_PERMISSION WHERE userId = '$userId' and is_delete = 0) )";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }

        if ($result['count(id)'] > 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 添加大事件
     * @param $personId    人物id
     * @param $userId        用户id
     * @param $familyId        家族id
     * @param $iconograph   事件的配图
     * @param $type         事件的类型,1是大事件,2是秘密事件
     * @param $openTime     如果是秘密事件，则有公开时间的设定
     * @param $expTime    事件的时间
     * @param $expName      事件姓名
     * @param $expContent    事件内容
     * @param $writeMode    撰写模式    0.单独撰写 1.联合撰写
     * @param $writer        作者的id
     * @return int 创建的事件Id或者-1
     */
    public function addEvent(Event $event)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        //$personId,$userId,$familyId,$iconograph,$type,$openTime,$expTime,$expName,$expContent,$write_mode,$writer

        //这里根据内容截取摘要
        if (!$event->isMd) {
            $text = strip_tags($event->expContent);
        } else {
            $text = $event->expContent;
        }
        $event->abstract = \mb_substr($text, 0, 144);
        $event->expContent = htmlspecialchars($event->expContent);

        $sql = "INSERT INTO $this->TABLE(sourceId,personId,userId,familyId,resources,is_md,iconograph,type,open_time,exp_time,exp_name,exp_content,abstract,write_mode,create_time,create_by,update_time,update_by)
                    VALUES('$event->sourceId','$event->personId','$event->userId','$event->familyId','$event->resources','$event->isMd','$event->iconograph','$event->type','$event->openTime','$event->expTime','$event->expName','$event->expContent','$event->abstract','$event->writeMode',now(),'$event->createBy',now(),'$event->updateBy')";

        try {
            $this->pdo->beginTransaction();

            $id = $this->pdo->insert($sql);

            //记录用户日志
            $function_name = __CLASS__.'->'.__FUNCTION__;
            $option_detail = 'APP添加大事件 ';
            $other_msg = [
                'event' => $event
            ];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $optionTableName = $this->TABLE;
            $tableId = $id;
            $accountDB = new CDBAccount();
            $accountDB->recordUserOptionLog($function_name, $option_detail, $event->createBy, $other_msg, $optionTableName, $tableId);

            //添加事件完成后需要给事件做一个历史记录
            $this->addEventEditHistory($id, $event->expTime, $event->expName, $event->expContent, $GLOBALS['EVENT_UPDATE_TYPE_INIT'], $event->createBy);

            $this->pdo->commit();
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            $this->pdo->rollback();
            $id = -1;
            // @codeCoverageIgnoreEnd
        }


        return $id;
    }

    /**
     * 增加编辑历史
     * @param $eventId            事件id
     * @param $expTime            事件时间
     * @param $expName            事件姓名
     * @param $expContent        事件内容
     * @param $eventEditType    编辑操作类型
     * @param $writer            这个版本的最后的撰写者
     * @param $createBy            创建人
     * @param $createTime        创建时间
     * @param $updateBy            更新人
     * @param $updateTime        更新时间
     * @return int 添加的历史的id
     */
    public function addEventEditHistory($eventId, $expTime, $expName, $expContent, $eventEditType, $writer)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_HISTORY(eventId,exp_time,exp_name,exp_content,edit_type,writer,create_time,create_by,update_time,update_by)
                    VALUES('$eventId','$expTime','$expName','$expContent','$eventEditType','$writer',now(),'$writer',now(),'$writer')";
        $id = $this->pdo->insert($sql);
        return $id;
    }

    /**
     * 检查编辑大事件的权限
     * @param $userId 用户Id
     * @param $eventId 大事件的Id
     * @return boolean true代表有权限,false代表无权限
     */
    public function checkUpdateEventPermission($userId, $eventId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        //先检查该userId是不是这个事件的创建者
        $sql = "SELECT familyId,write_mode,create_by FROM $this->TABLE WHERE id = '$eventId'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        }

        if ($result['create_by'] == $userId) {
            return true;
        }

        //再检查当前文章是不是多人编辑模式,0是单人，1是多人
        if ($result['write_mode'] == 0) {
            return false;
        }

        //最后检查在大事件是多人编辑模式下，该用户是否有权限编辑
        $familyDB = new CDBFamily();
        return $familyDB->isUserForFamily($result['familyId'], $userId);

    }

    /**
     * 编辑大事件
     * @param $id            事件id
     * @param $resources
     * @param $isMd
     * @param $expTime        事件的经历时间
     * @param $expName        事件姓名
     * @param $expContent    事件内容
     * @param $write_mode    撰写模式
     * @param $updateTime    更新时间
     * @param $updateBy        更新人
     * @return int 受影响的记录
     */
    public function updateEvent($id, $expTime, $expName, $expContent, $write_mode, $updateTime, $updateBy, $resources = "", $isMd = 0, $iconograph = '')
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $addNum = 0;

        //检查当前更新的人是否曾经编辑过
        $sql = "SELECT id FROM $this->TABLE_HISTORY WHERE eventId = '$id' AND writer = '$updateBy' LIMIT 1";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            //没编辑过
            $addNum = 1;
        }

        $updateIcon = '';
        if ($iconograph != '') {
            $updateIcon = "iconograph = $iconograph,";
        }

        //这里根据内容截取摘要
        if (!$isMd) {
            $text = strip_tags($expContent);
        } else {
            $text = $expContent;
        }
        $abstract = mb_substr($text, 0, 144);
        $expContent = htmlspecialchars($expContent);

        $sql = "UPDATE $this->TABLE set resources='$resources',is_md='$isMd',exp_time ='$expTime',exp_name = '$expName',exp_content = '$expContent',abstract = '$abstract', $updateIcon
            write_mode = '$write_mode',participate_num = participate_num + $addNum,version = version + 1,update_time = '$updateTime',
            update_by = '$updateBy' WHERE id = '$id' AND is_delete = '0' ";
        try {
            $this->pdo->beginTransaction();

            $rows = $this->pdo->update($sql);

            //记录用户日志
            $function_name = __CLASS__.'->'.__FUNCTION__;
            $option_detail = 'APP添加大事件 ';
            $other_msg = [
                'id' => $id,
                'expTime' => $expTime,
                'expName' => $expName,
                'expContent' => $expContent,
                'write_mode' => $write_mode,
                'updateTime' => $updateTime,
                'updateBy' => $updateBy,
                'resources' => $resources,
                'isMd' => $isMd,
                'iconograph' => $iconograph,
                'addNum' => $addNum
            ];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $optionTableName = $this->TABLE;
            $tableId = $id;
            $accountDB = new CDBAccount();
            $accountDB->recordUserOptionLog($function_name, $option_detail, $updateBy, $other_msg, $optionTableName, $tableId);

            //添加事件完成后需要给事件做一个历史记录,
            $this->addEventEditHistory($id, $expTime, $expName, $expContent, $GLOBALS['EVENT_UPDATE_TYPE_ADD'], $updateBy, $updateBy, $updateTime, $updateBy, $updateTime);

            $this->pdo->commit();
            // @codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            $rows = -1;
            // @codeCoverageIgnoreEnd
        }

        return $rows;
    }


    /**
     * 删除大事件
     * @param $id  事件id
     * @return int 删除的记录数
     */
    public function deleteEvent($id)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_EVENT SET is_delete = '1' WHERE id = $id";

        return $this->pdo->update($sql);
    }

    /**
     * 查询某一个大事件
     * @param $id 事件id
     * @return object 查询的事件
     */
    public function getEvent($id)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT e.id,e.sourceId,e.personId,tp.name as personName,e.userId,e.familyId,e.exp_time as expTime,e.iconograph,e.resources,e.is_md,
        e.type,e.exp_name as expName,e.exp_content as expContent,e.abstract,e.write_mode as writeMode,e.participate_num as participateNum,e.version as version,e.open_time as openTime,
        e.create_time as createTime,e.create_by as createBy,e.update_time as updateTime,e.update_by as updateBy 
        FROM $this->TABLE as e LEFT JOIN $this->TABLE_PERSON as tp ON e.personId=tp.id WHERE e.id = '$id'  AND e.is_delete = '0'";

        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            $event = new Event($result);
            return $event;
        }

        return null;
    }

    /**
     * 获取一个大事件所有的参与撰写人
     * @param $eventId 大事件id
     * @return array 撰写人的id和名字
     */
    public function getEventWriters($eventId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT DISTINCT th.writer,tu.nickname FROM $this->TABLE_HISTORY th 
                    INNER JOIN $this->TABLE_USER tu on th.writer = tu.id  
                    WHERE th.eventId = '$eventId'";

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 根据指定条件查询所有的秘密事件的总数
     * @param $personId    根据某个人查询,-1代表全部
     * @param $userId    根据某个用户查询,-1代表全部
     * @param $familyId    根据家族id查询,-1代表全部
     * @param $startTime    事件的开始时间
     * @param $endTime        事件的结束时间
     * @param $currentTime  当前时间
     * @return int 返回符合条件的事件的总数
     */
    public function getSecretEventsTotal($personId, $userId, $familyId, $startTime, $endTime, $currentTime)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE ";
        $where = " WHERE type = 2  AND is_delete = '0' ";

        if ($familyId != -1) {
            $where .= " AND familyId = '$familyId'";
        }

        if ($personId != -1) {
            $where .= " AND personId = '$personId'";
        }

        if ($userId != -1) {
            $where .= " AND userId = '$userId'";
        }

        if ($startTime != -1) {
            $where .= " AND exp_time >= '$startTime'";
        }

        if ($endTime != -1) {
            $where .= " AND exp_time <= '$endTime'";
        }

        if ($currentTime != -1) {
            $where .= " AND open_time <= '$currentTime'";
        }

        $sql .= $where;

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            //@codeCoverageIgnoreStart
            return 0;
            //@codeCoverageIgnoreEnd
        }
        $total = $result['count(id)'];
        return $total;
    }


    /**
     * 根据指定条件查询所有的秘密事件
     * @param $personId    根据某个人查询,-1代表全部
     * @param $userId    根据某个用户查询,-1代表全部
     * @param $familyId    根据家族id查询,-1代表全部
     * @param $startTime    事件的开始时间
     * @param $endTime        事件的结束时间
     * @param $currentTime  当前时间
     * @param $pageIndex    页码
     * @param $pageSize     每页数量
     * @return array 返回符合条件的事件
     */
    public function getSecretEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $currentTime, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT e.id,e.sourceId,e.personId,tp.name as personName,e.userId,e.familyId,e.exp_time as expTime,e.iconograph,e.resources,e.is_md,
        e.type,e.exp_name as expName,e.exp_content as expContent,e.abstract,e.write_mode as writeMode,e.participate_num as participateNum,e.version as version,
        e.create_time as createTime,e.create_by as createBy,e.update_time as updateTime,e.update_by as updateBy 
        FROM $this->TABLE as e LEFT JOIN $this->TABLE_PERSON as tp ON e.personId=tp.id";
        $where = " WHERE e.type = 2  AND e.is_delete = '0' ";

        if ($familyId != -1) {
            $where .= " AND e.familyId = '$familyId'";
        }

        if ($personId != -1) {
            $where .= " AND e.personId = '$personId'";
        }

        if ($userId != -1) {
            $where .= " AND e.userId = '$userId'";
        }

        if ($startTime != -1) {
            $where .= " AND e.exp_time >= '$startTime'";
        }

        if ($endTime != -1) {
            $where .= " AND e.exp_time <= '$endTime'";
        }

        if ($currentTime != -1) {
            $where .= " AND e.open_time <= '$currentTime'";
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $where .= " order by e.exp_time DESC LIMIT $offset,$pageSize";

        $sql .= $where;

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 根据指定条件查询所有的大事件的总数,不会包含秘密事件
     * @param $personId    根据某个人查询,-1代表全部
     * @param $userId    根据某个用户查询,-1代表全部
     * @param $familyId    根据家族id查询,-1代表全部
     * @param $startTime    事件的开始时间,-1代表全部
     * @param $endTime        事件的结束时间,-1代表全部
     * @return int 返回符合条件的事件的总数
     */
    public function getEventsTotal($personId, $userId, $familyId, $startTime, $endTime)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*) FROM $this->TABLE ";
        $where = " WHERE type = 1  AND is_delete = '0'  AND sourceId = 0 ";

        if ($familyId != -1) {
            $where .= " AND familyId = '$familyId'";
        }

        if ($personId != -1) {
            $where .= " AND personId = '$personId'";
        }

        if ($userId != -1) {
            $where .= " AND userId = '$userId'";
        }

        if ($startTime != -1) {
            $where .= " AND exp_time >= '$startTime'";
        }

        if ($endTime != -1) {
            $where .= " AND exp_time <= '$endTime'";
        }

        $sql .= $where;

        $result = $this->pdo->query($sql);
        $total = $result[0]['count(*)'];
        return $total;
    }


    /**
     * 根据指定条件查询所有的大事件
     * @param $personId    根据某个人查询,-1代表全部
     * @param $userId    根据某个用户查询,-1代表全部
     * @param $familyId    根据家族id查询,-1代表全部
     * @param $startTime    事件的开始时间
     * @param $endTime        事件的结束时间
     * @param $pageIndex    页码
     * @param $pageSize     每页数量
     * @param $isUserSelf   是不是用户自己
     * @return array 返回符合条件的事件
     */
    public function getEventsPaging($personId, $userId, $familyId, $startTime, $endTime, $pageIndex, $pageSize, $isUserSelf)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT e.id,e.sourceId,e.personId,tp.name as personName,e.userId,e.familyId,e.exp_time as expTime,e.iconograph,e.resources,e.is_md,
        e.type,e.exp_name as expName,e.exp_content as expContent,e.abstract,e.write_mode as writeMode,e.participate_num as participateNum,e.version as version,
        e.create_time as createTime,e.create_by as createBy,e.update_time as updateTime,e.update_by as updateBy 
        FROM $this->TABLE as e LEFT JOIN $this->TABLE_PERSON as tp ON e.personId=tp.id";
        $where = " WHERE e.is_delete = '0'  AND e.sourceId = 0 ";

        if (!$isUserSelf) {
            $where .= " AND e.type = 1 ";
        }

        if ($familyId != -1) {
            $where .= " AND e.familyId = '$familyId'";
        }

        if ($personId != -1) {
            $where .= " AND e.personId = '$personId'";
        }

        if ($userId != -1) {
            $where .= " AND e.userId = '$userId'";
        }

        if ($startTime != -1) {
            $where .= " AND e.exp_time >= '$startTime'";
        }

        if ($endTime != -1) {
            $where .= " AND e.exp_time <= '$endTime'";
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $where .= " order by e.exp_time DESC LIMIT $offset,$pageSize";

        $sql .= $where;

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取某一个事件的所有编辑历史
     * @param eventId 事件id
     * @return array 事件的编辑历史
     */
    public function getEventEditHistory($eventId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,exp_time as expTime,exp_name as expName,exp_content as expContent,edit_type as editType,
            writer,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime 
            FROM $this->TABLE_HISTORY WHERE eventId = '$eventId' ORDER BY create_time DESC";
        $result = $this->pdo->query($sql);        //返回事件的编辑历史
        return $result;
    }

    /**
     * 获取事件的历史版本
     * @param historyId 历史id
     * @return object 事件的历史版本
     */
    public function getEventHistory($historyId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,eventId,exp_time as expTime,exp_name as expName,exp_content as expContent,edit_type as editType,
            writer,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime 
            FROM $this->TABLE_HISTORY WHERE id = '$historyId' ";
        $result = $this->pdo->uniqueResult($sql);        //返回事件的编辑历史
        if ($result != null) {
            return new EventHistory($result);
        }
        return null;
    }

    /**
     * 获取用户编辑的事件总数
     * @param $personId        人物id
     * @param $searchUserId        搜索的用户id
     * @param $familyId        家族id
     * @param $myUserId        用户id
     * @return int                总数
     */
    public function getMyEditEventsCount($personId, $searchUserId, $familyId, $myUserId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(DISTINCT(th.eventId)) FROM $this->TABLE_HISTORY th 
                    inner join $this->TABLE tb on th.eventId = tb.id 
                    WHERE th.writer = '$myUserId'  AND tb.is_delete = '0' ";
        $where = "";
        if ($personId != -1) {
            $where .= " AND tb.personId = '$personId' ";
        }
        if ($searchUserId != -1) {
            $where .= " AND tb.userId = '$searchUserId'";
        }
        if ($familyId != -1) {
            $where .= " AND tb.familyId = '$familyId'";
        }
        $sql .= $where;

        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result['count(DISTINCT(th.eventId))'];
        }
        // @codeCoverageIgnoreStart
        return 0;
        // @codeCoverageIgnoreEnd
    }

    /**
     * 获取用户编辑的事件分页
     * @param $pageIndex        页码
     * @param $pageSize        页大小
     * @param $personId        人物id
     * @param $searchUserId        搜索的用户id
     * @param $familyId        家族id
     * @param $myUserId        用户id
     * @return array            一页数据
     */
    public function getMyEditEventsPaging($pageIndex, $pageSize, $personId, $searchUserId, $familyId, $myUserId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT DISTINCT(tb.id),tb.sourceId,tb.personId,tp.name as personName,tb.userId,tb.familyId,tb.resources,tb.is_md,tb.iconograph,tb.type,tb.open_time,tb.exp_time as expTime,tb.exp_name as expName,tb.exp_content as expContent,tb.abstract,tb.write_mode as writeMode,participate_num as participateNum,version,tb.create_time as createTime,tb.create_by as createBy,tb.update_time as updateTime,tb.update_by as updateBy FROM $this->TABLE_HISTORY th 
                    INNER JOIN $this->TABLE tb ON th.eventId = tb.id 
                    LEFT JOIN $this->TABLE_PERSON as tp ON tb.personId=tp.id
                    WHERE th.writer = '$myUserId' AND tb.is_delete = '0' ";
        $where = "";
        if ($personId != -1) {
            $where .= " AND tb.personId = '$personId' ";
        }
        if ($searchUserId != -1) {
            $where .= " AND tb.userId = '$searchUserId'";
        }
        if ($familyId != -1) {
            $where .= " AND tb.familyId = '$familyId'";
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $sql .= $where . " ORDER BY tb.id DESC limit $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    /*
   * @param $personId  人物Id
   *    删除人物相关的大事件
   * */
    public function deleteEventByPersonId($personId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE_EVENT SET is_delete = '1' WHERE personId='$personId'";
        return $this->pdo->update($sql);
    }

    /**
     * 添加用户评论
     * @param  $eventId           大事件ID
     * @param  $userId            用户ID
     * @param  $conmment          评论内容
     * @param  $replyTo           回复的评论ID
     * @param  $replyToUserId     回复的用户ID
     * @return $commentId         评论ID
     */
    public function addEventComment($eventId, $userId, $comment, $replyTo, $replyToUserId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_EVENT_COMMENT(eventId, comment, create_time, create_by, replyto, replyto_userId, update_time, update_by)VALUES('$eventId', '$comment', now(), '$userId', '$replyTo', '$replyToUserId', now(), '$userId')";

        $commentId = $this->pdo->insert($sql);

        return $commentId;
    }

    /**
     * 获取特定大事件的所有评论
     * @param  eventId       大事件ID
     * @return array         获取大事件所有评论
     */
    public function getEventCommentsTotal($eventId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT ct.id, update_by as userId, ut.photo, ct.comment, ct.replyto AS replyto, ct.replyto_userId AS replytoUserId, ut_tmp.nickname AS replytoUserName, ct.create_by AS createBy, ut.nickname AS createByName, ct.create_time AS createTime
            FROM $this->TABLE_EVENT_COMMENT ct INNER JOIN $this->TABLE_USER ut
                ON ut.id = ct.create_by
                LEFT JOIN $this->TABLE_USER ut_tmp
                ON ut_tmp.id = ct.replyto_userId
            WHERE ct.eventId = '$eventId' ORDER BY ct.id DESC;";

        return $this->pdo->query($sql);
    }

    /**
     * 删除大事件评论
     * @param  $commentId         评论ID
     * @return $result            删除的记录数
     */
    public function deleteComment($commentId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_EVENT_COMMENT SET isDelete=1 WHERE id = $commentId AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 验证用户是否对评论有操作权限
     * @param $userId     创建者ID
     * @param $commentId  评论ID
     * @return bool       true代表有权限，false代表无权限
     */
    public function verifyCommentIdAndUserId($userId, $commentId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT create_by AS createBy FROM $this->TABLE_EVENT_COMMENT WHERE id = '$commentId' AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null || $result['createBy'] != $userId) {
            //没有结果或者ID不匹配，返回false
            return false;
        } else {
            return true;
        }
    }

    /**
     * 检查大事件能否关联到用户
     * @param $eventId 大事件id
     * @return bool true代表可以 false代表不可以
     */
    public function checkEventCanLinkToUser($eventId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT userId FROM $this->TABLE WHERE id = '$eventId' ";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null || $result['userId'] > 0) {
            //不可以关联
            return false;
        } else {
            return true;
        }
    }

    /**
     * 检查大事件能否关联到人物
     * @param $eventId 大事件id
     * @return bool true代表可以 false代表不可以
     */
    public function checkEventCanLinkToPerson($eventId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT personId FROM $this->TABLE WHERE id = '$eventId' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result == null || $result['personId'] > 0) {
            //不可以关联
            return false;
        } else {
            return true;
        }
    }

    /**
     * 检查大事件能否关联到人物
     * @param $eventId 大事件id
     * @return bool true代表可以 false代表不可以
     */
    public function checkEventCanCopyToPerson($eventId, $personId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE WHERE (id = '$eventId' or sourceId = '$eventId') AND personId = '$personId' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result == null || $result['count(id)'] > 0) {
            //不可以关联
            return false;
        } else {
            return true;
        }
    }

    /**
     * 将用户大事件关联到人物
     * @param $eventId 大事件id
     * @param $personId 人物id
     * @return int 更新的记录数
     */
    public function linkUserEventToPerson($eventId, $personId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $personDB = new CDBPerson();
        $person = $personDB->getPersonById($personId);

        //更新大事件的personId和familyId
        $sql = "UPDATE $this->TABLE set personId = '$personId',familyId = '$person->familyId' WHERE id = '$eventId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 将人物大事件关联到用户
     * @param $personId 大事件id
     * @param $userId  用户id
     * @return int 更新的记录数
     */
    public function linkAllPersonEventsToUser($personId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set userId = '$userId' WHERE personId = '$personId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 在家族之间复制大事件
     * @param $eventId 大事件id
     * @param $toPersonId 复制到那个家族人物
     * @return int 复制后的大事件id
     */
    public function copyEventToAnotherPerson($eventId, $toPersonId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $personDB = new CDBPerson();
        $toPerson = $personDB->getPersonById($toPersonId);

        $event = $this->getEvent($eventId);

        $event->sourceId = $eventId;
        $event->personId = $toPersonId;
        $event->familyId = $toPerson->familyId;

        return $this->addEvent($event);
    }

    /**
     * 获取大事件的所有副本id（包括source)
     * @param $eventId 大事件Id
     * @param $sourceId 大事件源的id
     * @return array 大事件id数组
     */
    public function getEventAllCopy(int $eventId, int $sourceId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($sourceId == 0) {
            $sql = "SELECT id FROM $this->TABLE WHERE sourceId = '$eventId' ";
            $result = $this->pdo->query($sql);
            $result[]['id'] = $eventId;
        } else {
            $sql = "SELECT id FROM $this->TABLE WHERE sourceId = '$sourceId' ";
            $result = $this->pdo->query($sql);
            $result[]['id'] = $sourceId;
        }

        return $result;
    }

    /**
     * 查询人物已经他的事件数量
     * @param $familyId 族群id
     * @param $maxId    最大id
     * @param $sinceId  起始id
     * @param $count    数量
     * @return array 大事件数组
     */
    public function getPersonPagingWithEvent($familyId, $maxId, $sinceId, $count) {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $where = '';
        if ($maxId != 0) {
            $where .= " AND tb.personId < $maxId ";
        }

        if ($sinceId != 0) {
            $where .= " AND tb.personId > $maxId ";
        }

        $sql = "SELECT tp.id,tp.name,tp.photo,count(tb.personId) as num FROM $this->TABLE tb 
                INNER JOIN $this->TABLE_PERSON tp on tp.id = tb.personId 
                WHERE tb.familyId = '$familyId' AND tb.is_delete = 0 $where 
                GROUP BY tb.personId 
                HAVING count(tb.personId) > 0 
                ORDER BY tb.personId DESC LIMIT 0, $count";

        $result = $this->pdo->query($sql);

        foreach ($result as $key => $person) {
            $personId = $person['id'];
            $sql = "SELECT tb.abstract FROM $this->TABLE tb 
                    INNER JOIN $this->TABLE_PERSON tp on tp.id = tb.personId 
                    WHERE tb.personId = $personId ORDER BY tb.id DESC limit 0,1";

            $event = $this->pdo->uniqueResult($sql);
            $result[$key]['abstract'] = $event['abstract'];
        }

        return $result;

    } 

    /**
     * 获取有事件的人物的总数
     * @param $familyId 族群id
     * @return int 总数
     */
    public function getPersonWithEventTotal($familyId) {
        $this->init();
        $sql = "SELECT count(tb.personId) as num FROM $this->TABLE tb 
        INNER JOIN $this->TABLE_PERSON tp on tp.id = tb.personId 
        WHERE tb.familyId = '$familyId' AND tb.is_delete = 0 
        GROUP BY tb.personId 
        HAVING count(tb.personId) > 0 ";

        $res = $this->pdo->uniqueResult($sql);

        return $res['num'];
    }

}
