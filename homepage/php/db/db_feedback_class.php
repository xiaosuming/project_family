<?php
    /*用户反馈模块
    *@author kellen
    *date : 2017/2/28
    */

    namespace DB;

    use Util\Util;
    use DB\CDBManager;


    class CDBFeedback
    {
        public $pdo = null;
        public $TABLE = "gener_feedback";

        /**
         * @codeCoverageIgnore
         * @return bool
         */
        public function init()
        {
            if(!$this->pdo)
            {
                if(!isset($GLOBALS['pdo'])){
                    $this->pdo = new CDBManager($GLOBALS['db_host'] , $GLOBALS['db_user'] , $GLOBALS['db_pwd'] , $GLOBALS['db_name']);
                    if($this->pdo)
                        $GLOBALS['pdo'] = $this->pdo;
                }else{
                    $this->pdo = $GLOBALS['pdo'];
                }
            }
            return true;
        }

        /**
         * 提交反馈信息
         * @param $userId 用户id
         * @param $contact 用户联系方式，可以是手机或者邮箱
         * @param $message 用户反馈信息
         * @param $problem_type 问题类型
         * @param $photo 图片描述url
         * @param $clientFrom 1.web端 2.手机网页端 3.安卓端 4.ios端
         * @param $versionNumber
         * @return eventId 返回事件id
         */
        public function addFeedback($userId ,$contact, $message, $problem_type, $photo,$clientFrom,$versionNumber)
        {
            if(!$this->init())
            {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'] , null);
                exit;
                //@codeCoverageIgnoreEnd
            }

            $sql = "INSERT INTO $this->TABLE(userId,contact,feedback_message,problem_type,photo,handle_status,create_time,create_by,update_time,update_by,client_from,version_number) VALUES ('$userId' ,'$contact','$message','$problem_type','$photo','2',now(),'$userId',now(),'$userId','$clientFrom','$versionNumber')";

            $feedbackEventId = $this->pdo->insert($sql);
            return $feedbackEventId;
        }


        /**
         * 处理反馈信息(供管理员使用)
         * @param $userId
         * @param $feedbackId 反馈id
         * @param $status     反馈状态 0是未处理 1是正在处理 2是处理完毕
         * @param $remark     备注
         * @return int 更改信息的条目数
         */
        public function handleFeedbackMessage($userId, $feedbackId, $status, $remark)
        {
            if(!$this->init())
            {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'] , null);
                exit;
                //@codeCoverageIgnoreEnd
            }

            $sql = "UPDATE $this->TABLE SET handle_status = '$status',remark = '$remark',update_by = '$userId',update_time = now() WHERE id = '$feedbackId' ";
            $result = $this->pdo->update($sql);
            return $result;
        }

        /**
         * 获取反馈信息
         * @param $feedbackId  反馈id
         * @return mix 反馈信息
         */
        public function getFeedbackMessage($feedbackId){
            if(!$this->init())
            {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'] , null);
                exit;
                //@codeCoverageIgnoreEnd
            }
            $sql = "SELECT id,userId,contact,feedback_message as feedbackMessage,handle_status as handleStatus,remark,problem_type as problemType,photo,client_from as clientFrom,version_number as versionNumber,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime FROM $this->TABLE WHERE id = '$feedbackId' ";

            $message = $this->pdo->uniqueResult($sql);
            return $message;
        }

        /**
         * 按状态获取反馈信息总数
         * @param $status   反馈的状态 0代表未处理，1代表正在处理，2代表已处理
         * @return $message 返回反馈信息总数
         */
        public function getFeedbackMessageCountByStatus($status){
            if(!$this->init()){
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'] , null);
                exit;
                //@codeCoverageIgnoreEnd
            }
            $sql = "SELECT count(id) FROM $this->TABLE WHERE handle_status = '$status'";
            $result = $this->pdo->uniqueResult($sql);
            return $result['count(id)'];
        }


        /**
         * 获取反馈信息分页
         * @param $status   反馈的状态 0代表未处理，1代表正在处理，2代表已处理
         * @param $pageIndex 页码
         * @param $pageSize  每页大小
         * @return $message 返回反馈信息
         */
        public function getFeedbackMessagePaging($status,$pageIndex,$pageSize)
        {
            if(!$this->init())
            {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'] , null);
                exit;
                //@codeCoverageIgnoreEnd
            }

            $offset = ($pageIndex - 1) * $pageSize;

            $sql = "SELECT id,userId,contact,feedback_message as feedbackMessage,handle_status as handleStatus,remark,problem_type as problemType,photo,client_from as clientFrom,version_number as versionNumber,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime FROM $this->TABLE WHERE handle_status = '$status' order by id desc limit $offset,$pageSize";

            $messages = $this->pdo->query($sql);
            
            return $messages;
        }

    }