<?php
/**
 * 二维码统一分发的数据模型封装
 * @author: jiangpengfei
 * @date:   2019-03-29
 */

namespace DB;

use DB\CDBManager;
use Model\QRCode;

class CDBQRCode
{
    public $pdo = null;

    public $TABLE = 'gener_qrcode';

    public function __construct()
    {
        $this->init();
    }

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo) {
                    $GLOBALS['pdo'] = $this->pdo;
                }

            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }

        return true;
    }

    /**
     * 添加二维码
     * @param $qrcode
     * @return int 添加的记录id
     */
    public function addQRCode(QRCode $qrcode) {
        $sql = "INSERT INTO $this->TABLE 
                (id, code, do, data, userId, isDelete, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$qrcode->id', '$qrcode->code', '$qrcode->do', '$qrcode->data', '$qrcode->userId', '0', '$qrcode->userId', now(), '$qrcode->userId', now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取二维码
     * @param $code 二维码的code，可以通过code获取二维码的具体内容
     * @return mix QRCode对象或者null
     */
    public function getQRCode($code) {
        $sql = "SELECT id, code, do, data, userId, createBy, createTime, updateBy, updateTime FROM $this->TABLE WHERE code = '$code' AND isDelete = '0' ";

        $res = $this->pdo->uniqueResult($sql);

        if ($res == null) {
            return null;
        } else {
            return new QRCode($res);
        }
    }

}
