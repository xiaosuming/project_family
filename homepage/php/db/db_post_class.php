<?php
    /**
     * 人物个人空间模块，使用zset实现的
     * @author jiangpengfei
     * @date 2017-11-22
     */
    
    namespace DB;

    use DB\CDBManager;
    use Util\TaskQueue\Producer;
    use DB\RedisInstanceManager;

    class CDBPost
    {
        public $redis = null;

        public function __construct()
        {
            $redisManager = RedisInstanceManager::getInstance();
            $this->redis = $redisManager->getStorageInstance();
        }

        /**
         * 获取用户的timeline的key
         * @param $userId 用户id
         * @return string key值
         */
        private function getUserTimelineKey($userId){
            return 'pt'.$userId;
        }

        /**
         * 获取百科频道的key
         */
        public function getWikiChannelKey(){
            return "ckizuqun_wiki";
        }

        /**
         * 根据时间戳获取推文
         * TODO: 这里把wiki的select 2删除了
         * @param $userId 获取的用户id
         * @param $timestamp 时间戳
         * @param $size   获取推文的数量
         */
        public function pushPostWikiByTimestamp($userId,$timestamp,$size){
   
			$postIds = $this->redis->zRangeByscore($this->getWikiChannelKey(),$timestamp + 1,time(),array('limit' => array(0, $size),'withscores' => TRUE));
            $postIdCount = count($postIds);
            $maxTimestamp = $timestamp;

            if($postIdCount > 0){
                $now = time();
                foreach($postIds as $postId => $timestamp){
                    $maxTimestamp = $timestamp;
                    $this->redis->zAdd($this->getUserTimelineKey($userId),$now++,$postId);  //time()是时间戳，用来排序
                }
            }

            return [$postIdCount,$maxTimestamp];
        }

        /**
         * 设置推送任务
         * @param $pushUserIds 要推送的userId
         * @param $postId      推文内容
         * @param $postTime    推文时间戳，int型
         * @param $userId      用户id
         * @return mix 任务推送状态
         */
        public function setPostTask($pushUserIds,$postId,$postTime,$userId){
            //为了用户第一时间看到自己发的，这里特殊处理用户自己发的推文
            $this->redis->zAdd($this->getUserTimelineKey($userId),$postTime,$postId);  //time()是时间戳，用来排序

            $userIds = array();
            foreach($pushUserIds as $value){
                array_push($userIds,$value['userId']);
            }
            $data['u'] = $userIds;  //用户id数组
            $data['p'] = $postId;   //推文id
            $data['t'] = $postTime; //推送时间
            $str = \json_encode($data);

            $producer = new Producer();
            $producer->produce($GLOBALS['redis_post'],$str);
        }

        /**
         * 获取一页时间轴上的推文id
         * @param $userId 用户id
         * @param $pageIndex 页码
         * @param $pageSize  页大小
         * @return array 一页postId
         */
        public function getTimeline($userId,$pageIndex,$pageSize){
            $end = (1 - $pageIndex) * $pageSize - 1;
			$start = $end - $pageSize + 1;

            $result = $this->redis->zRange($this->getUserTimelineKey($userId),$start,$end);

            return $result;
        }

        /**
         * 根据sinceId或者maxId，获取一页时间轴上的推文id
         * @param $userId 用户id
         * @param $sinceId  起始id
         * @param $maxId    最大的id
         * @param $count    计数
         * @return array 一页postId
         */
        public function getTimelineBySinceIdAndMaxId($userId, $maxId, $sinceId, $count, $maxTimeStamp = 0, $sinceTimeStamp = 0) 
        {

            $maxIndex = 0;
            $sinceIndex = 0;
            $timestampMode = false;

            if ($maxId != 0) {
                $maxIndex = $this->redis->zRevRank($this->getUserTimelineKey($userId), $maxId);

                if (!$maxIndex) {
                    // maxIndex查不到,说明不存在，则使用maxTimeStamp
                    $timestampMode = true;
                }
            }
            
            if ($sinceId != 0) {
                $sinceIndex = $this->redis->zRevRank($this->getUserTimelineKey($userId), $sinceId);

                if (!$sinceIndex) {
                    // sinceIndex不存在
                    $timestampMode = true;
                }
            }

            if (!$timestampMode) {

                $start = 0;
                $end = 0;
                if ($maxIndex > 0 && $sinceIndex > 0) {
                    // maxIndex和sinceIndex都有效
                    $start = $maxIndex + 1;
                    $end = $start + $count - 1;
                    if ($end >= $sinceIndex) {
                        $end = $sinceIndex - 1;
                    }
                } else if ($maxIndex > 0) {
                    $start = $maxIndex + 1;
                    $end = $start + $count - 1;
                } else if ($sinceIndex > 0) {
                    $start = 0;
                    $end = $start + $count - 1;
                    if ($end >= $sinceIndex) {
                        $end = $sinceIndex - 1;
                    }
                } else {
                    $start = 0;
                    $end = $start + $count - 1;
                }
    
                $result = $this->redis->zRevRange($this->getUserTimelineKey($userId),$start,$end, true);
            } else {

                $startScore = 0;
                $endScore = PHP_INT_MAX;
                if ($maxTimeStamp != 0) {
                    $endScore = $maxTimeStamp;
                }

                if ($sinceTimeStamp != 0) {
                    $startScore = $sinceTimeStamp;
                }

                $result = $this->redis->zRevRangeByScore($this->getUserTimelineKey($userId), $endScore, $startScore, 
                            array('withscores' => TRUE, 'limit' => array(0, $count)));

                if (count($result) > 0) {
                    end($result);
                    $lastEleKey =  key($result);
                    reset($result);
                    $end = $this->redis->zRevRank($this->getUserTimelineKey($userId), $lastEleKey);
                } else {
                    // 没有查到。end设为最后
                    $end = $this->getTimelinePostCount($userId);
                }

            }

            return [$result, $end];
        }

        /**
         * 获取用户时间轴上推文的总数
         * @param $userId 用户id
         * @return int 总数
         */
        public function getTimelinePostCount($userId){

            $result = $this->redis->zSize($this->getUserTimelineKey($userId));
            if($result === FALSE){
                return 0;
            }

            return $result;
        }

        /**
         * 设置删除推文的任务
         * @param $pushUserIds 推送的用户id
         * @param $postId      推文id
         */
        public function setDeletePostTask($pushUserIds,$postId,$userId){

            //为了用户第一时间看到自己删的，这里特殊处理用户自己发的推文
            $this->redis->zRem($this->getUserTimelineKey($userId),$postId);

            $userIds = array();
            foreach($pushUserIds as $value){
                array_push($userIds,$value['userId']);
            }
            $data['u'] = $userIds;  //用户id数组
            $data['p'] = $postId;   //推文id
            $str = \json_encode($data);
            
            $producer = new Producer();
            $producer->produce($GLOBALS['redis_post_delete'], $str);

            return true;
        }
		/**
		 * @param userId
		 * @param startTimestamp
		 * @param endTimestamp
		 * return int 返回在此区间的推文的数量
		 */
		public function getPostCountByTimestamp($userId,$startTimestamp,$endTimestamp){
		
			$result = $this->redis->zCount($this->getUserTimelineKey($userId),$startTimestamp,$endTimestamp);
			return $result;
		}
    };