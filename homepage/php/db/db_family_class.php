<?php

namespace DB;

use DB\CDBManager;
use DB\CDBAccount;
use DB\CDBPerson;
use Model\Family;
use Model\Branch;
use Model\FamilyInnerBranch;
use Util\Pager;
use Util\Util;
use Util\PersonRecord;
use ThirdParty\InstantMessage;
use Util\SysLogger;
use Model\Person;
use Util\RelationParser;
use Model\MiniFamilyBindRecommend;
use TextDiff\TextDiff;
use Model\MiniFamilyUpdateApply;
use Util\RedLock;

use DB\CDBPushPerson;
use Util\TaskQueue\Producer;

class CDBFamily
{
    public $pdo = null;
    public $TABLE = "gener_family";
    public $TABLE_PERSON = "gener_person";
    public $TABLE_PERSON2 = "gener_person2";
    public $TABLE_ADMIN = "gener_familyAdmin";
    public $TABLE_FAMILY_BRANCH_ADMIN = "gener_familyBranchAdmin";
    public $TABLE_USER = "gener_user";
    public $TABLE_USER_PROFILE = "gener_user_profile";
    public $TABLE_FAMILY_USER = "gener_family_user";
    public $TABLE_BRANCH = "gener_branch";
    public $TABLE_FAMILY_BRANCH = "gener_family_branch";
    public $TABLE_MESSAGE = "gener_message";
    public $TABLE_PERSON_DETAIL = "gener_personDetail";
    public $TABLE_FAMILY_CHATGROUP = "gener_family_chatgroup";
    public $TABLE_FAMILY_STORAGE_LIMIT = "gener_family_storage_limit";
    public $TABLE_FAMILY_MERGE = "gener_family_merge";
    public $TABLE_INFORMATION = 'gener_information';
    public $TABLE_INFORMATION_CATEGORY = 'gener_information_category';
    public $TABLE_INFORMATION_FOLLOW = 'gener_information_follow';
    public $TABLE_FAMILY_INNER_BRANCH = "gener_family_inner_branch";
    public $TABLE_FAMILY_INNER_BRANCH_ADMIN = "gener_family_inner_branch_admin";
    public $TABLE_FAMILY_INNER_BRANCH_PERSON = "gener_family_inner_branch_person";

    public $TABLE_FAMILY_SNAPSHOT = "gener_family_snapshot";
    public $TABLE_PERSON_SNAPSHOT = "gener_person_snapshot";
    public $TABLE_FAMILY_ALBUM = "gener_family_album";
    public $TABLE_FAMILY_ALBUM_SNAPSHOT = "gener_family_album_snapshot";
    public $TABLE_FAMILY_PHOTO = "gener_family_photo";
    public $TABLE_FAMILY_PHOTO_SNAPSHOT = "gener_family_photo_snapshot";
    public $TABLE_ALBUM_PERSON = "gener_album_person";
    public $TABLE_ALBUM_PERSON_SNAPSHOT = "gener_album_person_snapshot";
    public $TABLE_SIGNIFICANTEVENT = "gener_significantEvent";
    public $TABLE_SIGNIFICANTEVENT_SNAPSHOT = "gener_significantEvent_snapshot";
    public $TABLE_GRAVE = "gener_grave";
    public $TABLE_GRAVE_SNAPSHOT = "gener_grave_snapshot";

    public $TABLE_MINIFAMILY_BIND_APPLY = "gener_minifamily_bind_apply";
    public $TABLE_MINIFAMILY_BIND_RECORD = "gener_minifamily_bind_record";
    public $TABLE_MINIFAMILY_BIND_ROOT_RECORD = "gener_minifamily_bind_root_record";
    public $TABLE_MINIFAMILY_UPDATE_APPLY = "gener_minifamily_update_apply";

    public $TABLE_PERMISSION = "gener_permission";
    public $TABLE_FAMILY_TREE = "gener_family_tree";


    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }


    /**
     * 检查家族是否存在
     * @param $familyId
     * @return bool
     */
    public function verifyFamilyIsExists($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') FROM $this->TABLE WHERE id = '$familyId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id)"] == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 通过分支验证userId和familyId是否匹配,userId不直接属于familyId，而是familyId的另一个分支家族包含userId
     * @param $userId 用户id
     * @param $verifyFamilyId 要验证家族id
     * @param $userFamilyId 用户属于的家族
     * @return boolean true代表有权限，false代表没有权限
     */
    public function verifyUserIdAndFamilyIdByBranch(int $userId, int $verifyFamilyId, int $userFamilyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //先验证用户传过来的数据是正确的
        if (!$this->isUserForFamily($userFamilyId, $userId)) {
            return false;
        }

        //再验证userFamilyId的分支家族中有verifyFamilyId
        $branches = $this->getBranchesByFamilyId($userFamilyId);
        foreach ($branches as $branch) {
            if ($verifyFamilyId == $branch['familyId']) {
                return true;
            }
        }
        return false;
    }

    /**
     * 检查用户id和分支id的权限
     * @param $userId 用户id
     * @param $branchId 分支id
     * @param $userFamilyId 用户家族id
     * @return bool true代表有权限,false代表无权限
     */
    public function verifyUserIdAndBranchIdByFamilyId(int $userId, int $branchId, int $userFamilyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //先检查userId和userFamilyId是否对应
        if (!$this->isUserForFamily($userFamilyId, $userId)) {
            return false;
        }

        $familyIds = $this->getFamilyIdsByBranchId($branchId);
        foreach ($familyIds as $familyId) {
            if ($familyId['familyId'] == $userFamilyId) {
                return true;
            }
        }

        return false;
    }

    /**
     * 验证通过合并家族方式的用户，是否对家族有读取权限
     *
     * @param $userId 用户id
     * @param $familyId 家族id
     *
     * @return bool true有权限，false无权限
     */
    public function verifyUserReadPermissionByMerge(int $userId, int $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $mergeFamilySql = "SELECT merge_familyId FROM $this->TABLE_FAMILY_MERGE WHERE src_familyId = '$familyId' ";
        $mergeFamilyIds = $this->pdo->query($mergeFamilySql);   // 合并的家族

        $array = array();

        array_walk($mergeFamilyIds, function (&$v, $k) {
            $v = $v['merge_familyId'];
        });

        $userFamilyIds = $this->getJoinFamilyList($userId);

        foreach ($userFamilyIds as $familyId) {
            if (in_array($familyId['id'], $mergeFamilyIds)) {
                return true;
            }
        }

        return false;
    }


    /**
     * 初始化家族存储
     */
    public function initFamilyStorage(int $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $maxSize = $GLOBALS['MAX_FAMILY_STORAGE'];

        $sql = "INSERT INTO $this->TABLE_FAMILY_STORAGE_LIMIT (familyId,max_size,used_size,create_time,update_time)VALUES('$familyId','$maxSize',0,now(),now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 创建家族后的初始化操作
     */
    private function createFamilyInitCallback($familyId)
    {
        $this->initFamilyStorage($familyId);
    }

    /**
     * 创建家族
     * @param $userId
     * @param $familyName 家族名
     * @param $adminName 管理员名
     * @param $surname      姓氏
     * @param $adminGender 管理员性别
     * @param $address     管理员的住址
     * @param $birthday    管理员的生日
     * @param $desc        家族描述
     * @param $photo       管理员的照片
     * @param $familyPhoto 家族照片
     * @param $hxChatRoom  环信的聊天室id
     * @param $groupType   群体类型，1是家族，2是家庭，3是同学，4是朋友
     * @return int 家族的Id或者-1
     */
    public function createFamily(
        $userId,
        $familyName,
        $adminName,
        $surname,
        $adminGender,
        $address,
        $birthday,
        $desc,
        $photo,
        $familyPhoto,
        $hxChatRoom,
        $groupType
    )
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 生成identity
        $identity = md5($userId . $familyName . time()) . Util::generateRandomCode(32);

        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            $sql = "INSERT INTO $this->TABLE(name,surname,min_level,max_level,
            permission,originator,description,photo,hx_chatroom,identity,groupType,
            create_by,create_time,update_by,update_time)
            VALUES
            ('$familyName','$surname','0','0','0','$userId','$desc','$familyPhoto',
            '$hxChatRoom','$identity','$groupType','$userId',now(),'$userId',now())";

            $familyId = $this->pdo->insert($sql);   //返回插入行的id或者-1
            if ($familyId < 0) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            //将创始人添加到管理员表
            if (!$this->addAdmin($familyId, $userId)) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败1");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            $personDao = new CDBPerson();
            //向人物表中插入一条人物记录
            $personId = $personDao->addPersonWithNoTransaction($userId, $familyId, $adminName, $adminGender, $address, $birthday, $photo);
            if ($personId < 0) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "新增人物失败");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            //更新人物资料中的家族数量
            $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1 WHERE userId = '$userId' ";
            $this->pdo->update($sql);

            $this->createFamilyInitCallback($familyId);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return [$familyId, $personId];                                //返回家族id
    }

    /**
     * 创建家族是自动生成圈子
     * @param $userId
     * @param $familyName
     * @param $adminName
     * @param $surname
     * @param $adminGender
     * @param $address
     * @param $birthday
     * @param $desc
     * @param $photo
     * @param $familyPhoto
     * @param $hxChatRoom
     * @param $groupType
     * @return array|int
     */
    public function createFamilyAndCircle($userId, $familyName, $adminName, $surname, $adminGender,
                                          $address, $birthday, $desc, $photo, $familyPhoto, $hxChatRoom, $groupType)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 生成identity
        $identity = md5($userId . $familyName . time()) . Util::generateRandomCode(32);

        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            //创建圈子
            $sql = "INSERT INTO $this->TABLE_INFORMATION_CATEGORY (userId,avatar,name,circleType,create_by,create_time,
            update_by,update_time) VALUES ('$userId','$familyPhoto','$familyName',3,'$userId',now(),'$userId',now())";

            $socialCircleId = $this->pdo->insert($sql);

            if ($socialCircleId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '创建圈子失败');
                $this->pdo->rollback();
                exit;
            }

            $sql = "INSERT INTO $this->TABLE(name,surname,min_level,max_level,
            permission,originator,description,photo,hx_chatroom,identity,groupType,socialCircleId,
            create_by,create_time,update_by,update_time)
            VALUES
            ('$familyName','$surname','0','0','0','$userId','$desc','$familyPhoto',
            '$hxChatRoom','$identity','$groupType','$socialCircleId','$userId',now(),'$userId',now())";

            $familyId = $this->pdo->insert($sql);   //返回插入行的id或者-1
            if ($familyId < 0) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            //创始人关注圈子
            $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
            VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
            $followId = $this->pdo->insert($sql);
            if ($followId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                $this->pdo->rollback();
                exit;
            }
            $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=1 WHERE id = '$socialCircleId'";
            $updateRow = $this->pdo->update($sql);
            if ($updateRow <= 0) {
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
                $this->pdo->rollback();
                exit;
            }

            //将创始人添加到管理员表
            if (!$this->addAdmin($familyId, $userId)) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败1");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            $personDao = new CDBPerson();
            //向人物表中插入一条人物记录
            $personId = $personDao->addPersonWithNoTransaction($userId, $familyId, $adminName, $adminGender, $address, $birthday, $photo);
            if ($personId < 0) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "新增人物失败");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            //更新人物资料中的家族数量
            $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1 WHERE userId = '$userId' ";
            $this->pdo->update($sql);

            $this->createFamilyInitCallback($familyId);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return [$familyId, $personId, $socialCircleId];                                //返回家族id
    }

    /**
     * 向家族用户中插入数据
     * @param $familyId 家族id
     * @param $userId   用户id
     * @param $userType 用户类型，1是有权限的临时用户，2是通过活动分享，问题分享的临时用户。无查看族群详情的权限
     * @return int 添加的记录数
     */
    public function addFamilyUser($familyId, $userId, $userType = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_FAMILY_USER(familyId,userId,userType,is_delete,create_time,update_time)
                    VALUES
                    ('$familyId','$userId', '$userType', '0',now(),now())";

        //var_dump($this);

        return $this->pdo->insert($sql);
    }

    /**
     * 删除家族申请用户
     * @param $familyId 家族Id
     * @param $userId   用户id
     * @return int 删除的记录数
     */
    public function deleteFamilyUser($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE_FAMILY_USER SET is_delete = '1' WHERE familyId = '$familyId' AND userId = '$userId'";
        return $this->pdo->update($sql);
    }

    /**
     * 删除家族
     * @param $familyId 家族id
     * @return int 删除的家族数量
     */
    public function deleteFamily($familyId, $socialCircleId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE set is_delete = '1' WHERE id = $familyId";
            $result = $this->pdo->update($sql);

            //更新家族中人物的状态为删除
            $sql = "UPDATE $this->TABLE_PERSON SET is_delete = '1' WHERE familyId = '$familyId' ";
            $this->pdo->update($sql);
            //检查用户是否在未绑定人物的表中
            $sql = "UPDATE $this->TABLE_FAMILY_USER SET is_delete = '1' WHERE familyId = '$familyId'";
            $this->pdo->update($sql);
            //删除家族时更新用户的家族数
            $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num - 1 WHERE userId in(
                    SELECT userId FROM $this->TABLE_PERSON WHERE familyId = '$familyId'
                ) ";
            $this->pdo->update($sql);
            //删除圈子
            $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET is_delete=1 WHERE id='$socialCircleId' AND is_delete=0";
            $this->pdo->update($sql);
            //删除圈子关注列表
            $sql = "UPDATE $this->TABLE_INFORMATION_FOLLOW SET isDelete=1 WHERE categoryId='$socialCircleId' AND isDelete=0";
            $this->pdo->update($sql);
            //删除圈子资讯和动态
            $sql = "UPDATE $this->TABLE_INFORMATION SET is_delete=1 WHERE categoryId='$socialCircleId' AND is_delete=0";
            $this->pdo->update($sql);
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return $result;
    }

    /**
     * 根据家族id获取家族信息
     * @param $familyId
     * @return object family对象
     */
    public function getFamilyById($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,name,surname,min_level as minLevel,max_level as maxLevel,
        hx_chatroom as hxChatRoom,qrcode,permission,originator,member_count as memberCount,
        description,photo,identity,type,groupType,socialCircleId,create_by as createBy,create_time as createTime,
        update_by as updateBy,update_time as updateTime,open_type as openType , start_level as startLevel
                    FROM $this->TABLE WHERE id = '$familyId' and is_delete = '0' ";

        $queryResult = $this->pdo->uniqueResult($sql);
        if ($queryResult != null) {
            $family = new Family($queryResult);
            return $family;
        }
        return null;
    }

    /**
     * 根据家族id数组获取家族信息
     * @param array $familyIds 家族id数组
     */
    public function getFamilyByIds($familyIds)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $str = '(' . Util::arrayToString($familyIds) . ')';


        $sql = "SELECT id,name,surname,min_level as minLevel,max_level as maxLevel,hx_chatroom as hxChatRoom,qrcode,permission,originator,member_count as memberCount,description,photo,groupType,socialCircleId,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime 
                    FROM $this->TABLE WHERE id in $str and is_delete = '0' ";

        $queryResult = $this->pdo->query($sql);

        return $queryResult;
    }

    /**
     * 更新家族中的level标志
     * @param $familyId        家族id
     * @param $minLevel        家族中的最大level
     * @param $maxLevel        家族中的最小level
     * @return int 更新的记录id，失败则返回-1
     */
    public function updateFamilyLevel($familyId, $minLevel, $maxLevel)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET min_level = $minLevel,max_level =$maxLevel WHERE id = $familyId  and is_delete = '0' ";
        $updateId = $this->pdo->update($sql);
        return $updateId;
    }

    /**
     * 自动更新家族level
     *
     * @param $familyId 族群id
     */
    public function autoUpdateFamilyLevel($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT MAX(level) as maxLevel, MIN(level) as minLevel FROM $this->TABLE_PERSON WHERE familyId = '$familyId' AND is_delete = '0' ";

        $res = $this->pdo->uniqueResult($sql);

        if ($res != null) {
            $maxLevel = $res['maxLevel'];
            $minLevel = $res['minLevel'];

            $sql = "UPDATE $this->TABLE SET max_level = '$maxLevel', min_level = '$minLevel' WHERE id = '$familyId' ";

            $updateRow = $this->pdo->update($sql);

            return $updateRow > 0;
        }
        //@codeCoverageIgnoreStart
        return false;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 更新家族描述
     * @param $familyId 家族id
     * @param $description 家族描述
     * @return int 更新的家族数
     */
    public function updateFamilyDescription($familyId, $description)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET description = '$description' WHERE id = '$familyId'  and is_delete = '0' ";
        $updateCount = $this->pdo->update($sql);
        return $updateCount;
    }

    /**
     * 更新家族照片
     * @param $familyId 家族id
     * @param $photo 家族照片
     */
    public function updateFamilyPhoto($familyId, $photo)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET photo = '$photo' WHERE id = '$familyId'  and is_delete = '0' ";
        $updateCount = $this->pdo->update($sql);
        return $updateCount;
    }

    /**
     * 修改家族名字
     * @param $familyId
     * @param $familyName
     * @return mixed
     */
    public function updateFamilyName($familyId, $familyName)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET name = '$familyName' WHERE id = '$familyId'  and is_delete = '0' ";
        $updateCount = $this->pdo->update($sql);
        return $updateCount;
    }

    /**
     * 获取用户创建的家族数
     * @param $userId 用户id
     * @return int 家族数
     */
    public function getUserCreateFamilyCount($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*) FROM $this->TABLE WHERE create_by = '$userId' AND is_delete = '0' ";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(*)'];
    }

    /**
     * 获取管理的家族列表
     * @param userId
     * @return array 家族列表
     */
    public function getMyManageFamily($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT tb.id,tb.name,tb.surname,tb.max_level as maxLevel,tb.min_level as minLevel,tb.hx_chatroom as hxChatRoom,tb.permission,tb.originator,tu.nickname as originatorName,tb.member_count as memberCount,tb.description,tb.photo,tb.groupType,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime from $this->TABLE tb 
                   inner join $this->TABLE_ADMIN ta on tb.id = ta.familyId  
                   inner join $this->TABLE_USER tu on tu.id = tb.originator 
                   WHERE ta.adminId = '$userId'  and tb.is_delete = '0'
                    ";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取加入的家族
     * @param $userId
     * @return array 获取加入的家族列表
     */
    public function getJoinFamily($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.name,tb.surname,tb.max_level as maxLevel,tb.min_level as minLevel,tb.hx_chatroom as hxChatRoom,tb.permission,tb.originator,tu.nickname as originatorName,tb.member_count as memberCount,tb.description,tb.photo,groupType,identity,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                    from $this->TABLE tb 
                    inner join $this->TABLE_PERSON tp on tb.id = tp.familyId 
                    left join $this->TABLE_USER tu on tu.id = tb.originator 
                    where tp.userId = '$userId'  and tb.is_delete = '0' AND tp.is_delete = '0' ";
        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * 获取临时加入的家族（加入家族但是没有绑定用户）
     * @param $userId 用户id
     * @return array 获取加入的家族列表
     */
    public function getJoinFamilyForTmp($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.name,tb.surname,tb.max_level as maxLevel,tb.min_level as minLevel,hx_chatroom as hxChatRoom,tb.permission,tb.originator,tu.nickname as originatorName,tb.member_count as memberCount,tb.description,tb.photo,tb.groupType,tb.identity,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                    from $this->TABLE tb 
                    inner join $this->TABLE_FAMILY_USER tfu on tb.id = tfu.familyId 
                    left join $this->TABLE_USER tu on tu.id = tb.originator 
                    where tfu.userId = '$userId'  and tfu.is_delete = '0' and tb.is_delete = '0' ";

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取加入的家族列表，这个列表是用来提供家族选择的(<select></select>)
     * @param $userId  用户id
     * @return array 获取加入的家族列表
     */
    public function getJoinFamilyList($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT DISTINCT tb.id,tb.name,tb.surname 
                    from $this->TABLE tb 
                    inner join $this->TABLE_PERSON tp on tb.id = tp.familyId 
                    where tp.userId = '$userId' and tb.is_delete = '0'";
        $result = $this->pdo->query($sql);
        return $result;
    }


    /**
     * 获取通过申请的用户列表
     * @param $familyId
     * @return mixed
     */
    public function getApplyUserList($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT fu.userId,gu.username,gu.nickname,gu.photo,fu.create_time as createTime, fu.update_time as updateTime FROM $this->TABLE_FAMILY_USER fu 
          INNER JOIN $this->TABLE_USER gu ON fu.userId=gu.id WHERE fu.is_delete='0' AND fu.familyId='$familyId' ORDER BY fu.id DESC";
        $result = $this->pdo->query($sql);
        return $result;
    }


    /**
     * 判断绑定的用户是否是申请列表中的用户
     * @param $applyUserId
     * @param $familyId
     * @return bool
     */
    public function isUserForApply($applyUserId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT userId FROM $this->TABLE_FAMILY_USER WHERE familyId='$familyId' AND userId='$applyUserId' AND is_delete='0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['userId'] == $applyUserId) {
            return true;
        }
        return false;
    }


    /**
     *    判断当前用户是否是家族中的创始人
     * @param $familyId 家族id
     * @param $userId 用户id
     * @return bool true是，false不是
     */
    public function isOriginatorForFamily($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT originator FROM $this->TABLE WHERE id = '$familyId'  and is_delete = '0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['originator'] == $userId) {
            return true;
        }
        return false;
    }

    /**
     * 判断当前用户是否是家族中的管理员
     * @param $familyId 家族id
     * @param $userId 用户id
     * @return bool true是，false不是
     */
    public function isAdminForFamily($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        # 旧版本弃用
//        $sql = "SELECT id FROM $this->TABLE_ADMIN WHERE familyId = '$familyId' AND adminId = '$userId' AND isDelete=0";
//        $result = $this->pdo->query($sql);
//        if (count($result) > 0) {
//            return true;
//        }
//        return false;

        return Util::checkPermission($this->getUserPermission($userId, $familyId)) < 4 ? false:true;
    }

    /**
     * 判断当前用户是否是家族中的成员,判断了gener_person表和gener_family_user表
     * @param $familyId 家族id
     * @param $userId   用户id
     * @return boolean
     */
    public function isUserForFamily($familyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        // 旧版本弃用
//        //检查用户是否在家族中绑定人物
//        $sql = "SELECT id FROM $this->TABLE_PERSON WHERE familyId = '$familyId' AND userId = '$userId' AND is_delete = '0' ";
//        $result = $this->pdo->uniqueResult($sql);
//        if ($result != null) {
//            return true;
//        }
//        //检查用户是否在未绑定人物的表中
//        $sql = "SELECT id FROM $this->TABLE_FAMILY_USER WHERE familyId = '$familyId' AND userId = '$userId' AND is_delete = '0' ";
//        $result = $this->pdo->uniqueResult($sql);
//        if ($result != null) {
//            return true;
//        }
//
//        return false;
        return $this->getUserPermission($userId, $familyId) ? true:false;
    }

    /**
     * 获取人物所在的分支
     * @param $personId 人物id
     * @return array 分支数组
     */
    public function getPersonInnerBranch($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, branchId, personId, createBy, createTime, updateBy, updateTime 
                FROM $this->TABLE_FAMILY_INNER_BRANCH_PERSON WHERE personId = '$personId' AND isDelete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 获取分支管理员所管理的分支
     * @param $userId  管理员id
     * @return array 分支数组
     */
    public function getAdminInnerBranch($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, branchId, userId, createBy, createTime, updateBy, updateTime FROM $this->TABLE_FAMILY_INNER_BRANCH_ADMIN WHERE userId = '$userId' AND isDelete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 检查用户是否是人物的分支管理员
     * @param
     */
    public function isAdminForPersonInBranch($userId, $personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 获取人物所在的分支
        $personBranches = $this->getPersonInnerBranch($personId);

        // 获取用户管理的分支
        $adminBranches = $this->getAdminInnerBranch($userId);

        // 检查是否有交集，有交集则有权限管理
        $adminBranchMap = [];
        foreach ($adminBranches as $adminBranch) {
            $adminBranchMap[$adminBranch['branchId']] = true;
        }

        foreach ($personBranches as $personBranch) {
            if (isset($adminBranchMap[$personBranch['branchId']])) {
                return true;
            }
        }

        return false;
    }

    /**
     * 添加家族管理员,将某个已存在的用户设置为家族管理员
     * @param $familyId 家族id
     * @param $userId 已存在的用户id
     * @return bool true添加成功,false添加失败
     */
    public function addAdmin($familyId, $userId)
    {
        $accountDb = new CDBAccount();
        //检查当前用户Id是否存在
        if ($accountDb->isUserIdExist($userId)) {
            if (!$this->init()) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
                exit;
                //@codeCoverageIgnoreEnd
            }

            //检查当前用户是否已经是管理员了
            if (!$this->isAdminForFamily($familyId, $userId)) {
                $currentUserId = $GLOBALS['userId'];
                $sql = "INSERT INTO $this->TABLE_ADMIN (familyId,adminId,create_by,create_time,update_by,update_time)
                            VALUES('$familyId','$userId','$currentUserId',now(),'$currentUserId',now())";
                $insertId = $this->pdo->insert($sql);
                if ($insertId > 0)
                    return true;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * 撤销某个家族管理员
     * @param $famiylId 家族id
     * @param $userId 已存在的用户id
     * @return bool true撤销成功,false撤销失败
     */
    public function rescindAdmin($familyId, $userId)
    {
        $accountDb = new CDBAccount();
        //检查当前用户id是否存在
        if ($accountDb->isUserIdExist($userId)) {
            if (!$this->init()) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
                exit;
                //@codeCoverageIgnoreEnd
            }
            $sql = "UPDATE $this->TABLE_ADMIN  SET isDelete=1 WHERE familyId = '$familyId' AND adminId = '$userId' AND isDelete=0";
            $deleteRows = $this->pdo->update($sql);
            if ($deleteRows > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 移交创始人权限
     * @param $familyId 家族id
     * @param $userId 要移交给的用户id
     * @return bool true移交成功，false移交失败
     */
    public function transferOriginator($familyId, $userId)
    {
        global $logger;            //获取全局的日志句柄变量
        $accountDb = new CDBAccount();
        //检查当前用户id是否存在
        if ($accountDb->isUserIdExist($userId)) {

            //判断要移交的人物是否是家族内部的
            if (!$this->isUserForFamily($familyId, $userId)) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
                exit;
                //@codeCoverageIgnoreEnd
            }

            if (!$this->init()) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
                exit;
                //@codeCoverageIgnoreEnd
            }
            try {
                $this->pdo->beginTransaction();
                //删除要移交的用户的管理员身份
                $sql = "UPDATE $this->TABLE_ADMIN SET isDelete=1 WHERE familyId = '$familyId' AND adminId = '$userId' AND isDelete=0";
                $this->pdo->update($sql);

                //将家族的创始人更改为userId
                $sql = "UPDATE $this->TABLE SET originator = '$userId' WHERE id = '$familyId' ";
                $updateRows = $this->pdo->update($sql);

                //将创始人添加到管理员表
                if (!$this->addAdmin($familyId, $userId)) {
                    //@codeCoverageIgnoreStart
                    Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败");
                    $this->pdo->rollback();            //事务回滚
                    exit;
                    //@codeCoverageIgnoreEnd
                }

                $this->pdo->commit();
                if ($updateRows > 0) {
                    return true;
                }
                //@codeCoverageIgnoreStart
            } catch (PDOException $e) {
                $this->pdo->rollback();
                $logger->error(exceptionFormat($e));
                //@codeCoverageIgnoreEnd
            }
        }

        return false;
    }


    /**
     * 获取家族创始人id
     * @param $familyId 家族id
     * @return mix 家族创始人id或者null
     */
    public function getFamilyOriginatorId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT originator FROM $this->TABLE WHERE id = '$familyId'  and is_delete = '0' ";

        $queryResult = $this->pdo->uniqueResult($sql);
        if ($queryResult != null) {
            return $queryResult['originator'];
        }
        return null;
    }


    /**
     * 获取家族管理员id
     * @param $familyId 家族id
     * @param array 家族管理员id数组
     *
     */
    public function getFamilyAdminId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT adminId FROM $this->TABLE_ADMIN WHERE familyId = '$familyId' AND isDelete=0";

        return $this->pdo->query($sql);
    }

    /**
     * 获取家族的qrcode
     * @param $familyId 家族id
     * @return mix 二维码地址或者null
     */
    public function getFamilyQRCode($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT qrcode FROM $this->TABLE WHERE id = '$familyId' ";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 更新用户二维码地址
     * @param $familyId 家族id
     * @param $qrcode   二维码地址
     * @return int 更新的记录数
     */
    public function updateFamilyQRCode($familyId, $qrcode)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET qrcode = '$qrcode' WHERE id = '$familyId'";
        return $this->pdo->update($sql);
    }

    /**
     * 获取家族的唯一标示码
     * @param $familyId 家族id
     * @return mix string:家族唯一标示码,如果家族不存在则返回null
     */
    public function getFamilyIdentity($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $identity = "";

        $sql = "SELECT identity FROM $this->TABLE WHERE id = '$familyId'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result === null) {
            //@codeCoverageIgnoreStart
            return null;
            //@codeCoverageIgnoreEnd
        } else {
            $identity = $result['identity'];
            if ($identity === "" || $identity === null) {
                //随机生成
                //@codeCoverageIgnoreStart
                $identity = Util::generateRandomCode(60);
                $sql = "UPDATE $this->TABLE SET identity = '$identity' WHERE id = '$familyId' AND is_delete = '0'";
                if (($result = $this->pdo->update($sql)) > 0) {
                    return $identity;
                } else {
                    return null;
                    //@codeCoverageIgnoreEnd
                }
            }

            return $identity;
        }
    }

    /**
     * 根据家族唯一标示码来获取家族id
     * @param $identity 家族唯一标示码
     * @return mix null代表不存在或者familyId
     */
    public function getFamilyIdByIdentity($identity)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE identity = '$identity' AND is_delete = '0'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return null;
        } else {
            return $result['id'];
        }
    }

    /**
     * 跟据identity获取家族信息
     * @param $identity 家族唯一标示码
     * @return mix null代表不存在，或者家族信息
     */
    public function getFamilyInfoByIdentity($identity)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,name,surname,photo,description,create_time FROM $this->TABLE WHERE identity = '$identity' AND is_delete = '0'";
        $result = $this->pdo->uniqueResult($sql);

        return $result;
    }

    /**
     * 接受申请加入家族
     * @param $familyId   家族id
     * @param $userId     用户id
     * @return int 插入的记录数
     */
    public function acceptApply($familyId, $userId)
    {
        //$TABLE_FAMILY_USER
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_FAMILY_USER
                    (familyId,userId,is_delete,create_time,update_time)
                    VALUES
                    ('$familyId','$userId','0',now(),now())";
        return $this->pdo->insert($sql);
    }

    /**
     * 根据家族id活动环信聊天室id
     * @param $familyId 家族id
     * @return string 环信的聊天室id
     */
    public function getChatRoomByFamilyId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT hx_chatroom FROM $this->TABLE WHERE id = '$familyId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result !== null) {
            return $result['hx_chatroom'];
        }
        return null;
    }

    /**
     * 将两个家族的同一个人合并
     * @params $fromPersonId 第一个人的id
     * @params $toPersonId 第二个人的id
     * @param action 合并方式
     * @return int 正数代表合并成功,为更新的记录数，非正数代表合并失败,-1代表人物不存在,-2代表人物是同一个家族的,-3代表人物已存在合并的家族
     */
    public function mergeTwoPerson(int $fromPersonId, int $toPersonId,$action)
    {
        $PERSON_NOT_EXIST_ERROR = -1;
        $PERSON_SAME_FAMILY_ERROR = -2;
        $PERSON_EXIST_REF_FAMILY_ERROR = -3;

        $familyFromSql = "SELECT id,familyId,ref_familyId,ref_personId FROM $this->TABLE_PERSON WHERE id = '$fromPersonId' OR id = '$toPersonId' ";
        $result = $this->pdo->query($familyFromSql);
        if (count($result) < 2) {
            //有人物不存在，直接返回错误
            //@codeCoverageIgnoreStart
            return $PERSON_NOT_EXIST_ERROR;
            //@codeCoverageIgnoreEnd
        }
        $familyFromId = null;
        $familyToId = null;
        $personFromRefPersonIds = array();
        $personToRefPersonIds = array();
        $personFromRefFamilyIds = array();
        $personToRefFamilyIds = array();

        foreach ($result as $person) {
            // if ($person['ref_familyId'] > 0) {
            //     //如果人物绑定了家族,则返回错误，不允许绑定
            //     return $PERSON_EXIST_REF_FAMILY_ERROR;
            // }
            if ($person['id'] == $fromPersonId) {
                $familyFromId = $person['familyId'];
                if ($person['ref_personId'] != null) {
                    $personIds = json_decode($person['ref_personId'], true);
                    $personFromRefPersonIds = $personIds;
                }

                if ($person['ref_familyId'] != null) {
                    $familyIds = json_decode($person['ref_familyId'], true);
                    $personFromRefFamilyIds = $familyIds;
                }
            } else if ($person['id'] == $toPersonId) {
                $familyToId = $person['familyId'];
                if ($person['ref_personId'] != null) {
                    $personIds = json_decode($person['ref_personId'], true);
                    $personToRefPersonIds = $personIds;
                }

                if ($person['ref_familyId'] != null) {
                    $familyIds = json_decode($person['ref_familyId'], true);
                    $personToRefFamilyIds = $familyIds;
                }
            }
        }

        if ($familyFromId == $familyToId) {
            //如果家族一等于家族二的id
            //@codeCoverageIgnoreStart
            return $PERSON_SAME_FAMILY_ERROR;
            //@codeCoverageIgnoreEnd
        }

        array_push($personFromRefPersonIds, intval($toPersonId));
        array_push($personFromRefFamilyIds, intval($familyToId));

        array_push($personToRefPersonIds, intval($fromPersonId));
        array_push($personToRefFamilyIds, intval($familyFromId));

        $personFromRefPersonIdsStr = json_encode($personFromRefPersonIds);
        $personToRefPersonIdsStr = json_encode($personToRefPersonIds);
        $personFromRefFamilyIdsStr = json_encode($personFromRefFamilyIds);
        $personToRefFamilyIdsStr = json_encode($personToRefFamilyIds);
        //将familyFrom的id添加到personTo的ref_familyId中,将familyTo的id添加到personFrom的ref_familyId中
        $sql = "UPDATE $this->TABLE_PERSON  
                        SET ref_personId = CASE id 
                        WHEN $fromPersonId THEN \"$personFromRefPersonIdsStr\"
                        WHEN $toPersonId THEN \"$personToRefPersonIdsStr\" 
                        END,
                        ref_familyId = CASE id 
                        WHEN $fromPersonId THEN \"$personFromRefFamilyIdsStr\" 
                        WHEN $toPersonId THEN \"$personToRefFamilyIdsStr\"  
                        END 
                        WHERE id  IN ($fromPersonId,$toPersonId) ";
        $result = $this->pdo->update($sql);

        // $this->addFamilyMergeRecord($familyFromId, $familyToId,$action);
        // if(!is_null($toPersonId)){
        //     $this->addFamilyUser($familyFromId, $toPersonId ,1);
        // }
        // if(!is_null($fromPersonId)){
        //    $this->addFamilyUser($familyToId, $fromPersonId ,1);
        // }
        return $result;
    }

    /**
     * 合并家族
     * @param $personOneId 人物一的id from
     * @param $personTwoId 人物二的id  to
     * @param $action 合并的动作
     * @return int 正数代表合并成功,为更新的记录数，
     *             非正数代表合并失败,-1代表人物不存在,-2代表人物是同一个家族的,-3代表人物已存在合并的家族,
     *             -4不存在的并操作,-5代表SQL操作失败
     */
    public function mergeFamily(int $fromPersonId, int $toPersonId, $action): int
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $UNKNOW_MERGE_ACTION = -4;
        $FAILED_SQL_EXEC = -5;

        $updateCount = 0;

        if ($action == $GLOBALS['MERGE_FAMILY_BY_MARRIAGE']) {
            $personDB = new CDBPerson();
            $personOne = $personDB->getPersonObjectById($fromPersonId);
            $personTwo = $personDB->getPersonObjectById($toPersonId);

            try {
                $this->pdo->beginTransaction();
                //给personOne增加一个配偶fromPersonSpouse
                $fromPersonSpouse = new Person();
                $fromPersonSpouse->id = Util::getNextId();
                $fromPersonSpouse->father = $personOne->father;
                $fromPersonSpouse->mother = $personOne->mother;
                $fromPersonSpouse->son = $personOne->son;
                $fromPersonSpouse->daughter = $personOne->daughter;
                $fromPersonSpouse->level = $personOne->level;
                $fromPersonSpouse->type = 2;
                if($personOne->gender == 0){
                    $fromPersonSpouse->gender =  1;
                    $fromPersonSpouse->brother =  array();
                    array_push($fromPersonSpouse->brother,$fromPersonSpouse->id);                  

                }else{
                    $fromPersonSpouse->gender =  0;
                    $fromPersonSpouse->sister =  array();
                    array_push($fromPersonSpouse->sister,$fromPersonSpouse->id); 
                }
                $fromPersonSpouse->familyId = $personOne->familyId;
                $fromPersonSpouse->userId = $personTwo->userId;
                $fromPersonSpouse->relation = $GLOBALS['RELATION_SPOUSE'];
                $fromPersonSpouse->corePersonId = $personOne->id;
                $fromPersonSpouse->name = $personTwo->name;
                $fromPersonSpouse->familyIndex = $personOne->familyIndex + 1 ;
                $fromPersonSpouseId = $personDB->addRelatePerson2($fromPersonSpouse, false);

                //给personTwo增加一个配偶toPersonSpouse
                $toPersonSpouse = new Person();
                $toPersonSpouse->id = Util::getNextId();
                $toPersonSpouse->father = $personTwo->father;
                $toPersonSpouse->mother = $personTwo->mother;
                $toPersonSpouse->son = $personTwo->son;
                $toPersonSpouse->daughter = $personTwo->daughter;
                if($personTwo->gender == 0){
                    $toPersonSpouse->gender =  1;
                    $toPersonSpouse->brother =  array();
                    array_push($toPersonSpouse->brother,$toPersonSpouse->id);                  

                }else{
                    $toPersonSpouse->gender =  0;
                    $toPersonSpouse->sister =  array();
                    array_push($toPersonSpouse->sister,$toPersonSpouse->id); 
                }
                $toPersonSpouse->familyId = $personTwo->familyId;
                $toPersonSpouse->level = $personTwo->level;
                $toPersonSpouse->type = 2;
                $toPersonSpouse->userId = $personOne->userId;
                $toPersonSpouse->relation = $GLOBALS['RELATION_SPOUSE'];
                $toPersonSpouse->corePersonId = $personTwo->id;
                $toPersonSpouse->name = $personOne->name;
                $toPersonSpouse->familyIndex = $personTwo->familyIndex + 1 ;
                $toPersonSpouseId = $personDB->addRelatePerson2($toPersonSpouse, false);

                //将fromPersonSpouseId和personTwo绑定
                if (($updateCount += $this->mergeTwoPerson($fromPersonSpouseId, $toPersonId,$action)) < 0) {
                    //绑定失败
                    //@codeCoverageIgnoreStart
                    $this->pdo->rollback();
                    return $FAILED_SQL_EXEC;
                    //@codeCoverageIgnoreEnd
                }

                //将toPersonSpouseId和personOne绑定
                if (($updateCount += $this->mergeTwoPerson($toPersonSpouseId, $fromPersonId,$action)) < 0) {
                    //绑定失败
                    //@codeCoverageIgnoreStart
                    $this->pdo->rollback();
                    return $FAILED_SQL_EXEC;
                    //@codeCoverageIgnoreEnd
                }
          

                $this->pdo->commit();
                //@codeCoverageIgnoreStart
            } catch (PDOException $e) {
                $this->pdo->rollback();
                return $FAILED_SQL_EXEC;
                //@codeCoverageIgnoreEnd
            }
        } else if ($action == $GLOBALS['MERGE_FAMILY_BY_PERSON']) {
            //根据同一个人来合并家族

            $updateCount = $this->mergeTwoPerson($fromPersonId, $toPersonId,$action);
        // } 
        /**************************版本3新增开始********************** */
        }elseif($action == $GLOBALS['MERGE_FAMILYS_TO_NEW_FAMILY']){
            $updateCount = $this->mergeFamilysToNewFamily($fromPersonId, $toPersonId);
        }
        /**************************版本3新增结束********************** */
        else {
            //@codeCoverageIgnoreStart
            return $UNKNOW_MERGE_ACTION;
            //@codeCoverageIgnoreEnd
        }
        return $updateCount;
    }

    /**
     * 新增家族分支
     */
    public function addBranch(Branch $branch)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_BRANCH (branch_name,branch_personId,type,create_time,create_by,update_time,update_by)
                VALUES('$branch->branchName','$branch->branchPersonId','$branch->type',now(),'$branch->createBy',now(),'$branch->updateBy')";
        return $this->pdo->insert($sql);
    }

    /**
     * 添加家族和fen
     *
     */
    public function addFamilyBranch(int $familyId, int $branchId, int $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_FAMILY_BRANCH (familyId,branchId,create_time,create_by,update_time,update_by)
                VALUES('$familyId','$branchId',now(),'$userId',now(),'$userId')";

        return $this->pdo->insert($sql);
    }

    /**
     * 更新人物的主分支id
     * @param $main 主分支id
     * @param $level 辈分
     * @return int 更新的记录数
     */
    private function updatePersonBranchId($mainBranchId, $familyId, $level = null)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_PERSON SET branchId = '$mainBranchId' WHERE familyId = '$familyId' ";


        if ($level != null) {
            $where = " AND level > '$level' ";
            $sql .= $where;
        }

        return $this->pdo->update($sql);
    }

    /**
     * 根据人物id更新人物分支id
     * @param $mainBranchId 主分支id
     * @param $personId     人物id
     * @return int 更新的记录数
     */
    private function updatePersonBranchIdByPersonId($mainBranchId, $personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_PERSON SET branchId = '$mainBranchId' WHERE id = '$personId' ";

        return $this->pdo->update($sql);
    }

    /**
     * 根据分支id查询拥有这些分支的家族
     * @param $branchId 分支id
     * @return array 家族数组
     */
    private function getFamilyIdsByBranchId($branchId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT familyId FROM $this->TABLE_FAMILY_BRANCH WHERE branchId = '$branchId' ";
        return $this->pdo->query($sql);
    }

    /**
     * 根据家族id查询分支
     * @param $familyId 家族id
     * @return array 分支数组
     */
    private function getBranchIdsByFamilyId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT branchId FROM $this->TABLE_FAMILY_BRANCH WHERE familyId = '$familyId' ";
        return $this->pdo->query($sql);
    }

    /**
     * 根据家族id获取分支情况
     * @param $familyId 家族id
     * @return array 分支情况
     */
    public function getBranchesByFamilyId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.branch_name as branchName,tb.branch_personId as branchPersonId,tp.familyId,tb.type,tb.create_time as createTime,tb.create_by as createBy FROM $this->TABLE_FAMILY_BRANCH tfb 
                INNER JOIN $this->TABLE_BRANCH tb on tb.id = tfb.branchId 
                LEFT JOIN $this->TABLE_PERSON tp on tb.branch_personId = tp.id 
                 WHERE tfb.familyId = '$familyId' ";
        return $this->pdo->query($sql);
    }

    /**
     * 合并分支
     * @param $personOneId 分支一的合并人物id
     * @param $personTwoId 分支二的合并人物id
     * @param $newBranchName 合并后的主分支名
     * @param $userId        操作用户的id
     * @return true代表成功，-1代表人物一存在子代，不能合并,-2代表人物不存在,-3是sql执行异常
     */
    public function mergeBranch(int $personOneId, int $personTwoId, string $newBranchName, int $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $ERROR_PERSON_ONE_EXIST_CHILD = -1;
        $ERROR_PERSON_NOT_EXIST = -2;
        $ERROR_MAIN_BRANCH_FAMILY_EMPTY = -3;       //主分支的家族为空

        $personDB = new CDBPerson();
        //获取人物一的信息
        $personOne = $personDB->getPersonById($personOneId);
        if ($personOne == null) {
            //@codeCoverageIgnoreStart
            return $ERROR_PERSON_NOT_EXIST;
            //@codeCoverageIgnoreEnd
        }
        //人物一的子辈必须是空集，不然不允许合并
        if (count($personOne->son) != 0 || count($personOne->daughter) != 0) {
            //@codeCoverageIgnoreStart
            return $ERROR_PERSON_ONE_EXIST_CHILD;
            //@codeCoverageIgnoreEnd
        }

        //获取人物二的信息
        $personTwo = $personDB->getPersonById($personTwoId);
        if ($personTwo == null) {
            //@codeCoverageIgnoreStart
            return $ERROR_PERSON_NOT_EXIST;
            //@codeCoverageIgnoreEnd
        }

        //查询personOneId所在家族
        $familyOne = $this->getFamilyById($personOne->familyId);
        $mainBranchId = $personOne->branchId;     //主分支id


        try {
            $this->pdo->beginTransaction();

            if ($mainBranchId == null) {
                //两个家族的合并
                //@codeCoverageIgnoreStart
                //将家族一创建为一个新的子分支
                $branchOne = new Branch();
                $branchOne->branchName = $familyOne->name . "[子分支]";
                $branchOne->branchPersonId = $personOneId;
                $branchOne->type = 2;   //子分支
                $branchOne->createBy = $userId;
                $branchOne->updateBy = $userId;
                $branchOneId = $this->addBranch($branchOne);    //分支一

                //将家族二创建为一个新的子分支
                $familyTwo = $this->getFamilyById($personTwo->familyId);
                $branchTwo = new Branch();
                $branchTwo->branchName = $familyTwo->name . "[子分支]";
                $branchTwo->branchPersonId = $personTwoId;
                $branchTwo->type = 2;   //子分支
                $branchTwo->createBy = $userId;
                $branchTwo->updateBy = $userId;
                $branchTwoId = $this->addBranch($branchTwo);    //分支二

                //创建一个主分支
                $mainBranch = new Branch();
                $mainBranch->branchName = $newBranchName . "[主支]";
                $mainBranch->branchPersonId = 0;         //主分支的personId无参考价值
                $mainBranch->type = 1;   //主分支
                $mainBranch->createBy = $userId;
                $mainBranch->updateBy = $userId;
                $mainBranchId = $this->addBranch($mainBranch);    //主分支

                //为家族一增加三个分支标记
                $this->addFamilyBranch($familyOne->id, $branchOneId, $userId);
                $this->addFamilyBranch($familyOne->id, $branchTwoId, $userId);
                $this->addFamilyBranch($familyOne->id, $mainBranchId, $userId);

                //为家族二增加三个分支标记
                $this->addFamilyBranch($familyTwo->id, $branchOneId, $userId);
                $this->addFamilyBranch($familyTwo->id, $branchTwoId, $userId);
                $this->addFamilyBranch($familyTwo->id, $mainBranchId, $userId);

                //将家族二中比personTwoId的level低的人物branchId全部置为mainBranchId
                $this->updatePersonBranchId($mainBranchId, $familyTwo->id, $personTwo->level);

                //将家族一中所有人物的branchId置为mainBranchId
                $this->updatePersonBranchId($mainBranchId, $familyOne->id);

                //将家族二中personTwoId的人物branchId置为mainBranchId
                $this->updatePersonBranchIdByPersonId($mainBranchId, $personTwoId);
                //@codeCoverageIgnoreEnd
            } else {
                //一个家族向一个主分支上的合并
                //检索出拥有这个主分支的所有家族familys
                $familyIds = $this->getFamilyIdsByBranchId($mainBranchId);
                if (count($familyIds) <= 0) {
                    //如果不存在这样的家族，直接返回错误
                    //@codeCoverageIgnoreStart
                    return $ERROR_MAIN_BRANCH_FAMILY_EMPTY;
                    //@codeCoverageIgnoreEnd
                }

                //将家族二创建为一个新的子分支newBranch
                $familyTwo = $this->getFamilyById($personTwo->familyId);
                $branchTwo = new Branch();
                $branchTwo->branchName = $familyTwo->name . "[子分支]";
                $branchTwo->branchPersonId = $personTwoId;
                $branchTwo->type = 2;   //子分支
                $branchTwo->createBy = $userId;
                $branchTwo->updateBy = $userId;
                $branchTwoId = $this->addBranch($branchTwo);    //分支二

                //检索出其中一个家族所有的分支branchs
                $branchIds = $this->getBranchIdsByFamilyId($familyIds[0]['familyId']);

                //为所有familys增加newBranch
                foreach ($familyIds as $familyId) {
                    $this->addFamilyBranch($familyId['familyId'], $branchTwoId, $userId);
                }

                //为家族二增加branchs和newBranch
                foreach ($branchIds as $branchId) {
                    $this->addFamilyBranch($familyTwo->id, $branchId['branchId'], $userId);
                }
                $this->addFamilyBranch($familyTwo->id, $branchTwoId, $userId);

                //将家族二中比personTwoId的level低的人物branchId全部置为mainBranchId
                $this->updatePersonBranchId($mainBranchId, $familyTwo->id, $personTwo->level);

                //将家族二中personTwoId的人物branchId置为mainBranchId
                $this->updatePersonBranchIdByPersonId($mainBranchId, $personTwoId);
            }

            //更新personOne和personTwo的指向
            if (($mergeResult = $this->mergeTwoPerson($personOneId, $personTwoId,$action)) < 0) {
                //@codeCoverageIgnoreStart
                $this->pdo->rollback();
                return -3;
                //@codeCoverageIgnoreEnd
            }

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -3;
            //@codeCoverageIgnoreEnd
        }

        return 1;
    }

    /**
     * 获取正在申请中的用户列表
     * @param $familyId 家族id
     */
    public function getApplyingUserList(int $familyId, $adminId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tm.id,tm.from_user as applyUserId,tu.nickname,tu.username,tu.photo,tm.content,tm.module,tm.action,tm.recordId,tm.extra_recordId,tm.create_by as createBy,tm.create_time as createTime  FROM $this->TABLE_MESSAGE tm
        INNER JOIN $this->TABLE_USER tu on tm.from_user = tu.id 
        WHERE to_user = '$adminId' AND isAccept = '0' AND action = ' " . $GLOBALS['APPLYJOIN'] . "' AND recordId = '$familyId' AND isDelete = '0'  ";

        return $this->pdo->query($sql);
    }

    /**
     * 获取家族性别分布
     *
     * @param $familyId 家族ID
     * @return array    性别分布数组
     */
    public function getFamilyGenderInfo($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT gender, COUNT(id) sum FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and is_delete = '0' GROUP BY gender";
        $res = $this->pdo->query($sql);
        $result = array();

        if (count($res) > 0) {
            $result = array(
                "female" => intval($res[0]['sum']),
                "male" => intval($res[1]['sum'] ?? 0)
            );
        }

        return $result;
    }

    /**
     * 获取家族年龄分布
     *
     * @param $familyId    家族ID
     * @return array       男女人数
     */
    public function getFamilyAgeInfo($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT TIMESTAMPDIFF(YEAR, birthday, NOW()) AS age FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and is_delete = 0";
        $res = $this->pdo->query($sql);
        $total = count($res);

        $result = array();
        $text = array("0~10", "11~20", "21~30", "31~40", "41~50", "51~60", "60以上", "未知");

        for ($i = 0; $i < count($text); $i++) {
            $result[$i]['name'] = $text[$i];
            $result[$i]['sum'] = 0;
        }

        foreach ($res as $i) {
            $age = $i['age'];
            if (is_null($age)) {
                //@codeCoverageIgnoreStart
                $result[7]['sum'] += 1;
                continue;
                //@codeCoverageIgnoreEnd
            }

            if ($age >= 0 && $age <= 10) {
                $result[0]['sum'] += 1;
                //@codeCoverageIgnoreStart
            } else if ($age > 10 && $age <= 20) {
                $result[1]['sum'] += 1;
            } else if ($age > 20 && $age <= 30) {
                $result[2]['sum'] += 1;
            } else if ($age > 30 && $age <= 40) {
                $result[3]['sum'] += 1;
            } else if ($age > 40 && $age <= 50) {
                $result[4]['sum'] += 1;
            } else if ($age > 50 && $age <= 60) {
                $result[5]['sum'] += 1;
            } else if ($age > 60) {
                $result[6]['sum'] += 1;
            } else {  //age小于0或不存在
                $result[7]['sum'] += 1;
                //@codeCoverageIgnoreEnd
            }
        }
        return $result;
    }

    /**
     * 获取家族成员的地区分布
     *
     * @param  $familyId     家族ID
     * @param  $country      国家，-1为国家统计
     * @param  $province     省份，-1为省份统计
     * @param  $city         城市，-1为城市统计
     * @param  $area         区，-1为区统计
     * @return array         各个地区的人数分布
     */
    public function getFamilyAreaInfo($familyId, $country, $province, $city, $area)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "";
        if ($country == -1) {
            $sql = "SELECT country, COUNT(*) sum FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and is_delete = 0 GROUP BY country";
        } else if ($province == -1) {
            $sql = "SELECT province, COUNT(*) sum FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and country = '$country' and is_delete = 0 GROUP BY province";
        } else if ($city == -1) {
            $sql = "SELECT city, COUNT(*) sum FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and country = '$country' and province = '$province' and is_delete = 0 GROUP BY city";
        } else if ($area == -1) {
            $sql = "SELECT area, COUNT(*) sum FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and country = '$country' and province = '$province' and city = '$city' and is_delete = 0 GROUP BY area";
        } else {
            $sql = "SELECT town, COUNT(*) sum FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and country = '$country' and province = '$province' and city = '$city' and area = '$area' and is_delete = 0 GROUP BY town";
        }
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取家族成员的血型分布
     *
     * @param familyId 家族ID
     * @return array    各种不同血型的人数
     */
    public function getFamilyBloodTypeInfo($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT blood_type as bloodType, COUNT(*) sum FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and is_delete = 0 GROUP BY blood_type";
        $res = $this->pdo->query($sql);
        $result = array();
        $unknownSum = 0;
        foreach ($res as $i) {
            $bloodType = $i['bloodType'];
            $tmp = array();
            if ($bloodType == "0" || is_null($bloodType)) {
                //@codeCoverageIgnoreStart
                $unknownSum += intval($i['sum']);
                //@codeCoverageIgnoreEnd
            } else {
                switch ($bloodType) {
                    case "1":
                        //@codeCoverageIgnoreStart
                        $tmp['name'] = "A";
                        break;
                    //@codeCoverageIgnoreEnd
                    case "2":
                        $tmp['name'] = "B";
                        break;
                    //@codeCoverageIgnoreStart
                    case "3":
                        $tmp['name'] = "O";
                        break;
                    case "4":
                        $tmp['name'] = "AB";
                        break;
                    //@codeCoverageIgnoreEnd
                }
                $tmp['sum'] = intval($i['sum']);
                array_push($result, $tmp);
            }
        }
        $tmp['name'] = "未知";
        $tmp['sum'] = $unknownSum;
        array_push($result, $tmp);
        return $result;
    }

    /**
     * 获取家族成员的婚姻状况
     *
     * @param  $familyId    家族ID
     * @return array        统计数据 包括未婚人数、已婚人数、离婚人数
     */
    public function getFamilyMaritalInfo($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT marital_status maritalStatus, COUNT(*) sum FROM $this->TABLE_PERSON WHERE familyId = '$familyId' and is_delete = 0 GROUP BY marital_status";
        $res = $this->pdo->query($sql);
        $result = array();
        foreach ($res as $i) {
            $maritalStatus = $i['maritalStatus'];
            $tmp = array();
            switch ($maritalStatus) {
                case "0":
                    $tmp['name'] = "未婚";
                    break;
                //@codeCoverageIgnoreStart
                case "1":
                    $tmp['name'] = "已婚";
                    break;
                case "2":
                    $tmp['name'] = "离婚";
                    break;
                default:
                    $tmp['name'] = "未知";
                    break;
                //@codeCoverageIgnoreEnd
            }
            $tmp['sum'] = intval($i['sum']);
            array_push($result, $tmp);
        }
        return $result;
    }

    /**
     * 获取家族成员的辈分分布
     *
     * @param   $familyId       家族ID
     * @return  array           统计数据，包括家族成员的辈分信息
     */
    public function getFamilyLevelInfo($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT level, COUNT(*) sum FROM $this->TABLE_PERSON WHERE is_delete = 0 and familyId = '$familyId' GROUP BY level";
        $res = $this->pdo->query($sql);
        $result = array();
        foreach ($res as $i) {
            $temp = array();
            $temp['familyLevel'] = intval($i['level']);
            $temp['sum'] = intval($i['sum']);
            array_push($result, $temp);
        }
        return $result;
    }

    /**
     * 获取家族成员的爱好分布
     *
     * @param  $familyId    家族ID
     * @return array        统计信息
     */
    public function getFamilyHobbyInfo($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT info ,COUNT(*) sum FROM $this->TABLE_PERSON_DETAIL WHERE personId IN(SELECT id FROM $this->TABLE_PERSON WHERE familyId = '$familyId' AND is_delete = '0') AND TYPE = '2' GROUP BY info";
        $res = $this->pdo->query($sql);
        $result = array();
        foreach ($res as $i) {
            $temp = array();
            $temp['info'] = $i['info'];
            $temp['sum'] = intval($i['sum']);
            array_push($result, $temp);
        }
        return $result;
    }

    /**
     * 创建家族群聊
     * @param $familyId 家族id
     * @param $chatGroupName 群组名
     * @param $description  群组描述
     * @param $maxusers 最大用户数
     * @param $isPublic 是否公开
     * @param $needPermit 是否需要管理员同意
     * @param $allowInvites 是否允许群成员邀请
     * @param $owner     群聊创建人的环信id
     * @param $userId    创建人
     * @param $members 成员数组
     * @param $customInfo 自定义字段
     * @codeCoverageIgnore
     * @return int 记录插入id,0代表未插入,-1代表创建失败
     */
    public function createChatGroupInFamily($familyId, $chatGroupName, $description, $maxusers, $isPublic, $needPermit, $allowInvites, $owner, $userId, $members, $customInfo)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //调用即时聊天接口
        $im = new InstantMessage();
        $chatGroupId = $im->createChatGroup($chatGroupName, $description, $maxusers, $owner, $members, $isPublic == 1, $needPermit == 1, $allowInvites == 1, $customInfo);

        if (!$chatGroupId) {
            return -1;
        }

        $sql = "INSERT INTO $this->TABLE_FAMILY_CHATGROUP (familyId,chatGroupId,chatGroupName,description,maxusers,is_public,needPermit,allowinvites,owner,create_by,create_time,update_by,update_time)VALUES('$familyId','$chatGroupId','$chatGroupName','$description','$maxusers','$isPublic','$needPermit','$allowInvites','$owner','$userId',now(),'$userId',now())";

        $this->pdo->insert($sql);
        return $chatGroupId;
    }

    /**
     * 根据家族id获取群聊列表
     * @param $familyId 家族id
     */
    public function getChatGroupsByFamilyId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tfc.id,tfc.familyId,tb.name as familyName,tfc.chatGroupId,tfc.chatGroupName,tfc.description,tfc.maxusers,tfc.is_public as isPublic,tfc.needPermit,tfc.allowinvites,tfc.owner,tfc.create_by as createBy,tfc.create_time as createTime,tfc.update_by as updateBy,tfc.update_time as updateTime 
        FROM $this->TABLE_FAMILY_CHATGROUP  tfc 
        LEFT JOIN $this->TABLE tb on tb.id = tfc.familyId 
        where tfc.familyId = '$familyId' AND tfc.is_delete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 根据环信群聊id获取群聊详情
     * @param $chatGroupId
     * @return map 群聊详情
     */
    public function getChatGroupByHxId($chatGroupId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "SELECT tfc.id,tfc.familyId,tb.name as familyName,tfc.chatGroupId,tfc.chatGroupName,tfc.description,tfc.maxusers,tfc.is_public as isPublic,tfc.needPermit,tfc.allowinvites,tfc.owner,tfc.create_by as createBy,tfc.create_time as createTime,tfc.update_by as updateBy,tfc.update_time as updateTime 
        FROM $this->TABLE_FAMILY_CHATGROUP  tfc 
        LEFT JOIN $this->TABLE tb on tb.id = tfc.familyId 
        where tfc.chatGroupId = '$chatGroupId' AND tfc.is_delete = '0'  ";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 根据我们自己的系统id获取群聊
     * @param $systemId 系统id
     * @return map 群聊详情
     */
    public function getChatGroupBySystemId($systemId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "SELECT tfc.id,tfc.familyId,tb.name as familyName,tfc.chatGroupId,tfc.chatGroupName,tfc.description,tfc.maxusers,tfc.is_public as isPublic,tfc.needPermit,tfc.allowinvites,tfc.owner,tfc.create_by as createBy,tfc.create_time as createTime,tfc.update_by as updateBy,tfc.update_time as updateTime 
        FROM $this->TABLE_FAMILY_CHATGROUP  tfc 
        LEFT JOIN $this->TABLE tb on tb.id = tfc.familyId 
        where tfc.id = '$systemId' AND tfc.is_delete = '0' ";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 根据环信的群聊id删除群聊
     * @param $chatGroupId 环信的群聊id
     * @return int 更新的记录数
     */
    public function deleteChatGroupByHxId($chatGroupId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_FAMILY_CHATGROUP SET is_delete = '1',update_time = now() WHERE chatGroupId = '$chatGroupId' AND is_delete = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 根据系统id删除群聊
     * @param $chatGroupId 环信的群聊id
     * @return int 更新的记录数
     */
    public function deleteChatGroupBySystemId($systemId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_FAMILY_CHATGROUP SET is_delete = '1' WHERE id = '$systemId' AND is_delete = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 获取绑定了userId的最低level
     * @param int $familyId
     * @return mix null表明记录不存在,int表示最低level
     */
    public function getFamilyMinLevelWithUserId(int $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT level FROM $this->TABLE_PERSON 
                WHERE familyId = '$familyId' AND is_delete = '0' AND userId > 0 
                ORDER BY level ASC limit 0,1 ";

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return null;
        } else {
            return $result['level'];
        }
    }

    /**
     * 添加家族合并的记录
     *
     * @param int $srcFamilyId
     * @param int $mergeFamilyId
     * @param action 合并方式
     * @param fromPersonid  合并发起人
     * @param toPersonId  接受合并人
     * @return mixed
     */
    public function addFamilyMergeRecord(int $srcFamilyId, int $mergeFamilyId,$action,$fromPersonId,$toPersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        //首先检查两个家族合并的记录是否存在
        // $sql = "SELECT id FROM $this->TABLE_FAMILY_MERGE 
        //         where src_familyId = '$srcFamilyId' AND merge_familyId = '$mergeFamilyId' ";

        // $result = $this->pdo->query($sql);

        // if (count($result) !== 0) {
        //     //记录已存在,直接返回true
        //     return $result[0]['id'];
        // } else {
            //否则添加新的记录
            $sql = "INSERT INTO $this->TABLE_FAMILY_MERGE 
                    (src_familyId,merge_familyId,merge_way,from_personId,to_personId,create_time,update_time)
                    VALUES
                    ($srcFamilyId,$mergeFamilyId,$action,$fromPersonId,$toPersonId,now(),now())";

            return $this->pdo->insert($sql);
        // }
    }


    /**
     * 分页获取家族导出的pdf列表
     * @param $familyId
     * @param $pageIndex
     * @param $pageSize
     * @return mixed
     */
    public function getExportFamilyPdfPaging($familyId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT id,familyId,preview_pdf,print_pdf,create_time,create_by,update_time,update_by,options,familyTreeName,startPersonId,family_name as familyName
                FROM gener_pdf_task WHERE finish = 1 AND familyId = '$familyId'  AND is_delete = 0 ORDER BY id DESC
                LIMIT $offset,$pageSize";

        return $this->pdo->query($sql);
    }

    /**
     * 获取开放的族群列表
     */
    public function getOpenFamilys()
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,name,surname,min_level as minLevel,max_level as maxLevel,
        hx_chatroom as hxChatRoom,qrcode,permission,originator,member_count as memberCount,
        description,photo,groupType,create_by as createBy,create_time as createTime,
        update_by as updateBy,update_time as updateTime,open_type as openType,socialCircleId 
                    FROM $this->TABLE WHERE open_type = '1' and is_delete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 过滤掉不符合要求的数据
     * @param $eventIdsArr
     * @param $familyId
     * @return mixed
     */
    public function checkEventIdIsExists($eventIdsArr, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $eventIdsString = Util::arrayToString($eventIdsArr);
        $sql = "SELECT id FROM gener_significantEvent WHERE id in ($eventIdsString) AND familyId = '$familyId' AND is_delete = 0";
        return $this->pdo->query($sql);
    }

    /**
     * 过滤掉不符合要求的数据
     * @param $photoIdsArr
     * @param $familyId
     * @return mixed
     */
    public function checkPhotoIdIsExists($photoIdsArr, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $photoIdsString = Util::arrayToString($photoIdsArr);
        $sql = "SELECT id FROM gener_family_photo WHERE id in ($photoIdsString) AND  is_delete=0 AND albumId IN 
          (SELECT id FROM gener_family_album WHERE familyId='$familyId' AND is_delete=0)";
        return $this->pdo->query($sql);
    }

    /**
     * 过滤掉不符合要求的数据
     * @param $graveIdsArr
     * @param $familyId
     * @return mixed
     */
    public function checkGraveIdIsExists($graveIdsArr, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $graveIdsString = Util::arrayToString($graveIdsArr);
        $sql = "SELECT id FROM gener_grave WHERE familyId = '$familyId' AND id in ($graveIdsString) ";
        return $this->pdo->query($sql);
    }

    /**
     * 获取家族内部的分支
     * @param $familyId 家族id
     * @return array 分支数组
     */
    public function getFamilyInnerBranch($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,familyId,personId,branchName,createBy,createTime,updateBy,updateTime FROM $this->TABLE_FAMILY_INNER_BRANCH WHERE familyId = '$familyId' AND isDelete = '0'";

        return $this->pdo->query($sql);
    }

    /**
     * 获取家族内部分支下的人物
     * @param $branchId 分支id
     * @return array 人物数组
     */
    public function getInnerBranchPersonIds($branchId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT personId FROM $this->TABLE_FAMILY_INNER_BRANCH_PERSON WHERE branchId = '$branchId' AND isDelete = '0'";

        return $this->pdo->query($sql);
    }

    /**
     * 根据人物id获取以该人物为起点的分支
     */
    public function getBranchByLeadPersonId($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,familyId,personId,branchName,createBy,createTime,updateBy,updateTime FROM $this->TABLE_FAMILY_INNER_BRANCH WHERE personId = '$personId' AND isDelete = '0' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result != null) {
            return new FamilyInnerBranch($result);
        }

        return null;
    }

    /**
     * 获取内建分支的详情
     */
    public function getInnerBranch($branchId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,familyId,personId,branchName,createBy,createTime,updateBy,updateTime FROM $this->TABLE_FAMILY_INNER_BRANCH WHERE id = '$branchId' AND isDelete = '0' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result != null) {
            return new FamilyInnerBranch($result);
        }

        return null;
    }

    /**
     * 在家族内部创建分支
     * @param $familyId   家族名
     * @param $branchName 分支名
     * @param $personId 分支起点的人物id
     * @param $userId  创建的用户id
     */
    public function createFamilyInnerBranch($familyId, $branchName, $personId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $branchMap = [];                // 分支表

            $familyInnerBranch = $this->getFamilyInnerBranch($familyId);
            foreach ($familyInnerBranch as $branch) {
                $branchMap[$branch['personId']] = $branch;
            }

            $this->pdo->beginTransaction();

            $sql = "INSERT INTO $this->TABLE_FAMILY_INNER_BRANCH 
                    (familyId, personId, branchName, createBy, createTime, updateBy, updateTime)
                    VALUES
                    ('$familyId', '$personId', '$branchName','$userId', now(), '$userId', now())";

            $createBranchId = $this->pdo->insert($sql);

            $needInsertPersonIdArray = [$personId];  // 需要插入到新分支的人物id数组
            $needQueryPersonIdArray = [$personId];   // 需要查询的人物id数组

            // 把该personId下面的所有后代都标记到该分支下
            // 1. 要考虑分支嵌套的问题。如果新的分支下有小的子分支，可以通过查询子分支来减少查询次数
            // 2. 遍历每个人物的后代，然后标记

            $personDB = new CDBPerson();
            while (count($needQueryPersonIdArray) > 0) {
                $personId = array_pop($needQueryPersonIdArray);
                // 查询这个personId是否在分支中
                if (isset($branchMap[$personId])) {
                    $branchId = $branchMap[$personId]['id'];
                    $personIds = $this->getInnerBranchPersonIds($branchId);
                    // 插入这个分支下的所有人物
                    foreach ($personIds as $insertPersonId) {
                        array_push($needInsertPersonIdArray, $insertPersonId['personId']);
                    }
                } else {
                    // 插入这个人物的子代和配偶
                    $person = $personDB->getPersonById($personId);
                    foreach ($person->son as $insertPersonId) {
                        array_push($needInsertPersonIdArray, $insertPersonId);
                        array_push($needQueryPersonIdArray, $insertPersonId);
                    }

                    foreach ($person->daughter as $insertPersonId) {
                        array_push($needInsertPersonIdArray, $insertPersonId);
                        array_push($needQueryPersonIdArray, $insertPersonId);
                    }

                    foreach ($person->spouse as $insertPersonId) {
                        array_push($needInsertPersonIdArray, $insertPersonId);
                    }
                }
            }

            $personTable = $this->TABLE_FAMILY_INNER_BRANCH_PERSON;

            $insertSql = "INSERT INTO  $personTable (branchId, personId, createBy, createTime, updateBy, updateTime) VALUES ";
            $isFirst = true;
            // 将所有的needInsertPersonIdArray组装成insert语句
            for ($i = 0; $i < count($needInsertPersonIdArray); $i++) {
                if ($isFirst) {
                    $insertSql .= "('$createBranchId','$needInsertPersonIdArray[$i]', '$userId', now(), '$userId', now())";
                    $isFirst = false;
                } else {
                    $insertSql .= ",('$createBranchId','$needInsertPersonIdArray[$i]', '$userId', now(), '$userId', now())";
                }
                if ($i != 0 && $i % 1000 == 0) {
                    // 每1000条组装一次
                    $this->pdo->insert($insertSql);
                    $insertSql = "INSERT INTO $personTable (branchId, personId, createBy, createTime, updateBy, updateTime)VALUES";
                    $isFirst = true;
                }
            }

            if (!$isFirst) {
                $this->pdo->insert($insertSql);
            }

            $this->pdo->commit();

            return $createBranchId;
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return -1;
        }
    }

    /**
     * 添加家族内建分支的管理员
     * @param $branchId 分支id
     * @param $adminUserId 管理员用户id
     * @param $userId    操作用户的id
     * @return int 插入的记录id
     */
    public function addInnerBranchAdmin($branchId, $adminUserId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_FAMILY_INNER_BRANCH_ADMIN 
                (branchId, userId, createBy, createTime, updateBy, updateTime) 
                VALUES 
                ('$branchId', '$adminUserId', '$userId', now(), '$userId', now())";

        return $this->pdo->insert($sql);
    }


    /**
     * 检查用户是否是分支管理员
     * @param $userId 用户id
     * @param $branchId 分支id
     * @return bool 是true 否 false
     */
    public function isAdminForBranch($userId, $branchId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT * FROM $this->TABLE_FAMILY_INNER_BRANCH_ADMIN WHERE branchId = '$branchId' AND userId = '$userId' AND isDelete = '0' ";

        $result = $this->pdo->query($sql);

        if (count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 撤销内建分支的管理员
     * @param $branchId 分支id
     * @param $adminUserId   用户id
     * @param $userId    操作用户的id
     * @return int 修改的记录数
     */
    public function rescindInnerBranchAdmin($branchId, $adminUserId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE  $this->TABLE_FAMILY_INNER_BRANCH_ADMIN SET isDelete = '1', updateBy = '$userId', updateTime = now() WHERE branchId = '$branchId' AND userId = '$userId' AND isDelete = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 添加家族快照
     * gener_family,gener_person,gener_family_album,gener_family_photo,gener_album_person
     * gener_significantEvent,gener_grave
     * @param $familyId 家族id
     * @param $tag      快照标签
     * @param $userId   操作的用户id
     * @return int|bool 执行成功返回版本号，失败返回false
     */
    public function addFamilySnapshot($familyId, $tag, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            // 查找出这个家族的最大版本号
            $sql = "SELECT max(version) as maxVersion FROM $this->TABLE_FAMILY_SNAPSHOT WHERE familyId = '$familyId' AND isDelete = '0' ";
            $result = $this->pdo->uniqueResult($sql);

            $version = 1;
            if ($result != null) {
                $version = $result['maxVersion'] + 1;
            }

            $sql = "INSERT INTO $this->TABLE_FAMILY_SNAPSHOT 
                    (familyId, tag, version, createBy, createTime, updateBy, updateTime)
                    VALUES
                    ('$familyId', '$tag', '$version', '$userId', now(), '$userId', now())";

            $this->pdo->insert($sql);

            // 将所有人物数据进行转移
            $sql = "INSERT INTO $this->TABLE_PERSON_SNAPSHOT
                (id, same_personId, infoCardId, branchId, familyId, userId, type, is_delete, name, zpname, zi, birthday, gender, photo, remark, share_url, is_dead, is_adoption, dead_time,ref_familyId, ref_personId,father, mother, son, daughter, spouse, sister, brother, level, confirm, blood_type, marital_status, phone, qq, qrcode, profileText, sideText, country, country_name, province, province_name, city, city_name, area, area_name, town, town_name, address, ranking, create_by,create_time,update_by,update_time, snapshotVersion)
             SELECT *,$version FROM $this->TABLE_PERSON WHERE familyId = '$familyId' AND is_delete = '0' ";

            $this->pdo->insert($sql);

            // 保留家族的相册信息
            $sql = "INSERT INTO $this->TABLE_FAMILY_ALBUM_SNAPSHOT
                    (id, album_name, description, familyId, album_size, create_time, update_time,is_delete, update_by,create_by, snapshotVersion) SELECT *,$version FROM $this->TABLE_FAMILY_ALBUM WHERE  familyId = '$familyId' AND is_delete = '0' ";

            $this->pdo->insert($sql);

            $sql = "INSERT INTO $this->TABLE_FAMILY_PHOTO_SNAPSHOT 
                    (id, albumId, photo, description, country, country_name, province, province_name, city, city_name, area, area_name, town, town_name, size, address, create_time, is_delete, create_by, snapshotVersion) SELECT *,$version FROM $this->TABLE_FAMILY_PHOTO WHERE albumId in (SELECT id FROM $this->TABLE_FAMILY_ALBUM WHERE familyId = '$familyId' AND is_delete = '0')";

            $this->pdo->insert($sql);

            $sql = "INSERT INTO $this->TABLE_ALBUM_PERSON_SNAPSHOT
                    (id, photoId, personId, isDelete, create_time, create_by, snapshotVersion) SELECT *,$version FROM $this->TABLE_ALBUM_PERSON WHERE photoId in (SELECT id FROM $this->TABLE_FAMILY_PHOTO WHERE albumId in (SELECT id FROM $this->TABLE_FAMILY_ALBUM WHERE familyId = '$familyId' AND is_delete = '0'))";

            $this->pdo->insert($sql);

            // 保留家族的大事件
            $sql = "INSERT INTO $this->TABLE_SIGNIFICANTEVENT_SNAPSHOT
                    (id,sourceId,personId,userId,is_delete,iconograph,familyId,resources,type,is_md,open_time,exp_time,exp_name,exp_content,abstract,write_mode,participate_num,version,create_time,create_by,update_time,update_by,snapshotVersion) SELECT *,$version FROM $this->TABLE_SIGNIFICANTEVENT WHERE familyId = '$familyId' AND is_delete = '0'";

            $this->pdo->insert($sql);

            // 保留家族的宗祠信息
            $sql = "INSERT INTO $this->TABLE_GRAVE_SNAPSHOT 
                    (id, personId, person_name, familyId, coor_x, coor_y, address, photo, description, isDelete, create_time, create_by, update_time, update_by, snapshotVersion) SELECT *,$version FROM $this->TABLE_GRAVE WHERE familyId = '$familyId' AND isDelete = '0' ";

            $this->pdo->insert($sql);

            $this->pdo->commit();

            return $version;
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return false;
        }
    }

    /**
     * 获取用户关注的圈子数量
     * @param $userId
     * @return bool
     */
    public function getCountUserFollowCircles($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') FROM $this->TABLE_INFORMATION_FOLLOW WHERE userId='$userId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);

        return $result["count('id')"];
    }

    public function getCircleListPaging($userId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND fw.id < '$maxId' AND fw.id > '$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND fw.id < '$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND fw.id > '$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT fw.id,fw.userId,u.username,fw.categoryId,c.avatar,c.name as categoryName,c.circleType,fw.createTime,fw.createBy,
            fw.updateTime,fw.updateBy FROM $this->TABLE_INFORMATION_FOLLOW as fw 
            LEFT JOIN $this->TABLE_USER as u ON fw.userId=u.id 
            LEFT JOIN $this->TABLE_INFORMATION_CATEGORY as c ON fw.categoryId=c.id
            WHERE fw.userId = '$userId' AND fw.isDelete=0" . $where . "  ORDER BY fw.id DESC LIMIT $count;";

        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * 离开家族
     * @param $familyId
     * @param $userId
     * @param $socialCircleId
     * @return int
     */
    public function leaveFamily($familyId, $userId, $socialCircleId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->TABLE_PERSON SET userId = 0,update_time = now()  WHERE familyId = '$familyId' AND userId = '$userId'  AND is_delete = '0'";
            $count = $this->pdo->update($sql);

            if ($count > 0) {
                //检查是否是家族管理员，如果是也删除掉
                $deleteSql = "UPDATE $this->TABLE_ADMIN SET isDelete=1 WHERE familyId = '$familyId' AND adminId = '$userId' AND isDelete=0 ";
                $this->pdo->update($deleteSql);

                //绑定用户到人物时，更新该人物的家族数资料
                $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num - 1,update_time = now() WHERE userId = '$userId' ";
                $this->pdo->update($sql);

                $sql = "UPDATE $this->TABLE SET member_count=member_count-1 WHERE id='$familyId' AND is_delete=0";
                $this->pdo->update($sql);
                //更新用户关注
                $sql = "UPDATE $this->TABLE_INFORMATION_FOLLOW SET isDelete=1 WHERE categoryId='$socialCircleId' 
                        AND userId = '$userId'";
                $this->pdo->update($sql);

                //更新用户圈子的关注数量
                $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow-1 WHERE id = '$socialCircleId' AND is_delete=0 ";
                $this->pdo->update($sql);

            }
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return $count;

    }

    /**
     * 检查群组的类型
     * @param $familyId 族群id
     * @param $groupType 预期的群组类型
     * @return bool 是true, 否false
     */
    public function checkGroupType($familyId, $groupType)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT groupType FROM $this->TABLE WHERE id = '$familyId' AND is_delete = '0' ";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return false;
        } else {
            if ($result['groupType'] == $groupType) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 获取加入的家族
     * @param $userId
     * @return array
     */
    public function getJoinFamilyPaging($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.name,tb.surname,tb.max_level as maxLevel,tb.min_level as minLevel,tb.hx_chatroom as hxChatRoom,tb.permission,tb.originator,tu.nickname as originatorName,tb.member_count as memberCount,tb.description,tb.photo,groupType,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                    from $this->TABLE tb 
                    inner join $this->TABLE_PERSON tp on tb.id = tp.familyId 
                    left join $this->TABLE_USER tu on tu.id = tb.originator 
                    where tp.userId = '$userId'  and tb.is_delete = '0' AND tp.is_delete = '0'  ORDER BY tb.id DESC ";
        $result = $this->pdo->query($sql);

        return $result;
    }


    /**
     * 申请将家庭绑定到家族
     * @param $miniFamilyId     家庭id
     * @param $personId         人物id
     * @param $familyId         家族id
     * @param $userId           用户id
     * @return int 申请的记录
     */
    public function applyBindMiniFamilyToFamily($miniFamilyId, $miniFamilyPersonId, $familyId, $familyPersonId, $remark, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_MINIFAMILY_BIND_APPLY 
                (miniFamilyId, miniFamilyPersonId, familyId, familyPersonId, remark, userId, isDelete, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$miniFamilyId', '$miniFamilyPersonId', '$familyId', '$familyPersonId', '$remark', '$userId', 0, '$userId', now(), '$userId', now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取绑定家庭的申请详情
     * @param $applyId 申请id
     * @return map|null 返回查找出来的申请详情或者null
     */
    public function getBindMiniFamilyApply($applyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tmba.id, tmba.miniFamilyId, tmba.miniFamilyPersonId, tmba.familyId, tmba.familyPersonId,tmba.remark, tmba.userId, tmba.createBy,tmba.createTime,tmba.updateBy,tmba.updateTime FROM $this->TABLE_MINIFAMILY_BIND_APPLY tmba WHERE tmba.id = '$applyId' AND isDelete = '0' ";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取家庭中人物的直系亲属
     * 这里的直系亲属指的是在族谱上是一家的，会排除配偶之外的关系
     * 只有家庭需要使用这个方法
     * 家庭中只能录入一个父亲、母亲
     * @param $miniFamilyId 家庭id
     * @param $personId     人物id
     * @param $excludeWaiqi 是否排除外戚，默认是false
     * @return array 符合条件的人物数组
     */
    public function getPersonDirectlyRelatedFamilyMembers($miniFamilyId, $personId, $excludeWaiqi = false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 查询所有人的信息，然后以当前人物为节点，查找所有的直系亲属
        $personDB = new CDBPerson();
        $persons = $personDB->getFamilyPersons($miniFamilyId);

        $personMap = [];
        // 建立人物id和人物的关联
        foreach ($persons as $person) {
            $personMap[$person['id']] = $person;
        }

        $core = $personMap[$personId];
        $topPerson = $core;
        while (count($topPerson['father']) > 0) {
            $topPerson = $personMap[$topPerson['father'][0]];
        }

        // 找到$topPerson为最高一代，从$topPerson向下查找所有人物即可
        $needSearchPersons = [$topPerson['id']];
        $allPersons = [$topPerson];

        // 把topPerson的兄弟姐妹也放在allPersons里
        foreach ($topPerson['brother'] as $personId) {
            if ($topPerson['id'] != $personId) {
                $allPersons[] = $personMap[$personId];
                array_push($needSearchPersons, $personId);
            }
        }

        foreach ($topPerson['sister'] as $personId) {
            if ($topPerson['id'] != $personId) {
                $findPerson = $personMap[$personId];
                if (!$excludeWaiqi) {
                    array_push($needSearchPersons, $personId);
                } else {
                    // 排除外戚，清空后代和配偶
                    $findPerson['son'] = [];
                    $findPerson['daughter'] = [];
                    $findPerson['spouse'] = [];
                }

                $allPersons[] = $findPerson;
            }
        }


        while (count($needSearchPersons) > 0) {
            $corePersonId = array_pop($needSearchPersons);

            $person = $personMap[$corePersonId];

            foreach ($person['spouse'] as $personId) {

                // 清空其他的
                $personMap[$personId]['father'] = [];
                $personMap[$personId]['mother'] = [];
                $personMap[$personId]['brother'] = [];
                $personMap[$personId]['sister'] = [];

                $allPersons[] = $personMap[$personId];
            }

            foreach ($person['son'] as $personId) {
                $allPersons[] = $personMap[$personId];
                array_push($needSearchPersons, $personId);
            }

            foreach ($person['daughter'] as $personId) {
                $findPerson = $personMap[$personId];
                if (!$excludeWaiqi) {
                    array_push($needSearchPersons, $personId);
                } else {
                    // 不要外戚，清空后代
                    $findPerson['son'] = [];
                    $findPerson['daughter'] = [];
                    $findPerson['spouse'] = [];
                }

                $allPersons[] = $findPerson;
            }
        }

        return $allPersons;
    }

    /**
     * 通过id查找人物, 这个方法只提供给家庭和家族绑定时使用
     * @param $personId 人物id
     * @return map 人物
     */
    private function getPersonById($personId)
    {
        //['userId', 'name', 'zi', 'zpname', 'birthday', 'photo', 'remark', 'isDead', 'deadTime', 'phone', 'qq', 'country', 'countryName', 'province', 'provinceName', 'city', 'cityName', 'area', 'areaName', 'town', 'townName', 'address', 'ranking'];
        $sql = "SELECT id,name,photo,userId,zi,zpname,birthday,remark,is_dead as isDead,dead_time as deadTime,phone,qq,country,country_name as countryName,province,province_name as provinceName,city,city_name as cityName,area,area_name as areaName,town,town_name as townName,address,ranking, gender,father,mother,sister,brother,spouse,son,daughter FROM $this->TABLE_PERSON WHERE id = '$personId' AND is_delete = '0' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return null;
        } else {
            $person = new Person($result);
            $parser = new RelationParser($person);
            $result['father'] = $parser->getFathers();
            $result['mother'] = $parser->getMothers();
            $result['sister'] = $parser->getSisters();
            $result['brother'] = $parser->getBrothers();
            $result['spouse'] = $parser->getSpouses();
            $result['son'] = $parser->getSons();
            $result['daughter'] = $parser->getDaughters();

            return $result;
        }
    }

    /**
     * 根据人物id数组获取人物数组，这个方法只提供给家族和家庭绑定时使用
     * 如果本地有缓存，会直接从本地获取
     * @param $personIds 人物id数组
     * @param $familyPersonMap 本地的缓存
     * @return array 人物数组
     */
    private function getPersonByIds($personIds, &$familyPersonMap)
    {

        $noCachePersonIds = [];
        $cachedResult = [];

        // 先找出本地没有缓存的id
        foreach ($personIds as $personId) {
            if (isset($familyPersonMap[$personId])) {
                $cachedResult[] = $familyPersonMap[$personId];
            } else {
                $noCachePersonIds[] = $personId;
            }
        }

        if (count($noCachePersonIds) == 0) {
            return $cachedResult;
        }

        $personIdStr = '(' . Util::arrayToString($noCachePersonIds) . ')';

        $sql = "SELECT id,name,photo,userId,zi,zpname,birthday,remark,is_dead as isDead,dead_time as deadTime,phone,qq,country,country_name as countryName,province,province_name as provinceName,city,city_name as cityName,area,area_name as areaName,town,town_name as townName,address,ranking, gender,father,mother,sister,brother,spouse,son,daughter FROM $this->TABLE_PERSON WHERE id in $personIdStr AND is_delete = '0' ";

        $result = $this->pdo->query($sql);

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['sister'] = $parser->getSisters();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();

            // 把查询出来的结果存入缓存中
            $familyPersonMap[$result[$i]['id']] = $result[$i];
        }

        $result = array_merge($cachedResult, $result);

        return $result;
    }

    /**
     * 一个人在一个人物数组中找到最匹配的
     * 会过滤已经匹配过的人物
     * @param $person 待匹配的人物
     * @param $matchPersons 备选的人物数组
     * @return map|null 匹配到的人物，或者null
     */
    public function getMostMatchPerson($person, $matchPersons, $reversePair)
    {
        foreach ($matchPersons as $matchPerson) {
            if ($person['name'] == $matchPerson['name']) {
                // 匹配到的人物
                if (!isset($reversePair[$matchPerson['id']])) {
                    // 这个人物没有使用过
                    return $matchPerson;
                }
            }
        }

        return null;
    }

    /**
     * 构建虚拟人物
     * @param $templatePerson  模板人物
     * @param $corePersonId    核心人物id
     * @param $relation        关系
     * @return map 人物
     */
    private function constructVirtualPerson($templatePerson, $corePersonId, $gender, $relation)
    {
        $newPerson = $templatePerson;
        // 清空所有的关系
        $newPerson['father'] = [];
        $newPerson['mother'] = [];
        $newPerson['spouse'] = [];
        $newPerson['sister'] = [];
        $newPerson['brother'] = [];
        $newPerson['son'] = [];
        $newPerson['daughter'] = [];

        // 只保留要记录下来的关系
        switch ($relation) {
            case '1':
                if ($gender == '0') {
                    $newPerson['daughter'] = [$corePersonId];
                } else {
                    $newPerson['son'] = [$corePersonId];
                }

                break;
            case '2':
                if ($gender == '0') {
                    $newPerson['daughter'] = [$corePersonId];
                } else {
                    $newPerson['son'] = [$corePersonId];
                }

                break;
            case '3':   // 兄弟
                if ($gender == '0') {
                    $newPerson['sister'] = [$corePersonId];
                } else {
                    $newPerson['brother'] = [$corePersonId];
                }
                break;
            case '4':   // 姐妹
                if ($gender == '0') {
                    $newPerson['sister'] = [$corePersonId];
                } else {
                    $newPerson['brother'] = [$corePersonId];
                }
                break;
            case '5':   // 配偶
                if ($gender == '0') {
                    $newPerson['spouse'] = [$corePersonId];
                } else {
                    $newPerson['spouse'] = [$corePersonId];
                }
                break;
            case '6':   // 儿子
                if ($gender == '0') {
                    $newPerson['mother'] = [$corePersonId];
                } else {
                    $newPerson['father'] = [$corePersonId];
                }
                break;
            case '7':   // 女儿
                if ($gender == '0') {
                    $newPerson['mother'] = [$corePersonId];
                } else {
                    $newPerson['father'] = [$corePersonId];
                }
                break;
        }

        $newPerson['isNew'] = true;
        $newPerson['id'] = Util::generateRandomCode(24);

        return $newPerson;
    }

    /**
     * 根据选择的人物绑定，推荐出所有的人物关系绑定
     * @param $familyId 家族id
     * @param $miniFamilyId 家庭id
     * @param $familyPersonId 家族人物id
     * @param $miniFamilyPersonId 家庭人物id
     * @return array|bool 推荐的绑定关系. 如果返回false，则表示因为数据异常（出现不合理的数据）导致无法进行匹配
     */
    public function recommendPersonBindByPersonId($miniFamilyId, $familyPersonId, $miniFamilyPersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 第一步，先获取家庭中需要绑定的人物(排除外戚)
        $miniFamilyPersons = $this->getPersonDirectlyRelatedFamilyMembers($miniFamilyId, $miniFamilyPersonId, true);


        $familyPersonMap = [];
        $miniFamilyPersonMap = [];
        // 将人物id和人物映射起来
        foreach ($miniFamilyPersons as $person) {
            $miniFamilyPersonMap[$person['id']] = $person;
        }

        // 然后根据已确定配对的点，向四周发散。这是一个递归的过程。
        $pair = [];             // pair记录已经配对的点，配对规则是从家庭到家族的映射
        $reversePair = [];      // reversePair是逆映射，主要负责检查匹配对象是否已经匹配过。
        $pair[$miniFamilyPersonId] = $familyPersonId;
        $reversePair[$familyPersonId] = $miniFamilyPersonId;

        $needMatchPersons = [$miniFamilyPersonMap[$miniFamilyPersonId]]; // 需要发散匹配的点

        while (count($needMatchPersons) > 0) {
            $person = array_pop($needMatchPersons);

            // 获取该人物的匹配对象
            $matchPersonId = $pair[$person['id']];
            $matchPerson = null;    // 对应已确定的匹配对象

            // 查找familyPersonMap中有没有
            if (isset($familyPersonMap[$matchPersonId])) {
                $matchPerson = $familyPersonMap[$matchPersonId];
            } else {
                // 从数据库中查询这个人物
                $matchPerson = $this->getPersonById($matchPersonId);

                // 缓存下来
                $familyPersonMap[$matchPersonId] = $matchPerson;

                if ($matchPerson == null) {
                    return false;   // 正常情况下不会出现null;
                }
            }

            $loops = [
                ['r' => 'father', 'v' => '1'],
                ['r' => 'mother', 'v' => '2'],
                ['r' => 'brother', 'v' => '3'],
                ['r' => 'sister', 'v' => '4'],
                ['r' => 'spouse', 'v' => '5'],
                ['r' => 'son', 'v' => '6'],
                ['r' => 'daughter', 'v' => '7'],
            ];


            // 比较person和matchPerson的父亲，形成新的匹配
            // 比较person和matchPerson的母亲，形成新的匹配
            // 比较person和matchPerson的兄弟，形成新的匹配
            // 比较person和matchPerson的姐妹，形成新的匹配
            // 比较persom和matchPerson的儿子，形成新的匹配
            // 比较person和matchPerson的女儿，形成新的匹配
            // 比较person和matchPerson的配偶，形成新的匹配
            foreach ($loops as $loop) {
                $key = $loop['r'];
                $rv = $loop['v'];
                foreach ($person[$key] as $personId) {
                    // 检查personId是否匹配过了
                    if (isset($pair[$personId])) {
                        continue;
                    } else {
                        // 进行匹配
                        if (count($matchPerson[$key]) > 0) {
                            $findPersons = $this->getPersonByIds($matchPerson[$key], $familyPersonMap);
                            $tmpMatchPerson = $this->getMostMatchPerson($miniFamilyPersonMap[$personId], $findPersons, $reversePair);

                            if ($tmpMatchPerson != null) {
                                $pair[$personId] = $tmpMatchPerson['id'];
                                $reversePair[$tmpMatchPerson['id']] = $personId;
                            } else {
                                // 这个人匹配不到，代表需要新增
                                // 这里自动生成一个虚拟的人物来对应（这个虚拟人物会保存所有需要新增的数据）
                                $newPerson = $this->constructVirtualPerson($miniFamilyPersonMap[$personId], $matchPerson['id'], $matchPerson['gender'], $rv);

                                // 使用这个虚拟人物进行匹配
                                $pair[$personId] = $newPerson['id'];
                                $reversePair[$newPerson['id']] = $personId;

                                $familyPersonMap[$newPerson['id']] = $newPerson;    // 新增加的人物保存到本地缓存中
                            }

                            if ($key != 'spouse' && $miniFamilyPersonMap[$personId]['gender'] == '1') {
                                // 配偶不需要向外延伸, 女性不需要向外延伸
                                $needMatchPersons[] = $miniFamilyPersonMap[$personId];
                            }
                        } else {
                            // 这个匹配人物上没有这个关系，构建一个虚拟人物
                            $newPerson = $this->constructVirtualPerson($miniFamilyPersonMap[$personId], $matchPerson['id'], $matchPerson['gender'], $rv);

                            // 使用这个虚拟人物进行匹配
                            $pair[$personId] = $newPerson['id'];
                            $reversePair[$newPerson['id']] = $personId;

                            $familyPersonMap[$newPerson['id']] = $newPerson;    // 新增加的人物保存到本地缓存中

                            if ($key != 'spouse' && $miniFamilyPersonMap[$personId]['gender'] == '1') {
                                // 配偶不需要向外延伸, 女性不需要向外延伸
                                $needMatchPersons[] = $miniFamilyPersonMap[$personId];
                            }
                        }
                    }
                }
            }
        }

        // 根据pair配对表，获取所有需要的人物信息
        $allPersonMap = [];
        foreach ($pair as $miniFamilyPersonId => $familyPersonId) {

            $miniFamilyPerson = $miniFamilyPersonMap[$miniFamilyPersonId];
            $miniFamilyPerson['isNew'] = false;
            $allPersonMap[$miniFamilyPersonId] = $miniFamilyPerson;

            $familyPerson = $familyPersonMap[$familyPersonId];
            if (!isset($familyPerson['isNew'])) {
                $familyPerson['isNew'] = false;
            }
            $allPersonMap[$familyPersonId] = $familyPerson;
        }

        return [$pair, $allPersonMap];
    }

    /**
     * 添加家庭绑定生成的记录
     * @param MiniFamilyBindRecommend $miniFamilyBindRecommend MiniFamilyBindRecommend对象
     * @return int 生成的记录id
     */
    public function addMiniFamilyBindRecommend(MiniFamilyBindRecommend $miniFamilyBindRecommend)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 保存这次推荐的结果
        $sql = "INSERT INTO gener_minifamily_bind_recommend 
                (applyId, familyId, miniFamilyId, familyPersonId, miniFamilyPersonId, recommendData, recommendType, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$miniFamilyBindRecommend->applyId', '$miniFamilyBindRecommend->familyId', '$miniFamilyBindRecommend->miniFamilyId', '$miniFamilyBindRecommend->familyPersonId', '$miniFamilyBindRecommend->miniFamilyPersonId', '$miniFamilyBindRecommend->recommendData', 
                '$miniFamilyBindRecommend->recommendType','$miniFamilyBindRecommend->createBy', now(), '$miniFamilyBindRecommend->updateBy', now() )";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取家庭绑定的推荐详情
     * @param int $recommendId 推荐id
     * @return MiniFamilyBindRecommend 推荐对象
     */
    public function getMiniFamilyBindRecommend($recommendId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT * FROM gener_minifamily_bind_recommend WHERE id = '$recommendId' AND isDelete = '0' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return null;
        } else {
            return new MiniFamilyBindRecommend($result);
        }
    }

    /**
     * 获取人物的相关联人物id, 提供给addMiniFamilyBindRecord使用
     * @param $person 人物
     * @return int 相关的人物id
     */
    private function getPersonRelatedId($person)
    {

        $keys = ['father', 'mother', 'spouse', 'sister', 'brother', 'son', 'daughter'];

        foreach ($keys as $key) {
            if (count($person[$key]) > 0) {
                return [$person[$key][0], $key];
            }
        }
    }

    /**
     * 更新人物的依赖id, 提供给addMiniFamilyBindRecord使用
     * @param $person 要更新的人物
     * @param $newRelatedId  新的依赖id
     * @return bool true 更新成功，false 更新失败
     */
    private function updatePersonRelatedId(&$person, $newRelatedId)
    {
        $keys = ['father', 'mother', 'spouse', 'sister', 'brother', 'son', 'daughter'];

        foreach ($keys as $key) {
            if (count($person[$key]) > 0) {
                $person[$key][0] = $newRelatedId;
                return true;
            }
        }

        return false;
    }

    /**
     * 添加新的人物， 提供给addMiniFamilyBindRecord使用
     */
    private function addNewPerson($newPerson, $familyId)
    {
        $person = new Person();
        $person->id = Util::getNextId();
        $person->country = 0;
        $person->countryName = 0;
        $person->province = 0;
        $person->provinceName = 0;
        $person->city = 0;
        $person->cityName = 0;
        $person->area = 0;
        $person->areaName = 0;
        $person->town = 0;
        $person->townName = 0;
        $person->address = $newPerson['address'];
        $person->name = $newPerson['name'];
        $person->birthday = $newPerson['birthday'];
        $person->gender = $newPerson['gender'];
        $person->photo = $newPerson['photo'];
        $person->familyId = $familyId;
        $person->branchId = 0;
        $person->zi = '';
        $person->remark = $newPerson['remark'];
        $person->isDead = $newPerson['isDead'];
        $person->deadTime = $newPerson['deadTime'];
        $person->zpname = '';
        $person->ranking = $newPerson['ranking'];
        $person->profileText = '';
        $person->sideText = '';

        list($relatedId, $relation) = $this->getPersonRelatedId($newPerson);

        $relationNumber = 1;
        switch ($relation) {
            case 'father':
                if ($newPerson['gender'] == '0') {
                    $relationNumber = 7;
                } else {
                    $relationNumber = 6;
                }
                break;
            case 'mother':
                if ($newPerson['gender'] == '0') {
                    $relationNumber = 7;
                } else {
                    $relationNumber = 6;
                }
                break;
            case 'sister':
                if ($newPerson['gender'] == '0') {
                    $relationNumber = 4;
                } else {
                    $relationNumber = 3;
                }
                break;
            case 'brother':
                if ($newPerson['gender'] == '0') {
                    $relationNumber = 4;
                } else {
                    $relationNumber = 3;
                }
                break;
            case 'spouse':
                $relationNumber = 5;
                break;
            case 'son':
                if ($newPerson['gender'] == '0') {
                    $relationNumber = 2;
                } else {
                    $relationNumber = 1;
                }
                break;
            case 'daughter':
                if ($newPerson['gender'] == '0') {
                    $relationNumber = 2;
                } else {
                    $relationNumber = 1;
                }
                break;
        }
        $person->corePersonId = $relatedId;
        $person->relation = $relationNumber;

        $personDB = new CDBPerson();
        $newPersonId = $personDB->addRelatePerson($person, false);
        return $newPersonId;
    }

    /**
     * 添加家庭到家族绑定的具体记录
     * @param $familyId 家族的id
     * @param $recommendData 系统推荐的绑定映射
     * @param $userId   用户的id
     * @return bool true成功，false失败
     */
    public function addMiniFamilyBindRecord($familyId, $miniFamilyId, $familyPersonId, $miniFamilyPersonId, $recommendData, $applyId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $personMap = $recommendData['persons'];
        $pair = $recommendData['pair'];

        $newPersons = [];
        $newPersonRelated = [];

        // 1.先把当前可增加的人物找出来
        // 2.同时生成 人物增加的对应关系, 即人物B依赖人物A时 ，A
        foreach ($personMap as $personId => $person) {
            if ($person['isNew']) {
                // 这个人物需要新增
                // 判断这个人当前是否可以新增
                list($relatedId, $relation) = $this->getPersonRelatedId($person);
                // 判断依赖的id的人物是否存在
                if (!$personMap[$relatedId]['isNew']) {
                    $newPersons[] = $person;
                } else {
                    // 依赖的人物还没有创建，因此不可以新增
                    if (isset($newPersonRelated[$relatedId])) {
                        $newPersonRelated[$relatedId][] = $person;
                    } else {
                        $newPersonRelated[$relatedId] = [$person];
                    }
                }
            }
        }

        try {
            $this->pdo->beginTransaction();

            // 添加rootRecord
            $sql = "INSERT INTO $this->TABLE_MINIFAMILY_BIND_ROOT_RECORD 
                    (familyId, miniFamilyId, familyPersonId, miniFamilyPersonId, createBy, createTime, updateBy, updateTime)
                    VALUES
                    ('$familyId', '$miniFamilyId', '$familyPersonId', '$miniFamilyPersonId', '$userId', now(), '$userId', now() )";

            $this->pdo->insert($sql);

            while (count($newPersons) > 0) {
                $newPerson = array_pop($newPersons);

                $virtualPersonId = $newPerson['id'];
                $newPersonId = $this->addNewPerson($newPerson, $familyId);

                // $virtualPersonId对应的人物都可以被新增了
                // $personMap的id对应人物也需要修改
                // $pair的对应关系也需要修改
                if (isset($newPersonRelated[$virtualPersonId])) {
                    // 取出需要新增的人物
                    $tmpPersons = $newPersonRelated[$virtualPersonId];

                    foreach ($tmpPersons as $key => $value) {
                        // 将这些人物的依赖更新为新的id：$newPersonId
                        if (!$this->updatePersonRelatedId($tmpPersons[$key], $newPersonId)) {
                            return false;
                        } else {
                            array_push($newPersons, $tmpPersons[$key]);
                        }
                    }
                }

                // 修改pair的对应关系
                foreach ($pair as $miniFamilyPersonId => $familyPersonId) {
                    if ($familyPersonId == $virtualPersonId) {
                        $pair[$miniFamilyPersonId] = $newPersonId;
                        break;
                    }
                }

            }

            if (count($pair) > 0) {

                $sql = "INSERT INTO $this->TABLE_MINIFAMILY_BIND_RECORD 
                (familyId, miniFamilyId, familyPersonId, miniFamilyPersonId, createBy, createTime, updateBy, updateTime)VALUES";
                // 根据pair的内容，进行关系的绑定
                $isFirst = true;
                foreach ($pair as $miniFamilyPersonId => $familyPersonId) {
                    if ($isFirst) {
                        $sql .= "('$familyId', '$miniFamilyId', '$familyPersonId', '$miniFamilyPersonId', '$userId', now(), '$userId', now() )";
                        $isFirst = false;
                    } else {
                        $sql .= ",('$familyId', '$miniFamilyId', '$familyPersonId', '$miniFamilyPersonId', '$userId', now(), '$userId', now() )";
                    }
                }

                $this->pdo->insert($sql);
            }

            // 这里要更新申请的接收状态
            $sql = "UPDATE $this->TABLE_MINIFAMILY_BIND_APPLY SET isAccept = '1', updateTime = now(), updateBy = '$userId' WHERE id = '$applyId' AND isDelete = '0' ";

            $this->pdo->update($sql);

            $this->pdo->commit();

            return true;
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
        }
    }

    public function getBindRootRecordById($recordId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, familyId, miniFamilyId, familyPersonId, miniFamilyPersonId, createBy, createTime, updateBy, updateTime FROM $this->TABLE_MINIFAMILY_BIND_ROOT_RECORD WHERE id = '$recordId' AND isDelete = '0' ";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取家族的所有的绑定记录, 并且会查询出详细信息
     * @param $familyId 家族id
     * @param $maxId    最大id
     * @param $sinceId 起始Id
     * @return array 家族中绑定的家庭
     */
    public function getAllFamilyBindRecordsPaging($familyId, $maxId, $sinceId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $where = '';
        if ($maxId > 0) {
            $where = " AND tmbrr.id < $maxId ";
        }

        if ($sinceId > 0) {
            $where .= " AND tmbrr.id > $sinceId ";
        }

        $sql = "SELECT tmbrr.id, tmbrr.miniFamilyId, tmbrr.familyId, tmbrr.familyPersonId, tb.name as miniFamilyName, tb.photo as miniFamilyPhoto, tp1.name as familyPersonName, tp1.photo as familyPersonPhoto, tp2.name as miniFamilyPersonName, tp2.photo as miniFamilyPersonPhoto, tmbrr.miniFamilyPersonId, tmbrr.createBy, tmbrr.createTime, tmbrr.updateBy, tmbrr.updateTime 
                FROM $this->TABLE_MINIFAMILY_BIND_ROOT_RECORD tmbrr 
                LEFT JOIN $this->TABLE tb on tb.id = tmbrr.miniFamilyId 
                LEFT JOIN $this->TABLE_PERSON tp1 on tp1.id = tmbrr.familyPersonId 
                LEFT JOIN $this->TABLE_PERSON tp2 on tp2.id = tmbrr.miniFamilyPersonId 
                WHERE tmbrr.familyId = '$familyId' AND tmbrr.isDelete = '0' $where ORDER BY tmbrr.id desc";

        return $this->pdo->query($sql);
    }

    /**
     * 获取家庭中所有的绑定的家族，并且会查询出详细的信息
     */
    public function getAllMiniFamilyBindRecords($miniFamilyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tmbrr.id, tmbrr.miniFamilyId, tmbrr.familyId, tmbrr.familyPersonId,tb.name as familyName, tb.photo as familyPhoto, tp1.name as familyPersonName, tp1.photo as familyPersonPhoto, tp2.name as miniFamilyPersonName, tp2.photo as miniFamilyPersonPhoto,  tmbrr.miniFamilyPersonId, tmbrr.createBy, tmbrr.createTime, tmbrr.updateBy, tmbrr.updateTime 
                FROM $this->TABLE_MINIFAMILY_BIND_ROOT_RECORD tmbrr 
                LEFT JOIN $this->TABLE tb on tb.id = tmbrr.familyId 
                LEFT JOIN $this->TABLE_PERSON tp1 on tp1.id = tmbrr.familyPersonId 
                LEFT JOIN $this->TABLE_PERSON tp2 on tp2.id = tmbrr.miniFamilyPersonId 
                WHERE tmbrr.miniFamilyId = '$miniFamilyId' AND tmbrr.isDelete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 获取家庭绑定的根节点记录
     * @param $familyId     家族id
     * @param $miniFamilyId 家庭id
     * @return map 家庭和家族的绑定记录
     */
    private function getMiniFamilyBindRootRecord($familyId, $miniFamilyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, familyId, miniFamilyId, familyPersonId, miniFamilyPersonId, createBy, createTime, updateBy, updateTime FROM $this->TABLE_MINIFAMILY_BIND_ROOT_RECORD WHERE familyId = '$familyId' AND miniFamilyId = '$miniFamilyId' AND isDelete = '0' ";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取家庭的绑定对，提供给getRecommendUpdateInfoFromMiniFamilyToFamily调用
     * @param $familyId 家族id
     * @param $miniFamilyId 家庭id
     * @return array 配对的记录数组
     */
    private function getMiniFamilyBindPairs($familyId, $miniFamilyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,familyId,miniFamilyId,familyPersonId,miniFamilyPersonId,createBy,createTime,updateBy,updateTime FROM $this->TABLE_MINIFAMILY_BIND_RECORD WHERE familyId = '$familyId' AND miniFamilyId = '$miniFamilyId' AND isDelete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 对比人物, 然后生成不同之处
     */
    private function comparePerson($personA, $personB)
    {


        // 需要对比的字段
        $needCompareParams = ['userId', 'name', 'zi', 'zpname', 'birthday', 'photo', 'remark', 'isDead', 'deadTime', 'phone', 'qq', 'country', 'countryName', 'province', 'provinceName', 'city', 'cityName', 'area', 'areaName', 'town', 'townName', 'address', 'ranking'];

        $differences = [];

        foreach ($needCompareParams as $key) {
            if ($personA[$key] != $personB[$key]) {
                $differences[$key] = [
                    'src' => $personB[$key],
                    'dst' => $personA[$key]
                ];
            }
        }

        return $differences;
    }

    /**
     * 从家庭向家族的更新信息推荐
     * 主要作用就是会计算出要更新的数据，要添加的数据等等
     * 家庭的某些数据一般是没有的，比如sideText,profileText，这些数据应该不作为对比项
     * @param $miniFamilyId 家庭id
     * @param $familyId     家族id
     * @return map 所有的diff map | 出现异常则返回false
     */
    public function getRecommendUpdateInfoFromMiniFamilyToFamily($miniFamilyId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 从绑定的root记录中找出绑定点
        $rootRecord = $this->getMiniFamilyBindRootRecord($familyId, $miniFamilyId);

        if ($rootRecord == null) {
            return false;
        }

        $familyPersonId = $rootRecord['familyPersonId'];
        $miniFamilyPersonId = $rootRecord['miniFamilyPersonId'];

        // 从家庭中取出所有需要检查的人物
        $miniFamilyPersons = $this->getPersonDirectlyRelatedFamilyMembers($miniFamilyId, $miniFamilyPersonId, true);

        // var_dump($miniFamilyPersons);
        // exit;

        $familyPersonMap = [];
        $miniFamilyPersonMap = [];
        foreach ($miniFamilyPersons as $person) {
            $miniFamilyPersonMap[$person['id']] = $person;
        }

        // 从数据库查询已经配对的人物
        $pairRecords = $this->getMiniFamilyBindPairs($familyId, $miniFamilyId);
        $pair = []; // $pair中保存的是已经配对的点,配对规则是从家庭到家族的映射
        $reversePair = [];  // reversePair是逆映射， 主要负责检查匹配对象是否已经匹配过。
        $needMatchPersons = []; // 需要发散的点的集合

        foreach ($pairRecords as $pairRecord) {
            $pair[$pairRecord['miniFamilyPersonId']] = $pairRecord['familyPersonId'];

            $reversePair[$pairRecord['familyPersonId']] = $pairRecord['miniFamilyPersonId'];

            $needMatchPersons[] = $miniFamilyPersonMap[$pairRecord['miniFamilyPersonId']];
        }

        while (count($needMatchPersons) > 0) {
            $person = array_pop($needMatchPersons);

            // 获取该人物的匹配对象
            $matchPersonId = $pair[$person['id']];
            $matchPerson = null;    // 对应已确定的匹配对象

            // 查找familyPersonMap中有没有
            if (isset($familyPersonMap[$matchPersonId])) {
                $matchPerson = $familyPersonMap[$matchPersonId];
            } else {
                // 从数据库中查询这个人物
                $matchPerson = $this->getPersonById($matchPersonId);

                // 缓存下来
                $familyPersonMap[$matchPersonId] = $matchPerson;

                if ($matchPerson == null) {
                    return false;   // 正常情况下不会出现null;
                }
            }

            $loops = [
                ['r' => 'father', 'v' => '1'],
                ['r' => 'mother', 'v' => '2'],
                ['r' => 'brother', 'v' => '3'],
                ['r' => 'sister', 'v' => '4'],
                ['r' => 'spouse', 'v' => '5'],
                ['r' => 'son', 'v' => '6'],
                ['r' => 'daughter', 'v' => '7'],
            ];


            // 比较person和matchPerson的父亲，形成新的匹配
            // 比较person和matchPerson的母亲，形成新的匹配
            // 比较person和matchPerson的兄弟，形成新的匹配
            // 比较person和matchPerson的姐妹，形成新的匹配
            // 比较persom和matchPerson的儿子，形成新的匹配
            // 比较person和matchPerson的女儿，形成新的匹配
            // 比较person和matchPerson的配偶，形成新的匹配
            foreach ($loops as $loop) {
                $key = $loop['r'];
                $rv = $loop['v'];
                foreach ($person[$key] as $personId) {
                    // 检查personId是否匹配过了
                    if (isset($pair[$personId])) {
                        continue;
                    } else {
                        // 进行匹配
                        if (count($matchPerson[$key]) > 0) {
                            $findPersons = $this->getPersonByIds($matchPerson[$key], $familyPersonMap);
                            $tmpMatchPerson = $this->getMostMatchPerson($miniFamilyPersonMap[$personId], $findPersons, $reversePair);

                            if ($tmpMatchPerson != null) {
                                $pair[$personId] = $tmpMatchPerson['id'];
                                $reversePair[$tmpMatchPerson['id']] = $personId;
                            } else {
                                // 这个人匹配不到，代表需要新增
                                // 这里自动生成一个虚拟的人物来对应（这个虚拟人物会保存所有需要新增的数据）
                                $newPerson = $this->constructVirtualPerson($miniFamilyPersonMap[$personId], $matchPerson['id'], $matchPerson['gender'], $rv);

                                // 使用这个虚拟人物进行匹配
                                $pair[$personId] = $newPerson['id'];
                                $reversePair[$newPerson['id']] = $personId;

                                $familyPersonMap[$newPerson['id']] = $newPerson;    // 新增加的人物保存到本地缓存中
                            }

                            if ($key != 'spouse' && $miniFamilyPersonMap[$personId]['gender'] == '1') {
                                // 配偶不需要向外延伸, 女性不需要向外延伸
                                $needMatchPersons[] = $miniFamilyPersonMap[$personId];
                            }
                        } else {
                            // 这个匹配人物上没有这个关系，构建一个虚拟人物
                            $newPerson = $this->constructVirtualPerson($miniFamilyPersonMap[$personId], $matchPerson['id'], $matchPerson['gender'], $rv);

                            // 使用这个虚拟人物进行匹配
                            $pair[$personId] = $newPerson['id'];
                            $reversePair[$newPerson['id']] = $personId;

                            $familyPersonMap[$newPerson['id']] = $newPerson;    // 新增加的人物保存到本地缓存中

                            if ($key != 'spouse' && $miniFamilyPersonMap[$personId]['gender'] == '1') {
                                // 配偶不需要向外延伸, 女性不需要向外延伸
                                $needMatchPersons[] = $miniFamilyPersonMap[$personId];
                            }
                        }
                    }
                }
            }
        }

        $result = [];
        $result['pair'] = $pair;
        $allPersonMap = [];

        $diff = [];
        // 这里还要对比那些已经绑定的人物的资料更新情况
        foreach ($pair as $miniFamilyPersonId => $familyPersonId) {
            $miniFamilyPerson = $miniFamilyPersonMap[$miniFamilyPersonId];
            $miniFamilyPerson['isNew'] = false;
            $allPersonMap[$miniFamilyPersonId] = $miniFamilyPerson;

            $familyPerson = $familyPersonMap[$familyPersonId];
            if (!isset($familyPerson['isNew'])) {
                $familyPerson['isNew'] = false;
            }
            $allPersonMap[$familyPersonId] = $familyPerson;

            $diff[$miniFamilyPersonId . 'vs' . $familyPersonId] = $this->comparePerson($miniFamilyPerson, $familyPerson);

        }

        $result['persons'] = $allPersonMap;
        $result['diff'] = $diff;

        return $result;
    }

    /**
     * 更新人物的不同之处
     * @param $personId 人物id
     * @param $diffs 不同之处, diffs必须大于0
     * @return int 更新的记录数
     */
    private function updatePersonDifference($personId, $diffs)
    {

        // $needCompareParams = ['userId', 'name', 'zi', 'zpname', 'birthday', 'photo', 'remark', 'isDead', 'deadTime', 'phone', 'qq', 'country', 'countryName', 'province', 'provinceName', 'city', 'cityName', 'area', 'areaName', 'town', 'townName', 'address', 'ranking'];

        $keyMap = [
            'userId' => 'userId',
            'name' => 'name',
            'zi' => 'zi',
            'zpname' => 'zpname',
            'birthday' => 'birthday',
            'photo' => 'photo',
            'remark' => 'remark',
            'isDead' => 'is_dead',
            'deadTime' => 'dead_time',
            'phone' => 'phone',
            'qq' => 'qq',
            'country' => 'country',
            'countryName' => 'country_name',
            'province' => 'province',
            'provinceName' => 'province_name',
            'city' => 'city',
            'cityName' => 'city_name',
            'area' => 'area',
            'areaName' => 'area_name',
            'town' => 'town',
            'townName' => 'town_name',
            'address' => 'address',
            'ranking' => 'ranking'
        ];

        $updates = '';
        $isFirst = true;

        foreach ($diffs as $key => $diff) {
            if ($isFirst) {
                $updates = $keyMap[$key] . " = '" . $diff['dst'] . "' ";
                $isFirst = false;
            } else {
                $updates .= "," . $keyMap[$key] . " = '" . $diff['dst'] . "' ";
            }
        }


        $sql = "UPDATE $this->TABLE_PERSON SET $updates WHERE id = '$personId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 从家庭中向家族更新信息
     * @param MiniFamilyBindRecommend $recommend
     * @return bool 更新成功还是失败
     */
    public function updateInfoFromMiniFamilyToFamily($applyId, MiniFamilyBindRecommend $recommend, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $miniFamilyId = $recommend->miniFamilyId;
        $familyId = $recommend->familyId;

        // $recommendData 中会记录新增的人物，以及要修改的diff
        $recommendData = json_decode($recommend->recommendData, true);

        $personMap = $recommendData['persons'];
        $pair = $recommendData['pair'];
        $diffs = $recommendData['diff'];

        $newPersons = [];
        $newPersonRelated = [];

        // 这里的pair包含了已经存储了对应关系的数据，因此需要去除这些pair
        $sql = "SELECT familyPersonId, miniFamilyPersonId FROM $this->TABLE_MINIFAMILY_BIND_RECORD WHERE miniFamilyId = '$miniFamilyId' AND familyId = '$familyId' AND isDelete = '0' ";
        $pairResult = $this->pdo->query($sql);

        foreach ($pairResult as $item) {
            unset($pair[$item['miniFamilyPersonId']]);
        }

        // 1.先把当前可增加的人物找出来
        // 2.同时生成 人物增加的对应关系, 即人物B依赖人物A时 ，A
        foreach ($personMap as $personId => $person) {
            if ($person['isNew']) {
                // 这个人物需要新增
                // 判断这个人当前是否可以新增
                list($relatedId, $relation) = $this->getPersonRelatedId($person);
                // 判断依赖的id的人物是否存在
                if (!$personMap[$relatedId]['isNew']) {
                    $newPersons[] = $person;
                } else {
                    // 依赖的人物还没有创建，因此不可以新增
                    if (isset($newPersonRelated[$relatedId])) {
                        $newPersonRelated[$relatedId][] = $person;
                    } else {
                        $newPersonRelated[$relatedId] = [$person];
                    }
                }
            }
        }

        try {
            $this->pdo->beginTransaction();
            while (count($newPersons) > 0) {
                $newPerson = array_pop($newPersons);

                $virtualPersonId = $newPerson['id'];
                $newPersonId = $this->addNewPerson($newPerson, $familyId);

                // $virtualPersonId对应的人物都可以被新增了
                // $personMap的id对应人物也需要修改
                // $pair的对应关系也需要修改
                if (isset($newPersonRelated[$virtualPersonId])) {
                    // 取出需要新增的人物
                    $tmpPersons = $newPersonRelated[$virtualPersonId];

                    foreach ($tmpPersons as $key => $value) {
                        // 将这些人物的依赖更新为新的id：$newPersonId
                        if (!$this->updatePersonRelatedId($tmpPersons[$key], $newPersonId)) {
                            return false;
                        } else {
                            array_push($newPersons, $tmpPersons[$key]);
                        }
                    }
                }

                // 修改pair的对应关系
                foreach ($pair as $miniFamilyPersonId => $familyPersonId) {
                    if ($familyPersonId == $virtualPersonId) {
                        $pair[$miniFamilyPersonId] = $newPersonId;
                        break;
                    }
                }

            }

            if (count($pair) > 0) {

                $sql = "INSERT INTO $this->TABLE_MINIFAMILY_BIND_RECORD 
                (familyId, miniFamilyId, familyPersonId, miniFamilyPersonId, createBy, createTime, updateBy, updateTime)VALUES";
                // 根据pair的内容，进行关系的绑定
                $isFirst = true;
                foreach ($pair as $miniFamilyPersonId => $familyPersonId) {
                    if ($isFirst) {
                        $sql .= "('$familyId', '$miniFamilyId', '$familyPersonId', '$miniFamilyPersonId', '$userId', now(), '$userId', now() )";
                        $isFirst = false;
                    } else {
                        $sql .= ",('$familyId', '$miniFamilyId', '$familyPersonId', '$miniFamilyPersonId', '$userId', now(), '$userId', now() )";
                    }
                }

                $this->pdo->insert($sql);
            }

            // 这里要找出所有变化的内容，然后进行更新
            foreach ($diffs as $key => $diff) {
                if (count($diff) > 0) {
                    // 有不同之处
                    $personIdArr = explode('vs', $key);
                    $familyPersonId = $personIdArr[1];

                    $this->updatePersonDifference($familyPersonId, $diff);
                }
            }

            // 这里要更新申请的接收状态
            $sql = "UPDATE $this->TABLE_MINIFAMILY_UPDATE_APPLY SET isAccept = '1', updateTime = now(), updateBy = '$userId' WHERE id = '$applyId' AND isDelete = '0' ";

            $this->pdo->update($sql);

            $this->pdo->commit();

            return true;
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return false;
        }

    }

    /**
     * 添加家庭更新的申请
     * @param $apply 申请
     * @return int 家庭更新申请的id
     */
    public function addMiniFamilyUpdateApply(MiniFamilyUpdateApply $apply)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_MINIFAMILY_UPDATE_APPLY 
                (recommendId, remark, familyId, miniFamilyId, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$apply->recommendId', '$apply->remark', '$apply->familyId', '$apply->miniFamilyId',  '$apply->createBy', now(), '$apply->updateBy', now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取家庭更新的申请
     * @param int $applyId 申请id
     * @return MiniFamilyUpdateApply|null 返回申请的对象或者null
     */
    public function getMiniFamilyUpdateApply(int $applyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, recommendId, remark, familyId, miniFamilyId, isAccept, createBy, createTime, updateBy, updateTime FROM $this->TABLE_MINIFAMILY_UPDATE_APPLY WHERE id = '$applyId' AND isDelete = '0' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return null;
        } else {
            $apply = new MiniFamilyUpdateApply($result);
            return $apply;
        }
    }

    /**
     * 拒绝绑定家庭的申请
     * @param int $applyId 申请id
     * @return int 更新的记录数
     */
    public function refuseBindMiniFamilyApply($applyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_MINIFAMILY_BIND_APPLY SET isAccept = '-1' WHERE id = '$applyId' AND isDelete = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 拒绝更新家庭的申请
     * @param int $applyId 申请id
     * @return int 更新的记录数
     */
    public function refuseUpdateMiniFamilyApply($applyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_MINIFAMILY_UPDATE_APPLY SET isAccept = '-1' WHERE id = '$applyId' AND isDelete = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 删除人物绑定记录
     * @param $personId 人物id
     * @param $userId   用户id
     * @return int 删除的记录数
     */
    public function deletePersonBindRecord($personId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录人物日志
        $personDB = new CDBPerson();
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $old_person = $personDB->getPersonById($personId);
        $familyId = $old_person->familyId;
        $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
        $option_detail = '删除人物绑定记录';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['userId' => $userId];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        $sql = "UPDATE $this->TABLE_MINIFAMILY_BIND_RECORD SET isDelete = '1', updateTime = now(), updateBy = '$userId' WHERE familyPersonId = '$personId' OR miniFamilyPersonId = '$personId' ";

        $this->pdo->update($sql);
    }

    /**
     * 获取所有的用户分享绑定的记录
     * @param $familyId 族群id
     * @return array 记录数组
     */
    public function getAllPersonBindUserRecord($familyId)
    {
        $this->init();
        $sql = "SELECT pbur.id,pbur.familyId,pbur.personId,gp.name as personName,pbur.fromUserId,
                u1.nickname as fromUserName,pbur.acceptUserId,u2.nickname as acceptUserName,
                pbur.type,pbur.createTime,pbur.createBy,pbur.updateTime,pbur.updateBy
                FROM gener_person_bind_user_record as pbur LEFT JOIN gener_person as gp ON pbur.personId = gp.id
                LEFT JOIN gener_user as u1 ON pbur.fromUserId = u1.id 
                LEFT JOIN gener_user as u2 ON pbur.acceptUserId = u2.id
                WHERE pbur.isDelete = 0 AND pbur.familyId = '$familyId' ORDER BY pbur.id DESC";

        return $this->pdo->query($sql);
    }

    public function getTotalPersonBindUserRecord($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') as total   FROM gener_person_bind_user_record WHERE familyId = '$familyId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        return $result['total'];

    }

    public function getPersonBindUserRecord($familyId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND pbur.id < '$maxId' AND pbur.id > '$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND pbur.id < '$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND pbur.id > '$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }
        $sql = "SELECT pbur.id,pbur.familyId,pbur.personId,gp.name as personName,pbur.fromUserId,
                u1.nickname as fromUserName,pbur.acceptUserId,u2.nickname as acceptUserName,
                pbur.type,pbur.createTime,pbur.createBy,pbur.updateTime,pbur.updateBy
                FROM gener_person_bind_user_record as pbur LEFT JOIN gener_person as gp ON pbur.personId = gp.id
                LEFT JOIN gener_user as u1 ON pbur.fromUserId = u1.id 
                LEFT JOIN gener_user as u2 ON pbur.acceptUserId = u2.id
                WHERE pbur.isDelete = 0 AND pbur.familyId = '$familyId' $where ORDER BY pbur.id DESC LIMIT $count";

        return $this->pdo->query($sql);
    }

    /**
     * 接受第二种临时用户的加入
     * @param $familyId 族群id
     * @param $socialCircleId 族群对应的圈子id
     * @param $userId   用户id
     * @param $fromUserId 来源用户id
     * @return bool true成功, false失败
     */
    public function acceptTemp2UserJoin($familyId, $socialCircleId, $userId, $fromUserId)
    {
        $this->init();

        $this->pdo->beginTransaction();
        $familyDb = new CDBFamily();
        $tmpFamilyUserId = $familyDb->addFamilyUser($familyId, $userId, 2);
        if ($tmpFamilyUserId <= 0) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加失败');
            $this->pdo->rollback();
            exit;
        }
        // 关注圈子
        $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
        VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
        $followId = $this->pdo->insert($sql);
        if ($followId <= 0) {
            Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
            $this->pdo->rollback();
            exit;
        }
        // 更新圈子关注数量
        $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=follow+1 WHERE id='$socialCircleId' AND is_delete=0";
        $updateRow = $this->pdo->update($sql);
        if ($updateRow <= 0) {
            Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
            $this->pdo->rollback();
            exit;
        }

        $personDB = new CDBPerson();
        if ($fromUserId != 0) {
            $personDB->insertPersonBindUserRecord($familyId, 0, $fromUserId, $userId, 5, $userId);
        }
        $this->pdo->commit();
        return true;
    }

    public function getFamilyIdBySocialCircleId($socialCircleId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->TABLE WHERE socialCircleId = '$socialCircleId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result['id'];
        } else {
            return false;
        }
    }

    /**
     * 同步私有数据库的家族
     * @param family 私有数据库的家族数据
     * @return id 增加的家族id
     */
    public function syncPrivateDBFamily(Family $family, $userId)
    {
        $this->init();

        $familyName = $family->name;
        // 生成identity
        $identity = md5($userId . $familyName . time()) . Util::generateRandomCode(32);

        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            $sql = "INSERT INTO $this->TABLE(name,surname,min_level,max_level,
            permission,originator,description,photo,hx_chatroom,identity,groupType,
            create_by,create_time,update_by,update_time)
            VALUES
            ('$familyName','$family->surname','0','0','0','$userId','$family->desc','$family->familyPhoto',
            '$family->hxChatRoom','$family->identity','$family->groupType','$userId',now(),'$userId',now())";

            $familyId = $this->pdo->insert($sql);   //返回插入行的id或者-1
            if ($familyId < 0) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            //将创始人添加到管理员表
            if (!$this->addAdmin($familyId, $userId)) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败1");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            $personDao = new CDBPerson();
            //向人物表中插入一条人物记录
            $personId = $personDao->addPersonWithNoTransaction($userId, $familyId, $adminName, $adminGender, $address, $birthday, $photo);
            if ($personId < 0) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "新增人物失败");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            //更新人物资料中的家族数量
            $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1 WHERE userId = '$userId' ";
            $this->pdo->update($sql);

            $this->createFamilyInitCallback($familyId);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return [$familyId, $personId];                                //返回家族id
    }

    /**
     * 删除分支
     * 2019-06-20
     * @param $personId 删除的节点id
     * @return bool
     *分支删除分三部 1：断开树; 2 子树设为is_delete = 1; 3 取消用户和人物之间的绑定; 4 记录
     */
    public function deleteBranch($personId, $familyId, $optionUser = 0)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //如果之前删除过，直接调用之前的数据
        //可能需要更多的判断 如上次操作是删除还是恢复


        try {
            //开启事务处理
            $this->pdo->beginTransaction();
            //记录人物日志
            $personDB = new CDBPerson();
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $personDB->getPersonById($personId);
            //$familyId = $old_person->familyId;
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $option_detail = '删除人物及分支';
            $option_user = $GLOBALS['userId'] ?? 0;

            $childs = $this->readChilds($personId, $familyId);
            //如果操作人自己在被删除的分支中，拒绝删除操作
            if (in_array($optionUser, $childs['userIds'])) {
                $this->pdo->rollback();
                return ['error_code' => $GLOBALS['ERROR_PERMISSION'], 'error_msg' => '自己在分支中，不能删除'];
            }
            //如果创始人在被删除的分支中，拒绝删除操作
            $originator = $this->getFamilyOriginatorId($familyId);
            if (in_array($originator, $childs['userIds'])) {
                $this->pdo->rollback();
                return ['error_code' => $GLOBALS['ERROR_PERMISSION'], 'error_msg' => '家族创始人在分支中，不能删除'];
            }

            //step one
            $res = $this->cutBranchTree($personId); //json关系
            if ($res['code'] == false) {
                $this->pdo->rollback();            //事务回滚
                $msg = $res['msg'];
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => $msg];
            }
            //step two
            if (!$this->setDeleteBranch($childs, 1)) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '删除分支失败'];
            }
            //step three 取消用户和人物之间的绑定
            if (!empty($childs['userIds'])) {
                $personDao = new CDBPerson();
                foreach ($childs['userIds'] as $key => $val) {
                    $personDao->cancelUserBindForTmp($key, $childs['familyId'], $val, false);
                }
            }
            //step four
            $insertId = $this->recordBranch($personId, json_encode($childs), $res['msg'], $childs['familyId'], $optionUser, 'delete');
            if ($insertId == false) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => '插入日志失败'];
            }

            $other_msg = ['logId' => $insertId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '数据库事务提交失败'];
            //@codeCoverageIgnoreEnd
        }

        //删除搜索索引
        $CDBPushPerson = new CDBPushPerson();
        $personDB = new CDBPerson();
        foreach ($childs['persons'] as $personId) {
            $person = $personDB->getPersonById($personId, false);
            if (empty($person)) {
                continue;
            }
            $modelId = $personId;
            $action = 2;
            $source = $person;
            $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
        }

        return ['error_code' => $GLOBALS['ERROR_SUCCESS'], 'error_msg' => ''];
    }

    /**
     * 还原分支
     * @param $personId 节点id
     * @param $optionUser 操作人id
     * @param $relation  关系id : father/mother => 123
     * @return bool
     * 分支还原分三部 1：连接树 2 子树设为is_delete = 0。3 添加用户和人物之间的绑定; 4 记录
     */
    public function reSetBranch($logId, $optionUser, $relation = '')
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $msg = $this->readBranchLog($logId, 'delete');
        if (empty($msg)) {//没有此人的记录
            return ['error_code' => $GLOBALS['ERROR_PERSON_EXISTS'], 'error_msg' => '还原失败，没有删除记录'];
        }
        $personId = $msg['personId'];
        $familyId = $msg['familyId'];
        //构建关系数组
        $relations = $msg['relations'];
        $parents = array_merge($relations['father'], $relations['mother']);
        if (!(empty($parents) && $relation <= 0)) {
            if ($relation > 0) {
                $sql = "SELECT id, gender, daughter, son, spouse, type, level FROM $this->TABLE_PERSON where id = $relation 
                AND type = 1 AND familyId = $familyId AND is_delete = 0 ";
                $relations['isNewRelation'] = true;
            } else {
                //考虑到父系主支可能有删除或者更新，所以关系需要重新构建
                $str = implode(',', $parents);
                $sql = "SELECT id, gender, daughter, son, spouse, type, level FROM $this->TABLE_PERSON where id in($str) 
                AND type = 1 AND familyId = $familyId AND is_delete = 0 ";
                $relations['isNewRelation'] = false;
            }


            $result = $this->pdo->uniqueResult($sql);
            if (empty($result)) {//还原的父系不在本族主线中 或者已经被删除 
                return ['error_code' => $GLOBALS['ERROR_PERSON_EXISTS'], 'error_msg' => '还原失败，父系不存在'];
            }
            $relations['brother'] = Util::relationSplit($result['son']);
            $relations['sister'] = Util::relationSplit($result['daughter']);
            $spouse = Util::relationSplit($result['spouse']);

            $relation = $result['id'];
            $msg['relations']['toPersonId'] = $relation;
            $relations['toPersonId'] = $relation;

            if ($result['gender'] == 1) {
                $relations['father'] = [$relation];
                $relations['mother'] = $spouse;
            } else {
                $relations['father'] = $spouse;
                $relations['mother'] = [$relation];
            }
            //添加自己进入数组
            $sql = "SELECT gender FROM $this->TABLE_PERSON where id = $personId AND familyId = $familyId ";
            $self = $this->pdo->uniqueResult($sql);
            if (empty($self)) {
                return ['error_code' => $GLOBALS['ERROR_PERSON_EXISTS'], 'error_msg' => '还原失败，还原分支不存在，或者数据出错'];
            }
            if ($self['gender'] == 1) {
                $relations['brother'] = array_unique(array_merge($relations['brother'], [$personId]));
            } else {
                $relations['sister'] = array_unique(array_merge($relations['sister'], [$personId]));
            }
            $relations['parentLevel'] = $result['level'];
        } else {
            return ['error_code' => $GLOBALS['ERROR_PERSON_EXISTS'], 'error_msg' => '还原失败，父系不存在'];
        }
        //如果没有父系母系，并且没有指定关系 


        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            //记录人物日志
            $personDB = new CDBPerson();
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $personDB->getPersonById($personId);
            //$familyId = $old_person->familyId;
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $option_detail = '还原人物及分支';
            $option_user = $GLOBALS['userId'] ?? 0;

            //step one
            $linkRes = $this->linkBranchTree($personId, $relations);
            if ($linkRes['code'] == false) {
                $this->pdo->rollback();            //事务回滚
                $msg = $linkRes['msg'];
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => $msg];
            }
            //step two
            $childs = $msg['persons'];
            $childs['parentRelations'] = $relations;
            $childs['optionUser'] = $optionUser;
            if (!$this->setDeleteBranch($childs, 0)) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '还原分支失败'];
            }
            //step three
            if (!empty($msg['userIds'])) {
                foreach ($msg['userIds'] as $key => $val) {
                    $sql = "SELECT id FROM $this->TABLE_PERSON WHERE userId = '$val' AND is_delete = '0' AND familyId = '$familyId' ";
                    $result = $this->pdo->uniqueResult($sql);
                    if ($result != null) {
                        //表示这个用户已经绑定家族中某个人
                        continue;
                    }
                    //恢复TABLE_PERSON 表中数据
                    $sql = "UPDATE $this->TABLE_PERSON SET userId = '$val',update_time = now() WHERE familyId='$familyId' AND id='$key' AND isDelete = 0";
                    $this->pdo->update($sql);
                    //恢复管理员身份
                    $deleteSql = "UPDATE $this->TABLE_ADMIN SET isDelete=0 WHERE familyId='$familyId' AND adminId='$val' AND isDelete = 0";
                    $this->pdo->update($deleteSql);

                    // 删除family_user表中对应数据
                    $sql = "UPDATE $this->TABLE_FAMILY_USER SET is_delete = 1, update_time = now()  
                    WHERE userId = '$val' AND familyId = '$familyId' ORDER BY id DESC LIMIT 1 ";
                    $this->pdo->update($sql);

                    //更新用户资料的家族数
                    $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1,update_time = now() WHERE userId = '$val'";
                    $this->pdo->update($sql);

                }
            }
            //step four
            $insertId = $this->recordBranch($personId, json_encode($msg['persons']), json_encode($msg['relations']), $familyId, $optionUser, 'reset', $logId);
            if ($insertId == false) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => '插入日志失败'];
            }

            $other_msg = ['resetLogId' => $insertId, 'lastLogId' => $logId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '数据库事务提交失败'];
            //@codeCoverageIgnoreEnd
        }


        //恢复搜索索引
        $CDBPushPerson = new CDBPushPerson();
        $personDB = new CDBPerson();
        foreach ($msg['persons']['persons'] as $personId) {
            $person = $personDB->getPersonById($personId);
            if (empty($person)) {
                continue;
            }
            $modelId = $personId;
            $action = 1;
            $source = $person;
            $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
        }

        return ['error_code' => $GLOBALS['ERROR_SUCCESS'], 'error_msg' => ''];
    }

    /**
     * 读取节点 遍历
     * @param $personId 节点id
     * @return arr :familyId 家族id persons 节点集合 Adoptions 过继集合 userIds 绑定用户集合
     * 实际上是将分支从此处断开，包括与上级（父,母，兄弟等）的关系
     */
    private function readChilds($personId, $familyId)
    {
        $this->res = [
            'familyId' => $familyId,
            'persons' => [(string)$personId], //统一用string
            'Adoptions' => [
                'Adoption' => [],//过继前集合
                'Adoptioned' => [],//过继后集合
            ],
            'userIds' => [
                //personId => userId
            ]
        ];
        $nextArray = [$personId];
        $this->searchChild($nextArray);

        $this->res['persons'] = array_unique($this->res['persons']);

        return $this->res;
    }

    private function searchChild($nextArray)
    {

        foreach ($nextArray as $key => $val) {
            $sql = "SELECT id,  same_personId, is_adoption, userId, daughter, son, spouse FROM $this->TABLE_PERSON where id = $val AND is_delete = 0";
            $result = $this->pdo->uniqueResult($sql);
            if (!empty($result)) {
                //添加过继
                if ($result['same_personId'] > 0) {
                    if ($result['is_adoption'] == 0) {
                        $this->res['Adoptions']['Adoption'][] = $result['id'];
                        $this->res['Adoptions']['Adoptioned'][] = $result['same_personId'];
                    } else {
                        $this->res['Adoptions']['Adoptioned'][] = $result['id'];
                        $this->res['Adoptions']['Adoption'][] = $result['same_personId'];
                    }
                }
                //添加绑定用户
                if ($result['userId'] > 0) {
                    $this->res['userIds'][$result['id']] = $result['userId'];
                }
                //查询下一层
                $daughters = Util::relationSplit($result['daughter']);
                $sons = Util::relationSplit($result['son']);
                $spouse = Util::relationSplit($result['spouse']);
                $nextArray = array_merge($daughters, $sons);
                $this->res['persons'] = array_merge($this->res['persons'], $nextArray);
                $this->res['persons'] = array_merge($this->res['persons'], $spouse);
                if (!empty($nextArray)) {
                    $this->searchChild($nextArray);
                }
            }

        }
        //return $this->res;
    }

    /**
     * 删除分支
     * @param $personId 节点id
     * @return json_encode($relations);
     * 实际上是将分支从此处断开与上级（父,母，兄弟等）的关系
     */
    private function cutBranchTree($personId)
    {

        if ($personId <= 0) {
            return ['code' => false, 'msg' => '删除主支失败，人物id错误'];
        }
        //记录父亲，母亲，兄弟，姐妹
        $sql = "SELECT gender, father, mother, brother, sister, spouse FROM $this->TABLE_PERSON where id = $personId AND is_delete = 0  ";
        $res = $this->pdo->uniqueResult($sql);
        if (empty($res)) {
            return ['code' => false, 'msg' => '删除失败，人物已经删除，请勿重复删除'];
        }

        //$res['father'] = (string) '@123|'
        $relations = $res;
        $relations['father'] = Util::relationSplit($res['father']);
        $relations['mother'] = Util::relationSplit($res['mother']);
        $relations['brother'] = Util::relationSplit($res['brother']);
        $relations['sister'] = Util::relationSplit($res['sister']);
        $relations['spouse'] = Util::relationSplit($res['spouse']);

        // 删除原来留存的父母，兄弟姐妹关系
        // $setParam = REPLACE($setParam,'$from','$to')";
        $from = '@' . $personId . '|';
        $to = '';
        $parentArray = array_merge($relations['father'], $relations['mother']);
        if (!empty($parentArray)) {
            $parents = implode(',', $parentArray);
            if ($relations['gender'] == 1) {
                $key = "son";
            } else {
                $key = "daughter";
            }
            $sql = "UPDATE $this->TABLE_PERSON SET $key = REPLACE($key, '$from', '$to'),update_time = now() WHERE id in($parents) and is_delete = 0 ";
            $updatecount = $this->pdo->update($sql);
            if ($updatecount == 0) {
                return ['code' => false, 'msg' => '删除主支失败，人物已经删除，请勿重复删除'];
            }
        } else {

            return ['code' => false, 'msg' => '人物没有父系，可能是最高人物，不能删除'];
        }

        //更新配偶的记录
        //if(!empty($relations['spouse'])){
        //    $spouses = implode(',', $relations['spouse']);
        //    $sql = "UPDATE $this->TABLE_PERSON SET spouse = REPLACE(spouse, '$from', '$to'),update_time = now() WHERE id in($spouses) ";
        //    $this->pdo->update($sql);
        //}

        //兄弟姐妹
        $siblingArray = array_merge($relations['sister'], $relations['brother']);
        if (!empty($siblingArray)) {
            $brothers = implode(',', $siblingArray);
            if ($relations['gender'] == 1) {
                $key = "brother";
            } else {
                $key = "sister";
            }
            $sql = "UPDATE $this->TABLE_PERSON SET $key = REPLACE($key, '$from','$to'),update_time = now() WHERE id in($brothers)  and is_delete = 0 ";
            $updatecount = $this->pdo->update($sql);
            if ($updatecount == 0) {
                return ['code' => false, 'msg' => '删除主支失败，人物已经删除，请勿重复删除'];
            }
        }

        // 删除当前人物的父/母, 兄弟姐妹
        //保留自己
        if ($relations['gender'] == 1) {
            $sql = "UPDATE $this->TABLE_PERSON SET father = '', mother = '', brother = '$from', sister = '',update_time = now() WHERE id = '$personId' ";
        } else {
            $sql = "UPDATE $this->TABLE_PERSON SET father = '', mother = '', brother = '', sister = '$from',update_time = now() WHERE id = '$personId' ";
        }

        $this->pdo->update($sql);
        $res = json_encode($relations);
        return ['code' => true, 'msg' => $res];

    }

    /**
     * 连接分支
     * @param $personId 节点id
     * @param $relations 关系数组
     * @return bool
     * 实际上是将分支从此处连接，包括与上级（父,母，兄弟等）的关系
     * 默认连接至之前的位置，可重新定义
     */
    private function linkBranchTree($personId, $relations)
    {

        $str = '@' . $personId . '|';

        //父母        
        $parentArray = array_merge($relations['father'], $relations['mother']);
        if (!empty($parentArray)) {
            $parents = implode(',', $parentArray);
            $sql = "SELECT count(*) FROM $this->TABLE_PERSON WHERE id in ($parents)  AND type = 1 AND is_delete = 0 ";
            $res = $this->pdo->uniqueResult($sql);
            if ($res['count(*)'] < 1) {//父母有本族的人被删除
                return ['code' => false, 'msg' => '还原主支失败，父系已经被删除'];
            }
            if ($relations['gender'] == 1) {
                $key = "son";
            } else {
                $key = "daughter";
            }
            $sql = "UPDATE $this->TABLE_PERSON SET $key = CONCAT($key, '$str'),update_time = now() WHERE id in($parents) AND is_delete = 0 ";
            $updateCount = $this->pdo->update($sql);
            if ($updateCount == 0) {
                return ['code' => false, 'msg' => '还原主支失败，父系人物已经删除'];
            }
        }

        //配偶
        //if(!empty($relations['spouse'])){
        //    $spouses = implode(',', $relations['spouse']);
        //    $sql = "UPDATE $this->TABLE_PERSON SET spouse = CONCAT(spouse, '$str'),update_time = now() WHERE id in($spouses) ";
        //    $this->pdo->update($sql);
        //}

        //兄弟姐妹
        //
        $siblingArray = array_merge($relations['sister'], $relations['brother']);
        if (!empty($siblingArray)) {
            $brothers = implode(',', $siblingArray);
            if ($relations['gender'] == 1) {
                $key = "brother";
            } else {
                $key = "sister";
            }
            $sql = "UPDATE $this->TABLE_PERSON SET $key = CONCAT($key, '$str'),update_time = now() WHERE id in($brothers) ";
            $this->pdo->update($sql);
        }

        //自己
        $father = Util::arrayToRelation($relations['father']);
        $mother = Util::arrayToRelation($relations['mother']);
        //$spouse  = Util::arrayToRelation($relations['spouse']);
        $brother = Util::arrayToRelation($relations['brother']);
        $sister = Util::arrayToRelation($relations['sister']);
        $sql = "UPDATE $this->TABLE_PERSON SET father = '$father', mother = '$mother', brother = '$brother', sister = '$sister',update_time = now() WHERE id = '$personId' ";
        $this->pdo->update($sql);

        //如果是新关系，需要更新level
        if ($relations['isNewRelation'] == true) {

            $this->setLevel($relations['parentLevel'], [$personId]);
        }


        return ['code' => true, 'msg' => ''];
    }

    private function setLevel($parentLevel = '', $childs = [])
    {

        //更改自己的level
        $newLevel = $parentLevel + 1;
        $childstr = implode(',', $childs);
        $sql = "UPDATE $this->TABLE_PERSON SET level = '$newLevel' WHERE id in ($childstr) ";
        $this->pdo->update($sql);
        foreach ($childs as $child) {

            //查询自己的配偶和子女
            $sql = "SELECT daughter, son, spouse FROM $this->TABLE_PERSON where id = $child ";
            $result = $this->pdo->uniqueResult($sql);
            if (!empty($result)) {

                $result['daughter'] = Util::relationSplit($result['daughter']);
                $result['son'] = Util::relationSplit($result['son']);
                $result['spouse'] = Util::relationSplit($result['spouse']);
                //更新配偶的level
                if (!empty($result['spouse'])) {
                    $spouses = implode(',', $result['spouse']);
                    $sql = "UPDATE $this->TABLE_PERSON SET level = '$newLevel' WHERE id in ($spouses) ";
                    $this->pdo->update($sql);
                }
                $nextChilds = array_merge($result['son'], $result['daughter']);
                if (!empty($nextChilds)) {
                    //子女不为空
                    $this->setLevel($newLevel, $nextChilds);
                }
            }


        }


    }


    /**
     * 设置is_delete
     * @param $personsId 子节点集
     * @param $is_delete 0 1
     * @return bool
     */
    private function setDeleteBranch($childs = [], $is_delete = 1)
    {
        $familyId = $childs['familyId'];
        $personIds = implode(',', $childs['persons']);

        //设置persons is_delete
        if (empty($childs['persons'])) {
            return false;
        }

        $deleteNum = count($childs['persons']);
        if ($is_delete == 1) {

            //更新家族人数
            $sql = "UPDATE $this->TABLE SET member_count = member_count - $deleteNum WHERE id='$familyId' AND is_delete = 0 ";
            $this->pdo->update($sql);

            //取消过继关系 全部清空 应需要再改进

            if (!empty($childs['Adoptions']['Adoption'])) {
                $AdoptionsArr = array_merge($childs['Adoptions']['Adoption'], $childs['Adoptions']['Adoptioned']);
                $AdoptionsIds = implode(',', $AdoptionsArr);
                $sql = "UPDATE $this->TABLE_PERSON SET is_adoption = 0, same_personId = 0 WHERE id in ($AdoptionsIds) ";
                $this->pdo->update($sql);
            }

            //再删除家族人物
            $sql = "UPDATE $this->TABLE_PERSON SET is_delete = 1, update_time = now() WHERE id in ($personIds) ";
            $this->pdo->update($sql);

            // 删除人物的分支记录
            $branchSql = "UPDATE $this->TABLE_FAMILY_INNER_BRANCH_PERSON SET isDelete = 1 WHERE personId in ($personIds) ";
            $this->pdo->update($branchSql);


        } else {
            //先恢复家族人物
            $sql = "UPDATE $this->TABLE_PERSON SET is_delete = 0, logId = 0, update_time = now() WHERE id in ($personIds) ";
            $this->pdo->update($sql);

            //更新家族人数
            $sql = "UPDATE $this->TABLE SET member_count = member_count + $deleteNum WHERE id='$familyId' AND is_delete = 0 ";
            $this->pdo->update($sql);

            //恢复过继关系
            if (!empty($childs['Adoptions']['Adoption'])) {
                foreach ($childs['Adoptions']['Adoption'] as $key => $val) {
                    //过继后的id
                    $same_personId = $childs['Adoptions']['Adoptioned'][$key];
                    //如果过继前的人物和过继后的人物都在子分支中才恢复
                    if (in_array($val, $childs['persons']) && in_array($same_personId, $childs['persons'])) {
                        //过继前
                        $sql = "UPDATE $this->TABLE_PERSON SET  same_personId = '$same_personId', is_adoption = 1 WHERE id = '$val' ";
                        $this->pdo->update($sql);

                        //过继后
                        $sql_same = "UPDATE $this->TABLE_PERSON SET  same_personId = '$val', is_adoption = 2 WHERE id = '$same_personId' ";
                        $this->pdo->update($sql_same);
                    }

                }
            }

            //恢复人物分支记录
            $relations = $childs['parentRelations'];
            $personDB = new CDBPerson();
            $userId = $childs['optionUser'];
            $parents = array_merge($relations['father'], $relations['mother']);
            //查找最高节点的分支信息，然后添加
            $parentId = $parents[0];
            $branchIds = $personDB->getPersonBranchList($parentId);
            if (count($branchIds) > 0) {
                foreach ($childs['persons'] as $personId) {
                    $firstBranch = $branchIds[0]['branchId'];
                    $sql = "INSERT INTO $this->TABLE_FAMILY_INNER_BRANCH_PERSON (branchId,personId,createBy,createTime,updateBy,updateTime)VALUES 
                    ('$firstBranch', '$personId', '$userId', now(), '$userId', now())";
                    for ($i = 1; $i < count($branchIds); $i++) {
                        $nextBranchId = $branchIds[$i]['branchId'];
                        $sql .= ",('$nextBranchId', '$personId', '$userId', now(), '$userId', now())";
                    }
                    $this->pdo->insert($sql);
                }
            }


        }

        return true;

    }

    /**
     * 记录分支操作日至
     * @param $personId 父节点
     * @param $persons  子节点集
     * @param $options 当前操作 delete reset
     * @return bool
     */
    private function recordBranch($personId, $persons, $relations, $familyId, $optionUser, $option, $logId = 0,$parentId=0 ,$parentName='')
    {
        $recordTableName = 'gener_family_branch_log';
        $sql = "INSERT INTO $recordTableName (personId, persons, relations, familyId, options, options_user, last_logId, record_time, update_time,parentId,parentName) VALUES
        ('$personId', '$persons', '$relations', '$familyId', '$option', '$optionUser', '$logId', now(), now(),$parentId,'$parentName' )";
        $insertId = $this->pdo->insert($sql);

        if ($insertId > 0) {
            if ($logId > 0) {//还原操作
                $sql = "UPDATE $recordTableName SET  last_logId = '$insertId' where id = '$logId'";
                $num = $this->pdo->update($sql);
                if ($num > 0) {
                    return $insertId;
                } else {
                    return false;
                }
            } else {
                return $insertId;
            }
        } else {
            return false;
        }

    }

    /**
     * 读取最近一次的分支日志
     * @param $logId 日至id
     * @return arr: persons 子节点集合 relations: 当前关系树 ..
     *
     */
    private function readBranchLog($logId, $options = 'delete')
    {
        $recordTableName = 'gener_family_branch_log';

        $sql = "SELECT personId, persons, relations, familyId, options_user, record_time FROM $recordTableName 
        where id = '$logId' AND options = '$options' AND last_logId = '0' ";
        $res = $this->pdo->uniqueResult($sql);
        if (empty($res)) {
            return false;
        }
        $res['persons'] = json_decode($res['persons'], true);
        $res['relations'] = json_decode($res['relations'], true);
        return $res;

    }

    /**
     * 读取家族所有的分支日志
     * @param $familyId 节点id
     * @return arr: id =>
     *  persons 子节点集合 relations: 当前关系树 ..
     *
     */
    public function readBranchLogs($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $recordTableName = 'gener_family_branch_log';

        $sql = "SELECT bl.id, bl.persons, bl.personId, p.name as person_name, p.photo as person_photo, bl.familyId, bl.options, bl.options_user, op.name as options_name, bl.record_time, bl.update_time
        FROM $recordTableName as bl 
        LEFT JOIN $this->TABLE_PERSON as p on p.id = bl.personId
        LEFT JOIN $this->TABLE_PERSON as op on op.userId = bl.options_user AND op.familyId = bl.familyId
        where bl.familyId = '$familyId' AND bl.options = 'delete' AND bl.last_logId = '0'
        order by bl.id desc ";
        $res = $this->pdo->query($sql);
        foreach ($res as &$v) {
            $queryPersons = json_decode($v['persons'], true);
            $personsArray = $queryPersons['persons'];
            if (!empty($personsArray)) {
                $personsString = implode(',', $personsArray);
                $sql = " SELECT id, name, photo FROM $this->TABLE_PERSON WHERE id in ($personsString)";
                $v['persons'] = $this->pdo->query($sql);
            }

        }
        return $res;
    }

    /**
     * @Deprecated 2019-06-21
     *
     * 验证用户id和familyId是否匹配,即该用户是属于这个家族的
     * 这个用户有查看家族内公开的信息的权限
     * @param $personId 用户id
     * @param $familyId 家族id
     * @return true 匹配，false不匹配
     */
    public function verifyPersonIdAndFamilyId($personId, $familyId, $isDelete = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->TABLE_PERSON WHERE id = '$personId' AND familyId = '$familyId'";
        if ($isDelete == '0') {
            $sql .= " AND is_delete = '0' ";
        }
        $result = $this->pdo->query($sql);
        if (count($result) > 0) {
            return true;
        }
        return false;
    }

    /**
     * 复制家族分支到另一个家族节点
     * @param isSetUserId 是否复制（设置）userId
     * @param firstPersonId 复制的分支人物id
     * @param newFamilyId 复制到的家族id
     * @param newParentId 复制到的人物id
     *
     * step one :获取 oldFamilyId 数据
     * step two :插入新数据
     * step three :处理过继关系
     * step four :循环(从上往下)
     */
    public function copyFamilyBranchByPerson($oldFamilyId, $oldPersonId, $newFamilyId, $newParentId, $optionUser, $isSetUserId = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $record = [ //记录人物对应关系 和 过继关系
            'error_code' => true,
            'error_msg' => '',
            'ids' => [], // oldId -> newId
            'samePersons' => [], //newId -> oldsame_personId  =>newsame_personId = $record['ids'][$record['samePersons']['newId']] 
            'userIds' => [] //newId -> userId

        ];
        //step one
        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            //记录人物日志
            $personId = $newFamilyId;
            $personDB = new CDBPerson();
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $personDB->getPersonById($personId);
            $familyId = $old_person->familyId;
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $option_detail = '复制人物分支到新家族';
            $option_user = $GLOBALS['userId'] ?? $optionUser;

            //step one
            //$this->getChildsPersons($oldFamilyId, $oldPersonId);
            $childs = [$oldPersonId];
            $oldParent = [
                'gender' => -1, //父系性别
                'newId' => $newParentId, //父系id
            ];
            $record = $this->copyBranchPersons($childs, $oldParent, $newFamilyId, $isSetUserId, $optionUser, $record);
            if ($record['error_code'] == false) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => $record['error_msg']];
            }

            //step two 更新过继关系
            $samePersons = $record['samePersons'];
            if (!empty($samePersons)) {
                foreach ($samePersons as $newId => $oldSamePersonId) {
                    if (isset($record['ids'][$oldSamePersonId])) {
                        $newSamePersonId = $record['ids'][$oldSamePersonId];
                        $sql = "UPDATE $this->TABLE_PERSON SET  same_personId = '$newSamePersonId' WHERE id = '$newId' ";
                        $this->pdo->update($sql);

                    }
                }
            }

            //step three 更新新家族人数
            $updateNum = count($record['ids']);
            $sql = "UPDATE $this->TABLE SET member_count = member_count + '$updateNum' WHERE id='$newFamilyId' AND is_delete = 0 ";
            $updateCount = $this->pdo->update($sql);
            if ($updateCount <= 0) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '更新数据库失败，请勿重复复制'];
            }

            //更新userId对应的家族数
            //记录 本次复制操作
            $recordTableName = 'gener_family_branch_copy_log';
            $recordContent = json_encode($record);
            $sql = "INSERT INTO $recordTableName (oldFamilyId, copyedPersonId, newFamilyId, newParentId, record, isSetUserId, option_user, error_persons, record_time)
            VALUES('$oldFamilyId', '$oldPersonId', '$newFamilyId', '$newParentId', '$recordContent', '$isSetUserId', '$optionUser', '', now() )";
            $insertId = $this->pdo->insert($sql);
            if ($insertId <= 0) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => '复制失败，插入日志出错'];
            }

            $other_msg = ['logId' => $insertId, 'oldFamilyId' => $oldFamilyId, 'oldPersonId' => $oldPersonId, 'isSetUserId' => $isSetUserId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $this->pdo->commit();

            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '数据库事务提交失败'];
            //@codeCoverageIgnoreEnd
        }

        //step four
        //添加新人物搜索索引
        $CDBPushPerson = new CDBPushPerson();
        $personDB = new CDBPerson();
        $error = [];
        foreach ($record['ids'] as $personRelation) {
            foreach ($personRelation as $newId) {
                $person = $personDB->getPersonById($newId);
                if (empty($person)) {
                    $error[] = $newId;
                    continue;
                }
                $modelId = $newId;
                $action = 1;
                $source = $person;
                $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
            }

        }

        if (!empty($error)) {//
            $errorContent = json_encode($error);
            $sql = "UPDATE $recordTableName SET error_persons = '$errorContent' WHERE id = '$insertId' ";
            $this->pdo->update($sql);
        }

        return ['error_code' => $GLOBALS['ERROR_SUCCESS'], 'error_msg' => '复制成功'];

    }

    /**
     * @param childs 下级人物类 集合 包括：儿子 女儿 配偶
     * 1. 更新关系树
     * 2. 查询 下级人物类对应的复制前家族人物的下级，构造插入数据，插入
     * same_personId  (过继关系) userId(绑定用户)
     * @return otherRelations 过继关系
     */

    private function copyBranchPersons($childs, $oldParent, $newFamilyId, $isSetUserId, $optionUser, $record)
    {
        $personDB = new CDBPerson();
        foreach ($childs as $key => $childId) {
            $oldPerson = $personDB->getPersonById($childId);

            $child = clone $oldPerson;//构建新人物数据类
            $child->id = Util::getNextId();
            $child->familyId = $newFamilyId;
            $child->refFamilyId = '';
            $child->refPersonId = '';

            //清空其他关系
            //same_personId,father,mother,son,daughter,spouse,
            $child->same_personId = 0;
            $child->father = '';
            $child->mother = '';
            $child->son = '';
            $child->daughter = '';
            $child->spouse = '';

            //记录id和过继关系 无需记录is_adoption 直接赋值
            $record['ids'][] = [$oldPerson->id => $child->id];
            if ($oldPerson->samePersonId > 0) {
                $record['samePersons'][$child->id] = $oldPerson->samePersonId;
            }
            if ($child->userId > 0) {
                $record['userIds'][$child->id] = $child->userId;
            }

            if ($oldPerson->type == 2) {//用oldtype 判断是否是配偶
                //配偶不需要再查询
                $child->childs = [];
                $child->oldSpouse = [];
                $child->relation = $GLOBALS['RELATION_SPOUSE'];
            } else {
                //循环体
                $child->childs = array_unique(array_merge($oldPerson->son, $oldPerson->daughter));// 去重复，防止数据重复出错
                $child->oldSpouse = $oldPerson->spouse;
                if ($child->gender == 1) {//男
                    $child->relation = $GLOBALS['RELATION_SON'];
                } else {
                    $child->relation = $GLOBALS['RELATION_DAUGHTER'];
                }
            }


            $child->corePersonId = $oldParent['newId'];
            if ($isSetUserId == 0) {
                $child->userId = 0;
            } else {
                //检查是否新家族存在次userId
                if ($child->userId > 0) {
                    $sql = "SELECT id from $this->TABLE_PERSON WHERE familyId = '$newFamilyId' AND userId = '$child->userId'";
                    $check = $this->pdo->uniqueResult($sql);
                    if (count($check) > 0) {
                        $record['error_code'] = false;
                        $record['error_msg'] = '用户已经在新家族中，请勿重新绑定';
                        return $record;
                    }
                }
            }

            //添加关系
            $insertId = $personDB->addRelatePerson($child, false, true); //添加关联人物会插入数据 所以无需再插入

            if ($insertId <= 0) {//插入失败
                $record['error_code'] = false;
                $record['error_msg'] = '插入新用户失败';
                return $record;
            }

            //查询下级,构建循环体需要的参数
            //上级的配偶也放在下级一起循环
            $nextChilds = array_merge($child->childs, $child->oldSpouse);
            if (!empty($nextChilds)) {
                $nextParent = [
                    'gender' => $child->gender,
                    'newId' => $child->id
                ];
                $record = $this->copyBranchPersons($nextChilds, $nextParent, $newFamilyId, $isSetUserId, $optionUser, $record);
            }
        }
        return $record;
    }

    /**
     * 更新家族初始世代 start_level
     * @param $familyId
     * @param $startLevel 初始世代
     */
    public function updateFamilyStartLevel($familyId, $optionUser, $startLevel = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $level = intval($startLevel);
        if ($level < 1) {
            return ['error_code' => $GLOBALS['ERROR_PARAM_MISSING'], 'error_msg' => 'startLevel参数错误'];
        }

        $sql = "UPDATE $this->TABLE SET start_level = '$level',update_time = now(), update_by = '$optionUser' where id = '$familyId' AND is_delete = '0' ";

        $updateCount = $this->pdo->update($sql);
        if ($updateCount > 0) {
            return ['error_code' => true, 'error_msg' => $level];
        } else {
            return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '更新失败，请重试'];
        }
    }

    /**
     * 添加家族内建分支的管理员
     * @param $familyId  家族id
     * @param $branchAdminId 管理员用户id
     * @param $startIndex    开始的分支Id
     * @param $userId    操作的用户id
     * @return int 插入的记录id
     */
    public function addBranchAdmin($familyId, $branchAdminId, $startUserId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_FAMILY_BRANCH_ADMIN 
                (familyId, branchAdminId,startIndex, userId,createBy, createTime, updateBy, updateTime) 
                VALUES 
                ('$familyId', '$branchAdminId', '$startUserId', '$userId', now(), '$userId', now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 撤销内建分支的管理员
     * @param $familyId 分支id
     * @param $branchAdminId   用户id
     * @param $startIndex    分组开始的id
     * @param $userId    操作用户的id
     * @return int 修改的记录数
     */
    public function rescindBranchAdmin($familyId, $branchAdminId, $startIndex, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE  $this->TABLE_FAMILY_BRANCH_ADMIN SET isDelete = '1', updateBy = '$userId', updateTime = now() WHERE branchId = '$branchId' AND userId = '$userId' AND isDelete = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 检查用户是否是分支管理员
     * @param $userId 用户id
     * @param $familyId 家族id
     * @return bool 是true 否 false
     */
    public function isAdminForBranch2($branchAdminId, $startIndex, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT * FROM $this->TABLE_FAMILY_BRANCH_ADMIN WHERE familyId = '$familyId' AND startIndex = '$startIndex' AND branchAdminId  = '$branchAdminId' AND isDelete = '0' ";

        $result = $this->pdo->query($sql);

        if (count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //v3版本
    //v3版本
    //v3版本
    //v3版本
    //v3版本
    //v3版本
    //v3版本
    /**
     * 
     * 
     * @param needVirtual 需要虚拟结点
     */


    public function createFamilyAndCircleV3($userId, $familyName, $surname,
                                            $desc, $familyPhoto, $photo, $hxChatRoom, $groupType,$needVirtual = true,$needTransaction = true)
    {   //创建家族不创建真实的人物而是创建虚拟的节点
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        // 生成identity
        $identity = md5($userId . $familyName . time()) . Util::generateRandomCode(32);

        try {
            //开启事务处理
            if($needTransaction){
                $this->pdo->beginTransaction();
            }

            //创建圈子
            $sql = "INSERT INTO $this->TABLE_INFORMATION_CATEGORY (userId,avatar,name,circleType,create_by,create_time,
            update_by,update_time) VALUES ('$userId','$familyPhoto','$familyName',3,'$userId',now(),'$userId',now())";

            $socialCircleId = $this->pdo->insert($sql);

            if ($socialCircleId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '创建圈子失败');
                if($needTransaction){
                    $this->pdo->rollback();
                }
                exit;
            }

            $sql = "INSERT INTO $this->TABLE(name,surname,min_level,max_level,
            permission,originator,description,photo,hx_chatroom,identity,groupType,socialCircleId,
            create_by,create_time,update_by,update_time)
            VALUES
            ('$familyName','$surname','0','0','0','$userId','$desc','$familyPhoto',
            '$hxChatRoom','$identity','$groupType','$socialCircleId','$userId',now(),'$userId',now())";

            $familyId = $this->pdo->insert($sql);   //返回插入行的id或者-1
            if ($familyId < 0) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败");
                $this->pdo->rollback();            //事务回滚
                exit;
                //@codeCoverageIgnoreEnd
            }

            //创始人关注圈子
            $sql = "INSERT INTO $this->TABLE_INFORMATION_FOLLOW (userId,categoryId,createTime,createBy,updateTime,updateBy)
            VALUES('$userId','$socialCircleId',now(),'$userId',now(),'$userId')";
            $followId = $this->pdo->insert($sql);
            if ($followId < 0) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], '添加关注失败');
                if($needTransaction){
                $this->pdo->rollback();
                }
                exit;
            }
            $sql = "UPDATE $this->TABLE_INFORMATION_CATEGORY SET follow=1 WHERE id = '$socialCircleId'";
            $updateRow = $this->pdo->update($sql);
            if ($updateRow <= 0) {
                Util::printResult($GLOBALS['ERROR_SQL_UPDATE'], '更新关注数量失败');
                if($needTransaction){
                $this->pdo->rollback();
                }
                exit;
            }

            //将创始人添加到管理员表
            if (!$this->addAdmin($familyId, $userId)) {
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败1");
                if($needTransaction){
                $this->pdo->rollback();            //事务回滚
                }
                exit;
                //@codeCoverageIgnoreEnd
            }

            //将创始人添加到权限表
            if (!$this->addPermission($familyId, $userId, 0, 0, 500, $userId)) {
                Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "创建家族失败3");
                if($needTransaction){
                $this->pdo->rollback();            //事务回滚
                }
                exit;
            }

            if($needVirtual){
                $personDao = new CDBPerson();
                //向人物表中插入一条人物记录
                $personId = $personDao->addPersonWithNoTransactionV3($familyId, $photo,0,'始祖');
               if ($personId < 0) {
                     Util::printResult($GLOBALS['ERROR_SQL_INSERT'], "新增人物失败");
                     if($needTransaction){
                     $this->pdo->rollback();            //事务回滚
                     }
                     exit;
              }
            }else{
                $personId = 0;
            }

            //更新人物资料中的家族数量
            $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1 WHERE userId = '$userId' ";
            $this->pdo->update($sql);

            $this->createFamilyInitCallback($familyId);

            if($needTransaction){
            $this->pdo->commit();
            }
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            if($needTransaction){
            $this->pdo->rollback();            //事务回滚
            }
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return [$familyId, $personId, $socialCircleId];                                //返回家族id
    }

    /**
     * 更新家族信息
     * @param $familyId 家族id
     * @param $photo 家族照片
     */
    public function updateFamilyInfo($familyId, $data)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "update $this->TABLE set ";
        foreach ($data as $k => $v) {
            $sql = $sql . "$k = '$v' ,";
        }
        $sql = substr($sql, 0, -1) . " ,update_time = now() WHERE id = '$familyId'  and is_delete = '0' ";

        $updateCount = $this->pdo->update($sql);
        return $updateCount;
    }

    /**
     * 获取加入的家族列表，这个列表是用来提供家族选择的(<select></select>)
     * @param $userId  用户id
     * @return array 获取加入的家族列表
     */
    public function getJoinFamilyListV3($userId)
    {
        # 已弃用
        # 已弃用
        # 已弃用
        # 已弃用
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT tb.id,tb.name,tb.surname,tb.description,tb.max_level as maxLevel,tb.min_level as minLevel,
                    tb.member_count as memberCount, tb.photo ,tb.create_time as createTime,'join' as type 
                    from $this->TABLE tb 
                    inner join $this->TABLE_PERSON tp on tb.id = tp.familyId 
                    where tp.userId = '$userId' and tb.is_delete = '0' order by tb.create_time DESC";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取临时加入的家族（加入家族但是没有绑定用户）
     * @param $userId 用户id
     * @return array 获取加入的家族列表
     */
    public function getJoinFamilyForTempV3($userId)
    {
        #已弃用
        #已弃用
        #已弃用
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.name,tb.surname,tb.description,tb.max_level as maxLevel,tb.min_level as minLevel,
                    tb.member_count as memberCount, tb.photo ,tb.create_time as createTime,'join' as type 
                    from $this->TABLE tb 
                    inner join $this->TABLE_FAMILY_USER tfu on tb.id = tfu.familyId 
                    left join $this->TABLE_USER tu on tu.id = tb.originator 
                    where tfu.userId = '$userId'  and tfu.is_delete = '0' and tb.is_delete = '0' order by tb.create_time DESC";

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取该用户创建的所有家族
     * @param $userId 用户id
     * @return array 获取加入的家族列表
     */
    public function getOriginatorFamilyList($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.name,tb.surname,tb.description,tb.max_level as maxLevel,tb.min_level as minLevel,
                    tb.member_count as memberCount, tb.photo,tb.create_time as createTime, 'originator' as type 
                    from $this->TABLE tb 
                   WHERE tb.originator = '$userId' and tb.is_delete = '0' order by tb.create_time DESC";

        $result = $this->pdo->query($sql);
        return $result;
    }


    /**
     * 获取开放的族群列表
     */
    public function getOpenFamilysV3()
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.name,tb.surname,tb.description,tb.max_level as maxLevel,tb.min_level as minLevel,
                    tb.member_count as memberCount, tb.photo, tb.create_time as createTime, tb.groupType ,'example' as type, '10000' as permission 
                    from $this->TABLE tb WHERE open_type = '1' and is_delete = '0' order by tb.create_time DESC";

        return $this->pdo->query($sql);
    }

    /**
     * 获取开放的族群列表
     */
    public function getOpenFamilyList()
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,name,surname,min_level as minLevel,max_level as maxLevel,
        hx_chatroom as hxChatRoom,qrcode,permission,originator,member_count as memberCount,
        description,photo,groupType,create_by as createBy,create_time as createTime,
        update_by as updateBy,update_time as updateTime,open_type as openType,socialCircleId ,'example' as type 
                    FROM $this->TABLE WHERE open_type = '1' and is_delete = '0' order by createTime DESC";

        return $this->pdo->query($sql);
    }

    /**
     * 用于获取基础的用户权限
     * 权限类型 0是加入族群尚未绑定 1是已经绑定的普通成员 2 是分支管理员 3是总管理员 4是创建者
     * @param familyId 家族ID
     * @param $userID 管理员 用户ID
     * @return  用户权限等级、 族内分支起始Index 、 分支结束Index、 分支名字
     */
    public function getFamilyChiefAdminId($familyId) :array
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT userId FROM $this->TABLE_PERMISSION WHERE is_delete = 0 and familyId = $familyId 
                and permissionType >=400";

        return $this->pdo->query($sql);
    }

    /**
     * 用于获取基础的用户权限
     * 权限类型 0是加入族群尚未绑定 1是已经绑定的普通成员 2 是分支管理员 3是总管理员 4是创建者
     * @param familyId 家族ID
     * @param $userID 管理员 用户ID
     * @return array 用户权限等级、 族内分支起始Index 、 分支结束Index、 分支名字
     */
    public function getFamilyBranchAdminId($familyId, $familyIndex, $level)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT max(family_index) as familyIndex FROM $this->TABLE_PERSON 
            WHERE familyId = $familyId and is_delete = 0 and type = 1 and  level < $level and  family_index < $familyIndex group by level";

        $familyIndexes = array_map(function ($v){
                    return $v['familyIndex'];
                },$this->pdo->query($sql)); # 获取被检查者的父系直通id
        $familyIndexes = $familyIndexes ? $familyIndexes:[$familyIndex];

        if ($familyIndexes){
            $sqlForId = "SELECT id FROM $this->TABLE_PERSON 
            WHERE familyId = $familyId and is_delete = 0 and family_index in ( ". implode(',',$familyIndexes) .")";

            $sql = "SELECT userId FROM $this->TABLE_PERMISSION WHERE is_delete = 0 and familyId = $familyId 
                and permissionType =300 and personId in ( $sqlForId ) ";
            return $this->pdo->query($sql);
        }

        return [];
    }

    /**
     * 用于获取基础的用户权限
     * 权限类型 0是加入族群尚未绑定 1是已经绑定的普通成员 2 是分支管理员 3是总管理员 4是创建者
     * @param familyId 家族ID
     * @param $userID 管理员 用户ID
     * @return array 用户权限等级、 族内分支起始Index 、 分支结束Index、 分支名字
     */
    public function getUserPermission($userId, $familyId, $needPersonInfo=false, $branchIdIsDelete=true)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($needPersonInfo){
            $sql = "SELECT permissionType as permission, startIndex, endIndex,userId, p.id as personId, level, family_index as familyIndex, p.name as name,t.personId as branchPerson  
                    FROM $this->TABLE_PERMISSION t left join (select id,level,family_index,name,familyId,is_delete from $this->TABLE_PERSON where 
                    is_delete = 0 and familyId = $familyId and userId = $userId limit 1 ) p on t.familyId = p.familyId
                    WHERE t.is_delete = 0 and t.familyId = $familyId and t.userId = $userId limit 1";
        }else{
            $sql = "SELECT permissionType as permission, startIndex, endIndex,userId,personId as branchPerson
                    FROM $this->TABLE_PERMISSION WHERE is_delete = 0 and familyId = $familyId and userId = $userId limit 1";
        }

        $permission = $this->pdo->uniqueResult($sql);

        if (!$permission){
            return [];
        }

        $permission['permission'] = intval($permission['permission']);

        if ($permission['permission'] === 300){
            $isDeleteSql = $branchIdIsDelete ? " is_delete = 0 and " : " ";
            $sql = "select level,family_index as familyIndex from $this->TABLE_PERSON where $isDeleteSql id = ". $permission['branchPerson'] ." limit 1";
            $levelAndFamilyIndex = $this->pdo->uniqueResult($sql);

            if (!$levelAndFamilyIndex){
                return [];
            }
            $personDB = new CDBPerson();
            $permission['startIndex'] = intval($levelAndFamilyIndex['familyIndex']);
            $permission['endIndex'] = $personDB->getLastPersonFamilyIndexV3($familyId,  $levelAndFamilyIndex['level'], $permission['startIndex']);
        }

        return $permission;
    }

    /**
     * 用于获取基础的用户权限
     * 权限类型 100是加入族群尚未绑定 200是已经绑定的普通成员 300 是分支管理员 400是总管理员 500是创建者
     * @param familyId 家族ID
     * @param $userID 管理员 用户ID
     * @return array 用户权限等级、 族内分支起始Index 、 分支结束Index、 分支名字
     */
    public function getPersonPermissionList($userId, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "select * from $this->TABLE_PERSON inner join (select familyId,userId,permissionType as permission  from $this->TABLE_PERMISSION where is_delete = 0)
                where familyId=$familyId and is_delete = 0";

        if ($needPersonInfo){
            $sql = "SELECT permissionType as permission, startIndex, endIndex,userId, p.id as personId, level, family_index as familyIndex, p.name as name,t.personId as branchPerson 
                    FROM $this->TABLE_PERMISSION t left join (select id,level,family_index,name,familyId,is_delete from $this->TABLE_PERSON where 
                    is_delete = 0 and familyId = $familyId and userId = $userId limit 1 ) p on t.familyId = p.familyId
                    WHERE t.is_delete = 0 and t.familyId = $familyId and t.userId = $userId limit 1";
        }else{
            $sql = "SELECT permissionType as permission, startIndex, endIndex,userId,personId as branchPerson
                    FROM $this->TABLE_PERMISSION WHERE is_delete = 0 and familyId = $familyId and userId = $userId limit 1";
        }

        $permission = $this->pdo->uniqueResult($sql);

        if (!$permission){
            return [];
        }

        $permission['permission'] = intval($permission['permission']);

        if ($permission['permission'] === 300){
            $sql = "select level,family_index as familyIndex from $this->TABLE_PERSON where is_delete = 0 and id = ". $permission['branchPerson'] ." limit 1";
            $levelAndFamilyIndex = $this->pdo->uniqueResult($sql);
            if (!$levelAndFamilyIndex){
                return [];
            }
            $personDB = new CDBPerson();
            $permission['startIndex'] = $levelAndFamilyIndex['familyIndex'];
            $permission['endIndex'] = $personDB->getLastPersonFamilyIndexV3($familyId,  $levelAndFamilyIndex['level'], $permission['startIndex']);
        }

        return $permission;
    }

    /*
     * v3
     * 检查添加人物时候的祖先是否是是女儿
     * */
    public function checkIsNotMainInAddPerson($familyId,$person){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT 1  FROM $this->TABLE_PERSON 
            WHERE familyId = $familyId and is_delete = 0 and type = 1 and isNotMain = 0 and gender = 0 and level < $person->level and  family_index < $person->familyIndex group by level limit 1";

        if ($this->pdo->uniqueResult($sql)){
            return true;
        }
        return false;
    }

    /**
     * 添加用户管理员权限
     * @param familyId 家族id
     * @param userId   用户id
     * @param startIndex  起始Index
     * @param endIndex  结束Index
     * @param permissionType 添加管理员的类型
     * @return int 添加的记录数
     */
    public function addPermission($familyId, $userId, $startIndex, $endIndex, $permissionType, $administratorId,$name=null,$fatherName=null)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($name && $fatherName){
            $sql = "INSERT INTO $this->TABLE_PERMISSION(familyId,userId,startIndex,endIndex,permissionType,is_delete,
                create_time,create_by,update_time,update_by,name,father_name)  
                    VALUES
                    ($familyId,$administratorId,$startIndex,$endIndex,$permissionType, 0,now(),$userId,now(),$userId,'$name', '$fatherName')";
        }elseif (!$name && !$fatherName){
            $sql = "INSERT INTO $this->TABLE_PERMISSION(familyId,userId,startIndex,endIndex,permissionType,is_delete,
                create_time,create_by,update_time,update_by)  
                    VALUES
                    ($familyId,$administratorId,$startIndex,$endIndex,$permissionType, 0,now(),$userId,now(),$userId)";
        }else{
            return -1;
        }

        return $this->pdo->insert($sql);
    }



    /**
     * 根据管理员id,删除管理员权限
     * @param familyId 家族Id
     * @param administratorId   被撤销权限的用户id
     * @return int 删除的记录数
     */
    public function deletePermission($familyId, $userId, $updateBy)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE_PERMISSION SET is_delete = 1,update_time = now(), update_by = $updateBy WHERE familyId = $familyId AND is_delete=0 AND  userId = $userId";
        return $this->pdo->update($sql);
    }

    /**
     * 更新权限
     * @param permissionType        管理员类型
     * @param familyId        家族id
     * @param administratorId        管理员id
     * @param startIndex        分支起始Index
     * @param endIndex      分支结束Index
     * @return int 更新的记录id，失败则返回-1
     */
    public function modifyPermission($familyId, $permissionType, $userId, $startIndex, $endIndex, $updateBy,$personId, $name=null ,$fatherName=null)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $permissionSql = $permissionType === null ? "":" ,permissionType = $permissionType ";
        if ($name !== null && $fatherName !== null){
            $sql = "UPDATE $this->TABLE_PERMISSION SET  userId = '$userId', update_by = $updateBy,update_time = now(),
                    startIndex = '$startIndex' ,endIndex = '$endIndex' ,name = '$name', father_name = '$fatherName' ,personId = $personId  $permissionSql
                    WHERE familyId = '$familyId'  and is_delete = '0' and userId = $userId";

        }elseif ($name === null && $fatherName === null){
            $sql = "UPDATE $this->TABLE_PERMISSION SET  userId = '$userId',
            startIndex = '$startIndex' ,endIndex = '$endIndex',update_by = $updateBy , personId = $personId, update_time = now() $permissionSql
            WHERE familyId = '$familyId'  and is_delete = '0' and userId = $userId ";

        }else{
            return -1;
        }

        $updateId = $this->pdo->update($sql);

        //记录人物日志
        $personDB = new CDBPerson();
        $person = $personDB->getPersonByUserAndFamily($familyId,$userId);
        $function_name = __CLASS__ . '->' . __FUNCTION__;
        $familyId = $person['familyId'];
        $old_data = json_encode($person, JSON_UNESCAPED_UNICODE);
        $option_detail = '更新家族人物权限';
        $option_user = $GLOBALS['userId'] ?? 0;
        $other_msg = ['level' => $person['level']];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $personDB->recordPersonOptionLog($person['id'], $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

        return $updateId;
    }

    /**
     *  获取分支内最后一个下标 endIndex
     * @param startIndex
     * @param level
     * @param familyId
     * @return endIndex
     */
    public function getBranchEndIndex($startIndex, $familyId, $level)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "SELECT   family_index  as familyIndex
        FROM $this->TABLE_PERSON2 WHERE is_delete = 0 and familyId = '$familyId' and level = '$level' and family_index > '$startIndex' 
        order by family_index asc limit 1 ";
        $result = $this->pdo->query($sql);
        // 当前辈分只有一人，即管理到末尾,默认 99,999,999;
        if ($result == null) {
            $endIndex = 99999999;
        } // 其他管理当前分支
        else {
            $endIndex = $result;
        }

        return $endIndex;

    }

    //  /**
    //  * 将两个家族的同一个人合并
    //  * @param $personOneId 第一个人的id
    //  * @param $personTwoId 第二个人的id
    //  * @return int 正数代表合并成功,为更新的记录数，非正数代表合并失败,-1代表人物不存在,-2代表人物是同一个家族的,-3代表人物已存在合并的家族
    //  */
    // public function mergeTwoPerson3(int $personOneId, int $personTwoId)
    // {
    //     $PERSON_NOT_EXIST_ERROR = -1;
    //     $PERSON_SAME_FAMILY_ERROR = -2;
    //     $PERSON_EXIST_REF_FAMILY_ERROR = -3;

    //     $familyOneSql = "SELECT ref_personId_id FROM $this->TABLE_FAMILY_TREE  WHERE id = '$personOneId' OR id = '$personTwoId' ";
        
    //     $result = $this->pdo->query($familyOneSql);
    //     if (count($result) < 2) {
    //         //有人物不存在，直接返回错误
    //         //@codeCoverageIgnoreStart
    //         return $PERSON_NOT_EXIST_ERROR;
    //         //@codeCoverageIgnoreEnd
    //     }
    //     $familyOneId = null;
    //     $familyTwoId = null;
    //     $personOneRefPersonIds = array();
    //     $personTwoRefPersonIds = array();
    //     $personOneRefFamilyIds = array();
    //     $personTwoRefFamilyIds = array();

    //     foreach ($result as $person) {
    //         // if ($person['ref_familyId'] > 0) {
    //         //     //如果人物绑定了家族,则返回错误，不允许绑定
    //         //     return $PERSON_EXIST_REF_FAMILY_ERROR;
    //         // }
    //         if ($person['id'] == $personOneId) {
    //             $familyOneId = $person['familyId'];
    //             if ($person['ref_personId'] != null) {
    //                 $personIds = json_decode($person['ref_personId'], true);
    //                 $personOneRefPersonIds = $personIds;
    //             }

    //             if ($person['ref_familyId'] != null) {
    //                 $familyIds = json_decode($person['ref_familyId'], true);
    //                 $personOneRefFamilyIds = $familyIds;
    //             }
    //         } else if ($person['id'] == $personTwoId) {
    //             $familyTwoId = $person['familyId'];
    //             if ($person['ref_personId'] != null) {
    //                 $personIds = json_decode($person['ref_personId'], true);
    //                 $personTwoRefPersonIds = $personIds;
    //             }

    //             if ($person['ref_familyId'] != null) {
    //                 $familyIds = json_decode($person['ref_familyId'], true);
    //                 $personTwoRefFamilyIds = $familyIds;
    //             }
    //         }
    //     }

    //     if ($familyOneId == $familyTwoId) {
    //         //如果家族一等于家族二的id
    //         //@codeCoverageIgnoreStart
    //         return $PERSON_SAME_FAMILY_ERROR;
    //         //@codeCoverageIgnoreEnd
    //     }

    //     array_push($personOneRefPersonIds, intval($personTwoId));
    //     array_push($personOneRefFamilyIds, intval($familyTwoId));

    //     array_push($personTwoRefPersonIds, intval($personOneId));
    //     array_push($personTwoRefFamilyIds, intval($familyOneId));

    //     $personOneRefPersonIdsStr = json_encode($personOneRefPersonIds);
    //     $personTwoRefPersonIdsStr = json_encode($personTwoRefPersonIds);
    //     $personOneRefFamilyIdsStr = json_encode($personOneRefFamilyIds);
    //     $personTwoRefFamilyIdsStr = json_encode($personTwoRefFamilyIds);
    //     //将familyOne的id添加到personTwo的ref_familyId中,将familyTwo的id添加到personOne的ref_familyId中
    //     $sql = "UPDATE $this->TABLE_PERSON  
    //                     SET ref_personId = CASE id 
    //                     WHEN $personOneId THEN \"$personOneRefPersonIdsStr\"
    //                     WHEN $personTwoId THEN \"$personTwoRefPersonIdsStr\" 
    //                     END,
    //                     ref_familyId = CASE id 
    //                     WHEN $personOneId THEN \"$personOneRefFamilyIdsStr\" 
    //                     WHEN $personTwoId THEN \"$personTwoRefFamilyIdsStr\"  
    //                     END 
    //                     WHERE id  IN ($personOneId,$personTwoId) ";
    //     $result = $this->pdo->update($sql);

    //     $this->addFamilyMergeRecord($familyOneId, $familyTwoId);

    //     return $result;
    // }


    /**
     * 获取用户加入的家族
     * @param $userId 用户id
     *  @param $permission 需要筛选的权限
     * @return array 获取加入的家族列表
     */
    public function getFamilyListByPermission(int $userId, string $permission = "",string $type = "")
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $permission_sql = $permission ? " and permissionType $permission":"";
        $type_sql = $type ? "'$type' as type":" CASE 
                    WHEN permissionType in (100,200,300,400) THEN
                        'join'
                    WHEN permissionType = 500 THEN
                        'originator'
                    ELSE 0 END
                    as type ";

        $sql = "SELECT tb.id,tb.name,tb.surname,tb.description,tb.max_level as maxLevel,tb.min_level as minLevel,permissionType as permission,
                    tb.member_count as memberCount, tb.photo ,tb.create_time as createTime, tb.groupType, $type_sql 
                    from $this->TABLE tb 
                    left join (select * from $this->TABLE_PERMISSION where is_delete = 0 and userId =$userId) tfu 
                    on tb.id = tfu.familyId 
                    where tfu.userId = '$userId'  and tfu.is_delete = '0' and tb.is_delete = '0' $permission_sql order by tb.create_time DESC";

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 复制家族分支到另一个家族节点
     * @param isSetUserId 是否复制（设置）userId
     * @param firstPersonId 复制的分支人物id
     * @param newFamilyId 复制到的家族id
     * @param newParentId 复制到的人物id
     *
     * step one :获取 oldFamilyId 数据
     * step two :插入新数据
     * step three :处理过继关系
     * step four :循环(从上往下)
     */
    public function copyFamilyBranchByPersonV3($oldFamilyId, $oldPersonId, $newFamilyId, $newParentId, $optionUser, $isSetUserId = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $record = [ //记录人物对应关系 和 过继关系
            'error_code' => true,
            'error_msg' => '',
            'ids' => [], // oldId -> newId
            'samePersons' => [], //newId -> oldsame_personId  =>newsame_personId = $record['ids'][$record['samePersons']['newId']]
            'userIds' => [] //newId -> userId

        ];
        //step one
        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            //记录人物日志
            $personId = $newFamilyId;
            $personDB = new CDBPerson();
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $personDB->getPersonObjectById($oldPersonId);
            $familyId = $old_person->familyId;
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $option_detail = '复制人物分支到新家族';
            $option_user = $GLOBALS['userId'] ?? $optionUser;

            # 查询拿到newParent的familyIndex
            $newParent = $personDB->getPersonObjectById($newParentId);
//            $updateNewParentIndex = $this->updateFamilyIndexForCopyBranch($old_person, $newParent);
            $newParentIndex = $newParent->familyIndex;
            if ($newParent->son || $newParent->daughter){
                #获取relation的最后一个子女，把恢复的分支插入最后一个位置
                $sqlBrotherSister = "select id,family_index as  familyIndex,level from $this->TABLE_PERSON where id in ( " . implode(',',array_merge($newParent->son,$newParent->daughter)) . " )
                                    and is_delete = 0 order by family_index desc limit 1";
                $brotherAndSister = $this->pdo->uniqueResult($sqlBrotherSister);
                $newParentIndex = $personDB->getLastPersonFamilyIndexV3($newParent->familyId,$brotherAndSister['level'],$brotherAndSister['familyIndex']);
            }

            # 更新新加入部分的familyIndex
            $oldPersonIndex = $old_person->familyIndex;
            $oldPersonLastIndex = $personDB->getLastPersonFamilyIndexV3($old_person->familyId, $old_person->level,$oldPersonIndex);
            $personCount = $oldPersonLastIndex - $oldPersonIndex + 1;
            $sql = "update $this->TABLE_PERSON set family_index = family_index + $personCount where is_delete = 0 and familyId = $newFamilyId and family_index > $newParentIndex";
            $updateIndex = $this->pdo->update($sql);
//            if ($updateIndex < 0){
//                $this->pdo->rollback();
//                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '更新数据库失败'];            }

            $difference =  $newParentIndex - $oldPersonIndex + 1;

            //step one
            //$this->getChildsPersons($oldFamilyId, $oldPersonId);
            $childs = [$oldPersonId];
            $oldParent = [
                'gender' => -1, //父系性别
                'newId' => $newParentId, //父系id
            ];
            $record = $this->copyBranchPersonsV3($childs, $oldParent, $newFamilyId, $isSetUserId, $optionUser, $record, $difference);
            if ($record['error_code'] == false) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => $record['error_msg']];
            }

            //step two 更新过继关系
            $samePersons = $record['samePersons'];
            if (!empty($samePersons)) {
                foreach ($samePersons as $newId => $oldSamePersonId) {
                    if (isset($record['ids'][$oldSamePersonId])) {
                        $newSamePersonId = $record['ids'][$oldSamePersonId];
                        $sql = "UPDATE $this->TABLE_PERSON SET  same_personId = '$newSamePersonId' WHERE id = '$newId' ";
                        $this->pdo->update($sql);

                    }
                }
            }

            //step three 更新新家族人数
            $updateNum = count($record['ids']);
            $sql = "UPDATE $this->TABLE SET member_count = member_count + '$updateNum' WHERE id='$newFamilyId' AND is_delete = 0 ";
            $updateCount = $this->pdo->update($sql);
            if ($updateCount <= 0) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '更新数据库失败，请勿重复复制'];
            }

            //更新userId对应的家族数
            //记录 本次复制操作
            $recordTableName = 'gener_family_branch_copy_log';
            $recordContent = json_encode($record);
            $sql = "INSERT INTO $recordTableName (oldFamilyId, copyedPersonId, newFamilyId, newParentId, record, isSetUserId, option_user, error_persons, record_time)
            VALUES('$oldFamilyId', '$oldPersonId', '$newFamilyId', '$newParentId', '$recordContent', '$isSetUserId', '$optionUser', '', now() )";
            $insertId = $this->pdo->insert($sql);
            if ($insertId <= 0) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => '复制失败，插入日志出错'];
            }

            $other_msg = ['logId' => $insertId, 'oldFamilyId' => $oldFamilyId, 'oldPersonId' => $oldPersonId, 'isSetUserId' => $isSetUserId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $this->pdo->commit();

            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '数据库事务提交失败'];
            //@codeCoverageIgnoreEnd
        }

        //step four
        //添加新人物搜索索引
        $CDBPushPerson = new CDBPushPerson();
        $personDB = new CDBPerson();
        $error = [];
        foreach ($record['ids'] as $personRelation) {
            foreach ($personRelation as $newId) {
                $person = $personDB->getPersonById($newId);
                if (empty($person)) {
                    $error[] = $newId;
                    continue;
                }
                $modelId = $newId;
                $action = 1;
                $source = $person;
                $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
            }

        }

        if (!empty($error)) {//
            $errorContent = json_encode($error);
            $sql = "UPDATE $recordTableName SET error_persons = '$errorContent' WHERE id = '$insertId' ";
            $this->pdo->update($sql);
        }

        return ['error_code' => $GLOBALS['ERROR_SUCCESS'], 'error_msg' => '复制成功'];

    }

    /**
     * 复制家族分支到另一个家族节点
     * @param isSetUserId 是否复制（设置）userId
     * @param firstPersonId 复制的分支人物id
     * @param newFamilyId 复制到的家族id
     * @param newParentId 复制到的人物id
     *
     * step one :获取 oldFamilyId 数据
     * step two :插入新数据
     * step three :处理过继关系
     * step four :循环(从上往下)
     */
    public function copyFamilyBranchByPersonV3_1($oldFamilyId, $oldPersonId, $newFamilyId, $newParentId, $optionUser, $isSetUserId = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $record = [ //记录人物对应关系 和 过继关系
            'error_code' => true,
            'error_msg' => '',
            'ids' => [], // oldId -> newId
            'samePersons' => [], //newId -> oldsame_personId  =>newsame_personId = $record['ids'][$record['samePersons']['newId']]
            'userIds' => [] //newId -> userId

        ];
        //step one
        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            //记录人物日志
            $personId = $newFamilyId;
            $personDB = new CDBPerson();
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $personDB->getPersonObjectById($oldPersonId);
            $familyId = $old_person->familyId;
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $option_detail = '复制人物分支到新家族';
            $option_user = $GLOBALS['userId'] ?? $optionUser;

            # 查询拿到newParent的familyIndex
            $newParent = $personDB->getPersonObjectById($newParentId);
//            $updateNewParentIndex = $this->updateFamilyIndexForCopyBranch($old_person, $newParent);
            $newParentIndex = $newParent->familyIndex;
            if ($newParent->son || $newParent->daughter){
                #获取relation的最后一个子女，把恢复的分支插入最后一个位置
                $sqlBrotherSister = "select id,family_index as  familyIndex,level from $this->TABLE_PERSON where id in ( " . implode(',',array_merge($newParent->son,$newParent->daughter)) . " )
                                    and is_delete = 0 order by family_index desc limit 1";
                $brotherAndSister = $this->pdo->uniqueResult($sqlBrotherSister);
                $newParentIndex = $personDB->getLastPersonFamilyIndexV3($newParent->familyId,$brotherAndSister['level'],$brotherAndSister['familyIndex']);
            }

            # 更新新加入部分的familyIndex
            $oldPersonIndex = $old_person->familyIndex;
            $oldPersonLastIndex = $personDB->getLastPersonFamilyIndexV3($old_person->familyId, $old_person->level,$oldPersonIndex);
            $personCount = $oldPersonLastIndex - $oldPersonIndex + 1;
            $sql = "update $this->TABLE_PERSON set family_index = family_index + $personCount where is_delete = 0 and familyId = $newFamilyId and family_index > $newParentIndex";
            $updateIndex = $this->pdo->update($sql);
//            if ($updateIndex < 0){
//                $this->pdo->rollback();
//                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '更新数据库失败'];            }

            $difference =  $newParentIndex - $oldPersonIndex + 1;

            //step one
            //$this->getChildsPersons($oldFamilyId, $oldPersonId);
            $sql = "select *,family_index as familyIndex,country_name as countryName, province_name as provinceName, city_name as cityName, area_name as areaName,
                    town_name as townName, marital_status as maritalStatus, blood_type as bloodType, is_dead as isDead, is_adoption as isAdoption 
                    from $this->TABLE_PERSON where familyId = $oldFamilyId and is_delete = 0 and family_index between $oldPersonIndex and $oldPersonLastIndex order by family_index";
            $oldBranchPeopleIds = $this->pdo->query($sql);
            $sql = "select userId from $this->TABLE_PERSON where familyId = $oldFamilyId and is_delete = 0 and userId != 0";
            $oldFamilyUserIds = $this->pdo->query($sql);
            $sql = "select userId from $this->TABLE_PERSON where familyId = $newFamilyId and is_delete = 0 and userId != 0";
            $newFamilyUserIds = $this->pdo->query($sql);
            if ($isSetUserId) {
                if (sizeof(array_diff($oldFamilyUserIds, $newFamilyUserIds)) !== sizeof($oldFamilyUserIds) || sizeof(array_diff($newFamilyUserIds, $oldFamilyUserIds)) !== sizeof($newFamilyUserIds) ) {
                    $this->pdo->rollback();            //事务回滚
                    $record['error_msg'] = '用户已经在新家族中，请勿重新绑定';
                    return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => $record['error_msg']];
                }
            }

            $newPeopleIdsList = [];
            # 新旧id的映射，绑定人物重复的判断
            foreach ($oldBranchPeopleIds as $oldId) {
                $newPeopleIdsList[$oldId['id']] = Util::getNextId();
            }
            # 移除第一个人
            array_shift($oldBranchPeopleIds);
//            $newParentLevel = $newParent->level;
            $sqlForOldBranchLevel = "select level from $this->TABLE_PERSON where familyId = $oldFamilyId and is_delete = 0 and
                family_index in ($oldPersonIndex, $oldPersonLastIndex) order by family_index";
            $OldBranchLevel = $this->pdo->query($sqlForOldBranchLevel);
            $oldBranchMinlevel = $OldBranchLevel[0]['level'];
//            $oldBranchMaxlevel = $OldBranchLevel[1]['level'];
            $levelDifference =  $newParent->level - $oldBranchMinlevel + 1;

            $childs = [$oldPersonId];
            $oldParent = [
                'gender' => -1, //父系性别
                'newId' => $newParentId, //父系id
            ];
//            var_dump(111);
            $record = $this->copyBranchPersonsV3_1($oldBranchPeopleIds, $newPeopleIdsList, $old_person ,$newParent, $isSetUserId, $newFamilyId, $difference, $levelDifference,$record);
            $peopleListForES = $record['peopleList'];
            unset($record['peopleList']);
            if ($record['error_code'] == false) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => $record['error_msg']];
            }
//            var_dump(1111);

            //step two 更新过继关系
            $samePersons = $record['samePersons'];
            if (!empty($samePersons)) {
                foreach ($samePersons as $newId => $oldSamePersonId) {
                    if (isset($record['ids'][$oldSamePersonId])) {
                        $newSamePersonId = $record['ids'][$oldSamePersonId];
                        $sql = "UPDATE $this->TABLE_PERSON SET  same_personId = '$newSamePersonId' WHERE id = '$newId' ";
                        $this->pdo->update($sql);

                    }
                }
            }

            //step three 更新新家族人数以及世代
            $updateNum = count($oldBranchPeopleIds) + 1;
            $sqlForFamilyLevel = "select max(level) as max, min(level) as min from gener_person where familyId = $newFamilyId and is_delete = 0";
            $levels = $this->pdo->uniqueResult($sqlForFamilyLevel);
            $sql = "UPDATE $this->TABLE SET member_count = member_count + '$updateNum', max_level = {$levels['max']}, 
                    min_level = {$levels['min']}, update_time = now(), update_by = $optionUser 
                    WHERE id='$newFamilyId' AND is_delete = 0 ";
            $updateCount = $this->pdo->update($sql);
            if ($updateCount <= 0) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '更新数据库失败，请勿重复复制'];
            }


            //更新userId对应的家族数
            //记录 本次复制操作
            $recordTableName = 'gener_family_branch_copy_log';
//            $recordContent = json_encode($record);
            $record['ids'] = [$record['ids'][0], 'addNum'=>$updateNum];
            $recordContent = json_encode($record);
            $sql = "INSERT INTO $recordTableName (oldFamilyId, copyedPersonId, newFamilyId, newParentId, record, isSetUserId, option_user, error_persons, record_time)
            VALUES('$oldFamilyId', '$oldPersonId', '$newFamilyId', '$newParentId', '$recordContent', '$isSetUserId', '$optionUser', '', now() )";
            $insertId = $this->pdo->insert($sql);
            if ($insertId <= 0) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => '复制失败，插入日志出错'];
            }

            $other_msg = ['logId' => $insertId, 'oldFamilyId' => $oldFamilyId, 'oldPersonId' => $oldPersonId, 'isSetUserId' => $isSetUserId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $this->pdo->commit();

            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '数据库事务提交失败'];
            //@codeCoverageIgnoreEnd
        }

        //step four
        //添加新人物搜索索引
        $CDBPushPerson = new CDBPushPerson();
        $personDB = new CDBPerson();
        $error = [];
//        print_r($peopleListForES);
        foreach ($peopleListForES as $person){
//            if (empty($person)) {
//                $error[] = $newId;
//                continue;
//            }
            $modelId = $person->id;
            $action = 1;
            $source = $person;
            $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
        }
//        foreach ($record['ids'] as $personRelation) {
//            foreach ($personRelation as $newId) {
//                $person = $personDB->getPersonById($newId);
//                if (empty($person)) {
//                    $error[] = $newId;
//                    continue;
//                }
//                $modelId = $newId;
//                $action = 1;
//                $source = $person;
//                $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
//            }
//
//        }

        if (!empty($error)) {//
            $errorContent = json_encode($error);
            $sql = "UPDATE $recordTableName SET error_persons = '$errorContent' WHERE id = '$insertId' ";
            $this->pdo->update($sql);
        }

        return ['error_code' => $GLOBALS['ERROR_SUCCESS'], 'error_msg' => '复制成功'];

    }

    private function MappingPersonIdForCopyBranch($ids, $peopleDict, $needString=false){
        $newIds = [];
        foreach ($ids as $id){
            $newIds[] = $peopleDict[$id];
        }
        return $needString ? Util::arrayToRelation($newIds):$newIds;
    }


    private function copyBranchPersonsV3_1($people, $peopleDict, $oldPerson, $newParent, $isSetUserId, $newFamilyId, $difference,$levelDifference,$record)
    {
        $personDB = new CDBPerson();
        $userId = $GLOBALS['userId'];
        $newPeopleList = [];

        $oldFamilyId = $oldPerson->familyId;
        #  先添加连接人物
        $linkPerson = clone $oldPerson;
        $linkPerson->id = $peopleDict[$linkPerson->id];
        $linkPerson->corePersonId = $newParent->id;
        $linkPerson->familyId = $newFamilyId;
        $linkPerson->refFamilyId = '';
        $linkPerson->refPersonId = '';

        //清空其他关系
        //same_personId,father,mother,son,daughter,spouse,
        $linkPerson->same_personId = 0;
        $linkPerson->father = '';
        $linkPerson->mother = '';
        $linkPerson->son = '';
        $linkPerson->daughter = '';
        $linkPerson->spouse = '';
        $linkPerson->userId = $isSetUserId ? $linkPerson->userId : 0;

        //记录id和过继关系 无需记录is_adoption 直接赋值
        $record['ids'][] = [$oldPerson->id => $linkPerson->id];
        if ($oldPerson->samePersonId > 0) {
            $record['samePersons'][$linkPerson->id] = $oldPerson->samePersonId;
        }
        if ($linkPerson->userId > 0 && $isSetUserId) {
            $record['userIds'][$linkPerson->id] = $linkPerson->userId;
        }

        if ($oldPerson->type == 2) {//用oldtype 判断是否是配偶
            //配偶不需要再查询
            $linkPerson->childs = [];
            $linkPerson->oldSpouse = [];
            $linkPerson->relation = $GLOBALS['RELATION_SPOUSE'];
        } else {
            //循环体
            $linkPerson->childs = array_unique(array_merge($oldPerson->son, $oldPerson->daughter));// 去重复，防止数据重复出错
            $linkPerson->oldSpouse = $oldPerson->spouse;
            if ($linkPerson->gender == 1) {//男
                $linkPerson->relation = $GLOBALS['RELATION_SON'];
            } else {
                $linkPerson->relation = $GLOBALS['RELATION_DAUGHTER'];
            }
        }
//        var_dump($linkPerson);
        $insertId = $personDB->addRelatePersonV3($linkPerson, $linkPerson->familyIndex+$difference, false, true); //添加关联人物会插入数据 所以无需再插入
        if (!$insertId){
            $record['error_code'] = false;
            $record['error_msg'] = '插入新用户失败';
            return  array_merge($record, ['peopleList'=>$newPeopleList]);
        }
        $newPeopleList[] = $personDB->getPersonById($insertId);


        $sqlForInsert = "INSERT INTO $this->TABLE_PERSON(
            id, zi,zpname,remark,branchId,name,type,
            profileText,sideText,country,country_name,province,province_name,
            city,city_name,area,area_name,town,town_name,address,
            birthday,gender,photo,share_url,is_dead,dead_time,level,
            infoCardId,is_delete,is_adoption,ref_familyId,ref_personId,
            confirm,blood_type,marital_status,phone,qq,qrcode,
            familyId,userId,ranking,create_by,create_time,update_by,update_time,family_index,isNotMain,bicolor,
            burialPlace,birthPlace,mother,father,son,daughter,spouse,brother,sister 
            )VALUES";

//        array_shift($people);

        foreach ($people as $person) {
            $newPerson = new Person($person);
            $oldPersonId = $newPerson->id;
            $newPerson->id = $peopleDict[$oldPersonId];
            $newPerson->familyId = $newFamilyId;
            $newPerson->refFamilyId = '';
            $newPerson->refPersonId = '';
            $newPerson->familyIndex = $newPerson->familyIndex + $difference;
            $newPerson->shareUrl = md5($newPerson->name . Util::generateRandomCode(24));
            $newPerson->level = $newPerson->level + $levelDifference;
            $newPerson->userId = $isSetUserId ? $newPerson->userId : 0;

            if ($newPerson->birthday == null || $newPerson->birthday == '') {
                $newPerson->birthday = "NULL";
            } else {
                $newPerson->birthday = "'$newPerson->birthday'";
            }
            if ($newPerson->deadTime == null || $newPerson->deadTime == '') {
                $newPerson->deadTime = "NULL";
            } else {
                $newPerson->deadTime = "'$newPerson->deadTime'";
            }
            if ($newPerson->infoCardId == null || $newPerson->infoCardId == '') {
                $newPerson->infoCardId = 'NULL';
            } else {
                $newPerson->infoCardId = "'$newPerson->infoCardId'";
            }
            if ($newPerson->confirm == null || $newPerson->confirm == '') {
                $newPerson->confirm = 'NULL';
            } else {
                $newPerson->confirm = "'$newPerson->confirm'";
            }

            //清空其他关系
            //same_personId,father,mother,son,daughter,spouse,
            $newPerson->same_personId = 0;
            $newPerson->father = $this->MappingPersonIdForCopyBranch(Util::idSplit($newPerson->father),$peopleDict,true);
            $newPerson->mother = $this->MappingPersonIdForCopyBranch(Util::idSplit($newPerson->mother),$peopleDict,true);
            $newPerson->son = $this->MappingPersonIdForCopyBranch(Util::idSplit($newPerson->son),$peopleDict,true);
            $newPerson->daughter = $this->MappingPersonIdForCopyBranch(Util::idSplit($newPerson->daughter),$peopleDict,true);
            $newPerson->spouse = $this->MappingPersonIdForCopyBranch(Util::idSplit($newPerson->spouse),$peopleDict,true);
            $newPerson->brother = $this->MappingPersonIdForCopyBranch(Util::idSplit($newPerson->brother),$peopleDict,true);
            $newPerson->sister = $this->MappingPersonIdForCopyBranch(Util::idSplit($newPerson->sister),$peopleDict,true);

            $newPeopleList[] = $newPerson;

            $sqlForInsert .= "(
            '$newPerson->id', '$newPerson->zi','$newPerson->zpname','$newPerson->remark','$newPerson->branchId','$newPerson->name','$newPerson->type',
           '$newPerson->profileText','$newPerson->sideText','$newPerson->country','$newPerson->countryName','$newPerson->province','$newPerson->provinceName',
           '$newPerson->city','$newPerson->cityName','$newPerson->area','$newPerson->areaName','$newPerson->town','$newPerson->townName','$newPerson->address',
           $newPerson->birthday,'$newPerson->gender','$newPerson->photo','$newPerson->shareUrl','$newPerson->isDead',$newPerson->deadTime,'$newPerson->level',
           $newPerson->infoCardId, '0', '$newPerson->isAdoption', '$newPerson->refFamilyId', '$newPerson->refPersonId',
           $newPerson->confirm, '$newPerson->bloodType', '$newPerson->maritalStatus', '$newPerson->phone', '$newPerson->qq', '$newPerson->qrcode',
           '$newPerson->familyId','$newPerson->userId','$newPerson->ranking','$userId',now(),'$userId',now(),$newPerson->familyIndex,
           $newPerson->isNotMain,$newPerson->bicolor, '$newPerson->burialPlace', '$newPerson->birthPlace',
           '$newPerson->mother', '$newPerson->father', '$newPerson->son', '$newPerson->daughter', '$newPerson->spouse', '$newPerson->brother', 
           '$newPerson->sister'
           ),";

            //记录id和过继关系 无需记录is_adoption 直接赋值
            $record['ids'][] = [$oldPersonId => $newPerson->id];
            if ($newPerson->samePersonId > 0) {
                $record['samePersons'][$newPerson->id] = $newPerson->samePersonId;
            }
            if ($newPerson->userId > 0 && $isSetUserId) {
                $record['userIds'][$newPerson->id] = $newPerson->userId;
            }
        }


//        print_r($peopleDict);
//        foreach ($peopleDict as $k=>$v){
//            print_r($personDB->getPersonObjectById($v));
//        }
//        var_dump(222);
//        print_r($sqlForInsert);
        $insertId = $people ? $this->pdo->insert(substr($sqlForInsert,0,-1)):0;
        if ($insertId === null){
            $record['error_code'] = false;
            $record['error_msg'] = '插入新用户失败';
            return array_merge($record, ['peopleList'=>$newPeopleList]);
        }

        // 添加person_pdf表的内容
        $sqlForPdfTile = "select * from gener_person_pdf_title where family_id = $oldFamilyId and is_delete = 0 ";
        $pdfTileDataList = $this->pdo->query($sqlForPdfTile);

        if ($pdfTileDataList){
            $sqlForInsertPdfTitle = "INSERT INTO gener_person_pdf_title(
             person_id, family_id,create_by,update_by,title,branch_name,zan,zan_type
            )VALUES";

            foreach ($pdfTileDataList as $title){
                if (array_key_exists($title['person_id'], $peopleDict)){
                    $sqlForInsertPdfTitle .= "({$peopleDict[$title['person_id']]},$newFamilyId,$userId,$userId,'{$title['title']}',
                    '{$title['branch_name']}','{$title['zan']}','{$title['zan_type']}'),";
                }
            }
            if (substr($sqlForInsertPdfTitle,-1) === ','){
                $insertIdForPdf = $this->pdo->insert(substr($sqlForInsertPdfTitle,0,-1));
                if (!$insertIdForPdf){
                    $record['error_code'] = false;
                    $record['error_msg'] = '插入新用户失败';
                    return array_merge($record, ['peopleList'=>$newPeopleList]);
                }
            }
        }

        // 绑定的人物添加权限
        if ($isSetUserId && $record['userIds']){
            $userIds = $record['userIds'];
            $sqlForPermission = "SELECT userId, permissionType as permission from $this->TABLE_PERMISSION where familyId = $newFamilyId and is_delete = 0";
            $newFamilyPermission = $this->pdo->query($sqlForPermission);
            $notCreatePermissionList = [];
            $updatePermissionList = [];
            foreach ($newFamilyPermission as $p){
                if ($p['permission'] < 200){
                    $updatePermissionList[] = $p['userId'];
                }else{
                    $notCreatePermissionList[] = $p['userId'];
                }
            }


            $sqlForPermission = "INSERT INTO $this->TABLE_PERMISSION(userId,familyId,permissionType,create_by,update_by) VALUES ";
            foreach ($userIds as $u){
                if (!in_array($u, $notCreatePermissionList)){
                    $sqlForPermission .= "($u, $newFamilyId, 200, $userId, $userId),";
                }
            }
            if (substr($sqlForPermission,-1) === ','){
                $insertIdForPermission = $this->pdo->insert(substr($sqlForPermission,0,-1));
                if (!$insertIdForPermission){
                    $record['error_code'] = false;
                    $record['error_msg'] = '插入新用户失败';
                    return array_merge($record, ['peopleList'=>$newPeopleList]);
                }
            }

            if ($updatePermissionList){
                $sqlForUpdataPermission = "UPDATE $this->TABLE_PERMISSION set permissionType = 200,update_by = $userId
                        where familyId = $newFamilyId and userId in ( " . implode(',', $updatePermissionList) . " ) ";
                $updateRow = $this->pdo->update($sqlForUpdataPermission);
            }
        }

//        var_dump(3333);
        #  更新linkperson的son,daughter字段与子女的mother or father字段
        if ($linkPerson->gender == 0){

            $sql = "select id from $this->TABLE_PERSON where familyId = $newFamilyId and is_delete = 0 and gender = 1 and mother = '@$linkPerson->id|'";
            $son = $this->pdo->query($sql);
//            print_r($sql."\n");
            $sql = "select id from $this->TABLE_PERSON where familyId = $newFamilyId and is_delete = 0 and gender = 0 and mother = '@$linkPerson->id|'";
            $daughter = $this->pdo->query($sql);
//            print_r($sql."\n");
        }else{

            $sql = "select id from $this->TABLE_PERSON where familyId = $newFamilyId and is_delete = 0 and gender = 1 and father = '@$linkPerson->id|'";
            $son = $this->pdo->query($sql);
//            print_r($sql."\n");
//            print_r($son);
            $sql = "select id from $this->TABLE_PERSON where familyId = $newFamilyId and is_delete = 0 and gender = 0 and father = '@$linkPerson->id|'";
            $daughter = $this->pdo->query($sql);
//            print_r($sql."\n");
//            print_r($daughter);
        }
        $son = Util::arrayToRelation(array_column($son,'id'));
        $daughter = Util::arrayToRelation(array_column($daughter,'id'));

        $sql = "update $this->TABLE_PERSON set son = '$son', daughter = '$daughter' 
            where is_delete = 0 and id = $linkPerson->id";
//        print_r($sql);

        $this->pdo->update($sql);


        $record['error_code'] = true;
        return array_merge($record, ['peopleList'=>$newPeopleList]);

    }

    /**
     * v3
     * v3
     * v3
     * @param childs 下级人物类 集合 包括：儿子 女儿 配偶
     * 1. 更新关系树
     * 2. 查询 下级人物类对应的复制前家族人物的下级，构造插入数据，插入
     * same_personId  (过继关系) userId(绑定用户)
     * @return otherRelations 过继关系
     */

    private function copyBranchPersonsV3($childs, $oldParent, $newFamilyId, $isSetUserId, $optionUser, $record, $difference)
    {
        $personDB = new CDBPerson();
        foreach ($childs as $key => $childId) {
            $oldPerson = $personDB->getPersonObjectById($childId);

            $child = clone $oldPerson;//构建新人物数据类
            $child->id = Util::getNextId();
            $child->familyId = $newFamilyId;
            $child->refFamilyId = '';
            $child->refPersonId = '';

            //清空其他关系
            //same_personId,father,mother,son,daughter,spouse,
            $child->same_personId = 0;
            $child->father = '';
            $child->mother = '';
            $child->son = '';
            $child->daughter = '';
            $child->spouse = '';

            //记录id和过继关系 无需记录is_adoption 直接赋值
            $record['ids'][] = [$oldPerson->id => $child->id];
            if ($oldPerson->samePersonId > 0) {
                $record['samePersons'][$child->id] = $oldPerson->samePersonId;
            }
            if ($child->userId > 0) {
                $record['userIds'][$child->id] = $child->userId;
            }

            if ($oldPerson->type == 2) {//用oldtype 判断是否是配偶
                //配偶不需要再查询
                $child->childs = [];
                $child->oldSpouse = [];
                $child->relation = $GLOBALS['RELATION_SPOUSE'];
            } else {
                //循环体
                $child->childs = array_unique(array_merge($oldPerson->son, $oldPerson->daughter));// 去重复，防止数据重复出错
                $child->oldSpouse = $oldPerson->spouse;
                if ($child->gender == 1) {//男
                    $child->relation = $GLOBALS['RELATION_SON'];
                } else {
                    $child->relation = $GLOBALS['RELATION_DAUGHTER'];
                }
            }


            $child->corePersonId = $oldParent['newId'];
            if ($isSetUserId == 0) {
                $child->userId = 0;
            } else {
                //检查是否新家族存在次userId
                if ($child->userId > 0) {
                    $sql = "SELECT id from $this->TABLE_PERSON WHERE familyId = '$newFamilyId' AND userId = '$child->userId'";
                    $check = $this->pdo->uniqueResult($sql);
                    if (count($check) > 0) {
                        $record['error_code'] = false;
                        $record['error_msg'] = '用户已经在新家族中，请勿重新绑定';
                        return $record;
                    }
                }
            }

            //添加关系
            $insertId = $personDB->addRelatePersonV3($child, $child->familyIndex+$difference, false, true); //添加关联人物会插入数据 所以无需再插入

            if ($insertId <= 0) {//插入失败
                $record['error_code'] = false;
                $record['error_msg'] = '插入新用户失败';
                return $record;
            }

            //查询下级,构建循环体需要的参数
            //上级的配偶也放在下级一起循环
            $nextChilds = array_merge($child->childs, $child->oldSpouse);
            if (!empty($nextChilds)) {
                $nextParent = [
                    'gender' => $child->gender,
                    'newId' => $child->id
                ];
                $record = $this->copyBranchPersonsV3($nextChilds, $nextParent, $newFamilyId, $isSetUserId, $optionUser, $record, $difference);
            }
        }
        return $record;
    }

    /*
     * v3
     * v3
     * v3
     * 修改familyIndex，仅限复制分支使用
     * @param $oldPerson 被复制的人物起点
     * @param $newParent 复制的家族的父母顶点
     * */
     function updateFamilyIndexForCopyBranch($oldPerson, $newParent){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        #开始变更familyIndex
        $personDB = new CDBPerson();
        $oldPersonIndex = $oldPerson->familyIndex;
        $oldPersonLastIndex = $personDB->getLastPersonFamilyIndexV3($oldPerson->familyId,$oldPerson->level,$oldPersonIndex);
        $personCount = $oldPersonLastIndex - $oldPersonIndex + 1;
        $userId = $GLOBALS['userId'];

        $newParentIndex = $newParent->familyIndex;
//            $newParentLastIndex = $personDB->getLastPersonFamilyIndexV3($newParent->familyId,$newParent->level,$newParentIndex);
        if ($newParent->son || $newParent->daughter){
            $sqlBrotherSister = "select id,family_index as  familyIndex,level from $this->TABLE_PERSON where id in ( " . implode(',',array_merge($newParent->son,$newParent->daughter)) . " ) and is_delete = 0 order by family_index desc limit 1";
            $brotherAndSister = $this->pdo->uniqueResult($sqlBrotherSister);
            $newParentIndex = $personDB->getLastPersonFamilyIndexV3($newParent->familyId,$brotherAndSister['level'],$brotherAndSister['familyIndex']);
//                var_dump($toPersonIndex);
//                var_dump($sqlBrotherSister);
        }

        $sql = "update $this->TABLE_PERSON set family_index = family_index + $personCount, update_by = $userId,update_time = now() where familyId = $newParent->familyId and family_index > $newParentIndex and is_delete = 0";
//        print_r($sql);
        return [$this->pdo->update($sql),$newParentIndex];
    }

    /**
     * v3
     * v3
     * v3
     * 删除分支
     * 2019-06-20
     * @param $personId 删除的节点id
     * @return array
     *分支删除分三部 1：断开树; 2 子树设为is_delete = 1; 3 取消用户和人物之间的绑定; 4 记录
     */
    public function deleteBranchV3($personId, $familyId, $optionUser = 0)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        //如果之前删除过，直接调用之前的数据
        //可能需要更多的判断 如上次操作是删除还是恢复


        try {
            //开启事务处理
            $this->pdo->beginTransaction();
            //记录人物日志
            $personDB = new CDBPerson();
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $personDB->getPersonById($personId);
            //$familyId = $old_person->familyId;
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $option_detail = '删除人物及分支';
            $option_user = $GLOBALS['userId'] ?? 0;

            $childs = $this->readChilds($personId, $familyId);
//            //如果操作人自己在被删除的分支中，拒绝删除操作
//            if (in_array($optionUser, $childs['userIds'])) {
//                $this->pdo->rollback();
//                return ['error_code' => $GLOBALS['ERROR_PERMISSION'], 'error_msg' => '自己在分支中，不能删除'];
//            }
//            //如果创始人在被删除的分支中，拒绝删除操作
//            $originator = $this->getFamilyOriginatorId($familyId);
//            if (in_array($originator, $childs['userIds'])) {
//                $this->pdo->rollback();
//                return ['error_code' => $GLOBALS['ERROR_PERMISSION'], 'error_msg' => '家族创始人在分支中，不能删除'];
//            }

            #先变更familyIndex
            $person = $personDB->getPersonObjectById($personId);
            $personIndex = $person->familyIndex;
            $personLastIndex = $personDB->getLastPersonFamilyIndexV3($person->familyId,$person->level,$personIndex);
            $sql = "update $this->TABLE_PERSON set family_index = (family_index - $personLastIndex + $personIndex - 1),update_time = now(), update_by = $option_user  where familyId = $person->familyId and family_index > $personLastIndex and is_delete = 0";
//            print_r($sql);
            $updateFamilyIndex = $this->pdo->update($sql);


            //step one
            $res = $this->cutBranchTree($personId); //json关系
            if ($res['code'] == false) {
                $this->pdo->rollback();            //事务回滚
                $msg = $res['msg'];
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => $msg];
            }

            //step two 先写入日志拿到日志的insertId
            $parents = $personDB->getMainParent($person);
            $insertId = $this->recordBranch($personId, json_encode($childs), $res['msg'], $childs['familyId'], $optionUser, 'delete',0,$parents?$parents['id']:0, $parents?$parents['name']:'');
            if ($insertId == false) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => '插入日志失败'];
            }

            $other_msg = ['logId' => $insertId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);


            //step three
            if (!$this->setDeleteBranchV3($insertId, $childs, 1)) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '删除分支失败'];
            }
            //step four 取消用户和人物之间的绑定
            if (!empty($childs['userIds'])) {
                $personDao = new CDBPerson();
                foreach ($childs['userIds'] as $key => $val) {
                    $personDao->cancelUserBindForTmp($key, $childs['familyId'], $val, false);
                }
            }
            //step five 更新family的世代（level）
            $sql = "update $this->TABLE set min_level = (select min(level) from $this->TABLE_PERSON where familyId =$familyId and is_delete = 0 ),
                    max_level = (select max(level) from $this->TABLE_PERSON where familyId =$familyId and is_delete = 0 ) where id =$familyId and is_delete = 0";
            if ($this->pdo->update($sql) < 0){
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '删除分支失败。'];
            }
//            $insertId = $this->recordBranch($personId, json_encode($childs), $res['msg'], $childs['familyId'], $optionUser, 'delete');
//            if ($insertId == false) {
//                $this->pdo->rollback();            //事务回滚
//                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => '插入日志失败'];
//            }

            $other_msg = ['logId' => $insertId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '数据库事务提交失败'];
            //@codeCoverageIgnoreEnd
        }

//        删除搜索索引
        $CDBPushPerson = new CDBPushPerson();
        $personDB = new CDBPerson();
        foreach ($childs['persons'] as $personId) {
            $person = $personDB->getPersonById($personId, false);
            if (empty($person)) {
                continue;
            }
            $modelId = $personId;
            $action = 2;
            $source = $person;
            $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
        }

        return ['error_code' => $GLOBALS['ERROR_SUCCESS'], 'error_msg' => ''];
    }

    /**
     * v3
     * v3
     * v3
     * 还原分支
     * @param $personId 节点id
     * @param $optionUser 操作人id
     * @param $relation  关系id : father/mother => 123
     * @return bool
     * 分支还原分三部 1：连接树 2 子树设为is_delete = 0。3 添加用户和人物之间的绑定; 4 记录
     */
    public function reSetBranchV3($logId, $optionUser, $relation = '')
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $msg = $this->readBranchLog($logId, 'delete');
        if (empty($msg)) {//没有此人的记录
            return ['error_code' => $GLOBALS['ERROR_PERSON_EXISTS'], 'error_msg' => '还原失败，没有删除记录'];
        }
        $personId = $msg['personId'];
        $familyId = $msg['familyId'];
        //构建关系数组
        $relations = $msg['relations'];
        $parents = array_merge($relations['father'], $relations['mother']);
        if (!(empty($parents) && $relation <= 0)) {
            if ($relation > 0) {
                $sql = "SELECT id, gender, daughter, son, spouse, type, level,family_index as familyIndex FROM $this->TABLE_PERSON where id = $relation 
                AND type = 1 AND familyId = $familyId AND is_delete = 0 ";
                $relations['isNewRelation'] = true;
            } else {
                //考虑到父系主支可能有删除或者更新，所以关系需要重新构建
                $str = implode(',', $parents);
                $sql = "SELECT id, gender, daughter, son, spouse, type, level,family_index as familyIndex FROM $this->TABLE_PERSON where id in($str) 
                AND type = 1 AND familyId = $familyId AND is_delete = 0 ";
                $relations['isNewRelation'] = false;
            }


            $result = $this->pdo->uniqueResult($sql);
            if (empty($result)) {//还原的父系不在本族主线中 或者已经被删除
                return ['error_code' => $GLOBALS['ERROR_PERSON_EXISTS'], 'error_msg' => '还原失败，父系不存在'];
            }
            $relations['brother'] = Util::relationSplit($result['son']);
            $relations['sister'] = Util::relationSplit($result['daughter']);
            $spouse = Util::relationSplit($result['spouse']);

            $relation = $result['id'];
            $msg['relations']['toPersonId'] = $relation;
            $relations['toPersonId'] = $relation;

            if ($result['gender'] == 1) {
                $relations['father'] = [$relation];
                $relations['mother'] = $spouse;
            } else {
                $relations['father'] = $spouse;
                $relations['mother'] = [$relation];
            }
            //添加自己进入数组
            $sql = "SELECT gender FROM $this->TABLE_PERSON where id = $personId AND familyId = $familyId ";
            $self = $this->pdo->uniqueResult($sql);
            if (empty($self)) {
                return ['error_code' => $GLOBALS['ERROR_PERSON_EXISTS'], 'error_msg' => '还原失败，还原分支不存在，或者数据出错'];
            }
            if ($self['gender'] == 1) {
                $relations['brother'] = array_unique(array_merge($relations['brother'], [$personId]));
            } else {
                $relations['sister'] = array_unique(array_merge($relations['sister'], [$personId]));
            }
            $relations['parentLevel'] = $result['level'];
        } else {
            return ['error_code' => $GLOBALS['ERROR_PERSON_EXISTS'], 'error_msg'  => '还原失败，父系不存在'];
        }
        //如果没有父系母系，并且没有指定关系


        try {
            //开启事务处理
            $this->pdo->beginTransaction();

            //记录人物日志
            $personDB = new CDBPerson();
            $function_name = __CLASS__ . '->' . __FUNCTION__;
            $old_person = $personDB->getPersonById($personId);
            //$familyId = $old_person->familyId;
            $old_data = json_encode($old_person, JSON_UNESCAPED_UNICODE);
            $option_detail = '还原人物及分支';
            $option_user = $GLOBALS['userId'] ?? 0;

            # 先将familyIndex还原
            $personForIndex = $personDB->getPersonObjectById($personId,false);
            $personIndex = $personForIndex->familyIndex;
//            $personLastIndex = $personDB->getLastPersonFamilyIndex($personForIndex->familyId,$personForIndex->level,$personIndex);
            $sqlForPersonCount = "select count(*) as count from $this->TABLE_PERSON where logId = $logId and is_delete = 1";
            $personCount = ($this->pdo->uniqueResult($sqlForPersonCount))['count'];
            $relationForIndex = $personDB->getPersonObjectById($relation);
            $relationIndex = $relationForIndex->familyIndex;
            if ($relationForIndex->son || $relationForIndex->daughter){
                #获取relation的最后一个子女，把恢复的分支插入最后一个位置
                $sqlBrotherSister = "select id,family_index as  familyIndex,level from $this->TABLE_PERSON where id in ( " . implode(',',array_merge($relationForIndex->son,$relationForIndex->daughter)) . " ) and is_delete = 0 order by family_index desc limit 1";
                $brotherAndSister = $this->pdo->uniqueResult($sqlBrotherSister);
                $relationIndex = $personDB->getLastPersonFamilyIndexV3($relationForIndex->familyId,$brotherAndSister['level'],$brotherAndSister['familyIndex']);
            }
//            if ($relationForIndex->son || $relationForIndex->daughter){
//                #先获取所有排行
//                $sqlBrotherSister = "select family_index as familyIndex,level,ranking from $this->TABLE_PERSON
//                    where id in ( " . implode(',',array_merge($relationForIndex->son,$relationForIndex->daughter)) . " )
//                    and is_delete = 0 order by family_index";
//                $rankings = $this->pdo->query($sqlBrotherSister);
//                $n = 0;
//                #人物删除前的排行与现有的排行做比较，插入当中
//                foreach ($rankings as $k => $ranking){
//                    if ($ranking['ranking'] >= $personForIndex->ranking){
//                        $n += 1;
//                        if ($k == 0){
//                            break;
//                        }
//                        $relationIndex = $rankings[$k-1]['familyIndex'];
//                        break;
//                    }
//                }
//                if ($n === count($rankings)){
//                    $relationIndex = $rankings[count($rankings)-1]['familyIndex'];
//                    $relationIndex = $personDB->getLastPersonFamilyIndexV3($relationForIndex->familyId,$relationForIndex->level + 1,$relationIndex);
//                }elseif ($n === 1){
//                    $relationIndex = $rankings[0]['familyIndex'];
//                }else{
//                    $relationIndex = $personDB->getLastPersonFamilyIndexV3($relationForIndex->familyId,$relationForIndex->level + 1,$relationIndex);
//                }
//            }
            $sql = "update $this->TABLE_PERSON set family_index = family_index + $personCount where familyId = $familyId and is_delete = 0 and family_index > $relationIndex";
//            print_r($sql."\n");
            $updateIndex = $this->pdo->update($sql);
            $difference =  $relationIndex - $personIndex + 1;
            $sql = "update $this->TABLE_PERSON set family_index = family_index + $difference where logId = $logId and is_delete = 1";
//            print_r($sql);
            $updateIndex = $this->pdo->update($sql);

            # 更新旧人物的ranking为目前排行的最大ranking+1
            $rankings = $personDB->getChildrenRanking($relation);
            $ranking = $rankings ? end($rankings):0;
            $sql = "update $this->TABLE_PERSON set ranking = $ranking + 1 where id = $personId";
            $updateRanking = $this->pdo->update($sql);
            if ($updateRanking < 0){
                $this->pdo->rollback();
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '还原分支失败'];
            }


            //step one
            $linkRes = $this->linkBranchTree($personId, $relations);
            if ($linkRes['code'] == false) {
                $this->pdo->rollback();            //事务回滚
                $msg = $linkRes['msg'];
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => $msg];
            }
            //step two
            $childs = $msg['persons'];
            $childs['parentRelations'] = $relations;
            $childs['optionUser'] = $optionUser;
            if (!$this->setDeleteBranch($childs, 0)) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '还原分支失败'];
            }
            //step three
            if (!empty($msg['userIds'])) {
                foreach ($msg['userIds'] as $key => $val) {
                    $sql = "SELECT id FROM $this->TABLE_PERSON WHERE userId = '$val' AND is_delete = '0' AND familyId = '$familyId' ";
                    $result = $this->pdo->uniqueResult($sql);
                    if ($result != null) {
                        //表示这个用户已经绑定家族中某个人
                        continue;
                    }
                    //恢复TABLE_PERSON 表中数据
                    $sql = "UPDATE $this->TABLE_PERSON SET userId = '$val',update_time = now() WHERE familyId='$familyId' AND id='$key' AND isDelete = 0";
                    $this->pdo->update($sql);
                    //恢复管理员身份
                    $deleteSql = "UPDATE $this->TABLE_ADMIN SET isDelete=0 WHERE familyId='$familyId' AND adminId='$val' AND isDelete = 0";
                    $this->pdo->update($deleteSql);

                    // 删除family_user表中对应数据
                    $sql = "UPDATE $this->TABLE_FAMILY_USER SET is_delete = 1, update_time = now()  
                    WHERE userId = '$val' AND familyId = '$familyId' ORDER BY id DESC LIMIT 1 ";
                    $this->pdo->update($sql);

                    //更新用户资料的家族数
                    $sql = "UPDATE $this->TABLE_USER_PROFILE SET family_num = family_num + 1,update_time = now() WHERE userId = '$val'";
                    $this->pdo->update($sql);

                }
            }
            //step four
            $insertId = $this->recordBranch($personId, json_encode($msg['persons']), json_encode($msg['relations']), $familyId, $optionUser, 'reset', $logId);
            if ($insertId == false) {
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_INSERT'], 'error_msg' => '插入日志失败'];
            }

            //step five 更新family的世代（level）
            $sql = "update $this->TABLE set min_level = (select min(level) from $this->TABLE_PERSON where familyId =$familyId and is_delete = 0 ),
                    max_level = (select max(level) from $this->TABLE_PERSON where familyId =$familyId and is_delete = 0 ) where id =$familyId and is_delete = 0";
            if ($this->pdo->update($sql) < 0){
                $this->pdo->rollback();            //事务回滚
                return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '删除分支失败。'];
            }

            $other_msg = ['resetLogId' => $insertId, 'lastLogId' => $logId];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $personDB->recordPersonOptionLog($personId, $familyId, $function_name, $old_data, $option_detail, $option_user, $other_msg);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return ['error_code' => $GLOBALS['ERROR_SQL_UPDATE'], 'error_msg' => '数据库事务提交失败'];
            //@codeCoverageIgnoreEnd
        }


//        恢复搜索索引
        $CDBPushPerson = new CDBPushPerson();
        $personDB = new CDBPerson();
        foreach ($msg['persons']['persons'] as $personId) {
            $person = $personDB->getPersonById($personId);
            if (empty($person)) {
                continue;
            }
            $modelId = $personId;
            $action = 1;
            $source = $person;
            $CDBPushPerson->setPushPersonTask($modelId, $action, $source);
        }

        return ['error_code' => $GLOBALS['ERROR_SUCCESS'], 'error_msg' => ''];
    }


    /**
     * v3
     * v3
     * v3
     * 设置is_delete并记录logId
     * @param $personsId 子节点集
     * @param $is_delete 0 1
     * @return bool
     */
    private function setDeleteBranchV3($logId, $childs = [], $is_delete = 1)
    {
        $familyId = $childs['familyId'];
        $personIds = implode(',', $childs['persons']);

        //设置persons is_delete
        if (empty($childs['persons'])) {
            return false;
        }

        $deleteNum = count($childs['persons']);
        if ($is_delete == 1) {

            //更新家族人数
            $sql = "UPDATE $this->TABLE SET member_count = member_count - $deleteNum WHERE id='$familyId' AND is_delete = 0 ";
            $this->pdo->update($sql);

            //取消过继关系 全部清空 应需要再改进

            if (!empty($childs['Adoptions']['Adoption'])) {
                $AdoptionsArr = array_merge($childs['Adoptions']['Adoption'], $childs['Adoptions']['Adoptioned']);
                $AdoptionsIds = implode(',', $AdoptionsArr);
                $sql = "UPDATE $this->TABLE_PERSON SET is_adoption = 0, same_personId = 0 WHERE id in ($AdoptionsIds) ";
                $this->pdo->update($sql);
            }

            //再删除家族人物
            $sql = "UPDATE $this->TABLE_PERSON SET is_delete = 1, logId = $logId, update_time = now() WHERE id in ($personIds) ";
//            print_r($sql);
            $this->pdo->update($sql);

            // 删除人物的分支记录
            $branchSql = "UPDATE $this->TABLE_FAMILY_INNER_BRANCH_PERSON SET isDelete = 1 WHERE personId in ($personIds) ";
            $this->pdo->update($branchSql);


        } else {
            //先恢复家族人物
            $sql = "UPDATE $this->TABLE_PERSON SET is_delete = 0, update_time = now() WHERE id in ($personIds) ";
            $this->pdo->update($sql);

            //更新家族人数
            $sql = "UPDATE $this->TABLE SET member_count = member_count + $deleteNum WHERE id='$familyId' AND is_delete = 0 ";
            $this->pdo->update($sql);

            //恢复过继关系
            if (!empty($childs['Adoptions']['Adoption'])) {
                foreach ($childs['Adoptions']['Adoption'] as $key => $val) {
                    //过继后的id
                    $same_personId = $childs['Adoptions']['Adoptioned'][$key];
                    //如果过继前的人物和过继后的人物都在子分支中才恢复
                    if (in_array($val, $childs['persons']) && in_array($same_personId, $childs['persons'])) {
                        //过继前
                        $sql = "UPDATE $this->TABLE_PERSON SET  same_personId = '$same_personId', is_adoption = 1 WHERE id = '$val' ";
                        $this->pdo->update($sql);

                        //过继后
                        $sql_same = "UPDATE $this->TABLE_PERSON SET  same_personId = '$val', is_adoption = 2 WHERE id = '$same_personId' ";
                        $this->pdo->update($sql_same);
                    }

                }
            }

            //恢复人物分支记录
            $relations = $childs['parentRelations'];
            $personDB = new CDBPerson();
            $userId = $childs['optionUser'];
            $parents = array_merge($relations['father'], $relations['mother']);
            //查找最高节点的分支信息，然后添加
            $parentId = $parents[0];
            $branchIds = $personDB->getPersonBranchList($parentId);
            if (count($branchIds) > 0) {
                foreach ($childs['persons'] as $personId) {
                    $firstBranch = $branchIds[0]['branchId'];
                    $sql = "INSERT INTO $this->TABLE_FAMILY_INNER_BRANCH_PERSON (branchId,personId,createBy,createTime,updateBy,updateTime)VALUES 
                    ('$firstBranch', '$personId', '$userId', now(), '$userId', now())";
                    for ($i = 1; $i < count($branchIds); $i++) {
                        $nextBranchId = $branchIds[$i]['branchId'];
                        $sql .= ",('$nextBranchId', '$personId', '$userId', now(), '$userId', now())";
                    }
                    $this->pdo->insert($sql);
                }
            }


        }

        return true;

    }

    /**
     * V3
     * V3
     * V3
     * 分页获取家族导出的pdf列表
     * @param $familyId
     * @param $pageIndex
     * @param $pageSize
     * @return mixed
     */
    public function getExportFamilyPdfPagingV3($familyId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT id,familyId,preview_pdf,print_pdf,create_time,create_by,update_time,update_by,null as photo,null as type, null as branch 
                FROM gener_pdf_task WHERE finish = 1 AND familyId = $familyId ORDER BY id DESC
                LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    /**
     * 
     * 合并两个家族生成新家族
     * @param  fromPersonId   从哪里
     * @param toPersonId    合并到哪里
     * @version 3
     * @author yuanxin
     * @return int updateRow
     */
    public function mergeFamilysToNewFamily($fromPersonId,$toPersonId) {
        try {
            $PERSON_NOT_EXIST_ERROR = -1;
            $PERSON_SAME_FAMILY_ERROR = -2;
            $result = -1;
            $personDB = new  CDBPerson();
            $return  =  -1;
            $fromPerson=$personDB->getPersonObjectById($fromPersonId);
            $toPerson = $personDB->getPersonObjectById($toPersonId);
            if ( is_null($fromPerson) ||  is_null($toPerson)) {
                //有人物不存在，直接返回错误
                return $PERSON_NOT_EXIST_ERROR;
            }
            if($fromPerson->familyId == $toPerson->familyId) {
                //家族相同，返回错误
                return $PERSON_SAME_FAMILY_ERROR;
            }
            $userId = $GLOBALS['userId'];
           // 获取from家族的所有人
            $lastPersonFamilyIndexB = $personDB->getLastPersonFamilyIndex($fromPerson->familyId, $fromPerson->level, $fromPerson->familyIndex);
            $familyPeopleB = $personDB->getFamilyPersonsByFamilyIndex($fromPerson->familyId, $fromPerson->familyIndex, $lastPersonFamilyIndexB);
            // 获取to家族的分支所有人
            $toPersonLastFamilyIndex = $personDB->getLastPersonFamilyIndex($toPerson->familyId, $toPerson->level, $toPerson->familyIndex);
            $toPersons = $personDB->getFamilyPersonsByFamilyIndex($toPerson->familyId, $toPerson->familyIndex, $toPersonLastFamilyIndex);

            // to家族偏移量
           $dIndex = $lastPersonFamilyIndexB - $fromPerson->familyIndex - $toPersonLastFamilyIndex + $toPerson->familyIndex;
            // 处理to家族数据库
            $deleteSql = "UPDATE  $this->TABLE_PERSON  SET is_delete = 1 WHERE familyId = $toPerson->familyId  AND  family_index 
                                                                                                                                        BETWEEN  $toPerson->familyIndex AND  $toPersonLastFamilyIndex;";
            $updateSql = "UPDATE  $this->TABLE_PERSON  SET family_index  =  family_index + $dIndex 
                                                                WHERE familyId = $toPerson->familyId AND family_index > $toPerson->familyIndex AND is_delete = 0; ";

            $this->pdo->beginTransaction();
            $this->pdo->update($deleteSql);
            $this->pdo->update($updateSql);
            $stack =  array();
            //修改合并下所有人物的信息
            foreach($familyPeopleB as $key => $person){
                $familyPeopleB[$key]['familyId'] = $toPerson->familyId;
                $nextId = Util::getNextId();
                $stack[$person['id']] = $nextId;
                $familyPeopleB[$key]['id'] =$nextId;
                $familyPeopleB[$key]['familyIndex'] += $toPerson->familyIndex - $fromPerson->familyIndex;
                $familyPeopleB[$key]['level'] += $toPerson->level - $fromPerson->level;
                $familyPeopleB[$key]['shareUrl'] = md5($person['name'] . Util::generateRandomCode(24));    //生成唯一share_url的code
            }


     
            foreach($familyPeopleB as $key => $person){
                $fatherArray = array();
                foreach($person['father'] as $father){
                    array_push($fatherArray,$stack[$father]);
                }
                $familyPeopleB[$key]['father'] = $fatherArray;
                
                $motherArray = array();
                foreach($person['mother'] as $mother){
                    array_push($motherArray,$stack[$mother]);
                }
                $familyPeopleB[$key]['mother'] = $motherArray;
                
                $brotherArray = array();
                foreach($person['brother'] as $brother){
                    array_push($brotherArray,$stack[$brother]);
                }
                $familyPeopleB[$key]['brother'] = $brotherArray;
                
                $sisterArray = array();
                foreach($person['sister'] as $sister){
                    array_push($sisterArray,$stack[$sister]);
                }
                $familyPeopleB[$key]['sister'] = $sisterArray;
                
                $sonArray = array();
                foreach($person['son'] as $son){
                    array_push($sonArray,$stack[$son]);
                }
                $familyPeopleB[$key]['son'] = $sonArray;
              
                $daughterArray = array();
                foreach($person['daughter'] as $daughter){
                    array_push($daughterArray,$stack[$daughter]);
                }
                $familyPeopleB[$key]['daughter'] = $daughterArray;
              
                $spouseArray = array();
                foreach($person['spouse'] as $spouse){
                    array_push($spouseArray,$stack[$spouse]);
                }
                $familyPeopleB[$key]['spouse'] = $spouseArray;
            }

      

                // 修改关联人物信息
                $familyPeopleB[0]['father'] = $toPerson->father;
                $familyPeopleB[0]['mother'] = $toPerson->mother;
                $familyPeopleB[0]['brother'] = $toPerson->brother;
                $familyPeopleB[0]['sister'] = $toPerson->sister;
                $familyPeopleB[0]['ranking'] = $toPerson->ranking;
                $familyPeopleB[0]['refPerson'] = $toPerson->ranking;
                $familyPeopleB[0]['ranking'] = $toPerson->ranking;


            if($fromPerson->gender == $GLOBALS['GENDER_MALE']){
                foreach($familyPeopleB[0]['brother'] as $key => $brother){
                    if($brother ==  $toPerson->id){
                        $familyPeopleB[0]['brother'][$key] = $familyPeopleB[0]['id'];
                    }
                }
            }else{
                foreach($familyPeopleB[0]['sister'] as $key => $sister){
                    if($sister ==  $toPerson->id){
                        $familyPeopleB[0]['sister'][$key] = $familyPeopleB[0]['id'];
                    }
                }
            }
            /**************************************************处理人物数据库字符********************************************* */

            for ($i = 0; $i < count($familyPeopleB); $i++) {
                $person = new Person($familyPeopleB[$i]);        //转换为person对象
                if($person->refFamilyId == array()){
                    $refFamilyId =  '';
                    $refPersonId =  '';
                }else{
                    $refFamilyId = json_encode($person->refFamilyId);
                   $refPersonId = json_encode($person->refPersonId);
                }
                if($person->father ==  array() ||  $person->father == ''){
                    $father =  '';
                }else{
                    $father = '';
                    foreach($person->father as $personFather){
                        $father = $father.'@'.$personFather.'|';
                   }
                }
                if($person->mother == array() || $person->mother == ''){
                    $mother =  '';
                }else{
                    $mother = '';
                    foreach($person->mother as $personMother){
                        $mother = $mother.'@'.$personMother.'|';
                    }
               }
                if($person->son == array() || $person->son == ''){
                    $son =  '';
                }else{
                    $son = '';
                    foreach($person->son as $personSon){
                        $son = $son.'@'.$personSon.'|';
                   }
                }
                if($person->daughter == array() || $person->daughter == ''){
                    $daughter =  '';
               }else{
                    $daughter = '';
                    foreach($person->daughter as $personDaughter){
                        $daughter = $daughter.'@'.$personDaughter.'|';
                    }
                }
                if($person->brother == array() || $person->brother == ''){
                    $brother =  '';
                }else{
                    $brother = '';
                    foreach($person->brother as $personBrother){
                        $brother = $brother.'@'.$personBrother.'|';
                    }
                }
                if($person->sister == array() || $person->sister == ''){
                    $sister =  '';
                }else{
                    $sister = '';
                    foreach($person->sister as $personSister){
                        $sister = $sister.'@'.$personSister.'|';
                    }
                }
                if($person->spouse == array() || $person->spouse == ''){
                    $spouse =  '';
                }else{
                    $spouse = '';
                    foreach($person->spouse as $personSpouse){
                        $spouse = $spouse.'@'.$personSpouse.'|';
                    }
                }
                if ($person->birthday == null || $person->birthday == '') {
                    $person->birthday = "NULL";
                } else {
                    $person->birthday = "'$person->birthday'";
                }
                if ($person->deadTime == null || $person->deadTime == '') {
                    $person->deadTime = "NULL";
                } else {
                    $person->deadTime = "'$person->deadTime'";
                }
                if ($person->infoCardId == null || $person->infoCardId == '') {
                    $person->infoCardId = 'NULL';
                } else {
                    $person->infoCardId = "'$person->infoCardId'";
                }
                if ($person->confirm == null || $person->confirm == '') {
                    $person->confirm = 'NULL';
                } else {
                    $person->confirm = "'$person->confirm'";
                }
                // 添加人物
                $insertSql  = "INSERT INTO $this->TABLE_PERSON(id, zi,zpname,remark,branchId,name,type,
                    profileText,sideText,country,country_name,province,province_name,
                    city,city_name,area,area_name,town,town_name,address,
                    birthday,gender,photo,share_url,is_dead,dead_time,level,
                    infoCardId,is_delete,is_adoption,ref_familyId,ref_personId,
                    sister,brother,father,mother,son,daughter,spouse,
                    confirm,blood_type,marital_status,phone,qq,qrcode,
                    familyId,userId,ranking,create_by,create_time,update_by,update_time,family_index,bicolor 
                    )
               VALUES(
                    '$person->id','$person->zi','$person->zpname','$person->remark','$person->branchId','$person->name','$person->type',
                   '$person->profileText','$person->sideText','$person->country','$person->countryName','$person->province','$person->provinceName',
                   '$person->city','$person->cityName','$person->area','$person->areaName','$person->town','$person->townName','$person->address',
                   $person->birthday,'$person->gender','$person->photo','$person->shareUrl','$person->isDead',$person->deadTime,'$person->level',
                   $person->infoCardId, 0, '$person->isAdoption', '$refFamilyId', '$refPersonId',
                  '$sister','$brother','$father','$mother','$son','$daughter','$spouse',
                   $person->confirm, '$person->bloodType', '$person->maritalStatus', '$person->phone', '$person->qq', '$person->qrcode',
                   $person->familyId,$person->userId,$person->ranking,$userId,now(),'$userId',now(),$person->familyIndex,0
                   )";

                 $result =  $this->pdo->insert($insertSql);
                //  var_dump($result);
            }
            /**************************************************处理to家族人物关系********************************************* */
            $count = 0;
            $from = '@' . $toPersonId . "|";
            $to = '@'.$familyPeopleB[0]['id'].'|';
            $records = array();
            $setParam = '';
            if($toPerson->gender == $GLOBALS['GENDER_MALE']){
                foreach($familyPeopleB[0]['brother'] as $person){
                   $setParam = 'brother';
                    if($person != $familyPeopleB[0]['id']){
                        $sql = "UPDATE $this->TABLE_PERSON  SET $setParam = REPLACE($setParam,'$from','$to') WHERE id = $person";
                        // var_dump($sql);
                        $this->pdo->update($sql);
                    }
                }
                foreach($familyPeopleB[0]['father'] as $person){
                   $setParam = 'son';
                    if($person != $familyPeopleB[0]['id']){
                        $sql = "UPDATE $this->TABLE_PERSON  SET $setParam = REPLACE($setParam,'$from','$to') WHERE id = $person";
                        // var_dump($sql);
                        $this->pdo->update($sql);
                    }
                }
            }else{
                foreach($familyPeopleB[0]['sister'] as $person){
                   $setParam = 'sister';
                    if($person != $familyPeopleB[0]['id']){
                        $sql = "UPDATE $this->TABLE_PERSON  SET $setParam = REPLACE($setParam,'$from','$to') WHERE id = $person";
                        $this->pdo->update($sql);
                    }
                }
                foreach($familyPeopleB[0]['mother'] as $person){
                   $setParam = 'daughter';
                    if($person != $familyPeopleB[0]['id']){
                        $sql = "UPDATE $this->TABLE_PERSON  SET $setParam = REPLACE($setParam,'$from','$to') WHERE id = $person ";
                        $this->pdo->update($sql);
                    }
                }
            }
            // $personDB->newUpdateRecord($records);
            $this->autoUpdateFamilyLevel($toPerson->familyId);
            //更新家族成员数
            // $MemerChange = dIndex
            $memberCountSql = "UPDATE $this->TABLE  SET member_count = member_count + $dIndex , update_time = now()  WHERE id = $toPerson->familyId";
             $this->pdo->update($memberCountSql);
            if($result>=0){
                   $this->pdo->commit();
                   return $familyPeopleB[0]['id'];
            }else{
                 $this->pdo->rollback();
                    return -1;
             }
        }catch(PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }
    }
       /**
     * 获取单条信息
     * //TODO 
     * 应该写一个model的
     * @param mergeId
     * @author yuanxin 
     * @version 3
     * @return array 
     */
  function getFamilyMergeInfoById($mergeId){
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $sql = "SELECT  id,src_familyId as fromFamilyId ,merge_familyId as toFamilyId,from_personId as fromPersonId,to_personId as toPersonId,
                        merge_way as mergeWay  FROM $this->TABLE_FAMILY_MERGE  WHERE  id  = $mergeId  AND  is_delete = 0;";
        return $this->pdo->query($sql);
    }

 



    /**
     * 获取家族所有关联，合并信息
     * @param familyId
     * @author yuanxin 
     * @version 3
     * @return array 
     */
    public function getFamilyMergeInfo($familyId){
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }

        $sql = "SELECT  id,src_familyId as fromFamilyId ,merge_familyId as toFamilyId,from_personId as fromPersonId,to_personId as toPersonId,
                        merge_way as mergeWay  FROM $this->TABLE_FAMILY_MERGE  WHERE is_delete = 0  AND  (src_familyId = $familyId or merge_familyId = $familyId);";
        return $this->pdo->query($sql);
    }

    /**
     * 读取家族所有的分支日志
     * @param $familyId 家族id
     * @param $name 搜索名字
     * @param string $orderBy 排序字段
     * @return array 日志信息列表
     *  persons 子节点集合 relations: 当前关系树 ..
     *
     */
    public function readBranchLogsV3($familyId,$name='',$orderBy='-record_time')
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $recordTableName = 'gener_family_branch_log';

        $nameSql = $name ? " and (op.nickname='$name' or p.name='$name') ":"";

        $orderByList = ['record_time','-record_time'];
        $orderBy = in_array($orderBy, $orderByList) ? $orderBy:'-record_time';
        $orderBy = $orderBy[0] === '-' ? substr($orderBy,1)." desc":"$orderBy asc";


        $sql = "SELECT distinct bl.id, bl.personId, p.name as person_name, p.photo as person_photo, bl.familyId, bl.options, bl.options_user, op.nickname as options_name, bl.record_time, bl.update_time, parentId, p2.name as parentName,p2.photo as parentPhoto 
        FROM $recordTableName as bl 
        LEFT JOIN $this->TABLE_PERSON as p on p.id = bl.personId
        LEFT JOIN $this->TABLE_USER as op on op.id = bl.options_user 
        LEFT JOIN $this->TABLE_PERSON as p2 on bl.parentId = p2.id 
        where bl.familyId = '$familyId' AND bl.options = 'delete' AND bl.last_logId = '0' $nameSql
        order by bl.$orderBy  limit 50";
        $res = $this->pdo->query($sql);

        return $res;
    }

    /**
     * 获取删除
     * @param $familyId 节点id
     * @param $familyId 节点id
     * @param $familyId 节点id
     * @param $familyId 节点id
     * @return array: id =>
     *  persons 子节点集合 relations: 当前关系树 ..
     *
     */
    public function readPeopleByBranchId($familyId,$logId,$page,$pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $recordTableName = 'gener_family_branch_log';

        $offset = ($page - 1) * $pageSize;

        # 为了兼容老版本
        $sql = "SELECT persons
        FROM $recordTableName 
        where familyId = $familyId AND id = $logId limit 1";
        $res = $this->pdo->uniqueResult($sql);
        $queryPersons = json_decode($res['persons'], true);
        $personsArray = $queryPersons['persons'];
        $personsString = implode(',', $personsArray);
        $sql = " SELECT SQL_CALC_FOUND_ROWS id, name, photo,level FROM $this->TABLE_PERSON WHERE id in ($personsString) order by level limit $offset,$pageSize";
        $sqlForCount = "SELECT FOUND_ROWS() as count";

        # 这是新版本的查询方法，后续可以改成这个
//        $sql = " SELECT SQL_CALC_FOUND_ROWS id, name, photo,level FROM $this->TABLE_PERSON WHERE familyId = $familyId and logId = $logId and is_delete = 1 order by level limit $offset,$pageSize";
//        $sqlForCount = "SELECT FOUND_ROWS() as count";
        $data = $this->pdo->query($sql);
        $count = $this->pdo->uniqueResult($sqlForCount)['count'];
        $sql = "select max(level) as maxLevel, min(level) as minLevel from $this->TABLE_PERSON WHERE id in ($personsString)";
        $level = $this->pdo->uniqueResult($sql);

        return [
            'page' =>$data ? $page:1,
            'count' => $count,
            'data' => $data,
            'minLevel'=> $level['minLevel'],
            'maxLevel'=> $level['maxLevel'],
        ];

    }


    /**
     * 根据家族id获取家族在世人数
     * @param $familyId 家族id
     * @param $isDead 是否在世
     * @return integer 在世人数
     */
    public function getAlivePeopleNumberByFamily($familyId, $isDead = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        # is_dead = 0 表示在世
        $sql = "SELECT count(*) as count FROM $this->TABLE_PERSON WHERE 
                familyId = '$familyId' and is_delete = 0 and is_dead = $isDead and is_adoption = 0 and bicolor = 0";

        return $this->pdo->uniqueResult($sql)['count'];
    }
}
