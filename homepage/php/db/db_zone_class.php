<?php
/**
 * 人物个人空间模块
 * @author jiangpengfei
 * @date 2016-12-25
 */

namespace DB;

use DB\CDBManager;
use DB\CDBPost;
use DB\CDBAccount;
use Model\Zone;
use Model\Post;
use Util\Util;
use Util\Paginator;

class CDBZone
{
    var $pdo = null;
    var $TABLE = "gener_zone";
    var $TABLE_USER = "gener_user";
    var $TABLE_BLACKLIST = "gener_user_blacklist";
    var $POST_TABLE = "gener_zonePost";
    var $COMMENT_TABLE = "gener_zoneComment";
    var $PERSON_TABLE = "gener_person";
    var $LIKE_TABLE = "gener_like";
    var $USER_TABLE = "gener_user";
    var $NEWS_TABLE = "gener_familyNews";
    var $COLLECTION_TABLE = "gener_collection";
    var $TABLE_USER_PROFILE = "gener_user_profile";

    /**
     * @codeCoverageIgnore
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 验证空间Id和用户id是否匹配，即用户是否有操作空间id的权限
     * @param $zoneId 空间id
     * @param $userId 用户id
     * @return bool true代表匹配，false代表不匹配
     */
    public function verifyZoneIdAndUserId($zoneId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE WHERE id = '$zoneId' AND userId = '$userId' ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result['count(id)'] > 0) {
            return true;
        }
        //@codeCoverageIgnoreStart
        return false;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 验证推文Id和用户id是否匹配，即用户是否有操作推文id的权限
     * @param $postId 推文id
     * @param $userId 用户id
     * @return bool true代表匹配，false代表不匹配
     */
    public function verifyPostIdAndUserId($postId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->POST_TABLE WHERE id = '$postId' AND userId = '$userId' AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);

        if ($result['count(id)'] > 0) {
            return true;
        }
        //@codeCoverageIgnoreStart
        return false;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 验证评论Id和用户id是否匹配，即用户是否有操作评论id的权限，推文的创建者可以删除推文下的评论
     * @param $commentId 评论id
     * @param $userId    用户id
     * @return bool true代表匹配，false代表不匹配
     */
    public function verifyCommentIdAndUserId($commentId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT module,userId FROM $this->COMMENT_TABLE WHERE id = '$commentId' AND isDelete = '0' ";

        $comment = $this->pdo->uniqueResult($sql);

        if ($comment == null) {
            return false;
        }

        if ($comment['userId'] == $userId) {
            // 推文、相册都可以删除自己的评论
            return true;
        }

        if ($comment['module'] == '5') {
            // 推文

            $sql = "SELECT pt.userId FROM $this->COMMENT_TABLE ct 
            INNER JOIN $this->POST_TABLE pt 
            on pt.id = ct.postId 
            WHERE ct.id = '$commentId' AND pt.isDelete = '0' ";

            $result = $this->pdo->uniqueResult($sql);

            if ($result == null)
                return false;

            if ($result['userId'] != $userId) {
                //如果评论的推文的创建者id不等于当前用户id，则没有权限
                return false;
            }

            return true;
        }

    }


    /**
     * 添加空间
     * @param $userId        空间绑定的用户id
     * @param $content        空间内容
     * @param $createTime    记录创建时间
     * @param $createBy    记录创建用户
     * @param $updateTime    记录更新时间
     * @param $updateBy        记录更新用户
     * @return int 创建的空间Id或者-1
     */
    public function addZone($userId, $content)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "INSERT INTO $this->TABLE(userId,content,create_time,create_by,update_time,update_by)VALUES('$userId','$content',now(),$userId,now(),$userId)";
        $id = $this->pdo->insert($sql);

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '添加空间';
        $other_msg = [
            'id' => $id,
            'content' => $content
           ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = $this->TABLE;
        $tableId = $id;
        $accountDB = new CDBAccount();
        $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        return $id;
    }

    /**
     * 编辑空间
     * @param $id            空间id
     * @param $content        空间内容
     * @param $canShare
     * @param $updateBy        更新人
     * @return int 受影响的记录数
     * @internal param 更新时间 $updateTime
     */
    public function updateZone($id, $content, $canShare, $updateBy)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set content = '$content',can_share = '$canShare', 
            update_time = now(),update_by = '$updateBy' WHERE id = '$id' ";

        $rows = $this->pdo->update($sql);
        return $rows;
    }


    /**
     * 删除空间
     * @param $id  空间id
     * @return int 删除的记录数
     */
    public function deleteZone($id)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set is_delete = '1' WHERE id = $id";

        return $this->pdo->update($sql);
    }

    /**
     * 根据用户id查询用户空间
     * @param $userId 用户id
     * @return object 查询的空间
     */
    public function getZoneByUserId($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,content,can_share as canShare,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy FROM $this->TABLE WHERE userId = '$userId' and is_delete = '0' ";

        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return new Zone($result);
        }
        return null;
    }

    /**
     * 根据空间id查询用户空间
     * @param $zoneId 空间id
     * @return object 查询的空间
     */
    public function getZoneByZoneId($zoneId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,content,can_share as canShare,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy FROM $this->TABLE WHERE id = '$zoneId'  and is_delete = '0'  ";

        $result = $this->pdo->uniqueResult($sql);

        if ($result != null) {
            return new Zone($result);
        }
        return null;
    }


    /**
     * 查询用户下的空间是否存在,防止重复添加
     * @param $userId 用户id
     * @return bool true空间存在false不存在
     */
    public function isZoneExist($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*) FROM $this->TABLE WHERE userId = '$userId'  and is_delete = '0' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['count(*)'] > 0) {
            return true;
        } else {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }
    }


    /**
     * 获取当前可以看到该条推文的用户集合
     * @TODO  这里的人物集合也可以处理成redis集合
     * @param $userId 用户id
     * @return array 人物id集合
     */
    public function getUsers($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT distinct userId FROM $this->PERSON_TABLE WHERE userId != '$userId' AND userId != '0' AND familyId in(SELECT familyId FROM $this->PERSON_TABLE WHERE userId = '$userId' )";
        return $this->pdo->query($sql);
    }

    /**
     * 获取指定家族中的用户集合（推文向指定家族发送)
     * @param $familyId 家族id
     * @param $userId   用户id
     * @return array 用户数组
     */
    public function getSameFamilyUsers(int $familyId, int $userId): array
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT userId FROM $this->PERSON_TABLE WHERE userId != '$userId' AND userId > 0 AND familyId = '$familyId' ";
        return $this->pdo->query($sql);
    }

    /**
     * 发表系统推文
     * @param $post    推文对象
     * @param $userIds  要推送的用户id，默认是全网
     * @return int 添加的推文id或者-1表示失败
     */
    public function addSystemPost(Post $post, $userIds = array())
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->POST_TABLE(zoneId,userId,familyId,content,coor_x,coor_y,address,photo,type,create_time,create_by,update_time,update_by)VALUES('$post->zoneId','$post->userId','$post->familyId','$post->content','$post->coorX','$post->coorY','$post->address','$post->photo','$post->type',now(),'$post->userId',now(),'$post->userId')";
            $postId = $this->pdo->insert($sql);

            if ($postId > 0) {
                //添加推文的同时要更新用户资料中的推文数
                $sql = "UPDATE $this->TABLE_USER_PROFILE SET post_num = post_num + 1 WHERE userId = '$post->userId' ";
                $this->pdo->update($sql);

                //记录用户日志
                $function_name = __CLASS__.'->'.__FUNCTION__;
                $option_detail = '添加系统推文';
                $other_msg = [
                    'post' => $post,
                    'userids' => $userIds
                ];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $optionTableName = $this->POST_TABLE;
                $tableId = $postId;
                $accountDB = new CDBAccount();
                $accountDB->recordUserOptionLog($function_name, $option_detail, $post->userId, $other_msg, $optionTableName, $tableId);
            }
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        $redisPost = new CDBPost();
        $pushUserIds = $userIds;

        if (count($pushUserIds) == 0) {
            //获取全系统的用户id
            $accountDB = new CDBAccount();
            $pushUserIds = $accountDB->getAllSystemUsers();
        }

        //设置推文的推送任务
        $taskPushResult = $redisPost->setPostTask($pushUserIds, $postId, Util::getTotalMicroTime(), $post->userId);

        return $postId;
    }

    /**
     * 将推文存储到族群的zset中
     */
    public function pushPostToFamily($postId, $familyArray) {

        $prefix = 'fpt';   // family post timeline
        $currentTime = Util::getCurrentTime();

        // TODO: 这里会触发bug
        // $redis = RedisConnect::getInstance();

        // $redis->multi();
        // $redis->select(0);
        // foreach($familyArray as $familyId) {
        //     $redis->zAdd($prefix.$familyId, $currentTime, $postId);
        // }
        // $redis->exec();
        return true;
    }

    /**
     * 发表推文
     * @param 推文对象|Post $post 推文对象
     * @param 推文要发到的家族id，0代表公开，|int $familyId 推文要发到的家族id，0代表公开，-1代表私密
     * @param int $all
     * @return int 添加的推文id或者-1表示失败
     */
    public function addPost(Post $post, $familyId = 0, $all = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->POST_TABLE(zoneId,userId,familyId,content,coor_x,coor_y,address,photo,type,create_time,create_by,update_time,update_by)VALUES('$post->zoneId','$post->userId','$post->familyId','$post->content','$post->coorX','$post->coorY','$post->address','$post->photo','$post->type',now(),'$post->userId',now(),'$post->userId')";
            $postId = $this->pdo->insert($sql);

            if ($postId > 0) {
                //添加推文的同时要更新用户资料中的推文数
                $sql = "UPDATE $this->TABLE_USER_PROFILE SET post_num = post_num + 1 WHERE userId = '$post->userId' ";
                $this->pdo->update($sql);

                 //记录用户日志
                 $function_name = __CLASS__.'->'.__FUNCTION__;
                 $option_detail = '添加推文';
                 $other_msg = [
                     'post' => $post,
                     'familyId' => $familyId,
                     'all' => $all
                 ];
                 $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                 $optionTableName = $this->POST_TABLE;
                 $tableId = $postId;
                 $accountDB = new CDBAccount();
                 $accountDB->recordUserOptionLog($function_name, $option_detail, $post->userId, $other_msg, $optionTableName, $tableId);
            }
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        $redisPost = new CDBPost();
        $pushUserIds = array();
        if ($familyId == 0) {
            if ($all == 1) {
                //@codeCoverageIgnoreStart
                $pushUserIds = $this->getUsers($post->userId);
                //@codeCoverageIgnoreEnd

                // 往族群里面推
                $familyDB = new CDBFamily();
                $familyList = $familyDB->getJoinFamilyList($post->userId);
                $familyArray = [];
                foreach($familyList as $family) {
                    $familyArray[] = $family['id'];
                }

                // 推送到族群中
                $this->pushPostToFamily($postId, $familyArray);
            } else {
                //三代以内
                $userFollowDB = new UserFollowDB();
                $pushUserIdsTmp = $userFollowDB->getUserFollow($post->userId);

                $pushUserIds = array();

                foreach ($pushUserIdsTmp as $userId) {
                    //@codeCoverageIgnoreStart
                    $user['userId'] = $userId;
                    //@codeCoverageIgnoreEnd
                    array_push($pushUserIds, $user);
                }
            }
            //@codeCoverageIgnoreStart
        } else if ($familyId > 0) {

            // 推送到族群中
            $this->pushPostToFamily($postId, [$familyId]);
            $pushUserIds = $this->getSameFamilyUsers($familyId, $post->userId);
            //@codeCoverageIgnoreEnd
        }

        //设置推文的推送任务
        $taskPushResult = $redisPost->setPostTask($pushUserIds, $postId, Util::getTotalMicroTime(), $post->userId);

        return $postId;
    }

    /**
     * 更新推文
     * @param $postId 推文id
     * @param $content 推文内容
     * @param $coorX
     * @param $coorY
     * @param $address 推文发表地址
     * @param $update_time 更新时间
     * @param $update_by  更新用户
     * @return int 返回更新的记录数
     */
    public function updatePost($postId, $content, $coorX, $coorY, $address, $update_time, $update_by)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->POST_TABLE SET content = '$content',coor_x='$coorX',coor_y='$coorY',address = '$address',update_time = '$update_time',update_by = '$update_by' WHERE id = '$postId' AND isDelete=0";
        $updatedRowCount = $this->pdo->update($sql);

         //记录用户日志
         $function_name = __CLASS__.'->'.__FUNCTION__;
         $option_detail = '更新推文';
         $other_msg = [
             'postId' => $postId,
             'content' => $content,
             'coorX' => $coorX,
             'coorY' => $coorY,
             'address' => $address,
             'update_time' => $update_time,
             'update_by' => $update_by,
         ];
         $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
         $optionTableName = $this->POST_TABLE;
         $tableId = $postId;
         $accountDB = new CDBAccount();
         $accountDB->recordUserOptionLog($function_name, $option_detail, $update_by, $other_msg, $optionTableName, $tableId);

        return $updatedRowCount;
    }

    /**
     * 删除推文
     * TODO，这里的删除的用户数组和推送的用户数组可能不一致
     * @param $postId 推文id
     * @return int 删除的记录数
     */
    public function deletePost($postId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $post = $this->getPost($postId);
        if ($post == null) {
            return 0;
        }

        try {
            $this->pdo->beginTransaction();

            $sql = "UPDATE $this->POST_TABLE SET isDelete=1 WHERE id = '$postId' AND isDelete=0";
            $result = $this->pdo->update($sql);
            if ($result > 0) {
                //删除推文的同时要更新用户资料中的推文数
                $sql = "UPDATE $this->TABLE_USER_PROFILE SET post_num = post_num - 1 WHERE userId = '$post->userId'  ";
                $this->pdo->update($sql);
                
                //记录用户日志
                $function_name = __CLASS__.'->'.__FUNCTION__;
                $option_detail = '删除推文';
                $other_msg = [
                    'postId' => $postId
                ];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $optionTableName = $this->POST_TABLE;
                $tableId = $postId;
                $accountDB = new CDBAccount();
                $accountDB->recordUserOptionLog($function_name, $option_detail, $post->userId, $other_msg, $optionTableName, $tableId);
            }

            $familyId = $post->familyId;
            $redisPost = new CDBPost();
            $pushUserIds = array();
            if ($familyId == 0) {
                //@codeCoverageIgnoreStart
                $pushUserIds = $this->getUsers($post->userId);
                //@codeCoverageIgnoreEnd
            } else if ($familyId > 0) {
                $pushUserIds = $this->getSameFamilyUsers($familyId, $post->userId);
            }

            //设置推文的推送任务
            $redisPost->setDeletePostTask($pushUserIds, $postId, $post->userId);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return $result;
    }

    /**
     * 获取推文详情
     * @param $postId
     * @return object 推文对象
     */
    public function getPost($postId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT pt.id,pt.zoneId,pt.userId,pt.familyId,ut.nickname as author,ut.photo as authorPhoto,pt.content,pt.coor_x as coorX,pt.coor_y as coorY,pt.address,pt.photo,ct.id as collection,lt.status as likeStatus,pt.comments as commentCount,pt.likes as likeCount,pt.type,pt.create_time as createTime,pt.create_by as createBy,pt.update_time as updateTime,pt.update_by as updateBy  from $this->POST_TABLE pt
            left join $this->USER_TABLE ut on pt.create_by = ut.id 
            left join $this->LIKE_TABLE lt ON pt.id = lt.postId AND lt.module = '1' AND lt.userId = ut.id 
            left join $this->COLLECTION_TABLE ct on pt.id = ct.recordId AND ct.moduleId = '5' AND ct.userId = ut.id AND ct.is_delete = '0' 
            WHERE pt.id = '$postId' AND pt.isDelete=0";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return null;
        }
        return new Post($result);
    }


    /**
     * 获取一页空间推文的数据
     * @param $zoneId 空间id
     * @param $pageIndex 页码
     * @param $pageSize  一页大小
     * @return array 一页推文数据
     */
    public function getZonePostsPaging($zoneId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;


        $sql = "SELECT pt.id,pt.zoneId,pt.userId,ut.nickname as author,ut.photo as authorPhoto,pt.content,pt.coor_x as coorX,pt.coor_y as coorY,pt.address,pt.photo,ct.id as collection,lt.status as likeStatus,pt.comments as commentCount,pt.likes as likeCount,pt.type,pt.create_time as createTime,pt.create_by as createBy,pt.update_time as updateTime,pt.update_by as updateBy  from $this->POST_TABLE pt
            left join $this->USER_TABLE ut on pt.create_by = ut.id 
            left join $this->LIKE_TABLE lt ON pt.id = lt.postId AND lt.module = '1' AND lt.userId = ut.id 
            left join $this->COLLECTION_TABLE ct on pt.id = ct.recordId AND ct.moduleId = '5' AND ct.userId = ut.id AND ct.is_delete = '0' 
            WHERE zoneId = '$zoneId' AND pt.isDelete = '0' ORDER BY id DESC LIMIT $offset,$pageSize";

        return $this->pdo->query($sql);
    }

    /**
     * 获取空间推文的总数
     * @param $zoneId 空间id
     * @return int 推文总数
     */
    public function getZonePostsTotalCount($zoneId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*) FROM $this->POST_TABLE WHERE zoneId = '$zoneId' AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);

        return $result['count(*)'];
    }

    /**
     * 添加一条推文评论
     * @param $postId 推文id
     * @param $userId 用户id
     * @param $replyToUserId 回复的用户id
     * @param $replyTo 回复评论的id,如果没有回复对象，传-1
     * @param $comment 评论内容
     * @param $module  模块id
     * @return int 添加的评论id
     */
    public function addPostComment($postId, $userId, $replyTo, $replyToUserId, $comment, $module)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->COMMENT_TABLE(postId,userId,replyto,replyto_userId,comment,module,create_time,create_by,update_time,update_by)VALUES('$postId','$userId','$replyTo','$replyToUserId','$comment','$module', now(),'$userId',now(),'$userId')";

            $commentId = $this->pdo->insert($sql);

             //记录用户日志
             $function_name = __CLASS__.'->'.__FUNCTION__;
             $option_detail = '添加推文评论';
             $other_msg = [
                 'postId' => $postId,
                 'replyTo' => $replyTo,
                 'replyToUserId' => $replyToUserId,
                 'comment' => $comment,
                 'module' => $module,
             ];
             $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
             $optionTableName = $this->COMMENT_TABLE;
             $tableId = $commentId;
             $accountDB = new CDBAccount();
             $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

            $this->updatePostCommentCount($postId, 1);
            $this->pdo->commit();
            return $commentId;
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
        }
    }
    //@codeCoverageIgnoreEnd

    /**
     * 更新推文评论
     * @param $commentId 评论id
     * @param $comment   评论内容
     * @param $updateTime 更新内容
     * @param $updateBy   更新人
     * @return int 更新的记录数
     */
    public function updatePostComment($commentId, $comment, $updateTime, $updateBy)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新推文评论';
        $other_msg = [
            'commentId' => $commentId,
            'comment' => $comment,
            'updateTime' => $updateTime,
            'comment' => $comment,
            'updateBy' => $updateBy,
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = $this->COMMENT_TABLE;
        $tableId = $commentId;
        $accountDB = new CDBAccount();
        $accountDB->recordUserOptionLog($function_name, $option_detail, $updateBy, $other_msg, $optionTableName, $tableId);

        $sql = "UPDATE $this->COMMENT_TABLE SET comment = '$comment',update_time = '$updateTime',update_by = '$updateBy' 
                WHERE id = '$commentId' AND isDelete = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 日志赞数加或减
     * @param $postId 推文id
     * @param $likeCount 点赞的记录数
     * @return boolean true成功,false失败
     */
    public function updatePostLikeCount($postId, $likeCount)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->POST_TABLE SET likes = '$likeCount' WHERE id = '$postId' AND isDelete=0";
        return $this->pdo->update($sql) > 0;
    }

    /**
     * 日志评论数加或减
     * @param $postId 推文id
     * @param $operation -1减一　+1加一
     * @return boolean true成功,false失败
     */
    public function updatePostCommentCount($postId, $operation)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->POST_TABLE SET comments = comments + $operation WHERE id = '$postId' AND isDelete = '0' ";
        return $this->pdo->update($sql) > 0;
    }

    /**
     * 删除推文评论
     * @param $commentId 评论id
     * @return int 删除的记录数
     */
    public function deletePostComment($commentId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '删除推文评论';
        $other_msg = [
            'commentId' => $commentId,
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = $this->COMMENT_TABLE;
        $tableId = $commentId;
        $accountDB = new CDBAccount();
        $accountDB->recordUserOptionLog($function_name, $option_detail, $GLOBALS['userId'], $other_msg, $optionTableName, $tableId);

        $sql = "UPDATE $this->COMMENT_TABLE SET isDelete = '1' 
                WHERE id = '$commentId' AND isDelete ='0' ";

        return $this->pdo->update($sql);
    }

    /*
     * 查询一条评论下的所有回复
     * @param $commentId 评论id
     *
     * */
    public function getAllReplyComment($commentId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT postId,userId,replyto,comment,create_time,create_by,update_time,update_by 
                from $this->COMMENT_TABLE WHERE replyto = '$commentId' AND isDelete = '0' ";

        return $this->pdo->query($sql);

    }

    /**
     * 查询推文下的所有评论
     * @param $postId 推文id
     * @param $module 模块id
     * @return array 获取推文下的所有评论
     */
    public function getPostCommentsTotal($postId, $module)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT ct.id,ct.userId,ct.replyto,ct.replyto_userId as replytoUserId,ut_tmp.nickname as replytoUsername,ct.comment,ct.create_time as createTime,ut.nickname as createByName,ut.photo,ct.create_by as createBy FROM $this->COMMENT_TABLE ct
            inner join $this->USER_TABLE ut on ut.id = ct.create_by
            left join $this->USER_TABLE ut_tmp on ut_tmp.id = ct.replyto_userId 
             WHERE postId = '$postId' AND module = '$module' AND isDelete = '0' ORDER BY ct.id DESC";

        return $this->pdo->query($sql);
    }

    /**
     * 获取相关的推文(所有加入的家族的成员)的总数,当前不在使用
     * @param $userId 用户id
     * @param $type 是否看系统生成 0不看系统生成 1看
     * @return int 总数
     */
    public function getRelatePostsPagingTotal_v1($userId, $type = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }


        $sql = "SELECT count(id) from $this->POST_TABLE WHERE isDelete=0 AND userId in (SELECT userId FROM $this->PERSON_TABLE WHERE familyId in (SELECT familyId FROM $this->PERSON_TABLE WHERE userId = '$userId')) ";

        if ($type == 0)
            //@codeCoverageIgnoreStart
            $sql = $sql . " and type = 1";    //限制post类型
            //@codeCoverageIgnoreEnd

        $result = $this->pdo->uniqueResult($sql);
        $count = $result['count(id)'];
        return $count;
    }

    /**
     * @codeCoverageIgnore
     * TODO 1、post冗余家族id；2、显示昵称(从第一个创建或者加入的家族的名字设置)，当前不在使用
     * 获取相关的一页推文
     * @param $userId 用户id
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @param $type 是否看系统生成 0不看系统生成 1看
     * @return Object Pager对象
     */
    public function getRelatePostsPaging_v1($userId, $pageIndex, $pageSize, $type = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $where = "";
        if ($type == 0)
            $where = " and type = 1 ";    //限制post类型

        $sql = "SELECT pt.id,pt.userId,ut.username as author,ut.photo as authorPhoto,pt.content,pt.address,pt.photo,lt.status as likeStatus,pt.comments as commentCount,pt.likes as likeCount,pt.create_time as createTime,pt.create_by as createBy,pt.update_time as updateTime,pt.update_by as updateBy 
            from $this->POST_TABLE pt 
            left join $this->LIKE_TABLE lt ON pt.id = lt.postId AND lt.module = '1' AND lt.userId = '$userId' 
            left join $this->USER_TABLE ut on pt.create_by = ut.id 
            WHERE pt.isDelete=0 AND pt.userId in (SELECT userId FROM $this->PERSON_TABLE WHERE familyId in (SELECT familyId FROM $this->PERSON_TABLE WHERE userId = '$userId'))  $where   
            ORDER BY id DESC LIMIT $offset,$pageSize";


        $result = $this->pdo->query($sql);
        return $result;

    }

    /**
     * 获取相关的推文(所有加入的家族的成员)的总数
     * @param $userId 用户id
     * @param $type 是否看系统生成 0不看系统生成 1看
     * @return int 总数
     */
    public function getRelatePostsPagingTotal($userId, $type = 1)
    {
        $redisPost = new CDBPost();
        $count = $redisPost->getTimelinePostCount($userId);

        return $count;
    }

    public function getRelatePostsPagingByMaxIdAndSinceId($userId, $maxId, $sinceId, $count,
    $maxTimeStamp = 0, $sinceTimeStamp = 0, $type = 1) {
        $redisPost = new CDBPost();
        list($timeline, $endIndex) = $redisPost->getTimelineBySinceIdAndMaxId($userId, $maxId, $sinceId, $count, $maxTimeStamp, $sinceTimeStamp);

        $where = "";
        $isFirst = true;
        $len = count($timeline);

        foreach ($timeline as $postId => $score) {
            if ($isFirst) {
                $where .= "WHERE pt.id = '$postId' ";
                $isFirst = false;
            }
            $where .= " OR pt.id = '$postId' ";
        }

        $result = [];
        if ($where != "") {

            $sql = "SELECT pt.id,pt.zoneId,pt.userId,ut.nickname as author,ut.photo as authorPhoto,pt.content,pt.coor_x as coorX,pt.coor_y as coorY,pt.address,pt.photo,ct.id as collection,lt.status as likeStatus,pt.comments as commentCount,pt.likes as likeCount,pt.type,pt.create_time as createTime,pt.create_by as createBy,pt.update_time as updateTime,pt.update_by as updateBy  from $this->POST_TABLE pt
                left join $this->LIKE_TABLE lt ON pt.id = lt.postId AND lt.module = '1' AND lt.userId = '$userId' 
                left join $this->COLLECTION_TABLE ct on pt.id = ct.recordId AND ct.moduleId = '5' AND ct.userId = '$userId' AND ct.is_delete = '0'
                left join $this->USER_TABLE ut on pt.create_by = ut.id 
                $where AND pt.over_report = 0 AND pt.isDelete = '0' ORDER BY pt.id DESC";

            $result = $this->pdo->query($sql);
        }

        return [$timeline, $result, $endIndex];
    }

    /**
     * TODO 1、post冗余家族id；2、显示昵称(从第一个创建或者加入的家族的名字设置)
     * 获取相关的一页推文,使用redis作为timeline
     * @param $userId 用户id
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @param $type 是否看系统生成 0不看系统生成 1看
     * @return Object Pager对象
     */
    public function getRelatePostsPaging($userId, $pageIndex, $pageSize, $type = 1)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $redisPost = new CDBPost();
        $result = $redisPost->getTimeline($userId, $pageIndex, $pageSize);

        $where = "";
        $isFirst = true;
        $len = count($result);
        foreach ($result as $value) {
            if ($isFirst) {
                $where .= "WHERE pt.id = '$value' ";
                $isFirst = false;
            }
            $where .= " OR pt.id = '$value' ";
        }

        if ($where != "") {

            $sql = "SELECT pt.id,pt.zoneId,pt.userId,ut.nickname as author,ut.photo as authorPhoto,pt.content,pt.coor_x as coorX,pt.coor_y as coorY,pt.address,pt.photo,ct.id as collection,lt.status as likeStatus,pt.comments as commentCount,pt.likes as likeCount,pt.type,pt.create_time as createTime,pt.create_by as createBy,pt.update_time as updateTime,pt.update_by as updateBy  from $this->POST_TABLE pt
                left join $this->LIKE_TABLE lt ON pt.id = lt.postId AND lt.module = '1' AND lt.userId = '$userId' 
                left join $this->COLLECTION_TABLE ct on pt.id = ct.recordId AND ct.moduleId = '5' AND ct.userId = '$userId' AND ct.is_delete = '0'
                left join $this->USER_TABLE ut on pt.create_by = ut.id 
                $where AND pt.over_report = 0 AND pt.isDelete = '0' ORDER BY pt.id DESC";

            $result = $this->pdo->query($sql);
        }

        return $result;
    }

    /**
     * 获取族群的推文列表
     */
    public function getFamilyPostsPaging($familyId, $pageIndex, $pageSize, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $prefix = 'fpt';
        $end = (1 - $pageIndex) * $pageSize - 1;
        $start = $end - $pageSize + 1;

        $redis = RedisConnect::getInstance();

        $result = $redis->zRange($prefix.$familyId, $start, $end);

        $where = "";
        $isFirst = true;
        foreach ($result as $value) {

            if ($isFirst) {
                $where .= "WHERE pt.id = '$value' ";
                $isFirst = false;
            }
            $where .= " OR pt.id = '$value' ";
        }

        if ($where != "") {

            $sql = "SELECT pt.id,pt.zoneId,pt.userId,ut.nickname as author,ut.photo as authorPhoto,pt.content,pt.coor_x as coorX,pt.coor_y as coorY,pt.address,pt.photo,ct.id as collection,lt.status as likeStatus,pt.comments as commentCount,pt.likes as likeCount,pt.type,pt.create_time as createTime,pt.create_by as createBy,pt.update_time as updateTime,pt.update_by as updateBy  from $this->POST_TABLE pt
                left join $this->LIKE_TABLE lt ON pt.id = lt.postId AND lt.module = '1' AND lt.userId = '$userId' 
                left join $this->COLLECTION_TABLE ct on pt.id = ct.recordId AND ct.moduleId = '5' AND ct.userId = '$userId' AND ct.is_delete = '0'
                left join $this->USER_TABLE ut on pt.create_by = ut.id 
                $where AND pt.over_report = 0 AND pt.isDelete = '0' ORDER BY pt.id DESC";

            $result = $this->pdo->query($sql);
        }

        return $result;
    }


    /**
     * @codeCoverageIgnore
     * @param $userId
     * @param $startTimestamp
     * @param $endTimestamp
     * @return int 返回在此区间的推文的数量
     */
    public function getPostCountByTimestamp($userId, $startTimestamp, $endTimestamp)
    {
        $redisPost = new CDBPost();
        $count = $redisPost->getPostCountByTimestamp($userId, $startTimestamp, $endTimestamp);
        return $count;
    }

    /**
     * 将推文设置成举报封禁状态
     * @param $postId
     * @return mixed
     */
    public function setPostOverReport($postId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->POST_TABLE set over_report = '1' WHERE id = '$postId' AND isDelete=0";
        return $this->pdo->update($sql);
    }


    /**
     * 检查用户是否在黑名单中
     * @param $userId 用户id
     * @param $blockUserId 要拉黑的用户id
     * @return bool true代表在黑名单中，false代表不在黑名单中
     */
    public function checkUserInBlackList(int $userId, int $blockUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE_BLACKLIST WHERE userId = '$userId' AND block_userId = '$blockUserId' AND isDelete=0 ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['count(id)'] > 0) {
            return true;
        } else {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }
    }

    /**
     * 将用户加入黑名单
     * @param int $userId
     * @param int $blockUserId
     * @return mixed
     */
    public function addUserToBlackList(int $userId, int $blockUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_BLACKLIST(userId,block_userId,create_time,create_by,update_time,update_by)VALUES
                ('$userId','$blockUserId',now(),'$userId',now(),'$userId')";

        return $this->pdo->insert($sql);
    }

    /**
     * 根据blockId获取屏蔽信息
     * @param $blackListId
     * @return mixed
     */
    public function getBlockByBlockId($blackListId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,block_userId,create_time,create_by,update_time,update_by FROM $this->TABLE_BLACKLIST WHERE id = '$blackListId' AND isDelete=0 ";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 从黑名单中删除用户
     * @param int $blackListId
     * @return mixed
     */
    public function deleteUserFromBlackList(int $blackListId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "DELETE FROM $this->TABLE_BLACKLIST WHERE id = '$blackListId' ";

        return $this->pdo->update($sql);
    }

    /**
     * 获取用户黑名单
     * @param int $userId
     * @return mixed
     */
    public function getUserBlackList(int $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.userId,tu.nickname,tu.photo,tb.block_userId,tb.create_time,tb.create_by,tb.update_time,tb.update_by FROM $this->TABLE_BLACKLIST tb 
                    LEFT JOIN $this->TABLE_USER tu on tu.id = tb.block_userId 
                    WHERE tb.userId = '$userId' AND tb.isDelete=0 ";
        return $this->pdo->query($sql);
    }
}
