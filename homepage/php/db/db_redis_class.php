<?php
    /**
     * 封装了redis的连接部分，其余部分直接参考
     * https://github.com/phpredis/phpredis#get
     *
     */

    namespace DB;

    class RedisConnect{

        public $host = null;
        public $port = null;
        public $pwd = null;
        public $redis = null;
        private static $connect = null;

        private function __construct($host,$port,$pwd){
            $this->host = $host;
            $this->port = $port;
            $this->pwd = $pwd;
        }

        public static function getInstance(){
            if(self::$connect == null){
                self::$connect = new \DB\RedisConnect($GLOBALS['redis_host'],$GLOBALS['redis_port'],$GLOBALS['redis_pass']);
                self::$connect->connect();
            }

            return self::$connect->redis;
        }

        private function connect(){
            if($this->redis == null){
                $this->redis = new \Redis();
                $this->redis->pconnect($this->host, $this->port);
                $this->redis->auth($this->pwd);
            }
        }

    }
