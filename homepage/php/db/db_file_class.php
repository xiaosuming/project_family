<?php
/**
 * 文件上传记录模块
 * author: jiangpengfei
 * date: 2017-03-10
 */

namespace DB;

use DB\CDBManager;
use Util\Util;

class CDBFile
{
        public $pdo = null;
		public $TABLE = "gener_file";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
        {
        	
            if (!$this->pdo)
            {
                //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
                if (!isset($GLOBALS['pdo']))
                {
                        $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                        if ($this->pdo)
                                $GLOBALS['pdo'] = $this->pdo;
                }
                else
                {
                        $this->pdo = $GLOBALS['pdo'];
                }
            }
            return true;
        }

        /**
         * 添加文件记录
         * @param $filePath 文件路径
         * @param $module 模块id 
         * @param $userId 用户id 
         * @return int 插入的id或者失败返回-1
         */
        public function addFile($filePath,$module,$userId){
            if(!$this->init()){
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
                //@codeCoverageIgnoreEnd
            }
            $sql = "INSERT INTO $this->TABLE(file_path,module,create_by,create_time)VALUES('$filePath','$module','$userId',now())";
            return $this->pdo->insert($sql);
        
        }

        /** 
         * 获取一页的文件数据
         * @param $pageIndex 页号
         * @param $pageSize  页大小
         * @param $module    模块
         * @param $userId    用户id
         */
        public function getFilesPaging($pageIndex,$pageSize,$module,$userId){
            if(!$this->init()){
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
                //@codeCoverageIgnoreEnd
            }

            $offset = ($pageIndex - 1) * $pageSize;

            $where = "";
            if($module != -1){
                $where = "WHERE module = '$module' AND  create_by = '$userId' ";
            }else{
		        $where = "WHERE create_by = '$userId' ";
	        }

            $sql = "SELECT file_path as filePath,module,create_by as createBy,create_time as createTime FROM $this->TABLE $where ORDER BY id DESC limit $offset,$pageSize";

            return $this->pdo->query($sql);
        }

        /**
         * 获取指定条件的文件的总数
         * @param $module 模块 -1代表全部
         * @param $userId 用户id
         * @return int 总数
         */
        public function getFilesCount($module,$userId){
            if(!$this->init()){
                //@codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
                //@codeCoverageIgnoreEnd
            }

            $where = "";

            if($module != -1){
	            $where = "WHERE module = '$module' AND  create_by = '$userId'  ";
            }else{
                $where = "WHERE create_by = '$userId' ";
            }

            $sql = "SELECT count(id) FROM $this->TABLE $where";

            $result = $this->pdo->uniqueResult($sql);
            if($result != null){
                return $result['count(id)'];
            }else{
                //@codeCoverageIgnoreStart
                return 0;
                //@codeCoverageIgnoreEnd
            }
        }
};
