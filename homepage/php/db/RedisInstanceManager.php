<?php
/**
 * redis实例的管理器
 * @Author: jiangpengfei
 * @Date: 2019-01-08
 */

namespace DB;

use BaseInterface\Singleton;
use Config\RedisInstanceConfig;

class RedisInstanceManager {

    use Singleton;

    private $instanceMap;

    private function __construct() {
        $this->instanceMap = [];
    }

    public function connect($host, $port, $pwd) {
        $redis = new \Redis();
        $redis->connect($host, $port);
        $redis->auth($pwd);
        
        return $redis;
    }

    public function getStorageInstance() {
        if (!isset($this->instanceMap['storage'])) {
            $config = RedisInstanceConfig::getStorageConfig();
            $redis = $this->connect($config['HOST'], $config['PORT'], $config['AUTH']);
            $this->instanceMap['storage'] = $redis;
        }

        return $this->instanceMap['storage'];
    }

    public function getTempStorageInstance() {
        if (!isset($this->instanceMap['tempstorage'])) {
            $config = RedisInstanceConfig::getTempStorageConfig();
            $redis = $this->connect($config['HOST'], $config['PORT'], $config['AUTH']);
            $this->instanceMap['tempstorage'] = $redis;
        }

        return $this->instanceMap['tempstorage'];
    }

    public function getCacheInstance() {
        if (!isset($this->instanceMap['cache'])) {
            $config = RedisInstanceConfig::getCacheConfig();
            $redis = $this->connect($config['HOST'], $config['PORT'], $config['AUTH']);
            $this->instanceMap['cache'] = $redis;
        }

        return $this->instanceMap['cache'];
    }

    public function getAuthInstance() {
        if (!isset($this->instanceMap['cache'])) {
            $config = RedisInstanceConfig::getCacheConfig();
            $redis = $this->connect($config['HOST'], $config['PORT'], $config['AUTH']);
            $this->instanceMap['cache'] = $redis;
        }

        return $this->instanceMap['cache'];
    }
}