<?php
/**
 * 支付的订单类
 * @author: jiangpengfei
 * @date:   2019-03-19
 */

namespace DB;

use DB\CDBManager;
use DB\RedisConnect;
use Util\Util;
use Model\Order;
use Util\SysConst;
use ThirdParty\Wxpay\Lib\WxPayApi;
use ThirdParty\Wxpay\Lib\WxPayUnifiedOrder;
use ThirdParty\Wxpay\WxPayConfig;
use ThirdParty\Wxpay\Lib\WxPayException;
use Util\SysLogger;
use ThirdParty\Wxpay\Lib\WxPaySandBox;
use ThirdParty\Alipay\AopClientBuilder;
use ThirdParty\Alipay\Aop\Request\AlipayTradeAppPayRequest;
use Model\OrderLogRecord;
use ThirdParty\InstantMessage;

class CDBOrder
{
    public $pdo = null;

    public $TABLE = 'gener_order';
    public $TABLE_ORDER_LOG_RECORD = 'gener_order_log_record';


    public function __construct() {
        $this->init();
    }

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }

        return true;
    }

    /**
     * 生成订单
     * @param Order $order 订单对象
     * @parma map prepayInfo 预下单的信息
     * @return 添加成功的记录
     */
    public function addOrder(Order $order, &$prepayInfo) {

        $payRequestBody = "私信付费购买";
        $subject = "付费私信订单";

        if ($order->payway == SysConst::$ORDER_PAYWAY_WEIXIN) {
            $wxConfig = new WxPayConfig();

            // 微信统一下单
            $wxPayUnifiedOrder = new WxPayUnifiedOrder();
            $wxPayUnifiedOrder->SetBody($payRequestBody);
            $wxPayUnifiedOrder->SetAttach($subject);
            $wxPayUnifiedOrder->SetOut_trade_no($order->id);
            $wxPayUnifiedOrder->SetTotal_fee($order->realPrice);
            $wxPayUnifiedOrder->SetTime_start(date("YmdHis"));
            $wxPayUnifiedOrder->SetTime_expire(date("YmdHis", time() + 1800));
            $wxPayUnifiedOrder->SetGoods_tag('chat');
            $wxPayUnifiedOrder->SetTrade_type('APP');

            // 获取沙箱密钥的代码
            // $wxPaySandBox = new WxPaySandBox();
            // WxPayApi::generSandBoxKey($wxConfig, $wxPaySandBox);
            // exit;

            try {
                $wxOrderResult = WxPayApi::unifiedOrder($wxConfig, $wxPayUnifiedOrder, $_SERVER['SERVER_ADDR']);
            } catch (WxPayException $e) {
                $logger = SysLogger::getInstance();
                $logger->error("微信支付出现错误, 预下单失败:", [Util::exceptionFormat($e)]);
                return false;
            }
            

            if ($wxOrderResult['return_code'] == 'FAIL') {
                $logger = SysLogger::getInstance();
                $logger->error("微信支付出现错误:", $wxOrderResult);
                return false;
            }

            if ($wxOrderResult['result_code'] != 'SUCCESS') {
                // 统一下单失败
                $logger = SysLogger::getInstance();
                $logger->error("微信支付出现错误:", $wxOrderResult);
                return false;
            } else {
                $prepayInfo = $wxOrderResult;    // 预支付id

                unset($prepayInfo['err_code']);
                unset($prepayInfo['err_code_des']);
                unset($prepayInfo['result_code']);
                unset($prepayInfo['return_code']);
                unset($prepayInfo['return_msg']);

                $order->wxPrepayId = $wxOrderResult['prepay_id'];

                $logger = SysLogger::getInstance(true, 'wxpay');
                $logger->info('wxpay_prepayorder', $wxOrderResult);
            }
        } else if ($order->payway == SysConst::$ORDER_PAYWAY_ALIPAY) {
            $alipayConf = \Yaconf::get('alipay');

            $aopClient = AopClientBuilder::instance();
            $request = new AlipayTradeAppPayRequest();

            $bizContent = [
                "body" => $payRequestBody,
                "subject" => $subject,
                "out_trade_no" => $order->id,
                "timeout_express" => "30m",
                "total_amount" => $order->realPrice / 100,
                "product_code" => 'QUICK_MSECURITY_PAY'
            ];
            
            $request->setNotifyUrl($alipayConf['notify_url']);
            $request->setBizContent(json_encode($bizContent));
            //这里和普通的接口调用不同，使用的是sdkExecute
            $response = $aopClient->sdkExecute($request);

            // 支付宝不需要预下单，只需要在这里调用返回即可
            $prepayInfo = $response;
            $logger = SysLogger::getInstance(true, 'alipay');
            $logData['response'] = $response;
            $logger->info('alipay_sign_response', $logData);
        } else {
            return false;
        }

        $sql = "INSERT INTO $this->TABLE
                (id, userId, status, totalPrice, realPrice, discountStrategyId, userRemark, failureTime, province, city, area, town, address, name, phone, goodsType, payway, wxPrepayId, isDelete, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$order->id', '$order->userId', '$order->status', '$order->totalPrice', '$order->realPrice', '$order->discountStrategyId', '$order->userRemark', '$order->failureTime', '$order->province', '$order->city', '$order->area', '$order->town', '$order->address', '$order->name', '$order->phone', '$order->goodsType', '$order->payway', '$order->wxPrepayId', '$order->isDelete', '$order->createBy', now(), '$order->updateBy', now())";

        $this->pdo->insert($sql);

        return $order->id;
    }

    /**
     * 更新订单状态
     * @param int $orderId 订单id
     * @param int $status  订单状态
     * @param int $payway  支付方式
     * @return int 更新的订单状态
     */
    public function updateOrderStatus(int $orderId, int $status, int $payway) {
         $sql = "UPDATE $this->TABLE set status = '$status', payway = '$payway', updateTime = now() WHERE id = '$orderId' AND isDelete = '0' ";

         return $this->pdo->update($sql);
    }

    /**
     * 获取订单的详情
     * @param int $orderId 订单id
     * @return Order 订单对象
     */
    public function getOrder(int $orderId) {
        $sql = "SELECT * FROM $this->TABLE WHERE id = '$orderId' AND isDelete = '0' ";
        $res = $this->pdo->uniqueResult($sql);

        if ($res == null) {
            return null;
        } else {
            return new Order($res);
        }
    }

    /**
     * 私信收费订单的成功回调
     * @param $orderId 订单id
     * @param $payway  支付方式
     * @return bool 回调执行成功或失败
     */
    public function chatOrderCallbackOnSuccess($orderId, $payway) {

        try {
            $this->pdo->beginTransaction();
            // 1. 更新订单状态
            $this->updateOrderStatus($orderId, SysConst::$ORDER_STATUS_SHIP_CONFIRM, $payway);
    
            // 2. 生成支付的日志记录
            $orderLogRecord = new OrderLogRecord();
            $orderLogRecord->id = Util::getNextId();
            $orderLogRecord->orderId = $orderId;
            $orderLogRecord->remark = '通过支付方式'.$payway."支付成功";
            $orderLogRecord->type = SysConst::$ORDER_STATUS_SHIP_CONFIRM;
            $orderLogRecord->createBy = 0;
            $orderLogRecord->updateBy = 0;
            $this->addOrderLogRecord($orderLogRecord);

            // 3. 发送私信
            $infoCardDB = new CDBInfoCard();
            $infoCardChatOrder = $infoCardDB->getInfoCardChatOrderByOrderId($orderId);
            if ($infoCardChatOrder == null) {
                // 私信不存在
                $data['orderId'] = $orderId;
                SysLogger::getInstance()->error('订单私信不存在', $data);
                return false;
            }

            $msg = $infoCardChatOrder->content;
            $accountDB = new CDBAccount();

            // 用户1的信息
            $user1Info = $accountDB->getUserInfo($infoCardChatOrder->userId);
            $user1 = $user1Info['hx_username'];
            
            // 通过名片获取用户2的信息
            $infoCard = $infoCardDB->getInfoCard($infoCardChatOrder->infoCardId);
            if ($infoCard == null) {
                return false;
            }

            // 用户2的信息
            $user2Info = $accountDB->getUserInfo($infoCard->userId);
            $user2 = $user2Info['hx_username'];
            
            // 不能因为发送消息失败，导致支付记录失败
            $this->pdo->commit();

            $im = new InstantMessage();
            
            if (!$im->sendMsgFromOneToAnother($user1, $user2, 1, $msg)) {
                // 发送消息不成功，记录错误日志
                $data['user1'] = $user1;
                $data['user2'] = $user2;
                $data['msg'] = $msg;
                SysLogger::getInstance()->error('私信发送消息失败', $data);
                return false;
            }
    

        } catch (\PDOException $e) {
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return false;
        }

    }

    /**
     * 添加订单日志记录
     * @param OrderLogRecord $record 订单日志记录对象
     * @return int 记录id
     */
    public function addOrderLogRecord(OrderLogRecord $record) {
        $sql = "INSERT INTO $this->TABLE_ORDER_LOG_RECORD 
                (id, orderId, remark, type, isDelete, createBy, createTime, updateBy, updateTime)
                VALUES
                ('$record->id', '$record->orderId', '$record->remark', '$record->type', 0, '$record->createBy', now(), '$record->updateBy', now())";

        $this->pdo->insert($sql);

        return $record->id;
    }
}