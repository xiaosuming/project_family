<?php
/**
 * 查找家族
 */

namespace DB;

use DB\CDBManager;
use Model\LookForFamilyTask;
use Util\Util;
use Util\RedLock\RedLock;
use Config\RedisInstanceConfig;
use Util\SysConst;

class CDBLookForFamily
{
    public $pdo = null;
    public $TABLE = "gener_lookfor_family_task";
    public $TABLE_RESULT = "gener_lookfor_family_task_result";
    public $TABLE_COMMENT = "gener_lookfor_family_task_comment";
    public $TABLE_REPLY = "gener_lookfor_family_task_reply";
    public $TABLE_USER = "gener_user";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 验证任务id和用户id
     * @param $taskId 任务id
     * @param $userId 用户Id
     * @return bool true代表有权限，false没有权限
     */
    public function verifyTaskIdAndUserId($taskId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT * FROM $this->TABLE WHERE id = '$taskId'  AND create_by = '$userId' ";
        $result = $this->pdo->query($sql);
        if (count($result) > 0) {
            return true;
        }
        return false;
    }

    /**
     * 验证任务结果id和用户id
     * @param int $taskResultId 任务结果id
     * @param
     */
    public function verifyTaskResultIdAndUserId($taskResultId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*) FROM $this->TABLE_RESULT tr 
                    INNER JOIN $this->TABLE tb ON tr.taskId = tb.id 
                    WHERE tr.id = '$taskResultId'  AND tb.create_by = '$userId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result['count(*)'] > 0) {
            return true;
        }
        //@codeCoverageIgnoreStart
        return false;
        //@codeCoverageIgnoreEnd
    }

    /**
     * 获取用户的所有寻亲任务
     * @param $userId 用户id
     * @param $isFinish 是否完成，不传代表不作为条件
     */
    public function getAllTask($userId, $isFinish = null)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,task_content as taskContent,exclude_familyIds as excludeFamilyIds,
                            priority,threshold,email,cycle,is_finish as isFinish,create_by as createBy,create_time as createTime,
                            update_by as updateBy,update_time as updateTime 
                    FROM  $this->TABLE 
                    WHERE create_by = '$userId' ";

        $where = '';

        if ($isFinish !== null) {
            $where = " AND is_finish = '$isFinish' ";
        }

        $where .= " ORDER BY id DESC";

        return $this->pdo->query($sql . $where);

    }

    /**
     * 新增查找家族的任务
     * @param $task
     * @return int 插入的记录id
     */
    public function addTask(LookForFamilyTask $task)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE
            (task_content,exclude_familyIds,priority,threshold,cycle,email,is_finish,create_by,create_time,update_by,update_time, show_type) 
            VALUES
            ('$task->taskContent','$task->excludeFamilyIds','$task->priority','$task->threshold','$task->cycle','$task->email',0,'$task->createBy',now(),'$task->updateBy',now(), '$task->showType')";


        return $this->pdo->insert($sql);
    }

    /**
     * 更新查找家族的任务的内容
     * @param $taskId 任务id
     * @param $taskContent 任务内容
     * @param $userId
     * @return int 更新的记录数
     */
    public function updateTaskContent($taskId, $taskContent, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET task_content = '$taskContent',update_time = now(),update_by = '$userId' WHERE id = '$taskId' AND is_finish = '0' ";

        return $this->pdo->update($sql);
    }

    /**
     * 更新查找家族任务的设置
     * @param $taskId 任务id
     * @param $priority 优先级
     * @param $threshold 阈值
     * @param $cycle    任务执行周期
     * @param $email    通知邮箱
     * @param $userId
     * @return int 更新的记录数
     */
    public function updateTaskSetting($taskId, $priority, $threshold, $cycle, $email, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET priority = '$priority',threshold = '$threshold',cycle = '$cycle',
            email = '$email',update_time = now(),update_by = '$userId'  WHERE id = '$taskId' AND is_finish = '0'  ";
        return $this->pdo->update($sql);
    }

    /**
     * 更新任务状态,不能更新已经失败的任务
     * @param $taskId  任务id
     * @param $status  要更新的状态，-1为失败，1为成功
     * @param $userId
     * @return int 更新的记录数
     */
    public function updateTaskStatus($taskId, $status, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET is_finish = '$status',update_time = now(),update_by = '$userId' WHERE id = '$taskId' AND is_finish > -1 ";
        return $this->pdo->update($sql);
    }

    /**
     * 重新提交任务
     * @param int $taskId 任务id
     * @param array $excludeFamilyIds 排除的家族id
     * @param int $userId
     * @return int
     */
    public function recommitTask(int $taskId, array $excludeFamilyIds, int $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $updateRow = 0;

        try {
            $this->pdo->beginTransaction();
            $this->updateTaskStatus($taskId, 0, $userId);     //打开任务

            $excludeFamilyIdsStr = "";

            foreach ($excludeFamilyIds as $familyId) {
                $excludeFamilyIdsStr .= "$familyId,";
            }

            $sql = "UPDATE $this->TABLE SET exclude_familyIds = CONCAT(exclude_familyIds,\"$excludeFamilyIdsStr\"),update_time = now(),update_by = '$userId'  WHERE id = '$taskId' ";

            $updateRow = $this->pdo->update($sql);

            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            $updateRow = 0;
            //@codeCoverageIgnoreEnd
        }

        return $updateRow;
    }

    /**
     * 获取任务执行结果分页
     * @param $taskId 任务id
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @return array 结果数组
     */
    public function getTaskResultPaging(int $taskId, int $pageIndex, int $pageSize, $filterEmpty=false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;
        $filterEmptySql = $filterEmpty ? " and find_contents != '[]' " : " ";
        $sql = "SELECT tr.id,tr.taskId,tr.is_find as isFind,tr.find_contents as findContents,tr.create_time as createTime FROM $this->TABLE_RESULT as tr 
                    WHERE tr.taskId = '$taskId' $filterEmptySql ORDER BY tr.id DESC limit $offset,$pageSize";

        return $this->pdo->query($sql);

    }

    /**
     * 获取任务结果总数
     * @param $taskId 任务Id
     * @return int 任务总数
     */
    public function getTaskResultTotal(int $taskId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE_RESULT WHERE taskId = '$taskId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result === null) {
            //@codeCoverageIgnoreStart
            return 0;
            //@codeCoverageIgnoreEnd
        } else {
            return $result['count(id)'];
        }
    }

    /**
     * 获取任务的某条执行结果
     * @param $taskResultId 任务结果
     * @return obj 任务的执行结果
     */
    public function getTaskResult(int $taskResultId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,taskId,is_find as isFind,find_contents as findContents,create_time as createTime FROM $this->TABLE_RESULT WHERE id = '$taskResultId' ";

        return $this->pdo->uniqueResult($sql);
    }


    ######################################
    # v3
    ######################################

    /**
     * 获取用户的所有寻亲任务
     * @param $userId 用户id
     * @param $isFinish 是否完成，不传代表不作为条件
     */
    public function getTaskList($userId, $page, $pageSize, $forUser = true, $isFinish = null)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $createBySql = $forUser ? " WHERE create_by = '$userId' ":" WHERE create_by > 0 AND show_type = 1";
        # show_type 为 0为所有人不可见，为1是所有人可见
        $sql = "SELECT SQL_CALC_FOUND_ROWS t.id,task_content as taskContent, is_finish as isFinish, 
                         read_num as readNum ,ifnull(countComment, 0) as countComment,
                            tu.nickname, tu.photo, create_time as createTime,create_by as createBy   
                    FROM  $this->TABLE t LEFT JOIN (SELECT task_id, count(id) as countComment FROM `$this->TABLE_COMMENT` 
                    WHERE is_delete = 0 GROUP BY task_id) tc on t.id = tc.task_id LEFT JOIN (SELECT id,nickname, photo from $this->TABLE_USER) tu 
                    on t.create_by = tu.id
                     $createBySql";
        $sqlForCount = "SELECT FOUND_ROWS() as count";

        $where = '';

        if ($isFinish !== null) {
            $where = " AND is_finish = '$isFinish' ";
        }

        $offset = intval($page - 1) * $pageSize;

        $where .= " ORDER BY id DESC ";
        $limit = " LIMIT $offset, $pageSize ";

//        $data = $this->pdo->query($sql . $where . $limit);
//        if (!$data){
//            return [
//                'tasks'=> $this->pdo->query($sql . $where . " LIMIT 0 , $pageSize"),
//                'page'=> $page,
//                'count'=> $this->pdo->uniqueResult($sqlForCount)['count'],
//            ];
//        }

        return [
            'tasks'=> $this->pdo->query($sql . $where . $limit),
            'page'=> $page,
            'count'=> $this->pdo->uniqueResult($sqlForCount)['count'],
        ];

    }

    /**
     * 获取任务详情
     * @param $taskId 任务id
     * @return array 任务详情
     */
    public function getTaskById(int $taskId, $needResult=false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $needResultSql = $needResult ? " LEFT JOIN (SELECT taskId,count(id) as resultCount from $this->TABLE_RESULT WHERE is_find = 0 GROUP BY taskId)
                            tr on t.id = tr.taskId" : "";
        $needResultField = $needResult ? " , ifnull(resultCount,0) as resultCount" : "";
        $sql = "SELECT id,task_content as taskContent,exclude_familyIds as excludeFamilyIds, show_type as showType,
                            priority,threshold,email,cycle,is_finish as isFinish,create_by as createBy,create_time as createTime,
                            update_by as updateBy,update_time as updateTime, read_num as readNum $needResultField FROM $this->TABLE t $needResultSql WHERE id = '$taskId' LIMIT 1";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取评论详情
     * @param $commentId 评论id
     * @param $joinTask 是否需要join task表
     * @return array 任务详情
     */
    public function getCommentById(int $commentId, bool $joinTask=false)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $joinTaskSql = $joinTask ? " LEFT JOIN (SELECT id, create_by as createBy, is_finish as isFinish from $this->TABLE) t2 ON t2.id = t.task_id" : "";
        $joinTaskFieds = $joinTask ? " ,createBy " : "";
        $sql = "SELECT t.id,content,author_id as authorId,task_id as taskId $joinTaskFieds FROM $this->TABLE_COMMENT t $joinTaskSql
                WHERE t.id = $commentId and t.is_delete = 0 LIMIT 1";

        return $this->pdo->uniqueResult($sql);
    }


    /**
     * 获取评论详情
     * @param $commentId 评论id
     * @param $joinTask 是否需要join task表
     * @return array 任务详情
     */
    public function getReplyById(int $replyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT * FROM $this->TABLE_REPLY t WHERE t.id = '$replyId' and t.is_delete = 0 LIMIT 1";

        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取任务的评论以及回复
     * @param $taskId 任务id
     * @param $replyNumber 回复数量
     * @param $page 页码
     * @param $pageSize 分页大小
     * @param $userId 访问用户id
     * @param $taskUserId 人物用户id
     * @return array 评论与回复数组
     */
    public function getCommentAndReplyByTaskId(int $taskId, int $replyNumber,int $page,int $pageSize, int $userId, int $taskUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $isTaskOwnerSql = $userId == $taskUserId ? "" : " AND author_id = $userId ";

        $offset = ($page - 1) * $pageSize;
        $sql = "SELECT SQL_CALC_FOUND_ROWS t.id,content,author_id as authorId,task_id as taskId, photo as authorPhoto, nickname as authorNickname, t.create_time as createTime FROM $this->TABLE_COMMENT t 
                INNER JOIN $this->TABLE_USER tu on t.author_id = tu.id 
                WHERE task_id = $taskId AND t.is_delete = 0 $isTaskOwnerSql ORDER BY t.create_time LIMIT $offset, $pageSize";
        $sqlForCount = "SELECT FOUND_ROWS() as count";
        $comments = $this->pdo->query($sql);
        $length = $this->pdo->uniqueResult($sqlForCount)['count'];
        $commentIds = array_column($comments, 'id');
        $commentsDict = [];
        foreach ($comments as $comment){
            $commentsDict[$comment['id']] = array_merge($comment, ['reply'=>[]]);
        }

        if ($replyNumber && $commentIds){
            $isTaskOwnerSql = $userId == $taskUserId ? "" : " AND (from_user = $userId OR to_user = $userId)";
            $commettIdsSql = $commentIds ? ("t.comment_id IN (" . implode(',', $commentIds) . ") AND ") : "";
            $sql = "SELECT t.id, from_user as fromUserId, to_user as toUserId, content, tu1.nickname as fromUser, tu2.nickname as toUser, t.comment_id 
                    as commentId, tu1.photo as fromUserPhoto, tu2.photo as toUserPhoto FROM $this->TABLE_REPLY t INNER JOIN (SELECT id,comment_id FROM $this->TABLE_REPLY tr 
                    WHERE is_delete = 0 $isTaskOwnerSql GROUP BY comment_id, id HAVING (SELECT COUNT(t2.id) from $this->TABLE_REPLY t2
                    WHERE is_delete = 0 $isTaskOwnerSql AND t2.comment_id = tr.comment_id AND t2.id >= tr.id) <= $replyNumber ORDER BY create_time DESC) tr 
                    ON t.id = tr.id INNER JOIN $this->TABLE_USER tu1 ON t.from_user = tu1.id INNER JOIN $this->TABLE_USER tu2 ON t.to_user = tu2.id 
                    WHERE $commettIdsSql t.is_delete = 0 $isTaskOwnerSql ORDER BY t.create_time DESC";
            $replys = $this->pdo->query($sql);


            foreach ($replys as $reply){
                $commentsDict[$reply['commentId']]['reply'][] = $reply;
            }
        }


        return [
            'comments'=>array_values($commentsDict),
            'page'=>$page,
            'count'=>intval($length)
        ];
    }

    /**
     * 获取任务评论的回复
     * @param $commentId 任务id
     * @param $replyNumber 回复数量
     * @param $page 页码
     * @param $pageSize 分页大小
     * @param $userId 访问用户id
     * @param $taskUserId 人物用户id
     * @return array 回复数组
     */
    public function getReplyListByCommentId(int $commentId,int $page, int $pageSize, int $userId, int $taskUserId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($page - 1) * $pageSize;
        $limitSql = " LIMIT $offset, $pageSize";
        $isTaskOwnerSql = $userId == $taskUserId ? "" : " AND (from_user = $userId OR to_user = $userId)";
        $sql = "SELECT SQL_CALC_FOUND_ROWS t.id, from_user as fromUserId, to_user as toUserId, content, tu1.nickname as fromUser, 
                tu2.nickname as toUser, t.comment_id as commentId, tu1.photo as fromUserPhoto, tu2.photo as toUserPhoto 
                FROM $this->TABLE_REPLY t INNER JOIN $this->TABLE_USER tu1 ON t.from_user = tu1.id 
                INNER JOIN $this->TABLE_USER tu2 ON t.to_user = tu2.id 
                WHERE comment_id = $commentId and t.is_delete = 0 $isTaskOwnerSql ORDER BY t.create_time DESC ";
        $sqlForCount = "SELECT FOUND_ROWS() as count";

        $data = $this->pdo->query($sql . $limitSql);

        return [
            'replys'=>$data,
            'page'=>$page,
            'count'=>intval($this->pdo->uniqueResult($sqlForCount)['count'])
        ];

    }

    /**
     * 获取任务详情
     * @param $taskId 任务id
     * @return int 任务详情
     */
    public function updateTaskReadNum(int $taskId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $redlockConfig = RedisInstanceConfig::getRedlockConfig();
        $redLock = new RedLock($redlockConfig);

        // 对操作加锁
        $lock = $redLock->lock(SysConst::$LOOK_FOR_FAMILY_TASK . $taskId);
        if (is_bool($lock) && !$lock) {
            // 没有拿到锁，报错
            foreach (range(0,4) as $times){
                sleep(0.05);
                $lock = $redLock->lock(SysConst::$LOOK_FOR_FAMILY_TASK . $taskId);
                if ($lock){
                    break;
                }
            }
            if (is_bool($lock) && !$lock) {
                return 0;
            }
        }

        $sql = "update $this->TABLE set read_num = read_num + 1 where id = $taskId";
        $result = $this->pdo->update($sql);

        // 释放锁
        $redLock->unlock($lock);
        return $result;
    }

    /**
     * 创建评论
     * @param $data 数据
     * @return int insertId
     */
    public function createComment(array $data)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "insert into $this->TABLE_COMMENT set ";

        foreach ($data as $k => $v){
            $sql .= " $k = '$v',";
        }

        $result = $this->pdo->insert(substr($sql,0,-1));

        return $result;
    }

    /**
     * 更新评论
     * @param $commentId 任务id
     * @param $data 数据
     * @return int updaterow
     */
    public function updateComment(int $commentId, array $data)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "update $this->TABLE_COMMENT set ";

        foreach ($data as $k => $v){
            $sql .= " $k = '$v',";
        }

        $sql = substr($sql, 0 ,-1) . "where id = '$commentId' and is_delete = 0";

        $result = $this->pdo->update($sql);

        return $result;
    }

    /**
     * 创建回复
     * @param $data 数据
     * @return int insertId
     */
    public function createReply(array $data)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "insert into $this->TABLE_REPLY set ";

        foreach ($data as $k => $v){
            $sql .= " $k = '$v',";
        }

        $result = $this->pdo->insert(substr($sql,0, -1));

        return $result;
    }

    /**
     * 更新回复
     * @param $commentId 任务id
     * @param $data 数据
     * @return int updaterow
     */
    public function updateReply(int $replyId, array $data)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "update $this->TABLE_REPLY set ";

        foreach ($data as $k => $v){
            $sql .= " $k = '$v',";
        }

        $sql = substr($sql,0,-1) . " where id = '$replyId' and is_delete = 0";

        $result = $this->pdo->update($sql);

        return $result;
    }

    /**
     * 更新查找家族任务的设置
     * @param $taskId 任务id
     * @param $priority 优先级
     * @param $threshold 阈值
     * @param $cycle    任务执行周期
     * @param $email    通知邮箱
     * @param $userId
     * @return int 更新的记录数
     */
    public function updateTaskSettingV3($taskId, $priority, $threshold, $cycle, $email, $userId,  $showType)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET priority = '$priority',threshold = '$threshold',cycle = '$cycle',email = '$email',
                update_time = now(),update_by = '$userId', show_type = $showType WHERE id = '$taskId' AND is_finish = '0'  ";
        return $this->pdo->update($sql);
    }

}
