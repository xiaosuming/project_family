<?php
namespace DB;

use DB\RedisConnect;
use Util\TaskQueue\Producer;

class InstantMessageDB{
    private $imDB;

    public function __construct(){
        $this->imDB = $GLOBALS['redis_instant_message'];
    }

    /**
     * 将任务推送到即时聊天任务队列中
     * @param $task 任务
     * @return bool true代表成功,false代表失败
     */
    private function push($task):bool{

        $producer = new Producer();
        $producer->produce($this->imDB,$task);

        return true;
    }

    /**
     * 将即时聊天注册任务添加到队列中,这个是注册用户
     * @param $username 要发送的电话号码
     * @param $password 要发送的验证码
     * @param $faceUrl  用户头像
     * @return bool true代表成功,false代表失败
     */
    public function pushUserTask(string $username,string $password, string $faceUrl, string $nickname = '') {
        $user['u'] = $username;
        $user['p'] = $password;
        $user['f'] = $faceUrl;
        $user['n'] = $nickname;
        $user['t'] = 1;

        $userStr = json_encode($user);

        return $this->push($userStr);
    }

    /**
     * 推送创建聊天室的任务
     * @param $name 聊天室名称
     * @param $description 聊天室描述
     * @param $maxuser  最大的用户数
     * @param $owner   创建人
     * @param $members 成员
     * @return bool true代表成功，false代表失败
     */
    public function pushChatRoomTask(string $name,string $description,int $maxuser,string $owner,array $members):bool{
        
        $chatroom['n'] = $name; //聊天室名称
        $chatroom['d'] = $description;
        $chatroom['m'] = $maxuser;
        $chatroom['o'] = $owner;
        $chatroom['me'] = $members;
        $chatroom['t'] = 2;     //代表类型，1是注册用户，2是创建聊天室

        return $this->push(json_encode($chatroom));
    }

};