<?php
/**
 * 提问悬赏模块
 */

namespace DB;

use DB\CDBManager;
use DB\CDBZone;
use DB\CDBPushQuestion;
use Model\Question;
use Model\Answer;
use Util\Util;
use Util\SysLogger;
use DB\CDBPoint;
use Model\PointRecord;
use DB\CDBAccount;

class CDBQuestion
{
    public $pdo = null;
    public $TABLE = "gener_user_question";
    public $ANSWER_TABLE = "gener_question_answer";
    public $COMMENT_TABLE = "gener_answer_comment";
    public $ADDITION_TABLE = "gener_question_addition";
    public $USER_TABLE = "gener_user";
    public $LIKE_TABLE = "gener_like";
    public $TABLE_INFORMATION_CATEGORY = 'gener_information_category';

    /**
     * @codeCoverageIgnore
     */
    public function init()
    {
        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo) {
                    $GLOBALS['pdo'] = $this->pdo;
                }
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 添加问题
     * @param $socialCircleId
     * @param $userId    用户ID
     * @param $title     标题
     * @param $content   内容
     * @param $photo     照片
     * @return int 问题ID
     */
    public function addQuestion($socialCircleId, $userId, $title, $content, $photo, $point = 0)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            if ($point > 0) {
                $this->pdo->beginTransaction();
            }

            $sql = "INSERT INTO $this->TABLE (socialCircleId,userId, title, content, photo, addition, point, create_time, create_by, update_time, update_by) 
                        VALUES ('$socialCircleId','$userId', '$title', '$content', '$photo', '0', '$point', NOW(), '$userId', NOW(), '$userId')";
            $questionId = $this->pdo->insert($sql);

            //记录用户日志
            $function_name = __CLASS__.'->'.__FUNCTION__;
            $option_detail = 'APP添加问题';
            $other_msg = [
                'socialCircleId' => $socialCircleId,
                'userId' => $userId,
                'title' => $title,
                'content' => $content,
                'photo' => $photo,
                'point' => $point
            ];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $optionTableName = $this->TABLE;
            $tableId = $questionId;
            $accountDB = new CDBAccount();
            $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

            if ($point > 0) {
                $pointDB = new CDBPoint();
                $pointRecord = new PointRecord();
                $pointRecord->userId = $userId;
                $pointRecord->amount = $point;
                $pointRecord->type = 2; // 减少
                $pointRecord->module = $GLOBALS['QUESTION_TASK_MODULE'];
                $pointRecord->action = $GLOBALS['ADD_QUESTION'];

                $pointDB->updatePointForUser($pointRecord, false);
            }

            if ($point > 0) {
                $this->pdo->commit();
            }
        } catch (\PDOException $e) {
            if ($point > 0) {
                $this->pdo->rollback();
            }
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return -1;
        }


        return $questionId;
    }

    /**
     * 根据问题ID获取问题详情
     *
     * @param  $questionId  问题ID
     *
     * @return Question     问题对象
     */
    public function getQuestionByQuestionId($questionId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT q.id, q.userId,tu.nickname,tu.photo as userPhoto, q.title, q.content, q.photo, q.addition,q.socialCircleId,ic.name as socialCircleName,ic.avatar,ic.circleType, q.point,
          q.isFinish,q.create_time AS createTime, q.create_by AS createBy, q.update_time AS updateTime, q.update_by AS updateBy 
          FROM $this->TABLE as q 
          LEFT JOIN $this->TABLE_INFORMATION_CATEGORY ic ON q.socialCircleId = ic.id
          LEFT JOIN $this->USER_TABLE tu ON tu.id = q.create_by 
          WHERE q.id = '$questionId' AND q.isDelete=0";

        $res = $this->pdo->uniqueResult($sql);

        if (is_null($res)) {
            return null;
        } else {
            $question = new Question($res);
            return $question;
        }
    }

    public function getAnswerInfoByQuestionId($questionId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT distinct aw.userId,u.nickname,u.photo FROM $this->ANSWER_TABLE as aw LEFT JOIN gener_user as u ON aw.userId = u.id
                WHERE aw.questionId = '$questionId' AND aw.isDelete=0 limit 3";
        return $this->pdo->query($sql);
    }


    /**
     * 删除问题
     * @param $questionId   问题ID
     * @param $userId
     * @return int
     */
    public function deleteQuestion($questionId, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $rows = 0;//deleted rows

        try {
            //begin
            $this->pdo->beginTransaction();

            //delete addition
            $sql = "UPDATE $this->ADDITION_TABLE SET isDelete=1 WHERE questionId = '$questionId' AND isDelete=0";
            $rows += $this->pdo->update($sql);

            //get answerId
            $sql = "SELECT id AS answerId FROM $this->ANSWER_TABLE where questionId = '$questionId' AND isDelete=0";
            $answerIDs = $this->pdo->query($sql);//id collection

            //delete comments, likes and answers
            foreach ($answerIDs as $i) {
                //get answerId
                $answerId = $i['answerId'];
                //delete comments
                $sql = "UPDATE $this->COMMENT_TABLE SET isDelete=1 WHERE answerId = '$answerId' AND isDelete=0";
                $rows += $this->pdo->update($sql);
                //delete likes
                $sql = "UPDATE $this->LIKE_TABLE SET isDelete = 1 WHERE postId='$answerId' AND module='14' AND isDelete=0";
                $rows += $this->pdo->update($sql);
                //delete answer
                $sql = "UPDATE $this->ANSWER_TABLE SET isDelete=1 WHERE id = '$answerId' AND isDelete=0";
                $rows += $this->pdo->update($sql);
            }

            //delete question
            $sql = "UPDATE $this->TABLE SET isDelete=1 WHERE id = '$questionId' AND isDelete=0";
            $rows += $this->pdo->update($sql);

            //get related users
            $zoneDB = new CDBZone();
            $pushUserIds = $zoneDB->getUsers($userId);

            //set redis delete task
            $questionPush = new CDBPushQuestion();
            $taskPushResult = $questionPush->setDeletePostTask($pushUserIds, $questionId, $userId, 1);

            //commit
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return $rows;
    }

    /**
     * 检查用户操作问题的权限
     *
     * @param $userId  用户ID
     * @param $questionId  问题ID
     *
     * @return boolean
     */
    public function checkUserIdAndQuestionId($userId, $questionId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT userId FROM $this->TABLE where id = '$questionId' AND isDelete=0";

        $res = $this->pdo->uniqueResult($sql);
        if ($res['userId'] == $userId) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 添加追问
     *
     * @param  $userId      用户ID
     * @param  $questionId  问题ID
     * @param  $content     追问内容
     * @param  $photo       追问图片
     *
     * @return int  追问ID
     */
    public function addAdditionQuestion($userId, $questionId, $content, $photo)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            //insert addition table
            $sql = "INSERT INTO $this->ADDITION_TABLE
                (questionId, content, photo, create_time, create_by, update_time, update_by) VALUES 
                ('$questionId', '$content', '$photo', NOW(), '$userId', NOW(), '$userId')";
            $additionId = $this->pdo->insert($sql);

            //update question table
            $sql = "UPDATE $this->TABLE SET addition='1', update_time=NOW(), update_by='$userId' WHERE id = '$questionId'";
            $this->pdo->update($sql);

             //记录用户日志
             $function_name = __CLASS__.'->'.__FUNCTION__;
             $option_detail = 'APP添加问题追问';
             $other_msg = [
                 'questionId' => $questionId,
                 'userId' => $userId,
                 'content' => $content,
                 'photo' => $photo
             ];
             $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
             $optionTableName = $this->ADDITION_TABLE;
             $tableId = $additionId;
             $accountDB = new CDBAccount();
             $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

            //commit
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $additionId;
    }

    /**
     * 获取某个问题的所有追问
     *
     * @param  $questionId      问题ID
     *
     * @return array    追问集
     */
    public function getAdditionQuestions($questionId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, questionId, content, photo, 
                       create_time AS createTime, create_by AS createBy,
                       update_time AS updateTime, update_by AS updateBy
                FROM $this->ADDITION_TABLE WHERE questionId = '$questionId' AND isDelete=0";

        return $this->pdo->query($sql);
    }

    /**
     * 更新答案数量
     * @param $questionId 问题id
     * @param $num 更新的值
     * @return 更新回答数量
     */
    public function updateAnswerCount($questionId, $num) {
        $this->init();

        $sql = "update $this->TABLE SET answerCount = answerCount + $num WHERE id = '$questionId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 添加用户回答
     *
     * @param  $userId      用户ID
     * @param  $questionId  问题ID
     * @param  $content     回答内容
     *
     * @return int          回答ID
     */
    public function addAnswer($userId, $questionId, $content)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();

            $sql = "INSERT INTO $this->ANSWER_TABLE
            (userId, questionId, content, create_time, create_by, update_time, update_by) VALUES
            ('$userId', '$questionId', '$content', NOW(), '$userId', NOW(), '$userId')";

            $answerId = $this->pdo->insert($sql);

            //记录用户日志
            $function_name = __CLASS__.'->'.__FUNCTION__;
            $option_detail = 'APP添加问题回答';
            $other_msg = [
                'questionId' => $questionId,
                'userId' => $userId,
                'content' => $content
            ];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $optionTableName = $this->ANSWER_TABLE;
            $tableId = $answerId;
            $accountDB = new CDBAccount();
            $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

            $this->updateAnswerCount($questionId, 1);

            $this->pdo->commit();

        } catch (\PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return 0;
        }
        return $answerId;
    }

    /**
     * 根据ID获取回答详情
     * @param $answerId 回答ID
     * @return object 回答对象
     */
    public function getAnswerById($answerId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT  id, userId, questionId, content, comment_count, like_count, 
        create_time AS createTime, create_by AS createBy, update_time AS updateTime, update_by AS updateBy 
        FROM $this->ANSWER_TABLE WHERE id = '$answerId' AND isDelete=0";

        $res = $this->pdo->uniqueResult($sql);
        if (is_null($res)) {
            return null;
        } else {
            $answer = new Answer($res);
            return $answer;
        }
    }

    /**
     * 分页获取某个问题的所有回答
     *
     * @param  $questionId  问题ID
     * @param  $pageIndex   页码
     * @param  $pageSize    分页大小
     *
     * @return Object
     */
    public function getAnswersPaging($questionId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($pageIndex > 0 && $pageSize > 0) {
            $firstElem = $pageSize * ($pageIndex - 1);
            $sql = "SELECT a.id, a.userId, u.nickname, u.photo, a.questionId, a.content, a.comment_count, a.like_count,
                           a.createTime, a.createBy, a.updateTime, a.updateBy
                    FROM (SELECT id, userId, questionId, content, comment_count, like_count,
                                create_time AS createTime, create_by AS createBy,
                                update_time AS updateTime, update_by AS updateBy 
                         FROM $this->ANSWER_TABLE WHERE questionId = '$questionId' AND isDelete=0 LIMIT $firstElem, $pageSize) AS a
                    LEFT JOIN
                         (SELECT id, nickname, photo FROM $this->USER_TABLE) AS u 
                    ON u.id=a.userId";

            $res = $this->pdo->query($sql);
        } else {
            return array();
        }

        return $res;
    }

    /**
     * 获取某个问题的回答数量
     *
     * @param  $questionId     问题ID
     * @return int  总数
     */
    public function getAnswersCount($questionId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT COUNT(*) AS sum FROM $this->ANSWER_TABLE WHERE questionId = '$questionId' AND isDelete=0";

        $res = $this->pdo->uniqueResult($sql);

        return $res['sum'];
    }

    /**
     * 检查用户操作回答的权限
     *
     * @param $userId   用户ID
     * @param $answerId 回答ID
     *
     * @return boolean
     */
    public function checkUserIdAndAnswerId($userId, $answerId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT userId FROM $this->ANSWER_TABLE where id = '$answerId' AND isDelete=0";
        $res = $this->pdo->uniqueResult($sql);

        if ($res['userId'] == $userId) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 用户修改回答
     *
     * @param $answerId     回答ID
     * @param $content      回答内容
     * @param $userId       用户ID
     */
    public function updateAnswerContent($answerId, $content, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->ANSWER_TABLE 
                SET content = '$content', update_time = NOW(), update_by = '$userId'
                WHERE id = '$answerId' AND isDelete=0";

        $rows = $this->pdo->update($sql);

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = 'APP更新问题回答';
        $other_msg = [
            'answerId' => $answerId,
            'userId' => $userId,
            'content' => $content
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = $this->ANSWER_TABLE;
        $tableId = $answerId;
        $accountDB = new CDBAccount();
        $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        return $rows;
    }

    /**
     * 获取回答详情
     *
     * @param $answerId         回答ID
     *
     * @return object
     */
    public function getAnswerDetail($answerId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id, userId, questionId, content, 
                    comment_count, like_count, 
                    create_time AS createTime, update_time AS updateTime 
                FROM $this->ANSWER_TABLE WHERE id='$answerId' AND isDelete=0";

        $res = $this->pdo->uniqueResult($sql);

        if (is_null($res)) {
            return null;
        } else {
            $answer = new Answer($res);
            return $answer;
        }
    }

    /**
     * 添加评论
     *
     * @param $answerId         回答ID
     * @param $userId           用户ID
     * @param $replyTo          回复的评论ID
     * @param $replyTo_userId   回复的用户ID
     * @param $comment          评论内容
     *
     * @return int 评论ID
     */
    public function addComment($answerId, $userId, $replyTo, $replyToUserId, $comment)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            //insert comment table
            $sql = "INSERT INTO $this->COMMENT_TABLE
            (answerId, userId, replyTo, replyTo_userId, COMMENT, create_time, create_by, update_time, update_by) VALUES
            ('$answerId', '$userId', '$replyTo', '$replyToUserId', '$comment', NOW(), '$userId', NOW(), '$userId')";
            $commentId = $this->pdo->insert($sql);
            //update answer table
            $sql = "UPDATE $this->ANSWER_TABLE SET comment_count=comment_count+1 WHERE id = '$answerId'";
            $res = $this->pdo->update($sql);
             
            //记录用户日志
            $function_name = __CLASS__.'->'.__FUNCTION__;
            $option_detail = 'APP添加问题评论';
            $other_msg = [
                'answerId' => $answerId,
                'userId' => $userId,
                'replyTo' => $replyTo,
                'replyToUserId' => $replyToUserId,
                'comment' => $comment
            ];
            $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
            $optionTableName = $this->COMMENT_TABLE;
            $tableId = $commentId;
            $accountDB = new CDBAccount();
            $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);
            
            //commit
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            //@codeCoverageIgnoreEnd
        }
        return $commentId;
    }

    /**
     * 获取某个回答的所有评论
     *
     * @param  $answerId  问题ID
     *
     * @return array      评论集
     */
    public function getComments($answerId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT ac.id, ac.answerId, ac.userId, u.nickname AS nickname, u.photo AS photo,
                    ac.replyTo AS replyto, ac.replyTo_userId AS replyToUserId, ut.nickname AS replyToNickname, ac.comment,
                    ac.create_time AS createTime, ac.create_by AS createBy,
                    ac.update_time AS updateTime, ac.update_by AS updateBy
                FROM (SELECT * FROM $this->COMMENT_TABLE WHERE answerId = '$answerId' AND isDelete=0) AS ac
                INNER JOIN(SELECT id, nickname, photo FROM $this->USER_TABLE) AS u
                    ON u.id = ac.userId
                LEFT JOIN (SELECT id, nickname FROM $this->USER_TABLE) AS ut
                    ON ut.id = ac.replyTo_userId";

        return $this->pdo->query($sql);
    }

    /**
     * 删除回答
     * @param $answerId 回答id
     * @param $userId   用户id
     */
    public function deleteAnswer($answerId, $userId) {
        $this->init();

        try {

            $answer = $this->getAnswerById($answerId);

            $this->pdo->beginTransaction();


            $this->updateAnswerCount($answer->questionId, -1);

            $sql = "UPDATE $this->ANSWER_TABLE set isDelete = '1' WHERE id = '$answerId' ";
            $row = $this->pdo->update($sql);

            $this->pdo->commit();

            return $row;
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            //@codeCoverageIgnoreEnd
        }

    }

    /**
     * 检查用户操作评论的权限
     *
     * @param $userId       用户ID
     * @param $commentId    评论ID
     *
     * @return boolean
     */
    public function checkUserIdAndCommentId($userId, $commemtId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT userId FROM $this->COMMENT_TABLE WHERE id = '$commemtId' AND isDelete=0";

        $res = $this->pdo->uniqueResult($sql);
        if ($res['userId'] == $userId) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除指定评论
     *
     * @param $commemtId 评论ID
     * @return int 删除的行数
     */
    public function deleteComment($commemtId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            //select answerId
            $sql = "SELECT answerId FROM $this->COMMENT_TABLE WHERE id = '$commemtId'";
            $answerId = $this->pdo->uniqueResult($sql)['answerId'];
            //delete from commit table
            $sql = "UPDATE $this->COMMENT_TABLE SET isDelete=1 WHERE id = '$commemtId' AND isDelete=0";
            $rows = $this->pdo->update($sql);
            //update answer table
            if ($rows > 0) {
                $sql = "UPDATE $this->ANSWER_TABLE SET comment_count=comment_count-1 WHERE id = '$answerId'";
                $this->pdo->update($sql);
            }
            //commit
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();            //事务回滚
            return -1;
            //@codeCoverageIgnoreEnd
        }

        return $rows;
    }

    /**
     * 更新回答点赞数
     * @param $answerId     回答ID
     * @param $likeCount    点赞数
     */
    public function updateAnswerLikeCount($answerId, $likeCount)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        if ($likeCount < 0 || is_string($likeCount)) {
            return 0;
        }
        $sql = "UPDATE $this->ANSWER_TABLE SET like_count = '$likeCount' WHERE id = '$answerId' AND isDelete=0";

        return $this->pdo->update($sql);
    }

    /**
     * 分页获取问题
     * @param array $socialCircleIds 圈子数组
     * @param $maxId 最大id
     * @param $sinceId 起始id
     * @param $count 总数量
     * @return array 一页数据
     */
    public function getQuestionPaging($socialCircleIds, $maxId, $sinceId, $count) {
        $this->init();

        $idStr = '('.Util::arrayToString($socialCircleIds).')';

        $where = '';
        if ($maxId != 0) {
            $where .= " AND q.id < $maxId ";
        }

        if ($sinceId != 0) {
            $where .= " AND q.id > $sinceId ";
        }

        $sql = "SELECT q.id,q.socialCircleId,ic.name as socialCircleName,ic.circleType,tu.nickname,tu.photo as userPhoto, q.userId, q.title, q.content, q.photo, q.addition, q.answerCount, q.create_time as createTime, q.create_by as createBy, q.update_time as updateTime, q.update_by as updateBy
                FROM $this->TABLE q 
                LEFT JOIN $this->TABLE_INFORMATION_CATEGORY as ic ON q.socialCircleId = ic.id
                LEFT JOIN $this->USER_TABLE tu ON tu.id = q.create_by 
                WHERE q.socialCircleId in $idStr $where AND q.isDelete = 0 ORDER BY q.id DESC LIMIT 0, $count ";

        return $this->pdo->query($sql);
    }


    /**
     * 分页获取问题
     * @param array $socialCircleIds 圈子数组
     * @param $maxId 最大id
     * @param $sinceId 起始id
     * @param $count 总数量
     * @return array 一页数据
     */
    public function getQuestionTotal($socialCircleIds) {
        $this->init();

        $idStr = '('.Util::arrayToString($socialCircleIds).')';

        $sql = "SELECT count(q.id) as num FROM $this->TABLE q 
                WHERE q.socialCircleId in $idStr AND q.isDelete = 0";

        $res = $this->pdo->uniqueResult($sql);

        return $res['num'];
    }

    /**
     * 设置最佳答案
     * @param $answerId 答案id
     * @param $answerId 答案id
     * @return bool 设置成功或失败
     */
    public function setBestAnswer($questionId, $questionAmount, $answerId, $answerUserId, $userId) {
        $this->init();

        try {
            $this->pdo->beginTransaction();
            // 将当前问题完成
            $sql = "UPDATE $this->TABLE SET isFinish = 1, update_by = '$userId', update_time = now() WHERE id = '$questionId' AND isDelete = '0' ";
            $row = $this->pdo->update($sql);

            if ($row == 0) {
                $this->pdo->rollback();
                return false;
            }

            // 将答案设为最佳
            $sql = "UPDATE $this->ANSWER_TABLE SET isBest = 1, update_by = '$userId', update_time = now() WHERE id = '$answerId' AND isDelete = '0' ";
            $row = $this->pdo->update($sql);

            if ($row == 0) {
                $this->pdo->rollback();
                return false;
            }

            if ($questionAmount > 0) {
                // 修改最佳答案用户的积分
                $pointDB = new CDBPoint();
                $pointRecord = new PointRecord();
                $pointRecord->userId = $answerUserId;
                $pointRecord->amount = $questionAmount;
                $pointRecord->type = 1;
                $pointRecord->module = $GLOBALS['QUESTION_TASK_MODULE'];
                $pointRecord->action = $GLOBALS['SET_BEST_ANSWER'];

                $pointDB->updatePointForUser($pointRecord, false);
            }


            $this->pdo->commit();

            return true;
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            return false;
        }

    }
    /*
     * 获取指定问题集的问题详情
     *
     * @param $questionIdSet  问题集
     *
     * @return array
     */
    public function getQuestionsDetail($questionIdSet) {
        if(!$this->init()){
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        if($questionIdSet == "()" || $questionIdSet == "" || $questionIdSet[0] != "(" || $questionIdSet[strlen($questionIdSet)-1] != ")") {
            return null;
        }

        $sql = "SELECT q.id, q.userId, q.title, q.content, q.photo, q.addition,
                       a.answerCount,q.createTime, q.createBy, q.updateTime, q.updateBy
                       FROM
                       (SELECT id, userId, title, content, photo, addition,
                       create_time AS createTime, create_by AS createBy,
                       update_time AS updateTime, update_by AS updateBy
                       FROM $this->TABLE WHERE id IN $questionIdSet AND isDelete=0) AS q
                LEFT JOIN (SELECT questionId, COUNT(*) AS answerCount
                           FROM $this->ANSWER_TABLE WHERE isDelete=0 GROUP BY questionId
                           HAVING questionId IN $questionIdSet) AS a
                ON q.id = a.questionId";

        return $this->pdo->query($sql);
    }
}
