<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/28 0028
 * Time: 10:21
 */

namespace DB;

use DB\CDBManager;
use Util\TaskQueue\Producer;
use Util\STConvert\STConvert;

class CDBPushPerson
{

    /**
     * 设置队列
     * @param $data
     * @return mixed
     */
    public function setPushTask($data)
    {
        $str = json_encode($data);
        $producer = new Producer();
        $producer->produce('redis_search_task', $str);
        return true;
    }


    /**
     * @param $type //任务类型,1是家族，2是人物，3是大事件，4是宗祠
     * @param $familyId // 家族id
     * @param $modelId //元数据id
     * @param $action //操作类型，1是增加，2是更新，3是删除
     * @param $source //任务内容
     * @return mixed
     */
    public function setPushPersonTask($modelId, $action, $source, $type = 2)
    {

        $stConvert = new STConvert(STConvert::CONVERT_TYPE_T2S);

        $id = $source->id;
        $CDBFamily = new CDBFamily();
        $familyId = $source->familyId;
        $family = $CDBFamily->getFamilyById($familyId);
        $familyName = $family->name;
        $photo = $source->photo;
        $name = $source->name;
        $birthday = $source->birthday;
        $zi = $source->zi;
        $zpname = $source->zpname;
        $familyId = $source->familyId;
        $remark = $source->remark;
        $sideText = $source->sideText;
        $profileText = $source->profileText;

        $countryName=$source->countryName;
        $provinceName=$source->provinceName;
        $cityName=$source->cityName;
        $areaName=$source->areaName;
        $townName=$source->townName;

        $address = $countryName . $provinceName  . $cityName  . $areaName  . $townName  . $source->address;
        $patterns = "/\d+/";
        $address=preg_replace($patterns,'',$address);
        $data['type'] = $type;
        $data['modelId'] = $modelId;
        $data['action'] = $action;
        $data['source'] = [
            'id' => $id,
            'familyId' => $familyId,
            'familyName' => $familyName,
            'photo' => $photo,
            'name' => $name,
            's_name' =>  $stConvert->convert($name),
            'birthday' => $birthday,
            'zi' => $zi,
            'zpname' => $zpname,
            'remark' => $remark,
            'address' => $address,
            'sideText' => $sideText,
            'profileText' => $profileText,
            'favorites' => array(),
            'skills' => array(),
            'jobs' => array()
        ];
        return $this->setPushTask($data);
    }

    /**
     * @param $modelId
     * @param $action
     * @param $source
     * @param int $type
     * @return mixed
     */
    public function setPushPersonSkills($modelId, $action, $source, $type = 2)
    {
        $personId = $source->id;
        $skills = $this->getPersonSkills($personId);
        $data['type'] = $type;
        $data['modelId'] = $modelId;
        $data['action'] = $action;
        $data['source'] = [
            'skills' => $skills
        ];
        return $this->setPushTask($data);
    }

    /**
     * @param $modelId
     * @param $action
     * @param $source
     * @param int $type
     * @return mixed
     */
    public function setPushPersonFavorites($modelId, $action, $source, $type = 2)
    {
        $personId = $source->id;
        $favorites = $this->getPersonFavorites($personId);
        $data['type'] = $type;
        $data['modelId'] = $modelId;
        $data['action'] = $action;
        $data['source'] = [
            'favorites' => $favorites
        ];
        return $this->setPushTask($data);
    }

    /**
     * @param $modelId
     * @param $action
     * @param $source
     * @param int $type
     * @return mixed
     */
    public function setPushPersonJobs($modelId, $action, $source, $type = 2)
    {
        $personId = $source->id;
        $jobs = $this->getPersonJobMsg($personId);
        $data['type'] = $type;
        $data['modelId'] = $modelId;
        $data['action'] = $action;
        $data['source'] = [
            'jobs' => $jobs
        ];
        return $this->setPushTask($data);
    }

    /**
     * @param $modelId
     * @param $action
     * @param $source
     * @param int $type
     * @return bool|mixed
     */
    public function setPushPersonPhoto($modelId, $action, $source, $type = 2)
    {
        $personId = $source->id;
        $CDBPerson = new CDBPerson();
        $result = $CDBPerson->getPhotosByPersonId($personId);
        if ($result != null) {
            $data['type'] = $type;
            $data['modelId'] = $modelId;
            $data['action'] = $action;
            $data['source'] = [
                'photo' => $result['photo']
            ];
            return $this->setPushTask($data);
        }
        return false;
    }


    /**
     * @param $modelId
     * @param $action
     * @param $source
     * @param int $type
     * @return mixed
     */
    public function setEditPushPersonTask($modelId, $action, $source, $type = 2)
    {
        $name = $source->name;
        $birthday = $source->birthday;
        $zi = $source->zi;
        $remark = $source->remark;
        $address = $source->countryName . $source->provinceName . $source->cityName . $source->areaName . $source->townName . $source->address;
        $sideText = $source->sideText;
        $profileText = $source->profileText;
        $patterns = "/\d+/";
        $address=preg_replace($patterns,'',$address);
        $data['type'] = $type;
        $data['modelId'] = $modelId;
        $data['action'] = $action;
        $data['source'] = [
            'name' => $name,
            'birthday' => $birthday,
            'zi' => $zi,
            'remark' => $remark,
            'address' => $address,
            'profileText' => $profileText,
            'sideText' => $sideText
        ];
        return $this->setPushTask($data);
    }


    /**
     * @param $modelId
     * @param $action
     * @param $source
     * @param int $type
     * @return mixed
     */
    public function setPushListForDeletePerson($modelId, $action, $source, $type = 2)
    {
        $data['type'] = $type;
        $data['modelId'] = $modelId;
        $data['action'] = $action;
        $data['source'] = $source;
        return $this->setPushTask($data);
    }

    /**
     * @param $personId
     * @return array
     */
    public function getPersonJobMsg($personId)
    {
        $jobs = array();
        $CDBPerson = new CDBPerson();
        //获取人物的工作信息
        $result = $CDBPerson->getPersonJob($personId);
        if ($result != '') {
            foreach ($result as $v) {
                $value = $v['company'] . $v['occupation'];
                array_push($jobs, $value);
            }
        }
        return $jobs;
    }

    /**
     * @param $personId
     * @return array
     */
    public function getPersonSkills($personId)
    {
        $skills = array();
        $CDBPerson = new CDBPerson();
        //获取人物的爱好,技能
        $result = $CDBPerson->getPersonTag($personId);
        if ($result != '') {
            foreach ($result as $v) {
                if ($v['type'] == 1) {
                    array_push($skills, $v['info']);
                }
            }
        }
        return $skills;
    }

    /**
     * @param $personId
     * @return array
     */
    public function getPersonFavorites($personId)
    {
        $favorites = array();
        $CDBPerson = new CDBPerson();
        //获取人物的爱好,技能
        $result = $CDBPerson->getPersonTag($personId);
        if ($result != '') {
            foreach ($result as $v) {
                if ($v['type'] == 2) {
                    array_push($favorites, $v['info']);
                }
            }
        }
        return $favorites;
    }

}
