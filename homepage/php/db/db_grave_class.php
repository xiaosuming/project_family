<?php
/**
 * 地图模块
 * author: jiangpengfei
 * date: 2017-03-29
 */

namespace DB;

use DB\CDBManager;
use DB\CDBFamily;
use Model\Grave;
use Util\Util;
use DB\CDBAccount;

class CDBGrave
{
    public $pdo = null;
    public $TABLE = "gener_grave";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }


    /**
     * 验证某个用户对某个祖坟的操作权限
     * 验证规则:1、管理员有权限操作任何家族内的祖坟;2、谁创建谁有权操作
     * @param $userId 用户id
     * @param $graveId 祖坟id
     * @return boolean true有权限，false无权限
     */
    public function verifyUserIdAndGraveId($userId, $graveId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        //1、先获取祖坟所属家族id
        $grave = $this->getGrave($graveId);
        $familyId = $grave->familyId;

        //2.查询当前用户是否是家族管理员
        $familyDB = new CDBFamily();
        if ($familyDB->isAdminForFamily($familyId, $userId)) {
            return true;    //是管理员，可以操作
        } else {
            //不是管理员，判断是不是这个祖坟的创建者
            if ($grave->createBy == $userId) {
                return true;      //是创建者，可以操作
            } else {
                return false;
            }
        }

    }

    /**
     * 检查人物的祖坟是否存在
     * @param $personId 人物id
     * @return bool true存在,false不存在
     */
    public function checkPersonGraveExist($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE  personId = '$personId' AND isDelete=0 ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 增加祖坟的地址
     * @param $personId 人物id
     * @param $personName
     * @param $familyId 家族id
     * @param $coorX 坐标的x值
     * @param $coorY 坐标的y值
     * @param $address 地址名称
     * @param $photo 图片地址
     * @param $description 描述
     * @param $userId 用户id
     * @return int 增加的祖坟Id
     */
    public function addGrave($personId, $personName, $familyId, $coorX, $coorY, $address, $photo, $description, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE(personId,person_name,familyId,coor_x,coor_y,address,photo,description,create_time,create_by,update_time,update_by)
                    VALUES('$personId','$personName','$familyId','$coorX','$coorY','$address','$photo','$description',now(),'$userId',now(),'$userId')";

        $id = $this->pdo->insert($sql);

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '添加祖坟/宗祠';
        $other_msg = [
            'personId' => $personId,
            'userId' => $userId,
            'personName' => $personName,
            'familyId' => $familyId,
            'coorX' => $coorX,
            'coorY' => $coorY,
            'address' => $address,
            'photo' => $photo,
            'description' => $description
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = $this->TABLE;
        $tableId = $id;
        $accountDB = new CDBAccount();
        $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        return $id;

    }

    /**
     * 更改祖坟的信息
     * @param $graveId 祖坟id
     * @param $coorX x坐标
     * @param $coorY y坐标
     * @param $address 地址名称
     * @param $photo 照片url
     * @param $description 描述
     * @param $userId 操作用户
     * @return int 修改的记录个数
     */
    public function updateGrave($graveId, $coorX, $coorY, $address, $photo, $description, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE set coor_x = '$coorX',coor_y = '$coorY',address = '$address',photo = '$photo',description = '$description',update_time = now(),update_by = $userId WHERE id = '$graveId' AND isDelete=0";

        $updateId = $this->pdo->update($sql);

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '更新祖坟/宗祠';
        $other_msg = [
            'graveId' => $graveId,
            'userId' => $userId,
            'updateId' => $updateId,
            'coorX' => $coorX,
            'coorY' => $coorY,
            'address' => $address,
            'photo' => $photo,
            'description' => $description
        ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = $this->TABLE;
        $tableId = $graveId;
        $accountDB = new CDBAccount();
        $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        return $updateId;
    }

    /**
     * 获取祖坟信息
     * @param $graveId 祖坟id
     * @return object 祖坟信息
     */
    public function getGrave($graveId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,personId,person_name as personName,familyId,coor_x as coorX,coor_y as coorY,address,photo,description,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy FROM $this->TABLE WHERE id = '$graveId' AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return new Grave($result);
        }
        return null;
    }

    /**
     * 删除祖坟信息
     * @param $graveId 祖坟id
     * @return int 删除的记录数
     */
    public function deleteGrave($graveId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET isDelete=1 WHERE id = $graveId AND isDelete=0";
        $deleteCount = $this->pdo->update($sql);
        return $deleteCount;
    }

    /**
     * 获取一页的祖坟信息
     * @param $pageIndex 页码
     * @param $pageSize 每页大小
     * @param $familyId 家族Id
     * @return array 一页的祖坟信息
     */
    public function getGravesInfoPaging($pageIndex, $pageSize, $familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT id,personId,person_name as personName,familyId,coor_x as coorX,coor_y as coorY,address,photo,description,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy FROM $this->TABLE WHERE familyId = '$familyId' AND isDelete=0 order by id desc limit $offset,$pageSize";

        return $this->pdo->query($sql);

    }

    /**
     * 获取祖坟的总数
     * @param $familyId 家族Id
     * @return int 祖坟总数
     */
    public function getGravesInfoCount($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE WHERE familyId = '$familyId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result['count(id)'];
        } else {
            //@codeCoverageIgnoreStart
            return 0;
            //@codeCoverageIgnoreEnd
        }
    }

    /**
     * 根据人物ID获取祖坟信息
     * @param $personId
     * @return mixed
     */
    public function getGraveInfoByPersonId($personId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,personId,person_name as personName,familyId,coor_x as coorX,coor_y as coorY,address,photo,description,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy FROM $this->TABLE WHERE personId = '$personId' AND isDelete=0 ";

        return $this->pdo->query($sql);
    }

    /**
     * 未创建宗祠的人物列表
     * @param $familyId
     * @return mixed
     */
    public function getUnCreateGravePersonListByFamilyId($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT gp.id,gp.name as personName,gf.name as familyName FROM gener_person as gp LEFT JOIN gener_family as gf
 
          ON gp.familyId = gf.id WHERE gp.familyId='$familyId' AND gp.is_delete=0 AND gp.id NOT IN (SELECT personId FROM gener_grave
          
            WHERE familyId = '$familyId' AND isDelete=0)";

        return $this->pdo->query($sql);
    }
}
