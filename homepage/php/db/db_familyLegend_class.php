<?php
/**
 * 家族传说模块
 * author:jiangpengfei
 * date:2017-04-20
 */

namespace DB;

use DB\CDBManager;
use DB\CDBPerson;
use DB\CDBFamily;
use Model\FamilyLegend;
use Util\Util;

class CDBFamilyLegend
{
    var $pdo = null;
    var $TABLE = "gener_family_legend";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }


    /**
     * 检查用户id和家族传说id的权限是否匹配
     * @param $userId 用户Id
     * @param $legendId 事件id
     * @return bool true是有权限，false为没有权限
     */
    public function verifyUserIdAndLegendId($userId, $legendId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT familyId FROM $this->TABLE WHERE id = '$legendId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        }
        $familyId = $result['familyId'];
        $familyDB = new CDBFamily();
        return $familyDB->isUserForFamily($familyId, $userId);
    }

    /**
     * 添加家族传说
     * @param $familyId 家族Id
     * @param $title    传说的标题
     * @param $content  传说的内容
     * @param $userId   用户id
     * @return int 添加的记录id
     */
    public function addLegend($familyId, $title, $content, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE(familyId,title,content,create_by,create_time,update_by,update_time)VALUES('$familyId','$title','$content','$userId',now(),'$userId',now())";
        return $this->pdo->insert($sql);
    }

    /**
     * 更新传说内容
     * @param $legendId 传说Id
     * @param $title    传说标题
     * @param $content  传说内容
     * @param $userId   用户id
     * @return int 更新的记录数
     */
    public function updateLegend($legendId, $title, $content, $userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET title = '$title',content = '$content',update_time = now(),update_by = '$userId' WHERE id = '$legendId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 删除传说记录
     * @param $legendId 传说id
     * @return int 删除的记录数
     */
    public function deleteLegend($legendId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET isDelete=1 WHERE id = '$legendId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 获取传说详情
     * @param $legendId 传说id
     * @return object 传说的对象
     */
    public function getLegend($legendId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,familyId,title,content,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
                    FROM $this->TABLE WHERE id = '$legendId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return new FamilyLegend($result);
        } else {
            return null;
        }
    }

    /**
     * 获取传说的总数
     * @param $familyId 家族id
     * @return int 获取传说的总数
     */
    public function getLegendsTotal($familyId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE WHERE familyId = '$familyId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result['count(id)'];
        } else {
            //@codeCoverageIgnoreStart
            return 0;
            //@codeCoverageIgnoreEnd
        }
    }

    /**
     * 获取一页传说的数据
     * @param $familyId 家族id
     * @param $pageIndex 页码
     * @param $pageSize  每页记录数
     * @return array 一页的内容数组
     */
    public function getLegendsPaging($familyId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT id,familyId,title,content,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
                    FROM $this->TABLE WHERE familyId = '$familyId' AND isDelete=0 ORDER BY id DESC LIMIT $offset,$pageSize ";
        return $this->pdo->query($sql);
    }

}
