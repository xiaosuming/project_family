<?php
namespace DB;

use DB\RedisConnect;
use Util\TaskQueue\Producer;

class MailDB{
    private $redis;
    private $mailDB;

    public function __construct(){
        $this->mailDB = $GLOBALS['redis_mail'];
    }
    
    /**
     * 将邮件添加到队列中，准备发送
     * @param $email 要发送的邮箱地址 
     * @param $content 要发送的邮件内容
     * @return boolean true代表成功,false代表失败
     */
    public function pushMail($email,$content){

        //使用Json保存邮件
        $data['e'] = $email;
        $data['c'] = $content;
        $data['t'] = 0;         //已发送的次数

        $producer = new Producer();
        $producer->produce($this->mailDB,json_encode($data));

        return true;
    }
    

};