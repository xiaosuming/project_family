<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/2 0002
 * Time: 17:22
 */

namespace DB;

use Util\TaskQueue\Producer;

class CDBPushGrave
{
    /**
     * 设置队列
     * @param $data
     * @return mixed
     */
    public function setPushTask($data)
    {
        $str = json_encode($data);
        $producer = new Producer();
        $producer->produce('redis_search_task', $str);
        return true;
    }

    /**
     * @param $type //任务类型,1是家族，2是人物，3是大事件，4是宗祠
     * @param $modelId //元数据id
     * @param $action //操作类型，1是增加，2是更新，3是删除
     * @param $source //任务内容
     * @return mixed
     */
    public function setPushGraveTask($modelId, $action, $source, $type = 4)
    {
        $data['type'] = $type;
        $data['modelId'] = $modelId;
        $data['action'] = $action;
        $data['source'] = $source;
        return $this->setPushTask($data);
    }


}