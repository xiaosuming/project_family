<?php
/**
 * 家族新鲜事模块
 * author: jiangpengfei
 * date: 2017-02-08
 */

namespace DB;

use DB\CDBManager;
use DB\CDBPerson;
use DB\CDBAccount;
use Model\FamilyNews;
use Util\Util;

class CDBFamilyNews
{
        public $pdo = null;
		public $TABLE = "gener_familyNews";
        public $TABLE_PERSON = "gener_person";
        public $TABLE_FAMILYADMIN = "gener_familyAdmin";
        public $TABLE_USER = "gener_user";
        public $TABLE_FAMILY = "gener_family";

        public function init()
        {

            if (!$this->pdo)
            {
                //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
                if (!isset($GLOBALS['pdo']))
                {
                        $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                        if ($this->pdo)
                                $GLOBALS['pdo'] = $this->pdo;
                }
                else
                {
                        $this->pdo = $GLOBALS['pdo'];
                }
            }
            return true;
        }

        /**
         * 验证用户id和家族新鲜事id是否有对应的管理权限，即该用户是否有权限管理家族新鲜事（删除）
         * @param $userId 用户id
         * @param $familyNewsId 家族新鲜事id
         * @return bool true表示有权限，false表示没有权限
         */
        public function verifyUserIdAndFamilyNewsIdManagePermission($userId,$familyNewsId){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }

            $sql = "SELECT count(*) FROM $this->TABLE_FAMILYADMIN tfa 
                    INNER JOIN $this->TABLE tfn ON tfn.familyId = tfa.familyId WHERE tfa.adminId = '$userId' AND tfn.id = '$familyNewsId' AND tfn.isDelete=0";
            $result = $this->pdo->uniqueResult($sql);

            if($result['count(*)'] > 0){
                //大于0表示是管理员或者创始人
                return true;
            }
            return false;
        }

        /**
         * 验证用户id和家族新鲜事id是否有对应的查看权限，即该用户是否有权限查看家族新鲜事
         * @param $userId 用户id
         * @param $familyNewsId 家族新鲜事id
         * @return bool true表示有权限，false表示没有权限
         */
        public function verifyUserIdAndFamilyNewsIdReadPermission($userId,$familyNewsId){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }

            $sql = "SELECT count(*) FROM $this->TABLE_PERSON tp
                    INNER JOIN $this->TABLE tfn ON tfn.familyId = tp.familyId WHERE tp.userId = '$userId' AND tfn.id='$familyNewsId' AND tfn.isDelete=0";
            $result = $this->pdo->uniqueResult($sql);

            if($result['count(*)'] > 0){
                //大于0表示是同属于一个家族
                return true;
            }
            return false;
        }

    /**
     * 新增家族新闻
     * @param $familyId 家族id
     * @param $module 新鲜事所属模块
     * @param $action 动作
     * @param $operatorId 操作人的id
     * @param $thingId
     * @param $thingName
     * @param $userId 当前登录的用户
     * @param string $extraInfo1
     * @param string $extraInfo2
     * @return int 新鲜事的id或者新增失败返回-1
     * @internal param 动作id $actionId
     * @internal param 操作的事物，在这里一般情况下表现为id值，例如新加入的成员的id，发布的文章的id $thing
     */
        public function addFamilyNews($familyId,$module,$action,$operatorId,$thingId,$thingName,$userId,$extraInfo1 = "",$extraInfo2 = ""){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }

            $sql = "INSERT INTO $this->TABLE(familyId,module,action,operatorId,thingId,thing_name,extra_info1,extra_info2,create_by,create_time,update_by,update_time)
                    VALUES('$familyId','$module','$action','$operatorId','$thingId','$thingName','$extraInfo1','$extraInfo2','$userId',now(),'$userId',now())";

            $familyNewsId = $this->pdo->insert($sql);
            return $familyNewsId;
        }

        /**
         * 同时新增多条家族新闻,因为性能问题，不能使用循环来使用addFamilyNews
         * @param $insertValue sql Insert语句的string值
         * @return int 最后一条插入的id
         */
        public function addMultiFamilyNews($insertValue){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }

            $sql = "INSERT INTO $this->TABLE(familyId,module,action,operatorId,thingId,thing_name,extra_info1,extra_info2,create_by,create_time,update_by,update_time)
                    VALUES $insertValue";

            $familyNewsId = $this->pdo->insert($sql);
            return $familyNewsId;
        }

        /**
         * 删除家族新闻
         * @param $familyNewsId 家族新闻的id
         * @return int 被删除的新闻条数
         */
        public function deleteFamilyNews($familyNewsId){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }

            $sql = "UPDATE $this->TABLE SET isDelete=1 WHERE id = '$familyNewsId' AND isDelete=0";
            $deleteRows = $this->pdo->update($sql);
            return $deleteRows;
        }

        /**
         * 获取家族新鲜事的总数
         * @param $familyId 家族id
         * @return int 总数
         */
        public function queryFamilyNewsCount($familyId){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }

            $sql = "SELECT count(*) FROM $this->TABLE WHERE familyId = '$familyId' AND isDelete=0 ";
            $result = $this->pdo->uniqueResult($sql);
            $count = $result['count(*)'];
            return $count;
        }

    /**
     * 查询家族新鲜事一页数据
     * @param $pageIndex 页码
     * @param $pageSize
     * @param $familyId  家族id
     * @return array 返回一页数据的数组
     * @internal param 页面数据量 $oageSize
     */
        public function queryFamilyNewsPaging($pageIndex,$pageSize,$familyId){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }
            $offset = ($pageIndex - 1)*$pageSize;

            $sql = "SELECT tb.id,tb.familyId,tf.name as familyName,tu.username,tu.nickname,tu.photo,tb.module,tb.action,tb.operatorId,tp.name,tb.thingId,tb.thing_name as thingName,extra_info1 as extraInfo1,extra_info2 as extraInfo2,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                    FROM $this->TABLE tb 
                    LEFT JOIN $this->TABLE_USER tu on tu.id = tb.operatorId 
                    left join $this->TABLE_PERSON tp on tb.operatorId = tp.id 
                    LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId 
                    WHERE tb.familyId = '$familyId' AND tb.isDelete=0 ORDER BY tb.id DESC limit $offset,$pageSize  ";
            $result = $this->pdo->query($sql);
            return $result;
        }

        /**
         * 根据新鲜事id获取事件的详情
         * @param $familyNewsId 家族新鲜事的id
         * @return object 家族新鲜事的对象
         */
        public function getFamilyNews($familyNewsId){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }
            $sql = "SELECT tb.id,tb.familyId,tf.name as familyName,tb.module,tb.action,tb.operatorId,tb.thingId,tb.thing_name as thingName,tb.extra_info1 as extraInfo1,tb.extra_info2 as extraInfo2,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime FROM $this->TABLE tb
            LEFT JOIN $this->TABLE_FAMILY tf ON tf.id = tb.familyId 
            WHERE tb.id = '$familyNewsId'  AND tb.isDelete=0";
            $result = $this->pdo->uniqueResult($sql);
            return new FamilyNews($result);
        }


        /**
         * 我应该看到的家族新鲜事的总数
         * @param $userId 用户Id
         * @return int 返回总数
         */
        public function getMyFamilyNewsCount($userId){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }

            $sql = "SELECT count(id) FROM $this->TABLE WHERE familyId in (
                       SELECT familyId from $this->TABLE_PERSON WHERE userId = '$userId'
                   ) " ;

            $result = $this->pdo->uniqueResult($sql);
            if($result != null){
                return $result['count(id)'];
            }
            return 0;
        }

        /**
         * 获取我应该看到的家族新鲜事的一页数据
         * @param $pageIndex 页码
         * @param $pageSize  页大小
         * @param $userId    用户id
         * @return array 新鲜事的一页数据
         */
        public function getMyFamilyNewsPaging($pageIndex,$pageSize,$userId){
            if(!$this->init()){
                Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
                exit;
            }

            $offset = ($pageIndex - 1) * $pageSize;

            $sql = "SELECT tb.id,tb.familyId,tf.name as familyName,tb.module,tb.action,tb.operatorId,tu.username,tu.nickname,tu.photo,tb.thingId,tb.thing_name as thingName,tb.extra_info1 as extraInfo1,tb.extra_info2 as extraInfo2,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                    FROM $this->TABLE tb 
                    LEFT JOIN $this->TABLE_USER tu on tu.id = tb.operatorId 
                    LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId 
                    WHERE tb.isDelete=0 AND tb.familyId IN (
                       SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId'
                   )  ORDER BY tb.id DESC LIMIT $offset,$pageSize" ;

            $result = $this->pdo->query($sql);

            return $result;
        }

    /**
     * 查询家族新鲜事一页数据
     * @param $pageIndex 页码
     * @param $pageSize
     * @param $familyId  家族id
     * @return array 返回一页数据的数组
     * @internal param 页面数据量 $oageSize
     */
    public function queryFamilyNewsPagingV3($pageIndex,$pageSize,$familyId, $nickname){
        if(!$this->init()){
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
        }
        $offset = ($pageIndex - 1) * $pageSize;
        $nicknameFilter = $nickname  ? " AND tu.nickname LIKE '%$nickname%' ":"";
//        left join $this->TABLE_PERSON tp on tb.thingId = tp.id
        $sql = "SELECT SQL_CALC_FOUND_ROWS tb.familyId,tf.name as familyName,tu.username,tu.nickname,tu.photo,tb.module,tb.action,tb.operatorId, thingId as personId, thing_name as name, tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                    FROM $this->TABLE tb 
                    LEFT JOIN $this->TABLE_USER tu on tu.id = tb.operatorId 

                    LEFT JOIN $this->TABLE_FAMILY tf on tf.id = tb.familyId 
                    WHERE tb.familyId = '$familyId' AND tb.isDelete=0 AND tb.module = 8 $nicknameFilter ORDER BY tb.id DESC limit $offset,$pageSize  ";
        $sqlForCount = "SELECT FOUND_ROWS() as count";
        $result = $this->pdo->query($sql);
        $length = $this->pdo->uniqueResult($sqlForCount)['count'];
        return [
            'page' => $pageIndex,
            'count' => $length,
            'data' => $result,
        ];
    }
};
