<?php
/**
 * 收藏模块
 * author: jiangpengfei
 * date: 2017-06-26
 */

namespace DB;

use DB\CDBManager;
use Util\Util;

class CDBCollection
{
        public $pdo = null;
		public $TABLE = "gener_collection";
        public $TABLE_USER_PROFILE = "gener_user_profile";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function initial()
        {

            if (!$this->pdo)
            {
                //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
                if (!isset($GLOBALS['pdo']))
                {
                        $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                        if ($this->pdo)
                                $GLOBALS['pdo'] = $this->pdo;
                }
                else
                {
                        $this->pdo = $GLOBALS['pdo'];
                }
            }
            return true;
        }

        /**
         * 添加收藏
         * @param $moduleId 模块id
         * @param $recordId 收藏的记录id
         * @param $recordTitle 记录的标题
         * @param $recordContent 记录的内容摘要
         * @param $extraInfo1   额外信息１
         * @param $extraInfo2   额外信息２
         * @param $userId 操作用户id
         * @return int 收藏id
         */
        public function addCollection($moduleId,$recordId,$recordTitle,$recordContent,$extraInfo1,$extraInfo2,$userId){
            if(!$this->initial()){
                // @codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
                exit;
                // @codeCoverageIgnoreEnd
			}
            try{

                $this->pdo->beginTransaction();
                $sql = "INSERT INTO $this->TABLE(userId,moduleId,recordId,record_title,record_content,extra_info1,extra_info2,create_time,create_by,update_time,update_by)VALUES('$userId','$moduleId','$recordId','$recordTitle','$recordContent','$extraInfo1','$extraInfo2',now(),'$userId',now(),'$userId') ";

                $result =  $this->pdo->insert($sql);
                if($result > 0){
                    //添加收藏的同时要更新用户资料中的收藏数
                    $sql = "UPDATE $this->TABLE_USER_PROFILE SET collection_num = collection_num + 1 WHERE userId = '$userId' ";
                    $this->pdo->update($sql);
                }

                $this->pdo->commit();
                // @codeCoverageIgnoreStart
            }catch(PDOException $e){
                $this->pdo->rollback();
                return -1;
                // @codeCoverageIgnoreEnd
            }
            return $result;
        }


        /**
         * 检查收藏是否存在
         * @param $moduleId 模块id
         * @param $recordId 记录id
         * @param $userId   用户id
         * @return boolean true代表存在,false不存在
         */
        public function checkCollectionExists($moduleId,$recordId,$userId){
            if(!$this->initial()){
                // @codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
                exit;
                // @codeCoverageIgnoreEnd
			}

            $sql = "SELECT count(id) FROM $this->TABLE WHERE userId = '$userId' AND moduleId = '$moduleId' AND recordId = '$recordId' AND is_delete = '0' ";

            $result = $this->pdo->uniqueResult($sql);
            return $result['count(id)'] > 0;
        }

        /**
         * 获取收藏总数
         * @param $userId 用户id
         * @return int 收藏的数量
         */
        public function getCollectionsTotal($userId){
            if(!$this->initial()){
                // @codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
                exit;
                // @codeCoverageIgnoreEnd
			}

            $sql = "SELECT count(id) FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0'";
            $result = $this->pdo->uniqueResult($sql);
            return $result['count(id)'];
        }

        /**
         * 获取一页收藏记录
         * @param $pageIndex 页码
         * @param $pageSize  页大小
         * @param $userId    用户id
         * @return array 一页的收藏记录
         */
        public function getCollectionsPaging($pageIndex,$pageSize,$userId){
            if(!$this->initial()){
                // @codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
                exit;
                // @codeCoverageIgnoreEnd
			}
            $offset = ($pageIndex - 1) * $pageSize;

            $sql = "SELECT id,userId,moduleId,recordId,record_title as recordTitle,record_content as recordContent,extra_info1 as extraInfo1,extra_info2 as extraInfo2,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy FROM $this->TABLE WHERE userId = '$userId' AND is_delete = '0' order by id desc limit $offset,$pageSize";

            return $this->pdo->query($sql);
        }

        /**
         * 删除收藏
         * @param $collectionId 收藏id
         * @return int 删除的记录数
         */
        public function deleteCollection($collectionId){
            if(!$this->initial()){
                // @codeCoverageIgnoreStart
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
                exit;
                // @codeCoverageIgnoreEnd
			}

            try{
                $this->pdo->beginTransaction();
                $sql = "UPDATE $this->TABLE SET is_delete = '1' WHERE id = '$collectionId' ";

                $result = $this->pdo->update($sql);


                if($result > 0){
                    //添加收藏的同时要更新用户资料中的收藏数
                    $sql = "UPDATE $this->TABLE_USER_PROFILE SET collection_num = collection_num - 1 WHERE userId in (
                        SELECT userId FROM $this->TABLE WHERE id = '$collectionId') ";
                    $this->pdo->update($sql);
                }

                $this->pdo->commit();
                // @codeCoverageIgnoreStart
            }catch(PDOException $e){
                $this->pdo->rollback();
                return -1;
                // @codeCoverageIgnoreEnd
            }

            return $result;
        }

};
