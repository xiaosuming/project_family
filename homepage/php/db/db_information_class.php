<?php
/**
 * Created by PhpStorm.
 * User: xll
 * Date: 18-10-29
 * Time: 下午2:39
 */

namespace DB;

use Util\Util;
use Util\SysLogger;
use DB\CDBAccount;

class CDBInformation
{
    public $pdo = null;
    public $infoCategoryTable = 'gener_information_category';
    public $userTable = 'gener_user';
    public $infoTable = 'gener_information';
    public $followTable = 'gener_information_follow';
    public $commentTable = 'gener_information_comment';
    public $likeTable = 'gener_information_like';
    public $shareTable = 'gener_information_share';
    public $sourceTable = 'gener_information_resource';
    public $TABLE_USER_PROFILE = "gener_user_profile";
    public $TABLE_FAMILY = 'gener_family';


    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo) {
                    $GLOBALS['pdo'] = $this->pdo;
                }
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 检查是否是系统用户
     * @param $userId
     * @return bool
     */
    public function isSystemUser($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT user_type FROM $this->userTable WHERE id='$userId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            if ($result['user_type'] == 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 添加资讯分类
     * @param $userId
     * @param $avatar
     * @param $name
     * @param $pid
     * @param $circleType
     * @return mixed
     */
    public function addInformationCategory($userId, $avatar, $name, $pid, $circleType)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO $this->infoCategoryTable (userId,avatar,name,pid,circleType,create_by,create_time,update_by,update_time)
          VALUES ('$userId','$avatar','$name','$pid','$circleType','$userId',now(),'$userId',now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 获取资讯分类列表
     * @return mixed
     */
    public function getInformationCategoryList()
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,userId,avatar,name,pid,num,follow,type,circleType,is_delete as isDelete,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime
                FROM  $this->infoCategoryTable WHERE is_delete = 0 AND (circleType=1 OR circleType=2)";
        return $this->pdo->query($sql);
    }

    /**
     * 获取用户资讯分类列表
     * @return mixed
     */
    public function getInformationCategoryListByUserId($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,userId,avatar,name,pid,num,follow,type,circleType,is_delete as isDelete,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime
                FROM  $this->infoCategoryTable WHERE is_delete = 0 AND userId = '$userId'";
        return $this->pdo->query($sql);
    }

    /**
     * 获取普通用户可以发布的分类列表
     * @return mixed
     */
    public function getUserPublishInformationCategoryList()
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,userId,avatar,name,pid,num,follow,type,circleType,is_delete as isDelete,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime
                FROM  $this->infoCategoryTable WHERE is_delete = 0 AND (circleType = 1 or circleType = 3 or circleType = 4 or circleType=5)";
        return $this->pdo->query($sql);
    }

    public function getUserPublishInfoCategoryList($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT f.id,f.userId,u.username,u.nickname,u.photo,f.categoryId,ic.avatar,ic.name,ic.num,ic.follow,ic.type,ic.circleType,f.isDelete,f.createTime,f.createBy,f.updateTime,f.updateBy FROM 
              $this->followTable as f 
              LEFT JOIN $this->infoCategoryTable as ic ON f.categoryId=ic.id
              LEFT JOIN $this->userTable as u ON f.userId=u.id 
              WHERE f.userId='$userId' AND f.isDelete = 0 AND ic.is_delete = 0  AND (ic.circleType = 1 or ic.circleType = 3 or ic.circleType = 4 or ic.circleType=5) ";
        return $this->pdo->query($sql);
    }

    /**
     * 获取官方创建的圈子列表
     * @return mixed
     */
    public function getOfficialInfoCategoryList()
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT ic.id,ic.userId,u.nickname,u.photo as userPhoto,ic.avatar,ic.name,ic.pid,ic.num,ic.follow,ic.circleType,ic.create_by as createBy,ic.create_time as createTime,ic.update_by as updateBy,ic.update_time as updateTime FROM 
                $this->infoCategoryTable as ic 
                LEFT JOIN $this->userTable as u ON ic.userId=u.id 
              WHERE  ic.is_delete = 0  AND (ic.circleType = 1 or ic.circleType = 2 )";
        return $this->pdo->query($sql);
    }

    /**
     * 获取用户可以发布活动的圈子列表
     * @param $userId
     * @return mixed
     */
    public function getUserPublishActivityAndQuestionInfoCategoryList($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT f.id,f.userId,u.username,u.nickname,u.photo,f.categoryId,ic.avatar,ic.name,ic.num,ic.follow,ic.type,ic.circleType,f.isDelete,f.createTime,f.createBy,f.updateTime,f.updateBy FROM 
              $this->followTable as f 
              LEFT JOIN $this->infoCategoryTable as ic ON f.categoryId=ic.id
              LEFT JOIN $this->userTable as u ON f.userId=u.id 
              WHERE f.userId='$userId' AND f.isDelete = 0 AND ic.is_delete = 0  AND (ic.circleType = 1 or ic.circleType = 3 or ic.circleType = 4 or ic.circleType=2) ";
        return $this->pdo->query($sql);
    }

    /**
     * 删除资讯分类
     * @param $childs 分类id集合
     * @return mixed
     */
    public function delInformationCategoryById($childs)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->infoCategoryTable SET is_delete = 1 WHERE id IN($childs)";

        return $this->pdo->update($sql);
    }

    /**
     * 根据id编辑资讯分类
     * @param $categoryId
     * @param $avatar
     * @param $name
     * @param $userId
     * @return mixed
     */
    public function updateInformationCategoryById($categoryId, $avatar, $name, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->infoCategoryTable SET 
                avatar = '$avatar',
                name='$name',
                update_by='$userId',
                update_time=now() 
                WHERE id = '$categoryId' AND is_delete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 获取资讯分类详情 根据分类id
     * @param $categoryId
     * @return mixed
     */
    public function getInfoCategoryDetail($categoryId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,avatar,name,pid,num,follow,type,circleType,is_delete as isDelete,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime
                FROM  $this->infoCategoryTable WHERE is_delete = 0 AND id='$categoryId'";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 添加资讯
     * @param $userId
     * @param $categoryId
     * @param $photo
     * @param $title
     * @param $content
     * @param $isMd
     * @param $resourceData
     * @return mixed
     */
    public function insertInformation($userId, $categoryId, $photo, $title, $content, $subContent, $isMd, $resourceData)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->infoTable (userId,categoryId,photo,title,content,sub_content,is_md,create_by,create_time,update_by,update_time)
             VALUES ('$userId','$categoryId','$photo','$title','$content','$subContent','$isMd','$userId',now(),'$userId',now())";

            $infoId = $this->pdo->insert($sql);

            if ($infoId > 0) {
                //记录用户日志
                $function_name = __CLASS__.'->'.__FUNCTION__;
                $option_detail = '添加app资讯';
                $other_msg = [
                    'categoryId' => $categoryId,
                    'photo' => $photo,
                    'title' => $title,
                    'content' => $content,
                    'subContent' => $subContent,
                    'isMd' => $isMd,
                    'resourceData' => $resourceData,
                ];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $optionTableName = $this->infoTable;
                $tableId = $infoId;
                $accountDB = new CDBAccount();
                $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);
                if (!empty($resourceData)) {
                    $str = '';
                    foreach ($resourceData as $key => $resource) {
                        $remark = $resource['remark'];
                        $url = $resource['url'];
                        $type = $resource['type'];

                        $str .= "($userId,$infoId," . "'" . $remark . "'," . "'" . $url . "'," . $type . ",'" . Util::getCurrentTime() . "'," . $userId . ",'" . Util::getCurrentTime() . "'," . $userId . "),";

                    }
                    $str = rtrim($str, ',');
                    $sql = "INSERT INTO $this->sourceTable (userId,infoId,remark,url,type,createTime,createBy,updateTime,updateBy)
                VALUES $str";
                    $insertRow = $this->pdo->insert($sql);

                    if ($insertRow > 0) {
                        $this->pdo->commit();
                    } else {
                        $this->pdo->rollback();
                        return -1;
                    }
                } else {
                    $this->pdo->commit();
                }
            }

        } catch (\PDOException $e) {
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            $this->pdo->rollback();
            return -1;
        }
        return $infoId;
    }


    /**
     * 根据资讯id获取资讯详情
     * @param $informationId
     * @return mixed
     */
    public function getInformationById($informationId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,u.username,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.content,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId = u.id
                WHERE i.id='$informationId' 
                AND i.is_delete = 0";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 根据资讯id获取资讯详情 登录状态
     * @param $informationId
     * @param $userId
     * @return mixed
     */
    public function getInformationByIdWithUserId($informationId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,u.username,u.nickname,u.photo as userPhoto,li.isDelete as isLike ,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.content,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id
                LEFT JOIN $this->userTable as u ON i.userId = u.id
                LEFT JOIN $this->likeTable as li ON i.id=li.infoId AND li.userId='$userId' AND li.isDelete=0 
                WHERE i.id='$informationId' 
                AND i.is_delete = 0";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 检查用户是否可以在此分类下创建资讯 not used
     * @param $categoryId
     * @param $userId
     * @return bool
     */
    public function checkPermissionUserCanPublishCategory($categoryId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT type FROM $this->infoCategoryTable WHERE id='$categoryId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return false;
        } else {
            if ($this->isSystemUser($userId) || $result['type'] == 2) {
                return true;
            } else {
                return false;
            }
        }
    }


    /**
     * 检查用户是否是资讯的创建者
     * @param $informationId
     * @param $userId
     */
    public function isUserOfInformation($informationId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->infoTable WHERE id='$informationId' AND userId='$userId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 编辑资讯
     * @param $informationId
     * @param $userId
     * @param $categoryId
     * @param $photo
     * @param $title
     * @param $content
     * @param $isMd
     * @param $resourceData
     * @return mixed
     */
    public function updateInformation($informationId, $userId, $categoryId, $photo, $title, $content, $subContent, $isMd, $resourceData)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->infoTable SET categoryId='$categoryId',
                photo='$photo',title='$title',content='$content',
                sub_content='$subContent',is_md='$isMd',update_time=now(),update_by='$userId'
                WHERE id='$informationId' AND is_delete=0";
            $updateRow = $this->pdo->update($sql);
            if ($updateRow > 0) {
                //记录用户日志
                $function_name = __CLASS__.'->'.__FUNCTION__;
                $option_detail = '更新app资讯';
                $other_msg = [
                    'id' => $informationId,
                    'categoryId' => $categoryId,
                    'photo' => $photo,
                    'title' => $title,
                    'content' => $content,
                    'subContent' => $subContent,
                    'isMd' => $isMd,
                    'resourceData' => $resourceData,
                ];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $optionTableName = $this->infoTable;
                $tableId = $informationId;
                $accountDB = new CDBAccount();
                $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

                $sql = "UPDATE $this->sourceTable SET isDelete=1 WHERE infoId = '$informationId' AND isDelete=0";
                $this->pdo->update($sql);
                if (empty($resourceData)) {
                    $this->pdo->commit();
                } else {
                    $str = '';
                    foreach ($resourceData as $key => $resource) {
                        $remark = $resource['remark'];
                        $url = $resource['url'];
                        $type = $resource['type'];

                        $str .= "($userId,$informationId," . "'" . $remark . "'," . "'" . $url . "'," . $type . ",'" . Util::getCurrentTime() . "'," . $userId . ",'" . Util::getCurrentTime() . "'," . $userId . "),";

                    }
                    $str = rtrim($str, ',');
                    $sql = "INSERT INTO $this->sourceTable (userId,infoId,remark,url,type,createTime,createBy,updateTime,updateBy)
                VALUES $str";
                    $insertRow = $this->pdo->insert($sql);
                    $this->pdo->commit();
                }
            }
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }

        return $updateRow;

    }

    /**
     * 删除资讯
     * @param $informationId
     * @return mixed
     */
    public function delInformationById($informationId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->infoTable SET is_delete=1 WHERE id='$informationId'";
            $delRow = $this->pdo->update($sql);
            if ($delRow > 0) {
                $sql = "UPDATE $this->sourceTable SET isDelete=1 WHERE infoId='$informationId' AND isDelete=0";
                $delRow1 = $this->pdo->update($sql);
                $this->pdo->commit();
            } else {
                $this->pdo->rollback();
                return -1;
            }
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }

        return $delRow;

    }

    /**
     * 通过审核
     * @param $informationId
     * @param $categoryId
     * @return int
     */
    public function acceptForInformation($informationId, $categoryId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->infoTable SET isAccept=1 WHERE id='$informationId' AND is_delete=0";
            $updateRow = $this->pdo->update($sql);
            if ($updateRow) {
                $sql = "UPDATE $this->infoCategoryTable SET num = num+1 WHERE id='$categoryId' AND is_delete=0";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }
        return $updateRow;
    }

    /**
     * 资讯审核未通过
     * @param $informationId
     * @return mixed
     */
    public function notAcceptForInformation($informationId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->infoTable SET isAccept = -1 WHERE id='$informationId' AND is_delete=0";
        $updateRow = $this->pdo->update($sql);
        return $updateRow;
    }

    /**
     * 根据分类id获取资讯列表
     * @param $categoryId
     * @return mixed
     */
    public function getInformationListByCategoryId($categoryId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,ic.circleType,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId = u.id 
                WHERE i.categoryId='$categoryId' AND i.is_delete = 0 AND i.isAccept=1 ORDER BY i.id DESC";
        return $this->pdo->query($sql);
    }

    /**
     * 根据分类id获取资讯列表 有数量限制
     * @param $categoryId
     * @param $dynamicsNum
     * @return mixed
     */
    public function getInformationListByCategoryIdWithLimit($categoryId, $dynamicsNum)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,ic.circleType,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId = u.id 
                WHERE i.categoryId='$categoryId' AND i.is_delete = 0 AND i.isAccept=1 ORDER BY i.id DESC LIMIT $dynamicsNum";
        return $this->pdo->query($sql);
    }

    /**
     * 根据分类id分页获取资讯列表
     * @param $categoryId
     * @param $pageIndex
     * @param $pageSize
     * @return mixed
     */
    public function getInformationListByCategoryIdWithPage($userId, $categoryId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,li.isDelete as isLike,u.username,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,ic.circleType,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                LEFT JOIN $this->likeTable as li ON i.id = li.infoId AND li.userId = '$userId' AND li.isDelete=0
                WHERE i.categoryId='$categoryId' AND i.is_delete = 0 AND i.isAccept=1 
                ORDER BY i.id DESC 
                LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    public function getInformationListByCategoryIdWithMaxIdAndSinceId($userId, $categoryId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND i.id<'$maxId' AND i.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND i.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND i.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,li.isDelete as isLike,u.username,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,ic.circleType,i.photo,i.title,i.content,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                LEFT JOIN $this->likeTable as li ON i.id = li.infoId AND li.userId = '$userId' AND li.isDelete=0
                WHERE i.categoryId='$categoryId' AND i.is_delete = 0 AND i.isAccept=1  $where
                ORDER BY i.id DESC 
                LIMIT $count";
        return $this->pdo->query($sql);
    }

    public function getAllOfficialInformationListPagingNoLogin($maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND i.id<'$maxId' AND i.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND i.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND i.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT id FROM $this->infoCategoryTable  WHERE  is_delete = 0  AND (circleType = 1 or circleType = 2)";

        $arr = $this->pdo->query($sql);

        $str = Util::arrayToString(array_column($arr, 'id'));
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,i.categoryId,i.photo,i.title,i.content,i.sub_content as subContent,i.comment,i.share,i.like,i.isAccept,i.is_md as isMd,u.nickname,u.photo as userPhoto,u.nickname as author,u.photo as authorPhoto,i.create_time as createTime,i.create_by as createBy,i.update_time as updateTime,i.update_by as updateBy 
                FROM $this->infoTable as i  
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                WHERE i.categoryId in ($str) AND i.is_delete = 0 AND i.isAccept=1  $where
                ORDER BY i.id DESC 
                LIMIT $count";
        return $this->pdo->query($sql);
    }


    public function getCountInformationListByCategoryId($categoryId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id)
                FROM $this->infoTable  
                WHERE categoryId='$categoryId' AND is_delete = 0 AND isAccept=1 ";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }


    public function getCountAllOfficialInformation()
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->infoCategoryTable  WHERE  is_delete = 0  AND (circleType = 1 or circleType = 2)";

        $arr = $this->pdo->query($sql);

        $str = Util::arrayToString(array_column($arr, 'id'));

        $sql = "SELECT count(id)
                FROM $this->infoTable  
                WHERE categoryId in ($str) AND is_delete = 0 AND isAccept=1 ";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }
    /**
     * 管理员分页获取待审核的资讯列表
     * @param $categoryId
     * @param $pageIndex
     * @param $pageSize
     * @return mixed
     */
    public function getReviewInformationListByCategoryIdWithPageByAdmin($categoryId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT i.id,i.userId,u.username,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                WHERE i.categoryId='$categoryId' AND i.is_delete = 0 AND i.isAccept=0 
                ORDER BY i.id DESC 
                LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    /**
     * 获取用户在某分类下创建的资讯列表
     * @param $categoryId
     * @param $userId
     * @return mixed
     */
    public function getInformationListByCategoryIdAndUserId($categoryId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                WHERE i.categoryId='$categoryId' 
                AND i.is_delete = 0
                AND i.userId = '$userId' ORDER BY i.id DESC";
        return $this->pdo->query($sql);
    }

    /**
     * 分页获取用户在某分类下创建的资讯列表
     * @param $categoryId
     * @param $pageIndex
     * @param $pageSize
     * @param $userId
     * @return mixed
     */
    public function getInformationListByCategoryIdAndUserIdWithPage($categoryId, $pageIndex, $pageSize, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,li.isDelete as isLike,u.username,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                LEFT JOIN $this->likeTable as li ON i.id = li.infoId AND li.userId = '$userId' AND li.isDelete=0
                WHERE i.categoryId='$categoryId' 
                AND i.is_delete = 0
                AND i.userId = '$userId' ORDER BY i.id DESC LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    public function getInformationListByCategoryIdAndUserIdWithMaxIdAndSinceId($categoryId, $maxId, $sinceId, $count, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND i.id<'$maxId' AND i.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND i.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND i.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,li.isDelete as isLike,u.username,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                LEFT JOIN $this->likeTable as li ON i.id = li.infoId AND li.userId = '$userId' AND li.isDelete=0
                WHERE i.categoryId='$categoryId' 
                AND i.is_delete = 0
                AND i.userId = '$userId'  $where  ORDER BY i.id DESC LIMIT $count";
        return $this->pdo->query($sql);
    }

    public function getCountInformationListByCategoryIdAndUserId($categoryId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) 
                FROM $this->infoTable  
                WHERE categoryId='$categoryId' 
                AND is_delete = 0
                AND userId = '$userId'";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }


    /**
     * 获取用户创建的资讯列表
     * @param $userId
     * @return mixed
     */
    public function getInformationListByUserId($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                WHERE  i.is_delete = 0
                AND i.userId = '$userId' ORDER BY i.id DESC";
        return $this->pdo->query($sql);
    }

    /**
     * 分页获取用户创建的资讯列表
     * @param $userId
     * @param $pageIndex
     * @param $pageSize
     * @return mixed
     */
    public function getInformationListByUserIdWithPage($userId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,li.isDelete as isLike,u.username,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                LEFT JOIN $this->likeTable as li ON i.id = li.infoId AND li.userId = '$userId' AND li.isDelete=0
                WHERE  i.is_delete = 0
                AND i.userId = '$userId' ORDER BY i.id DESC LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    public function getInformationListByUserIdWithMaxIdAndSinceId($userId, $maxId, $sinceId, $count)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND i.id<'$maxId' AND i.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND i.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND i.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT i.id,i.type,i.isArticle,i.coorX,i.coorY,i.address,i.userId,li.isDelete as isLike,u.username,u.nickname,u.photo as userPhoto,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.sub_content as subContent,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  
                LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                LEFT JOIN $this->likeTable as li ON i.id = li.infoId AND li.userId = '$userId' AND li.isDelete=0
                WHERE  i.is_delete = 0
                AND i.userId = '$userId' $where  ORDER BY i.id DESC LIMIT $count";
        return $this->pdo->query($sql);
    }

    public function getCountInformationListByUserId($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) 
                FROM $this->infoTable   
                WHERE  is_delete = 0
                AND userId = '$userId'";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }


    /**
     * 添加用户关注分类
     * @param $userId
     * @param $categoryId
     * @return mixed
     */
    public function addUserFollowCategory($userId, $categoryId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->followTable (userId,categoryId,createTime,createBy,updateTime,updateBy)
                    VALUES ('$userId','$categoryId',now(),'$userId',now(),'$userId')";
            $followId = $this->pdo->insert($sql);
            if ($followId > 0) {
                $sql = "UPDATE $this->infoCategoryTable SET follow=follow+1 WHERE id='$categoryId' AND is_delete=0";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }
        return $followId;
    }

    /**
     * 取消用户关注分类
     * @param $userId
     * @param $categoryId
     * @return mixed
     */
    public function cancelUserFollowCategory($userId, $categoryId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->followTable SET isDelete = 1 WHERE userId='$userId' AND categoryId='$categoryId' AND isDelete=0";
            $delRow = $this->pdo->update($sql);
            if ($delRow > 0) {
                $sql = "UPDATE $this->infoCategoryTable SET follow=follow-1 WHERE id='$categoryId' AND is_delete=0";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }
        return $delRow;
    }

    /**
     * 检查用户是否已经关注过该分类了
     * @param $userId
     * @param $categoryId
     * @return bool
     */
    public function checkUserFollowExist($userId, $categoryId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->followTable WHERE userId = '$userId' AND categoryId = '$categoryId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取用户关注的分类列表，这个方法只用来提供给查询用
     * @param $userId 用户id
     * @return array 关注列表
     */
    public function getUserFollowCategoryForQuery($userId)
    {
        $this->init();

        $sql = "SELECT f.id,f.userId,f.categoryId,ic.avatar,ic.name,ic.num,ic.follow,ic.type,ic.circleType,f.createTime,f.createBy,f.updateTime,f.updateBy FROM 
              $this->followTable as f 
              LEFT JOIN $this->infoCategoryTable as ic ON f.categoryId=ic.id
              WHERE f.userId='$userId' AND f.isDelete = 0 AND ic.is_delete = 0 ";
        return $this->pdo->query($sql);
    }

    /**
     * 获取用户关注的分类列表
     * @param $userId
     * @return mixed
     */
    public function getUserFollowCategoryList($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT f.id,f.userId,u.username,u.nickname,u.photo,f.categoryId,ic.avatar,ic.name,ic.num,ic.follow,ic.type,ic.circleType,f.isDelete,f.createTime,f.createBy,f.updateTime,f.updateBy FROM 
              $this->followTable as f 
              LEFT JOIN $this->infoCategoryTable as ic ON f.categoryId=ic.id
              LEFT JOIN $this->userTable as u ON f.userId=u.id 
              WHERE f.userId='$userId' AND f.isDelete = 0 AND ic.is_delete = 0 ";
        return $this->pdo->query($sql);
    }

    /**
     * 添加资讯评论
     * @param $userId
     * @param $infoId
     * @param $replyToCommentId
     * @param $replyToUserId
     * @param $comment
     * @return int
     */
    public function addComment($userId, $infoId, $replyToCommentId, $replyToUserId, $comment)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->commentTable (userId,infoId,replyToCommentId,replyToUserId,comment,createTime,createBy,updateTime,updateBy)
          VALUES ('$userId','$infoId','$replyToCommentId','$replyToUserId','$comment',now(),'$userId',now(),'$userId')";
            $commentId = $this->pdo->insert($sql);
            if ($commentId) {
                $sql = "UPDATE $this->infoTable SET comment=comment+1 WHERE id='$infoId' AND is_delete=0";
                $this->pdo->update($sql);
                //记录用户日志
                $function_name = __CLASS__.'->'.__FUNCTION__;
                $option_detail = '添加app资讯评论';
                $other_msg = [
                    'infoId' => $infoId,
                    'replyToCommentId' => $replyToCommentId,
                    'replyToUserId' => $replyToUserId,
                    'comment' => $comment
                ];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $optionTableName = $this->commentTable;
                $tableId = $commentId;
                $accountDB = new CDBAccount();
                $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }

        return $commentId;
    }

    /**
     * 获取某个资讯下的所有评论
     * @param $infoId
     * @return mixed
     */
    public function getInfoCommentListByInfoId($infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT c.id,c.userId,u.username,u.nickname,u.photo,c.infoId,c.replyToCommentId,c.replyToUserId,
            ru.username as replyToUsername,ru.nickname as replyToNickname,ru.photo as replyToPhoto,c.comment,c.isDelete,
            c.createBy,c.createTime,c.updateBy,c.updateTime
            FROM $this->commentTable as c  LEFT JOIN $this->userTable as u ON c.userId = u.id 
            LEFT JOIN $this->userTable as ru ON c.replyToUserId = ru.id WHERE c.infoId = '$infoId' AND c.isDelete=0;";
        return $this->pdo->query($sql);
    }


    /**
     * 获取资讯下的一级评论
     * @param $infoId
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @return mixed
     */
    public function getInfoCommentPagingByInfoId($infoId, $maxId, $sinceId, $count)
    {
        if ($maxId > 0 && $sinceId > 0) {
            $where = "  AND c.id<'$maxId' AND c.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where = "  AND c.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where = "  AND c.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where = '';
        }

        $sql = "SELECT c.id,c.userId,u.username,u.nickname,u.photo,c.infoId,c.replyToCommentId,c.replyToUserId,
            ru.username as replyToUsername,ru.nickname as replyToNickname,ru.photo as replyToPhoto,c.comment,c.isDelete,
            c.createBy,c.createTime,c.updateBy,c.updateTime
            FROM $this->commentTable as c  LEFT JOIN $this->userTable as u ON c.userId = u.id 
            LEFT JOIN $this->userTable as ru ON c.replyToUserId = ru.id WHERE c.infoId = '$infoId' AND c.replyToCommentId = '-1' AND c.isDelete=0 " . $where . "  ORDER BY c.id DESC LIMIT $count;";
        $result = $this->pdo->query($sql);

        return $result;
    }


    /**
     * 获取资讯评论的总数
     * @param $infoId
     * @return mixed
     */
    public function getCountInfoCommentByInfoId($infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') FROM $this->commentTable WHERE infoId='$infoId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        return $result["count('id')"];
    }

    /**
     * 获取某个评论下的所有回复评论
     * @param $commentId
     * @return mixed
     */
    public function getReplyCommentListByCommentId($commentId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT c.id,c.userId,u.username,u.nickname,u.photo,c.infoId,c.replyToCommentId,c.replyToUserId,
            ru.username as replyToUsername,ru.nickname as replyToNickname,ru.photo as replyToPhoto,c.comment,c.isDelete,
            c.createBy,c.createTime,c.updateBy,c.updateTime
            FROM $this->commentTable as c  LEFT JOIN $this->userTable as u ON c.userId = u.id 
            LEFT JOIN $this->userTable as ru ON c.replyToUserId = ru.id WHERE c.replyToCommentId = '$commentId' AND c.isDelete=0;";
        return $this->pdo->query($sql);
    }

    /**
     * 获取评论详情
     * @param $commentId
     * @return mixed
     */
    public function getCommentDetailById($commentId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,userId,infoId,replyToCommentId,replyToUserId,comment FROM $this->commentTable WHERE id='$commentId' AND isDelete=0";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 删除评论
     * @param $commentId
     * @return mixed
     */
    public function delComment($infoId, $commentId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->commentTable SET isDelete=1 WHERE id = '$commentId' AND isDelete=0";
            $delRow = $this->pdo->update($sql);
            if ($delRow) {
                $sql = "UPDATE $this->infoTable SET comment=comment-1 WHERE id='$infoId' AND is_delete=0";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }

        return $delRow;
    }

    /**
     * 添加资讯点赞
     * @param $userId
     * @param $infoId
     * @return int
     */
    public function addInfoLike($userId, $infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->likeTable (userId,infoId,createTime,createBy,updateTime,updateBy)
            VALUES ('$userId','$infoId',now(),'$userId',now(),'$userId')";
            $likeId = $this->pdo->insert($sql);
            if ($likeId) {
                $sql = "UPDATE $this->infoTable SET `like` = `like`+1 WHERE id='$infoId' AND is_delete=0";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }
        return $likeId;
    }

    /**
     * 取消用户资讯点赞
     * @param $userId
     * @param $infoId
     * @return int
     */
    public function cancelUserInfoLike($userId, $infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->likeTable SET isDelete = 1 WHERE userId = '$userId' AND infoId = '$infoId' AND isDelete=0";
            $delRow = $this->pdo->update($sql);
            if ($delRow) {
                $sql = "UPDATE $this->infoTable SET `like` = `like`-1 WHERE id='$infoId' AND is_delete=0";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }
        return $delRow;
    }

    /**
     * 获取用户收藏详情
     * @param $likeId
     * @return mixed
     */
    public function getUserInfoLikeDetail($likeId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT userId,infoId FROM $this->likeTable WHERE id='$likeId' AND isDelete=0";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取资讯的点赞列表
     * @param int $infoId
     * @return array 点赞列表
     */
    public function getInfoLikeList(int $infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT lt.userId, ut.photo, ut.nickname FROM $this->likeTable lt 
                LEFT JOIN $this->userTable ut on ut.id = lt.userId 
                WHERE lt.infoId = '$infoId' AND lt.isDelete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 获取资讯的分享列表
     * @param int $infoId 资讯id
     * @return array 分享列表
     */
    public function getInfoShareList(int $infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT st.userId, ut.photo, ut.nickname FROM $this->shareTable st 
                LEFT JOIN $this->userTable ut on ut.id = st.userId 
                WHERE st.infoId = '$infoId' AND st.isDelete = '0' ";

        return $this->pdo->query($sql);
    }

    /**
     * 检查用户是否已经点过赞了
     * @param $userId
     * @param $infoId
     * @return bool
     */
    public function checkUserInfoLikeExist($userId, $infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->likeTable WHERE userId='$userId' AND infoId='$infoId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取用户的点赞列表
     * @param $userId
     * @return mixed
     */
    public function getUserLikeList($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT l.id,l.userId,u.username,u.nickname,u.photo,l.infoId,i.categoryId,i.photo,i.title,i.content,i.sub_content as subContent,i.comment,i.share,i.like,
            i.is_md as isMd,l.createTime,l.createBy,l.updateTime,l.updateBy FROM $this->likeTable as l
            INNER JOIN $this->infoTable as i ON l.infoId=i.id
            LEFT JOIN $this->userTable as u ON l.userId=u.id
            WHERE l.userId='$userId' AND i.is_delete=0 AND l.isDelete=0";
        return $this->pdo->query($sql);
    }

    /**
     * 添加资讯分享
     * @param $userId
     * @param $infoId
     * @return int
     */
    public function addInfoShare($userId, $infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->shareTable (userId,infoId,createTime,createBy,updateTime,updateBy)
            VALUES ('$userId','$infoId',now(),'$userId',now(),'$userId')";
            $shareId = $this->pdo->insert($sql);
            if ($shareId) {
                $sql = "UPDATE $this->infoTable SET `share` = `share`+1 WHERE id='$infoId' AND is_delete=0";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }
        return $shareId;
    }

    /**
     * 检查用户是否已经分享过了了
     * @param $userId
     * @param $infoId
     * @return bool
     */
    public function checkUserInfoShareExist($userId, $infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->shareTable WHERE userId='$userId' AND infoId='$infoId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取用户的分享列表
     * @param $userId
     * @return mixed
     */
    public function getUserShareList($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT l.id,l.userId,u.username,u.nickname,u.photo,l.infoId,i.categoryId,i.photo,i.title,i.content,i.comment,i.share,i.like,
            i.is_md as isMd,l.isDelete,l.createTime,l.createBy,l.updateTime,l.updateBy FROM $this->shareTable as l
            LEFT JOIN $this->infoTable as i ON l.infoId=i.id
            LEFT JOIN $this->userTable as u ON l.userId=u.id 
            WHERE l.userId='$userId' AND i.is_delete=0";
        return $this->pdo->query($sql);
    }

    /**
     * 检查用户是否有资讯的权限
     * @param $infoId
     * @param $userId
     * @return bool
     */
    public function checkInformationPermissionWithUserId($infoId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->infoTable WHERE id='$infoId' AND userId = '$userId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 给资讯添加资源
     * @param $userId
     * @param $infoId
     * @param $remark
     * @param $url
     * @param $type
     */
    public function addInformationResource($userId, $infoId, $remark, $url, $type)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->sourceTable (userId,infoId,remark,url,type,createTime,createBy,updateTime,updateBy)
        VALUES ('$userId','$infoId','$remark','$url','$type',now(),'$userId',now(),'$userId')";
        return $this->pdo->insert($sql);
    }

    /**
     * 获取某个资讯的资源列表
     * @param $infoId
     * @return mixed
     */
    public function getResourceListByInfoId($infoId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,userId,infoId,remark,url,type,isDelete,createTime,createBy,updateTime,updateBy FROM $this->sourceTable
              WHERE infoId = '$infoId' AND isDelete=0";
        return $this->pdo->query($sql);
    }

    /**
     * 根据资源id获取资讯id
     */
    public function getInfoIdByResourceId($resourceId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT infoId FROM $this->sourceTable WHERE id='$resourceId' AND isDelete=0";
        return $this->pdo->uniqueResult($sql);

    }

    /**
     * 根据资源id编辑资源
     * @param $resourceId
     * @param $remark
     * @param $url
     * @param $type
     * @param $userId
     * @return mixed
     */
    public function updateResourceById($resourceId, $remark, $url, $type, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->sourceTable SET 
                remark='$remark',
                url='$url',
                type='$type',
                updateBy='$userId',updateTime=now() WHERE id='$resourceId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 获取一个分类下的所有关注用户
     * @param $categoryId 分类id
     * @return array 用户数组
     */
    public function getCategoryFollowers(int $categoryId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT userId FROM $this->followTable WHERE categoryId = '$categoryId' AND isDelete = '0' ";
        return $this->pdo->query($sql);
    }

    /**
     * 根据资讯id数组获取资讯
     * @param $infoIds 资讯id数组
     * @return array 资讯数组
     */
    public function getInfoByInfoIds(array $infoIds)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $infoIdStr = Util::arrayToString($infoIds);

        $sql = "SELECT i.id,i.userId,u.username,u.nickname,u.photo,i.categoryId,ic.avatar,ic.name as categoryName,i.photo,i.title,i.content,i.`comment`,i.`share`,i.`like`,i.isAccept,i.is_md as isMd,i.is_delete as isDelete,
                i.create_by as createBy,i.create_time as createTime,i.update_by as updateBy,i.update_time as updateTime 
                FROM $this->infoTable as i  LEFT JOIN $this->infoCategoryTable as ic ON i.categoryId=ic.id 
                LEFT JOIN $this->userTable as u ON i.userId=u.id
                WHERE i.id in ($infoIdStr) AND i.is_delete = 0 AND i.isAccept='1' ";

        return $this->pdo->query($sql);
    }

    /**
     * 根据圈子id获取家族id
     * @param $socialCircleId
     * @return mixed
     */
    public function getFamilyIdBySocialCircleId($socialCircleId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->TABLE_FAMILY WHERE socialCircleId='$socialCircleId' AND is_delete=0";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 发表动态
     * @param $type
     * @param $coorX
     * @param $coorY
     * @param $address
     * @param $userId
     * @param $categoryId
     * @param $photo
     * @param $content
     * @return int
     */
    public function addPost($type, $coorX, $coorY, $address, $userId, $categoryId, $photo, $content)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->infoTable (type,isArticle,coorX,coorY,address,userId,categoryId,photo,content,create_time,create_by,update_time,update_by)
            VALUES('$type',0,'$coorX','$coorY','$address','$userId','$categoryId','$photo','$content',now(),'$userId',now(),'$userId')";

            $postId = $this->pdo->insert($sql);

            if ($postId > 0) {
                //记录用户日志
                $function_name = __CLASS__.'->'.__FUNCTION__;
                $option_detail = '添加app推文';
                $other_msg = [
                    'type' => $type,
                    'coorX' => $coorX,
                    'coorY' => $coorY,
                    'address' => $address,
                    'categoryId' => $categoryId,
                    'address' => $address,
                    'photo' => $photo,
                    'content' => $content,
                ];
                $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                $optionTableName = $this->infoTable;
                $tableId = $postId;
                $accountDB = new CDBAccount();
                $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

                $sql = "UPDATE $this->TABLE_USER_PROFILE SET info_post_num = info_post_num + 1 WHERE userId = '$userId' ";
                $this->pdo->update($sql);
                //添加推文的同时要更新圈子的推文数量
                $sql = "UPDATE $this->infoCategoryTable SET num = num + 1 WHERE id = '$categoryId' ";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        $redisPost = new CDBInformationPost();

        $pushUserIds = $this->getCategoryFollowers($categoryId);

        //设置推文的推送任务
        $taskPushResult = $redisPost->setPostTask($pushUserIds, $postId, Util::getTotalMicroTime(), $userId);

        return $postId;
    }

    /**
     * 添加系统用户动态
     * @param $type
     * @param $coorX
     * @param $coorY
     * @param $address
     * @param $userId
     * @param $categoryId
     * @param $photo
     * @param $content
     * @param array $userIds
     * @return int
     */
    public function addSystemPost($type, $coorX, $coorY, $address, $userId, $categoryId, $photo, $content, $userIds = array())
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->infoTable (type,isArticle,coorX,coorY,address,userId,categoryId,photo,content,create_time,create_by,update_time,update_by)
            VALUES('$type',0,'$coorX','$coorY','$address','$userId','$categoryId','$photo','$content',now(),'$userId',now(),'$userId')";

            $postId = $this->pdo->insert($sql);

            if ($postId > 0) {
                 //记录用户日志
                 $function_name = __CLASS__.'->'.__FUNCTION__;
                 $option_detail = '添加app系统推文';
                 $other_msg = [
                     'type' => $type,
                     'coorX' => $coorX,
                     'coorY' => $coorY,
                     'address' => $address,
                     'categoryId' => $categoryId,
                     'address' => $address,
                     'photo' => $photo,
                     'content' => $content,
                     'userIds' => $userIds
                 ];
                 $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                 $optionTableName = $this->infoTable;
                $tableId = $postId;
                $accountDB = new CDBAccount();
                $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

                $sql = "UPDATE $this->TABLE_USER_PROFILE SET info_post_num = info_post_num + 1 WHERE userId = '$userId' ";
                $this->pdo->update($sql);
                //添加推文的同时要更新圈子的推文数量
                $sql = "UPDATE $this->infoCategoryTable SET num = num + 1 WHERE id = '$categoryId' ";
                $this->pdo->update($sql);
            }
            $this->pdo->commit();
            //@codeCoverageIgnoreStart
        } catch (PDOException $e) {
            $this->pdo->rollback();
            return -1;
            //@codeCoverageIgnoreEnd
        }
        $redisPost = new CDBInformationPost();
        $pushUserIds = $userIds;

        if (count($pushUserIds) == 0) {
            //获取全系统的用户id
            $accountDB = new CDBAccount();
            $pushUserIds = $accountDB->getAllSystemUsers();
        }
        //设置推文的推送任务
        $taskPushResult = $redisPost->setPostTask($pushUserIds, $postId, Util::getTotalMicroTime(), $userId);

        return $postId;
    }

    public function addPostArticle($type, $userId, $categoryId, $photo, $title, $content, $subContent, $isMd, $resourceData)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->infoTable (type,isArticle,userId,categoryId,photo,title,content,sub_content,is_md,create_by,create_time,update_by,update_time)
             VALUES ('$type',1,'$userId','$categoryId','$photo','$title','$content','$subContent','$isMd','$userId',now(),'$userId',now())";
            $infoId = $this->pdo->insert($sql);

            if ($infoId > 0) {
                 //记录用户日志
                 $function_name = __CLASS__.'->'.__FUNCTION__;
                 $option_detail = '添加app推文article';
                 $other_msg = [
                     'type' => $type,
                     'categoryId' => $categoryId,
                     'photo' => $photo,
                     'title' => $title,
                     'content' => $content,
                     'subContent' => $subContent,
                     'isMd' => $isMd,
                     'resourceData' => $resourceData
                 ];
                 $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                 $optionTableName = $this->infoTable;
                 $tableId = $infoId;
                 $accountDB = new CDBAccount();
                 $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

                $sql = "UPDATE $this->TABLE_USER_PROFILE SET info_post_num = info_post_num + 1 WHERE userId = '$userId' ";
                $this->pdo->update($sql);
                //添加推文的同时要更新圈子的推文数量
                $sql = "UPDATE $this->infoCategoryTable SET num = num + 1 WHERE id = '$categoryId' ";
                $this->pdo->update($sql);

                if (!empty($resourceData)) {
                    $str = '';
                    foreach ($resourceData as $key => $resource) {
                        $remark = $resource['remark'];
                        $url = $resource['url'];
                        $type = $resource['type'];

                        $str .= "($userId,$infoId," . "'" . $remark . "'," . "'" . $url . "'," . $type . ",'" . Util::getCurrentTime() . "'," . $userId . ",'" . Util::getCurrentTime() . "'," . $userId . "),";

                    }
                    $str = rtrim($str, ',');
                    $sql = "INSERT INTO $this->sourceTable (userId,infoId,remark,url,type,createTime,createBy,updateTime,updateBy)
                VALUES $str";
                    $insertRow = $this->pdo->insert($sql);

                    if ($insertRow > 0) {
                        $this->pdo->commit();
                    } else {
                        $this->pdo->rollback();
                        return -1;
                    }
                } else {
                    $this->pdo->commit();
                }
            }

        } catch (\PDOException $e) {
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            $this->pdo->rollback();
            return -1;
        }

        $redisPost = new CDBInformationPost();

        $pushUserIds = $this->getCategoryFollowers($categoryId);

        //设置推文的推送任务
        $taskPushResult = $redisPost->setPostTask($pushUserIds, $infoId, Util::getTotalMicroTime(), $userId);

        return $infoId;
    }

    public function addSystemPostArticle($type, $userId, $categoryId, $photo, $title, $content, $subContent, $isMd, $resourceData, $userIds = array())
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->pdo->beginTransaction();
            $sql = "INSERT INTO $this->infoTable (type,isArticle,userId,categoryId,photo,title,content,sub_content,is_md,create_by,create_time,update_by,update_time)
             VALUES ('$type',1,'$userId','$categoryId','$photo','$title','$content','$subContent','$isMd','$userId',now(),'$userId',now())";
            $infoId = $this->pdo->insert($sql);

            if ($infoId > 0) {
                 //记录用户日志
                 $function_name = __CLASS__.'->'.__FUNCTION__;
                 $option_detail = '添加app系统推文article';
                 $other_msg = [
                     'type' => $type,
                     'categoryId' => $categoryId,
                     'photo' => $photo,
                     'title' => $title,
                     'content' => $content,
                     'subContent' => $subContent,
                     'isMd' => $isMd,
                     'resourceData' => $resourceData,
                     'userIds' => $userIds
                 ];
                 $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
                 $optionTableName = $this->infoTable;
                 $tableId = $infoId;
                 $accountDB = new CDBAccount();
                 $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

                $sql = "UPDATE $this->TABLE_USER_PROFILE SET info_post_num = info_post_num + 1 WHERE userId = '$userId' ";
                $this->pdo->update($sql);
                //添加推文的同时要更新圈子的推文数量
                $sql = "UPDATE $this->infoCategoryTable SET num = num + 1 WHERE id = '$categoryId' ";
                $this->pdo->update($sql);

                if (!empty($resourceData)) {
                    $str = '';
                    foreach ($resourceData as $key => $resource) {
                        $remark = $resource['remark'];
                        $url = $resource['url'];
                        $type = $resource['type'];

                        $str .= "($userId,$infoId," . "'" . $remark . "'," . "'" . $url . "'," . $type . ",'" . Util::getCurrentTime() . "'," . $userId . ",'" . Util::getCurrentTime() . "'," . $userId . "),";

                    }
                    $str = rtrim($str, ',');
                    $sql = "INSERT INTO $this->sourceTable (userId,infoId,remark,url,type,createTime,createBy,updateTime,updateBy)
                VALUES $str";
                    $insertRow = $this->pdo->insert($sql);

                    if ($insertRow > 0) {
                        $this->pdo->commit();
                    } else {
                        $this->pdo->rollback();
                        return -1;
                    }
                } else {
                    $this->pdo->commit();
                }
            }

        } catch (\PDOException $e) {
            SysLogger::getInstance()->error(Util::exceptionFormat($e));
            $this->pdo->rollback();
            return -1;
        }

        $redisPost = new CDBInformationPost();
        $pushUserIds = $userIds;

        if (count($pushUserIds) == 0) {
            //获取全系统的用户id
            $accountDB = new CDBAccount();
            $pushUserIds = $accountDB->getAllSystemUsers();
        }
        //设置推文的推送任务
        $taskPushResult = $redisPost->setPostTask($pushUserIds, $infoId, Util::getTotalMicroTime(), $userId);

        return $infoId;
    }


    /**
     * 检查用户是否有在圈子发布动态或者文章的权限
     * @param $socialCircleId
     * @param $userId
     * @return bool
     */
    public function checkUserHasPermissionToPublishPostInCircle($socialCircleId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count('id') FROM $this->followTable WHERE userId='$userId' AND categoryId='$socialCircleId' AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);
        if ($result["count('id')"] > 0) {

            $categoryDetail = $this->getInfoCategoryDetail($socialCircleId);

            if ($categoryDetail == null) {
                return false;
            }

            if ($categoryDetail['circleType'] == 2 && !$this->isSystemUser($userId)) {
                return false;
            }

            return true;

        } else {
            return false;
        }
    }

    public function getPostsPagingTotal($userId)
    {
        $infoPostDB = new  CDBInformationPost();
        return $infoPostDB->getTimelinePostCount($userId);
    }

    public function getPostsPagingByMaxIdAndSinceId($userId, $maxId, $sinceId, $count, $maxTimeStamp = 0, $sinceTimeStamp = 0)
    {

        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $redisPost = new CDBInformationPost();
        list($timeline, $endIndex) = $redisPost->getTimelineBySinceIdAndMaxId($userId, $maxId, $sinceId, $count, $maxTimeStamp, $sinceTimeStamp);

        $isFirst = true;
        $len = count($timeline);

        $postIds = array_keys($timeline);
        $postIdsStr = Util::arrayToString($postIds);

        $result = [];
        if ($len > 0) {

            $sql = "SELECT pt.id,pt.type,pt.isArticle,pt.coorX,pt.coorY,pt.address,pt.userId,pt.categoryId,pt.photo,pt.title,pt.content,pt.sub_content as subContent,pt.comment,pt.share,pt.like,pt.isAccept,pt.is_md as isMd,ut.nickname,ut.photo as userPhoto,ut.nickname as author,ut.photo as authorPhoto,pt.content,pt.create_time as createTime,pt.create_by as createBy,pt.update_time as updateTime,pt.update_by as updateBy 
                from $this->infoTable pt
                left join $this->userTable ut on pt.create_by = ut.id 
                WHERE pt.id in ($postIdsStr) AND pt.is_delete = '0' ORDER BY pt.id DESC";

            $result = $this->pdo->query($sql);
        }

        return [$timeline, $result, $endIndex];
    }

    /**
     * 删除资讯
     * @param $postId
     * @param $categoryId
     * @param $userId
     * @param $userType
     * @return mixed
     */
    public function delInformationByIdInCircle($postId, $categoryId, $userId, $userType)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        try {
            $this->pdo->beginTransaction();
            $sql = "UPDATE $this->infoTable SET is_delete=1 WHERE id='$postId'";
            $delRow = $this->pdo->update($sql);
            if ($delRow > 0) {
                $sql = "UPDATE $this->sourceTable SET isDelete=1 WHERE infoId='$postId' AND isDelete=0";
                $delRow1 = $this->pdo->update($sql);
                $sql = "UPDATE $this->TABLE_USER_PROFILE SET info_post_num = info_post_num - 1 WHERE userId = '$userId' ";
                $this->pdo->update($sql);
                //添加推文的同时要更新圈子的推文数量
                $sql = "UPDATE $this->infoCategoryTable SET num = num - 1 WHERE id = '$categoryId' ";
                $this->pdo->update($sql);
                $this->pdo->commit();
            } else {
                $this->pdo->rollback();
                return -1;
            }
        } catch (\PDOException $e) {
            $this->pdo->rollback();
            return -1;
        }

        if ($userType == 1) {
            $pushUserIds = $this->getCategoryFollowers($categoryId);
        }

        if ($userType == 2) {
            //获取全系统的用户id
            $accountDB = new CDBAccount();
            $pushUserIds = $accountDB->getAllSystemUsers();
        }
        $redisPost = new CDBInformationPost();
        //设置推文的推送任务
        $taskPushResult = $redisPost->setDeletePostTask($pushUserIds, $postId, Util::getTotalMicroTime(), $userId);

        return $delRow;

    }
}
