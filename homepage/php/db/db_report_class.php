<?php
/**
 * 举报模块
 * author: jiangpengfei
 * date: 2018-02-07
 */

namespace DB;

use DB\CDBManager;
use Util\Util;

class CDBReport
{
    public $pdo = null;
    public $TABLE = "gener_report";

    /**
     * @codeCoverageIgnore
     */
    public function initial()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 添加举报
     * @param $moduleId 模块id
     * @param $recordId 举报的记录id
     * @param $reportType 举报的类型
     * @param $userId   用户id
     * @return int 举报id
     */
    public function addReport($moduleId, $recordId, $reportType, $userId)
    {
        if (!$this->initial()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO $this->TABLE
                    (userId,moduleId,recordId,report_type,create_time,create_by,update_time,update_by)
                    VALUES
                    ('$userId','$moduleId','$recordId','$reportType',now(),'$userId',now(),'$userId') ";

        return $this->pdo->insert($sql);
    }

    /**
     * 检查用户举报是否存在
     * @param $moduleId 模块id
     * @param $recordId 记录id
     * @param $reportType
     * @param $userId   用户id
     * @return mix int代表存在的记录id，false代表不存在
     * @internal param 记录类型 $recordType
     */
    public function checkUserReportExist($moduleId, $recordId, $reportType, $userId)
    {
        if (!$this->initial()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE userId = '$userId' AND moduleId = '$moduleId' AND recordId = '$recordId' AND report_type = '$reportType' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result['id'];
        } else {
            return false;
        }
    }


    /**
     * 获取举报总数
     * @param 模块id|int $moduleId 模块id,-1代表不作为条件
     * @param int $recordId
     * @param 举报类型，|int $reportType 举报类型，-1代表不作为条件
     * @return int 举报的数量
     */
    public function getReportsTotal($moduleId = -1, $recordId = -1, $reportType = -1)
    {
        if (!$this->initial()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $where = '';

        if ($moduleId != -1) {
            $where .= " moduleId = '$moduleId' AND ";
        }

        if ($recordId != -1) {
            $where .= " recordId = '$recordId' AND ";
        }

        if ($reportType != -1) {
            $where .= " report_type = '$reportType' AND ";
        }

        $sql = "SELECT count(id) FROM $this->TABLE WHERE $where is_delete = '0'";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取一页举报记录
     * @param $pageIndex 页码
     * @param $pageSize  页大小
     * @param $moduleId    模块id
     * @param $recordId    举报记录id
     * @param $reportType  举报类型
     * @return array 一页的举报记录
     */
    public function getReportsPaging($pageIndex, $pageSize, $moduleId = -1, $recordId = -1, $reportType = -1)
    {
        if (!$this->initial()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }
        $offset = ($pageIndex - 1) * $pageSize;

        $where = '';

        if ($moduleId != -1) {
            $where .= " moduleId = '$moduleId' AND ";
        }

        if ($recordId != -1) {
            $where .= " recordId = '$recordId' AND ";
        }

        if ($reportType != -1) {
            $where .= " report_type = '$reportType' AND ";
        }

        $sql = "SELECT id,userId,moduleId,recordId,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
                    FROM $this->TABLE WHERE $where  is_delete = '0' order by id desc limit $offset,$pageSize";

        return $this->pdo->query($sql);
    }

}