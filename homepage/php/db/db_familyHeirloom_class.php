<?php
/**
 * 传家宝模块
 * author:jiangpengfei
 * date:2017-04-21
 */

namespace DB;

use DB\CDBManager;
use DB\CDBPerson;
use DB\CDBFamily;
use Model\FamilyHeirloom;
use Util\Util;

class CDBFamilyHeirloom
{
    var $pdo = null;
    var $TABLE = "gener_family_heirloom";

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {

        if (!$this->pdo)
        {
            if (!isset($GLOBALS['pdo']))
            {
                    $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                    if ($this->pdo)
                            $GLOBALS['pdo'] = $this->pdo;
            }
            else
            {
                    $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }


    /**
    * 检查用户id和传家宝id的权限是否匹配
    * @param $userId 用户Id
    * @param $heirloomId 事件id
    * @return bool true是有权限，false为没有权限
    */
    public function verifyUserIdAndHeirloomId($userId,$heirloomId){
        if(!$this->init()){
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT familyId FROM $this->TABLE WHERE id = '$heirloomId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if($result == null){
            return false;
        }
        $familyId = $result['familyId'];
        $familyDB = new CDBFamily();
        return $familyDB->isUserForFamily($familyId, $userId);
    }

    /**
    * 添加传家宝
    * @param $familyId 家族Id
    * @param $title    传家宝的标题
    * @param $content  传家宝的内容
    * @param $photo    传家宝的照片
    * @param $userId   用户id
    * @return int 添加的记录id
    */
    public function addHeirloom($familyId,$title,$content,$photo,$userId){
        if(!$this->init()){
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE(familyId,title,content,photo,create_by,create_time,update_by,update_time)VALUES('$familyId','$title','$content','$photo','$userId',now(),'$userId',now())";
        return $this->pdo->insert($sql);
    }

    /**
    * 更新传家宝内容
    * @param $heirloomId 传家宝Id
    * @param $title    传家宝的标题
    * @param $content  传家宝的内容
    * @param $photo    传家宝的照片
    * @param $userId   用户id
    * @return int 更新的记录数
    */
    public function updateHeirloom($heirloomId,$title,$content,$photo,$userId){
        if(!$this->init()){
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET title = '$title',content = '$content',photo = '$photo',update_time = now(),update_by = '$userId' WHERE id = '$heirloomId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
    * 删除传家宝记录
    * @param $heirloomId 传家宝id
    * @return int 删除的记录数
    */
    public function deleteHeirloom($heirloomId){
        if(!$this->init()){
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET isDelete=1 WHERE id = '$heirloomId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
    * 获取传家宝详情
    * @param $heirloomId 传家宝id
    * @return object 传说的对象
    */
    public function getHeirloom($heirloomId){
        if(!$this->init()){
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,familyId,title,content,photo,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
                FROM $this->TABLE WHERE id = '$heirloomId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if($result != null){
            return new FamilyHeirloom($result);
        }else{
            return null;
        }
    }

    /**
    * 获取传家宝的总数
    * @param $familyId 家族id
    * @return int 获取传家宝的总数
    */
    public function getHeirloomsTotal($familyId){
        if(!$this->init()){
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE WHERE familyId = '$familyId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if($result != null){
            return $result['count(id)'];
        }else{
            // @codeCoverageIgnoreStart
            return 0;
            // @codeCoverageIgnoreEnd
        }
    }

    /**
    * 获取一页传家宝的数据
    * @param $familyId 家族id
    * @param $pageIndex 页码
    * @param $pageSize  每页记录数
    * @return array 一页的内容数组
    */
    public function getHeirloomsPaging($familyId,$pageIndex,$pageSize){
        if(!$this->init()){
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'],null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT id,familyId,title,content,photo,create_time as createTime,create_by as createBy,update_time as updateTime,update_by as updateBy 
                FROM $this->TABLE WHERE familyId = '$familyId' AND isDelete=0 ORDER BY id DESC LIMIT $offset,$pageSize ";
        return $this->pdo->query($sql);
    }
};
