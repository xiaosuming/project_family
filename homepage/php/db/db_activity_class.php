<?php
/**
 * 家族活动模块
 * author: jiangpengfei
 * date: 2017-03-29
 */

namespace DB;

use Model\Activity;
use Util\Util;
use DB\CDBAccount;

class CDBActivity
{
    public $pdo = null;
    public $TABLE = "gener_activity";
    public $TABLE_PERSON = "gener_person";
    public $TABLE_USER = "gener_user";
    public $TABLE_FAMILY = "gener_family";
    public $TABLE_ACTIVITY_PARTICIPATOR = "gener_activity_participator";
    public $TABLE_ACTIVITY_COMMENT = "gener_activityComment";
    public $TABLE_INFORMATION_CATEGORY = "gener_information_category";


    /**
     * @codeCoverageIgnore
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 检查是否是系统用户
     * @param $userId
     * @return bool
     */
    public function isSystemUser($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT user_type FROM $this->TABLE_USER WHERE id='$userId' AND is_delete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            if ($result['user_type'] == 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 检查用户对活动的查看和参与权限
     * @param $userId  用户id
     * @param $activityId 活动id
     * @return bool true代表有权限，false代表没有权限
     */
    public function verifyUserIdAndActivityId($userId, $activityId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE WHERE isDelete=0 AND id = '$activityId' AND familyId in (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId')";

        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            //@codeCoverageIgnoreStart
            return false;
            //@codeCoverageIgnoreEnd
        }
        if ($result['count(id)'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 创建活动，群发消息给家族成员
     * @param $familyId 家族id
     * @param $title   活动标题
     * @param $content 活动内容
     * @param $photo   活动配图
     * @param $startTime 起始时间
     * @param $endTime  终止时间
     * @param $deadline  报名截止时间
     * @param $userId  用户id
     * @param $coorX
     * @param $coorY
     * @param $address
     * @return int 创建的活动id
     */
    public function createActivity($familyId, $title, $content, $photo, $type, $startTime, $endTime, $deadline, $userId, $coorX, $coorY, $address)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE(familyId,title,content,photo,type,start_time,end_time,deadline,coorX,coorY,address,create_by,create_time,update_by,update_time)
                    VALUES('$familyId','$title','$content','$photo','$type','$startTime','$endTime','$deadline','$coorX','$coorY','$address','$userId',now(),'$userId',now())";


        return $this->pdo->insert($sql);
    }

    /**
     * 向圈子中创建活动
     * @param $socialCircleId
     * @param $title
     * @param $content
     * @param $photo
     * @param $type
     * @param $startTime
     * @param $endTime
     * @param $deadline
     * @param $userId
     * @param $coorX
     * @param $coorY
     * @param $address
     * @return mixed
     */
    public function addActivity($socialCircleId, $title, $content, $photo, $type, $startTime, $endTime, $deadline, $userId, $coorX, $coorY, $address)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE(socialCircleId,title,content,photo,type,start_time,end_time,deadline,coorX,coorY,address,create_by,create_time,update_by,update_time)
                    VALUES('$socialCircleId','$title','$content','$photo','$type','$startTime','$endTime','$deadline','$coorX','$coorY','$address','$userId',now(),'$userId',now())";

        $id = $this->pdo->insert($sql);

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = 'APP添加活动';
        $other_msg = [
            'id' => $id,
            'socialCircleId' => $socialCircleId,
            'title' => $title,
            'content' => $content,
            'photo' => $photo,
            'type' => $type,
            'startTime' => $startTime,
            'endTime' => $endTime,
            'deadline' => $deadline,
            'userId' => $userId,
            'coorX' => $coorX,
            'coorY' => $coorY,
            'address' => $address,
           ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = $this->TABLE;
        $tableId = $id;
        $accountDB = new CDBAccount();
        $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        return $id;
    }


    /**
     * 更新活动
     * @param $activityId 活动id
     * @param $title   活动标题
     * @param $content 活动内容
     * @param $photo   照片
     * @param $startTime 开始时间
     * @param $endTime  结束时间
     * @param $deadline 截止时间
     * @param $userId   用户id
     * @return int 更新的记录数
     */
    public function updateActivity($activityId, $title, $content, $photo, $startTime, $endTime, $deadline, $coorX, $coorY, $address, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET title = '$title', content = '$content',photo = '$photo',start_time = '$startTime',end_time = '$endTime',deadline = '$deadline',coorX='$coorX',coorY='$coorY',address='$address',update_by = '$userId',update_time = now() WHERE id = '$activityId' AND isDelete=0";

        $updateCount = $this->pdo->update($sql);

         //记录用户日志
         $function_name = __CLASS__.'->'.__FUNCTION__;
         $option_detail = 'APP更新活动';
         $other_msg = [
             'id' => $activityId,
             'title' => $title,
             'content' => $content,
             'photo' => $photo,
             'startTime' => $startTime,
             'endTime' => $endTime,
             'deadline' => $deadline,
             'userId' => $userId,
             'coorX' => $coorX,
             'coorY' => $coorY,
             'address' => $address,
            ];
         $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
         $optionTableName = $this->TABLE;
         $tableId = $activityId;
         $accountDB = new CDBAccount();
         $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

         return $updateCount;
        
    }


    /**
     * 删除活动
     * @param $activityId 活动id
     * @return int 删除的记录数
     */
    public function deleteActivity($activityId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET isDelete=1 WHERE id = '$activityId' AND isDelete=0";
        return $this->pdo->update($sql);
    }

    /**
     * 获取活动的具体内容
     * @param $activityId 活动id
     * @return object activity对象或者null
     */
    public function getActivity($activityId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.familyId,tb.socialCircleId,ic.name as socialCircleName,ic.circleType,tf.name as familyName,tb.title,tb.content,tb.photo,tb.type,tb.start_time as startTime,tb.end_time as endTime,deadline,tb.coorX,tb.coorY,tb.address,tb.create_by as createBy,tu.username as createByName,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, tu.nickname,tu.photo as userPhoto FROM $this->TABLE tb 
            LEFT JOIN $this->TABLE_USER tu ON tu.id = tb.create_by 
            LEFT JOIN $this->TABLE_FAMILY tf ON tf.id = tb.familyId 
            LEFT JOIN $this->TABLE_INFORMATION_CATEGORY ic ON tb.socialCircleId = ic.id
            WHERE tb.id = '$activityId' AND tb.isDelete=0";

        $result = $this->pdo->uniqueResult($sql);

        if ($result != null) {
            return new Activity($result);
        } else {
            return null;
        }
    }


    /**
     * 获取活动总数
     * @param $familyId 家族id -1代表全部
     * @param $userId   用户id
     * @return int 活动总数
     */
    public function getActivityCount($familyId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $where = "";
        if ($familyId == -1) {
            $where = "WHERE (familyId in 
                    (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId' AND is_delete = '0') OR familyId = 0 )";
        } else {
            $where = "WHERE ( familyId = '$familyId' OR familyId = 0 )";
        }

        $sql = "SELECT count(id) FROM $this->TABLE  $where AND isDelete=0 ";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取一页活动数据
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @param $familyId  家族id
     * @param $userId    用户id
     * @return array 一页活动数据
     */
    public function getActivityPaging($pageIndex, $pageSize, $familyId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $where = "";
        if ($familyId == -1) {
            $where = "WHERE (familyId in 
                    (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId' AND is_delete = '0' ) OR familyId = 0 )";
        } else {
            $where = "WHERE (familyId = '$familyId' OR familyId = 0 )";
        }

        $sql = "SELECT id,familyId,title,photo,type,start_time as startTime,end_time as endTime,deadline,coorX,coorY,address,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime 
                    FROM $this->TABLE 
                    $where  AND isDelete=0  ORDER BY id DESC LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    /**
     * 获取进行中的活动的总数
     * @param $familyId 家族id
     * @param $userId   用户id
     * @return int 总数
     */
    public function getActivedActivityCount($familyId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $where = "";
        if ($familyId == -1) {
            $where = "WHERE ( familyId in 
                    (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId' AND is_delete = '0') OR familyId=0)";
        } else {
            $where = "WHERE (familyId = '$familyId' OR familyId=0 )";
        }

        $sql = "SELECT count(id) 
                    FROM $this->TABLE 
                    $where  AND end_time >= now() AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取正在进行的活动数量
     * @param $socialCircleId
     * @param $userId
     * @return mixed
     */
    public function getSocialCircleActivedActivityCount($socialCircleId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($socialCircleId == -1) {
            $where = "WHERE socialCircleId in 
                    (SELECT categoryId FROM gener_information_follow WHERE userId = '$userId' AND isDelete = 0)";
        } else {
            $where = "WHERE socialCircleId = '$socialCircleId'";
        }

        $sql = "SELECT count(id) FROM $this->TABLE  $where  AND end_time >= now() AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取活动数量
     * @param $socialCircleId
     * @param $userId
     * @return mixed
     */
    public function getSocialCircleActivityCount($socialCircleId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($socialCircleId == -1) {
            $where = "WHERE socialCircleId in 
                    (SELECT categoryId FROM gener_information_follow WHERE userId = '$userId' AND isDelete = 0)";
        } else {
            $where = "WHERE socialCircleId = '$socialCircleId'";
        }

        $sql = "SELECT count(id) FROM $this->TABLE  $where  AND isDelete=0";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取一页进行中的活动
     * @param $pageIndex 页码
     * @param $pageSize  每页大小
     * @param $familyId 家族Id
     * @param $userId   用户id
     * @return array 一页进行中的活动的数据
     */
    public function getActivedActivityPaging($pageIndex, $pageSize, $familyId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $where = "";
        if ($familyId == -1) {
            $where = "WHERE ( familyId in 
                    (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId' AND is_delete = '0') OR familyId=0)";
        } else {
            $where = "WHERE (familyId = '$familyId' OR familyId=0 ) ";
        }

        $sql = "SELECT id,familyId,title,photo,type,start_time as startTime,end_time as endTime,deadline,coorX,coorY,address,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime 
                    FROM $this->TABLE 
                    $where AND end_time >= now() AND isDelete=0
                    ORDER BY id DESC 
                    LIMIT $offset,$pageSize";

        return $this->pdo->query($sql);
    }

    /**
     * 获取一页进行中的活动
     * @param $userId   用户id
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @param $socialCircleId
     * @return array 一页进行中的活动的数据
     */
    public function getSocialCircleActivedActivityPaging($userId, $maxId, $sinceId, $count, $socialCircleId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($socialCircleId == -1) {
            $where = "WHERE a.socialCircleId in 
                    (SELECT categoryId FROM gener_information_follow WHERE userId = '$userId' AND isDelete = 0)";
        } else {
            $where = "WHERE a.socialCircleId = '$socialCircleId'";
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where .= "  AND a.id<'$maxId' AND a.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where .= "  AND a.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where .= "  AND a.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where .= '';
        }
        $sql = "SELECT a.id,familyId,a.socialCircleId,ic.name as socialCircleName,ic.circleType,a.title,a.content,a.photo,a.type,
                    a.start_time as startTime,a.end_time as endTime,deadline,
                    a.coorX,a.coorY,a.address,a.create_by as createBy,a.create_time as createTime,
                    a.update_by as updateBy,a.update_time as updateTime ,tu.nickname,tu.photo as userPhoto
                    FROM $this->TABLE as a 
                    LEFT JOIN $this->TABLE_INFORMATION_CATEGORY as ic ON a.socialCircleId = ic.id 
                    LEFT JOIN $this->TABLE_USER tu ON tu.id = a.create_by 
                    $where  AND a.end_time >= now() AND a.isDelete=0
                    ORDER BY a.id DESC 
                    LIMIT $count";

        return $this->pdo->query($sql);
    }


    /**
     * 分页获取活动
     * @param $userId
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @param $socialCircleId
     * @return mixed
     */
    public function getSocialCircleActivityPaging($userId, $maxId, $sinceId, $count, $socialCircleId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($socialCircleId == -1) {
            $where = "WHERE a.socialCircleId in 
                    (SELECT categoryId FROM gener_information_follow WHERE userId = '$userId' AND isDelete = 0)";
        } else {
            $where = "WHERE a.socialCircleId = '$socialCircleId'";
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where .= "  AND a.id<'$maxId' AND a.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where .= "  AND a.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where .= "  AND a.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where .= '';
        }
        $sql = "SELECT a.id,familyId,a.socialCircleId,ic.name as socialCircleName,ic.circleType,a.title,a.content,a.photo,a.type,
                    a.start_time as startTime,a.end_time as endTime,deadline,
                    a.coorX,a.coorY,a.address,a.create_by as createBy,a.create_time as createTime,
                    a.update_by as updateBy,a.update_time as updateTime ,tu.nickname,tu.photo as userPhoto
                    FROM $this->TABLE as a 
                    LEFT JOIN $this->TABLE_INFORMATION_CATEGORY as ic ON a.socialCircleId = ic.id 
                    LEFT JOIN $this->TABLE_USER tu ON tu.id = a.create_by 
                    $where AND a.isDelete=0
                    ORDER BY a.id DESC 
                    LIMIT $count";

        return $this->pdo->query($sql);
    }

    /**
     * 分页获取活动
     * @param $maxId
     * @param $sinceId
     * @param $count
     * @return mixed
     */
    public function getOfficialSocialCircleActivityPagingNoLogin($maxId, $sinceId, $count)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $where = ' ';

        if ($maxId > 0 && $sinceId > 0) {
            $where .= "  AND a.id<'$maxId' AND a.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where .= "  AND a.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where .= "  AND a.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where .= '';
        }

        $sql = "SELECT id FROM $this->TABLE_INFORMATION_CATEGORY  WHERE  is_delete = 0  AND (circleType = 1 or circleType = 2)";

        $arr = $this->pdo->query($sql);

        $str = Util::arrayToString(array_column($arr, 'id'));

        $sql = "SELECT a.id,familyId,a.socialCircleId,ic.name as socialCircleName,ic.circleType,a.title,a.content,a.photo,a.type,
                    a.start_time as startTime,a.end_time as endTime,deadline,
                    a.coorX,a.coorY,a.address,a.create_by as createBy,a.create_time as createTime,
                    a.update_by as updateBy,a.update_time as updateTime ,tu.nickname,tu.photo as userPhoto
                    FROM $this->TABLE as a 
                    LEFT JOIN $this->TABLE_INFORMATION_CATEGORY as ic ON a.socialCircleId = ic.id 
                    LEFT JOIN $this->TABLE_USER tu ON tu.id = a.create_by WHERE a.socialCircleId IN ($str)
                    $where AND a.isDelete=0
                    ORDER BY a.id DESC 
                    LIMIT $count";


        return $this->pdo->query($sql);
    }

    public function getCountOfficialSocialCircleActivityPagingNoLogin()
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }
        $sql = "SELECT id FROM $this->TABLE_INFORMATION_CATEGORY  WHERE  is_delete = 0  AND (circleType = 1 or circleType = 2)";

        $arr = $this->pdo->query($sql);
        $str = Util::arrayToString(array_column($arr, 'id'));

        $sql = "SELECT  count('a.id') as sum
                    FROM $this->TABLE as a 
                    LEFT JOIN $this->TABLE_INFORMATION_CATEGORY as ic ON a.socialCircleId = ic.id 
                    LEFT JOIN $this->TABLE_USER tu ON tu.id = a.create_by 
                    WHERE  a.socialCircleId IN ($str) AND a.isDelete=0";

        $result = $this->pdo->uniqueResult($sql);
        return $result['sum'];
    }

    /**
     * 获取家族失效的活动的总数
     * @param $familyId 家族id
     * @param $userId   用户id
     * @return int 总数
     */
    public function getOverdueActivityCount($familyId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $where = "";
        if ($familyId == -1) {
            $where = "WHERE (familyId in 
                    (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId' AND is_delete = '0') OR familyId = 0)";
        } else {
            $where = "WHERE (familyId = '$familyId' OR familyId=0) ";
        }

        $sql = "SELECT count(id)  FROM $this->TABLE   $where AND isDelete=0 AND end_time < now()";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取圈子失效的活动的总数
     * @param $socialCircleId
     * @param $userId   用户id
     * @return int 总数
     */
    public function getSocialCircleOverdueActivityCount($socialCircleId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if ($socialCircleId == -1) {
            $where = "WHERE socialCircleId in 
                    (SELECT categoryId FROM gener_information_follow WHERE userId = '$userId' AND isDelete = '0')";
        } else {
            $where = "WHERE socialCircleId = '$socialCircleId'";
        }

        $sql = "SELECT count(id)  FROM $this->TABLE   $where AND isDelete=0 AND end_time < now()";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 获取家族一页失效的活动
     * @param $pageIndex 页码
     * @param $pageSize  每页数量
     * @param $familyId  家族id
     * @param $userId    用户id
     */
    public function getOverdueActivityPaging($pageIndex, $pageSize, $familyId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $where = "";
        if ($familyId == -1) {
            $where = "WHERE (familyId in 
                    (SELECT familyId FROM $this->TABLE_PERSON WHERE userId = '$userId' AND is_delete = '0') OR familyId = 0)";
        } else {
            $where = "WHERE ( familyId = '$familyId' OR familyId = 0) ";
        }

        $sql = "SELECT id,familyId,title,photo,type,start_time as startTime,end_time as endTime,deadline,coorX,coorY,address,create_by as createBy,create_time as createTime,update_by as updateBy,update_time as updateTime 
                    FROM $this->TABLE  $where AND isDelete=0 AND end_time < now() 
                    ORDER BY id DESC  LIMIT $offset,$pageSize";
        return $this->pdo->query($sql);
    }

    public function getSocialCircleOverdueActivityPaging($userId, $maxId, $sinceId, $count, $socialCircleId)
    {

        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        if ($socialCircleId == -1) {
            $where = "WHERE a.socialCircleId in 
                    (SELECT categoryId FROM gener_information_follow WHERE userId = '$userId' AND isDelete = 0)";
        } else {
            $where = "WHERE a.socialCircleId = '$socialCircleId'";
        }

        if ($maxId > 0 && $sinceId > 0) {
            $where .= "  AND a.id<'$maxId' AND a.id>'$sinceId' ";
        }
        if ($maxId > 0 && $sinceId == 0) {
            $where .= "  AND a.id<'$maxId'";
        }

        if ($maxId == 0 && $sinceId > 0) {
            $where .= "  AND a.id>'$sinceId'";
        }

        if ($maxId == 0 && $sinceId == 0) {
            $where .= '';
        }
        $sql = "SELECT a.id,familyId,a.socialCircleId,ic.name as socialCircleName,ic.circleType,a.title,a.content,a.photo,a.type,
                    a.start_time as startTime,a.end_time as endTime,deadline,
                    a.coorX,a.coorY,a.address,a.create_by as createBy,a.create_time as createTime,
                    a.update_by as updateBy,a.update_time as updateTime ,tu.nickname,tu.photo as userPhoto
                    FROM $this->TABLE as a 
                    LEFT JOIN $this->TABLE_INFORMATION_CATEGORY as ic ON a.socialCircleId = ic.id 
                    LEFT JOIN $this->TABLE_USER tu ON tu.id = a.create_by 
                    $where AND a.isDelete=0 AND a.end_time < now()
                    ORDER BY a.id DESC 
                    LIMIT $count";

        return $this->pdo->query($sql);
    }

    /**
     * 获取用户创建的一页活动
     * @param $pageIndex 页码
     * @param $pageSize  一页大小
     * @param $userId    用户id
     * @return array 活动数组
     */
    public function getUserCreatedActivityPaging($pageIndex, $pageSize, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $start = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT id,familyId,socialCircleId,title,photo,type,start_time as startTime,end_time as endTime,deadline,coorX,coorY,address,create_by as createBy,create_time as createTime, update_by as updateBy,update_time as updateTime FROM $this->TABLE WHERE create_by = '$userId' AND isDelete=0 ORDER BY id desc limit $start,$pageSize";

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取用户创建的活动总数
     * @param $userId 用户id
     * @return int 活动总数
     */
    public function getUserCreatedActivityCount($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT count(id) FROM $this->TABLE WHERE create_by = '$userId' AND isDelete=0 ";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];

    }

    /**
     * 获取用户参加的一页活动
     * @param $pageIndex 页码
     * @param $pageSize  页大小
     * @param $userId    用户id
     * @return array 活动数组
     */
    public function getUserParticipateActivityPaging($pageIndex, $pageSize, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $start = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT tb.id,tb.familyId,tb.title,tb.photo,tb.start_time as startTime,tb.end_time as endTime,deadline,tb.coorX,tb.coorY,tb.address,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
            FROM $this->TABLE_ACTIVITY_PARTICIPATOR  tap 
            LEFT JOIN $this->TABLE tb ON tb.id = tap.activityId 
            WHERE tap.userId = '$userId' AND tap.cancel=0 AND  tb.isDelete = 0 ORDER BY tap.id desc limit $start,$pageSize";

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 获取用户参加的活动总数
     * @param $userId 用户id
     * @return int 活动总数
     */
    public function getUserParticipateActivityCount($userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(id) FROM $this->TABLE_ACTIVITY_PARTICIPATOR WHERE userId = '$userId' AND cancel=0";
        $result = $this->pdo->uniqueResult($sql);
        return $result['count(id)'];
    }

    /**
     * 响应活动
     * @param $activityId 活动id
     * @param $userId     用户id
     * @return int OR BOOL 参加的记录id
     */
    public function participate($activityId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        //检查截止时间与现在时间的比较，获取截止时间
        $sql = "SELECT deadline FROM $this->TABLE WHERE id = '$activityId' AND isDelete=0";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            $deadline = $result['deadline'];
            $nowTime = date("Y-m-d H:i:s", time());
            if ($deadline < $nowTime) {
                return false;
            }
        } else {
            return '活动不存在';
        }
        //先检查是否曾经参加，后来取消了
        $sql = "SELECT id FROM $this->TABLE_ACTIVITY_PARTICIPATOR WHERE activityId = '$activityId' AND userId = '$userId' AND cancel=1 ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            $recordId = $result['id'];
            $sql = "UPDATE $this->TABLE_ACTIVITY_PARTICIPATOR SET cancel = '0' WHERE id = '$recordId'";
            $this->pdo->update($sql);
            return $recordId;
        } else {
            $sql = "INSERT INTO $this->TABLE_ACTIVITY_PARTICIPATOR(activityId,userId,cancel,create_time,create_by,update_time,update_by)VALUES('$activityId','$userId','0',now(),'$userId',now(),'$userId')";

            return $this->pdo->insert($sql);
        }
    }

    /**
     * 取消活动参加
     * @param $activityId 活动id
     * @param $userId     用户id
     * @return int        更改的记录数
     */
    public function cancelParticipate($activityId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_ACTIVITY_PARTICIPATOR SET cancel = '1' WHERE activityId = '$activityId' AND userId = '$userId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 检查活动参加状态
     * @param $activityId 活动id
     * @param $userId     用户id
     * @return int 0 没参加  1参加了
     */
    public function checkActivityParticipateStatus($activityId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT cancel FROM $this->TABLE_ACTIVITY_PARTICIPATOR WHERE activityId = '$activityId' AND userId = '$userId' ";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            $cancel = $result['cancel'];
            if ($cancel == 0) {
                return 1;
            }
            return 0;
        }
        return 0;
    }

    /**
     * 获取活动的参与者
     * @param $activityId 活动id
     * @return array 参与的人员信息
     */
    public function getActivityParticipator($activityId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT tu.id,tu.username,tu.nickname,tu.photo FROM $this->TABLE_ACTIVITY_PARTICIPATOR tb 
                    left join $this->TABLE_USER tu on tu.id = tb.userId 
                    WHERE tb.activityId = '$activityId' AND tb.cancel = '0' LIMIT 0,10";

        return $this->pdo->query($sql);
    }


    /**
     * 获取参加活动的用户总数
     * @param $activityId
     * @return mixed
     */
    public function getActivityParticipatorCount($activityId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT count(*) AS sum FROM $this->TABLE_ACTIVITY_PARTICIPATOR WHERE activityId = '$activityId'  AND cancel=0 ";

        $result = $this->pdo->uniqueResult($sql);
        return $result['sum'];
    }


    /**
     * 获取参加活动的用户分页
     * @param $activityId
     * @param $pageIndex
     * @param $pageSize
     * @return mixed
     */
    public function getActivityParticipatorPage($activityId, $pageIndex, $pageSize)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $start = ($pageIndex - 1) * $pageSize;
        $sql = "SELECT tu.id,tu.username,tu.nickname,tu.photo FROM $this->TABLE_ACTIVITY_PARTICIPATOR tb 
                    left join $this->TABLE_USER tu on tu.id = tb.userId 
                    WHERE tb.activityId = '$activityId' AND tb.cancel = '0' LIMIT $start,$pageSize";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * 根据用户id数组来获取参与者信息
     * @param $activityIds 活动id数组
     * @return mix 用户参与情况
     */
    public function getActivityParticipatorByIds($activityIds)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        if (!is_array($activityIds) || count($activityIds) <= 0) {
            return null;
        }

        $where = "tb.cancel = '0' AND ";
        $isFirst = true;
        foreach ($activityIds as $value) {
            if ($isFirst) {
                $where = $where . " (tb.activityId = '$value' ";
                $isFirst = false;
            } else {
                $where = $where . " OR tb.activityId = '$value' ";
            }
        }

        $sql = "SELECT tu.id,tu.username,tu.nickname,tu.photo,tb.activityId FROM $this->TABLE_ACTIVITY_PARTICIPATOR tb 
                    left join $this->TABLE_USER tu on tu.id = tb.userId 
                    WHERE " . $where . ")";

        return $this->pdo->query($sql);
    }


    /**
     * 对活动添加评论
     * @param $activityId
     * @param $userId
     * @param $replyTo
     * @param $replyToUserId
     * @param $comment
     * @param $path
     * @return mixed
     */
    public function addActivityComment($activityId, $userId, $replyTo, $replyToUserId, $comment, $path)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "INSERT INTO $this->TABLE_ACTIVITY_COMMENT(activityId,userId,replyto,replyto_userId,comment,create_time,create_by,update_time,update_by,path)
                  VALUES ('$activityId','$userId','$replyTo','$replyToUserId','$comment',now(),'$userId',now(),'$userId','$path')";
        $commentId = $this->pdo->insert($sql);

        //记录用户日志
        $function_name = __CLASS__.'->'.__FUNCTION__;
        $option_detail = '添加活动评论';
        $other_msg = [
            'activityId' => $activityId,
            'replyTo' => $replyTo,
            'replyToUserId' => $replyToUserId,
            'comment' => $comment,
            'path' => $path,
            'userId' => $userId,
           ];
        $other_msg = json_encode($other_msg, JSON_UNESCAPED_UNICODE);
        $optionTableName = $this->TABLE_ACTIVITY_COMMENT;
        $tableId = $commentId;
        $accountDB = new CDBAccount();
        $accountDB->recordUserOptionLog($function_name, $option_detail, $userId, $other_msg, $optionTableName, $tableId);

        return $commentId;
    }


    public function updatePath($commentId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE_ACTIVITY_COMMENT SET path='$commentId' WHERE id='$commentId'";
        return $this->pdo->update($sql);
    }


    /**
     * 获取活动下的所有评论
     * @param $activityId
     * @return mixed
     */
    public function getActivityComment($activityId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT gr.nickname as replytoUsername,gr.photo as replytoUserPhoto,gu.photo,gu.nickname,ga.id,ga.activityId,ga.userId,ga.replyto,ga.replyto_userId as replytoUserId,ga.comment,ga.create_time as createTime,ga.create_by as createBy,ga.update_time as updateTime,ga.update_by as updateBy FROM 
                $this->TABLE_ACTIVITY_COMMENT ga INNER JOIN $this->TABLE_USER gu ON gu.id=ga.userId LEFT JOIN $this->TABLE_USER gr ON gr.id=ga.replyto_userId WHERE ga.activityId='$activityId' AND ga.is_delete='0' ORDER BY ga.id DESC";
        $result = $this->pdo->query($sql);
        return $result;
    }


    public function selectPath($replyTo)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "SELECT id,path,replyto as replyTo FROM $this->TABLE_ACTIVITY_COMMENT WHERE id='$replyTo'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result != null) {
            return $result;
        } else {
            return false;
        }

    }


    /**
     * 删除一条评论下的所有回复
     * @param $commentId
     * @return mixed
     */
    public function deleteReplyComment($commentId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }
        $sql = "UPDATE $this->TABLE_ACTIVITY_COMMENT SET is_delete = '1' WHERE path like '%$commentId%'";
        $result = $this->pdo->update($sql);
        return $result;
    }


    /**
     * 删除评论
     * @param $commentId
     * @return mixed
     */
    public function deleteComment($commentId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_ACTIVITY_COMMENT SET is_delete = '1' WHERE id = $commentId";
        $result = $this->pdo->update($sql);

        return $result;
    }

    /**
     * 验证评论Id和用户id是否匹配，即用户是否有操作评论id的权限，活动的创建者可以删除活动下的评论
     * @param $commentId 评论id
     * @param $userId    用户id
     * @return bool true代表匹配，false代表不匹配
     */
    public function verifyCommentIdAndUserId($commentId, $userId)
    {
        if (!$this->init()) {
            // @codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            // @codeCoverageIgnoreEnd
        }

        $sql = "SELECT pt.create_by as createBy FROM $this->TABLE_ACTIVITY_COMMENT ct INNER JOIN $this->TABLE pt
                ON ct.activityId=pt.id WHERE ct.id='$commentId'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        }
        if ($result['createBy'] != $userId) {
            $sql = "SELECT create_by as createBy FROM $this->TABLE_ACTIVITY_COMMENT WHERE id='$commentId'";
            $result = $this->pdo->uniqueResult($sql);
            if ($result != null && $result['createBy'] == $userId) {
                return true;
            }
            return false;
        }
        return true;
    }
}
