<?php
/**
 * 家族对外分享时的数据库封装
 * @author: jiangpengfei
 * @date: 2018-02-02
 */

 
namespace DB;

use DB\CDBManager;
use DB\CDBAccount;
use DB\CDBPerson;
use Model\Family;
use Model\FamilyShare;
use Model\FamilyShareLog;
use Util\Util;

class CDBFamilyShare{
    private $pdo = null;
    private $TABLE = 'gener_family_share';
    private $TABLE_LOG = 'gener_family_share_log';
    private $TABLE_FAMILY = 'gener_family';

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 检查某个shareid和ip是否存在
     * @param $shareId 分享的id
     * @param $ip      访问的ip记录
     * @return bool true代表存在，false不存在
     */
    public function checkShareIdAndIpExist(int $shareId,string $ip){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE_LOG WHERE shareId = '$shareId' AND ip = '$ip' ";
        $result = $this->pdo->uniqueResult($sql);
        if($result == null){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 创建家族分享
     * @param $familyShareId FamilyShare对象
     * @return int 插入的记录id
     */
    public function createFamilyShare(FamilyShare $familyShare){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE
                (familyId,shareCode,time_limit,level_limit,views_limit,create_time,create_by,update_time,update_by)
                VALUES
                ('$familyShare->familyId','$familyShare->shareCode','$familyShare->timeLimit','$familyShare->levelLimit',
                '$familyShare->viewsLimit',now(),'$familyShare->createBy',now(),'$familyShare->createBy')";

        return $this->pdo->insert($sql);
    }

    /**
     * 删除家族分享
     * @param $familyShareId 家族分享的id
     * @return 更新的记录数
     */
    public function deleteFamilyShare(int $familyShareId){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET is_delete = '1' WHERE id = '$familyShareId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 获取家族分享的model
     * @param $familyShareId 家族分享id
     * @return mix FamilyShare对象或者是null
     */
    public function getFamilyShareModel(int $familyShareId){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.familyId,tb.shareCode,tb.time_limit as timeLimit,tb.level_limit as levelLimit,tb.views_limit as viewsLimit,
                tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                FROM $this->TABLE tb 
                WHERE tb.id = '$familyShareId' AND tb.is_delete = '0' ";

        $item = $this->pdo->uniqueResult($sql);
        if($item == null){
            return null;
        }else{
            return new FamilyShare($item);
        }
    }

    /**
     * 分享码
     * @param $shareCode 分享码
     * @return mix FamilyShare对象或者是null
     */
    public function getFamilyShareModelByShareCode(string $shareCode){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.familyId,tb.shareCode,tb.time_limit as timeLimit,tb.level_limit as levelLimit,tb.views_limit as viewsLimit,
                tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                FROM $this->TABLE tb 
                WHERE tb.shareCode = '$shareCode' AND tb.is_delete = '0' ";

        $item = $this->pdo->uniqueResult($sql);
        if($item == null){
            return null;
        }else{
            return new FamilyShare($item);
        }
    }

    /**
     * 获取某条家族分享详情
     * @param $familyShareId 家族分享id
     * @return mix FamilyShare对象或者是null
     */
    public function getFamilyShareDetail(int $familyShareId){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT tb.id,tb.familyId,tf.name as familyName,tf.photo as familyPhoto,tb.shareCode,tb.time_limit as timeLimit,tb.level_limit as levelLimit,tb.views_limit as viewsLimit,
                tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                FROM $this->TABLE tb 
                LEFT JOIN $this->TABLE_FAMILY tf on tf.id = 'tb.familyId' 
                WHERE tb.id = '$familyShareId' AND tb.is_delete = '0' ";

        $item = $this->pdo->uniqueResult($sql);
        if($item == null){
            return null;
        }else{
            return new FamilyShare($item);
        }
    }

    /**
     * 增加家族分享日志记录
     * @param $log 日志
     * @return int 插入的记录id
     */
    public function addFamilyShareLog(FamilyShareLog $log){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "INSERT INTO $this->TABLE_LOG 
                (shareId,ip,times,create_time,update_time)
                VALUES
                ('$log->shareId','$log->ip','1',now(),now())";

        return $this->pdo->insert($sql);
    }

    /**
     * 分页获取用户家族分页的记录
     * @param $userId 用户id
     * @param $familyId 家族id，-1则代表所有家族
     * @param $pageIndex 页码
     * @param $pageSize 每页记录
     * @return array 一页的分享记录
     */
    public function getUserFamilySharePaging(int $userId,int $familyId,int $pageIndex,int $pageSize){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $familyWhere = "";
        if($familyId != -1){
            $familyWhere = " AND tb.familyId = '$familyId' ";
        }

        $sql = "SELECT tb.id,tb.familyId,tf.name as familyName,tf.photo as familyPhoto,tb.shareCode,tb.time_limit as timeLimit,tb.level_limit as levelLimit,tb.views_limit as viewsLimit,
                tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime 
                FROM $this->TABLE tb 
                LEFT JOIN $this->TABLE_FAMILY tf on tf.id = 'tb.familyId' 
                WHERE tb.create_by = '$userId' AND tb.is_delete = '0' $familyWhere
                ORDER BY tb.id DESC 
                LIMIT $offset,$pageSize";

        return $this->pdo->query($sql);
    }

    /**
     * 获取用户家族分享的总数
     * @param $userId 用户id
     * @param $familyId 家族id，-1则代表所有家族
     * @return int 分享记录的总数
     */
    public function getUserFamilyShareTotal(int $userId,int $familyId){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $familyWhere = "";
        if($familyId != -1){
            $familyWhere = " AND familyId = '$familyId' ";
        }

        $sql = "SELECT count(*) FROM $this->TABLE WHERE create_by = '$userId' AND is_delete = '0' $familyWhere";

        $result = $this->pdo->uniqueResult($sql);
        return $result['count(*)'];
    }

    /**
     * 分页获取某条家族分享的日志
     * @param $shareId 分享记录的id
     * @param $pageIndex 页码
     * @param $pageSize 每页记录数
     * @return array 日志数组
     */
    public function getFamilyShareLogPaging(int $shareId,int $pageIndex,int $pageSize){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $offset = ($pageIndex - 1) * $pageSize;

        $sql = "SELECT id,shareId,ip,times,create_time as createTime,update_time as updateTime 
                FROM $this->TABLE_LOG WHERE id = '$shareId' 
                ORDER BY id DESC 
                LIMIT $offset,$pageSize";

        return $this->pdo->query($sql);

    }

    /**
     * 获取某条家族分享的日志总数
     * @param $shareId 分享id
     * @return int 总数
     */
    public function getFamilyShareLogTotal(int $shareId){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT count(*) FROM $this->TABLE_LOG WHERE id = '$shareId' ";

        $result = $this->pdo->uniqueResult($sql);

        return $result['count(*)'];
    }

    /**
     * 更新家族分享记录的查看次数
     */
    public function updateFamilyShareLogViewTimes(int $shareId,string $ip){
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE_LOG SET times = times + 1 WHERE shareId = '$shareId' AND ip = '$ip' ";
        return $this->pdo->update($sql);
    }
}