<?php
/**
 * 用户邀请码模块
 * author: jiangpengfei
 * date: 2017-12-04
 */

namespace DB;

use DB\CDBManager;
use Util\Util;

class CDBUserInvitationCode
{
    public $pdo = null;
    public $TABLE = "gener_user_invitation_code";
    public $TABLE_USER_CODE = "gener_user_code";

    /**
     * @codeCoverageIgnore
     */
    public function init()
    {

        if (!$this->pdo) {
            //pdo使用全局的，保证当有pdo实例存在时，不用重新创建
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }
        return true;
    }

    /**
     * 生成用户的邀请码
     * @param $owner  用户id
     * @return string 邀请码
     */
    public function addUserInvitationCode($owner)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $invitationCode = strtolower(Util::generateRandomCode(8));      //生成8位随机的邀请码,转换成小写存储

        $sql = "INSERT INTO $this->TABLE
                    (owner,code,create_time,create_by)
                    VALUES
                    ('$owner','$invitationCode',now(),'$owner')";

        $result = $this->pdo->insert($sql);
        return $invitationCode;
    }

    /**
     * 检查邀请码是否合法
     * @param $invitationCode 邀请码
     * @return boolean true代表邀请码可用，false代表不可用
     */
    public function checkInvitationCode($invitationCode)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE code = '$invitationCode'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 检查用户是否有二维码
     * @param $userId 用户Id
     * @return mix 没有返回false,有返回二维码
     */
    public function checkUserHasCode($userId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,code,create_time,create_by FROM $this->TABLE WHERE owner = '$userId'";
        $result = $this->pdo->uniqueResult($sql);
        if ($result == null) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 绑定用户id到邀请码中
     * @param $userId 用户id
     * @param $code
     * @return int 更新的记录数
     * @internal param 用户邀请码id $codeId
     */
    public function bindUserIdToInvitationCode($userId, $code)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], null);
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id FROM $this->TABLE WHERE code = '$code'";
        $result = $this->pdo->uniqueResult($sql);

        if ($result == null) {
            return -1;
        }

        $codeId = $result['id'];

        $sql = "INSERT INTO $this->TABLE_USER_CODE(codeId,userId,create_time,create_by)VALUES('$codeId','$userId',now(),'$userId')";
        return $this->pdo->insert($sql);
    }
}