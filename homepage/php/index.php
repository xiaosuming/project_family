<?php
    header('Access-Control-Allow-Origin: *');
    
    date_default_timezone_set("Asia/Shanghai");
    require_once("logicserver/action_file.php");
    require_once('vendor/autoload.php');
    //require_once('function/SystemMonitor.class.php');

    $donkeyConf = Yaconf::get('donkey');
    ini_set("donkeyid.node_id", $donkeyConf['nodeId']);

    use Util\Util;
    use Util\SystemMonitor;
    use Util\SysLogger;
    
    use Model\Limiter;

    // define("SYSTEM_START_MEMORY",memory_get_usage());     //定义系统开始内存占用
    // define("SYSTEM_START_TIME",microtime(true));      //定义系统开始时间

    //define('DEBUG_MODE',FALSE);            //调试模式的开启与关闭

    $logger = SysLogger::getInstance();
 
    $params = $_REQUEST;

    $action = isset($params['action']) ? $params['action'] : '';


    if(!array_key_exists($action, $action_file_array))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "action not exist";
        Util::printResult($errorCode, $data);
        exit;
    }

    $action_file = $action_file_array[$action];

    if (!file_exists($action_file))
    {
        $errorCode = $GLOBALS['ERROR_REQUEST'];
        $data = "action file not exist";
        Util::printResult($errorCode, $data);
        exit;
    }

    require_once($action_file);

    //TODO 在正式运行中，务必注释掉以下代码
    // if(DEBUG_MODE){
    //     //调试模式下，可以输出耗时和内存占用
    //     echo SystemMonitor::getUseTime()."(us)";
    //     echo SystemMonitor::getThroughputRate()."||||";
    //     echo SystemMonitor::getUseMem();
    // }
