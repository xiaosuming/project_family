<?php
    header('Access-Control-Allow-Origin: *');
    
    date_default_timezone_set("Asia/Shanghai");
    require_once("logicserver/action_file.php");
    require_once('vendor/autoload.php');
    
    //require_once('function/SystemMonitor.class.php');

    use Util\Util;
    use Util\SystemMonitor;
    use Util\SysLogger;
    use ThirdParty\Wxpay\WxPayConfig;
    use ThirdParty\Wxpay\PayNotifyCallBack;

    // define("SYSTEM_START_MEMORY",memory_get_usage());     //定义系统开始内存占用
    // define("SYSTEM_START_TIME",microtime(true));      //定义系统开始时间

    //define('DEBUG_MODE',FALSE);            //调试模式的开启与关闭

    $logger = SysLogger::getInstance(true, 'wxpay_notify');

    $data['row'] = file_get_contents('php://input');
    $data['request'] = $_REQUEST;

    $logger->info('wxpay_notify', $data);

    try {
        $wxConfig = new WxPayConfig();
        $notify = new PayNotifyCallBack();
        
        $result = $notify->Handle($wxConfig, $data['row'], false);
    } catch (WxPayException $e) {
        $logger = SysLogger::getInstance()->error('order_query', Util::exceptionFormat($e));
    }

    echo $result;