<?php
//暂时不在使用
    /**
     * 文件名:familyFilter.php
     * 功能:	对请求进行统一验证，这里主要是针对家族成员请求查看家族所有的资料时需要密码而进行的验证
     * 作者: 江鹏飞
     * 创建日期:2016-11-19
     * 更新日期:2016-11-19
     * 版本:1.0
     */
    require_once("function.php");
    require_once("Constants.php");
    if(!isset($_COOKIE['familyId']) || $_COOKIE['familyId']==""){
        printResult($GLOBALS['ERROR_FAMILY_LOGIN'], "当前状态没有登录");
        exit;
    }else{
        //判断请求的家族和cookie中的家族是否对应
    }