<?php
    /**
     * 文件名:filter.php,做测试用的，去掉了登录速度限制
     * 功能:	对请求进行统一验证,这里主要针对用user的验证
     * 作者: 江鹏飞
     * 创建日期:2016-11-12
     * 更新日期:2016-11-12
     * 版本:1.0
     */
    
    use Util\Util;
    use Util\RateLimit;
    use DB\CDBAccount;
    use Util\TempStorage;

    //检查当前请求中是否存在白名单并且请求是否在白名单里面    
    if(!isset($whiteList)||isset($whiteList)&&!array_key_exists($params['sub_action'], $whiteList)){
        $accountDB = new CDBAccount();
        if (!$accountDB)
                exit;
        if(isset($params['id_token'])){
            $res = $params['id_token'];
            $arrRes = explode("|",$res);

            if(count($arrRes) != 3){
                Util::printResult($GLOBALS['ERROR_LOGIN'], "当前状态没有登录");
                exit;
            }

            
            $GLOBALS['userId'] =  $arrRes[0];
            $GLOBALS['token'] = $arrRes[1];
            $GLOBALS['usingDevice'] = $arrRes[2];		//
            $deviceCode = isset($params['deviceCode']) ? $params['deviceCode'] : '';
            try{
                $GLOBALS['deviceCode'] = $deviceCode;
                if(!$accountDB->verifyUserIdAndTokenWithDevice($GLOBALS['userId'], $GLOBALS['token'],$GLOBALS['usingDevice'],$deviceCode)){
                    Util::printResult($GLOBALS['ERROR_LOGIN'], "当前状态没有登录");
                    exit;
                }
            }catch(PDOException $e){
                $logger->error(Util::exceptionFormat($e));
            }
        }else{
            Util::printResult($GLOBALS['ERROR_LOGIN'], "当前状态没有登录");
            exit;
        }
    }