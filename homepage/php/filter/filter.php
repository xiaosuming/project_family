<?php
    /**
     * 文件名:filter.php
     * 功能:	对请求进行统一验证,这里主要针对用user的验证
     * 作者: 江鹏飞
     * 创建日期:2016-11-12
     * 更新日期:2016-11-12
     * 版本:1.0
     */
    
    use Util\Util;
    use Util\RateLimit;
    use DB\CDBAccount;
    use Util\TempStorage;
    use Util\HttpContext;

//检查当前请求是否在速率限制的接口名单中
    if(isset($rateLimitList)&&array_key_exists($params['sub_action'], $rateLimitList)){
        // 使用token bucket算法
        $rateLimit = new RateLimit($GLOBALS['RATE_LIMIT_SHA']);

        $requestIP = $_SERVER["REMOTE_ADDR"];
        $limiter = $rateLimitList[$params['sub_action']];

        
        if ($limiter->extra !== null) {
            // 这一项也要拿到令牌
            $res2 = $rateLimit->getToken('rl_'.$params['sub_action'].$limiter->extra,
                                        $limiter->extraCapacity,
                                        $limiter->extraCycle);

            if (!$res2) {
                //没有拿到令牌则输出错误
                Util::printResult($GLOBALS['ERROR_OVER_LIMIT'], "您的请求已超过限制,请稍后再试");
                exit;
            }
        }


        $res1 = $rateLimit->getToken('rl_'.$params['sub_action'].$requestIP, $limiter->capacity, $limiter->cycle);
        /*
        if (!$res1) {
            //没有拿到令牌则判断有没有验证码
            if (!isset($params['captcha']) || !isset($params['captchaSession']) ) {
                //没有captcha
                Util::printResult($GLOBALS['ERROR_OVER_SPEED'], "请求速度过快，请输入验证码验证您的身份");
                exit;
            } else {

                $tempStorage = new TempStorage();
                $rightCaptcha = $tempStorage->get($params['captchaSession']);

                if ($rightCaptcha !== strtolower($params['captcha']) ){
                    // 图形验证码错误
                    $tempStorage->del($params['captchaSession']);
                    Util::printResult($GLOBALS['ERROR_CAPTCHA'], "验证码输入错误");
                    exit;
                }
                // 删除session
                $tempStorage->del($params['captchaSession']);
            }
            
        }*/



    }

    //检查当前请求中是否存在白名单并且请求是否在白名单里面    
    if(!isset($whiteList)||isset($whiteList)&&!array_key_exists($params['sub_action'], $whiteList)){
        $accountDB = new CDBAccount();

        if(isset($params['id_token'])){
            $res = $params['id_token'];
            $arrRes = explode("|",$res);

            if(count($arrRes) != 3){
                Util::printResult($GLOBALS['ERROR_LOGIN'], "当前状态没有登录");
                exit;
            }

            
            $GLOBALS['userId'] =  $arrRes[0];
            $GLOBALS['token'] = $arrRes[1];
            $GLOBALS['usingDevice'] = $arrRes[2];		//
            HttpContext::getInstance()->setEnv('userId', $GLOBALS['userId']);      
            $deviceCode = isset($params['deviceCode']) ? $params['deviceCode'] : '';
            // 增加deviceDetail
            $GLOBALS['DeviceDetail'] = $params['DeviceDetail'] ?? 'PC';
            if(isset($params['DeviceDetail'])){
                $GLOBALS['DeviceDetail'] = $params['DeviceDetail'];
            }elseif( $GLOBALS['usingDevice'] == 1){
                $GLOBALS['DeviceDetail'] = 'PC';
            }elseif($GLOBALS['usingDevice'] == 2){
                $GLOBALS['DeviceDetail'] = 'mobile';
            }elseif($GLOBALS['usingDevice'] == 3){
                $GLOBALS['DeviceDetail'] = 'miniprogram';
            }else{
                $GLOBALS['DeviceDetail'] = 'null';
            }
            try{

                 ##检查token redis 用于登陆身份识别
                 $TempStorage = new TempStorage();

                 if( $GLOBALS['usingDevice'] == 1 && $TempStorage->get($GLOBALS['token']) != $GLOBALS['userId']){
                    Util::printResult($GLOBALS['ERROR_LOGIN'], "当前状态没有登录");
                    exit;
                } 
                ##检查token redis 用于登陆身份识别
                 
                $GLOBALS['deviceCode'] = $deviceCode;
                if(!$accountDB->verifyUserIdAndTokenWithDevice($GLOBALS['userId'], $GLOBALS['token'],$GLOBALS['usingDevice'],$deviceCode)){
                    Util::printResult($GLOBALS['ERROR_LOGIN'], "当前状态没有登录");
                    exit;
                } else {
                    if (isset($params['private_db_host'])) {
                        HttpContext::getInstance()->setEnv('private_db_host', $params['private_db_host']);
                    }  

                    ##每次请求成功就重置token时间
                    if($GLOBALS['usingDevice'] == 1){
                        $TempStorage->setTemp($GLOBALS['token'], $GLOBALS['userId'],7200); 
                    }
                    ##每次请求成功就重置token时间

                }
            }catch(PDOException $e){
                $logger->error(Util::exceptionFormat($e));
            }
        }else{
            Util::printResult($GLOBALS['ERROR_LOGIN'], "当前状态没有登录");
            exit;
        }
    }