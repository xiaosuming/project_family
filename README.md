# 爱族群web项目

## 概要介绍
homepage是web的主要部分，docker是docker部署脚本，processor是php/cli下的处理脚本,search是网站的内部搜索


## git使用须知
项目主要有两个分支，`master`是稳定分支，只接受merge pull request，不接受代码直接更改。`dev`是开发分支。W

git的教程可以参考这里:http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000


## 后端开发请注意
项目将逐渐向php7迁移，请熟悉php7的新特性。本地开发环境需要将报错级别调至最低——notice，防止代码中有错。

数据库字段为`int`和`date`和`datetime`的，请一定要注意如果为空的默认处理

使用php-cs-fix来格式化代码
composer global require friendsofphp/php-cs-fixer

## 如果发现项目中有问题，可以提工单记录，防止忘记


项目计划概览：
![项目计划概览](http://192.168.3.61:3000/xinhuo/family/src/dev/524466380.png)


## 单元测试

在修改或者增加一些类和函数的时候，都需要增加单元测试代码。所有的单元测试代码会在提交时自动进行单元测试。

结果地址：http://192.168.3.61:8888/

## 接口测试的使用
在api_auto_test中，存放了接口测试的脚本。testcase是每一个接口的测试用户，后台开发人员在添加新的接口后，需要书写测试用例。

测试用例的书写：按照已有的例子书写。其中导出值的意思是该接口的返回值会导出保存其他。当其他接口使用了该导出值同名的导入值时，会自动导入使用。


所有测试用例的执行：./start.sh [dep]，当带上参数dep时。会重新构建数据库

## 项目php扩展依赖

该项目依赖以下php扩展
php7.0
php-redis
php-mysql 
php-mbstring 
php-gd 
php-curl 
php-mongodb
php-bcmath
php-xml
php-imagick

php_dv (任务队列中用来绘制族谱树的)
md2pic (任务队列中用来将markdown转换为图片的)
opencc4php: 简繁体转换,https://github.com/NauxLiu/opencc4php
opencc4php需要依赖opencc库。在debian系的linux系统下，可能会和系统中/usr/lib/x86_64-linux-gnu/下面的libopencc库冲突，需要删除后者。
donkeyid 用来生成主键id
yaconf

## 项目依赖yaconf来进行配置的管理，测试使用的配置在doc文件夹下